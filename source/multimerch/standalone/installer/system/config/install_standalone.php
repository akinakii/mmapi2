<?php
// Site
$_['site_base'] = HTTP_SERVER;
$_['site_ssl'] = HTTP_SERVER;

// Autostart DB if Opencart already installed
if (is_file(DIR_OPENCART . 'admin/config.php') && filesize(DIR_OPENCART . 'admin/config.php') > 0) {
	$lines = file(DIR_OPENCART . 'admin/config.php');

	foreach ($lines as $line) {
		if (strpos(strtoupper($line), 'DB_') !== false) {
			eval($line);
		}

		if (strpos(strtoupper($line), 'HTTP_CATALOG') !== false) {
			eval($line);
		}
		if (strpos(strtoupper($line), 'HTTPS_CATALOG') !== false) {
			eval($line);
		}
		if (strpos(strtoupper($line), 'DIR_DOWNLOAD') !== false) {
			eval($line);
		}
		if (strpos(strtoupper($line), 'DIR_CATALOG') !== false) {
			eval($line);
		}
		if (strpos(strtoupper($line), 'DIR_LOGS') !== false) {
			eval($line);
		}
		
		
	}
	// Database
	$_['db_autostart'] = true;
	$_['db_type'] = DB_DRIVER;
	$_['db_hostname'] = DB_HOSTNAME;
	$_['db_username'] = DB_USERNAME;
	$_['db_password'] = DB_PASSWORD;
	$_['db_database'] = DB_DATABASE;
	$_['db_port'] = DB_PORT;
}

// Language
$_['language_default'] = 'en-gb';
$_['language_autoload'] = array('en-gb');

// Actions
$_['action_default'] = 'install/main';
$_['action_router'] = 'startup/router';
$_['action_error'] = 'error/not_found';
$_['action_pre_action'] = array(
	'startup/language',
	'startup/upgrade',
);
