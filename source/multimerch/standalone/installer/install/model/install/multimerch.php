<?php

use \MultiMerch\Core\Shipping\Shipping;

class ModelInstallMultimerch extends Model
{
	private $_controllers = [
		"module/multimerch",
		"multimerch/attribute",
		"multimerch/badge",
		"multimerch/base",
		"multimerch/category",
		"multimerch/conversation",
		"multimerch/custom-field",
		"multimerch/coupon",
		"multimerch/dashboard",
		"multimerch/debug",
		"multimerch/event",

		"multimerch/field",
		"multimerch/field/attribute",
		"multimerch/field/default-configuration",
		"multimerch/field/variation",
		"multimerch/field/seller-property",

		"multimerch/feed",
		"multimerch/invoice",
		"multimerch/notification",
		"multimerch/option",
		"multimerch/order",
		"multimerch/payment",
		"multimerch/payment-gateway",
		"multimerch/payment-gateway-customer",
		"multimerch/payment-gateway-marketplace",
		"multimerch/payout",
		"multimerch/product",
		"multimerch/question",

		"multimerch/report",
		"multimerch/report/finances-payment",
		"multimerch/report/finances-payout",
		"multimerch/report/finances-seller",
		"multimerch/report/finances-transaction",
		"multimerch/report/sales",
		"multimerch/report/sales-customer",
		"multimerch/report/sales-day",
		"multimerch/report/sales-month",
		"multimerch/report/sales-product",
		"multimerch/report/sales-seller",

		"multimerch/review",
		"multimerch/seller",
		"multimerch/seller-group",
		"multimerch/settings",
		"multimerch/shipping-method",
		"multimerch/social_link",
		"multimerch/suborder-status",
		"multimerch/transaction",
		"multimerch/customer",
		"multimerch/geo_zone",
		"multimerch/country",
		"multimerch/zone",
		"multimerch/information",

		"total/mm_shipping_total",
		"total/ms_coupon"
	];
	private $settings = [
		"mxtconf_installed" => 1,

		/* license */
		"msconf_license_key" => '',
		"msconf_license_activated" => 0,

		/* badges */
		"msconf_badge_enabled" => 0,
		"msconf_badge_width" => 50,
		"msconf_badge_height" => 50,

		/* social links */
		"msconf_sl_status" => 0,
		"msconf_sl_icon_width" => 30,
		"msconf_sl_icon_height" => 30,

		/* messaging */
		"mmess_conf_enable" => 1,

		/* disqus comments */
		'mxtconf_disqus_enable' => 0,
		'mxtconf_disqus_shortname' => '',

		/* core settings */
		"msconf_seller_validation" => MsSeller::MS_SELLER_VALIDATION_NONE,

		"msconf_nickname_rules" => 0, // 0 - alnum, 1 - latin extended, 2 - utf

		"msconf_credit_order_statuses" => array(
			'oc' => array(5),
			'ms' => array()
		),
		"msconf_debit_order_statuses" => array(
			'oc' => array(8),
			'ms' => array()
		),
		"msconf_paypal_sandbox" => 1,
		"msconf_paypal_address" => "",

		"msconf_allow_withdrawal_requests" => 1,

		"msconf_allow_free_products" => 1,
		"msconf_allow_digital_products" => 0,

		"msconf_allow_seller_categories" => 0,
		"msconf_allow_multiple_categories" => 0,
		"msconf_enforce_childmost_categories" => 1,

		"msconf_allow_seller_attributes" => 0,
		"msconf_allow_seller_options" => 0,
		"msconf_allowed_seller_option_types" => array(
			'choose' => array('select', 'radio', 'checkbox'),
			'input' => array('text', 'textarea'),
			'file' => array('file'),
			'date' => array('date', 'time', 'datetime')
		),

		"msconf_product_included_fields" => array('price', 'images'),

		"msconf_images_limits" => array(0,0),
		"msconf_downloads_limits" => array(0,0),

		"msconf_account_pagination_limit" => 10,

		"msconf_enable_shipping" => 0, // 0 - no, 1 - yes, 2 - seller select

		"msconf_allow_relisting" => 0,

		"msconf_product_image_path" => 'sellers/',
		"msconf_temp_image_path" => 'tmp/',
		"msconf_temp_download_path" => 'tmp/',
		"msconf_default_seller_group_id" => 1,

		"msconf_allow_description_images" => 0,
		"msconf_description_images_type" => 'upload', //base64, upload

		/* Reviews */
		"msconf_reviews_enable" => 0,

		/* Import */
		"msconf_import_enable" => 0,
		"msconf_import_category_type" => 0, // 0 - All levels in a single cell, 1 - Different levels in different cells
		"msconf_feed_external_source_enabled" => 1,
		"msconf_feed_primary_product_field" => 'model',
		"msconf_scheduled_import_enabled" => 0,

		/* Shipping system */
		"msconf_shipping_type" => 3, // 0 - disabled, 1 - OC shipping, 2 - based on cart weight, 3 - based on cart total, 4 - flat, 5 - per-product
		"msconf_shipping_per_product_override_allow" => 0, // allow global shipping rules override by per-product rules
		"msconf_shipping_field_from_enabled" => 0,
		"msconf_shipping_field_processing_time_enabled" => 0,
		"msconf_shipping_field_shipping_method_enabled" => 1,
		"msconf_shipping_field_delivery_time_enabled" => 0,
		"msconf_shipping_field_cost_ai_enabled" => 0, // for per-product rules
		"msconf_shipping_field_cost_pwu_enabled" => 0, // for global rules
		"msconf_shipping_worldwide_destination_id" => 0, // fallback option for shipping destinations
		"mm_shipping_total_status" => 1, // MM shipping total module
		"mm_shipping_total_sort_order" => 1, // MM shipping total module

		/* Fee settings */
		"msconf_signup_fee_enabled" => 1,
		"msconf_listing_fee_enabled" => 1,
		"msconf_sale_fee_seller_enabled" => 1,
		"msconf_sale_fee_catalog_enabled" => 1,
		"msconf_sale_fee_calculation_mode" => 1,
		"msconf_fee_priority" => 2,

		/* Complaints system settings */
		"msconf_complaints_enable" => 0,

		/* MultiMerch wishlists */
		"msconf_wishlist_enabled" => 1,

		/* MultiMerch product attributes */
		"msconf_msf_attribute_enabled" => 1,

		/* MultiMerch product variations */
		"msconf_msf_variation_enabled" => 1,

		/* MultiMerch seller properties */
		"msconf_msf_seller_property_enabled" => 1,

		/* Logging */
		"msconf_logging_level" => \MultiMerch\Logger\Logger::LEVEL_ERROR,
		"msconf_logging_filename" => 'ms_logging.log',

		/* Questions */
		"msconf_allow_questions" => 0,

		/* Allow switching to holiday mode */
		"msconf_holiday_mode_allow" => 0,

		/* SEO */
		'msconf_config_seo_url_enable' => 0,
		'msconf_store_slug' => 'store',
		"msconf_sellers_slug" => 'sellers',
		"msconf_products_slug" => 'products',

		/* Coupons */
		"msconf_allow_seller_coupons" => 1,
		"ms_coupon_status" => 1,
		"ms_coupon_sort_order" => 4,

		/* Google map */
		"msconf_google_api_key" => '',

		/* Allowed html tags for ckeditor */
		"msconf_rte_whitelist" => "p,strong,em,u,s,a,div,span,h1,h2,h3,h4,h5,h6,ul,ol,li,img,pre,address,iframe",

		/* Default seller landing page */
		"msconf_seller_landing_page_id_default" => 0,
		"msconf_seller_landing_page_id" => 0,

		/* Fields to be shown at `Register seller` page, seller's profile and settings forms */
		"msconf_seller_included_fields" => array(
			'slogan' => array('enabled' => 1, 'enabled_reg' => 1),
			'description' => array('enabled' => 1, 'enabled_reg' => 1),
			'website' => array('enabled' => 1, 'enabled_reg' => 1),
			'company' => array('enabled' => 1, 'enabled_reg' => 1),
			'phone' => array('enabled' => 1, 'enabled_reg' => 1),
			'logo' => array('enabled' => 1, 'enabled_reg' => 1),
			'banner' => array('enabled' => 1, 'enabled_reg' => 1),
			'fullname' => array('enabled' => 1, 'enabled_reg' => 1),
			'address_1' => array('enabled' => 1, 'enabled_reg' => 1),
			'address_2' => array('enabled' => 1, 'enabled_reg' => 1),
			'city' => array('enabled' => 1, 'enabled_reg' => 1),
			'state' => array('enabled' => 1, 'enabled_reg' => 1),
			'zip' => array('enabled' => 1, 'enabled_reg' => 1)
		),

		/* Notifications */
		"msconf_notification_events" => [
			// Admin events
			'admin.created.message'					=> ['mail', 'onsite'],
			'admin.created.payout'					=> ['mail', 'onsite'],
			'admin.updated.account.status'			=> ['mail', 'onsite'],
			'admin.updated.order.status'			=> ['mail', 'onsite'],
			'admin.updated.suborder.status'			=> ['mail', 'onsite'],

			// Customer events
			'customer.created.account'				=> ['mail', 'onsite'],
			'customer.created.order'				=> ['mail', 'onsite'],
			'customer.created.ms_review'			=> ['mail', 'onsite'],
			'customer.created.message'				=> ['mail', 'onsite'],

			// Guest events
			'guest.created.order'					=> ['mail', 'onsite'],

			// Seller events
			'seller.created.account'				=> ['mail', 'onsite'],
			'seller.created.message'				=> ['mail', 'onsite'],
			'seller.created.product'				=> ['mail', 'onsite'],
			'seller.updated.invoice.status'			=> ['mail', 'onsite'],

			// System events
			'system.created.invoice'				=> ['mail', 'onsite'],
			'system.finished.import'				=> ['mail', 'onsite'],
		],

		"msconf_notification_count_unack_per_user" => ['admin.0' => 0],

		// deprecated
		"msconf_ms_custom_field_enabled" => 0,

		"msconf_enable_quantities" => 1, // 0 - no, 1 - yes, 2 - shipping dependent
		"msconf_enable_rte" => 1,
		"msconf_minimum_withdrawal_amount" => "50",
		"msconf_withdrawal_waiting_period" => 0,

		"msconf_notification_email" => "",
		"msconf_allow_inactive_seller_products" => 0,
		"msconf_disable_product_after_quantity_depleted" => 0,
		"msconf_graphical_sellermenu" => 1,
		"msconf_enable_seller_banner" => 1,

		"msconf_allow_specials" => 1,
		"msconf_allow_discounts" => 1,

		"msconf_attribute_display" => 1, // 0 - MM, 1 - OC, 2 - both

		"msconf_provide_buyerinfo" => 0, // 0 - no, 1 - yes, 2 - shipping dependent

		"msconf_allow_partial_withdrawal" => 1,

		"msconf_enable_update_seo_urls" => 0,
		"msconf_hide_customer_email" => 1,

		"msconf_enable_private_messaging" => 2, // 0 - no, 2 - yes (email only)

		"msconf_change_group" => 0,

		/* Favorite sellers */
		"msconf_allow_favorite_sellers" => 1,

		/* Deprecated for MM 8.7.3 release */
		"msconf_seller_terms_page" => "",
		'mxtconf_ga_seller_enable' => 0,
		"msconf_change_seller_nickname" => 1,
		"msconf_minimum_product_price" => 0,
		"msconf_maximum_product_price" => 0,
		"msconf_allowed_image_types" => 'png,jpg,jpeg',
		"msconf_allowed_download_types" => 'zip,rar,pdf',
		"msconf_msg_allowed_file_types" => 'png,jpg,jpeg,zip,rar,pdf,csv',
		"msconf_seller_avatar_seller_profile_image_width" => 100,
		"msconf_seller_avatar_seller_profile_image_height" => 100,
		"msconf_seller_avatar_seller_list_image_width" => 228,
		"msconf_seller_avatar_seller_list_image_height" => 228,
		"msconf_seller_avatar_product_page_image_width" => 100,
		"msconf_seller_avatar_product_page_image_height" => 100,
		"msconf_seller_avatar_dashboard_image_width" => 100,
		"msconf_seller_avatar_dashboard_image_height" => 100,
		"msconf_preview_seller_avatar_image_width" => 100,
		"msconf_preview_seller_avatar_image_height" => 100,
		"msconf_preview_product_image_width" => 100,
		"msconf_preview_product_image_height" => 100,
		"msconf_product_seller_profile_image_width" => 100,
		"msconf_product_seller_profile_image_height" => 100,
		"msconf_product_seller_products_image_width" => 100,
		"msconf_product_seller_products_image_height" => 100,
		"msconf_product_seller_product_list_seller_area_image_width" => 40,
		"msconf_product_seller_product_list_seller_area_image_height" => 40,
		"msconf_product_seller_banner_width" => 1140,
		"msconf_product_seller_banner_height" => 152,
		"msconf_min_uploaded_image_width" => 0,
		"msconf_min_uploaded_image_height" => 0,
		"msconf_max_uploaded_image_width" => 0,
		"msconf_max_uploaded_image_height" => 0,
		"msconf_max_description_image_width" => 750,
		"msconf_max_description_image_height" => 450,
	];

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->registry = $registry;

		$this->load->model("multimerch/install");
		$this->load->model('setting/setting');
		$this->load->model('extension/extension');
	}

	public function install()
	{
		/** @see \ModelMultimerchInstall::createSchema */
		$this->model_multimerch_install->createSchema();

		/** @see \ModelMultimerchInstall::createData */
		$this->model_multimerch_install->createData();
		$this->model_setting_setting->editSetting('mxtconf', $this->settings);
		$this->model_setting_setting->editSetting('msconf', $this->settings);
		$this->model_setting_setting->editSetting('mmess_conf', $this->settings);
		$this->model_setting_setting->editSetting('mm_shipping_total', $this->settings);
		$this->model_setting_setting->editSetting('ms_coupon', $this->settings);
		$this->model_multimerch_install->createAdditionalSettings();

		$this->load->model('user/user_group');

		$admin_group_id = 1;
		
		foreach ($this->_controllers as $c) {
			$this->model_user_user_group->addPermission($admin_group_id, 'access', $c);
			$this->model_user_user_group->addPermission($admin_group_id, 'modify', $c);
		}

		$dirs = array(
			DIR_IMAGE . $this->settings['msconf_product_image_path'],
			DIR_IMAGE . $this->settings['msconf_temp_image_path'],
			DIR_DOWNLOAD . $this->settings['msconf_temp_download_path']
		);

		$this->session->data['success'] = $this->language->get('ms_success_installed');
		$this->session->data['error'] = "";

		foreach ($dirs as $dir) {
			if (!file_exists($dir)) {
				if (!mkdir($dir, 0755)) {
					$this->session->data['error'] .= sprintf($this->language->get('ms_error_directory'), $dir);
				}
			} else {
				if (!is_writable($dir)) {
					$this->session->data['error'] .= sprintf($this->language->get('ms_error_directory_notwritable'), $dir);
				} else {
					$this->session->data['error'] .= sprintf($this->language->get('ms_error_directory_exists'), $dir);
				}
			}
		}

		$this->cache->set('multimerch_module_is_installed', true);
	}

	public function install_demodata($data)
	{
		$db = new DB($data['db_driver'], htmlspecialchars_decode($data['db_hostname']), htmlspecialchars_decode($data['db_username']), htmlspecialchars_decode($data['db_password']), htmlspecialchars_decode($data['db_database']), $data['db_port']);

		$sql_dumps = array_diff(scandir(DIR_APPLICATION . '../source/multimerch/standalone/demodata'), ['.', '..']);

		foreach ($sql_dumps as $sql_dump) {
			$lines = file(DIR_APPLICATION . '../source/multimerch/standalone/demodata/' . $sql_dump);

			if ($lines) {
				$sql = '';

				foreach($lines as $line) {
					if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
						$sql .= $line;

						if (preg_match('/;\s*$/', $line)) {
							$sql = str_replace("TRUNCATE TABLE `oc_", "TRUNCATE TABLE `" . $data['db_prefix'], $sql);
							$sql = str_replace("ALTER TABLE `oc_", "ALTER TABLE `" . $data['db_prefix'], $sql);
							$sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . $data['db_prefix'], $sql);

							$db->query($sql);

							$sql = '';
						}
					}
				}
			}
		}
	}
}
