<div id="ms-seller-landing">
	<div class="ms-section title">
		<h1><strong>Sell in our marketplace</strong></h1>
		<p class="ms-subtitle">Curabitur sed justo lacinia, dapibus neque posuere, bibendum erat. Vestibulum sagittis ullamcorper molestie.</p>
	</div>

	<div class="ms-section general">
		<table>
			<tbody>
				<tr>
					<td class="text">
						<h2>Create a seller account</h2>
						<p>Curabitur vitae nulla ac magna posuere egestas. Curabitur ac posuere nulla, sit amet mattis odio. Ut sit amet orci ligula. Integer aliquam turpis et ligula fringilla, convallis ullamcorper nibh dignissim. Duis eget pharetra nisl, et venenatis lectus. Nullam sit amet eleifend eros.</p>
						<p>Curabitur ullamcorper, odio nec facilisis dapibus, purus risus rutrum ante, nec accumsan nibh ex eget quam.</p>
						<p>Sed fringilla turpis arcu, quis posuere neque pretium a. Vestibulum rutrum metus quis nunc imperdiet facilisis</p>
						<p style="text-align: center; margin-top: 50px;"><a href="<?php echo $server_url . 'index.php?route=account/register-seller'; ?>" class="btn btn-primary">Create a seller account</a></p>
					</td>
					<td class="image">
						<img src="https://multimerch.com/wp-content/uploads/2013/04/multimerch-multi-vendor-marketplace-software-845x554.png" style="width: 640px;">
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="ms-section general">
		<table>
			<tbody>
				<tr>
					<td class="image">
						<img src="https://multimerch.com/wp-content/uploads/2013/04/multimerch-marketplace-features-845x684.png" style="width: 640px;">
					</td>
					<td class="text">
						<h2>Create a seller account</h2>
						<p>Curabitur vitae nulla ac magna posuere egestas. Curabitur ac posuere nulla, sit amet mattis odio. Ut sit amet orci ligula. Integer aliquam turpis et ligula fringilla, convallis ullamcorper nibh dignissim. Duis eget pharetra nisl, et venenatis lectus. Nullam sit amet eleifend eros.</p>
						<p>Curabitur ullamcorper, odio nec facilisis dapibus, purus risus rutrum ante, nec accumsan nibh ex eget quam.</p>
						<p>Sed fringilla turpis arcu, quis posuere neque pretium a. Vestibulum rutrum metus quis nunc imperdiet facilisis</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="ms-section features">
		<h2><strong>Marketplace features</strong></h2>
		<p class="ms-subtitle">Curabitur sed justo lacinia, dapibus neque posuere, bibendum erat. Vestibulum sagittis ullamcorper molestie.</p>

		<table class="ms-table">
			<tbody>
				<tr>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-enjoy.png" alt=""></div>
						<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed hendrerit dolor, ac vestibulum augue. Vestibulum a convallis sapien, quis pharetra diam. In in vehicula ipsum, ac scelerisque neque. Maecenas arcu ante, condimentum at velit vitae, vulputate consectetur odio. Curabitur mattis tincidunt massa, eget tincidunt quam pulvinar sit amet.</div>
					</td>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-people.png" alt=""></div>
						<div class="text">Nunc sed hendrerit dolor, ac vestibulum augue. Vestibulum a convallis sapien, quis pharetra diam. In in vehicula ipsum, ac scelerisque neque. Maecenas arcu ante, condimentum at velit vitae, vulputate consectetur odio.</div>
					</td>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-innovation.png" alt=""></div>
						<div class="text">Vestibulum a convallis sapien, quis pharetra diam. In in vehicula ipsum, ac scelerisque neque. Maecenas arcu ante, condimentum at velit vitae, vulputate consectetur odio. Curabitur mattis tincidunt massa, eget tincidunt quam pulvinar sit amet.</div>
					</td>
				</tr>
				<tr>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-honesty.png" alt=""></div>
						<div class="text">In in vehicula ipsum, ac scelerisque neque. Maecenas arcu ante, condimentum at velit vitae, vulputate consectetur odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed hendrerit dolor, ac vestibulum augue.</div>
					</td>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-education.png" alt=""></div>
						<div class="text">In in vehicula ipsum, ac scelerisque neque. Maecenas arcu ante, condimentum at velit vitae, vulputate consectetur odio. Curabitur mattis tincidunt massa, eget tincidunt quam pulvinar sit amet. Nunc sed hendrerit dolor, ac vestibulum augue. Vestibulum a convallis sapien, quis pharetra diam.</div>
					</td>
					<td class="feature">
						<div class="image"><img src="https://multimerch.com/wp-content/uploads/2016/07/multimerch-values-processes.png" alt=""></div>
						<div class="text">Pellentesque tincidunt consectetur lorem sed sodales. Nam vel arcu ut orci egestas placerat. Curabitur vulputate enim semper nisl eleifend, non placerat eros vestibulum. Ut ornare ornare lorem in tempus.</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="ms-section faq">
		<h2><strong>Frequently asked questions</strong></h2>
		<p class="ms-subtitle">Curabitur sed justo lacinia, dapibus neque posuere, bibendum erat. Vestibulum sagittis ullamcorper molestie.</p>

		<table class="ms-table">
			<tbody>
				<tr>
					<td class="question">
						<h3>How do I pay for MultiMerch Marketplace?</h3>
						<span class="answer">Maecenas luctus dui tortor, sed pretium ipsum rhoncus sed. Phasellus ac enim vel nisl ornare porttitor id a sem. Morbi vehicula velit sit amet ullamcorper feugiat.</span>
					</td>
					<td class="question">
						<h3>Do you offer a free trial?</h3>
						<span class="answer">Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel suscipit velit. Proin laoreet vulputate pellentesque. Praesent in mattis libero.</span>
					</td>
				</tr>
				<tr>
					<td class="question">
						<h3>Will you help me get started with MultiMerch?</h3>
						<span class="answer">Etiam id pharetra ex. Suspendisse commodo lorem quam, in imperdiet magna hendrerit et. Pellentesque pulvinar rutrum imperdiet. Proin eget accumsan tellus. Maecenas rutrum ullamcorper eros, eu aliquet tortor condimentum nec.</span>
					</td>
					<td class="question">
						<h3>Can you build a customized marketplace for me?</h3>
						<span class="answer">Vestibulum ullamcorper mattis mauris, non lacinia nisl porta sed. Mauris venenatis orci in leo ultrices, eget imperdiet nisi eleifend. Curabitur ante tellus, bibendum at erat ut, fermentum tincidunt magna.</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="ms-section pricing">
		<h2><strong>Pricing</strong></h2>
		<p class="ms-subtitle">Curabitur sed justo lacinia, dapibus neque posuere, bibendum erat. Vestibulum sagittis ullamcorper molestie.</p>

		<table class="ms-table ms-table-bordered">
			<tbody>
				<tr>
					<td>
						<h3>Basic</h3>
						<p><strong>Free</strong></p>
					</td>
					<td>
						<h3>Premium</h3>
						<p><strong>$29.99/month</strong></p>
					</td>
					<td>
						<h3>Ultimate</h3>
						<p><strong>$69.99/month</strong></p>
					</td>
				</tr>
				<tr>
					<td>
						<p>Lorem ipsum</p>
						<p>Dolor sit amet</p>
						<p>Consectetur adipiscing</p>
					</td>
					<td>
						<p>Donec at sodales</p>
						<p>Nunc viverra</p>
						<p>Ligula eu scelerisque</p>
						<p>Feugiat mauris tellus</p>
						<p>Dictum mauris<br></p>
					</td>
					<td>
						<p>Pellentesque maximus</p>
						<p>Elit at ipsum lacinia</p>
						<p>Ornare vehicula diam</p>
						<p>Phasellus luctus nisl</p>
						<p>At luctus congue</p>
						<p>Odio dui dapibus quam</p>
						<p>Eu volutpat velit<br></p>
					</td>
				</tr>
				<tr>
					<td>
						<p><a href="<?php echo $server_url . 'index.php?route=account/register-seller&seller_group_id=1'; ?>" class="btn btn-primary">Sign up</a></p>
					</td>
					<td>
						<p><a href="<?php echo $server_url . 'index.php?route=account/register-seller&seller_group_id=2'; ?>" class="btn btn-primary">Sign up</a></p>
					</td>
					<td>
						<p><a href="<?php echo $server_url . 'index.php?route=account/register-seller&seller_group_id=3'; ?>" class="btn btn-primary">Sign up</a></p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>