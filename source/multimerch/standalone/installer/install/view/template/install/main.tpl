<?php echo $header; ?>
<div class="container" style="position: relative;">
	<div id="progress-layer" style="display: none;">
		<div class="progress">
			<div class="progress-bar progress-bar-success" style="width: 0%">
				<?php echo sprintf($text_percentage_completed, 0); ?>
			</div>
		</div>
	</div>

	<div class="text-center mms-logo">
		<img src="<?php echo $mms_logo; ?>" width="200" />
	</div>
	<h1><?php echo $welcome_title; ?></h1>
	<br>
	<h4><?php echo $welcome_subtitle; ?></h4>
	<hr /><form id="formdata" method="POST" data-toggle="validator" role="form">
		<input type="hidden" name="db_driver" value="mysqli" />
		<div class="row">
			<div class="col-sm-12">
				<div  class="bg-warning alert-danger" style="display: none;  padding: 10px 15px; margin-bottom: 10px;" id="warning"><i class="fa fa-warning fa-lg fa-fw"></i> <span></span></div>

				<div class="panel panel-default form-horizontal">
					<div class="panel-heading">
						<b><?php echo $text_server_requirements; ?></b>
					</div>
					<div class="panel-body">
						<?php if (!empty($setup_errors['server_config']) || !empty($setup_errors['php_ext']) || !empty($setup_errors['chmod'])) { ?>
							<?php if (!empty($setup_errors['server_config'])) { ?>
								<div class="alert alert-danger"><i class="fa fa-times fa-fw"></i><strong><?php echo $error_server_config_title; ?></strong>
									<ul>
										<?php foreach ($setup_errors['server_config'] as $error) { ?>
											<li><?php echo $error; ?></li>
										<?php } ?>
									</ul>
								</div>
							<?php } ?>
							<?php if (!empty($setup_errors['php_ext'])) { ?>
								<div class="alert alert-danger"><i class="fa fa-times fa-fw"></i><strong><?php echo $error_php_ext_title; ?></strong>
									<ul>
										<?php foreach ($setup_errors['php_ext'] as $error) { ?>
											<li><?php echo $error; ?></li>
										<?php } ?>
									</ul>
								</div>
							<?php } ?>
							<?php if (!empty($setup_errors['chmod'])) { ?>
								<div class="alert alert-danger"><i class="fa fa-times fa-fw"></i><strong><?php echo $error_chmod_title; ?></strong>
									<ul>
										<?php foreach ($setup_errors['chmod'] as $error) { ?>
											<li><?php echo $error; ?></li>
										<?php } ?>
									</ul>
								</div>
							<?php } ?>
						<?php } else { ?>
							<div class="alert alert-success"><i class="fa fa-check fa-fw"></i> <?php echo $text_server_config_ok; ?></div>
							<div class="alert alert-success"><i class="fa fa-check fa-fw"></i> <?php echo $text_chmod_ok; ?></div>
						<?php } ?>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="panel panel-default form-horizontal">
					<div class="panel-heading">
						<b><?php echo $text_database_settings; ?></b>
					</div>
					<div class="panel-body">
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-db-hostname"><?php echo $entry_db_hostname; ?></label>
							<div class="col-sm-10">
								<input type="text" name="db_hostname" value="<?php echo $db_hostname; ?>" id="input-db-hostname" class="form-control" required />
								<?php if (!empty($error['db_hostname'])) { ?>
								<div class="text-danger"><?php echo $error['db_hostname']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-db-database"><?php echo $entry_db_database; ?></label>
							<div class="col-sm-10">
								<input type="text" name="db_database" value="<?php echo $db_database; ?>" id="input-db-database" class="form-control" required/>
								<?php if (!empty($error['db_database'])) { ?>
								<div class="text-danger"><?php echo $error['db_database']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-db-username"><?php echo $entry_db_username; ?></label>
							<div class="col-sm-10">
								<input type="text" name="db_username" value="<?php echo $db_username; ?>" id="input-db-username" class="form-control" required/>
								<?php if (!empty($error['db_username'])) { ?>
								<div class="text-danger"><?php echo $error['db_username']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-db-password"><?php echo $entry_db_password; ?></label>
							<div class="col-sm-10">
								<input type="password" name="db_password" value="<?php echo $db_password; ?>" id="input-db-password" class="form-control"/>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-db-port"><?php echo $entry_db_port; ?></label>
							<div class="col-sm-10">
								<input type="text" name="db_port" value="<?php echo $db_port; ?>" id="input-db-port" class="form-control" required/>
								<?php if (!empty($error['db_port'])) { ?>
								<div class="text-danger"><?php echo $error['db_port']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-db-prefix"><?php echo $entry_db_prefix; ?></label>
							<div class="col-sm-10">
								<input type="text" name="db_prefix" value="<?php echo $db_prefix; ?>" id="input-db-prefix" class="form-control" />
								<?php if (!empty($error['db_prefix'])) { ?>
								<div class="text-danger"><?php echo $error['db_prefix']; ?></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="panel panel-default form-horizontal">
					<div class="panel-heading">
						<b><?php echo $text_admin_credentials; ?></b>
					</div>
					<div class="panel-body">
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-username"><?php echo $entry_username; ?></label>
							<div class="col-sm-10">
								<input type="text" name="username" value="<?php echo $username; ?>" id="input-username" class="form-control" required />
								<?php if (!empty($error['username'])) { ?>
								<div class="text-danger"><?php echo $error['username']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
							<div class="col-sm-10">
								<input type="text" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" required />
								<?php if (!empty($error['password'])) { ?>
								<div class="text-danger"><?php echo $error['password']; ?></div>
								<?php } ?>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
							<div class="col-sm-10">
								<input type="text" type="email" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" required />
								<?php if (!empty($error['email'])) { ?>
								<div class="text-danger"><?php echo $error['email']; ?></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="panel panel-default form-horizontal">
					<div class="panel-heading">
						<b><?php echo $text_default_config; ?></b>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="demo-data"><?php echo $text_language; ?></label>
							<div class="col-sm-10">
								<select name="language" class="form-control" disabled>
									<option value="1"><?php echo $text_language_eng; ?></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="demo-data"><?php echo $text_theme; ?></label>
							<div class="col-sm-10">
								<select name="theme" class="form-control" disabled>
									<option value="default"><?php echo $text_theme_default; ?></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="demo-data"><?php echo $text_install_demo_data; ?></label>
							<div class="col-sm-10">
								<select id="demo-data" name="install_demodata" class="form-control" disabled>
									<option value="1"><?php echo $text_yes; ?></option>
									<option value="1"><?php echo $text_no; ?></option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<div class="text-center col-sm-6 col-sm-offset-3" style="margin-top: 25px;">
		<?php if (empty($setup_errors['server_config']) && empty($setup_errors['php_ext']) && empty($setup_errors['chmod'])) { ?>
			<button type="button" class="btn btn-primary btn-block btn-lg" id="btn-install"> <span style="display: none;" id="text-installing"><i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i> <?php echo $button_installing; ?></span><span id="text-install"><?php echo $button_install; ?></span></button>
		<?php } else { ?>
			<button type="button" class="btn btn-primary btn-block btn-lg disabled"><span><?php echo $button_install; ?></span></button>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
</div>
<script type="text/javascript">
$(function () {
	var $xhr = false;

	$('.text-danger').remove();

	$('#formdata').validator({
		'focus': false
	});

	$('#btn-install').click(function () {
		$(this).attr('disabled', true);
		start_install('');
	});

	function start_install(init_step) {
		set_progress_bar(0);
		install_step(init_step);
	}
	function install_step(next_step) {
		$('#warning').hide();
		$('.text-danger').remove();
		show_processing();
		if ($xhr) {
			$xhr.abort();
		}

		rtype = 'POST';
		formdata = $('#formdata').serialize();

		if (next_step) {
			next_step = '&step=' + next_step;
		} else {
			// Seems like first step - at the start we need to send user data (mysql creds, passwords) and test it
			next_step = '';

		}
		$xhr = $.ajax({
			url: '<?php echo $install_action_url; ?>' + next_step,
			type: rtype,
			data: formdata,
			success: function (data) {

				$('#log').append('<div>' + data.message + '</div>');
				set_progress_bar(data.current_progress);
				if (data.next_step != 'finished' && parseInt(data.next_step) > 0) {
					install_step(data.next_step);

				} else {
					hide_processing();
					if (data.next_step == 'finished') {
						if (data.redirect) {
							window.location = data.redirect.replace(/&amp;/g, '&');
						}
					} else {
						if (data.error) {

							for (var prop in data.error) {
								if ($("input[name=" + prop + "]").length > 0) {
									$("input[name=" + prop + "]").parent().append('<div class="text-danger">' + data.error[prop] + '</div>');
									$("input[name=" + prop + "]").closest('.form-group').addClass('has-error');
								} else {
									$('#warning span').html(data.error.warning);
									$('#warning').show();
								}

								window.scrollTo(0, 0);
							}

						}
						//console.log('WHAT THE HELL ARE YOU? ' + data);
					}

				}
			},
			error: function (data) {
				console.log('error ' + data);
			}
		})
	}

	function set_progress_bar(progress) {
		$('.panel-log').show();
		$('.progress-bar').css('width', progress + '%');
		$('#current-progress').html(progress);
	}
	function show_processing() {
		$('#text-installing').show();
		$('#progress-layer').show();
		$('#text-install').hide();
		$('#btn-install').prop('disabled', true);
	}
	function hide_processing() {
		$('#text-installing').hide();
		$('#progress-layer').hide();
		$('#text-install').show();
		$('#btn-install').prop('disabled', false);
	}
})


</script>
<style>
	#progress-layer{
		position: absolute;
		left: 0;
		top: 0;
		bottom: 0;
		right: 0;
		display: flex;
		justify-content: center;
		align-items: center;
		z-index: 10;
		background: rgba(251,251,251,0.8);
	}
	.progress{
		width: 90%;
	}
	#text-install {
		text-transform: none;
	}
	.mms-logo {
		margin-top: 20px;
	}
</style>
<?php echo $footer; ?>