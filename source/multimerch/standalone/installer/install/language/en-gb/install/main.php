<?php
// Heading
$_['heading_title'] = "MultiMerch Marketplace setup";
$_['welcome_title'] = "Welcome to MultiMerch Marketplace setup (v. 8.27 alpha)";
$_['welcome_subtitle'] = "Thank you for choosing MultiMerch for your marketplace platform!<br><br>The initial setup won't take more than a minute. You will configure your new marketplace afterwards.";

// Texts
$_['text_percentage_completed'] = "<span id='current-progress'>%s</span>%% completed";
$_['text_server_requirements'] = "Server requirements";
$_['text_database_settings'] = "Database settings";
$_['text_admin_credentials'] = "Admin credentials";
$_['text_default_config'] = "Default marketplace configuration";
$_['text_language'] = "Language";
$_['text_language_eng'] = "English";
$_['text_theme'] = "Theme";
$_['text_theme_default'] = "default";
$_['text_install_demo_data'] = "Demo data";
$_['text_install_demo_data_note'] = "Install demo-data note";
$_['text_yes'] = "Yes";
$_['text_no'] = "No";
$_['text_server_config_ok'] = "Your server configuration is suitable for MultiMerch";
$_['text_chmod_ok'] = "Your folder permissions are correct";

// Buttons
$_['button_install'] = "Install MultiMerch Marketplace";
$_['button_installing'] = "Installing...";

// Entry
$_['entry_db_hostname']      = "Hostname";
$_['entry_db_username']      = "Username";
$_['entry_db_password']      = "Password";
$_['entry_db_database']      = "Database";
$_['entry_db_port']          = "Port";
$_['entry_db_prefix']        = "Prefix";
$_['entry_username']         = "Admin username";
$_['entry_password']         = "Password";
$_['entry_email']            = "E-Mail";

// Error
$_['error_db_hostname'] 	 = "Hostname required!";
$_['error_db_username'] 	 = "Username required!";
$_['error_db_database']		 = "Database Name required!";
$_['error_db_port']		     = "Database Port required!";
$_['error_db_prefix'] 		 = "DB Prefix can only contain lowercase characters in the a-z range, 0-9 and underscores";
$_['error_db_connect'] 		 = "Error: Could not connect to the database please make sure the database server, username and password is correct!";
$_['error_username'] 		 = "Username required!";
$_['error_password'] 		 = "Password required!";
$_['error_email'] 			 = "Invalid E-Mail!";
$_['error_config'] 			 = "Error: Could not write to config.php please check you have set the correct permissions on: ";

$_['error_server_config_title'] = "There are issues with your server configuration that prevent MultiMerch from being installed:";
$_['error_version']          = "You need support for PHP 7.x to run MultiMerch";
$_['error_session']          = "MultiMerch will not work with session.auto_start enabled!";
$_['error_mysql_version']	 = "You need MySQL server 5.6.5 or higher to run MultiMerch";

$_['error_php_ext_title']	 = "The following PHP extensions need to be enabled:";
$_['error_chmod_title']		 = "The following directories need to be server-writable (chmod 775):";
