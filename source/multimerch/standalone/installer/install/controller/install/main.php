<?php
class ControllerInstallMain extends Controller {
	private $data = [
		'db_hostname' => 'localhost',
		'db_username' => 'root',
		'db_database' => '',
		'db_password' => '',
		'db_port' => 3306,
		'db_prefix' => 'mms_',
		'username' => 'admin',
		'password' => '',
		'email' => '',
		'setup_errors' => [
			'server_config' => [],
			'php_ext' => [],
			'chmod' => []
		]
	];

	private $json_response = [
		'current_progress' => 0,
		'message' => '',
		'error' => false
	];

	public function __construct($registry)
	{
		parent::__construct($registry);

		if (empty($this->session->data['install_stamp'])) {
			$this->data['install_stamp'] = substr(md5(time()), 0, 5);
			$this->session->data['install_stamp'] = $this->data['install_stamp'];
			$this->log('Installation started');
		} else {
			$this->data['install_stamp'] = $this->session->data['install_stamp'];
		}

		$this->data['steps'] = [
			'check_inputs',
			'install_opencart',
			'install_vqmod',
			'install_basel',
			'unzip_multimerch',
			'install_multimerch',
			'install_mm_basel_integration',
			'install_standalone',
			'install_demodata'
		];

		$this->load->model('extension/modification');

		$this->data = array_merge($this->data, $this->load->language('install/main'));

		$this->data['mysqli'] = extension_loaded('mysqli');

		$this->validate_requirements();

		$this->data['next_step'] = !empty($this->session->data['next_step']) ? $this->session->data['next_step'] : false;
		$this->data['total_steps'] = count($this->data['steps']);
		$this->data['install_action_url'] = $this->url->link('install/main/process_step');
	}

	public function index()
	{
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['mms_logo'] = 'image/mm_logo.png';

		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');

		$this->log('index loaded');

		$this->response->setOutput($this->load->view('install/main', $this->data));
	}

	// Main step-processing function
	public function process_step()
	{
		if (!$this->is_ajax_request()) {
			exit('AJAX request required');
		}

		if (!$this->validate_inputs()) {
			$this->json_response['message'] = 'Fields validation failed';
			$this->send_json($this->json_response);
			return;
		}

		$step = !empty($this->request->get['step']) ? $this->request->get['step'] : 0;

		if (!isset($this->data['steps'][$step])) {
			$this->json_response['message'] = 'Step ' . $step . ' does not exist';

			$this->log('Step ' . $step . ' not found!');

			$this->send_json($this->json_response);
			return;
		}

		$run_step = '_step_' . $this->data['steps'][$step];
		$this->log('Starting step ' . $run_step);

		$step_result = $this->$run_step();

		// If next step exist - installation not finished - remember next step and sending it to frontend
		if ($step_result === true) {
			$this->log('Step ' . $run_step . ' finished!');
			if (isset($this->data['steps'][$step + 1])) {
				$next_step = $step + 1;

				$this->log('Moving to step ' . $this->data['steps'][$next_step] . '...');

				$this->session->data['next_step'] = $next_step;
				$this->json_response['next_step'] = $next_step;
				$this->json_response['current_progress'] = intval(100 / $this->data['total_steps'] * $this->session->data['next_step']);
			} else {
				file_put_contents(DIR_OPENCART . 'config.php', "define('MMS_INSTALLED', 'TRUE');" . PHP_EOL, FILE_APPEND | LOCK_EX);
				file_put_contents(DIR_OPENCART . 'admin/config.php', "define('MMS_INSTALLED', 'TRUE');" . PHP_EOL, FILE_APPEND | LOCK_EX);

				unset($this->session->data['install_stamp']);
				unset($this->session->data['next_step']);

				$this->session->data['do_after_install'] = TRUE;

				$this->log('Installation finished!');

				$this->json_response['next_step'] = 'finished';
				$this->json_response['current_progress'] = 100;

				// Login as admin and redirect to panel
				if (isset($this->request->post['username']) && isset($this->request->post['password'])) {
					$this->registry->set('user', new Cart\User($this->registry));

					if ($this->user->login($this->request->post['username'], html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8'))) {
						$this->session->data['token'] = token(32);

						$this->json_response['redirect'] = str_replace(['/install/', '&amp;'], ['/admin/', '&'], $this->url->link('multimerch/dashboard', 'token=' . $this->session->data['token'], true));
					}
				}

				// Remove install folder
				if (is_dir(DIR_OPENCART . 'install')) {
					$this->rmdir_recursive(DIR_OPENCART . 'install');
				}
			}
		}

		$this->send_json($this->json_response);
	}

	private function _step_install_opencart()
	{
		$this->load->model('install/opencart');

		$this->model_install_opencart->database($this->request->post);

		$this->log('Opencart DB installed OK');

		$output = '<?php' . "\n";
		$output .= '// HTTP' . "\n";
		$output .= 'define(\'HTTP_SERVER\', \'' . HTTP_OPENCART . '\');' . "\n\n";

		$output .= '// HTTPS' . "\n";
		$output .= 'define(\'HTTPS_SERVER\', \'' . HTTP_OPENCART . '\');' . "\n\n";

		$output .= '// DIR' . "\n";
		$output .= 'define(\'DIR_APPLICATION\', \'' . DIR_OPENCART . 'catalog/\');' . "\n";
		$output .= 'define(\'DIR_SYSTEM\', \'' . DIR_OPENCART . 'system/\');' . "\n";
		$output .= 'define(\'DIR_IMAGE\', \'' . DIR_OPENCART . 'image/\');' . "\n";
		$output .= 'define(\'DIR_LANGUAGE\', \'' . DIR_OPENCART . 'catalog/language/\');' . "\n";
		$output .= 'define(\'DIR_TEMPLATE\', \'' . DIR_OPENCART . 'catalog/view/theme/\');' . "\n";
		$output .= 'define(\'DIR_CONFIG\', \'' . DIR_OPENCART . 'system/config/\');' . "\n";
		$output .= 'define(\'DIR_CACHE\', \'' . DIR_OPENCART . 'system/storage/cache/\');' . "\n";
		$output .= 'define(\'DIR_DOWNLOAD\', \'' . DIR_OPENCART . 'system/storage/download/\');' . "\n";
		$output .= 'define(\'DIR_LOGS\', \'' . DIR_OPENCART . 'system/storage/logs/\');' . "\n";
		$output .= 'define(\'DIR_MODIFICATION\', \'' . DIR_OPENCART . 'system/storage/modification/\');' . "\n";
		$output .= 'define(\'DIR_UPLOAD\', \'' . DIR_OPENCART . 'system/storage/upload/\');' . "\n\n";

		$output .= '// DB' . "\n";
		$output .= 'define(\'DB_DRIVER\', \'mysqli\');' . "\n";
		$output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($this->request->post['db_hostname']) . '\');' . "\n";
		$output .= 'define(\'DB_USERNAME\', \'' . addslashes($this->request->post['db_username']) . '\');' . "\n";
		$output .= 'define(\'DB_PASSWORD\', \'' . addslashes(html_entity_decode($this->request->post['db_password'], ENT_QUOTES, 'UTF-8')) . '\');' . "\n";
		$output .= 'define(\'DB_DATABASE\', \'' . addslashes($this->request->post['db_database']) . '\');' . "\n";
		$output .= 'define(\'DB_PORT\', \'' . addslashes($this->request->post['db_port']) . '\');' . "\n";
		$output .= 'define(\'DB_PREFIX\', \'' . addslashes($this->request->post['db_prefix']) . '\');' . "\n";

		$file = fopen(DIR_OPENCART . 'config.php', 'w');

		fwrite($file, $output);

		fclose($file);

		$this->log('Front config written OK');

		$output = '<?php' . "\n";
		$output .= '// HTTP' . "\n";
		$output .= 'define(\'HTTP_SERVER\', \'' . HTTP_OPENCART . 'admin/\');' . "\n";
		$output .= 'define(\'HTTP_CATALOG\', \'' . HTTP_OPENCART . '\');' . "\n\n";

		$output .= '// HTTPS' . "\n";
		$output .= 'define(\'HTTPS_SERVER\', \'' . HTTP_OPENCART . 'admin/\');' . "\n";
		$output .= 'define(\'HTTPS_CATALOG\', \'' . HTTP_OPENCART . '\');' . "\n\n";

		$output .= '// DIR' . "\n";
		$output .= 'define(\'DIR_APPLICATION\', \'' . DIR_OPENCART . 'admin/\');' . "\n";
		$output .= 'define(\'DIR_SYSTEM\', \'' . DIR_OPENCART . 'system/\');' . "\n";
		$output .= 'define(\'DIR_IMAGE\', \'' . DIR_OPENCART . 'image/\');' . "\n";
		$output .= 'define(\'DIR_LANGUAGE\', \'' . DIR_OPENCART . 'admin/language/\');' . "\n";
		$output .= 'define(\'DIR_TEMPLATE\', \'' . DIR_OPENCART . 'admin/view/template/\');' . "\n";
		$output .= 'define(\'DIR_CONFIG\', \'' . DIR_OPENCART . 'system/config/\');' . "\n";
		$output .= 'define(\'DIR_CACHE\', \'' . DIR_OPENCART . 'system/storage/cache/\');' . "\n";
		$output .= 'define(\'DIR_DOWNLOAD\', \'' . DIR_OPENCART . 'system/storage/download/\');' . "\n";
		$output .= 'define(\'DIR_LOGS\', \'' . DIR_OPENCART . 'system/storage/logs/\');' . "\n";
		$output .= 'define(\'DIR_MODIFICATION\', \'' . DIR_OPENCART . 'system/storage/modification/\');' . "\n";
		$output .= 'define(\'DIR_UPLOAD\', \'' . DIR_OPENCART . 'system/storage/upload/\');' . "\n";
		$output .= 'define(\'DIR_CATALOG\', \'' . DIR_OPENCART . 'catalog/\');' . "\n\n";

		$output .= '// DB' . "\n";
		$output .= 'define(\'DB_DRIVER\', \'' . addslashes($this->request->post['db_driver']) . '\');' . "\n";
		$output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($this->request->post['db_hostname']) . '\');' . "\n";
		$output .= 'define(\'DB_USERNAME\', \'' . addslashes($this->request->post['db_username']) . '\');' . "\n";
		$output .= 'define(\'DB_PASSWORD\', \'' . addslashes(html_entity_decode($this->request->post['db_password'], ENT_QUOTES, 'UTF-8')) . '\');' . "\n";
		$output .= 'define(\'DB_DATABASE\', \'' . addslashes($this->request->post['db_database']) . '\');' . "\n";
		$output .= 'define(\'DB_PORT\', \'' . addslashes($this->request->post['db_port']) . '\');' . "\n";
		$output .= 'define(\'DB_PREFIX\', \'' . addslashes($this->request->post['db_prefix']) . '\');' . "\n";

		$file = fopen(DIR_OPENCART . 'admin/config.php', 'w');

		fwrite($file, $output);

		fclose($file);

		$this->log('Admin config written OK');

		$this->json_response['message'] = 'OC installed';

		return true;
	}

	private function _step_install_vqmod()
	{
		$this->load->model('install/vqmod');
		$result = $this->model_install_vqmod->install();

		if (isset($result['success']) && isset($result['message'])) {
			$this->log($result['message']);
			$this->json_response['message'] = $result['message'];
		}

		return true;
	}

	private function _step_install_basel()
	{
		// Copy Basel files to site root
		$this->copyFiles(DIR_OPENCART . 'source/basel/core', DIR_OPENCART);

		// ##################################
		// Add Basel Modification
		// ##################################

		$xml = file_get_contents(DIR_OPENCART . 'source/basel/basel_theme.ocmod.xml');

		$data = [
			'name' => 'Basel Theme',
			'code' => 'basel_theme',
			'id' => 'basel_theme',
			'extension_install_id' => 'basel_theme',
			'author' => 'Openthemer.com',
			'version' => 'v. 1.2.8.0',
			'xml' => $xml,
			'link' => '',
			'status' => '1',
		];

		$this->model_extension_modification->addModification($data);

		$this->log('Basel modification added OK');

		// ##################################
		// Set User Permissions
		// ##################################

		$this->load->model('user/user_group');
		$user_id = 1;
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/basel');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/basel');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/product_tabs');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/product_tabs');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/productgroups');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/productgroups');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/question');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/question');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/subscriber');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/subscriber');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/basel/testimonial');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/basel/testimonial');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/blog/blog');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/blog/blog');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/blog/blog_category');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/blog/blog_category');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/blog/blog_comment');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/blog/blog_comment');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/blog/blog_setting');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/blog/blog_setting');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_carousel');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_carousel');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_categories');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_categories');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_content');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_content');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_instagram');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_instagram');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_layerslider');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_layerslider');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_megamenu');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_megamenu');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/basel_products');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/basel_products');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/blog_latest');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/blog_latest');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/blog_category');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/blog_category');
		$this->model_user_user_group->addPermission($user_id, 'access', 'extension/module/quickcheckout');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'extension/module/quickcheckout');

		$this->log('Basel permissions added OK');

		// ##################################
		// Default Blog Settings
		// ##################################
		$this->load->model('setting/setting');
		$blog_data = [
			'blogsetting_post_date_added' => '1',
			'blogsetting_post_comments_count' => '1',
			'blogsetting_post_page_view' => '1',
			'blogsetting_post_author' => '1',
			'blogsetting_share' => '1',
			'blogsetting_post_thumb' => '1',
			'blogsetting_date_added' => '1',
			'blogsetting_comments_count' => '1',
			'blogsetting_page_view' => '1',
			'blogsetting_author' => '1',
			'blogsetting_rel_thumb' => '0',
			'blogsetting_rel_blog_per_row' => '2',
			'blogsetting_rel_prod_per_row' => '3',
			'blogsetting_rel_thumbs_w' => '570',
			'blogsetting_rel_thumbs_h' => '350',
			'blogsetting_rel_prod_height' => '334',
			'blogsetting_rel_prod_width' => '262',
			'blogsetting_blogs_per_page' => '5',
			'blogsetting_thumbs_w' => '1140',
			'blogsetting_thumbs_h' => '700',
			'blogsetting_date_added' => '1',
			'blogsetting_comments_count' => '1',
			'blogsetting_page_view' => '1',
			'blogsetting_author' => '1',
			'blogsetting_layout' => '1'
		];

		$this->model_setting_setting->editSetting('blogsetting', $blog_data);

		$this->load->model('install/basel');
		$this->model_install_basel->createTables();

		$this->log('Basel database installed OK');

		$basel = [
			'basel_header' => 'header_mm',
			'top_line_style' => '1',
			'top_line_height' => '41',
			'main_header_height' => '104',
			'main_header_height_mobile' => '70',
			'main_header_height_sticky' => '70',
			'menu_height_normal' => '64',
			'menu_height_sticky' => '64',
			'logo_maxwidth' => '195',
			'basel_sticky_header' => '0',
			'basel_home_overlay_header' => '0',
			'header_login' => '1',
			'header_search' => '1',
			'primary_menu' => '55',
			'use_custom_links' => '0',
			'basel_back_btn' => '0',
			'basel_hover_zoom' => '1',
			'meta_description_status' => '1',
			'product_page_countdown' => '1',
			'basel_share_btn' => '1',
			'ex_tax_status' => '0',
			'product_question_status' => '0',
			'questions_new_status' => '0',
			'basel_rel_prod_grid' => '4',
			'category_thumb_status' => '0',
			'category_subs_status' => '1',
			'basel_subs_grid' => '5',
			'basel_prod_grid' => '4',
			'catalog_mode' => '0',
			'basel_cut_names' => '0',
			'items_mobile_fw' => '2',
			'quickview_status' => '1',
			'salebadge_status' => '0',
			'stock_badge_status' => '1',
			'countdown_status' => '1',
			'wishlist_status' => '1',
			'compare_status' => '1',
			'overwrite_footer_links' => '1',
			'basel_top_promo_status' => '1',
			'basel_top_promo_close' => '0',
			'basel_cookie_bar_status' => '0',
			'basel_popup_note_status' => '0',
			'basel_popup_note_once' => '0',
			'basel_popup_note_home' => '0',
			'basel_popup_note_m' => '767',
			'basel_cart_icon' => 'global-cart-basket',
			'basel_main_layout' => '0',
			'product_tabs_style' => 'nav-tabs-sm text-center',
			'basel_sticky_columns' => '0',
			'basel_design_status' => '1',
			'basel_typo_status' => '1',
			'basel_custom_css_status' => '0',
			'basel_custom_js_status' => '0',
			'body_font_italic_status' => '0',
			'basel_thumb_swap' => '0',
			'basel_price_update' => '1',
			'basel_sharing_style' => 'small',
			'newlabel_status' => '30',
			'basel_cart_action' => '0',
			'basel_wishlist_action' => '0',
			'basel_compare_action' => '0',
			'top_line_width' => 'boxed',
			'main_header_width' => 'boxed',
			'main_menu_align' => 'menu-aligned-center',
			'basel_promo' => '',
			'basel_promo2' => '',
			'secondary_menu' => '35',
			'basel_titles_listings' => 'default_bc',
			'basel_titles_product' => 'default_bc',
			'basel_titles_account' => 'default_bc',
			'basel_titles_checkout' => 'default_bc',
			'basel_titles_contact' => 'default_bc',
			'basel_titles_blog' => 'default_bc',
			'basel_titles_default' => 'default_bc',
			'product_layout' => 'images-left',
			'full_width_tabs' => '0',
			'questions_per_page' => '',
			'basel_map_style' => '0',
			'basel_map_lat' => '',
			'basel_map_lon' => '',
			'basel_map_api' => '',
			'footer_block_1' => '',
			'footer_block_title' => [
				1 => 'About Us'
			],
			'footer_block_2' => [
				1 => 'STORE - worldwide fashion store since 1978. We sell over 1000+ branded products on our web-site.'
			],
			'footer_infoline_1' => [
				1 => '<i class=&quot;fa fa-location-arrow&quot;></i> 451 Wall Street, USA, New York'
			],
			'footer_infoline_2' => [
				1 => '<i class=&quot;fa fa-mobile&quot;></i> Phone: (064) 332-1233'
			],
			'footer_infoline_3' => '',
			'basel_payment_img' => 'catalog/basel-demo/payments.png',
			'basel_copyright' => [
				1 => '© {year} The Marketplace. All rights reserved.'
			],
			'basel_top_promo_width' => 'full-width',
			'basel_top_promo_align' => 'text-center',
			'basel_top_promo_text' => [
				1 => 'Welcome to The Marketplace! Purchase from thousands of our sellers or sign up and sell your own items.'
			],
			'basel_cookie_bar_url' => '',
			'basel_popup_note_delay' => '8000',
			'basel_popup_note_w' => '920',
			'basel_popup_note_h' => '480',
			'basel_popup_note_img' => 'catalog/basel-demo/popup-note.jpg',
			'basel_popup_note_title' => [
				1 => '<b>HEY YOU, SIGN UP AND CONNECT TO BASEL & CO</b>'
			],
			'basel_popup_note_block' => [
				1 => '<p style=&quot;font-size:16px;color:#666666&quot;>Be the first to learn about our latest trends and get exclusive offers.</p>{signup}'
			],
			'basel_content_width' => 'wide_container',
			'basel_sticky_columns_offset' => '100',
			'basel_widget_title_style' => '3',
			'basel_list_style' => '1',
			'basel_body_bg_color' => '#eeeeee',
			'basel_body_bg_img' => '',
			'basel_body_bg_img_pos' => 'top left',
			'basel_body_bg_img_size' => 'auto',
			'basel_body_bg_img_repeat' => 'no-repeat',
			'basel_body_bg_img_att' => 'scroll',
			'basel_top_note_bg' => '#1db26a',
			'basel_top_note_color' => '#ffffff',
			'basel_top_line_bg' => '#ffffff',
			'basel_top_line_color' => '#222222',
			'basel_header_bg' => '#ffffff',
			'basel_header_color' => '#222222',
			'basel_header_accent' => '#333333',
			'basel_header_menu_bg' => '#ffffff',
			'basel_header_menu_color' => '#222222',
			'basel_search_scheme' => 'dark-search',
			'basel_menutag_sale_bg' => '#ff6029',
			'basel_menutag_new_bg' => '#1db26a',
			'basel_bc_bg_color' => '#222222',
			'basel_bc_bg_img' => '',
			'basel_bc_bg_img_pos' => 'top left',
			'basel_bc_bg_img_size' => 'auto',
			'basel_bc_bg_img_repeat' => 'no-repeat',
			'basel_bc_bg_img_att' => 'scroll',
			'basel_bc_color' => '#ffffff',
			'basel_primary_accent_color' => '#1db26a',
			'basel_salebadge_bg' => '#ff6029',
			'basel_salebadge_color' => '#ffffff',
			'basel_newbadge_bg' => '#ff6600',
			'basel_newbadge_color' => '#ffffff',
			'basel_price_color' => '#ff6029',
			'basel_vertical_menu_bg' => '#ffffff',
			'basel_vertical_menu_bg_hover' => '#ffffff',
			'basel_default_btn_bg' => '#ff6029',
			'basel_default_btn_color' => '#ffffff',
			'basel_default_btn_bg_hover' => '#ff6029',
			'basel_default_btn_color_hover' => '#ffffff',
			'basel_contrast_btn_bg' => '#ff6029',
			'basel_footer_bg' => '#ffffff',
			'basel_footer_color' => '#222222',
			'basel_footer_h5_sep' => '#cccccc',
			'body_font_fam' => "'Lato', sans-serif",
			'body_font_bold_weight' => '700',
			'contrast_font_fam' => "'Lato', sans-serif",
			'body_font_size_16' => '16px',
			'body_font_size_15' => '15px',
			'body_font_size_14' => '14px',
			'body_font_size_13' => '13px',
			'body_font_size_12' => '12px',
			'headings_fam' => "'Lato', sans-serif",
			'headings_weight' => '400',
			'headings_size_sm' => '20px',
			'headings_size_lg' => '28px',
			'h1_inline_fam' => "'Lato', sans-serif",
			'h1_inline_size' => '36px',
			'h1_inline_weight' => '300',
			'h1_inline_trans' => 'uppercase',
			'h1_inline_ls' => '0px',
			'h1_breadcrumb_fam' => "'Lato', sans-serif",
			'h1_breadcrumb_size' => '26px',
			'h1_breadcrumb_weight' => '400',
			'h1_breadcrumb_trans' => 'uppercase',
			'h1_breadcrumb_ls' => '0px',
			'widget_lg_fam' => "'Lato', sans-serif",
			'widget_lg_size' => '26px',
			'widget_lg_weight' => '400',
			'widget_lg_trans' => 'uppercase',
			'widget_lg_ls' => '0px',
			'widget_sm_fam' => "'Lato', sans-serif",
			'widget_sm_size' => '16px',
			'widget_sm_weight' => '700',
			'widget_sm_trans' => 'uppercase',
			'widget_sm_ls' => '0.75px',
			'menu_font_fam' => "'Lato', sans-serif",
			'menu_font_size' => '14px',
			'menu_font_weight' => '400',
			'menu_font_trans' => 'uppercase',
			'menu_font_ls' => '0.5px',
			'subcat_image_width' => '200',
			'subcat_image_height' => '264',
			'quickview_popup_image_width' => '465',
			'quickview_popup_image_height' => '590',
			'basel_custom_css' => '',
			'basel_custom_js' => '',
			'basel_fonts' => [
				1 => [
					'import' => 'Lato:300,300i,400,700',
					'name' => "'Lato', sans-serif"
				]
			],
			'basel_footer_columns' => [
				1 => [
					'title' => [
						1 => 'Buyers'
					],
					'sort' => 1,
					'links' => [
						0 => [
							'title' => [
								1 => 'Sign in'
							],
							'target' => 'account/login?type=customer',
							'sort' => 1
						],
						1 => [
							'title' => [
								1 => "Buyer's guide"
							],
							'target' => 'buyer-guide',
							'sort' => 2
						],
						2 => [
							'title' => [
								1 => 'Shipping information'
							],
							'target' => 'shipping-information',
							'sort' => 3
						],
						3 => [
							'title' => [
								1 => 'Return policy'
							],
							'target' => 'return-policy',
							'sort' => 4
						]
					]
				],
				2 => [
					'title' => [
						1 => 'Sellers'
					],
					'sort' => 2,
					'links' => [
						0 => [
							'title' => [
								1 => 'Sell with us'
							],
							'target' => 'sell',
							'sort' => 1
						],
						1 => [
							'title' => [
								1 => 'Seller FAQ'
							],
							'target' => 'vendor-help',
							'sort' => 2
						]
					]
				],
				3 => [
					'title' => [
						1 => 'Follow us'
					],
					'sort' => 3,
					'links' => [
						0 => [
							'title' => [
								1 => '<i class=&quot;fa fa-instagram&quot;></i> Instagram'
							],
							'target' => 'https://instagram.com',
							'sort' => 1
						],
						1 => [
							'title' => [
								1 => '<i class=&quot;fa fa-facebook-square&quot;></i> Facebook'
							],
							'target' => 'https://facebook.com/multimerch.marketplace',
							'sort' => 2
						],
						2 => [
							'title' => [
								1 => '<i class=&quot;fa fa-twitter&quot;></i> Twitter'
							],
							'target' => 'https://twitter.com/multimerch',
							'sort' => 3
						]
					]
				]
			],
			'basel_links' => ''
		];

		$theme_default = [
			'theme_default_directory' => 'basel',
			'theme_default_status' => 1,
			'theme_default_image_category_width' => '335',
			'theme_default_image_category_height' => '425',
			'theme_default_image_thumb_width' => '550',
			'theme_default_image_thumb_height' => '550',
			'theme_default_image_popup_width' => '910',
			'theme_default_image_popup_height' => '1155',
			'theme_default_image_product_width' => '200',
			'theme_default_image_product_height' => '200',
			'theme_default_image_additional_width' => '130',
			'theme_default_image_additional_height' => '165',
			'theme_default_image_related_width' => '262',
			'theme_default_image_related_height' => '334',
			'theme_default_image_compare_width' => '130',
			'theme_default_image_compare_height' => '165',
			'theme_default_image_wishlist_width' => '55',
			'theme_default_image_wishlist_height' => '70',
			'theme_default_image_cart_width' => '100',
			'theme_default_image_cart_height' => '127',
			'theme_default_image_location_width' => '268',
			'theme_default_image_location_height' => '50',
			'theme_default_product_limit' => '48',
			'theme_default_product_description_length' => '100',
		];

		foreach ($basel as $key => $value) {
			$this->_add_setting('basel', $key, $value);
		}

		foreach ($theme_default as $key => $value) {
			$this->_add_setting('theme_default', $key, $value);
		}

		$this->_add_setting('basel_version', 'basel_theme_version', '1.2.8.0');

		$this->log('Basel settings installed OK');

		// Quickcheckout
		$xml = file_get_contents(DIR_OPENCART . 'source/basel/one_page_checkout.ocmod.xml');

		$data = [
			'name' => 'Basel One Page Checkout',
			'code' => 'basel_onepage_checkout',
			'id' => 'basel_onepage_checkout',
			'extension_install_id' => 'basel_onepage_checkout',
			'author' => 'OpenThemer.com',
			'version' => '1.0',
			'xml' => $xml,
			'link' => '',
			'status' => '1',
		];

		$this->model_extension_modification->addModification($data);

		$this->log('Quickcheckout modification added OK');

		$quickcheckout_settings = [
			'quickcheckout_login_module' => '1',
			'quickcheckout_cart' => '1',
			'quickcheckout_reward' => '1',
			'quickcheckout_voucher' => '1',
			'quickcheckout_coupon' => '1',
			'quickcheckout_field_comment' => ['display' => 'on', 'default' => [1 => ''], 'placeholder' => ''],
			'quickcheckout_field_register' => ['display' => 'on', 'sort_order' => ''],
			'quickcheckout_field_newsletter' => ['display' => 'on', 'default' => 'on', 'sort_order' => ''],
			'quickcheckout_field_zone' => ['display' => 'on', 'required' => 'on', 'default' => '3563', 'sort_order' => '13'],
			'quickcheckout_field_country' => ['display' => 'on', 'required' => 'on', 'default' => '222', 'sort_order' => '12'],
			'quickcheckout_field_postcode' => ['display' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '11'],
			'quickcheckout_field_city' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '10'],
			'quickcheckout_field_address_2' => ['default' => [1 => ''], 'placeholder' => '', 'sort_order' => '9'],
			'quickcheckout_field_address_1' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '8'],
			'quickcheckout_field_customer_group' => ['display' => 'on', 'sort_order' => '7'],
			'quickcheckout_field_company' => ['display' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '6'],
			'quickcheckout_field_fax' => ['default' => [1 => ''], 'placeholder' => '', 'sort_order' => '5'],
			'quickcheckout_field_telephone' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '4'],
			'quickcheckout_field_email' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '3'],
			'quickcheckout_field_lastname' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '2'],
			'quickcheckout_field_firstname' => ['display' => 'on', 'required' => 'on', 'default' => [1 => ''], 'placeholder' => '', 'sort_order' => '1'],
			'quickcheckout_custom_css' => '',
			'quickcheckout_slide_effect' => '0',
			'quickcheckout_responsive' => '1',
			'quickcheckout_layout' => '2',
			'quickcheckout_loading_display' => '1',
			'quickcheckout_load_screen' => '1',
			'quickcheckout_proceed_button_text' => [1 => ''],
			'quickcheckout_payment_target' => '#button-confirm, .button, .btn',
			'quickcheckout_auto_submit' => '0',
			'quickcheckout_text_error' => '1',
			'quickcheckout_highlight_error' => '1',
			'quickcheckout_edit_cart' => '1',
			'quickcheckout_save_data' => '1',
			'quickcheckout_confirmation_page' => '1',
			'quickcheckout_debug' => '0',
			'quickcheckout_minimum_order' => '0',
			'quickcheckout_status' => '1',
			'quickcheckout_html_header' => [1 => ''],
			'quickcheckout_html_footer' => [1 => ''],
			'quickcheckout_payment_module' => '1',
			'quickcheckout_payment_reload' => '0',
			'quickcheckout_payment' => '1',
			'quickcheckout_payment_default' => 'cod',
			'quickcheckout_payment_logo' => ['cod' => '', 'free_checkout' => ''],
			'quickcheckout_shipping_module' => '1',
			'quickcheckout_shipping_reload' => '0',
			'quickcheckout_shipping' => '1',
			'quickcheckout_shipping_default' => 'flat',
			'quickcheckout_shipping_logo' => ['flat' => ''],
			'quickcheckout_survey' => '0',
			'quickcheckout_survey_required' => '0',
			'quickcheckout_survey_text' => [1 => ''],
			'quickcheckout_survey_type' => '0',
			'quickcheckout_delivery' => '0',
			'quickcheckout_delivery_time' => '0',
			'quickcheckout_delivery_required' => '0',
			'quickcheckout_delivery_unavailable' => '&quot;2013-10-31&quot;, &quot;2013-08-11&quot;, &quot;2013-12-25&quot;',
			'quickcheckout_delivery_min' => '1',
			'quickcheckout_delivery_max' => '30',
			'quickcheckout_delivery_min_hour' => '09',
			'quickcheckout_delivery_max_hour' => '17',
			'quickcheckout_delivery_days_of_week' => '',
			'quickcheckout_countdown' => '0',
			'quickcheckout_countdown_start' => '0',
			'quickcheckout_countdown_date_start' => '',
			'quickcheckout_countdown_date_end' => '',
			'quickcheckout_countdown_time' => '00:00',
			'quickcheckout_countdown_text' => [1 => '']
		];

		foreach ($quickcheckout_settings as $key => $value) {
			$this->_add_setting('quickcheckout', $key, $value);
		}

		$this->log('Basel installed OK');
		$this->json_response['message'] = 'Basel installed';

		return true;
	}

	private function _step_unzip_multimerch()
	{
		$this->copyFiles(DIR_OPENCART . 'source/multimerch/core', DIR_OPENCART);

		// Copy MultiMerch installer into install/model/multimerch/ folder
		if (!copy(DIR_OPENCART . 'admin/model/multimerch/install.php', DIR_APPLICATION . 'model/multimerch/install.php')) {
			$this->json_response['message'] = 'Unable to copy MultiMerch installer';
			return false;
		}

		$this->json_response['message'] = 'MultiMerch unzipped';

		return true;
	}

	private function _step_install_multimerch()
	{
		$this->load->model('install/multimerch');

		// Required for createSchema
		$this->registry->set('currency', new Cart\Currency($this->registry));

		// MultiMerch registry entries
		$this->registry->set('MsLoader', \MsLoader::getInstance()->setRegistry($this->registry));
		$this->registry->set('ms_events', new \MultiMerch\Event\EventCollection());
		$this->registry->set('ms_event_manager', new \MultiMerch\Event\EventManager());
		$this->registry->set('MsHooks', new \MultiMerch\Core\Hook());
		$this->registry->set('ms_logger', new \MultiMerch\Logger\Logger());

		$this->model_install_multimerch->install();

		$this->log('MultiMerch installed OK');
		$this->session->data['success'] = '';
		$this->json_response['message'] = 'MultiMerch installed';

		return true;
	}

	private function _step_install_demodata()
	{
		$this->load->model('install/multimerch');

		$this->model_install_multimerch->install_demodata($this->request->post);

		// Add permissions
		$this->load->model('user/user_group');
		$user_id = 1;
		$this->model_user_user_group->addPermission($user_id, 'access', 'module/multimerch_productfilter');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'module/multimerch_productfilter');
		$this->model_user_user_group->addPermission($user_id, 'access', 'module/multimerch_tiles');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'module/multimerch_tiles');
		$this->model_user_user_group->addPermission($user_id, 'access', 'multimerch/complaint');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'multimerch/complaint');
		$this->model_user_user_group->addPermission($user_id, 'access', 'multimerch/payment/bank_transfer');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'multimerch/payment/bank_transfer');
		$this->model_user_user_group->addPermission($user_id, 'access', 'multimerch/payment/paypal');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'multimerch/payment/paypal');
		$this->model_user_user_group->addPermission($user_id, 'access', 'payment/ms_stripe_connect');
		$this->model_user_user_group->addPermission($user_id, 'modify', 'payment/ms_stripe_connect');

		// Change favicon
		$this->load->model('setting/setting');
		$this->_add_setting('config', 'config_icon', 'catalog/mms favico neworange transparent.png');

		// Enable product filter
		$this->_add_setting('multimerch_productfilter', 'multimerch_productfilter_status', '1');
		$this->MsLoader->MsProductFilter->saveDefaultBlocks([
			1 => '6-0',
			2 => '5-0',
			3 => '3-0'
		]);

		$this->log('Demodata installed OK');
		$this->json_response['message'] = 'Demodata installed';

		return true;
	}

	private function _step_install_mm_basel_integration()
	{
		$this->copyFiles(DIR_OPENCART . 'source/multimerch/integration', DIR_OPENCART);

		$this->log('Multimerch+Basel installed OK');
		$this->json_response['message'] = 'Multimerch+Basel installed';

		return true;
	}

	private function _step_install_standalone()
	{
		$this->copyFiles(DIR_OPENCART . 'source/multimerch/standalone/files', DIR_OPENCART);

		// Clear vqmod cache
		$this->clearVQModCache();

		$this->log('Standalone installed OK');
		$this->json_response['message'] = 'Standalone installed';

		return true;
	}

	private function _step_check_inputs()
	{
		if ($this->validate_inputs()) {
			$this->json_response['message'] = 'User inputs are fine!';
			return true;
		} else {
			return false;
		}
	}

	/* HELPERS */

	private function send_json($json = false)
	{
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function is_ajax_request()
	{
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
	}

	private function validate_requirements()
	{
		// Server configuration
		if (phpversion() < '7.0') {
			$this->data['setup_errors']['server_config'][] = $this->language->get('error_version');
		}

		if (ini_get('session.auto_start')) {
			$this->data['setup_errors']['server_config'][] = $this->language->get('error_session');
		}

		// PHP extensions
		if (!ini_get('file_uploads')) {
			$this->data['setup_errors']['php_ext'][] = 'file_uploads';
		}

		if (!array_filter(['mysqli'], 'extension_loaded')) {
			$this->data['setup_errors']['php_ext'][] = 'mysqli';
		}

		if (!extension_loaded('gd')) {
			$this->data['setup_errors']['php_ext'][] = 'GD';
		}

		if (!extension_loaded('curl')) {
			$this->data['setup_errors']['php_ext'][] = 'cURL';
		}

		if (!function_exists('openssl_encrypt')) {
			$this->data['setup_errors']['php_ext'][] = 'openSSL';
		}

		if (!extension_loaded('zlib')) {
			$this->data['setup_errors']['php_ext'][] = 'Zlib';
		}

		if (!extension_loaded('zip')) {
			$this->data['setup_errors']['php_ext'][] = 'Zip';
		}

		if (!function_exists('iconv') || !extension_loaded('mbstring')) {
			$this->data['setup_errors']['php_ext'][] = 'mbstring';
		}

		// CHMOD
		if (!is_writable(DIR_OPENCART . 'image') && !chmod(DIR_OPENCART . 'image', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_OPENCART . 'image';
		}

		if (!is_writable(DIR_OPENCART . 'image/cache') && !chmod(DIR_OPENCART . 'image/cache', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_OPENCART . 'image/cache';
		}

		if (!is_writable(DIR_OPENCART . 'image/catalog') && !chmod(DIR_OPENCART . 'image/catalog', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_OPENCART . 'image/catalog';
		}

		if (!is_writable(DIR_SYSTEM . 'storage/cache') && !chmod(DIR_SYSTEM . 'storage/cache', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_SYSTEM . 'storage/cache';
		}

		if (!is_writable(DIR_SYSTEM . 'storage/logs') && !chmod(DIR_SYSTEM . 'storage/logs', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_SYSTEM . 'storage/logs';
		}

		if (!is_writable(DIR_SYSTEM . 'storage/download') && !chmod(DIR_SYSTEM . 'storage/download', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_SYSTEM . 'storage/download';
		}

		if (!is_writable(DIR_SYSTEM . 'storage/upload') && !chmod(DIR_SYSTEM . 'storage/upload', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_SYSTEM . 'storage/upload';
		}

		if (!is_writable(DIR_SYSTEM . 'storage/modification') && !chmod(DIR_SYSTEM . 'storage/modification', 0775)) {
			$this->data['setup_errors']['chmod'][] = DIR_SYSTEM . 'storage/modification';
		}
	}

	private function validate_inputs()
	{
		if (!$this->request->post['db_hostname']) {
			$this->json_response['error']['db_hostname'] = $this->language->get('error_db_hostname');
		}

		if (!$this->request->post['db_username']) {
			$this->json_response['error']['db_username'] = $this->language->get('error_db_port');
		}

		if (!$this->request->post['db_database']) {
			$this->json_response['error']['db_database'] = $this->language->get('error_db_database');
		}

		if (!$this->request->post['db_port']) {
			$this->json_response['error']['db_port'] = $this->language->get('error_db_port');
		}

		if (!$this->json_response['error']) {

			if ($this->request->post['db_prefix'] && preg_match('/[^a-z0-9_]/', $this->request->post['db_prefix'])) {
				$this->json_response['error']['db_prefix'] = $this->language->get('error_db_prefix');
			}

			if ($this->request->post['db_driver'] == 'mysqli') {
				try {
					$mysqli = @mysqli_connect($this->request->post['db_hostname'], $this->request->post['db_username'], html_entity_decode($this->request->post['db_password'], ENT_QUOTES, 'UTF-8'), $this->request->post['db_database'], intval($this->request->post['db_port']));
				} catch (Exception $e) {
					$this->json_response['error']['warning'] = $e->getMessage();
				} finally {
					// Unable to connect to server
					if (mysqli_connect_errno() > 0) {
						$err = 'MySQLi error: ' . mysqli_connect_errno() . ' - ';

						$this->json_response['error']['warning'] = $err . mb_convert_encoding(mysqli_connect_error(), 'UTF-8', mb_detect_encoding(mysqli_connect_error()));
						header('Content-Type: application/json');
						echo json_encode($this->json_response);
						exit();
					} else {
						// Check MySQL version
						if ($mysqli->server_version < 50605) {
							$this->json_response['error']['warning'] = $this->language->get('error_mysql_version');
							header('Content-Type: application/json');
							echo json_encode($this->json_response);
							exit();
						}
					}
				}

				if ($mysqli->connect_error) {
					$this->json_response['error']['warning'] = $mysqli->connect_error;
				} else {
					$mysqli->close();
				}
			} elseif ($this->request->post['db_driver'] == 'mpdo') {
				try {
					new \DB\mPDO($this->request->post['db_hostname'], $this->request->post['db_username'], $this->request->post['db_password'], $this->request->post['db_database'], $this->request->post['db_port']);
				} catch (Exception $e) {
					$this->json_response['error']['warning'] = $e->getMessage();
				}
			}
		}

		if (!$this->request->post['username']) {
			$this->json_response['error']['username'] = $this->language->get('error_username');
		}

		if (!$this->request->post['password']) {
			$this->json_response['error']['password'] = $this->language->get('error_password');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->json_response['error']['email'] = $this->language->get('error_email');
		}


		if (!is_writable(DIR_OPENCART . 'config.php')) {
			$this->json_response['error']['warning'] = $this->language->get('error_config') . DIR_OPENCART . 'config.php!';
		}

		if (!is_writable(DIR_OPENCART . 'admin/config.php')) {
			$this->json_response['error']['warning'] = $this->language->get('error_config') . DIR_OPENCART . 'admin/config.php!';
		}

		return !$this->json_response['error'];
	}

	private function _add_setting($code = false, $key = false, $value = false)
	{
		$store_id = 0;
		$old_value = $this->model_setting_setting->getSettingValue($key, $store_id);
		if ($old_value) {
			$this->model_setting_setting->editSettingValue($code, $key, $value, $store_id);
		} else {
			if (!is_array($value)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET "
					. "store_id = '" . $store_id . "', "
					. "`code` = '" . $this->db->escape($code) . "', "
					. "`key` = '" . $this->db->escape($key) . "', "
					. "`value` = '" . $this->db->escape($value) . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET "
					. "store_id = '" . $store_id . "', "
					. "`code` = '" . $this->db->escape($code) . "', "
					. "`key` = '" . $this->db->escape($key) . "', "
					. "`value` = '" . $this->db->escape(json_encode($value, true)) . "', "
					. "serialized = '1'");
			}
		}
	}

	private function log($message = '')
	{
		if (!$message) {
			return;
		}

		if (!is_writable(DIR_SYSTEM . 'storage/logs') && !chmod(DIR_SYSTEM . 'storage/logs', 0775)) {
			exit(DIR_SYSTEM . 'storage/logs' . ' not writable! Set it to 775 before continue');
		}

		$filename = DIR_SYSTEM . 'storage/logs/install_' . $this->data['install_stamp'] . '.log';
		$handle = @fopen($filename, "a");

		if (!$handle) {
			return;
		}

		flock($handle, LOCK_EX);
		$now = date("d.m.Y, H:i:s");
		$rn = "\r\n";
		$message = $now . " - " . $message . $rn;
		fwrite($handle, $message);
		flock($handle, LOCK_UN);
		fclose($handle);
	}

	private function clearVQModCache()
	{
		if (is_file(DIR_OPENCART . 'vqmod/mods.cache')) {
			unlink(DIR_OPENCART . 'vqmod/mods.cache');
		}

		if (is_file(DIR_OPENCART . 'vqmod/checked.cache')) {
			unlink(DIR_OPENCART . 'vqmod/checked.cache');
		}

		$vqcache_files = glob(DIR_OPENCART . 'vqmod/vqcache/*');
		foreach ($vqcache_files as $filepath) {
			unlink($filepath);
		}
	}

	private function copyFiles($src, $dst)
	{
		$dir = opendir($src);
		@mkdir($dst);
		while (false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					$this->copyFiles($src . '/' . $file, $dst . '/' . $file);
				} else {
					if (!copy($src . '/' . $file, $dst . '/' . $file)) {
						closedir($dir);
						exit('Unable to copy files! Please, check permissions!');
					}
				}
			}
		}

		closedir($dir);
	}

	private function rmdir_recursive($dir) {
		foreach (scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir("$dir/$file")) {
				$this->rmdir_recursive("$dir/$file");
			} else {
				unlink("$dir/$file");
			}
		}

		rmdir($dir);
	}
}