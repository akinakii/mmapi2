<?php echo $header; ?>

<?php if ($product_layout != "full-width") { ?>
	<style>
		.product-page .image-area {
			<?php if (($product_layout == "images-left") && $images) { ?>
				width: <?php echo ($img_w + $img_a_w + 20); ?>px;
			<?php } else { ?>
				width: <?php echo $img_w; ?>px;
			<?php } ?>
		}
		.product-page .main-image {
			width:<?php echo $img_w; ?>px;
		}
		.product-page .image-additional {
			<?php if ($product_layout == "images-left") { ?>
				width: <?php echo $img_a_w; ?>px;
				height: <?php echo $img_h; ?>px;
			<?php } else { ?>
				width: <?php echo $img_w; ?>px;
			<?php } ?>
		}
		.product-page .image-additional.has-arrows {
			<?php if ($product_layout == "images-left") { ?>
				height: <?php echo ($img_h - 40); ?>px;
			<?php } ?>
		}
		@media (min-width: 992px) and (max-width: 1199px) {
			.product-page .image-area {
				<?php if ($product_layout == "images-left") { ?>
					width: <?php echo (($img_w + $img_a_w)/1.25 + 20); ?>px;
				<?php } else { ?>
					width: <?php echo $img_w/1.25; ?>px;
				<?php } ?>
			}
			.product-page .main-image {
				width:<?php echo $img_w/1.25; ?>px;
			}
			.product-page .image-additional {
				<?php if ($product_layout == "images-left") { ?>
					width: <?php echo $img_a_w/1.25; ?>px;
					height: <?php echo $img_h/1.25; ?>px;
				<?php } else { ?>
					width: <?php echo $img_w/1.25; ?>px;
				<?php } ?>
			}
		}
	</style>
<?php } ?>

<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
</ul>

<div class="container product-layout <?php echo $product_layout; ?>">
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-md-9 col-sm-8'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>

    	<div id="content" class="product-main no-min-height <?php echo $class; ?>">
    
			<div class="table product-info product-page">
				<div class="table-cell left">
					<?php if ($thumb || $images) { ?>
						<div class="image-area hover-zoom-disabled" id="gallery">
							<?php if ($thumb) { ?>
								<div class="main-image">
									<?php if (($price) && ($special) && ($sale_badge)) { ?>
										<span class="badge sale_badge"><i><?php echo $sale_badge; ?></i></span>
									<?php } ?>

									<?php if (isset($is_new)) { ?>
										<span class="badge new_badge"><i><?php echo $basel_text_new; ?></i></span>
									<?php } ?>

									<?php if (($qty < 1) && ($stock_badge_status)) { ?>
										<span class="badge out_of_stock_badge"><i><?php echo $basel_text_out_of_stock; ?></i></span>
									<?php } ?>

									<a class="<?php if (!$images) { echo "link cloud-zoom"; } else if (($product_layout == 'full-width')) { echo "link"; } else { echo "cloud-zoom"; } ?>" id="main-image" href="<?php echo $popup; ?>" rel="position:'inside', showTitle: false"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
								</div>
							<?php } ?>

							<?php if ($images) { ?>
								<ul class="image-additional">
									<?php foreach ($images as $image) { ?>
										<li><a class="link <?php if ($product_layout != 'full-width') { echo "cloud-zoom-gallery locked"; } ?>" href="<?php echo $image['popup']; ?>" rel="useZoom: 'main-image', smallImage: '<?php echo $image['thumb_lg']; ?>'"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
									<?php } ?>
									<?php if ($thumb && ($product_layout != "full-width")) { ?>
										<li><a class="link cloud-zoom-gallery locked active" href="<?php echo $popup; ?>" rel="useZoom: 'main-image', smallImage: '<?php echo $thumb; ?>'"><img src="<?php echo $thumb_sm; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
									<?php } ?>
								</ul>
							<?php } ?>
						</div>
					<?php } ?>
				</div>

				<div class="table-cell w100 right">
					<div class="inner">
						<div class="product-header">
							<div class="product-h1">
								<h1 id="page-title-mm"><?php echo $heading_title; ?></h1>
							</div>

							<?php if ($this->config->get('msconf_reviews_enable')) { ?>
							<div class="mm-product-rating pull-left">
								<div class="ms-ratings side">
									<div class="ms-empty-stars"></div>
									<div class="ms-full-stars" style="width: <?php echo $avg_rating * 20; ?>%"></div>
								</div>
								(<?php echo sprintf(1 === (int)$total_reviews ? $this->language->get('mms_review_total_placeholder_single') : $this->language->get('mms_review_total_placeholder_multiple'), '', "$('html,body').animate({ scrollTop: $('#tab-mm-reviews').offset().top }, 'slow'); return false;", $total_reviews); ?>)
							</div>
							<?php } ?>

							<?php if ($basel_share_btn) { ?>
							<div class="pull-right">
								<?php if ($basel_sharing_style == 'large') { ?>
								<div class="lg-share">
									<div class="social-icons round inversed">
										<a class="icon facebook fb_share external" rel="nofollow"><i class="fa fa-facebook"></i></a>
										<a class="icon twitter twitter_share external" rel="nofollow"><i class="fa fa-twitter"></i></a>
										<a class="icon google google_share external" rel="nofollow"><i class="icon-google-plus"></i></a>
										<a class="icon pinterest pinterest_share external" rel="nofollow"><i class="fa fa-pinterest"></i></a>
										<a class="icon vk vk_share external" rel="nofollow"><i class="fa fa-vk"></i></a>
									</div>
								</div>
								<?php } else { ?>
								<p class="info share"><b><?php echo $basel_text_share; ?>:</b>
									<a class="single_share fb_share external" rel="nofollow"><i class="fa fa-facebook"></i></a>
									<a class="single_share twitter_share external" rel="nofollow"><i class="fa fa-twitter"></i></a>
									<a class="single_share google_share external" rel="nofollow"><i class="icon-google-plus"></i></a>
									<a class="single_share pinterest_share external" rel="nofollow"><i class="fa fa-pinterest"></i></a>
									<a class="single_share vk_share external" rel="nofollow"><i class="fa fa-vk"></i></a>
								</p>
								<?php } ?>
							</div>
							<?php } ?>
						</div>

						<div id="product">
							<?php if ($discounts) { ?>
							<p class="discount">
								<?php foreach ($discounts as $discount) { ?>
								<span><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><i class="price"><?php echo $discount['price']; ?></i></span>
								<?php } ?>
							</p>
							<?php } ?>

							<?php if ($options) { ?>
							<div class="options">
								<?php foreach ($options as $option) { ?>

								<?php if ($option['type'] == 'select') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
												<?php if ($option_value['price']) { ?>
												(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
												<?php } ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'radio') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell radio-cell name">
										<label class="control-label"><?php echo $option['name']; ?>:</label>
									</div>
									<div class="table-cell radio-cell">
										<div id="input-option<?php echo $option['product_option_id']; ?>">
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<div class="radio<?php if ($option_value['image']) echo ' has-image'; ?>">
												<label>
													<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
													<?php if ($option_value['image']) { ?>
													<img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" data-toggle="tooltip" data-title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>" />
													<?php } ?>
													<span class="name">
														<?php echo $option_value['name']; ?>
														<?php if ($option_value['price']) { ?>
														(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
														<?php } ?>
													</span>
												</label>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'checkbox') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell checkbox-cell name">
										<label class="control-label"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell checkbox-cell">
										<div id="input-option<?php echo $option['product_option_id']; ?>">
											<?php foreach ($option['product_option_value'] as $option_value) { ?>
											<div class="checkbox<?php if ($option_value['image']) echo ' has-image'; ?>">
												<label>
													<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
													<?php if ($option_value['image']) { ?>
													<img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" data-toggle="tooltip" data-title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>" />
													<?php } ?>
													<span class="name">
																	<?php echo $option_value['name']; ?>
														<?php if ($option_value['price']) { ?>
														(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
														<?php } ?>
																</span>
												</label>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'text') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'textarea') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'file') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
										<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'date') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<div class="input-group date">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
											<span class="input-group-btn">
														<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
													</span>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'datetime') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<div class="input-group datetime">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
											<span class="input-group-btn">
														<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
													</span>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php if ($option['type'] == 'time') { ?>
								<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> table-row">
									<div class="table-cell name">
										<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
									</div>
									<div class="table-cell">
										<div class="input-group time">
											<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
											<span class="input-group-btn">
														<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
													</span>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php } ?> <!-- foreach option -->
							</div>
							<?php } ?>

							<div class="form-group buy catalog_hide">
								<div class="options padding-b0">
									<?php if ($price) { ?>
									<div class="form-group table-row">
										<div class="table-cell name">
											<label class="control-label">Price:</label>
										</div>
										<div class="table-cell">
											<ul class="list-unstyled price">
												<?php if (!$special) { ?>
													<li><span class="live-price"><?php echo $price; ?><span></li>
												<?php } else { ?>
													<li><div class="price-old"><?php echo $price; ?></div><div class="live-price-new"><?php echo $special; ?></div></li>
													<span id="special_countdown"></span>
												<?php } ?>
											</ul>
										</div>
									</div>
									<?php } ?>

									<?php if (!empty($ms_product_shipping_short)) { ?>
									<div class="form-group table-row">
										<div class="table-cell name va-top">
											<label class="control-label">Shipping:</label>
										</div>
										<div class="table-cell">
											<div>
												<strong class="black">
													<?php if (0 == (float)$ms_product_shipping_short['cost']) { ?>
														Free shipping
													<?php } else { ?>
														From <?php echo $ms_product_shipping_short['cost_formatted']; ?>
													<?php } ?>
												</strong>
												<br/>
												Ships to: <?php echo $ms_product_shipping_short['destinations'][0]['name']; ?> (<a class="link-dotted-bottom" href="" onclick="$('html,body').animate({ scrollTop: $('#tab-mm-shipping').offset().top }, 'slow'); return false;">see details</a>)
											</div>
										</div>
									</div>
									<?php } ?>

									<div class="form-group table-row">
										<div class="table-cell name">
											<label class="control-label">Quantity:</label>
										</div>
										<div class="table-cell">
											<input type="number" step="1" min="<?php echo $minimum; ?>" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" class="form-control input-quantity" />
										</div>
									</div>
								</div>

								<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />

								<button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-lg"><?php if (($qty < 1) && ($stock_badge_status)) { ?><?php echo $basel_text_out_of_stock; ?><?php } else { ?><?php echo $button_cart; ?><?php } ?></button>
								<button type="button" id="button-buy-now" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-lg btn-buy-now"><?php if (($qty < 1) && ($stock_badge_status)) { ?><?php echo $basel_text_out_of_stock; ?><?php } else { ?><?php echo "Buy now"; ?><?php } ?></button>

								<div class="margin-t15">
									<p class="info is_wishlist"><a <?php if ($this->config->get('msconf_wishlist_enabled')) { ?>class="ms-wishlist-add" data-product_id="<?php echo $product_id; ?>"<?php } else { ?>onclick="wishlist.add('<?php echo $product_id; ?>');"<?php } ?>><i class="icon-heart"></i> <?php echo $button_wishlist; ?></a></p>
									<?php if($this->config->get('msconf_complaints_enable') && $this->customer->getId() != $seller['seller_id']) { ?>
									<p class="info is_compare"><a href="<?php echo $this->url->link('multimerch/complaint', 'product_id=' . $product_id) ?>"><i class="fa fa-flag-o"></i> <?php echo $ms_complaint_product_button_title; ?></a></p>
									<?php } ?>
								</div>
							</div>

							<?php if ($minimum > 1) { ?>
							<div class="alert alert-sm alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
							<?php } ?>

							<?php if (!empty($seller)) { ?>
							<!-- start sellerinfo -->
							<div class="options">
								<div class="table-cell name va-top">
									<label class="control-label">Seller:</label>
								</div>
								<div class="table-cell">
									<div class="mm_box mm_description">
										<!-- mm catalog product seller avatar block start -->
										<div class="info-box">
											<a class="avatar-box thumbnail" href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" /></a>
											<div>
												<strong><?php echo $seller['nickname']; ?></strong>
												<?php if($this->config->get('msconf_reviews_enable')) { ?>
												<div>
													<div class="ms-ratings seller">
														<div class="ms-empty-stars"></div>
														<div class="ms-full-stars" style="width: <?php echo $seller_avg_rating * 20; ?>%"></div>
													</div>
													(<?php echo sprintf(1 === (int)$total_reviews ? $this->language->get('mms_review_total_placeholder_single') : $this->language->get('mms_review_total_placeholder_multiple'), $seller['href'] . '#tab-review', '', $seller_total_reviews); ?>)
												</div>
												<?php } ?>

												<a class="btn btn-link" href="<?php echo $seller['href']; ?>"><span><?php echo $ms_catalog_seller_profile; ?></span></a>

												<?php if ($this->config->get('mmess_conf_enable')): ?>
												<!-- mm catalog product messaging start -->
												<?php if ((!$this->customer->getId()) || ($this->customer->getId() != $seller['seller_id'])): ?>
												<?php echo $contactForm; ?>
												<div class="contact">
													<?php if ($this->customer->getId()) { ?>
													<div class="button-group">
														<button type="button" class="btn btn-link ms-sellercontact" data-toggle="modal" data-target="#contactDialog"><span><?php echo $ms_catalog_product_contact; ?></span></button>
													</div>
													<?php } else { ?>
													<?php echo sprintf($this->language->get('ms_sellercontact_signin'), $this->url->link('account/login', '', 'SSL'), $seller['nickname']); ?>
													<?php } ?>
												</div>
												<?php endif; ?>
												<!-- mm catalog product messaging end -->
												<?php endif; ?>
											</div>
										</div>
										<!-- mm catalog product seller avatar block end -->
									</div>
								</div>
								<div class="form-group table-row">
									<div class="table-cell name va-top">
										<label class="control-label">Returns:</label>
									</div>
									<div class="table-cell">
										<a href="<?php echo $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id'], 'SSL'); ?>" class="btn btn-link" target="_blank">See seller's return policies</a>
									</div>
								</div>
							</div>

							<?php if ($this->config->get('mxtconf_ga_seller_enable') && !empty($seller['settings']['slr_ga_tracking_id'])) { ?>
							<!-- mm catalog product google analytics code start -->
							<script>
								(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
								(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
									m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
								})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

								ga('create', '<?php echo $seller['settings']['slr_ga_tracking_id'] ?>', 'auto');
								ga('send', 'pageview');
							</script>
							<!-- mm catalog product google analytics code end -->
							<?php } ?>

							<?php if ($this->config->get('mxtconf_ga_seller_enable') && !empty($seller['settings']['slr_ga_tracking_id'])) { ?>
							<!-- mm catalog product google analytics code start -->
							<script>
								(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
								(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
									m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
								})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

								ga('create', '<?php echo $seller['settings']['slr_ga_tracking_id'] ?>', 'auto');
								ga('send', 'pageview');
							</script>
							<!-- mm catalog product google analytics code end -->
							<?php } ?>
							<?php } ?>

							<div class="clearfix"></div>

							<div class="info-holder">
								<?php if ($price && $tax) { ?>
								<p class="info p-tax"><b><?php echo $text_tax; ?></b> <span class="live-price-tax"><?php echo $tax; ?></span></p>
								<?php } ?>

								<?php if ($price && $points) { ?>
								<p class="info"><b><?php echo $text_points; ?></b> <?php echo $points; ?></p>
								<?php } ?>
							</div><!-- .info-holder ends -->
						</div><!-- #product ends -->
					</div><!-- .inner ends -->
				</div><!-- .table-cell.right ends -->
			</div>
		</div><!-- .#content.product-main ends -->
    
		<?php echo $column_right; ?>
	</div> <!-- .row ends -->

	<div class="row">
		<div class="col-sm-12">
			<?php echo $content_top; ?>
		</div>
	</div>

	<?php if ($attribute_groups) { ?>

	<hr>

	<div id="tab-specification">
		<h3 class="iblock-title"><?php echo $tab_attribute; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<?php foreach ($attribute_groups as $attribute_group) { ?>
						<?php $len = count($attribute_group['attribute']); ?>
						<?php $specs_left = array_slice($attribute_group['attribute'], 0, ceil($len / 2)); ?>
						<?php $specs_right = array_slice($attribute_group['attribute'], ceil($len / 2)); ?>

						<div class="row">
							<div class="col-sm-6">
								<?php foreach ($specs_left as $attribute) { ?>
									<div class="spec-row">
										<?php echo $attribute['name']; ?>: <strong><?php echo $attribute['text']; ?></strong>
									</div>
								<?php } ?>
							</div>
							<div class="col-sm-6">
								<?php foreach ($specs_right as $attribute) { ?>
									<div class="spec-row">
										<?php echo $attribute['name']; ?>: <strong><?php echo $attribute['text']; ?></strong>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>

					<p class="read-more" style="display: none;">
						<a href="#" role="button"><?php echo 'See full specification...'; ?></a>
					</p>
					<p class="read-less">
						<a href="#" role="button"><?php echo $this->language->get('ms_collapse'); ?></a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if ($description) { ?>

	<hr>

	<div id="tab-description">
		<h3 class="text-left iblock-title"><?php echo $tab_description; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<div class="text"><?php echo $description; ?></div>

					<?php if(strlen(preg_replace('/[^a-zA-Z]/', '', $description)) > 1700) { ?>
						<style>
							#tab-description .tab-pane.short {
								height: 230px;
							}
						</style>

						<p class="read-more">
							<a href="#" role="button"><?php echo 'See full description...'; ?></a>
						</p>
						<p class="read-less">
							<a href="#" role="button"><?php echo $this->language->get('ms_collapse'); ?></a>
						</p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<!-- Product Reviews tab header -->
	<?php if ($this->config->get('msconf_reviews_enable')) { ?>

	<hr>

	<div id="tab-mm-reviews">
		<h3 class="text-left iblock-title"><?php echo $tab_review; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<div id="ms_product_reviews">
						<?php echo $ms_product_reviews; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if (!in_array((int)$this->config->get('msconf_shipping_type'), [\MultiMerch\Core\Shipping\Shipping::TYPE_DISABLED, \MultiMerch\Core\Shipping\Shipping::TYPE_OC])) { ?>

	<hr>

	<div id="tab-mm-shipping">
		<h3 class="text-left iblock-title"><?php echo $ms_account_product_tab_shipping; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<?php echo $ms_product_shipping; ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if ($this->config->get('msconf_allow_questions')) { ?>

	<hr>

	<div id="tab-mm-questions">
		<h3 class="text-left iblock-title"><?php echo $ms_product_questions_header; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<div id="mm-questions">
						<?php echo $ms_product_questions; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if (!empty($ms_product_custom_fields)) { ?>

	<hr>

	<div id="tab-mm-cf">
		<h3 class="text-left iblock-title"><?php echo $ms_account_product_tab_custom_fields; ?></h3>

		<div class="row iblock-wrapper">
			<div class="col-sm-12">
				<div class="tab-pane short">
					<table class="table table-bordered">
						<tbody>
						<?php foreach ($ms_product_custom_fields as $custom_field_id => $custom_field) { ?>
						<tr>
							<td><?php echo $custom_field['name']; ?></td>
							<td><?php echo $custom_field['value']; ?></td>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php echo $content_bottom; ?>
</div> <!-- .container ends -->

<script src="catalog/view/theme/basel/js/lightgallery/js/lightgallery.min.js"></script>
<script src="catalog/view/theme/basel/js/lightgallery/js/lg-zoom.min.js"></script>
<script src="catalog/view/theme/basel/js/cloudzoom/cloud-zoom.1.0.2.min.js"></script>
<?php if ($basel_price_update) { ?>
	<script src="index.php?route=extension/basel/live_options/js&product_id=<?php echo $product_id; ?>"></script>
<?php } ?>

<?php if (isset($sale_end_date) && $product_page_countdown) { ?>
	<script>
		$(function() {
			$("#special_countdown").countdown("<?php echo $sale_end_date; ?>")
				.on('update.countdown', function(e) {
					$('#special_countdown').html(
						"<div class=\"special_countdown\"></span><p><span class=\"icon-clock\"></span> <?php echo $basel_text_offer_ends; ?></p><div>" +
						e.offset.totalDays + "<i><?php echo $basel_text_days; ?></i></div><div>" +
						e.offset.hours + " <i><?php echo $basel_text_hours; ?></i></div><div>" +
						e.offset.minutes + " <i><?php echo $basel_text_mins; ?></i></div><div>" +
						e.offset.seconds + " <i><?php echo $basel_text_secs; ?></i></div></div>"
					);
				});
		});
	</script>
<?php } ?>

<script><!--
	$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
		$.ajax({
			url: 'index.php?route=product/product/getRecurringDescription',
			type: 'post',
			data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
			dataType: 'json',
			beforeSend: function() {
				$('#recurring-description').html('');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['success']) {
					$('#recurring-description').html(json['success']);
				}
			}
		});
	});

	$(document).on('click', '.read-more a', function() {
		var totalHeight = 0;

		var $p  = $(this).parent();
		var $up = $p.closest('div');
		var $ps = $up.find('.text');

		$up.css({
			"height": $up.height(),
			"max-height": 9999
		}).animate({
			"height": $ps.outerHeight() + 70
		});

		$p.fadeOut();
		$p.siblings('.read-less').fadeIn();

		return false;
	});

	$(document).on('click', '.read-less a', function() {
		var $p  = $(this).parent();
		var $up = $p.closest('div');

		$up.css({
			"height": $up.height()
		}).animate({
			"height": 230
		});

		$p.fadeOut();
		$p.siblings('.read-more').fadeIn();

		return false;
	});

	$(function () {
		if ($('#tab-specification table').outerHeight() > 230) {
			$('#tab-specification .read-more').show();
		}
	});
//--></script>

<script><!--
$('#button-cart').on('click', function () {
	updateCart(false);
});

$('#button-buy-now').on('click', function() {
	updateCart(true);
});

function updateCart(redirect) {
	redirect = redirect || false;

	$.ajax({
		url: 'index.php?route=extension/basel/basel_features/add_to_cart' + (redirect ? '&redirect_checkout=1' : ''),
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'number\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function(json) {
			$('body').append('<span class="basel-spinner ajax-call"></span>');
		},

		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.table-cell').removeClass('has-error');

			if (json['error']) {
				$('.basel-spinner.ajax-call').remove();
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="alert alert-warning text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="alert alert-warning text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="alert alert-warning text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success_redirect']) {
				location = json['success_redirect'];
			} else if (json['success']) {
				$('.table-cell').removeClass('has-error');
				$('.alert, .popup-note, .basel-spinner.ajax-call, .text-danger').remove();

				html = '<div class="popup-note">';
				html += '<div class="inner">';
				html += '<a class="popup-note-close" onclick="$(this).parent().parent().remove()">&times;</a>';
				html += '<div class="table">';
				html += '<div class="table-cell v-top img"><img src="' + json['image'] + '" /></div>';
				html += '<div class="table-cell v-top">' + json['success'] + '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				$('body').append(html);
				setTimeout(function() {$('.popup-note').hide();}, 8100);
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('.cart-total-items').html( json['total_items'] );
					$('.cart-total-amount').html( json['total_amount'] );
				}, 100);

				$('#cart-content').load('index.php?route=common/cart/info #cart-content > *');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}
//--></script>
<script><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="alert alert-warning text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script><!--
$(document).ready(function() {
	<?php if ($product_layout == "full-width") { ?>
		// Sticky information
		$('.table-cell.right .inner').theiaStickySidebar({containerSelector:'.product-info'});
	<?php } ?>
});
//--></script>

<?php if ($product_layout != "full-width") { ?>
<script>
	$(document).ready(function() {
		$('.image-additional a.link').click(function (e) {
			if ($(this).hasClass("locked")) {
				e.stopImmediatePropagation();
			}
			$('.image-additional a.link.active').removeClass('active');
			$(this).addClass('active')
		});

		<?php if ($images) { ?>
			$('.cloud-zoom-wrap').click(function (e) {
				e.preventDefault();
				$('.image-additional a.link.active').removeClass('locked').trigger('click').addClass('locked');
			});
		<?php } else { ?>
			$('.cloud-zoom-wrap').click(function (e) {
				e.preventDefault();
				$('#main-image').trigger('click');
			});
		<?php } ?>

		$('.image-additional').slick({
			prevArrow: "<a class=\"icon-arrow-left\"></a>",
			nextArrow: "<a class=\"icon-arrow-right\"></a>",
			appendArrows: '.image-additional .slick-list',
			arrows:true,
			<?php if ($direction == 'rtl') { ?>
			rtl: true,
			<?php } ?>
			infinite:false,
			<?php if ($product_layout == "images-left") { ?>
			slidesToShow: <?php echo floor(($img_h)/$img_a_h) ;?>,
			vertical:true,
			verticalSwiping:true,
			<?php } else { ?>
			slidesToShow: <?php echo round($img_w/$img_a_w) ;?>,
			<?php } ?>
			responsive: [{
				breakpoint: 992,
				settings: {
					vertical:false,
					verticalSwiping:false
				}
			}]
		});
	});
//--></script>
<?php } ?>

<script>
	$(document).ready(function() {
		// Image Gallery
		$("#gallery").lightGallery({
			selector: '.link',
			download:false,
			hideBarsDelay:99999
		});
	});
//--></script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Product",
"image": [
<?php if ($thumb) { ?>
"<?php echo $thumb; ?>"
<?php } ?>
],
"description": "<?php echo $meta_description; ?>",
<?php if ($review_qty) { ?>
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": "<?php echo $rating; ?>",
"reviewCount": "<?php echo $review_qty; ?>"},
<?php } ?>
"name": "<?php echo $heading_title; ?>",
"sku": "<?php echo $model; ?>",
<?php if ($manufacturer) { ?>
"brand": "<?php echo $manufacturer; ?>",
<?php } ?>
"offers": {
"@type": "Offer",
<?php if ($qty > 0) { ?>
"availability": "http://schema.org/InStock",
<?php } else { ?>
"availability": "http://schema.org/OutOfStock",
<?php } ?>
<?php if ($price) { ?>
<?php if ($special) { ?>
"price": "<?php echo $special_snippet; ?>",
<?php } else { ?>
"price": "<?php echo $price_snippet; ?>",
<?php } ?>
<?php } ?>
"priceCurrency": "<?php echo $currency_code; ?>"
}
}
</script>
<?php echo $footer; ?>