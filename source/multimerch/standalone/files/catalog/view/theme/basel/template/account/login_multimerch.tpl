<?php echo $header; ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
	<?php if ($column_left && $column_right) { ?>
	<?php $class = 'col-sm-6'; ?>
	<?php } elseif ($column_left || $column_right) { ?>
	<?php $class = 'col-md-9 col-sm-8'; ?>
	<?php } else { ?>
	<?php $class = 'col-sm-12'; ?>
	<?php } ?>
	<div id="content" class="<?php echo $class; ?> no-min-height">
	    <?php echo $content_top; ?>

	    <div class="login-area">
		<div class="row">

		    <div class="col-sm-6 col-sm-offset-3">
			<h2 class="text-center"><?php echo $button_login; ?></h2>

			<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
			<?php } ?>
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
			<?php } ?>

			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-vertical">
			    <div class="form-group">
				<label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
				<input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
			    </div>
			    <div class="form-group">
				<label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
				<input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
				<a href="<?php echo $forgotten; ?>" class="label-link pull-right margin-b20"><?php echo $text_forgotten; ?></a>  
			    </div>
			  
			    <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary btn-block margin-b10" />
			    <?php echo $text_register_before; ?> <a class="label-link" href="<?php echo $register; ?>"><?php echo $text_register; ?></a>
			    <?php if ($redirect) { ?>
			    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
			    <?php } ?>
			</form>
		    </div>


		</div>
	    </div>

	    <?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>