<?php
// Modules
$_['heading_title_latest'] 	= 	'Последние публикации блога';
$_['heading_title_category'] = 	'Категории блога';
$_['text_show_all'] = 			'Посетите блог';

// Blog
$_['text_blog'] = 				'Блог';
$_['text_filter_by'] = 			'Публикации, отмеченные как: ';
$_['text_posted_by'] = 			'Опубликовал';
$_['text_read'] = 				'раз прочитано';
$_['text_comments'] = 			'Комментарии';
$_['text_related_blog'] = 		'Публикации по теме';
$_['text_related_products'] = 	'Товары по теме';
$_['text_write_comment'] = 		'Написать комментарий';
$_['text_no_blog_posts'] = 		'Публикаций нет';
$_['text_error'] = 				'Страница не найдена';
$_['text_read_more'] = 			'Читать дальше';
$_['text_tags'] = 				'Тэги:';
$_['text_tax'] = 				'Без налога:';
$_['text_write_comment'] = 		'Написать комментарий';
$_['email_notification'] = 		'Новый комментарий в блоге от: %s';

// Comment
$_['entry_name'] = 				'Имя';
$_['entry_email'] = 			'Email (не публикуется)';
$_['entry_comment'] = 			'Ваш комментарий';
$_['entry_captcha'] = 			'Ответьте на вопрос ниже:';
$_['button_send'] = 			'Отправить';
$_['text_success_approve'] = 	'Спасибо! Ваш комментарий отправлен на модерацию';
$_['text_success'] = 			'Спасибо! Ваш комментарий успешно отправлен';
$_['error_name'] = 				'Ваше имя должно быть длиннее 2-х и короче 64-х символов';
$_['error_email'] = 			'Ошибка: Ваш email недействителен';
$_['error_comment'] = 			'Ошибка: Текст комментария должен быть длиннее 5-и и короче 3000 символов';
$_['error_captcha'] = 			'Ошибка: Неверный проверочный ответ';