<?php

/***** INSERTED *****/$_['ms_language_version'] = '1.29.0.0';

// **********
// * Global *
// **********
$_['ms_viewinstore'] = 'Просмотреть в магазине';
$_['ms_view'] = 'Просмотр';
$_['ms_view_modify'] = 'Просмотр / Изменить';
$_['ms_view_invoice'] = 'Посмотреть счёт';
$_['ms_publish'] = 'Опубликован';
$_['ms_unpublish'] = 'Не опубликован';
$_['ms_activate'] = 'Активировать';
$_['ms_deactivate'] = 'Отключить';
$_['ms_edit'] = 'Правка';
$_['ms_relist'] = 'Перевыставить';
$_['ms_remove'] = 'Убрать';
$_['ms_delete'] = 'Удалить';
$_['ms_type'] = 'Тип';
$_['ms_amount'] = 'Кол-во';
$_['ms_status'] = 'Статус';
$_['ms_date_paid'] = 'Дата платежа';
$_['ms_last_message'] = 'Последнее сообщение';
$_['ms_description'] = 'Описание';
$_['ms_id'] = '#';
$_['ms_name'] = 'Название';
$_['ms_by'] = 'от';
$_['ms_action'] = 'Действие';
$_['ms_sender'] = 'Отправитель';
$_['ms_message'] = 'Сообщение';
$_['ms_none'] = 'Нет';
$_['ms_drag_drop_here'] = 'Перетащите файлы сюда для загрузки';
$_['ms_drag_drop_click_here'] = 'Перетащите изображение сюда или кликните для загрузки';
$_['ms_or'] = 'или';
$_['ms_select_files'] = 'Выбрать файлы';
$_['ms_allowed_extensions'] = 'Разрешенные расширения: %s';
$_['ms_all_products'] = 'Все товары';
$_['ms_from'] = 'От';
$_['ms_to'] = 'До';
$_['ms_up_to'] = 'До';
$_['ms_on'] = '';
$_['ms_store'] = 'Магазин';
$_['ms_store_owner'] = 'Владелец Магазина';
$_['ms_sort_order'] = 'Порядок сортировки';
$_['ms_yes'] = 'Да';
$_['ms_no'] = 'Нет';
$_['ms_add'] = 'Добавить';
$_['ms_reply'] = 'Ответить';
$_['ms_expand'] = 'Развернуть';
$_['ms_collapse'] = 'Свернуть';
$_['ms_days'] = 'дня(ей)';
$_['ms_products_template'] = '%s товар(ов)';
/***** INSERTED *****/$_['ms_see_more'] = "See more";
/***** INSERTED *****/$_['ms_enabled'] = "Enabled";
/***** INSERTED *****/$_['ms_disabled'] = "Disabled";

$_['ms_default_select_value'] = 'значение по умолчанию';
$_['ms_placeholder_deleted'] = "*Удален*";
$_['ms_seller_deleted'] = '*Продавец удален*';
$_['ms_product_deleted'] = '*Товар удален*';

$_['ms_date_created'] = 'Дата создания';
$_['ms_date_added'] = 'Дату добавлено';
$_['ms_date_modified'] = 'Дата изменена';
$_['ms_date'] = 'Дата создания';

$_['ms_button_submit'] = 'Сохранить';
$_['ms_button_add_special'] = 'Добавить акцию';
$_['ms_button_add_discount'] = 'Добавить скидку';
$_['ms_button_save'] = 'Сохранить';
$_['ms_button_cancel'] = 'Отмена';
$_['ms_button_back'] = 'Назад';
$_['ms_button_pay'] = 'Оплатить';

$_['ms_button_select_image'] = 'Выбор изображения';

$_['ms_transaction_order_created'] = 'Заказ создан';
$_['ms_transaction_sale'] = 'Продажа: %s (-%s сбор)';
$_['ms_transaction_sale_no_commission'] = 'Продажа: %s';
$_['ms_transaction_sale_fee'] = 'Сбор с продажи: %s';
$_['ms_transaction_sale_fee_order'] = 'Сбор с продажи: Заказ #%s';
$_['ms_transaction_sale_fee_order_refund'] = 'Вернуть сбор с продажи: Заказ #%s';
$_['ms_transaction_refund'] = 'Возврат: %s';
$_['ms_transaction_shipping'] = 'Доставка: %s';
$_['ms_transaction_shipping_order'] = 'Доставка: Заказ #%s';
$_['ms_transaction_shipping_refund'] = 'Возврат за доставку: %s';
$_['ms_transaction_shipping_order_refund'] = 'Возврат стоимости доставки: Заказ #%s';
$_['ms_transaction_coupon'] = 'Скидка по купону торговой площадки';
$_['ms_transaction_coupon_refund'] = 'Возврат средств по купону торговой площадки';
$_['ms_transaction_ms_coupon'] = 'Скидка по купону';
$_['ms_transaction_ms_coupon_refund'] = 'Возврат средств по купону';


// Mails

// Seller
$_['ms_mail_greeting'] = "Здравствуйте %s,\n";
$_['ms_mail_greeting_no_name'] = "Здравствуйте,\n";
$_['ms_mail_ending'] = "\nС уважением,\n%s";
$_['ms_mail_message'] = "\nСообщение:\n%s";

$_['ms_mail_subject_seller_account_created'] = 'Аккаунт продавца успешно создан';
$_['ms_mail_seller_account_created'] = <<<EOT
Ваш аккаунт продавца %s был успешно зарегистрирован!

Вы можете добавить новые товары.
EOT;

$_['ms_mail_subject_seller_account_awaiting_moderation'] = 'Аккаунт продавца ожидает подтверждения';
$_['ms_mail_seller_account_awaiting_moderation'] = <<<EOT
Ваш аккаунт продавца %s был успешно зарегистрирован и сейчас ожидает подтверждения.

Вы получите уведомление, как только аккаунт будет одобрен.
EOT;

$_['ms_mail_subject_product_awaiting_moderation'] = 'Товар ожидает подтверждения';
$_['ms_mail_product_awaiting_moderation'] = <<<EOT
Ваш товар %s из %s ожидает подтверждения.

Вы получите уведомление, как только товар будет одобрен.
EOT;

$_['ms_mail_subject_product_purchased'] = 'Новый заказ';
$_['ms_mail_product_purchased'] = <<<EOT
Товар(ы) были куплены в %s.

Покупатель: %s (%s)

Товары:
%s
Всего: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
Товары(ы) были куплены в %s.

Покупатель: %s

Товары:
%s
Всего: %s
EOT;

$_['ms_mail_subject_seller_contact'] = 'Новое сообщение от пользователя';
$_['ms_mail_seller_contact'] = <<<EOT
Вы получили новое сообщение от пользователя!

Имя: %s

Email: %s

Товар: %s

Сообщение:
%s
EOT;

$_['ms_mail_seller_contact_no_mail'] = <<<EOT
Вы получили новое сообщение от пользователя!

Имя: %s

Товар: %s

Сообщение:
%s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
Адрес доставки:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'Комментарий: %s';

$_['ms_mail_subject_product_reviewed'] = 'Новый отзыв о товаре';
$_['ms_mail_product_reviewed'] = <<<EOT
Новый отзыв оставлен для %s.
Пройдите по следующей ссылке чтобы просмотреть его : <a href="%s">%s</a>
EOT;

$_['ms_mail_subject_product_returned'] = 'Запрос на возврат товара';
$_['ms_mail_product_returned'] = <<<EOT
Покупатель оставил запрос на возврат вашего товара %s. Администратор %s скоро свяжется с вами.
EOT;

$_['ms_mail_subject_withdraw_request_submitted'] = 'Запрос выплаты отправлен';
$_['ms_mail_withdraw_request_submitted'] = <<<EOT
Мы получили Ваш запрос на выплату. Вы получите перевод, как только запрос будет обработан.
EOT;

$_['ms_mail_subject_withdraw_request_completed'] = 'Заказ выплаты завершен';
$_['ms_mail_withdraw_request_completed'] = <<<EOT
Ваш заказ на выплату был обработан. Перевод был отправлен.
EOT;

$_['ms_mail_subject_withdraw_request_declined'] = 'Заказ выплаты отклонен';
$_['ms_mail_withdraw_request_declined'] = <<<EOT
Ваш заказ на выплату был отклонен. Сумма была возвращена к Вашему балансу %s.
EOT;

$_['ms_mail_subject_transaction_performed'] = 'Новая транзакция';
$_['ms_mail_transaction_performed'] = <<<EOT
Новая транзакция была добавлена на Ваш аккаунт %s.
EOT;

$_['ms_mail_subject_remind_listing'] = 'Публикация товара завершена';
$_['ms_mail_seller_remind_listing'] = <<<EOT
Публикация товара %s была успешно завершена. При желании Вы можете повторно опубликовать товар в своей панели управления продавца.
EOT;

// *********
// * Admin *
// *********
$_['ms_mail_admin_subject_seller_account_created'] = 'Создан новый аккаунт продавца';
$_['ms_mail_admin_seller_account_created'] = <<<EOT
Новый аккаунт продавца %s был успешно создан!
Имя продавца: %s (%s)
E-mail: %s
EOT;

$_['ms_mail_admin_subject_seller_account_awaiting_moderation'] = 'Новый аккаунт продавца ожидает подтверждения';
$_['ms_mail_admin_seller_account_awaiting_moderation'] = <<<EOT
Новый аккаунт продавца %s был успешно создан и ожидает модерации.
Имя продавца: %s (%s)
E-mail: %s

Вы можете продолжить в разделе Multiseller - Продавцы в панели администратора.
EOT;

$_['ms_mail_admin_subject_product_created'] = 'Добавлен новый товар';
$_['ms_mail_admin_product_created'] = <<<EOT
Новый товар %s был добавлен в %s.

Вы можете просмотреть и отредактировать его в панели администратора.
EOT;

$_['ms_mail_admin_subject_new_product_awaiting_moderation'] = 'Новый товар ожидает подтверждения';
$_['ms_mail_admin_new_product_awaiting_moderation'] = <<<EOT
Новый товар %s был добавлен в %s и ожидает модерации.

Вы можете продолжить в разделе Multiseller - Товары в панели администратора.
EOT;

$_['ms_mail_admin_subject_edit_product_awaiting_moderation'] = 'Товар изменен и ожидает модерации';
$_['ms_mail_admin_edit_product_awaiting_moderation'] = <<<EOT
Товар %s для %s был изменен и ожидает модерации.

Вы можете продолжить в разделе Multiseller - Товары в панели администратора.
EOT;

$_['ms_mail_admin_subject_withdraw_request_submitted'] = 'Запрос выплаты ожидает подтверждения';
$_['ms_mail_admin_withdraw_request_submitted'] = <<<EOT
Новый запрос на выплату был отправлен.

Вы можете продолжить в разделе Multiseller - Финансы в панели администратора.
EOT;

// Catalog - Mail
// Attributes
$_['ms_mail_subject_attribute_created'] = 'Новый атрибут создан продавцом %s: %s';
$_['ms_mail_attribute_created'] = <<<EOT
Новый атрибут был создан продавцом <strong>%s</strong>: <strong>%s</strong>.

Пожалуйста, нажмите <a href="%s">this link</a> для утверждения, отклонения и управление атрибутами.
EOT;

// Attribute groups
$_['ms_mail_subject_attribute_group_created'] = 'Новая группа атрибутов создана продавцом %s: %s';
$_['ms_mail_attribute_group_created'] = <<<EOT
Новая группа атрибутов была создана продавцом <strong>%s</strong>: <strong>%s</strong>.

Пожалуйста, нажмите <a href="%s">this link</a> для утверждения, отклонения и управления группами атрибутов.
EOT;

// Options
$_['ms_mail_subject_option_created'] = 'Новая опция создана продавцом %s: %s';
$_['ms_mail_option_created'] = <<<EOT
Новая опция была создана продавцом <strong>%s</strong>: <strong>%s</strong>.

Пожалуйста, нажмите <a href="%s">this link</a> для утверждения, отклонения и управление опциями.
EOT;

// Categories
$_['ms_mail_subject_category_created'] = 'Новая категория создана продавцом %s: %s';
$_['ms_mail_category_created'] = <<<EOT
Новая категория была создана продавцом <strong>%s</strong>: <strong>%s</strong>.

Пожалуйста, нажмите <a href="%s">эту ссылку</a> чтобы утвердить, отклонить и управлять категориями.
EOT;


// Success
$_['ms_success_product_published'] = 'Товар опубликован';
$_['ms_success_product_unpublished'] = 'Товар не опубликован';
$_['ms_success_product_created'] = 'Товар создан';
$_['ms_success_product_updated'] = 'Товар обновлен';
$_['ms_success_product_deleted'] = 'Товар удален';

// Errors
$_['ms_error_sellerinfo_nickname_empty'] = 'Никнейм не может быть пустым';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'Никнейм может содержать только алфавитно-цифровые символы';
$_['ms_error_sellerinfo_nickname_utf8'] = 'Никнейм может содержать только печатные UTF-8 символы';
$_['ms_error_sellerinfo_nickname_latin'] = 'Никнейм может содержать только алфавитно-цифровые символы и диакритические знаки';
$_['ms_error_sellerinfo_nickname_length'] = 'Никнейм должен содержать от 4 до 32 символов';
$_['ms_error_sellerinfo_nickname_taken'] = 'Этот никнейм уже занят';
$_['ms_error_sellerinfo_description_length'] = 'Описание не может быть больше 1000 символов';
$_['ms_error_sellerinfo_terms'] = 'Внимание: Вы должны согласиться с %s!';

$_['ms_error_file_extension'] = "Ошибка загрузки %s: расширение файла (%s) не допустимо (%s).";
$_['ms_error_file_type'] = "Ошибка при загрузке %s: тип файла недействителен.";
$_['ms_error_file_post_size'] = "Ошибка загрузки %s: post_max_size вашего сервера (%s) превышен.";
$_['ms_error_file_upload_size'] = "Ошибка загрузки %s: upload_max_filesize вашего сервера (%s) превышено.";
$_['ms_error_file_upload_error'] = 'Ошибка при загрузке %s: %s.';
$_['ms_file_default_filename'] = 'файл';
$_['ms_file_unclassified_error'] = 'Неклассифицированная ошибка';
$_['ms_file_filename_error_greater'] = 'Имя файла больше, чем %s символов.';
$_['ms_file_filename_error_less'] = 'Имя файла меньше, чем %s символов';
$_['ms_file_cross_session_upload'] = "Файл не находится в текущем сеансе";
$_['ms_file_cross_product_file'] = "Файл не принадлежит этому товару";

$_['ms_error_image_too_small'] = 'Размер изображения слишком мал. Минимально допустимый размер: %s x %s (Ширина x Высота)';
$_['ms_error_image_too_big'] = 'Размер изображения слишком большой. Максимально допустимый размер: %s x %s (Ширина x Высота)';
$_['ms_error_form_submit_error'] = 'Произошла ошибка при отправке формы. Пожалуйста, свяжитесь с администрацией магазина для получения дополнительной информации.';
$_['ms_error_form_notice'] = 'Пожалуйста, проверьте все поля формы на наличие ошибок.';
$_['ms_error_product_price_empty'] = 'Пожалуйста, укажите цену для Вашего товара';
$_['ms_error_product_price_invalid'] = 'Недопустимая цена';
$_['ms_error_product_price_low'] = 'Цена слишком низкая';
$_['ms_error_product_price_high'] = 'Цена слишком высокая';
$_['ms_error_product_category_empty'] = 'Пожалуйста, укажите категорию';
$_['ms_error_product_model_empty'] = 'Модель товара не может быть пустой';
$_['ms_error_product_model_length'] = 'Модель товара должна содержать от %s до %s символов';
$_['ms_error_product_image_count'] = 'Пожалуйста, загрузите не менее %s изображений для Вашего товара';
$_['ms_error_product_download_count'] = 'Пожалуйста, загрузите не менее %s файлов для Вашего товара';
$_['ms_error_product_image_maximum'] = 'Разрешено не более %s изображений';
$_['ms_error_product_download_maximum'] = 'Разрешено не более %s файлов';
$_['ms_error_contact_text'] = 'Сообщение не может содержать больше 2000 символов';
$_['ms_error_contact_allfields'] = 'Пожалуйста, заполните все поля';
$_['ms_error_invalid_quantity_discount_priority'] = 'Ошибка в поле приоритета - введите правильное значение';
$_['ms_error_invalid_quantity_discount_quantity'] = 'Количество должно быть 2 или больше';
$_['ms_error_invalid_quantity_discount_price'] = 'Указана неверная цена скидки за количество';
$_['ms_error_invalid_quantity_discount_dates'] = 'Поле даты для скидки за количество должно быть заполнено';
$_['ms_error_invalid_special_price_priority'] = 'Ошибка в поле приоритета - введите правильное значение';
$_['ms_error_invalid_special_price_price'] = 'Указана неверная специальная цена';
$_['ms_error_invalid_special_price_dates'] = 'Поле даты для специальной цены должно быть заполнено';
$_['ms_error_slr_gr_product_number_limit_exceeded'] = 'Вы опубликовали максимально разрешенное количество товаров для вашей учетной записи. Пожалуйста, свяжитесь с нами для получения дополнительной информации и для снятия ограничения на количество товаров.';
$_['ms_error_fixed_coupon_warning'] = "Внимание: Этот код купона не может быть использован в это время!";
$_['ms_error_voucher_warning'] = "Внимание: Этот подарочный сертификат не может быть использован в это время!";
$_['ms_error_product_forbid_to_buy_own_product'] = "Вы не можете приобрести свои собственные товары!";

// Account - General
$_['ms_account_register_new'] = "Новый продавец";
$_['ms_account_register_customer'] = "Зарегистрироваться как покупатель";
$_['ms_account_register_seller'] = "Зарегистрироваться как продавец";
$_['ms_account_register_seller_note'] = "Создайте новую учетную запись продавца и продавайте ваши товары!";
$_['ms_account_register_account_information'] = "Учетная запись";
$_['ms_account_register_firstname'] = "Имя";
$_['ms_account_register_lastname'] = "Фамилия";
$_['ms_account_register_email'] = "Эл. почта";
$_['ms_account_register_password'] = "Пароль";
$_['ms_account_register_password_confirm'] = "Подтвердите пароль";
$_['ms_account_register_store_information'] = "Информация о магазине";
$_['ms_account_register_store_name'] = "Название магазина";
$_['ms_account_register_slogan'] = "Слоган";
$_['ms_account_register_description'] = "Описание";
$_['ms_account_register_website'] = "Сайт";
$_['ms_account_register_company'] = "Компания";
$_['ms_account_register_phone'] = "Телефон";
$_['ms_account_register_avatar'] = "Логотип";
$_['ms_account_register_banner'] = "Баннер";
$_['ms_account_register_address_information'] = "Адрес";
$_['ms_account_register_address_fullname'] = "Полное имя";
$_['ms_account_register_address_line1'] = "Адрес 1";
$_['ms_account_register_address_line2'] = "Адрес 2";
$_['ms_account_register_city'] = "Город";
$_['ms_account_register_state'] = "Штат/Провинция/Регион";
$_['ms_account_register_zip'] = "Почтовый индекс";
$_['ms_account_register_country'] = "Страна";
$_['ms_account_register_success_created'] = "Добро пожаловать в учетную запись вашего магазина!";

$_['ms_account_profile_general'] = "Общая информация";
$_['ms_account_profile_group'] = "Выберите ваш план";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_SALE] = "%s + %s%% с продажи";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_LISTING] = "%s + %s%% за листинг";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_SIGNUP] = "%s за регистрацию";
$_['ms_account_group_select_plan'] = "Выбрать";

$_['ms_seller'] = 'Продавец';
$_['ms_seller_account_heading'] = 'Мой магазин';
$_['ms_catalog'] = 'Каталог';
$_['ms_account_dashboard'] = 'Панель управления';
$_['ms_account_customer'] = 'Покупатель';
$_['ms_account_my_account'] = 'Мой аккаунт';
$_['ms_account_overview'] = 'Обзор';
$_['ms_account_sellerinfo'] = 'Профиль продавца';
$_['ms_account_sellerinfo_new'] = 'Новый аккаунт продавца';
$_['ms_account_sellerinfo_new_short'] = 'Новый аккаунт';
$_['ms_account_newproduct'] = 'Новый товар';
$_['ms_account_products'] = 'Товары';
$_['ms_account_transactions'] = 'Транзакции';
$_['ms_account_payments'] = 'Платежи';
$_['ms_account_payment_requests'] = 'Счета';
$_['ms_account_import'] = 'Импорт товаров из CSV';
/***** INSERTED *****/$_['ms_account_import_list'] = "View imports";
$_['ms_account_orders'] = 'Заказы';
$_['ms_account_revenue'] = 'Выручка';
$_['ms_account_views'] = 'Просмотры';
$_['ms_account_withdraw'] = 'Запрос выплаты';
$_['ms_account_stats'] = 'Статистика';
$_['ms_account_settings'] = 'Настройки';
$_['ms_account_profile'] = 'Профиль';
$_['ms_account_member_since'] = 'С момента регистрации';
$_['ms_account_dashboard_in_month'] = 'этот месяц';
$_['ms_account_no_orders'] = 'У вас нет заказов.';
$_['ms_account_general'] = 'Общее';
$_['ms_account_to_customer_account'] = '< Аккаунт покупателя';
$_['ms_account_to_seller_account'] = '< Аккаунт продавца';
/***** INSERTED *****/$_['ms_seller_fields'] = 'Product fields';
/***** INSERTED *****/$_['ms_seller_attributes'] = 'Attributes';

$_['ms_account_payment_requests_empty'] = "Нет счетов.";

// customer account

$_['ms_account_edit_info'] = 'Редактировать информацию.';
$_['ms_account_password'] = 'Изменить пароль';
$_['ms_account_wishlist'] = 'Список желаний';
$_['ms_account_newsletter'] = 'Рассылка';
$_['ms_account_address_book'] = 'Адресная книга';
$_['ms_account_order_history'] = 'История заказов';
$_['ms_account_reward_points'] = 'Баллы';
$_['ms_account_returns'] = 'Возвраты';
$_['ms_account_logout'] = 'Выйти';

// Disqus
$_['mxt_disqus_comments'] = 'Комментарии Disqus';

// Analytics
$_['mxt_google_analytics'] = 'Google Analytics';
$_['mxt_google_analytics_code'] = 'Tracking ID';
$_['mxt_google_analytics_code_note'] = 'Укажите ваш Google Analytics tracking ID чтобы следить за эффективностью вашего профиля и ваших товаров.';

// Badges

// Soclal links
$_['ms_sl_social_media'] = 'Социальные медиа.';

// Account - New product
$_['ms_account_newproduct_heading'] = 'Новый товар';
//General Tab
$_['ms_account_product_tab_specials'] = 'Акция';
$_['ms_account_product_tab_discounts'] = 'Скидка за количество';
$_['ms_account_product_name_description'] = 'Название и описание';
$_['ms_account_product_additional_data'] = 'Дополнительная информация';
$_['ms_account_product_search_optimization'] = 'Поисковая оптимизация';

$_['ms_account_product_name'] = 'Название';
$_['ms_account_product_name_note'] = 'Название товара';

$_['ms_account_product_description'] = 'Описание';
$_['ms_account_product_description_note'] = 'Описание товара';

$_['ms_account_product_meta_description'] = 'Мета-теги описания';
$_['ms_account_product_meta_description_note'] = 'Укажите мета-теги описания для Вашего товара';

$_['ms_account_product_seo_keyword'] = 'SEO Keyword';
$_['ms_account_product_seo_keyword_note'] = 'Это будет показано в урл вашего товара. Не используйте пробелы или специальные символы.';

$_['ms_account_product_meta_title'] = 'Meta Title';
$_['ms_account_product_meta_title_note'] = 'Meta Title - это заголовок который появится на странице списка товаров.';

$_['ms_account_product_meta_keyword'] = 'Мета-теги ключевые слова';
$_['ms_account_product_meta_keyword_note'] = 'Укажите Meta Tag Keywords для Вашего товара';

$_['ms_account_product_tags'] = 'Теги';
$_['ms_account_product_tags_note'] = 'Теги товара';

$_['ms_account_product_price'] = 'Цена';
/***** INSERTED *****/$_['ms_account_product_unit_price'] = 'Unit Price';
$_['ms_account_product_price_note'] = 'Укажите стоимость вашего товара (напр. %s1%s000%s00%s)';

$_['ms_account_product_digital'] = 'Цифровой товар';

$_['ms_account_product_attribute'] = 'Аттрибут';
$_['ms_account_product_attributes'] = 'Аттрибуты';
$_['ms_account_product_value'] = 'Значение';
$_['ms_account_product_new_attribute'] = '+ Добавить аттрибут';

$_['ms_account_product_listing_balance'] = "Эта сумма будет автоматически вычтена из вашего баланса.";
$_['ms_account_product_listing_pg'] = "Платежный счет на эту сумму будет создан в разделе Счета в вашем аккаунте.";

$_['ms_account_product_listing_flat'] = 'Стоимость публикации этого товара <span>%s</span>';
$_['ms_account_product_listing_percent'] = 'Стоимость публикации этого товара зависит от его цены. Текущая стоимость: <span>%s</span>.';

$_['ms_account_product_listing_fee_category'] = "Стоимость публикации данного товара определяется категорией, к которой он относится. Текущая стоимость публикации: %s.";
$_['ms_account_product_listing_fee_seller'] = "Стоимость публикации данного товара определяется вашей группой. Текущая стоимость публикации: %s.";
$_['ms_account_product_listing_fee_combined'] = "Стоимость публикации данного товара определяется категорией, к которой он относится, и вашей группой. Текущая стоимость публикации: %s.";

$_['ms_account_product_categories'] = 'Категории торговой площадки';
$_['ms_account_product_vendor_categories'] = 'Мои категории';
$_['ms_account_product_category_select'] = 'Пожалуйста, выберите категорию';
$_['ms_account_product_category_add'] = '+ Добавить вторичную категорию';
$_['ms_account_product_category_add_more'] = '+ Добавить больше категорий';
$_['ms_account_product_category_note'] = 'Категория товара';
$_['ms_account_product_vendor_category_note'] = 'Выберите категорию для вашего товара из собственного списка категорий';
$_['ms_account_product_error_category_not_childmost'] = "Пожалуйста, выберите категорию";
$_['ms_account_product_quantity'] = 'Количество';
$_['ms_account_product_quantity_note']    = 'Укажите количество для Вашего товара';
$_['ms_account_product_minorderqty'] = 'Минимальное количество';
$_['ms_account_product_minorderqty_note']    = 'Укажите минимальное количество заказа вашей продукции';
$_['ms_account_product_files'] = 'Файлы';
$_['ms_account_product_download'] = 'Загрузки';
$_['ms_account_product_download_note'] = 'Загрузите файлы для Вашего товара. Поддерживаются расширения: %s';
$_['ms_account_product_image'] = 'Изображения';
$_['ms_account_product_image_note'] = 'Выберите изображения для Вашего товара. Первое изображение будет использовано, как превью. Вы можете менять порядок изображений перетаскивая их. Поддерживаются расширения: %s';
//Data Tab
$_['ms_account_product_model'] = 'Модель';
$_['ms_account_product_sku'] = 'SKU';
$_['ms_account_product_sku_note'] = 'Stock Keeping Unit';
$_['ms_account_product_upc']  = 'UPC';
$_['ms_account_product_upc_note'] = 'Universal Product Code';
$_['ms_account_product_ean'] = 'EAN';
$_['ms_account_product_ean_note'] = 'European Article Number';
$_['ms_account_product_jan'] = 'JAN';
$_['ms_account_product_jan_note'] = 'Japanese Article Number';
$_['ms_account_product_isbn'] = 'ISBN';
$_['ms_account_product_isbn_note'] = 'International Standard Book Number';
$_['ms_account_product_mpn'] = 'MPN';
$_['ms_account_product_mpn_note'] = 'Manufacturer Part Number';
$_['ms_account_product_manufacturer'] = 'Производитель';
$_['ms_account_product_manufacturer_note'] = '(Автозавершение)';
$_['ms_account_product_tax_class'] = 'Налоговый класс';
$_['ms_account_product_date_available'] = 'Дата поступления';
$_['ms_account_product_stock_status'] = 'Наличие';
$_['ms_account_product_subtract'] = 'Вычитать со склада';
$_['ms_account_product_customer_group'] = 'Группа покупателей';
$_['ms_account_product_msf_variations'] = "Вариации";
$_['ms_account_product_msf_variations_variants'] = "Варианты";
$_['ms_account_product_msf_variations_price'] = "Цена";
$_['ms_account_product_msf_variations_quantity'] = "Количество";
$_['ms_account_product_msf_variations_status'] = "Включить";
$_['ms_account_product_msf_variations_not_selected'] = "Пожалуйста, выберите хотя бы один вариант для %s";
$_['ms_account_product_msf_variations_select_category'] = "Пожалуйста, выберите категорию, чтобы получить все доступные товару опции.";
$_['ms_account_product_msf_variation_error_quantity'] = "Пожалуйста, выберите количество для вариантов продукта";
$_['ms_account_product_msf_variation_error_price_invalid'] = "Неверная цена для варианта товара %s";
$_['ms_account_product_msf_variation_error_price_empty'] = "Пожалуйста, укажите цену для варианта товара %s";
$_['ms_account_product_msf_variation_error_price_price_low'] = "Цена для варианта товара %s слишком низкая";
$_['ms_account_product_msf_variation_error_price_price_high'] = "Цена для варианта товара %s слишком высокая";
$_['ms_account_product_msf_variation_error_quantity'] = "Please, specify a quantity for product variant %s";

// Options
$_['ms_account_product_tab_options'] = 'Опции';
$_['ms_options_add'] = '+ Добавить опцию';
$_['ms_options_add_value'] = '+ Добавить значение';
$_['ms_options_price'] = 'Цена';
$_['ms_options_subtract'] = 'Вычесть';
$_['ms_options_quantity'] = 'Количество';
$_['ms_options_values'] = 'Значения';
$_['ms_options_required'] = 'Обязательное поле?';
$_['ms_options_remove'] = 'Удалить';

$_['ms_account_product_manufacturer'] = 'Производитель';
$_['ms_account_product_manufacturer_note'] = '(Автозавершение)';
$_['ms_account_product_tax_class'] = 'Налоговый класс';
$_['ms_account_product_date_available'] = 'Дата поступления';
$_['ms_account_product_stock_status'] = 'Наличие';
$_['ms_account_product_subtract'] = 'Вычитать со склада';

$_['ms_account_product_priority'] = 'Приоритет';
$_['ms_account_product_date_start'] = 'Дата начала';
$_['ms_account_product_date_end'] = 'Дата окончания';



// Account - Edit product
$_['ms_account_editproduct_heading'] = 'Редактировать товар';

// Account - Seller
$_['ms_account_sellerinfo_heading'] = 'Профиль продавца';
$_['ms_account_sellerinfo_breadcrumbs'] = 'Профиль продавца';
$_['ms_account_sellerinfo_nickname'] = 'Название магазина';
$_['ms_account_sellerinfo_nickname_note'] = 'Укажите название Вашего магазина';
$_['ms_account_sellerinfo_slogan'] = 'Слоган';
$_['ms_account_sellerinfo_slogan_note'] = 'Укажите Ваш девиз';
$_['ms_account_sellerinfo_description'] = 'Описание';
$_['ms_account_sellerinfo_description_note'] = 'Опишите Ваш магазин';
$_['ms_account_sellerinfo_avatar'] = 'Логотип';
$_['ms_account_sellerinfo_avatar_note'] = 'Выберите логотип';
$_['ms_account_sellerinfo_banner'] = 'Баннер';
$_['ms_account_sellerinfo_banner_note'] = 'Загрузите баннер, который будет отображаться на странице Вашего профиля';
$_['ms_account_sellerinfo_website'] = "Сайт";
$_['ms_account_sellerinfo_website_note'] = "Укажите ваш сайт";
$_['ms_account_sellerinfo_company'] = "Компания";
$_['ms_account_sellerinfo_company_note'] = "Укажите название вашей компании";
$_['ms_account_sellerinfo_phone'] = "Телефон";
$_['ms_account_sellerinfo_phone_note'] = "Укажите номер вашего телефона";
$_['ms_account_sellerinfo_fee_flat'] = 'Существует плата зе регистрацию <span>%s</span>, чтобы стать продавцом в %s.';
$_['ms_account_sellerinfo_fee_balance'] = 'Эта сумма будет вычтена из вашего начального баланса.';
$_['ms_account_sellerinfo_fee_pg'] = 'Ваши запросы на оплату можно найти в разделе Счета.';
$_['ms_account_sellerinfo_saved'] = 'Данные аккаунта продавца сохранены.';
$_['ms_account_sellerinfo_logo_note'] = "Выберите Ваш логотип (для счетов)";

$_['ms_account_status'] = 'Ваш статус аккаунта продавца: ';
$_['ms_account_status_tobeapproved'] = 'Ваша учетная запись будет активирована после подтверждения администратором.';
$_['ms_account_status_please_fill_in'] = 'Пожалуйста заполните данную форму, чтобы создать учетную запись продавца.';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'Активен';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'Неактивен';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'Отключен';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'Незавершённый';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'Удалён';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'Не оплачен регистрационный сбор';

// Account - Products
$_['ms_account_products_heading'] = 'Ваши товары';
$_['ms_account_products_breadcrumbs'] = 'Ваши товары';
$_['ms_account_products_product'] = 'Товар';
$_['ms_account_products_sales'] = 'Продаж';
$_['ms_account_products_earnings'] = 'Выручка';
$_['ms_account_products_status'] = 'Статус';
$_['ms_account_products_confirmdelete'] = 'Вы уверены, что хотите удалить свой товар?';
$_['ms_account_products_empty'] = "У Вас пока нет товаров.";

$_['ms_not_defined'] = 'Не определен';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'Активен';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'Неактивен';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'Отключен';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'Удалён';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'Не оплачен публикационный сбор';
$_['ms_product_status_' . MsProduct::STATUS_IMPORTED] = 'Импортировано';

$_['ms_import_text_results'] = 'Результаты импорта:';

// Account - Conversations and Messages
$_['ms_account_conversations'] = 'Диалоги';
$_['ms_account_messages'] = 'Сообщения';
$_['ms_sellercontact_success'] = 'Ваше сообщение было успешно отправлено';
$_['ms_account_conversations_empty'] = "У Вас пока нет сообщений.";

$_['ms_account_conversations_heading'] = 'Ваши Диалоги';
$_['ms_account_conversations_breadcrumbs'] = 'Ваши Диалоги';

$_['ms_account_conversations_status'] = 'Статус';
$_['ms_account_conversations_with'] = 'Диалог с';
$_['ms_account_conversations_title'] = 'Название';
$_['ms_account_conversations_type'] = 'Тип диалога';
$_['ms_account_conversations_textarea_placeholder'] = 'Введите ваше сообщение...';

$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_CUSTOMER] = 'покупатель';
$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_SELLER] = 'продавец';
$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_ADMIN] = 'администратор';


$_['ms_conversation_title_product'] = 'Запрос о товаре: %s';
$_['ms_conversation_title_order'] = 'Запрос о заказе: %s';
$_['ms_conversation_title'] = 'Запрос от %s';
$_['ms_conversation_customer_deleted'] = '*Покупатель удален*';

$_['ms_account_conversations_read'] = 'Прочтенное';
$_['ms_account_conversations_unread'] = 'Не прочтенное';

$_['ms_account_conversations_start_with_seller'] = 'Начать новый диалог с продавцом %s';
$_['ms_account_conversations_start_with_customer'] = 'Начать новый диалог с клиентом %s';

$_['ms_account_messages_heading'] = 'Сообщения';

$_['ms_message_text'] = 'Ваше сообщение';
$_['ms_post_message'] = 'Отправить';

$_['ms_customer_does_not_exist'] = 'Аккаунт заказчика удален';
$_['ms_error_empty_message'] = 'Сообщение не может быть пустым';

$_['ms_mail_subject_private_message'] = 'Получено новое личное сообщение';
$_['ms_mail_private_message'] = <<<EOT
Вы получили новое личное сообщение от %s!

%s

%s

Вы можете ответить в разделе сообщений Вашего аккаунта.
EOT;

$_['ms_mail_subject_order_updated'] = 'Ваш заказ #%s был обновлен %s';
$_['ms_mail_order_updated'] = <<<EOT
Ваш заказ %s был обновлен %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s

EOT;

$_['ms_mail_subject_seller_vote'] = 'Голосовать за продавца';
$_['ms_mail_seller_vote_message'] = 'Голосовать за продавца';

// Account - Transactions
$_['ms_account_transactions_heading'] = 'Ваши финансы';
$_['ms_account_transactions_breadcrumbs'] = 'Ваши финансы';
$_['ms_account_transactions_earnings'] = 'Ваша общая выручка:';
$_['ms_account_transactions_description'] = 'Описание';
$_['ms_account_transactions_amount'] = 'Сумма';
$_['ms_account_transactions_empty'] = "У Вас пока нет транзакций.";

// Payments
$_['ms_payment_payments_heading'] = 'Ваши платежи';
$_['ms_payment_payments'] = 'Платежи';

// Account - Orders
$_['ms_account_orders_heading'] = 'Ваши заказы';
$_['ms_account_orders_breadcrumbs'] = 'Ваши заказы';
$_['ms_account_orders_id'] = '#';
$_['ms_account_orders_customer'] = 'Покупатель';
$_['ms_account_orders_products'] = 'Товары';
$_['ms_account_orders_transactions'] = 'Транзакции';
$_['ms_account_orders_history'] = 'История статуса заказа';
$_['ms_account_orders_marketplace_history'] = 'История статусов торговой площадке';
$_['ms_account_orders_addresses'] = 'Адрес';
$_['ms_account_orders_total'] = 'Сумма';
$_['ms_account_orders_noorders'] = "У Вас пока нет заказов.";
$_['ms_account_orders_notransactions'] = 'Для этого заказа еще нет балансовых транзакций!';
$_['ms_account_orders_nohistory'] = 'Нет истории для этого заказа!';
$_['ms_account_orders_comment'] = 'Комментарий';
$_['ms_account_orders_add_comment'] = 'Сообщите клиенту, почему вы меняете статус заказа...';
$_['ms_account_orders_status_select_default'] = '-- Выберите новый статус --';
$_['ms_account_orders_change_status'] = 'Измените статус заказа';
$_['ms_account_orders_attachments']    = 'Вложения';
$_['ms_account_orders_store_commission_deducted'] = 'C учетом вычета комиссии торговой площадки';
/***** INSERTED *****/$_['ms_order_column_order_id'] = 'Order #';

/***** INSERTED *****/$_['ms_account_order_column_order_id'] = 'Order #';
/***** INSERTED *****/$_['ms_account_order_payment_status'] = 'Payment status';



$_['ms_account_order_information'] = 'Информация заказа';

// Account - Dashboard
$_['ms_account_dashboard_heading'] = 'Панель управления продавца';
$_['ms_account_dashboard_breadcrumbs'] = 'Панель управления продавца';
$_['ms_account_sellersetting_breadcrumbs'] = 'Настройки продавца';
$_['ms_account_dashboard_gross_sales'] = 'Суммарные продажи';
$_['ms_account_dashboard_total_current_balance'] = 'Текущий баланс';
$_['ms_account_dashboard_total_earnings'] = 'Общая выручка';
$_['ms_account_dashboard_total_orders'] = 'Всего заказов';
$_['ms_account_dashboard_total_views'] = 'Всего просмотров';
$_['ms_account_dashboard_sales_analytics'] = 'Аналитика продаж';
$_['ms_account_dashboard_top_selling_products'] = 'Лидеры продаж';
$_['ms_account_dashboard_top_viewed_products'] = 'Самые просматриваемые товары';
$_['ms_account_dashboard_top_rated_products'] = 'Топ товары по рейтингу';
$_['ms_account_dashboard_last_messages'] = 'Последние сообщения';
$_['ms_account_dashboard_last_reviews'] = 'Последние отзывы';
$_['ms_account_dashboard_last_orders'] = 'Последние заказы';
$_['ms_account_dashboard_last_invoices'] = 'Последние счета';
$_['ms_account_dashboard_msg_from_admin'] = 'Администратор';
$_['ms_account_dashboard_no_results_not_enough_data'] = 'Недостаточно данных.';
$_['ms_account_dashboard_no_results_no_product_sales'] = "У Вас пока нет продаж.";
$_['ms_account_dashboard_no_results_no_product_views'] = "У Вас пока нет просмотров.";
$_['ms_account_dashboard_no_results_no_data'] = 'Нет данных.';
$_['ms_account_dashboard_no_results_orders'] = 'У вас пока нет заказов.';
$_['ms_account_dashboard_no_results_reviews'] = 'У вас пока нет отзывов.';
$_['ms_account_dashboard_no_results_messages'] = 'У вас пока нет сообщений.';
$_['ms_account_dashboard_no_results_invoices'] = 'У вас пока нет счетов.';

$_['ms_account_dashboard_column_product'] = 'Товар';
$_['ms_account_dashboard_column_period'] = 'Длительность';
$_['ms_account_dashboard_column_total_views'] = 'Общее количество просмотров';
$_['ms_account_dashboard_column_total_sales'] = 'Общее количество продаж';
$_['ms_account_dashboard_column_gross'] = 'Брутто';
$_['ms_account_dashboard_column_rating'] = 'Рейтинг';
$_['ms_account_dashboard_column_from'] = 'От';
$_['ms_account_dashboard_column_message'] = 'Сообщение';
$_['ms_account_dashboard_column_date'] = 'Дата';
$_['ms_account_dashboard_column_comment'] = 'Комментарий';
$_['ms_account_dashboard_column_customer'] = 'Клиент';
$_['ms_account_dashboard_column_status'] = 'Статус';
$_['ms_account_dashboard_column_total'] = 'Всего';
$_['ms_account_dashboard_column_order'] = 'Заказ';
$_['ms_account_dashboard_column_description'] = 'Описание';
$_['ms_account_dashboard_column_type'] = 'Тип';
$_['ms_account_dashboard_column_conversation'] = 'Диалог';

// Account - Seller return
$_['ms_account_returns_heading'] = 'Возвраты товара';
$_['ms_account_returns_breadcrumbs'] = 'Панель управления продавца';
$_['ms_account_return_id'] = 'ID возврата';
$_['ms_account_return_order_id'] = 'ID заказа';
$_['ms_account_return_customer'] = 'Покупатель';
$_['ms_account_return_status'] = 'Статус';
$_['ms_account_return_date_added'] = 'Дата добавления';
$_['ms_account_return_customer_info'] = 'Информация о заказе';


// Account - Settings
$_['ms_seller_information'] = "Информация";
$_['ms_seller_first_name'] = "Имя";
$_['ms_seller_last_name'] = "Фамилия";
$_['ms_seller_email'] = "Эл. почта";
$_['ms_seller_account'] = "Аккаунт";
$_['ms_seller_address'] = "Адрес";
$_['ms_seller_full_name'] = "Полное имя";
$_['ms_seller_address1'] = "Адресная строка 1";
$_['ms_seller_address1_placeholder'] = 'Улица, а/я, название компании';
$_['ms_seller_address2'] = "Адресная строка 2";
$_['ms_seller_address2_placeholder'] = 'Квартира, здание, корпус, этаж';
$_['ms_seller_city'] = "Город";
$_['ms_seller_state'] = "Штат/Область/Регион";
$_['ms_seller_zip'] = "Почтовый индекс";
$_['ms_seller_country'] = "Страна";
$_['ms_seller_country_select'] = "-- Пожалуйста, выберите страну из списка --";
$_['ms_success_settings_saved'] = "Настройки успешно сохранены!";

// @todo 9.0: check usage and remove this
$_['ms_seller_company'] = 'Компания';
$_['ms_seller_website'] = 'Вебсайт';
$_['ms_seller_phone'] = 'Телефон';

// Account - Request withdrawal
$_['ms_account_withdraw_balance'] = 'Ваш текущий баланс:';
$_['ms_account_balance_reserved_formatted'] = '-%s ожидает вывода';
$_['ms_account_balance_waiting_formatted'] = '-%s период ожидания';

// Account - Stats
$_['ms_account_stats_heading'] = 'Статистика';
$_['ms_account_stats_breadcrumbs'] = 'Статистика';
$_['ms_account_stats_tab_summary'] = 'Сводка';
$_['ms_account_stats_tab_by_product'] = 'По товару';
$_['ms_account_stats_tab_by_year'] = 'По году';
$_['ms_account_stats_summary_comment'] = 'Ниже приводится сводка Ваших продаж';
$_['ms_account_stats_sales_data'] = 'Данные по продажам';
$_['ms_account_stats_number_of_orders'] = 'Количество продаж';
$_['ms_account_stats_total_revenue'] = 'Общая выручка';
$_['ms_account_stats_average_order'] = 'Средний чек';
$_['ms_account_stats_statistics'] = 'Статистика';
$_['ms_account_stats_grand_total'] = 'ИТОГО продажи';
$_['ms_account_stats_product'] = 'Товар';
$_['ms_account_stats_sold'] = 'Продано';
$_['ms_account_stats_total'] = 'Итог';
$_['ms_account_stats_this_year'] = 'Этот год';
$_['ms_account_stats_year_comment'] = '<span id="sales_num">%s</span> Продаж за указанный период';
$_['ms_account_stats_show_orders'] = 'Показать продажи с: ';
$_['ms_account_stats_month'] = 'Месяц';
$_['ms_account_stats_num_of_orders'] = 'Количество продаж';
$_['ms_account_stats_total_r'] = 'Общая выручка';
$_['ms_account_stats_average_order'] = 'Средний чек';
$_['ms_account_stats_today'] = 'Сегодня, ';
$_['ms_account_stats_yesterday'] = 'Вчера, ';
$_['ms_account_stats_daily_average'] = 'Ежедневно в среднем по ';
$_['ms_account_stats_date_month_format'] = 'F Y';
$_['ms_account_stats_projected_totals'] = 'Прогнозируемые итоги для ';
$_['ms_account_stats_grand_total_sales'] = 'ИТОГО продажи';

// Product page - Seller information
$_['ms_catalog_product_seller_information'] = 'Информация о продавце';
$_['ms_catalog_product_contact'] = 'Связаться с продавцом';

$_['ms_footer'] = '<br>MultiMerch Marketplace от <a href="https://multimerch.com/">multimerch.com</a>';

// Seller modules
$_['ms_newsellers_sellers'] = 'Новые продавцы';

$_['ms_topsellers_sellers'] = 'Лучшие продавцы';

$_['ms_listsellers_sellers'] = 'Продавцы';

// Catalog - Sellers list
$_['ms_catalog_sellers_heading'] = 'Все продавцы';
$_['ms_catalog_sellers_description'] = 'Список продавцов магазина';
$_['ms_sort_nickname_desc'] = 'Имя (Я - А)';
$_['ms_sort_nickname_asc'] = 'Имя (А - Я)';
$_['ms_catalog_sellers_map_view'] = 'Карта';

// Catalog - Seller profile page
$_['ms_catalog_sellers'] = 'Все продавцы';
$_['ms_catalog_sellers_empty'] = 'Еще нет продавцов.';
$_['ms_catalog_seller_profile'] = 'Просмотр профиля';
$_['ms_catalog_seller_profile_heading'] = 'Профиль %s';
$_['ms_catalog_seller_profile_breadcrumbs'] = 'Профиль %s';

$_['ms_catalog_seller_profile_total_sales'] = 'Продаж';
$_['ms_catalog_seller_profile_total_products'] = 'Товары';
$_['ms_catalog_seller_profile_view_products'] = 'Магазин';
$_['ms_catalog_seller_profile_featured_products'] = 'Недавно опубликованные товары';
$_['ms_catalog_seller_profile_search'] = 'Поиск всех товаров';
$_['ms_catalog_seller_profile_rating'] = 'Рейтинг';
$_['ms_catalog_seller_profile_total_reviews'] = '(%s %s)';


// Catalog - Seller's products list
$_['ms_catalog_seller_products_heading'] = "%s товаров";
$_['ms_catalog_seller_products_sales'] = "%s продаж";
$_['ms_catalog_seller_products_contact'] = "Связаться";
$_['ms_catalog_seller_products_profile'] = "Просмотреть профиль";
$_['ms_catalog_seller_products_breadcrumbs'] = "%s товаров";
$_['ms_catalog_seller_products_empty'] = "У этого продавца пока нет товаров!";

// Catalog - Seller contact dialog
$_['ms_sellercontact_signin'] = '<a href="%s">Авторизуйтесь</a> для связи с %s';
$_['ms_sellercontact_sendto'] = 'Отправить сообщение для %s';
$_['ms_sellercontact_text'] = 'Сообщение: ';
$_['ms_sellercontact_close'] = 'Закрыть';
$_['ms_sellercontact_send'] = 'Отправить';
$_['ms_sellercontact_success'] = 'Ваше сообщение было успешно отправлено';

// Product filters
$_['ms_entry_filter'] = 'Фильтры';
$_['ms_autocomplete'] = '(Автозавершение)';

// Related products
$_['ms_catalog_products_related_products']  = 'Похожие товары';

//Stores
$_['ms_catalog_products_stores']    = 'Магазины';

// Dimensions
$_['ms_catalog_products_measurements']    = 'Размеры';
$_['ms_catalog_products_size']          = 'Размер (Д x Ш x В)';
$_['ms_catalog_products_size_length']   = 'Длина';
$_['ms_catalog_products_size_width']    = 'Ширина';
$_['ms_catalog_products_size_height']   = 'Высота';
$_['ms_catalog_products_weight']        = 'Вес';

// Invoices
$_['heading_invoice_title']         = 'Счёт заказа';
$_['column_total_shipping'] = 'Сумма доставки';

// Validation
$_['ms_validate_default'] = 'Поле \'%s\' заполнено неверно';
$_['ms_validate_required'] = 'Поле \'%s\' обязательно для заполнения';
$_['ms_validate_alpha_numeric'] = 'Поле \'%s\' может содержать только буквенно-цифровые символы.';
$_['ms_validate_latin'] = 'Поле \'%s\' может содержать только латинские символы.';
$_['ms_validate_utf8'] = 'Поле \'%s\'  может содержать только символы UTF-8';
$_['ms_validate_max_len'] = "Поле '%s' %s должно быть '%s' или короче";
$_['ms_validate_min_len'] = "Поле '%s' %s должно быть '%s' или длиннее";
$_['ms_validate_phone_number'] = 'Поля \'%s\' не является действительным номером телефона';
$_['ms_validate_valid_url'] = 'Поле \'%s\' должно выть корректным URL';
$_['ms_validate_numeric'] = ' \'%s\' поле может содержать только числовые символы';
$_['ms_validate_email'] = 'Поле \'%s\' должно быть адресом электронной почты.';

$_['ms_validate_email_exists'] = 'Этот адрес электронной почты уже зарегистрирован';
$_['ms_validate_password_confirm'] = 'Поля паролей не совпадают';


//Order history

$_['ms_order_placed'] = 'Дата заказа:';
$_['ms_order_total'] = 'Всего:';
$_['ms_order_dispatch'] = 'Получатель:';
$_['ms_order_details'] = 'Детали заказа';
$_['ms_order_details_by_seller'] = 'Информация о заказе по продавцу';
$_['ms_order_products_by'] = 'Продавец:';
$_['ms_order_id'] = "Уникальный номер заказа продавца:";
$_['ms_order_current_status'] = "Текущий статус заказа продавца:";
$_['ms_order_status'] = "Seller's order status:";
$_['ms_order_status_initial'] = 'Заказ создан';
$_['ms_marketplace_order_status'] = 'Статус заказа торговой площадки';
$_['ms_order_status_history'] = "История статусов заказа продавца";
$_['ms_order_sold_by'] = 'Продал:';
$_['ms_order_buy_again'] = 'Купить ещё раз';
$_['ms_order_feedback'] = 'Оставить отзыв';
$_['ms_order_return'] = 'Возврат';

$_['ms_order_success_suborders_modified'] = 'Успех: Вы изменили заказ #%s и связанный с ним предзаказ на %s!';
$_['ms_order_success_transactions_created'] = 'Транзакция #%s была создана.';

/***** INSERTED *****/$_['ms_payment_status'] = 'Payment status';
/***** INSERTED *****/$_['ms_payment_history'] = 'Payment history';
/***** INSERTED *****/$_['ms_payment_method'] = 'Payment method';
/***** INSERTED *****/$_['ms_order_status'] = 'Order status';

// Questions
$_['mm_questions_tab'] = 'Вопросы (%s)';
$_['mm_question_title'] = 'Вопросы';
$_['mm_question'] = 'Вопрос';
$_['mm_question_posted_by'] = 'Опубликовал:';
$_['mm_question_answers'] = 'Ответы';
$_['mm_question_answer_by'] = 'Ответил:';
$_['mm_question_no_answers'] = 'На этот вопрос еще нет ответов';
$_['mm_question_no_questions'] = 'Для этого товара ещё нет вопросов';
$_['mm_question_write_answer'] = 'Ответить на этот вопрос';
$_['mm_question_submit'] = 'Отправить';
$_['mm_question_ask'] = 'Задайте Ваш вопрос о данном товаре';
$_['mm_question_signin'] = 'Пожалуйста, авторизуйтесь чтобы задать вопрос';
$_['mm_question_answers_textarea_placeholder'] = 'Ответ на вопрос';
$_['mm_question_seller_answer'] = "Ответ продавца";

$_['question_title'] = 'Вопросы';
$_['posted_by'] = 'Опубликовал:';
$_['answer_by'] = 'Ответил:';
$_['no_answers'] = 'Нет ответов =(';
$_['write_answer'] = 'Напишите ваш ответ';

// Customer feedback
$_['ms_customer_product_rate_heading'] = 'Оцените ваш опыт';
$_['ms_customer_product_rate_stars_label'] = 'Пожалуйста, оцените эту покупку';
$_['ms_customer_product_rate_comments'] = 'Оставьте комментарии';
$_['ms_customer_product_rate_comments_placeholder'] = 'Пожалуйста, оставьте комментарии о Вашем опыте покупок с данным продавцом';
$_['ms_customer_product_rate_btn_submit'] = 'Отправить отзыв';
$_['ms_customer_product_rate_drag_drop_here'] = 'Для загрузки перетяните изображение сюда';
$_['ms_customer_product_rate_characters_left'] = 'символов осталось';
$_['ms_customer_product_rate_drag_drop_allowed'] = 'Разрешеные расширения: %s';
$_['ms_customer_product_rate_form_error'] = 'Все поля должны быть заполнены!';

// Reviews
$_['mm_review_comments_title'] = 'Отзывы покупателей';
$_['mm_review_rating_summary'] = '%s из 5 (%s %s)';
$_['mm_review_rating_review'] = 'отзыв';
$_['mm_review_rating_reviews'] = 'отзывов';
$_['mm_review_stats_stars'] = '%s зв.';
$_['mm_review_no_reviews'] = 'Ещё нет отзывов!';
$_['mm_review_seller_profile_history'] = 'История недавних отзывов';
$_['mm_review_seller_profile_history_positive'] = 'Положительные (4-5 звёзд)';
$_['mm_review_seller_profile_history_neutral'] = 'Нейтральные (3 звезды)';
$_['mm_review_seller_profile_history_negative'] = 'Отрицательные (1-2 звезды)';
$_['mm_review_one_month'] = '1 месяц';
$_['mm_review_three_months'] = '3 месяца';
$_['mm_review_six_months'] = '6 месяцев';
$_['mm_review_twelve_months'] = '12 месяцев';
$_['mm_review_submit_success'] = 'Спасибо, что оставили ваш отзыв!';
$_['mm_review_seller_response'] = "Ответ продавца";
$_['mm_review_comments'] = 'Комментарий';
$_['mm_review_comments_placeholder'] = 'Ответить на этот отзыв';
$_['mm_review_no_comments'] = 'Вы еще не ответили на этот отзыв.';
$_['mm_review_comments_success_added'] = 'Ваш комментарий был успешно отправлен!';
$_['mm_review_comments_error_signin'] = 'Пожалуйста, войдите на сайт, чтобы оставить комментарий!';
$_['mm_review_comments_error_review_id'] = 'Ошибка: Не указано id отзыва!';
$_['mm_review_comments_error_notext'] = 'Ошибка: Вы должны ввести сообщение!';
$_['mm_review_comments_textarea_placeholder'] = "Укажите свой ответ на комментарий клиента здесь. Это будет отображаться на торговой площадке.";
$_['mm_review_comments_post_message'] = 'Отправить';
$_['mm_review_total_reviews_empty'] = 'Просмотров нет';
$_['mm_review_total_reviews_one'] = '%s просмотров';
$_['mm_review_total_reviews_multiple'] = '%s просмотров';

// Seller > Account-Product > Shipping
$_['ms_account_product_tab_shipping'] = 'Доставка';
$_['ms_account_product_shipping_from'] = 'Доставка из';
$_['ms_account_product_shipping_free'] = 'Бесплатная доставка';
$_['ms_account_product_shipping_free_note'] = 'Стоимость доставки не будет учтена.';
$_['ms_account_product_shipping_processing_time'] = 'Время обработки заказа';
$_['ms_account_product_shipping_locations_to'] = 'Доставка в';
$_['ms_account_product_shipping_locations_destination'] = 'Место назначения';
$_['ms_account_product_shipping_locations_company'] = 'Способ доставки';
$_['ms_account_product_shipping_locations_delivery_time'] = 'Время доставки';
$_['ms_account_product_shipping_locations_cost'] = 'Стоимость';
$_['ms_account_product_shipping_locations_cost_fixed_pwu'] = 'Цена (фиксированая + за единицу веса)';
$_['ms_account_product_shipping_locations_additional_cost'] = 'Дополнительная позиция';
$_['ms_account_product_shipping_locations_add_btn'] = '+ Добавить местоположение';
$_['ms_account_product_shipping_elsewhere'] = 'По всему миру';

$_['ms_account_product_shipping_combined_enabled'] = 'Комбинированная доставка включена, стоимость доставки для этого товара будет рассчитана автоматически.';
$_['ms_account_product_shipping_combined_override'] = 'Пренебречь правилами комбинированной доставки';

// Seller > Account-Settings > Shipping
$_['ms_account_settings_shipping_settings'] = 'Настройки доставки';
$_['ms_account_settings_shipping_weight'] = 'Вес (%s)';
$_['ms_account_settings_shipping_comment'] = 'Комментарий';
$_['ms_account_settings_shipping_add'] = '+ Добавить способ доставки';

// Seller > Account-Settings > Payments
$_['ms_account_setting_payments_tab'] = 'Платежи';

// Product page > Shipping
$_['mm_product_shipping_title'] = 'Информация о доставке';
$_['mm_product_shipping_free'] = 'Бесплатная доставка';
$_['mm_product_shipping_from_country'] = 'Страна доставки';
$_['mm_product_shipping_processing_time'] = 'Время обработки заказа';
$_['mm_product_shipping_processing_days'] = '%s %s';
$_['mm_product_shipping_locations_note'] = 'Время доставки может различаться, особенно во время загруженных периодов.';
$_['mm_product_shipping_not_specified'] = 'Продавец не указал информацию о доставке для данного товара!';
$_['mm_product_shipping_digital_product'] = 'Товар цифровой и не нуждается в доставке!';

// Checkout > Payment method
/***** INSERTED *****/$_['mm_checkout_payment_method_add_comments'] = "Add comments for sellers";
/***** INSERTED *****/$_['mm_checkout_payment_method_suborder_comment_title'] = "Comments about order by %s";

// Checkout > Shipping
$_['mm_not_specified'] = 'Не указана';
$_['mm_checkout_shipping_ew_location_delivery_time_name'] = 'Время доставки зависит от местоположения';
$_['mm_checkout_shipping_delivery_details_title'] = 'Адрес доставки';
$_['mm_checkout_shipping_delivery_details_change'] = 'Изменить';
$_['mm_checkout_shipping_products_title'] = 'Выберите <b>предпочитаемый способ доставки</b> для каждого товара';
$_['mm_checkout_shipping_products_price'] = 'Цена: ';
$_['mm_checkout_shipping_products_quantity'] = 'Количество: ';
$_['mm_checkout_shipping_products_seller'] = 'Продавец: ';
$_['mm_checkout_shipping_method_title'] = 'Пожалуйста, выберите способ доставки';
$_['mm_checkout_shipping_method_title_shot'] = 'Способ доставки';
$_['mm_checkout_shipping_method_free'] = 'Бесплатная доставка';
$_['mm_checkout_shipping_not_required'] = 'Доставка не требуется.';
$_['mm_checkout_shipping_digital_products'] = 'Цифровой товар. Доставка не требуется';
$_['mm_checkout_shipping_not_available'] = 'Товар не может быть доставлен по вашему адресу и будет удален из корзины!';
$_['mm_checkout_shipping_total'] = 'Сумма доставки: ';
$_['mm_checkout_shipping_product_delete_warning'] = 'Товары, которые не могут быть доставлены по вашему адресу, будут удалены из корзины.';
$_['mm_checkout_shipping_no_selected_methods'] = 'Ошибка: Вы должны выбрать способ доставки для каждого товара!';

$_['mm_checkout_shipping_error_maxweight_exceeded'] = 'Общий вес товаров %s (%s) превышает маскимально доступный вес для этого продавца. (%s). Пожалуйста удалите товары %s <a href="%s">из Вашей корзины</a> или товары будут удаленны автоматически.';
$_['mm_checkout_shipping_error_minweight_not_exceeded'] = 'Общий вес товаров %s (%s) в Вашей корзине не достигает минимального веса, разрешенного этим продавцом (%s). Пожалуйста, добавьте другие товары %s <a href="%s">из его каталога</a> в Вашу корзину или товары будут удалены автоматически.';
$_['mm_checkout_shipping_error_no_available_methods'] = 'Ошибка: для некоторых товаров из Вашей корзины отсутствуют способы доставки!';
$_['mm_checkout_shipping_error_shipping_address'] = 'Ошибка: не задан адрес доставки!';

// Account > Order history
$_['mm_account_order_shipping_cost'] = 'Стоимость доставки';
$_['mm_account_order_shipping_total'] = 'Доставка всего';
$_['mm_account_order_shipping_via'] = 'Доставка %s';

// Payments
$_['ms_pg_new_payment'] = 'Новый платеж';
$_['ms_pg_payment_method'] = 'Способ оплаты';
$_['ms_pg_payment_requests'] = 'Счета';
$_['ms_pg_payment_form_select_method'] = 'Выбрать способ оплаты';

$_['ms_pg_payment_type_' . MsPgPayment::TYPE_PAID_REQUESTS] = 'Оплаченные счета';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_SALE] = 'Продаж';

$_['ms_pg_payment_status_' . MsPgPayment::STATUS_INCOMPLETE] = '<p style="color: red">Ожидается</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_INCOMPLETE] = 'Ожидается';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_COMPLETE] = '<p style="color: green">Завершен</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_COMPLETE] = 'Завершен';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = '<p style="color: blue">Ожидает подтверждения</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = 'Ожидает подтверждения';

$_['ms_pg_payment_error_not_available'] = 'Ни один из способов оплаты на данный момент не доступен';
$_['ms_pg_payment_error_no_method'] = 'Ошибка: выберите способ оплаты!';
$_['ms_pg_payment_error_receiver_data'] = 'Ошибка: нет данных получателя!';
$_['ms_pg_payment_error_sender_data'] = 'Ошибка: нет данных отправителя!';

// Combined shipping
$_['ms_account_setting_ssm_title'] = 'Настройки комбинированной доставки';
$_['ms_account_setting_ssm_success'] = 'Вы успешно изменили настройки комбинированной доставки!';
$_['ms_account_setting_ssm_error_data'] = 'Ошибка: Нет данных для обработки!';
$_['ms_account_setting_ssm_error_methods'] = 'Ошибка: Не указан ни один способ доставки!';
$_['ms_account_setting_ssm_error_location'] = 'Вы должны выбрать местоположение!';
$_['ms_account_setting_ssm_error_method'] = 'Вы должны выбрать способ доставки!';
$_['ms_account_setting_ssm_error_delivery_time'] = 'Вы должны выбрать время доставки!';
$_['ms_account_setting_ssm_error_weight'] = 'Необходимо указать диапазон веса!';
$_['ms_account_setting_ssm_error_cost'] = 'Необходимо указать стоимость!';
$_['ms_account_setting_ssm_error_no_sm'] = 'Ни один из способов доставки на данный момент не доступен';
$_['ms_account_setting_ssm_error_no_dt'] = 'Ни одного срока доставки сейчас не указано!';
$_['ms_account_setting_ssm_error_no_gz'] = 'Ни одной адресной зоны сейчас не указано!';

$_['ms_product_ssm_weight_range_template'] = '%s - %s %s';

// Seller attibutes
$_['ms_account_attribute_heading'] = 'Ваши атрибуты';
$_['ms_account_attribute_breadcrumbs'] = 'Ваши атрибуты';
$_['ms_account_attribute'] = 'Аттрибут';
$_['ms_account_attributes'] = 'Аттрибуты';
$_['ms_account_attribute_manage'] = 'Управление атрибутами';
$_['ms_account_attribute_new'] = 'Новый атрибут';
$_['ms_account_attribute_name'] = 'Название';
$_['ms_account_newattribute_heading'] = 'Новый Атрибут';
$_['ms_account_editattribute_heading'] = 'Изменить Атрибут';
$_['ms_account_attributes_empty'] = "У Вас пока нет атрибутов.";

$_['ms_account_attribute_name_note'] = 'Укажите имя вашего атрибута';
$_['ms_account_attribute_attr_group_note'] = 'Приложить ваш атрибут к существующей группы атрибутов или <a href="%s">создать свою собственную группу</a>';
$_['ms_account_attribute_sort_order_note'] = 'Укажите порядок сортировки для вашего атрибута';

$_['ms_success_attribute_created'] = 'Атрибут успешно создан! Вы сможете использовать его, как только он будет утвержден администратором торговой площадки.';
$_['ms_success_attribute_updated'] = 'Атрибут обновлен!';
$_['ms_success_attribute_deleted'] = 'Атрибут удалено!';
$_['ms_success_attribute_activated'] = 'Атрибут включен!';
$_['ms_success_attribute_deactivated'] = 'Атрибут деактивировано!';

$_['ms_error_attribute_assigned_to_products'] = 'Внимание: Этот атрибут не может быть удален поскольку в настоящее время он назначен на %s товары!';
$_['ms_error_attribute_id'] = 'id атрибута не установлено!';

// Seller attibute groups
$_['ms_account_attribute_group_heading'] = 'Ваши Группы Атрибутов';
$_['ms_account_attribute_group_breadcrumbs'] = 'Ваши группы атрибутов';
$_['ms_account_attribute_group'] = 'Группа атрибута';
$_['ms_account_attribute_groups'] = 'Группы атрибутов';
$_['ms_account_attribute_group_new'] = 'Новая группа атрибута';
$_['ms_account_attribute_group_name'] = 'Название';
$_['ms_account_newattributegroup_heading'] = 'Новая группа атрибута';
$_['ms_account_editattributegroup_heading'] = 'Изменить группу атрибута';
$_['ms_account_attribute_groups_empty'] = "У Вас пока нет групп атрибутов.";

$_['ms_account_attribute_group_name_note'] = 'Укажите имя вашей группы атрибута';
$_['ms_account_attribute_group_sort_order_note'] = 'Укажите порядок сортировки вашей группы атрибутов';

$_['ms_success_attribute_group_created'] = 'Группа атрибутов успешно создана! Вы сможете использовать ее, как только она будет утверждено администратором торговой площадки.';
$_['ms_success_attribute_group_updated'] = 'Группа атрибутов обновлена!';
$_['ms_success_attribute_group_deleted'] = 'Группа атрибутов удалена!';
$_['ms_success_attribute_group_activated'] = 'Группа атрибутов активирована!';
$_['ms_success_attribute_group_deactivated'] = 'Группа атрибутов деактивирована!';

$_['ms_error_attribute_group_assigned_to_attributes'] = 'Внимание: Эта группа атрибутов не может быть удалена поскольку в настоящее время она назначена на %s атрибуты!';
$_['ms_error_attribute_group_id'] = 'id группы атрибута не установлено!';

$_['ms_seller_attribute_status_' . MsAttribute::STATUS_DISABLED] = 'Отключен';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_APPROVED] = 'Утвержденный';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_ACTIVE] = 'Активен';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_INACTIVE] = 'Неактивен';

// Seller options
$_['ms_account_option_heading'] = 'Ваши Опции';
$_['ms_account_option_breadcrumbs'] = 'Ваши Опции';
$_['ms_account_option'] = 'Опция';
$_['ms_account_options'] = 'Опции';
$_['ms_account_option_value'] = 'Значение опции';
$_['ms_account_option_values'] = 'Значение опций';
$_['ms_account_option_new'] = 'Новая опция';
$_['ms_account_option_name'] = 'Название';
$_['ms_account_option_manage'] = 'Управление опциями';
$_['ms_account_newoption_heading'] = 'Новая опция';
$_['ms_account_editoption_heading'] = 'Редактирования опции';
$_['ms_account_options_empty'] = "У Вас пока нет опций.";

$_['ms_account_option_name_note'] = 'Укажите имя вашей опции';
$_['ms_account_option_sort_order_note'] = 'Укажите порядок сортировки вашей опции';
$_['ms_account_option_type_note'] = 'Укажите тип для вашей опции';

$_['ms_success_option_created'] = 'Опция успешно создана! Вы сможете использовать ее, как только она будет утверждена администратором торговой площадки.';
$_['ms_success_option_updated'] = 'Опция отредактирована!';
$_['ms_success_option_deleted'] = 'Опция удалена!';
$_['ms_success_option_activated'] = 'Опция активирована!';
$_['ms_success_option_deactivated'] = 'Опция деактивирована!';

$_['ms_error_option_assigned_to_products'] = 'Внимание: Эта опция не может быть удалена поскольку в настоящее время она назначена на %s товары!';
$_['ms_error_option_id'] = 'Id опции не установлено!';
$_['ms_error_option_values'] = 'Значение опции обязательное!';

$_['ms_seller_option_status_' . MsOption::STATUS_DISABLED] = 'Отключено';
$_['ms_seller_option_status_' . MsOption::STATUS_APPROVED] = 'Утверждено';
$_['ms_seller_option_status_' . MsOption::STATUS_ACTIVE] = 'Активна';
$_['ms_seller_option_status_' . MsOption::STATUS_INACTIVE] = 'Неактивна';

$_['ms_account_option_type_choose'] = 'Выбрать';
$_['ms_account_option_type_input'] = 'Ввод';
$_['ms_account_option_type_file'] = 'Файл';
$_['ms_account_option_type_date'] = 'Дата создания';
$_['ms_account_option_type_select'] = 'Выбрать';
$_['ms_account_option_type_radio'] = 'Радио';
$_['ms_account_option_type_checkbox'] = 'Флажок';
$_['ms_account_option_type_text'] = 'Текст';
$_['ms_account_option_type_textarea'] = 'Текстовая область';
$_['ms_account_option_type_file'] = 'Файл';
$_['ms_account_option_type_time'] = 'Время';
$_['ms_account_option_type_datetime'] = 'Дата и время';

// Seller categories
$_['ms_account_category_heading'] = 'Ваши Категории';
$_['ms_account_category_breadcrumbs'] = 'Ваши Категории';
$_['ms_account_category'] = 'Категория';
$_['ms_account_category_primary'] = 'Первичная категория';
$_['ms_account_categories'] = 'Категории';
$_['ms_account_category_manage'] = 'Управление категориями';
$_['ms_account_category_new'] = 'Новая категория';
$_['ms_account_newcategory_heading'] = 'Новая категория';
$_['ms_account_editcategory_heading'] = 'Редактировать Категорию';
$_['ms_account_categories_empty'] = "У Вас пока нет категорий.";

$_['ms_account_category_name'] = 'Имя';
$_['ms_account_category_name_note'] = 'Укажите название категории';
$_['ms_account_category_description'] = 'Описание';
$_['ms_account_category_description_note'] = 'Опишите свою категорию';

$_['ms_account_category_additional_data'] = 'Дополнительная информация';
$_['ms_account_category_parent'] = 'Родитель';
$_['ms_account_category_no_parent'] = '-- Никто --';
$_['ms_account_category_parent_note'] = 'Укажите родительскую категорию, чтобы построить свою собственную структуру категорий';
$_['ms_account_category_filters'] = 'Фильтры';

$_['ms_account_category_search_optimization'] = 'Поисковая оптимизация';
$_['ms_account_category_meta_title'] = 'Мета название';
$_['ms_account_category_meta_title_note'] = 'Мета Название - это то, что будет отображаться в заголовке вашей страницы с перечнем категорий';
$_['ms_account_category_meta_description'] = 'Мета описание';
$_['ms_account_category_meta_description_note'] = 'Мета Описание используется поисковыми системами для описания вашей категории в результатах поиска. Без форматирования';
$_['ms_account_category_meta_keyword'] = 'Мета ключевые слова';
$_['ms_account_category_meta_keyword_note'] = 'Мета Ключевые слова могут быть использованы в поисковых системах, чтобы определить, о чем ваш список категорий';
$_['ms_account_category_seo_keyword'] = 'SEO URL';
$_['ms_account_category_seo_keyword_note'] = "Это будет отображаться в URL вашей страницы категории. Не используйте пробелы и специальные символы";

$_['ms_account_category_image'] = 'Изображение';
$_['ms_account_category_sort_order'] = 'Порядок сортировки';
$_['ms_account_category_sort_order_note'] = 'Укажите порядок сортировки для вашей категории';

$_['ms_success_category_created'] = 'Категория успешно создана! Вы сможете использовать её, как только она будет одобрена администратором торговой площадки.';
$_['ms_success_category_updated'] = 'Категория обновлена!';
$_['ms_success_category_deleted'] = 'Категорию удалено!';
$_['ms_success_category_activated'] = 'Категория активирована!';
$_['ms_success_category_deactivated'] = 'Категория выключена!';

$_['ms_seller_category_status_' . MsCategory::STATUS_DISABLED] = 'Отключено';
$_['ms_seller_category_status_' . MsCategory::STATUS_ACTIVE] = 'Активна';
$_['ms_seller_category_status_' . MsCategory::STATUS_INACTIVE] = 'Неактивна';

// Seller reviews management
$_['ms_account_reviews'] = 'Отзывы';
$_['ms_account_review_heading'] = 'Отзывы';
$_['ms_account_review_breadcrumbs'] = 'Отзывы';
$_['ms_account_review_manage'] = 'Управление отзывами';
$_['ms_account_review_column_product'] = 'Товар';
$_['ms_account_review_column_rating'] = 'Рейтинг';
$_['ms_account_review_column_comment'] = 'Комментарий';
$_['ms_account_review_column_date_added'] = 'Отправлено';
$_['ms_account_reviews_empty'] = "У Вас пока нет отзывов.";

$_['ms_account_editreview_heading'] = "Отзывы клиентов о вашем товаре";
$_['ms_account_editreview_product'] = 'Товар';
$_['ms_account_editreview_order'] = 'ID Заказа';
$_['ms_account_editreview_customer'] = 'Клиент';
$_['ms_account_editreview_review'] = 'Отзыв';
$_['ms_account_editreview_your_response'] = 'Ваш ответ';
$_['ms_account_editreview_response'] = 'Ответ';
$_['ms_account_editreview_rating'] = 'Рейтинг';
$_['ms_account_editreview_customer_images'] = "Изображения клиента";
$_['ms_account_editreview_images'] = 'Изображения';

$_['ms_success_review_updated'] = 'Отзыв обновлен!';
$_['ms_success_review_deleted'] = 'Отзыв удален!';
$_['ms_error_review_id'] = 'Отзыв не найдено!';

$_['ms_seller_review_status_' . MsCategory::STATUS_ACTIVE] = 'Активен';
$_['ms_seller_review_status_' . MsCategory::STATUS_INACTIVE] = 'Неактивен';

// Seller questions management
$_['ms_account_questions'] = 'Вопросы';
$_['ms_account_question_heading'] = 'Вопросы';
$_['ms_account_question_breadcrumbs'] = 'Вопросы';
$_['ms_account_question_manage'] = 'Управление вопросами';
$_['ms_account_question_column_product'] = 'Товар';
$_['ms_account_question_column_customer'] = 'Покупатель';
$_['ms_account_question_column_answer'] = 'Ответ';
$_['ms_account_question_column_date_added'] = 'Отправлено';
$_['ms_account_questions_empty'] = "У Вас пока нет вопросов.";

$_['ms_account_editquestion_heading'] = 'Вопрос по вашему товаре';
$_['ms_account_editquestion_product'] = 'Товар';
$_['ms_account_editquestion_customer'] = 'Покупатель';
$_['ms_account_editquestion_question'] = 'Вопрос';
$_['ms_account_editquestion_your_answer'] = 'Ваш ответ';
$_['ms_account_editquestion_answer'] = 'Ответ';
$_['ms_account_question_no_answers'] = 'Еще не ответили';
$_['ms_questions_customer_deleted'] = '*Покупатель удалил*';

$_['ms_success_question_submitted'] = 'Ваш вопрос успешно отправлен!';
$_['ms_success_question_updated'] = 'Вопрос обновлен!';
$_['ms_success_question_deleted'] = 'Вопрос удален!';
$_['ms_success_question_answered'] = 'Вопрос отвечен!';
$_['ms_error_question_id'] = 'Вопрос не найден!';
$_['ms_error_question_text'] = 'Ваш вопрос должен вводиться в текстовом поле!';

// Reports
$_['ms_report_guest_checkout'] = 'Гость';
$_['ms_report_report'] = 'Отчеты';
$_['ms_report_sales'] = 'Продажи';
$_['ms_report_sales_list'] = 'Список продаж';
$_['ms_report_sales_day'] = 'Продажи за день';
$_['ms_report_sales_month'] = 'Продажи за месяц';
$_['ms_report_sales_product'] = 'Продажи по товарам';
$_['ms_report_finances'] = 'Финансы';
$_['ms_report_finances_transaction'] = 'Транзакции';
$_['ms_report_finances_payment'] = 'Платежи';
$_['ms_report_finances_payout'] = 'Выплаты';
$_['ms_report_sales_list_empty'] = "У Вас пока нет продаж.";
$_['ms_report_finances_transaction_empty'] = "У Вас пока нет транзакций.";
$_['ms_report_finances_payment_empty'] = "У Вас пока нет платежей.";
$_['ms_report_finances_payout_empty'] = "У Вас пока нет выплат.";

$_['ms_report_column_date'] = 'Дата';
$_['ms_report_column_date_month'] = 'Месяц';
$_['ms_report_column_order'] = 'Заказ';
$_['ms_report_column_product'] = 'Товар';
$_['ms_report_column_gross'] = 'Сумма';
$_['ms_report_column_net_marketplace'] = 'Выручка торговой площадки';
$_['ms_report_column_net_seller'] = 'Выручка продавца';
$_['ms_report_column_tax'] = 'Налог';
$_['ms_report_column_shipping'] = 'Доставка';
$_['ms_report_column_total'] = 'Сумма';
$_['ms_report_column_total_sales'] = 'Всего продаж';
$_['ms_report_column_product'] = 'Товар';
$_['ms_report_column_transaction'] = 'Транзакция';
$_['ms_report_column_description'] = 'Описание';
$_['ms_report_column_payment'] = 'Платеж';
$_['ms_report_column_payout'] = 'Выплата';
$_['ms_report_column_method'] = 'Способ оплаты';

$_['ms_report_date_range_today'] = 'Cегодня';
$_['ms_report_date_range_yesterday'] = 'Вчера';
$_['ms_report_date_range_last7days'] = 'Последние 7 дней';
$_['ms_report_date_range_last30days'] = 'Последние 30 дней';
$_['ms_report_date_range_thismonth'] = 'Этот месяц';
$_['ms_report_date_range_lastmonth'] = 'Прошлый месяц';
$_['ms_report_date_range_custom'] = 'Пользовательский диапазон';
$_['ms_report_date_range_apply'] = 'Применить';
$_['ms_report_date_range_cancel'] = 'Отмена';

$_['ms_report_date_range_day_mo'] = 'Пн';
$_['ms_report_date_range_day_tu'] = 'Вт';
$_['ms_report_date_range_day_we'] = 'Ср';
$_['ms_report_date_range_day_th'] = 'Чт';
$_['ms_report_date_range_day_fr'] = 'Пт';
$_['ms_report_date_range_day_sa'] = 'Сб';
$_['ms_report_date_range_day_su'] = 'Вс';

$_['ms_report_date_range_month_jan'] = 'Январь';
$_['ms_report_date_range_month_feb'] = 'Февраль';
$_['ms_report_date_range_month_mar'] = 'Март';
$_['ms_report_date_range_month_apr'] = 'Апрель';
$_['ms_report_date_range_month_may'] = 'Май';
$_['ms_report_date_range_month_jun'] = 'Июнь';
$_['ms_report_date_range_month_jul'] = 'Июль';
$_['ms_report_date_range_month_aug'] = 'Август';
$_['ms_report_date_range_month_sep'] = 'Сентябрь';
$_['ms_report_date_range_month_oct'] = 'Октябрь';
$_['ms_report_date_range_month_nov'] = 'Ноябрь';
$_['ms_report_date_range_month_dec'] = 'Декабрь';

// Product custom fields
$_['ms_account_product_tab_custom_fields'] = 'Дополнительное';
$_['ms_account_product_cf_file_allowed_ext'] = 'Загрузить файлы. Допустимые расширения: %s';
$_['ms_account_product_cf_file_uploaded'] = 'Файл был успешно загружен!';
$_['ms_account_product_error_field_required'] = "'%s' обязательное для заполнения поле!";
$_['ms_account_product_error_field_validation'] = "'%s' файл не подтвержден! Образец: %s";

$_['ms_account_product_text_placeholder'] = 'Поле, обязательное для заполнения!';
$_['ms_account_product_textarea_placeholder'] = 'Поле, обязательное для заполнения!';
$_['ms_account_product_date_placeholder'] = 'Поле, обязательное для заполнения!';

// Discount coupons
$_['ms_seller_account_coupon'] = "Купоны";
$_['ms_seller_account_coupon_breadcrumbs'] = "Купоны";
$_['ms_seller_account_newcoupon_heading'] = "Создать новый скидочный купон";
$_['ms_seller_account_editcoupon_heading'] = "Изменить скидочный купон";
$_['ms_seller_account_coupon_empty'] = "У Вас пока нет купонов.";
$_['ms_seller_account_coupon_success_deleted'] = "Скидочный купон был успешно удален!";
$_['ms_seller_account_coupon_error_id'] = "Ошибка удаления скидочного купона: введен неправильный id!";
$_['ms_seller_account_coupon_manage'] = "Управление скидочными купонами";
$_['ms_seller_account_coupon_create'] = "Создать купон";
$_['ms_seller_account_coupon_general'] = "Свойства купонов";
$_['ms_seller_account_coupon_name'] = "Название";
$_['ms_seller_account_coupon_name_note'] = "Назовите этот купон (не отображается публично)";
$_['ms_seller_account_coupon_description'] = "Описание";
$_['ms_seller_account_coupon_description_note'] = "Опишите этот купон";
$_['ms_seller_account_coupon_code'] = "Код";
$_['ms_seller_account_coupon_code_note'] = "Укажите код скидки, который будет использоваться клиентом, например 20PERCENT ИЛИ BLACKFRIDAY. Только буквенно-цифровые символы, не более 12 символов";
$_['ms_seller_account_coupon_value'] = "Значение";
$_['ms_seller_account_coupon_value_note'] = "Укажите значение для этого купона - это может быть фиксированная сумма или процент от стоимости корзины";
$_['ms_seller_account_coupon_uses'] = "Использовано раз";
$_['ms_seller_account_coupon_max_uses'] = "Максимальное количество использований";
$_['ms_seller_account_coupon_max_uses_total'] = "На купон";
$_['ms_seller_account_coupon_max_uses_customer'] = "На одного клиента";
$_['ms_seller_account_coupon_max_uses_note'] = "Ограничьте максимальное количество раз использований этого купона. Оставьте пустым для неограниченного количества раз.";
$_['ms_seller_account_coupon_date_period'] = "Диапазон дат";
$_['ms_seller_account_coupon_date_period_note'] = "Укажите даты начала и окончания вашей дисконтной кампании. Это сделает купон доступным только в течение этого периода. Оставьте поле пустым, чтобы ограничения по дате отсутствовали";
$_['ms_seller_account_coupon_date_placeholder'] = "Выберите дату";
$_['ms_seller_account_coupon_date_start'] = "Дата начала";
$_['ms_seller_account_coupon_date_end'] = "Дата конца";
$_['ms_seller_account_coupon_min_order_total'] = "Минимальная стоимость корзины";
$_['ms_seller_account_coupon_min_order_total_note'] = "Укажите минимальную общую стоимость товаров в корзине покупателя после которой купон можно будет использовать. Оставьте поле пустым, чтобы ограничения по минимальной стоимости корзины отсутствовали";
$_['ms_seller_account_coupon_login_required'] = "Вход в систему";
$_['ms_seller_account_coupon_login_required_note'] = "Укажите, должен ли пользователь войти в систему, чтобы использовать этот купон";
$_['ms_seller_account_coupon_status'] = "Статус";
$_['ms_seller_account_coupon_status_note'] = "Активируйте или деактивируйте этот купон";
$_['ms_seller_account_coupon_type_' . MsCoupon::TYPE_DISCOUNT_PERCENT] = "Процент";
$_['ms_seller_account_coupon_type_' . MsCoupon::TYPE_DISCOUNT_FIXED] = "Фиксированная сумма";
$_['ms_seller_account_coupon_status_' . MsCoupon::STATUS_ACTIVE] = "Активен";
$_['ms_seller_account_coupon_status_' . MsCoupon::STATUS_DISABLED] = "Неактивен";
$_['ms_seller_account_coupon_restrictions'] = "Ограничения";
$_['ms_seller_account_coupon_products'] = "Товары";
$_['ms_seller_account_coupon_products_placeholder'] = "Скидка этого купона по умолчанию действует на все товары продавца";
$_['ms_seller_account_coupon_products_empty'] = "Отсутствуют товары, для которых можно применить этот купон.";
$_['ms_seller_account_coupon_products_note'] = "Применяйте этот купон только к определенным товарам или исключите определенные товары из этого купона";
$_['ms_seller_account_coupon_products_include'] = "Применять купон к следующим товарам";
$_['ms_seller_account_coupon_products_exclude'] = "Исключите следующие товары из купона";
$_['ms_seller_account_coupon_customers'] = "Покупатели";
$_['ms_seller_account_coupon_customers_note'] = "Укажите покупателей, которые могут использовать этот купон. Оставьте место пустым, если вы хотите, чтобы все покупатели были включены";
$_['ms_seller_account_coupon_categories'] = "Категории";
$_['ms_seller_account_coupon_categories_placeholder'] = "Все категории включены по умолчанию.";
$_['ms_seller_account_coupon_categories_placeholder_products_specified'] = "Используются ограничения по товарам, поэтому оставляя это поле пустым, это означает что все категории исключены";
$_['ms_seller_account_coupon_categories_empty'] = "В настоящее время у вас нет категорий, которым можно применить купоны.";
$_['ms_seller_account_coupon_categories_note'] = "Примените этот купон только к определенным категориям или исключите определенные категории из этого купона";
$_['ms_seller_account_coupon_categories_include'] = "Применять купон к следующим категориям";
$_['ms_seller_account_coupon_categories_exclude'] = "Исключите следующие категории из купона";
$_['ms_seller_account_coupon_created'] = "Успех: Купон успешно создан!";
$_['ms_seller_account_coupon_updated'] = "Успех: Купон успешно обновлен!";
$_['ms_seller_account_coupon_code_error_exists'] = "Ошибка: Этот код купона уже используется!";

$_['ms_total_coupon_title'] = "Купон от %s (%s)";
$_['ms_cart_coupon_heading'] = "Использовать скидочный купон";
$_['ms_cart_coupon_field_label'] = "Введите код скидочного купона тут";
$_['ms_cart_coupon_field_placeholder'] = "Введите код скидочного купона...";
$_['ms_cart_coupon_button_apply'] = "Применить скидочный купон";
$_['ms_cart_coupon_error_empty'] = "Поле(я) скидочного купона пустое!";
$_['ms_cart_coupon_error_apply'] = "Невозможно применить скидочный купон к товару(ам) %s's товара!";
$_['ms_cart_coupon_success_applied'] = "Скидочный купон был успешно применен %s's товара(ов)!";

// Stripe subscription
$_['ms_stripe_subscription'] = 'Подписка';
$_['ms_stripe_subscription_plan_selected'] = 'Выбранный план';
$_['ms_stripe_subscription_terms'] = 'Правила';
$_['ms_stripe_subscription_plan_interval_day'] = "в день";
$_['ms_stripe_subscription_plan_interval_month'] = "в месяц";
$_['ms_stripe_subscription_plan_interval_year'] = "в год";
$_['ms_stripe_subscription_plan_base_info'] = "%s %s";
$_['ms_stripe_subscription_plan_per_seat_info'] = "%s за товар %s";
$_['ms_stripe_subscription_your_card'] = "Карта";
$_['ms_stripe_subscription_period'] = "Следующий платеж";
$_['ms_stripe_subscription_button_signup'] = "Подписаться";
$_['ms_stripe_subscription_error_card_details'] = "Ошибка: Введите данные вашей карты!";
$_['ms_stripe_subscription_error_subscription'] = "Ошибка: Невозможно оформить подписку!";

// Complaint seller/product form
$_['ms_complaint_form_breadcrumbs'] = 'Пожаловаться';
$_['ms_complaint_form_heading'] = 'Пожаловаться';
$_['ms_complaint_form_title'] = 'Пожалуйста опишите характер вашей жалобы:';
$_['ms_complaint_form_error_comment_short'] = 'Текст жалобы должен быть длиннее %s символов!';
$_['ms_complaint_form_error_comment_long'] = 'Текст жалобы должен быть короче %s символов!';
$_['ms_complaint_form_success'] = 'Ваша жалоба отправлена!';
$_['ms_complaint_product_button_title'] = 'Пожаловаться на этот товар';
$_['ms_complaint_button_submit'] = 'Пожаловаться';
$_['ms_complaint_seller_link_title'] = 'Пожаловаться на этого продавца';

$_['ms_complaint_type_product'] = 'product';
$_['ms_complaint_type_seller'] = 'seller';

// Complaint emails to admin
$_['ms_mail_admin_subject_complaint_received'] = 'Получена новая жалоба о %s';
$_['ms_mail_admin_complaint_received'] = <<<EOT
Покупатель пожаловался на %s: %s.

Причина:
%s

Пожалуйста нажмите <a href="%s">сюда</a> чтобы просмотреть и ответить на жалобу.

EOT;

// Holiday mode
$_['ms_account_setting_holiday_mode_tab'] = 'Режим отпуска';
$_['ms_account_setting_holiday_mode_active'] = 'Включен (ваш магазин находится в режиме отпуска, новые заказы не принимаются)';
$_['ms_account_setting_holiday_mode_inactive'] = 'Выключен (ваш магазин принимает новые заказы)';
$_['ms_account_setting_holiday_mode_message'] = 'Сообщение для покупателей';
$_['ms_account_setting_holiday_mode_message_note'] = 'Пожалуйста введите сообщение об отпуске для покупателей, которое отображается в вашем профиле (например, Я в отпуске до 22/07 и не принимаю новых заказов!).';

$_['ms_account_setting_holiday_mode_warning'] = 'Режим отпуска включен!';

$_['ms_front_holiday_mode_warning'] = 'Этот магазин временно не принимает новых заказов:<br />%s';
$_['ms_front_holiday_mode_warning_empty'] = 'Этот магазин временно не принимает новых заказов.';

// MultiMerch notifications
$_['ms_seller_account_notification'] = "Оповещения";
$_['ms_seller_account_notification_breadcrumbs'] = "Оповещения";
$_['ms_seller_account_notification_message'] = "Сообщение";

// Invoicing system
$_['ms_invoices'] = "Счета";
$_['ms_invoice_heading'] = "Счета";
$_['ms_invoice_breadcrumbs'] = "Счета";
$_['ms_invoice_column_invoice_id'] = "Счет #";
$_['ms_invoice_column_payment_info'] = "Платеж";
$_['ms_invoice_column_type'] = "Тип";
$_['ms_invoice_column_title'] = "Заголовок";
$_['ms_invoice_column_status'] = "Статус";
$_['ms_invoice_column_date_generated'] = "Дата создания";
$_['ms_invoice_column_total'] = "Сумма";

$_['ms_invoice_error_not_selected'] = 'Выберите хотя бы один счет!';

$_['ms_invoice_type_listing'] = "Стоимость публикации";
$_['ms_invoice_type_signup'] = "Стоимость регистрации";
$_['ms_invoice_type_sale'] = "Продажа";
$_['ms_invoice_type_payout'] = "Выплата";

$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID] = '<p style="color: red">Ожидается</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_PAID] = '<p style="color: green">Оплачен</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_VOIDED] = '<p style="color: red">Отменен</p>';

$_['ms_invoice_title_product_listing'] = "Стоимость публикации товара <a href='%s'>%s</a>";
$_['ms_invoice_title_seller_signup'] = "Комиссия за регистрацию в %s";
$_['ms_invoice_title_sale_order_commission'] = "Комиссия платформы за товары в заказе <a href='%s'>#%s</a>";
$_['ms_invoice_title_sale_order_products'] = "Товары заказа <a href='%s'>#%s</a>";
$_['ms_invoice_title_seller_payout'] = "Выплата %s";

$_['ms_invoice_item_title_product_listing'] = "Стоимость публикации товара %s";
$_['ms_invoice_item_title_seller_signup'] = "Комиссия за регистрацию в %s";
$_['ms_invoice_item_title_sale_commission'] = "Комиссия платформы за %s в заказе #%s";
$_['ms_invoice_item_title_sale_product'] = "Товар %s";

$_['ms_invoice_total_title_subtotal'] = "Промежуточная сумма";
$_['ms_invoice_total_title_shipping'] = "Доставка платформы";
$_['ms_invoice_total_title_mm_shipping_total'] = "Доставка продавца";
$_['ms_invoice_total_title_coupon'] = "Купон";
$_['ms_invoice_total_title_ms_coupon'] = "Купон продавца";
$_['ms_invoice_total_title_tax'] = "Налог";
$_['ms_invoice_total_title_total'] = "Сумма";

// Shipping system
$_['ms_shipping_per_product_override_off_note'] = "Стоимость доставки для данного товара будет рассчитываться автоматически в соответствии с <a href='%s' target='_blank'>вашими общими правилами доставки</a>.";
$_['ms_shipping_per_product_override_off_btn'] = "Пересчитать общие правила для данного товара?";
$_['ms_shipping_per_product_override_on_note'] = "Определите специальные правила доставки для данного товара, чтобы заменить ими <a href='%s' target='_blank'>ваши общие правила доставки</a>.";
$_['ms_shipping_per_product_override_on_btn'] = "Убрать специальный расчет и вернуться к общим правилам?";

$_['ms_shipping_title'] = "Доставка";
$_['ms_shipping_rules_manage_title'] = "Изменить правила доставки";
$_['ms_shipping_field_shipping_from'] = "Доставка из";
$_['ms_shipping_field_processing_days'] = "Дни на обработку";
$_['ms_shipping_field_destinations'] = "Пункт назначения";
$_['ms_shipping_field_shipping_method'] = "Методы доставки";
$_['ms_shipping_field_delivery_time'] = "Время доставки";
$_['ms_shipping_field_cart_weight'] = "Вес в корзине (от - до)";
$_['ms_shipping_field_cart_total'] = "Итог корзины (от - до)";
$_['ms_shipping_field_cost'] = "Стоимость";
$_['ms_shipping_field_cost_ai'] = "Стоимость за дополнительную единицу";
$_['ms_shipping_btn_add_destination'] = "+ Добавить пункт назначения";

$_['ms_shipping_placeholder_cost_pwu'] = "+ %s за единицу веса";
$_['ms_shipping_placeholder_cost_ai'] = "+ %s за дополнительную единицу";

$_['ms_shipping_error_rules_empty_global'] = "Пожалуйста, <a href='%s'>измените ваши правила доставки,</a> чтобы ваши товары стали доступны покупателям";
$_['ms_shipping_error_field_from_country_id_required'] = "Вы должны указать страну доставки";
$_['ms_shipping_error_field_processing_days_required'] = "Вы должны указать время на обработку";
$_['ms_shipping_error_rules_required'] = "Пожалуйста, укажите хотя бы одно правило доставки";
$_['ms_shipping_error_rule_destinations_required'] = "Вы должны указать пункт назначения для данного правила доставки";
$_['ms_shipping_error_rule_shipping_method_id_required'] = "Вы должны указать метод доставки для данного правила доставки";
$_['ms_shipping_error_rule_delivery_time_id_required'] = "Вы должны указать время доставки для данного правила доставки";
$_['ms_shipping_error_rule_weight_range_required'] = "Вы должны указать допустимый вес корзины для данного правила доставки";
$_['ms_shipping_error_rule_total_range_required'] = "Вы должны указать итог корзины для данного правила доставки";
$_['ms_shipping_error_rule_cost_required'] = "Вы должны указать стоимость для данного правила доставки";
$_['ms_shipping_error_rule_cost_pwu_required'] = "Вы должны указать стоимость за единицу веса для данного правила доставки";
$_['ms_shipping_error_rule_cost_ai_required'] = "Вы должны указать стоимость за дополнительную единицу для данного правила доставки";

$_['ms_shipping_checkout_info_title'] = "Пожалуйста, выберите варианты доставки для товаров в вашем заказе.";
$_['ms_shipping_checkout_info_title_with_digital'] = "Пожалуйста, выберите варианты доставки для материальных товаров в вашей корзине.";
$_['ms_shipping_checkout_title_seller_table'] = "Выберите варианты доставки для %s";
$_['ms_shipping_checkout_title_marketplace_table'] = "Выберите варианты доставки для";
$_['ms_shipping_checkout_title_select_option'] = "Выберите варианты доставки";
$_['ms_shipping_checkout_title_shipping_total'] = "Общее по доставке";
$_['ms_shipping_checkout_remove_product'] = "Убрать товар";
$_['ms_shipping_checkout_error_general'] = "Возникли проблемы с доставкой одного из ваших товаров из заказа (детали ниже).";
$_['ms_shipping_checkout_error_individual_general'] = "Этот товар не может быть отправлен по выбранному адресу. Пожалуйста, измените адрес или удалите данный товар из заказа.";
$_['ms_shipping_checkout_error_combined_general'] = "Эти товары не могут быть отправлены по выбранному адресу. Пожалуйста, измените адрес или удалите данные товары из заказа.";
$_['ms_shipping_checkout_error_cart_weight_less_than_min'] = "Общий вес товаров (%s) ниже минимально допустимого данным продавцом (%s). Пожалуйста, добавьте к вашему заказу больше товаров от этого продавца.";
$_['ms_shipping_checkout_error_cart_weight_more_than_max'] = "Общий вес товаров (%s) превышает максимально допустимый данным продавцом. Пожалуйста, уберите из вашего заказа какие-нибудь товары от этого продавца.";
$_['ms_shipping_checkout_error_cart_total_less_than_min'] = "Цена товаров (%s) ниже минимально допустимой данным продавцом (%s). Пожалуйста, добавьте к вашему заказу больше товаров от этого продавца.";
$_['ms_shipping_checkout_error_cart_total_more_than_max'] = "Цена товаров (%s) превышает максимально допустимую данным продавцом. Пожалуйста, уберите из вашего заказа какие-нибудь товары от этого продавца.";

$_['ms_wishlist_title'] = "Список желаний";
$_['ms_wishlist_breadcrumbs'] = "Список желаний";
$_['ms_wishlist_date_added'] = "Добавлено на %s";
$_['ms_wishlist_add_success'] = "<strong>%s</strong> был успешно добавлен в ваш список желаний!";
$_['ms_wishlist_add_error_login'] = "Пожалуйста, войдите в аккаунт или зарегистрируйтесь, чтобы добавлять товары в список желаний.";
$_['ms_wishlist_btn_to_wishlist'] = "Перейти в список желаний";
$_['ms_wishlist_btn_login'] = "Войти / Зарегистрироваться";
$_['ms_wishlist_btn_continue'] = "Продолжить покупки";

// Favorite sellers
$_['ms_favorite_seller_title'] = "Любимые продавцы";
$_['ms_favorite_seller_breadcrumbs'] = "Любимые продавцы";
$_['ms_favorite_seller_no_results'] = "Вы не подписаны ни на одного продавца!";
$_['ms_favorite_seller_success_follow'] = "Вы добавили <strong>%s</strong> к вашему списку любимых продавцов!";
$_['ms_favorite_seller_success_unfollow'] = "Вы убрали <strong>%s</strong> из списка любимых покупателей.";
$_['ms_favorite_seller_error_login'] = "Пожалуйста, войдите в ваш аккаунт или зарегистрируйтесь, чтобы подписаться на %s.";
$_['ms_favorite_seller_follow_button'] = "Подписаться на продавца";
$_['ms_favorite_seller_unfollow_button'] = "Отписаться";
$_['ms_favorite_seller_btn_login'] = "Войти / Регистрация";
$_['ms_favorite_seller_btn_to_favorite_sellers'] = "Посмотреть любимых продавцов";
$_['ms_favorite_seller_btn_continue'] = "Продолжить покупки";
$_['ms_favorite_seller_date_added'] = "Добавлено на %s";

// Product MSF variations
$_['ms_product_msf_variation_price_from'] = "От %s";
$_['ms_product_msf_variation_error_not_selected'] = "Пожалуйста, выберите вариант товара!";
$_['ms_product_msf_variation_error_status'] = "Товар недействителен!";
$_['ms_product_msf_variation_error_out_of_stock'] = "Товара нет в наличии!";
$_['ms_product_msf_variation_error_quantity'] = "Товар недоступен в выбранном количестве!";

// MSF seller properties
$_['ms_field_seller_property_error_field_empty'] = "Поле '%s' обязательно";
$_['ms_field_seller_property_error_field_length'] = "Поле '%s' должно быть короче '255' символов!";

// MM product filter blocks
$_['ms_product_filter_block_heading'] = "Фильтры";
$_['ms_product_filter_block_title_general'] = "Общее";
$_['ms_product_filter_block_title_attributes'] = "Атрибуты";
$_['ms_product_filter_block_title_oc_options'] = "Опции";
$_['ms_product_filter_block_title_oc_manufacturers'] = "Производители";
$_['ms_product_filter_block_title_price'] = "Цена";
$_['ms_product_filter_block_title_category'] = "Категории";

/***** INSERTED *****/$_['ms_print_order_packing_slip'] = "Print packing slip";
/***** INSERTED *****/$_['ms_print_order_invoice'] = "Print invoice";
