<?php
$_['ppa_title']    = 'PayPal Adaptive';
$_['ppa_sandbox']	= 'Предупреждение: платёжная система находится в режиме \'Песочница\'. Средства с вашего аккаунта не будут списаны.';

$_['ppa_error_distribution']        = 'Ошибка конфигурации: Неверный объём распределения. ID заказа: %s';
$_['ppa_error_noreceivers']        = 'Ошибка конфигурации: Не указаны возможные получатели. ID заказа: %s';
$_['ppa_error_no_unique_mails']        = 'Ошибка конфигурации: Два или более получателя имеют одинаковый указанный адрес PayPal. ID Заказа: %s';
$_['ppa_error_generic']        = 'Ошибка конфигурации: Пожалуйста, свяжитесь с владельцем магазина. ID заказа: %s';
$_['ppa_error_request']        = 'PayPal Ошибка Запроса: Correlation ID %s';
$_['ppa_error_response']        = 'PayPal Ошибка Ответа: Status %s Correlation ID: %s';

$_['ms_transaction_order'] = 'Продажа: ID заказа #%s';