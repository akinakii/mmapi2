<?php
/***** INSERTED *****/$_['text_title']    = "Stripe";

$_['text_start_title'] = "Принимайте платежи через Stripe";
$_['text_start_description'] = "Привяжите ваш аккаунт Stripe для приема платежей напрямую. <a href='//stripe.com/se/payments' target='_blank'>Узнать больше...</a>";

$_['text_connected_account'] = "Номер аккаунта Stripe";
$_['text_deauthorize'] = "Отвязать";
$_['text_click_here'] = "Нажмите сюда";
$_['text_payment_description'] = "Платеж Stripe: Заказ #%s";
$_['text_checkout_confirm'] = "Завершить заказ";

$_['success_account_connected'] = "Аккаунт Stripe успешно привязан!";
$_['success_account_connected_alert'] = "Ваш аккаунт Stripe привязан - вы можете получать платежи от покупателей.";
$_['success_deauthorized'] = "Вы отвязали ваш аккаунт Stripe!";

$_['error_account_not_connected'] = "Ошибка: Аккаунт Stripe не привязан";
$_['error_access_denied'] = "Ошибка: Запрет на выполнения действия на стороне Stripe";
$_['error_authorize'] = "Ошибка: Ваш аккаунт не может быть привязан к Stripe";
$_['error_api_connection'] = "Ошибка: Ошибка API";
$_['error_deauthorize'] = "Ошибка: Ваш аккаунт Stripe не привязан или не существует";
$_['error_checkout_token'] = "Ошибка: Не удалось получить токен Stripe";
$_['error_no_order'] = "Ошибка: Заказ не существует";
/***** INSERTED *****/$_['error_card'] = "Your card was declined. Please contact your card issuer or try a different card.";
