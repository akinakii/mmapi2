<?php
// Product Listings
$_['basel_text_offer_ends'] 			= 'Предложение ограничено и закончится через:';
$_['basel_text_sale'] 					= 'Распродажа';
$_['basel_text_out_of_stock'] 			= 'Нет в наличии';
$_['basel_text_new'] 					= 'Новое';
$_['basel_text_days'] 					= 'дн';
$_['basel_text_hours'] 					= 'ч';
$_['basel_text_mins'] 					= 'мин';
$_['basel_text_secs'] 					= 'сек';

// Product page
$_['basel_text_share'] 					= 'Поделиться';

// Categoty Listings Module
$_['basel_text_products']     			= 'Товары';
$_['basel_text_view_products']     		= 'Посмотреть товары';

// Quickview
$_['basel_button_quickview'] 			= 'Быстрый просмотр';
$_['basel_text_view_details'] 			= 'Подробнее';
$_['basel_text_select_option'] 			= 'Выбрать опцию';

// Live Search
$_['basel_text_search']     			= 'Поиск';
$_['basel_text_view_all'] 				= 'Посмотреть все результаты';
$_['basel_text_no_result'] 				= 'Нет результатов';
$_['basel_text_category']     			= 'Все категории';

// Contact Form
$_['basel_text_name'] 					= 'Ваше имя';
$_['basel_text_email'] 					= 'Ваш email';
$_['basel_text_message'] 				= 'Ваше сообщение';
$_['basel_text_captcha']				= 'Ответьте на вопрос ниже';
$_['basel_text_submit'] 				= 'Отправить сообщение';
$_['basel_error_name']      			= 'Ваше имя должно быть от 2-х до 30-и символов в длину';
$_['basel_error_email'] 				= 'Email недействителен или не указан';
$_['basel_error_message'] 				= 'Сообщение отсутствует';
$_['basel_error_captcha'] 				= 'Неправильный проверочный ответ';
$_['basel_text_success_form'] 			= 'Спасибо! Ваше сообщение отправлено.';
$_['basel_email_subject'] 				= 'Новое письмо от %';

// Newsletter Subscribe
$_['basel_subscribe_email'] 			= 'Ваш электронный адрес';
$_['basel_subscribe_btn'] 				= 'Регистрация';
$_['basel_unsubscribe_btn'] 			= 'Отписаться';
$_['basel_subscribe_invalid_email'] 	= 'Некорректный формат email';
$_['basel_subscribe_success'] 			= 'Подписка оформлена';
$_['basel_subscribe_email_exist'] 		= 'Данный email уже подписан';
$_['basel_unsubscribe_not_found'] 		= 'Email не найден';
$_['basel_unsubscribe_unsubscribed'] 	= 'Подписка отменена';

// Product Questions and Answers
$_['basel_button_ask']     				= 'Задать вопрос';
$_['basel_tab_questions']     			= 'Вопросы &amp; Ответы';
$_['basel_text_recent_questions'] 		= 'Недавние вопросы';
$_['basel_text_no_questions']     		= 'По данному товару нет вопросов.';
$_['basel_text_question_from']    		= 'Вопрос от';
$_['basel_text_no_answer']        		= 'На этот вопрос еще не ответили';
$_['basel_text_our_answer']       		= 'Ваш ответ:';
$_['basel_button_send']       			= 'Отправить';
$_['basel_heading_ask']     			= 'Задайте вопрос об этом товаре';
$_['basel_entry_name']     				= 'Ваше имя';
$_['basel_entry_email']     			= 'Ваш email&nbsp;&nbsp;<i style="color:#999999">(публиковаться не будет)</i>';
$_['basel_entry_question']     			= 'Ваш вопрос';
$_['basel_entry_captcha']     			= 'Ответьте на проверочный вопрос';
$_['basel_error_text']            		= 'Ваш вопрос должен быть длиннее 10 символов и короче 1000';
$_['basel_error_captcha']         		= 'Проверочный ответ не совпадает с вопросом';
$_['basel_text_success_qa'] 			= 'Спасибо! Мы ответим вам при первой же возможности';
$_['basel_text_subject']				= '%s - вопрос по товару';
$_['basel_text_waiting']				= 'Вы получили новый вопрос по товару, ответьте на него.';
$_['basel_text_product_qa']				= 'Товар: %s';
$_['basel_text_author']					= 'Автор: %s';
$_['basel_text_question']				= 'Вопрос:';

// Cookie Bar
$_['basel_cookie_info']     			= 'Чтобы улучшить работу сайта, мы используем cookie. Оставаясь здесь, вы соглашаетесь с тем, чтобы мы использовали ваши cookie';
$_['basel_cookie_btn_close']     		= 'Закрыть';
$_['basel_cookie_btn_more_info']     	= 'Больше информации';