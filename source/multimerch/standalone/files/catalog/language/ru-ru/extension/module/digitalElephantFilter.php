<?php
// Heading 
$_['heading_title']      = 'Фильтр';
$_['text_price_range']   = 'Цена';
$_['text_categories']    = 'Категория';
$_['text_manufacturers'] = 'Бренд';
$_['text_all'] 			 = 'Посмотреть все';
$_['text_clear']         = 'Очистить';
$_['text_ok']            = 'Применить';

$_['text_show_more']     = 'Загрузить еще';