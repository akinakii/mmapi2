<?php
// Text
$_['ms_import_text_title'] 			= 'Импорт товаров';
$_['ms_import_text_header'] 			= 'Импорт товаров';
$_['ms_import_text_account']        = 'Аккаунт';
$_['ms_import_text_start_new_import'] = 'Импорт данных';
$_['ms_import_text_example_url']   = 'Скачать пример CSV файла';

$_['ms_import_text_name']          = 'Имя';
$_['ms_import_text_date']          = 'Дата';
$_['ms_import_text_type']          = 'Тип';
$_['ms_import_text_processed']     = 'Обработано';
$_['ms_import_text_added']         = 'Добавлено';
$_['ms_import_text_errors']        = 'Ошибки';
$_['ms_import_text_actions']       = 'Действия';

$_['ms_imports_text_imports']      = 'Импорт';
$_['ms_imports_text_new_import']      = 'Новый импорт';
$_['ms_imports_text_import_data']      = 'Импорт данных';
$_['ms_imports_text_import_filename']      = 'Имя файла импорта';
$_['ms_imports_text_import_continue']      = 'Сохранить И Продолжить';
$_['ms_imports_text_import_back']      = 'Назад';

//field_types
$_['ms_imports_text_field_type_sku']      		= 'sku';
$_['ms_imports_text_field_type_name']      		= 'название';
$_['ms_imports_text_field_type_description']    = 'описание';
$_['ms_imports_text_field_type_price']      	= 'цена';
$_['ms_imports_text_field_type_category']      	= 'все категории';
$_['ms_imports_text_field_type_quantity']      	= 'количество';
$_['ms_imports_text_field_type_category1']      = 'Категория 1 уровня';
$_['ms_imports_text_field_type_category2']      = 'Категория 2 уровня';
$_['ms_imports_text_field_type_category3']      = 'Категория 3 уровня';
$_['ms_imports_text_field_type_image_url']      = 'главное изображение';
$_['ms_imports_text_field_type_images_url']     = 'изображения';
$_['ms_imports_text_field_type_model']      	= 'модель';
$_['ms_imports_text_field_type_currency']      	= 'валюта';
$_['ms_imports_text_field_type_weight']      	= 'вес';
$_['ms_imports_text_field_type_weight_class_id']= 'weight class_id';
$_['ms_imports_text_field_type_tax_class_id']   = 'идентификатор налогов';
$_['ms_imports_text_field_type_available_to']   = 'доступен для';
$_['ms_imports_text_field_type_manufacturer']   = 'производитель';

$_['ms_imports_text_field_type_product_name']   = 'имя';
$_['ms_imports_text_field_type_product_description']   = 'описание';

//steps
$_['ms_import_text_title_step1']   = 'Загрузить';
$_['ms_import_text_title_step2']   = 'Маппинг';
$_['ms_import_text_title_step3']   = 'Подтвердить';

//step1
$_['ms_import_text_type_seller']   = 'Продавцы';
$_['ms_import_text_type_product']  = 'Товары';
$_['ms_import_text_type_category'] = 'Категории';
$_['ms_import_text_select_config'] = 'Выберите конфигурацию';
$_['ms_import_text_select_you_config'] = 'Выберите вашу конфигурацию';

//step2
$_['ms_imports_text_import_steps2']   = 'Шаг 1 с 3';
$_['ms_import_text_choose_file_for_import']   = 'Выберите файл для импорта:';
$_['ms_import_text_upload_file']   = 'Выберите файл для импорта:';
$_['ms_import_text_file_encoding']   = 'Кодирования файла:';
$_['ms_import_text_rows_limits']   = 'Ограничения строк:';
$_['ms_import_text_separators']   = 'Разделители:';
$_['ms_import_text_cell_separator']   = 'Разделитель строк:';
$_['ms_import_text_cell_container']   = 'Разделитель клеток:';
$_['ms_import_text_upload_file_note']   = 'Допустимое расширения: csv';
$_['ms_import_text_select_file']   = 'Выберите файл';

//step3
$_['ms_imports_text_import_steps3']   = 'Шаг 2 с 3';
$_['ms_imports_text_import_file_field']      = 'Поле файла';
$_['ms_imports_text_import_oc_field']      = 'OC тип поля';
$_['ms_imports_text_import_select_oc_field_type']      = 'Выбрать OC тип поля';
$_['ms_imports_text_import']      = 'Импорт';
$_['ms_imports_text_column_label']      = 'Метка столбца с csv';
$_['ms_imports_text_product_property']      = 'Свойства товара';
$_['ms_imports_text_preview_information']      = 'Информация для предварительного просмотра';

//step4
$_['ms_imports_text_import_steps4']   = 'Шаг 3 с 3';
$_['ms_imports_text_import_update_config']      = 'Обновить конфигурацию';
$_['ms_imports_text_import_save_config']      = 'Сохранить конфигурацию';
$_['ms_imports_text_import_save_config_success']      = 'Ваша конфигурация сохранена!';
$_['ms_imports_text_import_update_config_success']      = 'Ваша конфигурация обновлена!';
$_['ms_imports_text_samples_of_data']      = 'Образцы данных';
$_['ms_imports_text_file_column']      = 'Колонка';
$_['ms_imports_text_not_specified'] = 'Не указано';
$_['ms_import_text_start_row']   = 'Начальная строка';
$_['ms_import_text_finish_row']   = 'Конечная строка';
$_['ms_import_text_default_quantity']   = 'Количество по умолчанию';
$_['ms_import_text_default_product_status']   = 'Статус товара по умолчанию';
$_['ms_import_text_delimiter_category']   = 'Разделитель категорий';
$_['ms_import_text_fill_category']   = 'Заполнить родительскую категорию';
$_['ms_import_text_stock_status']   = 'Состояние запасов';
$_['ms_import_text_product_approved']   = 'Товар утвержден';
$_['ms_import_text_images_path']   = 'Путь к изображению';
$_['ms_imports_text_yes']      = 'Да';
$_['ms_imports_text_no']      = 'Нет';
$_['ms_imports_text_enabled']      = 'Включено';
$_['ms_imports_text_disabled']      = 'Отключено';
$_['ms_imports_text_config_name']      = 'Имя конфигурации';
$_['ms_imports_text_update_field']      = 'Обновить поле';

//errors
$_['ms_import_text_error_type']    = 'Ошибка типа!';
$_['ms_import_text_error_file']    = 'Ошибка файла!';
$_['ms_import_text_error_encoding']    = 'Ошибка кодирования!';
$_['ms_import_text_error_parsing_columns']    = 'Ошибка сортировки столбцов!';
$_['ms_import_text_error_type_not_set']    = 'Тип не установлен!';
$_['ms_import_text_error_file_not_set']    = 'Файл не установлен!';
$_['ms_import_text_error_edit_only_you_config']    = 'Вы можете обновлять только вашу конфигурацию!';
$_['ms_import_text_error_no_data_for_import']    = 'Не найдено данных для импорта!';
$_['ms_import_text_error_invalid_data_for_import']    = 'Недействительные данные для импорта!';
$_['ms_import_text_error_noset_rows_separator']    = 'Не установлен разделитель строк!';
$_['ms_import_text_error_noset_cells_separator']    = 'Не установлен разделитель клеток!';

$_['ms_import_text_error_noset_sku']    = 'Не задано свойство sku!';
$_['ms_import_text_error_noset_name']    = 'Не задано свойство имя!';

//results
$_['ms_import_text_result_all_rows'] = 'Обработано строк: ';
$_['ms_import_text_result_new_rows_count'] = 'Создано новых товаров: ';
$_['ms_import_text_result_update_rows_count'] = 'Существующих товаров обновлено: ';
$_['ms_import_text_result_duplicate_rows_count'] = 'Найдено дублированных строк: ';