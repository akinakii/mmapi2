<?php

$_['msn_mail_subject_admin_updated_order_status'] = "Ваш заказ #%s был обновлён продавцом %s";
$_['msn_mail_admin_updated_order_status'] = <<<EOT
Ваш заказ #%s был обновлён продавцом %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s
EOT;

$_['msn_mail_subject_admin_updated_suborder_status'] = "%s обновил ваш заказ #%s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
%s обновил ваш заказ на %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s
EOT;

$_['msn_mail_subject_customer_created_account'] = 'Зарегистрирован новый покупатель';
$_['msn_mail_customer_created_account'] = <<<EOT
В %s зарегистрирован новый покупатель!
Имя: %s
Эл. почта: %s
EOT;

$_['msn_mail_subject_customer_created_message'] = "Новое сообщение";
$_['msn_mail_customer_created_message'] = <<<EOT
Вы получили новое сообщение от %s!

%s

%s

Вы можете ответить на него через ваш аккаунт.
EOT;

$_['msn_mail_subject_customer_created_order'] = "Покупатель %s совершил новый заказ в %s";
$_['msn_mail_customer_created_order'] = <<<EOT
Покупатель %s совершил новый заказ в %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s
EOT;

$_['msn_mail_subject_customer_created_ms_review'] = 'Новый отзыв о товаре';
$_['msn_mail_customer_created_ms_review'] = <<<EOT
Получен новый отзыв о %s.
Просмотреть отзыв можно по ссылке: <a href="%s">%s</a>
EOT;

$_['msn_mail_subject_guest_created_order'] = "Гость разместил новый заказ на %s";
$_['msn_mail_guest_created_order'] = <<<EOT
Гость разместил новый заказ на %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s
EOT;

$_['msn_mail_subject_seller_created_account'] = 'Зарегистрирован новый продавец';
$_['msn_mail_seller_created_account'] = <<<EOT
В %s зарегистрирован новый продавец!
Продавец: %s (%s)
Эл. почта: %s
EOT;

$_['msn_mail_subject_seller_created_message'] = "Новое сообщение";
$_['msn_mail_seller_created_message'] = <<<EOT
Вы получили новое сообщение от %s!

%s

%s

Вы можете ответить на него через ваш аккаунт.
EOT;

$_['msn_mail_subject_seller_created_product'] = 'Новый товар';
$_['msn_mail_seller_created_product'] = <<<EOT
Новый товар %s был добавлен продавцом %s.

Вы можете просмотреть или отредактировать его через панель администратора.
EOT;

$_['msn_mail_subject_seller_updated_invoice_status'] = "Продавец оплатил счет";
$_['msn_mail_seller_updated_invoice_status'] = <<<EOT
Продавец %s оплатил счет #%s.
EOT;

$_['msn_mail_subject_system_created_invoice'] = "Новый неоплаченный счет";
$_['msn_mail_system_created_invoice'] = <<<EOT
Вам выставлен новый счет #%s.
EOT;

/***** INSERTED *****/$_['msn_mail_subject_system_finished_import'] = "Your products import is finished";
/***** INSERTED *****/$_['msn_mail_system_finished_import'] = <<<EOT
Products import has been finished. You can now see your products in Products section of you seller panel.
EOT;

