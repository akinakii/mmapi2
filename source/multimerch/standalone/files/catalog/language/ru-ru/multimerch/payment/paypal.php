<?php
// Text
$_['text_title'] 			= 'PayPal';
$_['text_payment_form_title'] = 'Заплатить с помощью PayPal';

$_['text_success'] 			= 'Вы успешно изменили настройки PayPal';

$_['text_pp_address'] 		= 'PayPal Адрес';

$_['text_receiver'] 		= 'Получатель';
$_['text_sender'] 			= 'Отправитель';

$_['button_save'] 			= 'Сохранить';

$_['error_pp_address'] 		= 'Вы должны заполнить поле PayPal!';
$_['error_receiver_data'] 	= 'Информация об этом способе оплаты отсутствует. Пожалуйста, <a href="%s" target="_blank">свяжетсь</a> с владельцем магазина для более подробной информации.';
$_['error_sender_data'] 	= 'Вы <a href="%s" target="_blank">изменили</a> настройки для этого способа оплаты!';

