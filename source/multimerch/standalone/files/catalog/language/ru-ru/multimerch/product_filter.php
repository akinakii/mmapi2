<?php

$_['text_categories'] = "Категории";
$_['text_manufacturers'] = "Бренды";
$_['text_price'] = "Цена";
$_['text_price_range_min'] = "От";
$_['text_price_range_max'] = "До";
$_['text_no_products'] = "Нет товаров с выбранными параметрами фильтра.";

$_['button_go'] = "<i class='fa fa-check'></i>";
