<?php

$_['msn_onsite_admin_created_message'] = "<strong>%s</strong> (персонал %s) ответил на ваше сообщение <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_admin_updated_order_status'] = "Статус оплаты <a href='%s' class='order'>#%s</a> был изменен на <strong>%s</strong>.";
$_['msn_onsite_admin_updated_suborder_status'] = "Статус заказа <a href='%s' class='order'>#%s</a> был изменен на <strong>%s</strong>.";
$_['msn_onsite_admin_created_payout'] = "Вы получили новую выплату на сумму %s.";
$_['msn_onsite_admin_updated_account_status'] = "Статус вашего аккаунта продавца был изменен на <strong>%s</strong>.";

$_['msn_onsite_customer_created_message'] = "Покупатель <strong>%s</strong> ответил на ваше сообщение <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "Получен новый заказ <a href='%s' class='order'>#%s</a>.";
$_['msn_onsite_customer_created_ms_review'] = "Вы получили новый <a href='%s' class='catalog'><strong>отзыв</strong></a> от <strong>%s</strong>.";

$_['msn_onsite_guest_created_order'] = "Вы получили новый заказ <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_system_created_invoice'] = "Создан новый неоплаченный <a href='%s' class='catalog'><strong>счет</strong></a> <strong>#%s</strong>.";
