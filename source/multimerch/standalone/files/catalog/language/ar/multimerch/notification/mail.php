<?php

$_['msn_mail_subject_admin_updated_order_status'] = "تحديث الطلب #%s من %s";
$_['msn_mail_admin_updated_order_status'] = <<<EOT
طلبك لدى %s تم تحديثه بواسطة %s:

الطلب#: %s

المنتجات:
%s

الحالة: %s

تعليق:
%s
EOT;

$_['msn_mail_subject_admin_updated_suborder_status'] = "Your order #%s has been updated by %s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
Your order at %s has been updated by %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_customer_created_account'] = 'New customer account created';
$_['msn_mail_customer_created_account'] = <<<EOT
تم إنشاء حساب عميل جديد في %s!
اسم العميل: %s
البريد الإلكتروني: %s
EOT;

$_['msn_mail_subject_customer_created_message'] = "رسالة جديدة";
$_['msn_mail_customer_created_message'] = <<<EOT
لقد استلمت رسالة جديدة من %s!

%s

%s

يمكنك الرد عليها في قسم الرسائل في صفحتك الشخصية.
EOT;

$_['msn_mail_subject_customer_created_order'] = "قام %s بوضع طلب جديد %s";
$_['msn_mail_customer_created_order'] = <<<EOT
قام العميل %s بوضع طلب جديد في %s:

الطلب#: %s

المنتجات:
%s
الحالة: %s

تعليق:
%s
EOT;

$_['msn_mail_subject_customer_created_ms_review'] = 'مراجعة منتج';
$_['msn_mail_customer_created_ms_review'] = <<<EOT
تم تقديم مراجعة جديدة لـ %s.
قم بزيارة الرابط التالي لمشاهدتها: <a href="%s">%s</a>
EOT;

$_['msn_mail_subject_guest_created_order'] = "Guest has placed a new order at %s";
$_['msn_mail_guest_created_order'] = <<<EOT
Guest has placed a new order at %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_seller_created_account'] = 'بائع جديد';
$_['msn_mail_seller_created_account'] = <<<EOT
تم إنشاء حساب بائع جديد في %s!
اسم البائع: %s (%s)
الريد الإلكتروني: %s
EOT;

$_['msn_mail_subject_seller_created_message'] = "رسالة جديدة";
$_['msn_mail_seller_created_message'] = <<<EOT
لقد استلمت رسالة جديدة من %s!

%s

%s

يمكنك الرد عليها في قسم الرسائل في صفحتك الشخصية.
EOT;

$_['msn_mail_subject_seller_created_product'] = 'إضافة منتج جديد';
$_['msn_mail_seller_created_product'] = <<<EOT
تمت إضافة المنتج %s بواسطة %s.

تستطيع عرضه وتعديله في صفحة الإدارة.
EOT;

$_['msn_mail_subject_seller_updated_invoice_status'] = "البائع دفع الفاتورة";
$_['msn_mail_seller_updated_invoice_status'] = <<<EOT
البائع %s قام بدفع الفاتورة #%s.
EOT;

$_['msn_mail_subject_system_created_invoice'] = "فاتورة غير مدفوعة";
$_['msn_mail_system_created_invoice'] = <<<EOT
لديك فاتورة غير مدفوعة جديدة #%s.
EOT;

/***** INSERTED *****/$_['msn_mail_subject_system_finished_import'] = "Your products import is finished";
/***** INSERTED *****/$_['msn_mail_system_finished_import'] = <<<EOT
Products import has been finished. You can now see your products in Products section of you seller panel.
EOT;

