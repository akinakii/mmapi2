<?php

$_['msn_onsite_admin_created_message'] = "<strong>%s</strong> (إدارة %s) قام بالرد على المحادثة <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_admin_updated_order_status'] = "Payment status for order <a href='%s' class='order'>#%s</a> has been changed to <strong>%s</strong>.";
$_['msn_onsite_admin_updated_suborder_status'] = "Order <a href='%s' class='order'>#%s</a> status has been changed to <strong>%s</strong>.";
$_['msn_onsite_admin_created_payout'] = "لقد استلمت مدفوعات من قبل %s.";
$_['msn_onsite_admin_updated_account_status'] = "حالة حساب البائع الخاصة بك تم تغييرها إلى <strong>%s</strong>.";

$_['msn_onsite_customer_created_message'] = "قام العميل <strong>%s</strong> بالرد على المحادثة <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "لقد تلقيت طلب جديد <a href='%s' class='order'>#%s</a>.";
$_['msn_onsite_customer_created_ms_review'] = "لقد تلقيت <a href='%s' class='catalog'><strong>مراجعة</strong></a> جديدة من قبل <strong>%s</strong>.";

$_['msn_onsite_guest_created_order'] = "You have received a new order <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_system_created_invoice'] = "لديك <a href='%s' class='catalog'><strong>فاتورة</strong></a> غير مدفوعة جديدة <strong>#%s</strong>.";
