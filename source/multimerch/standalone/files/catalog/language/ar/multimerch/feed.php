<?php
// Text
/***** INSERTED *****/$_['ms_feed_title_import_products'] = "Import products";

/***** INSERTED *****/$_['ms_feed_label_file_encoding'] = "File encoding";
/***** INSERTED *****/$_['ms_feed_label_select_file'] = "Select file";

/***** INSERTED *****/$_['ms_feed_column_csv_caption'] = "Field caption from CSV";
/***** INSERTED *****/$_['ms_feed_column_product_property'] = "Product property";
/***** INSERTED *****/$_['ms_feed_column_preview_information'] = "Preview information";

/***** INSERTED *****/$_['ms_feed_import_result_all_rows'] = "Processed rows: ";
/***** INSERTED *****/$_['ms_feed_import_result_new_rows_count'] = "New products created: ";
/***** INSERTED *****/$_['ms_feed_import_result_update_rows_count'] = "Existing products updated: ";
/***** INSERTED *****/$_['ms_feed_import_result_duplicate_rows_count'] = "Duplicate rows found: ";

/***** INSERTED *****/$_['ms_feed_not_specified'] = "Not specified";
/***** INSERTED *****/$_['ms_feed_status_active'] = "Active";
/***** INSERTED *****/$_['ms_feed_status_inactive'] = "Inactive";

/***** INSERTED *****/$_['ms_feed_step_upload_title'] = "Source";
/***** INSERTED *****/$_['ms_feed_step_mapping_title'] = "Mapping";
/***** INSERTED *****/$_['ms_feed_step_confirm_title'] = "Preview and confirm";

/***** INSERTED *****/$_['ms_feed_step_upload_seq_number'] = "Step 1 of 3";
/***** INSERTED *****/$_['ms_feed_step_mapping_seq_number'] = "Step 2 of 3";
/***** INSERTED *****/$_['ms_feed_step_confirm_seq_number'] = "Step 3 of 3";

/***** INSERTED *****/$_['ms_feed_source_import_type'] = "Import type";
/***** INSERTED *****/$_['ms_feed_source_import_type_note'] = "Select the way you'll import your products. You can upload a file or specify a remote product feed.";
/***** INSERTED *****/$_['ms_feed_source_local'] = "Upload a CSV file";
/***** INSERTED *****/$_['ms_feed_source_url'] = "Specify a remote CSV feed";
/***** INSERTED *****/$_['ms_feed_source_local_label'] = "Upload CSV file";
/***** INSERTED *****/$_['ms_feed_source_local_note'] = "Upload your product feed in a CSV format";
/***** INSERTED *****/$_['ms_feed_source_url_label'] = "Remote feed URL";
/***** INSERTED *****/$_['ms_feed_source_url_placeholder'] = "https://example.com/products.csv";
/***** INSERTED *****/$_['ms_feed_source_url_note'] = "Specify the URL of your remote product feed. This must be a valid CSV file.";
/***** INSERTED *****/$_['ms_feed_source_url_btn'] = "Download";

/***** INSERTED *****/$_['ms_feed_to_step_mapping'] = "Continue to field mapping";
/***** INSERTED *****/$_['ms_feed_to_step_confirm'] = "Continue to import";
/***** INSERTED *****/$_['ms_feed_start_new_import'] = "Import products";

/***** INSERTED *****/$_['ms_feed_error_source_file'] = 'Error: You must select source file!';
/***** INSERTED *****/$_['ms_feed_error_source_url_not_specified'] = "Error: Source url is not specified!";
/***** INSERTED *****/$_['ms_feed_error_source_url_invalid'] = "Error: Invalid source url!";
/***** INSERTED *****/$_['ms_feed_error_source_file_invalid'] = "Error: Invalid source file!";

/***** INSERTED *****/$_['ms_feed_error_mapping_field'] = "Required product property '%s' is not mapped.";

// CSV columns captions
/***** INSERTED *****/$_['ms_feed_caption_model'] = "Model";
/***** INSERTED *****/$_['ms_feed_caption_sku'] = "SKU";
/***** INSERTED *****/$_['ms_feed_caption_price'] = "Price";
/***** INSERTED *****/$_['ms_feed_caption_quantity'] = "Quantity";
/***** INSERTED *****/$_['ms_feed_caption_manufacturer'] = "Manufacturer";
/***** INSERTED *****/$_['ms_feed_caption_image_url'] = "Primary image";
/***** INSERTED *****/$_['ms_feed_caption_images_url'] = "Additional images";
/***** INSERTED *****/$_['ms_feed_caption_categories_imploded'] = "Categories";
/***** INSERTED *****/$_['ms_feed_caption_currency_code'] = "Currency";
/***** INSERTED *****/$_['ms_feed_caption_weight'] = "Weight";
/***** INSERTED *****/$_['ms_feed_caption_weight_class'] = "Weight class";
/***** INSERTED *****/$_['ms_feed_caption_tax'] = "Tax class";
/***** INSERTED *****/$_['ms_feed_caption_name_single_lang'] = "Product name";
/***** INSERTED *****/$_['ms_feed_caption_name_multi_lang'] = "Product name (%s)";
/***** INSERTED *****/$_['ms_feed_caption_description_single_lang'] = "Product description";
/***** INSERTED *****/$_['ms_feed_caption_description_multi_lang'] = "Product description (%s)";
/***** INSERTED *****/$_['ms_feed_caption_category_single'] = "Category %s";

/***** INSERTED *****/$_['ms_feed_success_import_started'] = "Your products are being imported. We will notify you when it's done.";
/***** INSERTED *****/$_['ms_feed_import_name_template'] = "%s's products import";
/***** INSERTED *****/$_['ms_feed_import_schedule_title'] = "Import type";
/***** INSERTED *****/$_['ms_feed_import_schedule_yes'] = "Recurring import";
/***** INSERTED *****/$_['ms_feed_import_schedule_no'] = "One-time import";
/***** INSERTED *****/$_['ms_feed_import_schedule_template'] = "Run import every ";
/***** INSERTED *****/$_['ms_feed_import_schedule_5_minutes'] = "5 minutes";
/***** INSERTED *****/$_['ms_feed_import_schedule_30_minutes'] = "30 minutes";
/***** INSERTED *****/$_['ms_feed_import_schedule_1_hour'] = "hour";
/***** INSERTED *****/$_['ms_feed_import_schedule_4_hours'] = "4 hours";
/***** INSERTED *****/$_['ms_feed_import_schedule_12_hours'] = "12 hours";
/***** INSERTED *****/$_['ms_feed_import_schedule_24_hours'] = "day";

/***** INSERTED *****/$_['ms_feed_import_list'] = "Imports";
/***** INSERTED *****/$_['ms_feed_import_list_single'] = "Import history";
/***** INSERTED *****/$_['ms_feed_import_list_scheduled'] = "Scheduled imports";
/***** INSERTED *****/$_['ms_feed_import_list_scheduled_edit'] = "Edit scheduled import";
/***** INSERTED *****/$_['ms_feed_import_date_run'] = "Date run";
/***** INSERTED *****/$_['ms_feed_import_date_last_run'] = "Date last run";
/***** INSERTED *****/$_['ms_feed_import_date_next_run'] = "Date next run";
/***** INSERTED *****/$_['ms_feed_import_rows_processed'] = "Processed";
/***** INSERTED *****/$_['ms_feed_import_rows_added'] = "Added";
/***** INSERTED *****/$_['ms_feed_import_rows_updated'] = "Updated";
/***** INSERTED *****/$_['ms_feed_import_settings'] = "Import settings";
/***** INSERTED *****/$_['ms_feed_import_settings_url'] = "Feed URL";
/***** INSERTED *****/$_['ms_feed_import_settings_cycle'] = "Import schedule";
/***** INSERTED *****/$_['ms_feed_import_settings_cycle_note'] = "Cycle note";
/***** INSERTED *****/$_['ms_feed_import_settings_reschedule'] = "Reschedule next run";
/***** INSERTED *****/$_['ms_feed_import_settings_reschedule_note'] = "Reschedule next run note";
/***** INSERTED *****/$_['ms_feed_import_settings_history'] = "History";
/***** INSERTED *****/$_['ms_feed_import_status_message_0'] = "Import is queued.";
/***** INSERTED *****/$_['ms_feed_import_status_message_1'] = "Import has started.";
/***** INSERTED *****/$_['ms_feed_import_status_message_2'] = "Import is finished.";
/***** INSERTED *****/$_['ms_feed_import_settings_success_updated'] = "Scheduled import is successfully updated.";
/***** INSERTED *****/$_['ms_feed_import_settings_success_deleted'] = "Scheduled import is deleted.";
/***** INSERTED *****/$_['ms_feed_import_settings_error_import_id'] = "Unable to retrieve import id";


