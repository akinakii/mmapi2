<?php

$_['text_categories'] = "الأقسام";
$_['text_manufacturers'] = "Brands";
$_['text_price'] = "السعر";
$_['text_price_range_min'] = "أدنى";
$_['text_price_range_max'] = "أعلى";
$_['text_no_products'] = "لا يوجد أي منتجات تطابق الفلترة المختارة.";

$_['button_go'] = "<i class='fa fa-check'></i>";
