<?php
// Text
$_['text_title'] 			= 'PayPal';
$_['text_payment_form_title'] = 'الدفع عبر PayPal';

$_['text_success'] 			= 'لقد قمت بتعديل PayPal بنجاح';

$_['text_pp_address'] 		= 'حساب PayPal';

$_['text_receiver'] 		= 'المستلم';
$_['text_sender'] 			= 'المرسل';

$_['button_save'] 			= 'حفظ';

$_['error_pp_address'] 		= 'يجب تعبئة خانة حساب PayPal!';
$_['error_receiver_data'] 	= 'للأسف! لا توجد معلومات موضحة لوسيلة الدفع هذه. يرجى, <a href="%s" target="_blank">التواصل معنا</a> لمزيد من المعلومات.';
$_['error_sender_data'] 	= 'يجب عليك <a href="%s" target="_blank">تحديد</a> الإعدادات لوسيلة الدفع هذه!';

