<?php
// Text
$_['text_title'] 			= 'حساب ام فلوس';

$_['text_success'] 			= 'لقد تم تعديل معلومات حسابك بنجاح!';

$_['text_fullname'] 		= 'الاسم الكامل';
$_['text_momayaznum'] 		= 'رقم المميز';
$_['text_mfloosnum'] 		= 'رقم حساب ام فلوس';

$_['text_full_name'] 		= '%s';
$_['text_full_bank_name'] 	= 'حساب ام فلوس';
$_['text_receiver'] 		= 'المستلم';
$_['text_sender'] 			= 'المرسل';

$_['text_global_info'] 		= 'يجب أن تقوم بتحويل المال يدوياً إلى رقم حساب ام فلوس المسجل أدناه في خانة \'المستلم\'.';

$_['button_save'] 			= 'حفظ';

$_['error_fname'] 			= 'قم بكتابة اسمك الكامل!';
$_['error_momayaznum'] 			= 'قم بكتابة رقم المميز المكون من 6 أرقام!';
$_['error_mfloosnum'] 			= 'قم بكتابة رقم حساب ام فلوس الخاص بك!';

$_['error_receiver_data'] 	= 'للأسف! لا توجد معلومات حول طريقة الدفع هذه في الوقت الحالي. يرجى, <a href="%s" target="_blank">التواصل معنا</a>.';
$_['error_sender_data'] 	= 'يجب عليك أن <a href="%s" target="_blank">تحدد</a> إعدادات لوسيلة الدفع هذه!';
