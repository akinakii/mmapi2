<?php
// Text
$_['ms_import_text_title'] 			= 'استيراد المنتجات';
$_['ms_import_text_header'] 			= 'استيراد المنتجات';
$_['ms_import_text_account']        = 'الحساب';
$_['ms_import_text_start_new_import'] = 'استيراد البيانات';
$_['ms_import_text_example_url']   = 'تنزيل ملف CSV تجريبي';

$_['ms_import_text_name']          = 'الإسم';
$_['ms_import_text_date']          = 'التاريخ';
$_['ms_import_text_type']          = 'النوع';
$_['ms_import_text_processed']     = 'تم معالجته';
$_['ms_import_text_added']         = 'تم اضافته';
$_['ms_import_text_errors']        = 'الأخطاء';
$_['ms_import_text_actions']       = 'الإجراءات';

$_['ms_imports_text_imports']      = 'الاستيرادات';
$_['ms_imports_text_new_import']      = 'استيراد جديد';
$_['ms_imports_text_import_data']      = 'بيانات الاستيراد';
$_['ms_imports_text_import_filename']      = 'اسم ملف الاستيراد';
$_['ms_imports_text_import_continue']      = 'الحفظ والمتابعة';
$_['ms_imports_text_import_back']      = 'العودة';

//field_types
$_['ms_imports_text_field_type_sku']      		= 'sku';
$_['ms_imports_text_field_type_name']      		= 'الإسم';
$_['ms_imports_text_field_type_description']    = 'الوصف';
$_['ms_imports_text_field_type_price']      	= 'السعر';
$_['ms_imports_text_field_type_category']      	= 'كافة الأقسام';
$_['ms_imports_text_field_type_quantity']      	= 'الكمية';
$_['ms_imports_text_field_type_category1']      = 'مستوى القسم 1';
$_['ms_imports_text_field_type_category2']      = 'مستوى القسم 2';
$_['ms_imports_text_field_type_category3']      = 'مستوى القسم 3';
$_['ms_imports_text_field_type_image_url']      = 'الصورة الرئيسية';
$_['ms_imports_text_field_type_images_url']     = 'الصور الأخرى';
$_['ms_imports_text_field_type_model']      	= 'الموديل';
$_['ms_imports_text_field_type_currency']      	= 'العملة';
$_['ms_imports_text_field_type_weight']      	= 'الوزن';
$_['ms_imports_text_field_type_weight_class_id']= 'weight class_id';
$_['ms_imports_text_field_type_tax_class_id']   = 'الرقم التسلسلي لنوع الضريبة';
$_['ms_imports_text_field_type_available_to']   = 'متاح لـ';
$_['ms_imports_text_field_type_manufacturer']   = 'الشركة';

$_['ms_imports_text_field_type_product_name']   = 'الإسم';
$_['ms_imports_text_field_type_product_description']   = 'الوصف';

//steps
$_['ms_import_text_title_step1']   = 'رفع';
$_['ms_import_text_title_step2']   = 'الربط';
$_['ms_import_text_title_step3']   = 'تأكيد';

//step1
$_['ms_import_text_type_seller']   = 'البائعون';
$_['ms_import_text_type_product']  = 'المنتجات';
$_['ms_import_text_type_category'] = 'الأقسام';
$_['ms_import_text_select_config'] = 'اختر التهيئة';
$_['ms_import_text_select_you_config'] = 'اختر تهيئتك';

//step2
$_['ms_imports_text_import_steps2']   = 'الخطوة 1 من 3';
$_['ms_import_text_choose_file_for_import']   = 'اختر ملف الاستيراد:';
$_['ms_import_text_upload_file']   = 'رفع الملف:';
$_['ms_import_text_file_encoding']   = 'ترميز الملف:';
$_['ms_import_text_rows_limits']   = 'حد الصفوف:';
$_['ms_import_text_separators']   = 'الفواصل:';
$_['ms_import_text_cell_separator']   = 'الفاصل بين الصفوف:';
$_['ms_import_text_cell_container']   = 'الفاصل بين الخلايا:';
$_['ms_import_text_upload_file_note']   = 'الإمتدادات المسموحة: csv';
$_['ms_import_text_select_file']   = 'اختر ملف';

//step3
$_['ms_imports_text_import_steps3']   = 'الخطوة 2 من 3';
$_['ms_imports_text_import_file_field']      = 'خانة الملف';
$_['ms_imports_text_import_oc_field']      = 'خانة الموقع';
$_['ms_imports_text_import_select_oc_field_type']      = 'اختر نوع خانة الموقع';
$_['ms_imports_text_import']      = 'استيراد';
$_['ms_imports_text_column_label']      = 'عنوان العمود في الملف';
$_['ms_imports_text_product_property']      = 'خصائص المنتج';
$_['ms_imports_text_preview_information']      = 'معاينة المعلومات';

//step4
$_['ms_imports_text_import_steps4']   = 'الخطوة 3 من 3';
$_['ms_imports_text_import_update_config']      = 'تحديث التهيئة';
$_['ms_imports_text_import_save_config']      = 'حفظ التهيئة';
$_['ms_imports_text_import_save_config_success']      = 'تم حفظ تهيئتك!';
$_['ms_imports_text_import_update_config_success']      = 'تم تحديث تهيئتك!';
$_['ms_imports_text_samples_of_data']      = 'عينات من البيانات';
$_['ms_imports_text_file_column']      = 'العمود';
$_['ms_imports_text_not_specified'] = 'غير محدد';
$_['ms_import_text_start_row']   = 'الصف الأول';
$_['ms_import_text_finish_row']   = 'الصف الأخير';
$_['ms_import_text_default_quantity']   = 'الكمية الافتراضية';
$_['ms_import_text_default_product_status']   = 'حالة المنتج الافتراضية';
$_['ms_import_text_delimiter_category']   = 'تحديد القسم';
$_['ms_import_text_fill_category']   = 'قم بوضع القسم الرئيسي';
$_['ms_import_text_stock_status']   = 'حالة التوفر';
$_['ms_import_text_product_approved']   = 'تم قبول المنتج';
$_['ms_import_text_images_path']   = 'مسار الصور';
$_['ms_imports_text_yes']      = 'نعم';
$_['ms_imports_text_no']      = 'لا';
$_['ms_imports_text_enabled']      = 'تم التفعيل';
$_['ms_imports_text_disabled']      = 'تم التعطيل';
$_['ms_imports_text_config_name']      = 'اسم التهيئة';
$_['ms_imports_text_update_field']      = 'خانة التحديث';

//errors
$_['ms_import_text_error_type']    = 'نوع خاطئ!';
$_['ms_import_text_error_file']    = 'خطاً في الملف!';
$_['ms_import_text_error_encoding']    = 'خطأ في الترميز!';
$_['ms_import_text_error_parsing_columns']    = 'Parsing columns error!';
$_['ms_import_text_error_type_not_set']    = 'Type not set!';
$_['ms_import_text_error_file_not_set']    = 'File not set!';
$_['ms_import_text_error_edit_only_you_config']    = 'You can update only you config!';
$_['ms_import_text_error_no_data_for_import']    = 'Not found data for import!';
$_['ms_import_text_error_invalid_data_for_import']    = 'Invalid data for import!';
$_['ms_import_text_error_noset_rows_separator']    = 'Not set rows separator!';
$_['ms_import_text_error_noset_cells_separator']    = 'Not set cells separator!';

$_['ms_import_text_error_noset_sku']    = 'Not set property sku!';
$_['ms_import_text_error_noset_name']    = 'Not set property name!';

//results
$_['ms_import_text_result_all_rows'] = 'Processed rows: ';
$_['ms_import_text_result_new_rows_count'] = 'New products created: ';
$_['ms_import_text_result_update_rows_count'] = 'Existing products updated: ';
$_['ms_import_text_result_duplicate_rows_count'] = 'Duplicate rows found: ';