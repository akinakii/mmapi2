<?php

/***** INSERTED *****/$_['ms_language_version'] = '1.29.0.0';

// **********
// * Global *
// **********
$_['ms_viewinstore'] = 'العرض في المتجر';
$_['ms_view'] = 'عرض';
$_['ms_view_modify'] = 'عرض / تعديل';
$_['ms_view_invoice'] = 'عرض الفاتورة';
$_['ms_publish'] = 'نشر';
$_['ms_unpublish'] = 'عدم نشر';
$_['ms_activate'] = 'تفعيل';
$_['ms_deactivate'] = 'تعطيل';
$_['ms_edit'] = 'تعديل';
$_['ms_relist'] = 'إعادة وضع';
$_['ms_remove'] = 'Remove';
$_['ms_delete'] = 'حذف';
$_['ms_type'] = 'النوع';
$_['ms_amount'] = 'المبلغ';
$_['ms_status'] = 'الحالة';
$_['ms_date_paid'] = 'تاريخ الدفع';
$_['ms_last_message'] = 'آخر رسالة';
$_['ms_description'] = 'الوصف';
$_['ms_id'] = '#';
$_['ms_name'] = 'الاسم';
$_['ms_by'] = 'بواسطة';
$_['ms_action'] = 'الإجراءات';
$_['ms_sender'] = 'المرسل';
$_['ms_message'] = 'الرسالة';
$_['ms_none'] = 'لا شيء';
$_['ms_drag_drop_here'] = 'اسحب الملفات إلى هنا للرفع';
$_['ms_drag_drop_click_here'] = 'اسحب الصورة إلى هنا للرفع';
$_['ms_or'] = 'أو';
$_['ms_select_files'] = 'اختر الملفات';
$_['ms_allowed_extensions'] = 'الامتدادات المسموح بها: %s';
$_['ms_all_products'] = 'كافة المنتجات';
$_['ms_from'] = 'من';
$_['ms_to'] = 'إلى';
$_['ms_up_to'] = 'يصل إلى';
$_['ms_on'] = 'على';
$_['ms_store'] = 'المتجر';
$_['ms_store_owner'] = 'الإدارة';
$_['ms_sort_order'] = 'ترتيب الفرز';
$_['ms_yes'] = 'نعم';
$_['ms_no'] = 'لا';
$_['ms_add'] = 'إضافة';
$_['ms_reply'] = 'رد';
$_['ms_expand'] = 'توسيع';
$_['ms_collapse'] = 'طي';
$_['ms_days'] = 'يوم/أيام';
$_['ms_products_template'] = '%s product(s)';
/***** INSERTED *****/$_['ms_see_more'] = "See more";
/***** INSERTED *****/$_['ms_enabled'] = "Enabled";
/***** INSERTED *****/$_['ms_disabled'] = "Disabled";

$_['ms_default_select_value'] = '-- لا شيء --';
$_['ms_placeholder_deleted'] = "*محذوف*";
$_['ms_seller_deleted'] = '*البائع محذوف*';
$_['ms_product_deleted'] = '*المنتج محذوف*';

$_['ms_date_created'] = 'التاريخ';
$_['ms_date_added'] = 'تاريخ الإنشاء';
$_['ms_date_modified'] = 'تاريخ التعديل';
$_['ms_date'] = 'التاريخ';

$_['ms_button_submit'] = 'تأكيد';
$_['ms_button_add_special'] = '+ إضافة عرض خاص';
$_['ms_button_add_discount'] = '+ إضافة خصم';
$_['ms_button_save'] = 'حفظ';
$_['ms_button_cancel'] = 'إلغاء';
$_['ms_button_back'] = 'العودة';
$_['ms_button_pay'] = 'الدفع';

$_['ms_button_select_image'] = 'اختر صورة';

$_['ms_transaction_order_created'] = 'تم وضع الطلب';
$_['ms_transaction_sale'] = 'Sale: %s (-%s fees)';
$_['ms_transaction_sale_no_commission'] = 'Sale: %s';
$_['ms_transaction_sale_fee'] = 'Selling fee: %s';
$_['ms_transaction_sale_fee_order'] = 'Selling fee: Order #%s';
$_['ms_transaction_sale_fee_order_refund'] = 'Refund selling fee: Order #%s';
$_['ms_transaction_refund'] = 'المرتجعات: %s';
$_['ms_transaction_shipping'] = 'الشحن: %s';
$_['ms_transaction_shipping_order'] = 'Shipping: Order #%s';
$_['ms_transaction_shipping_refund'] = 'مرتجع الشحن: %s';
$_['ms_transaction_shipping_order_refund'] = 'Shipping refund: Order #%s';
$_['ms_transaction_coupon'] = 'Marketplace coupon discount';
$_['ms_transaction_coupon_refund'] = 'Marketplace coupon refund';
$_['ms_transaction_ms_coupon'] = 'Coupon discount';
$_['ms_transaction_ms_coupon_refund'] = 'Coupon refund';


// Mails

// Seller
$_['ms_mail_greeting'] = "مرحباً %s,\n";
$_['ms_mail_greeting_no_name'] = "مرحباً,\n";
$_['ms_mail_ending'] = "\nتحياتنا,\n%s";
$_['ms_mail_message'] = "\nالرسالة:\n%s";

$_['ms_mail_subject_seller_account_created'] = 'انشاء حساب بائع';
$_['ms_mail_seller_account_created'] = <<<EOT
تم إنشاء حساب البائع الخاص بك في %s !

تستطيع الآن إضافة منتجاتك.
EOT;

$_['ms_mail_subject_seller_account_awaiting_moderation'] = 'حساب البائع في انتظار المراجعة';
$_['ms_mail_seller_account_awaiting_moderation'] = <<<EOT
تم إنشاء حساب البائع الخاص بك في %s وهو قيد المراجعة.

سيتم اعلامك حالما تتم الموافقة على حسابك.
EOT;

$_['ms_mail_subject_product_awaiting_moderation'] = 'المنتج في انتظار المراجعة';
$_['ms_mail_product_awaiting_moderation'] = <<<EOT
المنتج: %s الموضوع في %s في انتظار الموافقة.

سيتم اعلامك حالما تتم الموافقة عليه.
EOT;

$_['ms_mail_subject_product_purchased'] = 'طلب جديد';
$_['ms_mail_product_purchased'] = <<<EOT
تم شراء منتج أو أكثر من منتجاتك في %s.

العميل: %s (%s)

المنتجات:
%s
الإجمالي: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
تم شراء منتج أو أكثر من منتجاتك في %s.

العميل: %s

المنتجات:
%s
الإجمالي: %s
EOT;

$_['ms_mail_subject_seller_contact'] = 'رسالة من عميل';
$_['ms_mail_seller_contact'] = <<<EOT
لقد استلمت رسالة جديدة من عميل!

العميل: %s

البريد الالكتروني: %s

المنتج: %s

الرسالة:
%s
EOT;

$_['ms_mail_seller_contact_no_mail'] = <<<EOT
لقد استلمت رسالة جديدة من عميل!

العميل: %s

المنتج: %s

الرسالة:
%s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
عنوان التوصيل:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'الملاحظات: %s';

$_['ms_mail_subject_product_reviewed'] = 'مراجعة جديدة لمنتج';
$_['ms_mail_product_reviewed'] = <<<EOT
تم وضع تقييم جديد لمنتج %s.
يرجى زيارة الرابط التالي للمشاهدة: <a href="%s">%s</a>
EOT;

$_['ms_mail_subject_product_returned'] = 'طلب ارجاع منتج';
$_['ms_mail_product_returned'] = <<<EOT
قام عميل بطلب إرجاع المنتج %s. سيقوم أحد المسؤولين في %s بالتواصل معك قريباً.
EOT;

$_['ms_mail_subject_withdraw_request_submitted'] = 'طلب المستحقات';
$_['ms_mail_withdraw_request_submitted'] = <<<EOT
لقد استلمنا طلبك بنجاح. سيتم تسليم أرباحك حالما يتم معالجة طلبك.
EOT;

$_['ms_mail_subject_withdraw_request_completed'] = 'اكتملت عملية الدفع';
$_['ms_mail_withdraw_request_completed'] = <<<EOT
تم معالجة طلب مستحقاتك. سوف تتلقى أرباحك الآن.
EOT;

$_['ms_mail_subject_withdraw_request_declined'] = 'رفض طلب الدفع';
$_['ms_mail_withdraw_request_declined'] = <<<EOT
تم رفض طلب الدفع الخاص بك. تم إعادة المستحقات إلى رصيدك في %s.
EOT;

$_['ms_mail_subject_transaction_performed'] = 'معاملة جديدة';
$_['ms_mail_transaction_performed'] = <<<EOT
تم إضافة معاملة جديدة إلى حسابك في %s.
EOT;

$_['ms_mail_subject_remind_listing'] = 'انتهاء صلاحية نشر منتجك';
$_['ms_mail_seller_remind_listing'] = <<<EOT
انتهت فترة نشر منتجك في %s. اذهب إلى قسم البائع في حسابك لإعادة نشر المنتج.
EOT;

// *********
// * Admin *
// *********
$_['ms_mail_admin_subject_seller_account_created'] = 'انشاء حساب بائع جديد';
$_['ms_mail_admin_seller_account_created'] = <<<EOT
تم انشاء حساب بائع جديد في  %s!
اسم البائع: %s (%s)
البريد الالكتروني: %s
EOT;

$_['ms_mail_admin_subject_seller_account_awaiting_moderation'] = 'حساب بائع بإنتظار الموافقة';
$_['ms_mail_admin_seller_account_awaiting_moderation'] = <<<EOT
حساب بائع جديد في %s قد تم انشاءه وهو في انتظار موافقتك.
اسم البائع: %s (%s)
البريد الالكتروني: %s

تستطيع معالجة الطلب في قسم  Multiseller - البائعون في صفحة الإدارة.
EOT;

$_['ms_mail_admin_subject_product_created'] = 'اضافة منتج جديد';
$_['ms_mail_admin_product_created'] = <<<EOT
تم إضافة المنتج: %s إلى %s.

تستطيع عرضه أو تعديله من صفحة الإدارة.
EOT;

$_['ms_mail_admin_subject_new_product_awaiting_moderation'] = 'منتج جديد بإنتظار الموافقة';
$_['ms_mail_admin_new_product_awaiting_moderation'] = <<<EOT
تم إضافة المنتج: %s إلى %s وهو بإنتظار الموافقة.

تستطيع معالجة الطلب في قسم  Multiseller - المنتجات في صفحة الإدارة.
EOT;

$_['ms_mail_admin_subject_edit_product_awaiting_moderation'] = 'تعديل منتج بإنتظار الموافقة';
$_['ms_mail_admin_edit_product_awaiting_moderation'] = <<<EOT
تم تعديل المنتج: %s إلى %s وهو بإنتظار الموافقة.

تستطيع معالجة الطلب في قسم  Multiseller - المنتجات في صفحة الإدارة.
EOT;

$_['ms_mail_admin_subject_withdraw_request_submitted'] = 'طلب مستحقات بانتظار الموافقة';
$_['ms_mail_admin_withdraw_request_submitted'] = <<<EOT
طلب مستحقات جديد تم وضعه بإنتظار موافقتك.

تستطيع معالجة الطلب في قسم  Multiseller - المالية في صفحة الإدارة.
EOT;

// Catalog - Mail
// Attributes
$_['ms_mail_subject_attribute_created'] = 'مواصفة جديدة أضافها البائع %s: %s';
$_['ms_mail_attribute_created'] = <<<EOT
مواصفة جديدة وضعها البائع <strong>%s</strong>: <strong>%s</strong>.

<a href="%s">اضغط هنا</a> للموافقة، الرفض أو إدارة المواصفات.
EOT;

// Attribute groups
$_['ms_mail_subject_attribute_group_created'] = 'مجموعة مواصفات جديدة أضافها البائع %s: %s';
$_['ms_mail_attribute_group_created'] = <<<EOT
مجموعة مواصفات جديدة وضعها البائع <strong>%s</strong>: <strong>%s</strong>.

<a href="%s">اضغط هنا</a> للموافقة، الرفض أو إدارة مجموعات المواصفات.
EOT;

// Options
$_['ms_mail_subject_option_created'] = 'خيار جديد أضافه البائع %s: %s';
$_['ms_mail_option_created'] = <<<EOT
خيار جديد أضافه البائع <strong>%s</strong>: <strong>%s</strong>.

<a href="%s">اضغط هنا</a> للموافقة، الرفض أو إدارة الخيارات.
EOT;

// Categories
$_['ms_mail_subject_category_created'] = 'قسم جديد أضافه البائع %s: %s';
$_['ms_mail_category_created'] = <<<EOT
قسم جديد أضافه البائع <strong>%s</strong>: <strong>%s</strong>.

<a href="%s">اضغط هنا</a> للموافقة، الرفض أو إدارة الأقسام.
EOT;


// Success
$_['ms_success_product_published'] = 'تم نشر المنتج';
$_['ms_success_product_unpublished'] = 'تم إلغاء نشر المنتج';
$_['ms_success_product_created'] = 'تم إنشاء المنتج';
$_['ms_success_product_updated'] = 'تم تحديث المنتج';
$_['ms_success_product_deleted'] = 'تم حذف المنتج';

// Errors
$_['ms_error_sellerinfo_nickname_empty'] = 'الاسم المستعار لا يمكن أن يكون فارغاً';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'الاسم الاستعار يجب أن يحوي حروف لاتينية وأرقام فقط';
$_['ms_error_sellerinfo_nickname_utf8'] = 'الاسم المستعار يجب أن يحوي رموز UTF-8 قابلة للطباعة فقط';
$_['ms_error_sellerinfo_nickname_latin'] = 'الاسم الاستعار يجب أن يحوي حروف لاتينية وأرقام وتشكيلات فقط';
$_['ms_error_sellerinfo_nickname_length'] = 'الاسم المستعار يجب أن يكون بين 4 و 32 حرفاً';
$_['ms_error_sellerinfo_nickname_taken'] = 'الاسم المستعار هذا قد تم أخذه';
$_['ms_error_sellerinfo_description_length'] = 'يجب أن لا يزيد الوصف عن 5000 حرف';
$_['ms_error_sellerinfo_terms'] = 'تحذير: يجب عليك أن توافق على %s!';

$_['ms_error_file_extension'] = "خطأ في رفع %s: امتداد الملف (%s) غير مسموح به (%s).";
$_['ms_error_file_type'] = "خطأ في رفع %s: نوع الملف غير صالح.";
$_['ms_error_file_post_size'] = "خطأ في رفع %s: تم تجاوز post_max_size (%s) للخادم.";
$_['ms_error_file_upload_size'] = "خطأ في رفع %s: تم تجاوز upload_max_filesize (%s) للخادم.";
$_['ms_error_file_upload_error'] = 'خطأ في رفع %s: %s.';
$_['ms_file_default_filename'] = 'الملف';
$_['ms_file_unclassified_error'] = 'خطأ غير معرف';
$_['ms_file_filename_error_greater'] = 'اسم الملف أكبر من %s حرفاً';
$_['ms_file_filename_error_less'] = 'اسم الملف أصغر من %s حرف';
$_['ms_file_cross_session_upload'] = "الملف ليس في الجلسة الحالية";
$_['ms_file_cross_product_file'] = "الملف لا ينتمي إلى هذا المنتج";

$_['ms_error_image_too_small'] = 'أبعاد الصورة صغيرة جداً. الحد الأدنى للأبعاد المسموح به: %s × %s (العرض × الطول)';
$_['ms_error_image_too_big'] = 'أبعاد الصورة كبيرة للغاية. الحد الأعلى للأبعاد المسموح به: %s × %s (العرض × الطول)';
$_['ms_error_form_submit_error'] = 'حدث خطأ أثناء إرسال النموذج. يرجى التواصل مع الإدارة للمزيد من المعلومات.';
$_['ms_error_form_notice'] = 'يرجى التحقق من عدم وجود أخطاء في جميع علامات تبويب النموذج.';
$_['ms_error_product_price_empty'] = 'يرجى تحديد سعر للمنتج';
$_['ms_error_product_price_invalid'] = 'السعر غير صالح';
$_['ms_error_product_price_low'] = 'السعر منخفض جداً';
$_['ms_error_product_price_high'] = 'السعر مرتفع جداً';
$_['ms_error_product_category_empty'] = 'يرجى اختيار القسم';
$_['ms_error_product_model_empty'] = 'موديل المنتج لا يمكن أن يكون فارغاً';
$_['ms_error_product_model_length'] = 'الموديل يجب أن يكون بين %s و %s حرفاً';
$_['ms_error_product_image_count'] = 'يرجى رفع %s صور(ة) على الأقل لمنتجك';
$_['ms_error_product_download_count'] = 'يرجى رفع %s ملف تنزيل على الأقل لمنتجك';
$_['ms_error_product_image_maximum'] = 'لا يمكنك إضافة أكثر من %s صور(ة)';
$_['ms_error_product_download_maximum'] = 'لا يمكنك إضافة أكثر من %s ملف تنزيل';
$_['ms_error_contact_text'] = 'الرسالة يجب أن لا تزيد عن 2000 حرف';
$_['ms_error_contact_allfields'] = 'يرجى تعبئة جميع الحقول';
$_['ms_error_invalid_quantity_discount_priority'] = 'خطأ في حقل الأولوية - الرجاء إدخال قيمة صالحة';
$_['ms_error_invalid_quantity_discount_quantity'] = 'الكمية يجب أن تكون 2 أو أكثر';
$_['ms_error_invalid_quantity_discount_price'] = 'تم إدخال سعر خصم غير صالح';
$_['ms_error_invalid_quantity_discount_dates'] = 'يجب ملء حقول التاريخ للخصومات الكمية';
$_['ms_error_invalid_special_price_priority'] = 'خطأ في حقل الأولوية - الرجاء إدخال قيمة صالحة';
$_['ms_error_invalid_special_price_price'] = 'تم إدخال سعر خاص غير صالح';
$_['ms_error_invalid_special_price_dates'] = 'يجب ملء حقول التاريخ للأسعار الخاصة';
$_['ms_error_slr_gr_product_number_limit_exceeded'] = 'لقد قمت بإنشاء الحد الأقصى للمنتجات المسموح به لحسابك. يرجى التواصل معنا لمزيد من المعلومات بخصوص رفع حدود المنتجات.';
$_['ms_error_fixed_coupon_warning'] = "تحذير: لا يمكن استخدام رمز الكوبون هذا في الوقت الحالي!";
$_['ms_error_voucher_warning'] = "تحذير: قسيمة الهدية هذه لا يمكن استخدامها في الوقت الحالي!";
$_['ms_error_product_forbid_to_buy_own_product'] = "لا يمكنك شراء منتجاتك!";

// Account - General
$_['ms_account_register_new'] = 'بائع جديد';
$_['ms_account_register_customer'] = "تسجيل حساب عميل";
$_['ms_account_register_seller'] = 'تسجيل حساب بائع';
$_['ms_account_register_seller_note'] = 'قم بالتسجيل كبائع وابدأ ببيع منتجاتك!';
$_['ms_account_register_account_information'] = "معلومات الحساب";
$_['ms_account_register_firstname'] = "الاسم الثلاثي";
$_['ms_account_register_lastname'] = "اللقب";
$_['ms_account_register_email'] = "البريد الالكتروني";
$_['ms_account_register_password'] = "كلمة المرور";
$_['ms_account_register_password_confirm'] = "تأكيد كلمة المرور";
$_['ms_account_register_store_information'] = "معلومات المتجر";
$_['ms_account_register_store_name'] = "اسم المتجر";
$_['ms_account_register_slogan'] = "الشعار النصي";
$_['ms_account_register_description'] = "الوصف";
$_['ms_account_register_website'] = "الموقع الالكتروني";
$_['ms_account_register_company'] = "الشركة";
$_['ms_account_register_phone'] = "الهاتف";
$_['ms_account_register_avatar'] = "الشعار";
$_['ms_account_register_banner'] = "الغلاف";
$_['ms_account_register_address_information'] = "معلومات العنوان";
$_['ms_account_register_address_fullname'] = "الاسم الكامل";
$_['ms_account_register_address_line1'] = "العنوان 1";
$_['ms_account_register_address_line2'] = "العنوان 2";
$_['ms_account_register_city'] = "المدينة";
$_['ms_account_register_state'] = "الولاية/المحافظة";
$_['ms_account_register_zip'] = "الرمز البريدي";
$_['ms_account_register_country'] = "الدولة";
$_['ms_account_register_success_created'] = "مرحباً بك في حسابك الجديد كبائع!";

$_['ms_account_profile_general'] = "معلومات عامة";
$_['ms_account_profile_group'] = "اختر الخطة المناسبة لك";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_SALE] = "%s + %s%% رسوم مبيعات";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_LISTING] = "%s + %s%% رسوم نشر";
$_['ms_account_seller_group_fee_' . MsCommission::RATE_SIGNUP] = "%s رسوم تسجيل";
$_['ms_account_group_select_plan'] = "اختر";

$_['ms_seller'] = 'البائع';
$_['ms_seller_account_heading'] = 'حساب البائع';
$_['ms_catalog'] = 'القسم';
$_['ms_account_dashboard'] = 'لوحة المراقبة';
$_['ms_account_customer'] = 'العميل';
$_['ms_account_my_account'] = 'حسابي';
$_['ms_account_overview'] = 'نظرة عامة';
$_['ms_account_sellerinfo'] = 'الصفحة الشخصية للبائع';
$_['ms_account_sellerinfo_new'] = 'إنشاء حساب بائع جديد';
$_['ms_account_sellerinfo_new_short'] = 'حساب جديد';
$_['ms_account_newproduct'] = 'إضافة منتج';
$_['ms_account_products'] = 'المنتجات';
$_['ms_account_transactions'] = 'الحسابات المالية';
$_['ms_account_payments'] = 'المدفوعات';
$_['ms_account_payment_requests'] = 'الفواتير';
$_['ms_account_import'] = 'استيراد المنتجات من ملف CSV';
/***** INSERTED *****/$_['ms_account_import_list'] = "View imports";
$_['ms_account_orders'] = 'الطلبات';
$_['ms_account_revenue'] = 'الإيرادات';
$_['ms_account_views'] = 'المشاهدات';
$_['ms_account_withdraw'] = 'طلب سحب';
$_['ms_account_stats'] = 'الإحصائيات';
$_['ms_account_settings'] = 'الإعدادات';
$_['ms_account_profile'] = 'الملف الشخصي';
$_['ms_account_member_since'] = 'Member since';
$_['ms_account_dashboard_in_month'] = 'هذا الشهر';
$_['ms_account_no_orders'] = 'ليس لديك طلبات.';
$_['ms_account_general'] = 'عام';
$_['ms_account_to_customer_account'] = 'الذهاب إلى حسابي كعميل >';
$_['ms_account_to_seller_account'] = 'الذهاب إلى حسابي كبائع >';
/***** INSERTED *****/$_['ms_seller_fields'] = 'Product fields';
/***** INSERTED *****/$_['ms_seller_attributes'] = 'Attributes';

$_['ms_account_payment_requests_empty'] = "ليس لديك أي فواتير حالياً.";

// customer account

$_['ms_account_edit_info'] = 'تعديل المعلومات';
$_['ms_account_password'] = 'تغيير كلمة السر';
$_['ms_account_wishlist'] = 'المفضلة';
$_['ms_account_newsletter'] = 'القائمة البريدية';
$_['ms_account_address_book'] = 'دفتر العناوين';
$_['ms_account_order_history'] = 'المشتريات';
$_['ms_account_reward_points'] = 'نقاط المكافآت';
$_['ms_account_returns'] = 'المرتجعات';
$_['ms_account_logout'] = 'تسجيل الخروج';

// Disqus
$_['mxt_disqus_comments'] = 'تعليقات Disqus';

// Analytics
$_['mxt_google_analytics'] = 'تحليلات Google';
$_['mxt_google_analytics_code'] = 'رقم التتبع (Tracking ID)';
$_['mxt_google_analytics_code_note'] = 'قم بوضع معرف تتبع تحليلات Google لتتبع أداء منتجاتك وصفحتك الشخصية';

// Badges

// Soclal links
$_['ms_sl_social_media'] = 'وسائل التواصل الاجتماعي';

// Account - New product
$_['ms_account_newproduct_heading'] = 'منتج جديد';
//General Tab
$_['ms_account_product_tab_specials'] = 'عروض خاصة';
$_['ms_account_product_tab_discounts'] = 'أسعار الجملة';
$_['ms_account_product_name_description'] = 'معلومات عامة';
$_['ms_account_product_additional_data'] = 'بيانات إضافية';
$_['ms_account_product_search_optimization'] = 'تحسين البحث';

$_['ms_account_product_name'] = 'الإسم';
$_['ms_account_product_name_note'] = 'ضع إسماً لمنتجك';

$_['ms_account_product_description'] = 'الوصف';
$_['ms_account_product_description_note'] = 'صِف منتجك. تأكد أن وصفك مكتمل ودقيق لجذب المزيد من المشتريين';

$_['ms_account_product_meta_description'] = 'وصف الميتا';
$_['ms_account_product_meta_description_note'] = 'يتم استخدام وصف الميتا من قبل محركات البحث لوصف منتجك.';

$_['ms_account_product_seo_keyword'] = 'رابط SEO';
$_['ms_account_product_seo_keyword_note'] = 'يتم عرضها في رابط منتجك. يجب عدم استخدام المسافات والرموز.';

$_['ms_account_product_meta_title'] = 'عنوان الميتا';
$_['ms_account_product_meta_title_note'] = 'عنوان الميتا هو ما يظهر كعنوان لمنتجك';

$_['ms_account_product_meta_keyword'] = 'كلمات الميتا الدلالية';
$_['ms_account_product_meta_keyword_note'] = 'قد تستخدم الكلمات الدلالية من قبل محركات البحث لتحديد محتوى منتجك';

$_['ms_account_product_tags'] = 'الكلمات الدلالية';
$_['ms_account_product_tags_note'] = 'ضع قائمة من الكلمات الدلالية المناسبة لوصف منتجك قم بوضع هذه الفاصلة التي بين القوسين (,) بين الكلمات';

$_['ms_account_product_price'] = 'السعر';
/***** INSERTED *****/$_['ms_account_product_unit_price'] = 'Unit Price';
$_['ms_account_product_price_note'] = 'ضع سعر منتجك (مثال: %s1%s000%s00%s)';

$_['ms_account_product_digital'] = 'منتج رقمي';

$_['ms_account_product_attribute'] = 'الصفة';
$_['ms_account_product_attributes'] = 'المواصفات';
$_['ms_account_product_value'] = 'القيمة';
$_['ms_account_product_new_attribute'] = '+ إضافة صفة';

$_['ms_account_product_listing_balance'] = "This amount will be automatically deducted from your balance.";
$_['ms_account_product_listing_pg'] = "A payable invoice for this amount will be generated in the Invoices section of your account.";

$_['ms_account_product_listing_flat'] = 'رسوم نشر هذا المنتج هي <span>%s</span>';
$_['ms_account_product_listing_percent'] = 'رسوم نشر المنتج تعتمد على سعره. رسوم نشر هذا المنتج: <span>%s</span>.';

$_['ms_account_product_listing_fee_category'] = "The listing fee for this product is calculated based on product's category. Current listing fee: %s.";
$_['ms_account_product_listing_fee_seller'] = "The listing fee for this product is calculated based on your group. Current listing fee: %s.";
$_['ms_account_product_listing_fee_combined'] = "The listing fee for this product is calculated based on your group and product's category. Current listing fee: %s.";

$_['ms_account_product_categories'] = 'أقسام المتجر';
$_['ms_account_product_vendor_categories'] = 'أقسام صفحتك الشخصية';
$_['ms_account_product_category_select'] = 'Please select category';
$_['ms_account_product_category_add'] = '+ Add secondary category';
$_['ms_account_product_category_add_more'] = '+ Add more categories';
$_['ms_account_product_category_note'] = 'اختر القسم المناسب لمنتجك للسماح للمشتريين بالعثور على منتجك';
$_['ms_account_product_vendor_category_note'] = 'اختر القسم المناسب لمنتجك من قائمة  الأقسام الخاصة بك';
$_['ms_account_product_error_category_not_childmost'] = "Please select a category";
$_['ms_account_product_quantity'] = 'الكمية';
$_['ms_account_product_quantity_note']    = 'حدد الكمية المتوفرة من منتجك';
$_['ms_account_product_minorderqty'] = 'الحد الأدنى للكمية';
$_['ms_account_product_minorderqty_note']    = 'حدد الحد الأدنى للكمية عند طلب شراء منتجك';
$_['ms_account_product_files'] = 'الملفات';
$_['ms_account_product_download'] = 'التنزيلات';
$_['ms_account_product_download_note'] = 'ارفع ملفات لمنتجك. الامتدادات المسموح بها: %s';
$_['ms_account_product_image'] = 'الصور';
$_['ms_account_product_image_note'] = 'ارقع صور منتجك. سيتم التعامل مع أول صورة كصورة أساسية للمنتج. تستطيع تغيير ترتيب الصور عبر السحب. الامتدادات المسموح بها: %s';
//Data Tab
$_['ms_account_product_model'] = 'الموديل';
$_['ms_account_product_sku'] = 'SKU';
$_['ms_account_product_sku_note'] = 'وحدة حفظ المخزون';
$_['ms_account_product_upc']  = 'UPC';
$_['ms_account_product_upc_note'] = 'الرمز العالمي للمنتج';
$_['ms_account_product_ean'] = 'EAN';
$_['ms_account_product_ean_note'] = 'الرمز الأوروبي للمنتج';
$_['ms_account_product_jan'] = 'JAN';
$_['ms_account_product_jan_note'] = 'الرمز الياباني للمنتج';
$_['ms_account_product_isbn'] = 'ISBN';
$_['ms_account_product_isbn_note'] = 'الرقم العالمي الموحد للكتاب';
$_['ms_account_product_mpn'] = 'MPN';
$_['ms_account_product_mpn_note'] = 'رقم قطعة المصنع';
$_['ms_account_product_manufacturer'] = 'الشركة';
$_['ms_account_product_manufacturer_note'] = '(تكملة تلقائية)';
$_['ms_account_product_tax_class'] = 'فئة الضريبة';
$_['ms_account_product_date_available'] = 'تاريخ التوفر';
$_['ms_account_product_stock_status'] = 'حالة نفاذ الكمية';
$_['ms_account_product_subtract'] = 'الخصم من الكمية';
$_['ms_account_product_customer_group'] = 'مجموعة العميل';
$_['ms_account_product_msf_variations'] = "Variations";
$_['ms_account_product_msf_variations_variants'] = "Variants";
$_['ms_account_product_msf_variations_price'] = "Price";
$_['ms_account_product_msf_variations_quantity'] = "Quantity";
$_['ms_account_product_msf_variations_status'] = "Enabled";
$_['ms_account_product_msf_variations_not_selected'] = "Please, select at least one variant for %s";
$_['ms_account_product_msf_variations_select_category'] = "Please, select category to get available options for a product.";
$_['ms_account_product_msf_variation_error_quantity'] = "Please, set quantity for product variant";
$_['ms_account_product_msf_variation_error_price_invalid'] = "Invalid price for product variant %s";
$_['ms_account_product_msf_variation_error_price_empty'] = "Please, specify a price for product variant %s";
$_['ms_account_product_msf_variation_error_price_price_low'] = "Price too low for product variant %s";
$_['ms_account_product_msf_variation_error_price_price_high'] = "Price too high for product variant %s";
$_['ms_account_product_msf_variation_error_quantity'] = "Please, specify a quantity for product variant %s";

// Options
$_['ms_account_product_tab_options'] = 'الخيارات';
$_['ms_options_add'] = '+ إضافة خيار';
$_['ms_options_add_value'] = '+ إضافة قيمة';
$_['ms_options_price'] = 'السعر';
$_['ms_options_subtract'] = 'خصم';
$_['ms_options_quantity'] = 'الكمية';
$_['ms_options_values'] = 'القيّم';
$_['ms_options_required'] = 'إلزامي؟';
$_['ms_options_remove'] = 'حذف';

$_['ms_account_product_manufacturer'] = 'الشركة';
$_['ms_account_product_manufacturer_note'] = '(تكملة تلقائية)';
$_['ms_account_product_tax_class'] = 'فئة الضريبة';
$_['ms_account_product_date_available'] = 'تاريخ التوفر';
$_['ms_account_product_stock_status'] = 'حالة نفاذ الكمية';
$_['ms_account_product_subtract'] = 'الخصم من الكمية';

$_['ms_account_product_priority'] = 'الأولوية';
$_['ms_account_product_date_start'] = 'تاريخ البدء';
$_['ms_account_product_date_end'] = 'تاريخ الانتهاء';



// Account - Edit product
$_['ms_account_editproduct_heading'] = 'تعديل المنتج';

// Account - Seller
$_['ms_account_sellerinfo_heading'] = 'الملف الشخصي للبائع';
$_['ms_account_sellerinfo_breadcrumbs'] = 'الملف الشخصي للبائع';
$_['ms_account_sellerinfo_nickname'] = 'اسم المتجر';
$_['ms_account_sellerinfo_nickname_note'] = 'حدد اسماً لمتجرك.';
$_['ms_account_sellerinfo_slogan'] = 'الشعار النصي';
$_['ms_account_sellerinfo_slogan_note'] = 'صف متجرك في عبارة واحدة';
$_['ms_account_sellerinfo_description'] = 'الوصف';
$_['ms_account_sellerinfo_description_note'] = 'صف  متجرك';
$_['ms_account_sellerinfo_avatar'] = 'الشعار';
$_['ms_account_sellerinfo_avatar_note'] = 'ضع شعاراً لمتجرك';
$_['ms_account_sellerinfo_banner'] = 'الغلاف';
$_['ms_account_sellerinfo_banner_note'] = 'ارفع صورة ليتم استخدامها كغلاف لصفحتك الشخصية';
$_['ms_account_sellerinfo_website'] = "الموقع الالكتروني";
$_['ms_account_sellerinfo_website_note'] = "ضع موقعك الالكتروني";
$_['ms_account_sellerinfo_company'] = "الشركة";
$_['ms_account_sellerinfo_company_note'] = "حدد اسم شركتك";
$_['ms_account_sellerinfo_phone'] = "الهاتف";
$_['ms_account_sellerinfo_phone_note'] = "ضع رقم هاتفك";
$_['ms_account_sellerinfo_fee_flat'] = 'هناك رسوم تسجيل ل<span>%s</span> لتكون بائعاً في %s.';
$_['ms_account_sellerinfo_fee_balance'] = 'سيتم خصم هذا المبلغ من رصيدك الأولي.';
$_['ms_account_sellerinfo_fee_pg'] = 'يمكنك العثور على طلبات الرسوم في قسم الفواتير.';
$_['ms_account_sellerinfo_saved'] = 'تم حفظ بيانات حساب البائع.';
$_['ms_account_sellerinfo_logo_note'] = "اختر شعارك (يتم عرضه في الفواتير)";

$_['ms_account_status'] = 'حالة حساب البائع: ';
$_['ms_account_status_tobeapproved'] = ' سوف تتمكن من استخدام حسابك حالما تتم الموافقة من الإدارة.';
$_['ms_account_status_please_fill_in'] = 'يرجى تعبئة النموذج التالي لإنشاء حساب البائع.';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'مفعل';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'غير مفعل';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'معطل';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'غير مكتمل';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'محذوف';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'لم يتم تسديد رسوم التسجيل';

// Account - Products
$_['ms_account_products_heading'] = 'منتجاتك';
$_['ms_account_products_breadcrumbs'] = 'منتجاتك';
$_['ms_account_products_product'] = 'المنتج';
$_['ms_account_products_sales'] = 'المبيعات';
$_['ms_account_products_earnings'] = 'الأرباح';
$_['ms_account_products_status'] = 'الحالة';
$_['ms_account_products_confirmdelete'] = 'هل تريد حذف هذا المنتج؟';
$_['ms_account_products_empty'] = "ليس لديك أي منتجات حتى الآن.";

$_['ms_not_defined'] = 'غير محدد';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'مفعل';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'غير مفعل';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'معطل';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'محذوف';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'رسوم نشر غير مسددة';
$_['ms_product_status_' . MsProduct::STATUS_IMPORTED] = 'تم استيراده';

$_['ms_import_text_results'] = 'نتائج الإستيراد:';

// Account - Conversations and Messages
$_['ms_account_conversations'] = 'المحادثات';
$_['ms_account_messages'] = 'الرسائل';
$_['ms_sellercontact_success'] = 'تم إرسال رسالتك بنجاح';
$_['ms_account_conversations_empty'] = "ليس لديك محادثات حالياً.";

$_['ms_account_conversations_heading'] = 'محادثاتك';
$_['ms_account_conversations_breadcrumbs'] = 'محادثاتك';

$_['ms_account_conversations_status'] = 'الحالة';
$_['ms_account_conversations_with'] = 'المحادثة مع';
$_['ms_account_conversations_title'] = 'العنوان';
$_['ms_account_conversations_type'] = 'نوع المحادثة';
$_['ms_account_conversations_textarea_placeholder'] = 'اكتب رسالتك...';

$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_CUSTOMER] = 'المشتري';
$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_SELLER] = 'البائع';
$_['ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_ADMIN] = 'الإدارة';


$_['ms_conversation_title_product'] = 'استفسار بخصوص المنتج: %s';
$_['ms_conversation_title_order'] = 'استفسار بخصوص الطلب: %s';
$_['ms_conversation_title'] = 'استفسار من %s';
$_['ms_conversation_customer_deleted'] = '*تم حذف العميل*';

$_['ms_account_conversations_read'] = 'مقروء';
$_['ms_account_conversations_unread'] = 'غير مقروء';

$_['ms_account_conversations_start_with_seller'] = 'بدء محادثة جديدة مع %s';
$_['ms_account_conversations_start_with_customer'] = 'بدء محادثة جديدة مع العميل %s';

$_['ms_account_messages_heading'] = 'الرسائل';

$_['ms_message_text'] = 'رسالتك';
$_['ms_post_message'] = 'إرسال';

$_['ms_customer_does_not_exist'] = 'تم حذف حساب العميل';
$_['ms_error_empty_message'] = 'لا يمكن ترك خانة الرسالة فارغاً';

$_['ms_mail_subject_private_message'] = 'تم استلام رسالة خاصة';
$_['ms_mail_private_message'] = <<<EOT
لقد استلمت رسالة خاصة من %s!

%s

%s

تستطيع الرد عليها في قسم الرسائل في حسابك.
EOT;

$_['ms_mail_subject_order_updated'] = 'تم تحديث طلبك #%s بواسطة %s';
$_['ms_mail_order_updated'] = <<<EOT
تم تحديث طلبك %s بواسطة %s:

الطلب#: %s

المنتجات:
%s

الحالة: %s

الملاحظات:
%s

EOT;

$_['ms_mail_subject_seller_vote'] = 'التصويت للبائع';
$_['ms_mail_seller_vote_message'] = 'التصويت للبائع';

// Account - Transactions
$_['ms_account_transactions_heading'] = 'الأرصدة';
$_['ms_account_transactions_breadcrumbs'] = 'الأرصدة';
$_['ms_account_transactions_earnings'] = 'أرباحك إلى اليوم:';
$_['ms_account_transactions_description'] = 'الوصف';
$_['ms_account_transactions_amount'] = 'المبلغ';
$_['ms_account_transactions_empty'] = "لا يوجد لديك أي عمليات حالياً.";

// Payments
$_['ms_payment_payments_heading'] = 'مدفوعاتك';
$_['ms_payment_payments'] = 'المدفوعات';

// Account - Orders
$_['ms_account_orders_heading'] = 'سجل مبيعاتك';
$_['ms_account_orders_breadcrumbs'] = 'سجل مبيعاتك';
$_['ms_account_orders_id'] = '#';
$_['ms_account_orders_customer'] = 'العميل';
$_['ms_account_orders_products'] = 'المنتجات';
$_['ms_account_orders_transactions'] = 'الحسابات';
$_['ms_account_orders_history'] = 'سجل حالة الطلب';
$_['ms_account_orders_marketplace_history'] = 'سجل حالة المتجر';
$_['ms_account_orders_addresses'] = 'العناوين';
$_['ms_account_orders_total'] = 'الإجمالي';
$_['ms_account_orders_noorders'] = 'لا يوجد لديك طلبات حالياً.';
$_['ms_account_orders_notransactions'] = 'لا يوجد أي معاملات مالية لهذا الطلب حالياً!';
$_['ms_account_orders_nohistory'] = 'لا يوجد سجل لهذا الطلب حتى الآن!';
$_['ms_account_orders_comment'] = 'ملاحظات';
$_['ms_account_orders_add_comment'] = 'دع المشتري يعلم سبب تغيير حالة الطلب...';
$_['ms_account_orders_status_select_default'] = '-- اختر الحالة --';
$_['ms_account_orders_change_status'] = 'تغيير حالة الطلب';
$_['ms_account_orders_attachments']    = 'المرفقات';
$_['ms_account_orders_store_commission_deducted'] = 'تم خصم عمولة المتجر';
/***** INSERTED *****/$_['ms_order_column_order_id'] = 'Order #';

/***** INSERTED *****/$_['ms_account_order_column_order_id'] = 'Order #';
/***** INSERTED *****/$_['ms_account_order_payment_status'] = 'Payment status';



$_['ms_account_order_information'] = 'معلومات الطلب';

// Account - Dashboard
$_['ms_account_dashboard_heading'] = 'لوحة مراقبة البائع';
$_['ms_account_dashboard_breadcrumbs'] = 'لوحة مراقبة البائع';
$_['ms_account_sellersetting_breadcrumbs'] = 'إعدادات البائع';
$_['ms_account_dashboard_gross_sales'] = 'إجمالي المبيعات';
$_['ms_account_dashboard_total_current_balance'] = 'الرصيد الحالي';
$_['ms_account_dashboard_total_earnings'] = 'كافة الأرباح';
$_['ms_account_dashboard_total_orders'] = 'كافة الطلبات';
$_['ms_account_dashboard_total_views'] = 'كافة المشاهدات';
$_['ms_account_dashboard_sales_analytics'] = 'تحليل المبيعات';
$_['ms_account_dashboard_top_selling_products'] = 'المنتجات الأكثر مبيعاً';
$_['ms_account_dashboard_top_viewed_products'] = 'المنتجات الأكثر مشاهدةً';
$_['ms_account_dashboard_top_rated_products'] = 'المنتجات الأكثر تقييماً';
$_['ms_account_dashboard_last_messages'] = 'آخر الرسائل';
$_['ms_account_dashboard_last_reviews'] = 'آخر المراجعات';
$_['ms_account_dashboard_last_orders'] = 'آخر الطلبات';
$_['ms_account_dashboard_last_invoices'] = 'آخر الفواتير';
$_['ms_account_dashboard_msg_from_admin'] = 'الإدارة';
$_['ms_account_dashboard_no_results_not_enough_data'] = 'لا تتوفر بيانات كافية.';
$_['ms_account_dashboard_no_results_no_product_sales'] = "لم تقم ببيع أي منتج حتى الآن.";
$_['ms_account_dashboard_no_results_no_product_views'] = "ليس لديك أي مراجعات حول المنتج حتى الآن.";
$_['ms_account_dashboard_no_results_no_data'] = 'لا توجد بيانات.';
$_['ms_account_dashboard_no_results_orders'] = 'لا توجد لديك طلبات حالياً.';
$_['ms_account_dashboard_no_results_reviews'] = 'لا توجد لديك مراجعات حالياً.';
$_['ms_account_dashboard_no_results_messages'] = 'لا توجد لديك رسائل حالياً.';
$_['ms_account_dashboard_no_results_invoices'] = 'لا توجد لديك فواتير حالياً.';

$_['ms_account_dashboard_column_product'] = 'المنتج';
$_['ms_account_dashboard_column_period'] = 'الفترة';
$_['ms_account_dashboard_column_total_views'] = 'إجمالي المشاهدات';
$_['ms_account_dashboard_column_total_sales'] = 'إجمالي المبيعات';
$_['ms_account_dashboard_column_gross'] = 'الأرباح';
$_['ms_account_dashboard_column_rating'] = 'التقييم';
$_['ms_account_dashboard_column_from'] = 'من';
$_['ms_account_dashboard_column_message'] = 'الرسالة';
$_['ms_account_dashboard_column_date'] = 'التاريخ';
$_['ms_account_dashboard_column_comment'] = 'التعليق';
$_['ms_account_dashboard_column_customer'] = 'العميل';
$_['ms_account_dashboard_column_status'] = 'الحالة';
$_['ms_account_dashboard_column_total'] = 'الإجمالي';
$_['ms_account_dashboard_column_order'] = 'الطلب';
$_['ms_account_dashboard_column_description'] = 'الوصف';
$_['ms_account_dashboard_column_type'] = 'النوع';
$_['ms_account_dashboard_column_conversation'] = 'المحادثات';

// Account - Seller return
$_['ms_account_returns_heading'] = 'مرتجعات المنتج';
$_['ms_account_returns_breadcrumbs'] = 'لوحة مراقبة البائع';
$_['ms_account_return_id'] = 'رقم المرتجع';
$_['ms_account_return_order_id'] = 'رقم الطلب';
$_['ms_account_return_customer'] = 'العميل';
$_['ms_account_return_status'] = 'الحالة';
$_['ms_account_return_date_added'] = 'تاريخ الإنشاء';
$_['ms_account_return_customer_info'] = 'معلومات الطلب';


// Account - Settings
$_['ms_seller_information'] = "المعلومات";
$_['ms_seller_first_name'] = "الاسم الثلاثي";
$_['ms_seller_last_name'] = "اللقب";
$_['ms_seller_email'] = "البريد الإلكتروني";
$_['ms_seller_account'] = "الحساب";
$_['ms_seller_address'] = "العنوان";
$_['ms_seller_full_name'] = "الإسم كاملاً";
$_['ms_seller_address1'] = "العنوان 1";
$_['ms_seller_address1_placeholder'] = 'الشارع, صندوق البريد, اسم الشركة... الخ';
$_['ms_seller_address2'] = "العنوان 2";
$_['ms_seller_address2_placeholder'] = 'الشقة, الجناح, الوحدة, البناية, الطابق...الخ';
$_['ms_seller_city'] = "المدينة";
$_['ms_seller_state'] = "الولاية/المحافظة/المنطقة";
$_['ms_seller_zip'] = "الرمز البريدي";
$_['ms_seller_country'] = "الدولة";
$_['ms_seller_country_select'] = "-- الرجاء قم باختيار دولتك من القائمة --";
$_['ms_success_settings_saved'] = "تم حفظ الإعدادات بنجاح!";

// @todo 9.0: check usage and remove this
$_['ms_seller_company'] = 'الشركة';
$_['ms_seller_website'] = 'الموقع الإلكتروني';
$_['ms_seller_phone'] = 'رقم الهاتف';

// Account - Request withdrawal
$_['ms_account_withdraw_balance'] = 'رصيدك الحالي:';
$_['ms_account_balance_reserved_formatted'] = '-%s قيد السحب';
$_['ms_account_balance_waiting_formatted'] = '-%s قيد الانتظار';

// Account - Stats
$_['ms_account_stats_heading'] = 'الإحصائيات';
$_['ms_account_stats_breadcrumbs'] = 'الإحصائيات';
$_['ms_account_stats_tab_summary'] = 'ملخص';
$_['ms_account_stats_tab_by_product'] = 'عبر المنتج';
$_['ms_account_stats_tab_by_year'] = 'عبر السنة';
$_['ms_account_stats_summary_comment'] = 'يوجد في الأسفل ملخص مبيعاتك';
$_['ms_account_stats_sales_data'] = 'بيانات المبيعات';
$_['ms_account_stats_number_of_orders'] = 'عدد الطلبات';
$_['ms_account_stats_total_revenue'] = 'إجمالي الأرباح';
$_['ms_account_stats_average_order'] = 'متوسط الطلب';
$_['ms_account_stats_statistics'] = 'الإحصائيات';
$_['ms_account_stats_grand_total'] = 'إجمالي المبيعات الكلية';
$_['ms_account_stats_product'] = 'المنتج';
$_['ms_account_stats_sold'] = 'تم بيع';
$_['ms_account_stats_total'] = 'الإجمالي';
$_['ms_account_stats_this_year'] = 'هذه السنة';
$_['ms_account_stats_year_comment'] = '<span id="sales_num">%s</span> مباع في الفترة المحددة';
$_['ms_account_stats_show_orders'] = 'عرض الطلبات من: ';
$_['ms_account_stats_month'] = 'الشهر';
$_['ms_account_stats_num_of_orders'] = 'عدد الطلبات';
$_['ms_account_stats_total_r'] = 'إجمالي الأرباح';
$_['ms_account_stats_average_order'] = 'متوسط الطلب';
$_['ms_account_stats_today'] = 'اليوم, ';
$_['ms_account_stats_yesterday'] = 'أمس, ';
$_['ms_account_stats_daily_average'] = 'المتوسط اليومي ل';
$_['ms_account_stats_date_month_format'] = 'm/Y';
$_['ms_account_stats_projected_totals'] = 'الإجماليات المتوقعة ل';
$_['ms_account_stats_grand_total_sales'] = 'إجمالي المبيعات الكلية';

// Product page - Seller information
$_['ms_catalog_product_seller_information'] = 'معلومات البائع';
$_['ms_catalog_product_contact'] = 'التواصل مع البائع';

$_['ms_footer'] = '<br>MultiMerch Marketplace by <a href="http://multimerch.com/">multimerch.com</a>';

// Seller modules
$_['ms_newsellers_sellers'] = 'البائعون الجدد';

$_['ms_topsellers_sellers'] = 'أفضل البائعين';

$_['ms_listsellers_sellers'] = 'قائمة البائعين';

// Catalog - Sellers list
$_['ms_catalog_sellers_heading'] = 'البائعون';
$_['ms_catalog_sellers_description'] = 'قائمة البائعين';
$_['ms_sort_nickname_desc'] = 'الإسم (تصاعدي)';
$_['ms_sort_nickname_asc'] = 'الإسم (تنازلي)';
$_['ms_catalog_sellers_map_view'] = 'الخريطة';

// Catalog - Seller profile page
$_['ms_catalog_sellers'] = 'البائعون';
$_['ms_catalog_sellers_empty'] = 'لا يوجد بائعون حالياً.';
$_['ms_catalog_seller_profile'] = 'عرض الصفحة الشخصية';
$_['ms_catalog_seller_profile_heading'] = 'صفحة %s الشخصية';
$_['ms_catalog_seller_profile_breadcrumbs'] = 'صفحة %s الشخصية';

$_['ms_catalog_seller_profile_total_sales'] = 'المبيعات';
$_['ms_catalog_seller_profile_total_products'] = 'المنتجات';
$_['ms_catalog_seller_profile_view_products'] = 'View store';
$_['ms_catalog_seller_profile_featured_products'] = 'المنتجات الموضوعة حديثاً';
$_['ms_catalog_seller_profile_search'] = 'بحث جميع المنتجات';
$_['ms_catalog_seller_profile_rating'] = 'التقييم';
$_['ms_catalog_seller_profile_total_reviews'] = '(%s %s)';


// Catalog - Seller's products list
$_['ms_catalog_seller_products_heading'] = "%s منتجات";
$_['ms_catalog_seller_products_sales'] = "%s مبيعات";
$_['ms_catalog_seller_products_contact'] = "ارسل رسالة";
$_['ms_catalog_seller_products_profile'] = "الصفحة الشخصية";
$_['ms_catalog_seller_products_breadcrumbs'] = "منتجات %s";
$_['ms_catalog_seller_products_empty'] = "ليس لدى هذا البائع أي منتجات حالياً!";

// Catalog - Seller contact dialog
$_['ms_sellercontact_signin'] = 'يرجى <a href="%s">تسجيل الدخول</a> للتواصل مع %s';
$_['ms_sellercontact_sendto'] = 'ارسل رسالة إلى %s';
$_['ms_sellercontact_text'] = 'الرسالة: ';
$_['ms_sellercontact_close'] = 'إغلاق';
$_['ms_sellercontact_send'] = 'إرسال';
$_['ms_sellercontact_success'] = 'تم إرسال رسالتك بنجاح';

// Product filters
$_['ms_entry_filter'] = 'الفلاتر';
$_['ms_autocomplete'] = '(تكملة تلقائية)';

// Related products
$_['ms_catalog_products_related_products']  = 'المنتجات ذات الصلة';

//Stores
$_['ms_catalog_products_stores']    = 'المتاجر';

// Dimensions
$_['ms_catalog_products_measurements']    = 'القياسات';
$_['ms_catalog_products_size']          = 'الحجم (الطول×العرض×السُمك)';
$_['ms_catalog_products_size_length']   = 'الطول';
$_['ms_catalog_products_size_width']    = 'العرض';
$_['ms_catalog_products_size_height']   = 'السُمك';
$_['ms_catalog_products_weight']        = 'الوزن';

// Invoices
$_['heading_invoice_title']         = 'فاتورة الطلب';
$_['column_total_shipping'] = 'الشحن';

// Validation
$_['ms_validate_default'] = 'خانة \'%s\' غير صالحة';
$_['ms_validate_required'] = 'خانة \'%s\' مطلوبة';
$_['ms_validate_alpha_numeric'] = 'خانة \'%s\' يجب أن تحوي حروف لاتينية وأرقام فقط';
$_['ms_validate_latin'] = 'خانة \'%s\' يجب أن تحتوي على أحرف لاتينية فقط';
$_['ms_validate_utf8'] = 'خانة \'%s\' يجب أن تحتوي على أحرف بترميز UTF-8 فقط';
$_['ms_validate_max_len'] = 'خانة \'%s\' %s يجب أن تكون \'%s\' أو أصغر في الطول';
$_['ms_validate_min_len'] = 'خانة \'%s\' %s يجب أن تكون \'%s\' أو أكبر في الطول';
$_['ms_validate_phone_number'] = 'خانة \'%s\' ليست رقم هاتف';
$_['ms_validate_valid_url'] = 'خانة \'%s\' يجب أن تحوي رابطاً صالحاً';
$_['ms_validate_numeric'] = 'خانة \'%s\' يجب أن تحوي على أرقام فقط';
$_['ms_validate_email'] = 'خانة \'%s\' يجب أن تكون عنوان بريد الالكتروني صالح';

$_['ms_validate_email_exists'] = 'البريد الالكتروني الذي أدخلته مسجل من قبل';
$_['ms_validate_password_confirm'] = 'كلمة السر لا تتطابق مع التأكيد';


//Order history

$_['ms_order_placed'] = 'تاريخ الطلب:';
$_['ms_order_total'] = 'الإجمالي:';
$_['ms_order_dispatch'] = 'إرسال إلى:';
$_['ms_order_details'] = 'تفاصيل الطلب';
$_['ms_order_details_by_seller'] = 'تفاصيل الطلب من قبل البائع';
$_['ms_order_products_by'] = 'البائع:';
$_['ms_order_id'] = "رقم الطلب المميز للبائع:";
$_['ms_order_current_status'] = "حالة الطلب الحالية للبائع:";
$_['ms_order_status'] = "Seller's order status:";
$_['ms_order_status_initial'] = 'تم إنشاء الطلب';
$_['ms_marketplace_order_status'] = 'حالة طلب المتجر';
$_['ms_order_status_history'] = "سجل حالات طلب البائع";
$_['ms_order_sold_by'] = 'بيع بواسطة:';
$_['ms_order_buy_again'] = 'الشراء مجدداً';
$_['ms_order_feedback'] = 'وضع تقييم';
$_['ms_order_return'] = 'إرجاع منتجات';

$_['ms_order_success_suborders_modified'] = 'تم بنجاح تعديل الطلب #%s والطلبات الفرعية المتعلقة بـ%s!';
$_['ms_order_success_transactions_created'] = 'تم إنشاء المعاملة #%s.';

/***** INSERTED *****/$_['ms_payment_status'] = 'Payment status';
/***** INSERTED *****/$_['ms_payment_history'] = 'Payment history';
/***** INSERTED *****/$_['ms_payment_method'] = 'Payment method';
/***** INSERTED *****/$_['ms_order_status'] = 'Order status';

// Questions
$_['mm_questions_tab'] = 'الأسئلة (%s)';
$_['mm_question_title'] = 'الأسئلة';
$_['mm_question'] = 'السؤال';
$_['mm_question_posted_by'] = 'سؤال من:';
$_['mm_question_answers'] = 'الإجابات';
$_['mm_question_answer_by'] = 'الرد من:';
$_['mm_question_no_answers'] = 'ليس هناك إجابة لهذا السؤال بعد';
$_['mm_question_no_questions'] = 'لا يوجد أي أسئلة حول هذا المنتج حتى الآن';
$_['mm_question_write_answer'] = 'إجابة هذا السؤال';
$_['mm_question_submit'] = 'تقديم';
$_['mm_question_ask'] = 'ضع سؤالاً حول هذا المنتج';
$_['mm_question_signin'] = 'يرجى تسجيل الدخول لوضع سؤالك.';
$_['mm_question_answers_textarea_placeholder'] = 'إجابة السؤال';
$_['mm_question_seller_answer'] = "إجابة البائع";

$_['question_title'] = 'الأسئلة';
$_['posted_by'] = 'سؤال من:';
$_['answer_by'] = 'الرد من:';
$_['no_answers'] = 'لا توجد إجابات';
$_['write_answer'] = 'اكتب إجابتك';

// Customer feedback
$_['ms_customer_product_rate_heading'] = 'قَيّم تجربتك';
$_['ms_customer_product_rate_stars_label'] = 'يرجى تقييم هذه الصفقة';
$_['ms_customer_product_rate_comments'] = 'وضع تعليق';
$_['ms_customer_product_rate_comments_placeholder'] = 'يرجى ترك تعليقات حول تجربة التسوق مع هذا البائع';
$_['ms_customer_product_rate_btn_submit'] = 'إرسال التقييم';
$_['ms_customer_product_rate_drag_drop_here'] = 'اسحب الصور هنا لتحميل';
$_['ms_customer_product_rate_characters_left'] = 'حرف متبقي';
$_['ms_customer_product_rate_drag_drop_allowed'] = 'الإمتدادات المسموح بها: %s';
$_['ms_customer_product_rate_form_error'] = 'يرجى التأكد من تعبئة كافة الخانات!';

// Reviews
$_['mm_review_comments_title'] = 'تقييم العميل';
$_['mm_review_rating_summary'] = '%s من 5 (%s %s)';
$_['mm_review_rating_review'] = 'التقييم';
$_['mm_review_rating_reviews'] = 'التقييمات';
$_['mm_review_stats_stars'] = '%s نجوم';
$_['mm_review_no_reviews'] = 'لا توجد تقييمات حالياً!';
$_['mm_review_seller_profile_history'] = 'سجل التقييمات الحديثة';
$_['mm_review_seller_profile_history_positive'] = 'إيجابي (4-5 نجوم)';
$_['mm_review_seller_profile_history_neutral'] = 'متوسط (3 نجوم)';
$_['mm_review_seller_profile_history_negative'] = 'سلبي (1-2 نجوم)';
$_['mm_review_one_month'] = 'شهر واحد';
$_['mm_review_three_months'] = '3 أشهر';
$_['mm_review_six_months'] = '6 أشهر';
$_['mm_review_twelve_months'] = '12 شهر';
$_['mm_review_submit_success'] = 'شكراً لك على تقديم تقييمك!';
$_['mm_review_seller_response'] = "رد البائع";
$_['mm_review_comments'] = 'التعليق';
$_['mm_review_comments_placeholder'] = 'الرد على هذا التقييم';
$_['mm_review_no_comments'] = 'لم تقم بالرد على هذا التقييم حتى الآن.';
$_['mm_review_comments_success_added'] = 'تم إرسال تعليقك بنجاح!';
$_['mm_review_comments_error_signin'] = 'الرجاء تسجيل الدخول لنشر التعليق!';
$_['mm_review_comments_error_review_id'] = 'خطأ: لم يتم تحديد معرف التقييم!';
$_['mm_review_comments_error_notext'] = 'خطأ: يجب أن تكتب رسالة!';
$_['mm_review_comments_textarea_placeholder'] = "ضع ردك لتقييم العميل هنا. سيتم عرض الرد كذلك في المتجر.";
$_['mm_review_comments_post_message'] = 'تأكيد';
$_['mm_review_total_reviews_empty'] = 'No reviews yet';
$_['mm_review_total_reviews_one'] = '%s review';
$_['mm_review_total_reviews_multiple'] = '%s reviews';

// Seller > Account-Product > Shipping
$_['ms_account_product_tab_shipping'] = 'إعدادات الشحن';
$_['ms_account_product_shipping_from'] = 'الشحن من';
$_['ms_account_product_shipping_free'] = 'شحن مجاني';
$_['ms_account_product_shipping_free_note'] = 'لن يتم النظر في أسعار الشحن';
$_['ms_account_product_shipping_processing_time'] = 'زمن المعالجة';
$_['ms_account_product_shipping_locations_to'] = 'الشحن إلى';
$_['ms_account_product_shipping_locations_destination'] = 'الوجهة';
$_['ms_account_product_shipping_locations_company'] = 'شركة الشحن';
$_['ms_account_product_shipping_locations_delivery_time'] = 'زمن التوصيل';
$_['ms_account_product_shipping_locations_cost'] = 'التكلفة';
$_['ms_account_product_shipping_locations_cost_fixed_pwu'] = 'التكلفة (ثابتة + لكل وحدة وزنية)';
$_['ms_account_product_shipping_locations_additional_cost'] = 'منتج إضافي';
$_['ms_account_product_shipping_locations_add_btn'] = '+ إضافة موقع';
$_['ms_account_product_shipping_elsewhere'] = 'كافة أنحاء العالم';

$_['ms_account_product_shipping_combined_enabled'] = 'الشحن التجميعي مفعل, تكلفة شحن هذا المنتج سيتم حسابها تلقائياً.';
$_['ms_account_product_shipping_combined_override'] = 'تجاوز قواعد الشحن التجميعي';

// Seller > Account-Settings > Shipping
$_['ms_account_settings_shipping_settings'] = 'إعدادات الشحن';
$_['ms_account_settings_shipping_weight'] = 'الوزن (%s)';
$_['ms_account_settings_shipping_comment'] = 'التعليق';
$_['ms_account_settings_shipping_add'] = '+ إضافة وسيلة';

// Seller > Account-Settings > Payments
$_['ms_account_setting_payments_tab'] = 'إعدادات الدفع';

// Product page > Shipping
$_['mm_product_shipping_title'] = 'معلومات الشحن';
$_['mm_product_shipping_free'] = 'شحن مجاني';
$_['mm_product_shipping_from_country'] = 'الشحن من';
$_['mm_product_shipping_processing_time'] = 'زمن المعالجة';
$_['mm_product_shipping_processing_days'] = '%s %s';
$_['mm_product_shipping_locations_note'] = 'قد تختلف أوقات التوصيل المقدرة خصوصاً خلال فترات الذروة.';
$_['mm_product_shipping_not_specified'] = 'لم يحدد البائع معلومات التوصيل لهذا المنتج!';
$_['mm_product_shipping_digital_product'] = 'هذا المنتج رقمي لا يتطلب شحن!';

// Checkout > Payment method
/***** INSERTED *****/$_['mm_checkout_payment_method_add_comments'] = "Add comments for sellers";
/***** INSERTED *****/$_['mm_checkout_payment_method_suborder_comment_title'] = "Comments about order by %s";

// Checkout > Shipping
$_['mm_not_specified'] = 'غير محدد';
$_['mm_checkout_shipping_ew_location_delivery_time_name'] = 'وقت التوصيل يعتمد';
$_['mm_checkout_shipping_delivery_details_title'] = 'عنوان التوصيل';
$_['mm_checkout_shipping_delivery_details_change'] = 'تغيير';
$_['mm_checkout_shipping_products_title'] = 'إختر <b>طريقة التوصيل المفضلة لك</b> لكل منتج';
$_['mm_checkout_shipping_products_price'] = 'السعر: ';
$_['mm_checkout_shipping_products_quantity'] = 'الكمية: ';
$_['mm_checkout_shipping_products_seller'] = 'مباع بواسطة: ';
$_['mm_checkout_shipping_method_title'] = 'يرجى اختيار طريقة التوصيل';
$_['mm_checkout_shipping_method_title_shot'] = 'طريقة التوصيل';
$_['mm_checkout_shipping_method_free'] = 'توصيل مجاني';
$_['mm_checkout_shipping_not_required'] = 'لا يتطلب النقل.';
$_['mm_checkout_shipping_digital_products'] = 'المنتجات الرقمية لا تحتاج إلى شحن.';
$_['mm_checkout_shipping_not_available'] = 'لا يمكننا الشحن إلى عنوانك. سيتم حذف المنتجات من السلة.';
$_['mm_checkout_shipping_total'] = 'إجمالي الشحن: ';
$_['mm_checkout_shipping_product_delete_warning'] = 'المنتجات التي لا يمكن شحنها إلى عنوانك سيتم حذفها من سلة مشترياتك.';
$_['mm_checkout_shipping_no_selected_methods'] = 'خطأ: يجب أن تختار وسائل الشحن!';

$_['mm_checkout_shipping_error_maxweight_exceeded'] = 'الوزن الإجمالي للمنتجات التي تم شراءها من البائع %s هو (%s) وقد تجاوز الوزن الذي يسمح به هذا البائع وهو (%s). يرجى حذف بعض من منتجات %s <a href="%s">من سلتك</a> أو سيتم حذف المنتجات آلياً إذا تابعت.';
$_['mm_checkout_shipping_error_minweight_not_exceeded'] = 'الوزن الإجمالي للمنتجات التي تم شراءها من البائع %s هو (%s) لم يتجاوز الحد الأدنى للوزن الذي يسمح به هذا البائع وهو (%s). يرجى إضافة المزيد من منتجات %s <a href="%s">من معرضه</a> إلى سلتك أو سيتم حذف المنتجات آلياً إذا تابعت.';
$_['mm_checkout_shipping_error_no_available_methods'] = 'خطأ: لا توجد وسائل شحن متاحة لبعض المنتجات في سلة مشترياتك!';
$_['mm_checkout_shipping_error_shipping_address'] = 'خطأ: لم يتم تحديد عنوان الشحن!';

// Account > Order history
$_['mm_account_order_shipping_cost'] = 'تكلفة الشحن';
$_['mm_account_order_shipping_total'] = 'إجمالي الشحن';
$_['mm_account_order_shipping_via'] = 'Shipped via %s';

// Payments
$_['ms_pg_new_payment'] = 'دفع جديد';
$_['ms_pg_payment_method'] = 'طريقة الدفع';
$_['ms_pg_payment_requests'] = 'الفواتير';
$_['ms_pg_payment_form_select_method'] = 'اختر طريقة الدفع';

$_['ms_pg_payment_type_' . MsPgPayment::TYPE_PAID_REQUESTS] = 'الفواتير المدفوعة';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_SALE] = 'المبيعات';

$_['ms_pg_payment_status_' . MsPgPayment::STATUS_INCOMPLETE] = '<p style="color: red">غير مكتمل</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_INCOMPLETE] = 'Incomplete';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_COMPLETE] = '<p style="color: green">مكتمل</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_COMPLETE] = 'Complete';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = '<p style="color: blue">بإنتظار التأكيد</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = 'Waiting for confirmation';

$_['ms_pg_payment_error_not_available'] = 'للأسف! لا توجد وسيلة دفع متاحة في الوقت الحالي!';
$_['ms_pg_payment_error_no_method'] = 'خطأ: يجب أن تختار وسيلة دفع!';
$_['ms_pg_payment_error_receiver_data'] = 'خطأ: بيانات المستلم غير موجودة!';
$_['ms_pg_payment_error_sender_data'] = 'خطأ: بيانات المرسل غير موجودة!';

// Combined shipping
$_['ms_account_setting_ssm_title'] = 'إعدادات الشحن التجميعي';
$_['ms_account_setting_ssm_success'] = 'لقد قمت بتعديل إعدادات الشحن التجميعي بنجاح!';
$_['ms_account_setting_ssm_error_data'] = 'خطأ: لا توجد بيانات لمعالجتها!';
$_['ms_account_setting_ssm_error_methods'] = 'خطأ: لم يتم تحديد الوسائل!';
$_['ms_account_setting_ssm_error_location'] = 'يجب عليك تحديد الموقع!';
$_['ms_account_setting_ssm_error_method'] = 'يجب عليك تحديد وسيلة الشحن!';
$_['ms_account_setting_ssm_error_delivery_time'] = 'يجب عليك تحديد وقت التسليم!';
$_['ms_account_setting_ssm_error_weight'] = 'يجب عليك تحديد نطاق الوزن!';
$_['ms_account_setting_ssm_error_cost'] = 'يجب عليك تحديد التكلفة!';
$_['ms_account_setting_ssm_error_no_sm'] = 'لا توجد وسائل شحن متاحة في الوقت الحالي!';
$_['ms_account_setting_ssm_error_no_dt'] = 'لا توجد أوقات تسليم متاحة في الوقت الحالي!';
$_['ms_account_setting_ssm_error_no_gz'] = 'لا توجد مناطق جغرافية متاحة في الوقت الحالي!';

$_['ms_product_ssm_weight_range_template'] = '%s - %s %s';

// Seller attibutes
$_['ms_account_attribute_heading'] = 'المواصفات الخاصة بك';
$_['ms_account_attribute_breadcrumbs'] = 'مواصفاتك';
$_['ms_account_attribute'] = 'المواصفة';
$_['ms_account_attributes'] = 'المواصفات';
$_['ms_account_attribute_manage'] = 'إدارة المواصفات';
$_['ms_account_attribute_new'] = 'مواصفة جديدة';
$_['ms_account_attribute_name'] = 'الإسم';
$_['ms_account_newattribute_heading'] = 'مواصفة جديدة';
$_['ms_account_editattribute_heading'] = 'تعديل المواصفة';
$_['ms_account_attributes_empty'] = "لم تقم بإنشاء مواصفات حتى الآن.";

$_['ms_account_attribute_name_note'] = 'حدد إسماً لمواصفتك';
$_['ms_account_attribute_attr_group_note'] = 'قم بإرفاق المواصفة الخاصة بك إلى مجموعة مواصفات موجودة أو <a href="%s">قم بإنشاء مجموعتك الخاصة</a>';
$_['ms_account_attribute_sort_order_note'] = 'حدد ترتيب الفرز للمواصفة الخاصة بك';

$_['ms_success_attribute_created'] = 'تم إنشاء المواصفة بنجاح! ستكون قادراً على استخدامها بمجرد موافقة الإدارة عليها.';
$_['ms_success_attribute_updated'] = 'تم تحديث المواصفة!';
$_['ms_success_attribute_deleted'] = 'تم حذف المواصفة!';
$_['ms_success_attribute_activated'] = 'تم تنشيط المواصفة!';
$_['ms_success_attribute_deactivated'] = 'تم تعطيل المواصفة!';

$_['ms_error_attribute_assigned_to_products'] = 'تحذير: لا يمكن حذف هذه المواصفة كونها مسجلة في %s منتجات!';
$_['ms_error_attribute_id'] = 'لم يتم تعيين معرف المواصفة!';

// Seller attibute groups
$_['ms_account_attribute_group_heading'] = 'مجموعة المواصفات الخاصة بك';
$_['ms_account_attribute_group_breadcrumbs'] = 'مجموعة مواصفاتك';
$_['ms_account_attribute_group'] = 'مجموعة المواصفات';
$_['ms_account_attribute_groups'] = 'مجموعات المواصفات';
$_['ms_account_attribute_group_new'] = 'مجموعة مواصفات جديدة';
$_['ms_account_attribute_group_name'] = 'الإسم';
$_['ms_account_newattributegroup_heading'] = 'مجموعة مواصفات جديدة';
$_['ms_account_editattributegroup_heading'] = 'تعديل مجموعة المواصفات';
$_['ms_account_attribute_groups_empty'] = "لم تقم بإنشاء مجموعة مواصفات حتى الآن.";

$_['ms_account_attribute_group_name_note'] = 'ضع إسماً لمجموعة المواصفات';
$_['ms_account_attribute_group_sort_order_note'] = 'حدد ترتيب الفرز لمجموعة المواصفات الخاصة بك';

$_['ms_success_attribute_group_created'] = 'تم إنشاء مجموعة المواصفات بنجاح! ستكون قادراً على استخدامها بمجرد موافقة الإدارة عليها.';
$_['ms_success_attribute_group_updated'] = 'تم تحديث مجموعة المواصفات!';
$_['ms_success_attribute_group_deleted'] = 'تم حذف مجموعة المواصفات!';
$_['ms_success_attribute_group_activated'] = 'تم تنشيط مجموعة المواصفات!';
$_['ms_success_attribute_group_deactivated'] = 'تم تعطيل مجموعة المواصفات!';

$_['ms_error_attribute_group_assigned_to_attributes'] = 'تحذير: لا يمكن حذف مجموعة المواصفات هذه كونها تحوي على %s مواصفات!';
$_['ms_error_attribute_group_id'] = 'لم يتم تعيين معرف مجموعة المواصفات!';

$_['ms_seller_attribute_status_' . MsAttribute::STATUS_DISABLED] = 'معطل';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_APPROVED] = 'موافق عليه';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_INACTIVE] = 'غير نشط';

// Seller options
$_['ms_account_option_heading'] = 'الخيارات الخاصة بك';
$_['ms_account_option_breadcrumbs'] = 'خياراتك';
$_['ms_account_option'] = 'الخيار';
$_['ms_account_options'] = 'الخيارات';
$_['ms_account_option_value'] = 'قيمة الخيار';
$_['ms_account_option_values'] = 'قيم الخيار';
$_['ms_account_option_new'] = 'خيار جديد';
$_['ms_account_option_name'] = 'الإسم';
$_['ms_account_option_manage'] = 'إدارة الخيارات';
$_['ms_account_newoption_heading'] = 'خيار جديد';
$_['ms_account_editoption_heading'] = 'تعديل الخيار';
$_['ms_account_options_empty'] = "لم تقم بإنشاء خيارات حتى الآن.";

$_['ms_account_option_name_note'] = 'ضع إسماً للخيار';
$_['ms_account_option_sort_order_note'] = 'حدد ترتيب فرز الخيار';
$_['ms_account_option_type_note'] = 'حدد نوع الخيار';

$_['ms_success_option_created'] = 'تم إنشاء الخيار بنجاح! ستكون قادراً على استخدامه بمجرد موافقة الإدارة عليه.';
$_['ms_success_option_updated'] = 'تم تحديث الخيار!';
$_['ms_success_option_deleted'] = 'تم حذف الخيار!';
$_['ms_success_option_activated'] = 'تم تنشيط الخيار!';
$_['ms_success_option_deactivated'] = 'تم تعطيل الخيار!';

$_['ms_error_option_assigned_to_products'] = 'تحذير: لا يمكن حذف هذا الخيار كونه مسجل في %s منتجات!';
$_['ms_error_option_id'] = 'لم يتم تحديد معرف الخيار!';
$_['ms_error_option_values'] = 'قيم الخيار مطلوبة!';

$_['ms_seller_option_status_' . MsOption::STATUS_DISABLED] = 'معطل';
$_['ms_seller_option_status_' . MsOption::STATUS_APPROVED] = 'موافق عليه';
$_['ms_seller_option_status_' . MsOption::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_option_status_' . MsOption::STATUS_INACTIVE] = 'غير نشط';

$_['ms_account_option_type_choose'] = 'اختيار';
$_['ms_account_option_type_input'] = 'ادخال';
$_['ms_account_option_type_file'] = 'ملف';
$_['ms_account_option_type_date'] = 'تاريخ';
$_['ms_account_option_type_select'] = 'قائمة منسدلة';
$_['ms_account_option_type_radio'] = 'زر الاختيار (مفرد)';
$_['ms_account_option_type_checkbox'] = 'صندوق الاختيار (متعدد)';
$_['ms_account_option_type_text'] = 'نص (سطر واحد)';
$_['ms_account_option_type_textarea'] = 'حق نصي (متعدد الأسطر)';
$_['ms_account_option_type_file'] = 'ملف';
$_['ms_account_option_type_time'] = 'وقت';
$_['ms_account_option_type_datetime'] = 'تاريخ ووقت';

// Seller categories
$_['ms_account_category_heading'] = 'الأقسام الخاصة بك';
$_['ms_account_category_breadcrumbs'] = 'أقسامك';
$_['ms_account_category'] = 'القسم';
$_['ms_account_category_primary'] = 'Primary category';
$_['ms_account_categories'] = 'الأقسام';
$_['ms_account_category_manage'] = 'إدارة الأقسام';
$_['ms_account_category_new'] = 'قسم جديد';
$_['ms_account_newcategory_heading'] = 'قسم جديد';
$_['ms_account_editcategory_heading'] = 'تعديل القسم';
$_['ms_account_categories_empty'] = "لم تقم بإنشاء أي أقسام حتى الآن.";

$_['ms_account_category_name'] = 'الإسم';
$_['ms_account_category_name_note'] = 'ضع إسماً للقسم';
$_['ms_account_category_description'] = 'الوصف';
$_['ms_account_category_description_note'] = 'قم بوصف قسمك';

$_['ms_account_category_additional_data'] = 'بيانات اضافية';
$_['ms_account_category_parent'] = 'المجموعة الرئيسية';
$_['ms_account_category_no_parent'] = '-- لا شيء --';
$_['ms_account_category_parent_note'] = 'حدد مجموعة رئيسية لبناء هيكل خاص بأقسامك';
$_['ms_account_category_filters'] = 'الفلترات';

$_['ms_account_category_search_optimization'] = 'تحسين البحث';
$_['ms_account_category_meta_title'] = 'عنوان الميتا';
$_['ms_account_category_meta_title_note'] = 'عنوان الميتا هو ما سيظهر كعنوان في صفحة الفئة';
$_['ms_account_category_meta_description'] = 'وصف الميتا';
$_['ms_account_category_meta_description_note'] = 'تستخدم محركات البحث وصف الميتا لوصف قسمك في نتائج البحث. بدون تنسيقات';
$_['ms_account_category_meta_keyword'] = 'كلمات الميتا';
$_['ms_account_category_meta_keyword_note'] = 'قد تستخدم كلمات الميتا من قبل محركات البحث لمعرفة ما الذي يحويه قسمك';
$_['ms_account_category_seo_keyword'] = 'رابط SEO';
$_['ms_account_category_seo_keyword_note'] = "سيظهر هذا في رابط صفحة قسمك. لا تستخدم مسافات أو أحرف خاصة";

$_['ms_account_category_image'] = 'صورة';
$_['ms_account_category_sort_order'] = 'ترتيب الفرز';
$_['ms_account_category_sort_order_note'] = 'حدد ترتيب الفرز الخاص بقسمك';

$_['ms_success_category_created'] = 'تم بنجاح إنشاء قسمك! ستكون قادراً على استخدامها بمجرد موافقة الإدارة عليه.';
$_['ms_success_category_updated'] = 'تم تحديث القسم!';
$_['ms_success_category_deleted'] = 'تم حذف القسم!';
$_['ms_success_category_activated'] = 'تم تنشيط القسم!';
$_['ms_success_category_deactivated'] = 'تم تعطيل القسم!';

$_['ms_seller_category_status_' . MsCategory::STATUS_DISABLED] = 'معطل';
$_['ms_seller_category_status_' . MsCategory::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_category_status_' . MsCategory::STATUS_INACTIVE] = 'غير نشط';

// Seller reviews management
$_['ms_account_reviews'] = 'التقييمات';
$_['ms_account_review_heading'] = 'التقييمات';
$_['ms_account_review_breadcrumbs'] = 'التقييمات';
$_['ms_account_review_manage'] = 'إدارة التقييمات';
$_['ms_account_review_column_product'] = 'المنتج';
$_['ms_account_review_column_rating'] = 'التقييم';
$_['ms_account_review_column_comment'] = 'التعليق';
$_['ms_account_review_column_date_added'] = 'التاريخ';
$_['ms_account_reviews_empty'] = "لا يوجد لديك أي مراجعات حالياً.";

$_['ms_account_editreview_heading'] = "ملاحظة العميل على منتجك";
$_['ms_account_editreview_product'] = 'المنتج';
$_['ms_account_editreview_order'] = 'رقم الطلب';
$_['ms_account_editreview_customer'] = 'العميل';
$_['ms_account_editreview_review'] = 'التعليق';
$_['ms_account_editreview_your_response'] = 'ردك';
$_['ms_account_editreview_response'] = 'الرد';
$_['ms_account_editreview_rating'] = 'التقييم';
$_['ms_account_editreview_customer_images'] = "صور العميل";
$_['ms_account_editreview_images'] = 'الصور';

$_['ms_success_review_updated'] = 'تم تحديث التقييم!';
$_['ms_success_review_deleted'] = 'تم حذف التقييم!';
$_['ms_error_review_id'] = 'التقييم غير موجود!';

$_['ms_seller_review_status_' . MsCategory::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_review_status_' . MsCategory::STATUS_INACTIVE] = 'غير نشط';

// Seller questions management
$_['ms_account_questions'] = 'الأسئلة';
$_['ms_account_question_heading'] = 'الأسئلة';
$_['ms_account_question_breadcrumbs'] = 'الأسئلة';
$_['ms_account_question_manage'] = 'إدارة الأسئلة';
$_['ms_account_question_column_product'] = 'المنتج';
$_['ms_account_question_column_customer'] = 'العميل';
$_['ms_account_question_column_answer'] = 'الجواب';
$_['ms_account_question_column_date_added'] = 'التاريخ';
$_['ms_account_questions_empty'] = "ليس لديك أسئلة حالياً.";

$_['ms_account_editquestion_heading'] = 'سؤال عن منتجك';
$_['ms_account_editquestion_product'] = 'المنتج';
$_['ms_account_editquestion_customer'] = 'العميل';
$_['ms_account_editquestion_question'] = 'السؤال';
$_['ms_account_editquestion_your_answer'] = 'جوابك';
$_['ms_account_editquestion_answer'] = 'الجواب';
$_['ms_account_question_no_answers'] = 'لا يوجد جواب حتى الآن';
$_['ms_questions_customer_deleted'] = '*تم حذف العميل*';

$_['ms_success_question_submitted'] = 'تم وضع سؤالك بنجاح!';
$_['ms_success_question_updated'] = 'تم تحديث السؤال!';
$_['ms_success_question_deleted'] = 'تم حذف السؤال!';
$_['ms_success_question_answered'] = 'تمت إجابة السؤال!';
$_['ms_error_question_id'] = 'لم يتم العثور على السؤال!';
$_['ms_error_question_text'] = 'يجب أن تكتب سؤالك!';

// Reports
$_['ms_report_guest_checkout'] = 'ضيف';
$_['ms_report_report'] = 'التقارير';
$_['ms_report_sales'] = 'المبيعات';
$_['ms_report_sales_list'] = 'قائمة المبيعات';
$_['ms_report_sales_day'] = 'المبيعات اليومية';
$_['ms_report_sales_month'] = 'المبيعات الشهرية';
$_['ms_report_sales_product'] = 'المبيعات حسب المنتج';
$_['ms_report_finances'] = 'المالية';
$_['ms_report_finances_transaction'] = 'العملية';
$_['ms_report_finances_payment'] = 'المدفوع';
$_['ms_report_finances_payout'] = 'المستحق';
$_['ms_report_sales_list_empty'] = "ليس لديك مبيعات حالياً.";
$_['ms_report_finances_transaction_empty'] = "ليس لديك عمليات حالياً.";
$_['ms_report_finances_payment_empty'] = "ليس لديك مدفوعات حالياً.";
$_['ms_report_finances_payout_empty'] = "لم تقم بإستلام أي مدفوعات حتى الآن.";

$_['ms_report_column_date'] = 'التاريخ';
$_['ms_report_column_date_month'] = 'الشهر';
$_['ms_report_column_order'] = 'الطلب';
$_['ms_report_column_product'] = 'المنتج';
$_['ms_report_column_gross'] = 'الإجمالي';
$_['ms_report_column_net_marketplace'] = 'صافي المتجر';
$_['ms_report_column_net_seller'] = 'صافي البائع';
$_['ms_report_column_tax'] = 'الضريبة';
$_['ms_report_column_shipping'] = 'الشحن';
$_['ms_report_column_total'] = 'الصافي';
$_['ms_report_column_total_sales'] = 'إجمالي المبيعات';
$_['ms_report_column_product'] = 'المنتج';
$_['ms_report_column_transaction'] = 'العملية';
$_['ms_report_column_description'] = 'الوصف';
$_['ms_report_column_payment'] = 'المدفوع';
$_['ms_report_column_payout'] = 'المستحق';
$_['ms_report_column_method'] = 'الوسيلة';

$_['ms_report_date_range_today'] = 'اليوم';
$_['ms_report_date_range_yesterday'] = 'أمس';
$_['ms_report_date_range_last7days'] = 'آخر 7 أيام';
$_['ms_report_date_range_last30days'] = 'آخر 30 يوم';
$_['ms_report_date_range_thismonth'] = 'هذا الشهر';
$_['ms_report_date_range_lastmonth'] = 'الشهر الماضي';
$_['ms_report_date_range_custom'] = 'فترة مخصصة';
$_['ms_report_date_range_apply'] = 'تطبيق';
$_['ms_report_date_range_cancel'] = 'إلغاء';

$_['ms_report_date_range_day_mo'] = 'إث';
$_['ms_report_date_range_day_tu'] = 'ث';
$_['ms_report_date_range_day_we'] = 'أر';
$_['ms_report_date_range_day_th'] = 'خ';
$_['ms_report_date_range_day_fr'] = 'ج';
$_['ms_report_date_range_day_sa'] = 'س';
$_['ms_report_date_range_day_su'] = 'أح';

$_['ms_report_date_range_month_jan'] = 'يناير';
$_['ms_report_date_range_month_feb'] = 'فبراير';
$_['ms_report_date_range_month_mar'] = 'مارس';
$_['ms_report_date_range_month_apr'] = 'إبريل';
$_['ms_report_date_range_month_may'] = 'مايو';
$_['ms_report_date_range_month_jun'] = 'يونيو';
$_['ms_report_date_range_month_jul'] = 'يوليو';
$_['ms_report_date_range_month_aug'] = 'أغسطس';
$_['ms_report_date_range_month_sep'] = 'سبتمبر';
$_['ms_report_date_range_month_oct'] = 'أكتوبر';
$_['ms_report_date_range_month_nov'] = 'نوفمبر';
$_['ms_report_date_range_month_dec'] = 'ديسمبر';

// Product custom fields
$_['ms_account_product_tab_custom_fields'] = 'إضافي';
$_['ms_account_product_cf_file_allowed_ext'] = 'ارفع ملفاتك. الامتدادات المسموح بها: %s';
$_['ms_account_product_cf_file_uploaded'] = 'تم رفع الملف بنجاح!';
$_['ms_account_product_error_field_required'] = "خانة '%s' مطلوبة!";
$_['ms_account_product_error_field_validation'] = "لم يتم التحقق من خانة '%s'! النمط: %s";

$_['ms_account_product_text_placeholder'] = 'الخانة مطلوبة!';
$_['ms_account_product_textarea_placeholder'] = 'الخانة مطلوبة!';
$_['ms_account_product_date_placeholder'] = 'الخانة مطلوبة!';

// Discount coupons
$_['ms_seller_account_coupon'] = "الكوبونات";
$_['ms_seller_account_coupon_breadcrumbs'] = "الكوبونات";
$_['ms_seller_account_newcoupon_heading'] = "إضافة كوبون جديد";
$_['ms_seller_account_editcoupon_heading'] = "تعديل الكوبون";
$_['ms_seller_account_coupon_empty'] = "لم تقم بإنشاء أي كوبون حتى الآن.";
$_['ms_seller_account_coupon_success_deleted'] = "تم حذف الكوبون بنجاح!";
$_['ms_seller_account_coupon_error_id'] = "حدث خطأ أثناء حذف الكوبون: تم تمرير معرف خاطئ!";
$_['ms_seller_account_coupon_manage'] = "إدارة الكوبونات";
$_['ms_seller_account_coupon_create'] = "إنشاء كوبون";
$_['ms_seller_account_coupon_general'] = "خصائص الكوبون";
$_['ms_seller_account_coupon_name'] = "الإسم";
$_['ms_seller_account_coupon_name_note'] = "ضع إسماً لهذا الكوبون (كمرجع لك فقط. لا يظهر للعامة)";
$_['ms_seller_account_coupon_description'] = "الوصف";
$_['ms_seller_account_coupon_description_note'] = "صف هذا الكوبون";
$_['ms_seller_account_coupon_code'] = "الرمز";
$_['ms_seller_account_coupon_code_note'] = "ضع رمزاً لهذا الكوبون ليتم استخدامه من قبل العميل مثال 20PERCENT أو BLACKFRIDAY. الحد الأقصى 12 حرفاً";
$_['ms_seller_account_coupon_value'] = "قيمة الخصم";
$_['ms_seller_account_coupon_value_note'] = "ضع قيمة خصم لهذا الكوبون - يمكن أن تكون قيمة ثابتة أو نسبة من قيمة منتجاتك في سلة مشتريات العميل";
$_['ms_seller_account_coupon_uses'] = "مرات الإستخدام";
$_['ms_seller_account_coupon_max_uses'] = "حدود الإستخدام";
$_['ms_seller_account_coupon_max_uses_total'] = "الحد الأقصى للكوبون";
$_['ms_seller_account_coupon_max_uses_customer'] = "الحد الأقصى للعميل";
$_['ms_seller_account_coupon_max_uses_note'] = "قم بتحديد الحد الأقصى المسموح به لإستخدام هذا الكوبون (أجمالياً أو لكل عميل). ضعه فارغاً لإستخدام غير محدود";
$_['ms_seller_account_coupon_date_period'] = "نطاق التاريخ";
$_['ms_seller_account_coupon_date_period_note'] = "حدد تاريخ لبدء وانتهاء فترة كوبون الخصم. هذا سيجعل الكوبون متاحاً خلال الفترة المحددة فقط. ضعه فارغاً لعدم وضع نطاق زمني";
$_['ms_seller_account_coupon_date_placeholder'] = "اختر التاريخ";
$_['ms_seller_account_coupon_date_start'] = "تاريخ البدء";
$_['ms_seller_account_coupon_date_end'] = "تاريخ الإنتهاء";
$_['ms_seller_account_coupon_min_order_total'] = "أقل قيمة للمشتريات";
$_['ms_seller_account_coupon_min_order_total_note'] = "ضع حداً أدنى لإجمالي قيمة المشتريات في سلة العميل قبل أن يتمكن من استخدام الكوبون. ضعه فارغاً لعدم التحديد";
$_['ms_seller_account_coupon_login_required'] = "طلب التسجيل";
$_['ms_seller_account_coupon_login_required_note'] = "حدد ما إذا كنت تريد أن يكون العميل مسجلاً أو لا لإستخدام هذا الكوبون";
$_['ms_seller_account_coupon_status'] = "الحالة";
$_['ms_seller_account_coupon_status_note'] = "تفعيل أو تعطيل هذا الكوبون";
$_['ms_seller_account_coupon_type_' . MsCoupon::TYPE_DISCOUNT_PERCENT] = "نسبة مئوية";
$_['ms_seller_account_coupon_type_' . MsCoupon::TYPE_DISCOUNT_FIXED] = "قيمة محددة";
$_['ms_seller_account_coupon_status_' . MsCoupon::STATUS_ACTIVE] = "مفعل";
$_['ms_seller_account_coupon_status_' . MsCoupon::STATUS_DISABLED] = "معطل";
$_['ms_seller_account_coupon_restrictions'] = "قيود الكوبون";
$_['ms_seller_account_coupon_products'] = "المنتجات";
$_['ms_seller_account_coupon_products_placeholder'] = "يتم تضمين جميع المنتجات بشكل افتراضي";
$_['ms_seller_account_coupon_products_empty'] = "ليس لديك حالياً أي منتجات لتضمينها في هذا الكوبون.";
$_['ms_seller_account_coupon_products_note'] = "تطبيق هذا الكوبون على المنتجات المحددة فقط أو استبعاد منتجات معينة من الخصم";
$_['ms_seller_account_coupon_products_include'] = "تطبيق الكوبون على المنتجات التالية";
$_['ms_seller_account_coupon_products_exclude'] = "استبعاد المنتجات التالية من الكوبون";
$_['ms_seller_account_coupon_customers'] = "العملاء";
$_['ms_seller_account_coupon_customers_note'] = "حدد العملاء الذين تنطبق عليهم هذا الكوبون. اتركه فارغاً لتضمين كافة العملاء";
$_['ms_seller_account_coupon_categories'] = "الأقسام";
$_['ms_seller_account_coupon_categories_placeholder'] = "كل الأقسام مضمنة افتراضياً";
$_['ms_seller_account_coupon_categories_placeholder_products_specified'] = "تم تحديد تقييد المنتج. ترك هذه الخانة فارغةً يعني عدم تضمين كافة الأقسام";
$_['ms_seller_account_coupon_categories_empty'] = "ليس لديك حالياً أقسام خاصة بك لتضمينها في هذا الكوبون.";
$_['ms_seller_account_coupon_categories_note'] = "تطبيق هذا الكوبون على أقسام محددة فقط أو استبعاد أقسام محددة من الخصم";
$_['ms_seller_account_coupon_categories_include'] = "تطبيق الكوبون على الأقسام التالية";
$_['ms_seller_account_coupon_categories_exclude'] = "استبعاد الأقسام التالية من الكوبون";
$_['ms_seller_account_coupon_created'] = "لقد تم إنشاء الكوبون بنجاح!";
$_['ms_seller_account_coupon_updated'] = "لقد تم تعديل الكوبون بنجاح!";
$_['ms_seller_account_coupon_code_error_exists'] = "خطأ: رمز الكوبون هذا قد تم استخدامه!";

$_['ms_total_coupon_title'] = "كوبون من %s (%s)";
$_['ms_cart_coupon_heading'] = "استخدام الكوبون";
$_['ms_cart_coupon_field_label'] = "ادخل رمز الكوبون هنا";
$_['ms_cart_coupon_field_placeholder'] = "ادخل رمز الكوبون...";
$_['ms_cart_coupon_button_apply'] = "تطبيق الكوبون";
$_['ms_cart_coupon_error_empty'] = "خانة الكوبونات فارغة!";
$_['ms_cart_coupon_error_apply'] = "تعذر تطبيق الكوبون على منتجات %s!";
$_['ms_cart_coupon_success_applied'] = "تم تطبيق الكوبون على منتجات %s بنجاح!";

// Stripe subscription
$_['ms_stripe_subscription'] = 'الاشتراك';
$_['ms_stripe_subscription_plan_selected'] = 'الخطة';
$_['ms_stripe_subscription_terms'] = 'الشروط';
$_['ms_stripe_subscription_plan_interval_day'] = "يومي";
$_['ms_stripe_subscription_plan_interval_month'] = "شهري";
$_['ms_stripe_subscription_plan_interval_year'] = "سنوي";
$_['ms_stripe_subscription_plan_base_info'] = "%s %s";
$_['ms_stripe_subscription_plan_per_seat_info'] = "%s لكل منتج %s";
$_['ms_stripe_subscription_your_card'] = "بطاقتك";
$_['ms_stripe_subscription_period'] = "الدفعة التالية";
$_['ms_stripe_subscription_button_signup'] = "التسجيل";
$_['ms_stripe_subscription_error_card_details'] = "خطأ: يجب تزويدنا بتفاصيل بطاقتك!";
$_['ms_stripe_subscription_error_subscription'] = "خطأ: فشل إنشاء الإشتراك!";

// Complaint seller/product form
$_['ms_complaint_form_breadcrumbs'] = 'تقديم بلاغ';
$_['ms_complaint_form_heading'] = 'تقديم بلاغ';
$_['ms_complaint_form_title'] = 'يرجى شرح سبب تقديم هذا البلاغ:';
$_['ms_complaint_form_error_comment_short'] = 'نص البلاغ يجب أن يكون أكثر من %s حرفاً!';
$_['ms_complaint_form_error_comment_long'] = 'نص البلاغ يجب أن يكون أقل من %s حرفاً!';
$_['ms_complaint_form_success'] = 'تم تقديم البلاغ!';
$_['ms_complaint_product_button_title'] = 'الإبلاغ عن هذا المنتج';
$_['ms_complaint_button_submit'] = 'تقديم البلاغ';
$_['ms_complaint_seller_link_title'] = 'الإبلاغ عن هذا البائع';

$_['ms_complaint_type_product'] = 'منتج';
$_['ms_complaint_type_seller'] = 'بائع';

// Complaint emails to admin
$_['ms_mail_admin_subject_complaint_received'] = 'تم استلام شكوى جديدة من عميل حول %s';
$_['ms_mail_admin_complaint_received'] = <<<EOT
تم تقديم شكوى من عميل بخصوص التالي %s: %s.

سبب البلاغ:
%s

يرجى الضغط على <a href="%s">هذا الرابط</a> لعرض وإدارة الشكاوى.

EOT;

// Holiday mode
$_['ms_account_setting_holiday_mode_tab'] = 'وضع الإجازة';
$_['ms_account_setting_holiday_mode_active'] = 'تفعيل (لن يتم قبول طلبات جديدة في وضع الإجازة)';
$_['ms_account_setting_holiday_mode_inactive'] = 'تعطيل (متجرك مفعل ويستقبل طلبات جديدة)';
$_['ms_account_setting_holiday_mode_message'] = 'رسالة إلى العملاء';
$_['ms_account_setting_holiday_mode_message_note'] = 'ضع ملاحظة حول إجازتك لعرضها على عملائك في صفحات متجرك (مثال: أنا في إجازة ولن استلم طلبات حتى تاريخ 22/07!).';

$_['ms_account_setting_holiday_mode_warning'] = 'وضع الإجازة مفعل ومتجرك لا يستقبل طلبات جديدة!';

$_['ms_front_holiday_mode_warning'] = 'هذا المتجر لا يستقبل طلبات جديدة حالياً:<br />%s';
$_['ms_front_holiday_mode_warning_empty'] = 'هذا المتجر لا يستقبل طلبات جديدة حالياً.';

// MultiMerch notifications
$_['ms_seller_account_notification'] = "الإشعارات";
$_['ms_seller_account_notification_breadcrumbs'] = "الإشعارات";
$_['ms_seller_account_notification_message'] = "رسالة";

// Invoicing system
$_['ms_invoices'] = "Invoices";
$_['ms_invoice_heading'] = "Invoices";
$_['ms_invoice_breadcrumbs'] = "Invoices";
$_['ms_invoice_column_invoice_id'] = "Invoice #";
$_['ms_invoice_column_payment_info'] = "Payment";
$_['ms_invoice_column_type'] = "Type";
$_['ms_invoice_column_title'] = "Title";
$_['ms_invoice_column_status'] = "Status";
$_['ms_invoice_column_date_generated'] = "Date generated";
$_['ms_invoice_column_total'] = "Total";

$_['ms_invoice_error_not_selected'] = 'You must select at least one invoice!';

$_['ms_invoice_type_listing'] = "Listing fee";
$_['ms_invoice_type_signup'] = "Signup fee";
$_['ms_invoice_type_sale'] = "Sale";
$_['ms_invoice_type_payout'] = "Payout";

$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID] = '<p style="color: red">Unpaid</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_PAID] = '<p style="color: green">Paid</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_VOIDED] = '<p style="color: red">Voided</p>';

$_['ms_invoice_title_product_listing'] = "Listing fee for product <a href='%s'>%s</a>";
$_['ms_invoice_title_seller_signup'] = "Signup fee at %s";
$_['ms_invoice_title_sale_order_commission'] = "Marketplace fee for products in order <a href='%s'>#%s</a>";
$_['ms_invoice_title_sale_order_products'] = "Order <a href='%s'>#%s</a> products";
$_['ms_invoice_title_seller_payout'] = "Payout to %s";

$_['ms_invoice_item_title_product_listing'] = "Listing fee for product %s";
$_['ms_invoice_item_title_seller_signup'] = "Signup fee at %s";
$_['ms_invoice_item_title_sale_commission'] = "Marketplace fee for %s in order #%s";
$_['ms_invoice_item_title_sale_product'] = "Product %s";

$_['ms_invoice_total_title_subtotal'] = "Subtotal";
$_['ms_invoice_total_title_shipping'] = "Marketplace shipping";
$_['ms_invoice_total_title_mm_shipping_total'] = "Vendor shipping";
$_['ms_invoice_total_title_coupon'] = "Coupon";
$_['ms_invoice_total_title_ms_coupon'] = "Vendor coupon";
$_['ms_invoice_total_title_tax'] = "Tax";
$_['ms_invoice_total_title_total'] = "Total";

// Shipping system
$_['ms_shipping_per_product_override_off_note'] = "Shipping costs for this product will be calculated automatically according to <a href='%s' target='_blank'>your global shipping rules</a>.";
$_['ms_shipping_per_product_override_off_btn'] = "Override global rules for this product?";
$_['ms_shipping_per_product_override_on_note'] = "Specify custom shipping rules for this product to override <a href='%s' target='_blank'>your global shipping rules</a>.";
$_['ms_shipping_per_product_override_on_btn'] = "Remove the override and fall back to global rules?";

$_['ms_shipping_title'] = "Shipping";
$_['ms_shipping_rules_manage_title'] = "Manage shipping rules";
$_['ms_shipping_field_shipping_from'] = "Shipping from";
$_['ms_shipping_field_processing_days'] = "Processing days";
$_['ms_shipping_field_destinations'] = "Destinations";
$_['ms_shipping_field_shipping_method'] = "Shipping method";
$_['ms_shipping_field_delivery_time'] = "Delivery time";
$_['ms_shipping_field_cart_weight'] = "Cart weight (from - to)";
$_['ms_shipping_field_cart_total'] = "Cart total (from - to)";
$_['ms_shipping_field_cost'] = "Cost";
$_['ms_shipping_field_cost_ai'] = "Cost per additional item";
$_['ms_shipping_btn_add_destination'] = "+ Add destination";

$_['ms_shipping_placeholder_cost_pwu'] = "+ %s per weight unit";
$_['ms_shipping_placeholder_cost_ai'] = "+ %s per additional item";

$_['ms_shipping_error_rules_empty_global'] = "Please <a href='%s'>configure your shipping rules</a> to make your products available to customers";
$_['ms_shipping_error_field_from_country_id_required'] = "You must specify shipping country";
$_['ms_shipping_error_field_processing_days_required'] = "You must specify processing time";
$_['ms_shipping_error_rules_required'] = "Please specify at least one shipping rule";
$_['ms_shipping_error_rule_destinations_required'] = "You must specify a destination for the shipping rule";
$_['ms_shipping_error_rule_shipping_method_id_required'] = "You must specify a shipping method for the shipping rule";
$_['ms_shipping_error_rule_delivery_time_id_required'] = "You must specify a delivery time for the shipping rule";
$_['ms_shipping_error_rule_weight_range_required'] = "You must specify a cart weight range for the shipping rule";
$_['ms_shipping_error_rule_total_range_required'] = "You must specify a cart total range for the shipping rule";
$_['ms_shipping_error_rule_cost_required'] = "You must specify a cost for the shipping rule";
$_['ms_shipping_error_rule_cost_pwu_required'] = "You must specify a cost per weight unit for the shipping rule";
$_['ms_shipping_error_rule_cost_ai_required'] = "You must specify a cost per additional item for the shipping rule";

$_['ms_shipping_checkout_info_title'] = "Please select delivery options for the products in your order.";
$_['ms_shipping_checkout_info_title_with_digital'] = "Please select delivery options for the tangible products in your cart.";
$_['ms_shipping_checkout_title_seller_table'] = "Select delivery options for %s";
$_['ms_shipping_checkout_title_marketplace_table'] = "Select delivery options for";
$_['ms_shipping_checkout_title_select_option'] = "Select delivery option";
$_['ms_shipping_checkout_title_shipping_total'] = "Total shipping";
$_['ms_shipping_checkout_remove_product'] = "Remove product";
$_['ms_shipping_checkout_error_general'] = "There is an issue with the delivery of some of the items in your order (see below for more information).";
$_['ms_shipping_checkout_error_individual_general'] = "This item can't be dispatched to your selected address. Please change the address or delete the item from your order.";
$_['ms_shipping_checkout_error_combined_general'] = "These items can't be dispatched to your selected address. Please change the address or delete the items from your order.";
$_['ms_shipping_checkout_error_cart_weight_less_than_min'] = "The total weight of the products (%s) is less than the minimum weight required by this seller (%s). Please add a few more items by this seller to your order.";
$_['ms_shipping_checkout_error_cart_weight_more_than_max'] = "The total weight of the products (%s) exceeds the maximum weight allowed by this seller (%s). Please delete some items by this seller from your order.";
$_['ms_shipping_checkout_error_cart_total_less_than_min'] = "The total price of the products (%s) is less than the minimum total required by this seller (%s). Please add a few more items by this seller to your order.";
$_['ms_shipping_checkout_error_cart_total_more_than_max'] = "The total price of the products (%s) exceeds the maximum total allowed by this seller (%s). Please delete some items by this seller from your order.";

$_['ms_wishlist_title'] = "Wishlist";
$_['ms_wishlist_breadcrumbs'] = "Wishlist";
$_['ms_wishlist_date_added'] = "Added on %s";
$_['ms_wishlist_add_success'] = "You have successfully added <strong>%s</strong> to your Wishlist!";
$_['ms_wishlist_add_error_login'] = "Please, log in or sign up to add products to your Wishlist.";
$_['ms_wishlist_btn_to_wishlist'] = "Go to Wishlist";
$_['ms_wishlist_btn_login'] = "Log in / Sign up";
$_['ms_wishlist_btn_continue'] = "Continue shopping";

// Favorite sellers
$_['ms_favorite_seller_title'] = "Favorite sellers";
$_['ms_favorite_seller_breadcrumbs'] = "Favorite sellers";
$_['ms_favorite_seller_no_results'] = "You are not following any seller!";
$_['ms_favorite_seller_success_follow'] = "You have successfully added <strong>%s</strong> to your favorite sellers!";
$_['ms_favorite_seller_success_unfollow'] = "You have removed <strong>%s</strong> from your favorite sellers.";
$_['ms_favorite_seller_error_login'] = "Please, log in or sign up to follow %s.";
$_['ms_favorite_seller_follow_button'] = "Follow seller";
$_['ms_favorite_seller_unfollow_button'] = "Unfollow";
$_['ms_favorite_seller_btn_login'] = "Log in / Sign up";
$_['ms_favorite_seller_btn_to_favorite_sellers'] = "View your favorite sellers";
$_['ms_favorite_seller_btn_continue'] = "Continue shopping";
$_['ms_favorite_seller_date_added'] = "Added on %s";

// Product MSF variations
$_['ms_product_msf_variation_price_from'] = "From %s";
$_['ms_product_msf_variation_error_not_selected'] = "Please, select product variant!";
$_['ms_product_msf_variation_error_status'] = "Product is not available!";
$_['ms_product_msf_variation_error_out_of_stock'] = "Product is out of stock!";
$_['ms_product_msf_variation_error_quantity'] = "Product is not available in selected quantity!";

// MSF seller properties
$_['ms_field_seller_property_error_field_empty'] = "The '%s' field is required";
$_['ms_field_seller_property_error_field_length'] = "The '%s' field needs to be less than '255' in length!";

// MM product filter blocks
$_['ms_product_filter_block_heading'] = "Filters";
$_['ms_product_filter_block_title_general'] = "General";
$_['ms_product_filter_block_title_attributes'] = "Attributes";
$_['ms_product_filter_block_title_oc_options'] = "Options";
$_['ms_product_filter_block_title_oc_manufacturers'] = "Manufacturers";
$_['ms_product_filter_block_title_price'] = "Price";
$_['ms_product_filter_block_title_category'] = "Categories";

/***** INSERTED *****/$_['ms_print_order_packing_slip'] = "Print packing slip";
/***** INSERTED *****/$_['ms_print_order_invoice'] = "Print invoice";
