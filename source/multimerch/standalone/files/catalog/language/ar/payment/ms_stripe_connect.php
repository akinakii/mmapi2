<?php
/***** INSERTED *****/$_['text_title']    = "Stripe";

$_['text_start_title'] = "إبدأ في قبول مدفوعات البطاقات الإتمانية عبر سترايب";
$_['text_start_description'] = "قم بربط حساب سترايب الخاص بك للبدء في قبول المدفوعات من العملاء على الفور وبشكل مباشر في حسابك المصرفي. <a href='//stripe.com/se/payments' target='_blank'>المزيد من التفاصيل...</a>";

$_['text_connected_account'] = "أربط حساب سترايب المعرف.";
$_['text_deauthorize'] = "إلغاء الإعتماد";
$_['text_click_here'] = "إضغط هنا";
$_['text_payment_description'] = "دفعات سترايب: طلب #%s";
$_['text_checkout_confirm'] = "وضع الطلب";

$_['success_account_connected'] = "تم ربط حساب سترايب بنجاح!";
$_['success_account_connected_alert'] = "حساب سترايب الخاص بك مربوط حالياً ويمكنك قبول المدفوعات بالبطاقة الإتمانية من عملائك.";
$_['success_deauthorized'] = "لقم قمت بإلغاء الإعتماد من حساب سترايب المربوط بنجاح!";

$_['error_account_not_connected'] = "خطأ: حساب سترايب غير مربوط";
$_['error_access_denied'] = " خطأ: المستخدم رفض طلب ربط حساب سترايب الخاص بك";
$_['error_authorize'] = "خطأ: حسابك غير مربوط بسترايب";
$_['error_api_connection'] = "خطأ: حدث خطأ في اتصال API";
$_['error_deauthorize'] = "خطأ: أنت غير مربوط بحساب سترايب الخاص بمالك السوق, او أن حسابك غير موجود!";
$_['error_checkout_token'] = "خطأ: تعذر استرداد الرمز الشريطي";
$_['error_no_order'] = "خطأ: الطلب غير موجود";
/***** INSERTED *****/$_['error_card'] = "Your card was declined. Please contact your card issuer or try a different card.";
