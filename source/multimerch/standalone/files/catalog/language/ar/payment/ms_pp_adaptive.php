<?php
$_['ppa_title']    = 'PayPal Adaptive';
$_['ppa_sandbox']	= 'تحذير: وسيلة الدفع هذه في \'الوضع الاختباري\'. لن يتم خصم أي مبلغ من حسابك.';

$_['ppa_error_distribution']        = 'خطأ في التهيئة: توزيع المبلغ غير صالح. رقم الطلب: %s';
$_['ppa_error_noreceivers']        = 'خطأ في التهيئة: لم يتم تحديد مستلمين صالحين. رقم الطلب: %s';
$_['ppa_error_no_unique_mails']        = 'خطأ في التهيئة: اثنان أو أكثر من المستلمين لديهم نفس عنوان PayPal. رقم الطلب: %s';
$_['ppa_error_generic']        = 'خطأ في التهيئة: يرجى التواصل مع الإدارة. رقم الطلب: %s';
$_['ppa_error_request']        = 'خطأ في طلب PayPal: معرف الارتباط %s';
$_['ppa_error_response']        = 'خطأ في استجابة PayPal: الحالة %s معرف الارتباط: %s';

$_['ms_transaction_order'] = 'البيع: رقم الطلب #%s';