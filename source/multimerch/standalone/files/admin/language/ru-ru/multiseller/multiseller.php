<?php

// General
$_['ms_enabled'] = 'Вкл.';
$_['ms_disabled'] = 'Выкл.';
$_['ms_apply'] = 'Применить';
$_['ms_type'] = 'Тип';
$_['ms_type_checkbox'] = 'Чекбокс';
$_['ms_type_date'] = 'Дата';
$_['ms_type_datetime'] = 'Дата и время';
$_['ms_type_file'] = 'Файл';
$_['ms_type_image'] = 'Изображение';
$_['ms_type_radio'] = 'Переключатель';
$_['ms_type_select'] = 'Выбор';
$_['ms_type_text'] = 'Текст';
$_['ms_type_textarea'] = 'Текстовое поле';
$_['ms_type_time'] = 'Время';
$_['ms_type_choose'] = 'Выбор';
$_['ms_type_input'] = 'Ввод';
$_['ms_store'] = 'Магазин';
$_['ms_store_default'] = 'По умолчанию';
$_['ms_id'] = '#';
$_['ms_login_as_vendor'] = '<a href="%s">Войдите как продавец</a> чтобы редактировать информацию о доставке';
$_['ms_sort_order'] = 'Порядок сортировки';

$_['ms_default_select_value'] = 'По умолчанию';
$_['ms_placeholder_deleted'] = "*Удален*";
$_['ms_seller_deleted'] = "*Продавец удален*";
$_['ms_product_deleted'] = "*Товар удален*";

$_['ms_button_approve'] = 'Утвердить';
$_['ms_button_decline'] = 'Отклонить';

$_['ms_fixed_coupon_warning'] = "<b>Внимание:</b> Установленные купоны (вся корзина) не могут быть применены к корзине покупок разных продавцов и предостеречь продавца от правильно рассчитанной комиссии! Процентные купоны будут работать, как ожидалось.";
$_['ms_voucher_warning'] = "<b>Внимание:</b> Подарочные ваучеры не могут быть применены к корзине покупок разных продавцов и предостеречь продавца от правильно рассчитанной комиссии!";
$_['ms_error_directory'] = "Внимание: Не удалось создать директорию: %s. Пожалуйста, создайте директорию вручную. <br />";
$_['ms_error_directory_notwritable'] = "Внимание: Директория уже существует и запрещена для записи: %s. Пожалуйста, разрешите запись в директорию. <br />";
$_['ms_error_directory_exists'] = "Внимание: Директория уже существует: %s. Пожалуйста, убедитесь, что она пустая. <br />";
$_['ms_error_product_publish'] = 'Ошибка публикации товаров: учетная запись продавца неактивна.';
$_['ms_success_installed'] = 'Расширение успешно установлено';
$_['ms_success_product_status'] = 'Статус товара изменен.';

$_['ms_db_upgrade'] = ' Пожалуйста, <a href="%s">нажмите здесь</a> чтобы обновить базу данных MultiMerch Marketplace до последней версии (%s -> %s).';
$_['ms_files_upgrade'] = ' Внимание: Версия файлов MultiMerch (%s) старше чем версия необходимая вашей структуре базы данных (%s). Это может быть вызвано загрузкой старой версии MultiMerch сверху по новой. Пожалуйста, обновите свои файлы MultiMerch или переустановите MultiMerch';
$_['ms_db_success'] = 'База данных MultiMerch обновлена!';
$_['ms_db_latest'] = 'База данных MultiMerch уже обновлена!';
$_['ms_multimerch_not_installed'] = 'Предупреждение: MultiMerch не установлено!';

$_['ms_error_php_version'] = 'Для работы MultiMerch требуется PHP 7.0 или новее. Пожалуйста, свяжитесь с провайдером вашего хостинга для обновления PHP, или измените его версию через CPanel > Выбрать версию PHP';

$_['heading_title'] = '<b>[MultiMerch]</b> Digital Marketplace';
$_['text_no_results'] = 'Нет результатов';
$_['error_permission'] = 'Внимание: У вас нет доступа к изменению модуля!';

$_['ms_error_withdraw_response'] = 'Ошибка: нет ответа';
$_['ms_error_withdraw_status'] = 'Ошибка: неудачная транзакция';
$_['ms_success'] = 'Успешно завершено';
$_['ms_success_transactions'] = 'Транзация успешно завершена';
$_['ms_success_payment_deleted'] = 'Платеж удален';
$_['text_success']                 = 'Настройки успешно изменены!';

$_['ms_none'] = 'Нет';
$_['ms_seller'] = 'Продавец';
$_['ms_all_sellers'] = 'Все продавцы';
$_['ms_balance'] = 'Счет';
$_['ms_amount'] = 'Сумма';
$_['ms_product'] = 'Товар';
$_['ms_quantity'] = 'Количество';
$_['ms_sales'] = 'Продажы';
$_['ms_price'] = 'Цена';
$_['ms_net_amount'] = 'Чистая сумма';
$_['ms_from'] = 'От';
$_['ms_to'] = 'К';
$_['ms_paypal'] = 'PayPal';
$_['ms_date_created'] = 'Дата создания';
$_['ms_status'] = 'Статус';
$_['ms_date_modified'] = 'Дата изменения';
$_['ms_date_paid'] = 'Дата оплаты';
$_['ms_date_last_paid'] = 'Дата последнего платежа';
$_['ms_date'] = 'Дата';
$_['ms_description'] = 'Описание';
$_['ms_confirm'] = 'Подтвердить';
$_['ms_total'] = 'Всего';
$_['ms_method'] = 'Способ';

$_['ms_commission'] = 'Комиссия';
$_['ms_commissions_fees'] = 'Сбор';

$_['ms_store_settings'] = 'Настройки магазина';
$_['ms_seller_full_name'] = "Полное имя";
$_['ms_seller_address'] = "Адрес";
$_['ms_seller_address1'] = "Адресная строка 1";
$_['ms_seller_address1_placeholder'] = 'Улица, а/я, название компании';
$_['ms_seller_address2'] = "Адресная строка 2";
$_['ms_seller_address2_placeholder'] = 'Квартира, здание, корпус, этаж';
$_['ms_seller_city'] = "Город";
$_['ms_seller_state'] = "Штат/Область/Регион";
$_['ms_seller_zip'] = "Почтовый индекс";
$_['ms_seller_country'] = "Страна";
$_['ms_seller_company'] = 'Компания';

$_['ms_catalog_sellerinfo_information'] = 'Информация';
$_['ms_seller_website'] = 'Вебсайт';
$_['ms_seller_phone'] = 'Телефон';
$_['ms_seller_error_deleting'] = 'Ошибка удаления аккаунта продавца';
$_['ms_seller_success_deleting'] = 'Аккаунт продавца успешно удален';

$_['ms_commission_' . MsCommission::RATE_SALE] = 'Сбор с продажи';
$_['ms_commission_' . MsCommission::RATE_LISTING] = 'Сбор за публикацию';
$_['ms_commission_' . MsCommission::RATE_SIGNUP] = 'Регистрационный сбор';

$_['ms_commission_short_' . MsCommission::RATE_SALE] = 'П';
$_['ms_commission_short_' . MsCommission::RATE_LISTING] = 'Л';
$_['ms_commission_short_' . MsCommission::RATE_SIGNUP] = 'РЕГ';
$_['ms_commission_actual'] = 'Текущие размеры комиссий';

$_['ms_name'] = 'Название';
$_['ms_config_width'] = 'Ширина';
$_['ms_config_height'] = 'Высота';
$_['ms_description'] = 'Описание';

$_['ms_enable'] = 'Включить';
$_['ms_disable'] = 'Выключить';
$_['ms_create'] = 'Создать';
$_['ms_delete'] = 'Удалить';
$_['ms_uninstall'] = 'Удалить';
$_['ms_remove'] = 'Убрать';
$_['ms_view_in_store'] = 'Посмотреть в магазине';
$_['ms_view'] = 'Посмотреть';
$_['ms_add'] = 'Добавить';
$_['ms_back'] = 'Назад';
$_['ms_enable_at_registration'] = 'Включить на регистрации';

$_['ms_button_pay'] = 'Оплатить';

$_['ms_logo'] = 'Лого';
$_['ms_field'] = 'Поле';

// Menu
$_['ms_menu_multiseller'] = 'MultiMerch';
$_['ms_menu_dashboard'] = 'Панель управления';
$_['ms_menu_sellers'] = 'Продавцы';
$_['ms_menu_seller_groups'] = 'Группы продавцов';
$_['ms_menu_seller_properties'] = 'Атрибуты продавца';
$_['ms_menu_catalog'] = 'Каталог';
$_['ms_menu_attributes'] = 'Атрибуты';
$_['ms_menu_categories'] = 'Категории';
$_['ms_menu_options'] = 'Опции';
$_['ms_menu_products'] = 'Товары';
$_['ms_menu_imports'] = 'Импорт';
$_['ms_menu_custom_fields'] = 'Произвольные поля';
$_['ms_menu_orders'] = 'Заказы';
$_['ms_menu_finances'] = 'Финансы';
$_['ms_menu_payment'] = 'Платежи';
$_['ms_menu_invoice_seller'] = 'Счета продавцов';
$_['ms_menu_invoice'] = 'Счета';
$_['ms_menu_payment_gateway'] = 'Способы оплаты';
$_['ms_menu_payment_gateway_tab_seller'] = 'Шлюз продавца';
$_['ms_menu_payment_gateway_tab_marketplace'] = 'Шлюз торговой площадки';
$_['ms_menu_payment_gateway_tab_customer'] = 'Шлюз пользователя';
$_['ms_menu_payment_gateway_settings'] = 'Настройка способа оплаты';
$_['ms_menu_payout'] = 'Выплаты';
$_['ms_menu_payout_generate'] = 'Cгенерировать';
$_['ms_menu_payout_view'] = 'Просмотреть выплаты';
$_['ms_menu_transactions'] = 'Транзакции';
$_['ms_menu_conversations'] = 'Диалоги';
$_['ms_menu_complaints'] = 'Жалобы';
$_['ms_menu_reviews'] = 'Отзывы';
$_['ms_menu_questions'] = 'Вопросы';
$_['ms_menu_shipping_method'] = 'Способы доставки';
$_['ms_menu_event'] = 'Активность';
$_['ms_menu_settings'] = 'Настройки';
$_['ms_menu_install'] = 'Установить';
$_['ms_menu_marketplace'] = 'Магазин';
$_['ms_menu_system'] = 'Система';
$_['ms_menu_coupon'] = 'Купоны';
$_['ms_menu_notification'] = 'Оповещения';
$_['ms_menu_zones'] = 'Зоны';

$_['ms_menu_reports'] = 'Отчеты';
$_['ms_menu_reports_sales'] = 'Продажи';
$_['ms_menu_reports_sales_list'] = 'Список продаж';
$_['ms_menu_reports_sales_by_day'] = 'Продажи за день';
$_['ms_menu_reports_sales_by_month'] = 'Продажи за месяц';
$_['ms_menu_reports_sales_by_product'] = 'Продажи по товарам';
$_['ms_menu_reports_sales_by_seller'] = 'Продажи по продавцам';
$_['ms_menu_reports_sales_by_customer'] = 'Продажи по клиенту';
$_['ms_menu_reports_finances'] = 'Финансы';
$_['ms_menu_reports_finances_transactions'] = 'Транзакции';
$_['ms_menu_reports_finances_seller'] = 'Финансы продавца';
$_['ms_menu_reports_finances_payouts'] = 'Выплаты';
$_['ms_menu_reports_finances_payments'] = 'Платежи';

$_['ms_menu_customers'] = 'Покупатели';

// Settings
$_['ms_settings_heading'] = 'Настройки';
$_['ms_settings_breadcrumbs'] = 'Настройки';
$_['ms_config_seller_validation'] = 'Валидация продавцов';
$_['ms_config_seller_validation_note'] = 'Если включено, то аккаунты новых продавцов будут созданы как неактивные до утверждения администратором';
$_['ms_config_seller_validation_none'] = 'Без валидации';
$_['ms_config_seller_validation_activation'] = 'Активация через эл. почту';
$_['ms_config_seller_validation_approval'] = 'Подтверждение вручную';

$_['ms_config_seller_landing_page'] = "Посадочная страница продавца";
$_['ms_config_seller_landing_page_note'] = "Выберите посадочную страницу для продавцов, используемую MultiMerch по умолчанию";

$_['ms_error_htaccess'] = 'MultiMerch SEO требует включить файл .htaccess. Пожалуйста, переименуйте '. $_SERVER['DOCUMENT_ROOT'] .'/.htaccess.txt to '. $_SERVER['DOCUMENT_ROOT'] .'/.htaccess в корневых настройках и убедитесь, что ваш сервер настроен на поддержку mod_rewrite.';
$_['ms_error_htaccess_txt'] = 'MultiMerch SEO требует включить файл .htaccess. Пожалуйста, скопируйте файл htaccess.txt из архива в корни вашей торговой площадки '. $_SERVER['DOCUMENT_ROOT'] .', переименуйте его в .htaccess и убедитесь, что сервер настроен на поддержку mod_rewrite.';

$_['ms_settings_error_vendor_shipping_methods'] = 'Пожалуйста, <a target="_blank" href="%s">создайте по крайней мере один способ доставки</a> для корректной работы!';
$_['ms_settings_error_vendor_shipping_times'] = 'Пожалуйста, создайте по крайней мере одну опцию времени доставки для корректной работы!';
$_['ms_settings_error_vendor_duplicate_seo_slug'] = 'Пожалуйста, укажите разные ЧПУ для продавцов и товаров!';
$_['ms_settings_error_vendor_seo_slug_required'] = "Поле ключевых слов для SEO должно быть заполнено";

$_['ms_config_general'] = 'Общие';
$_['ms_config_limits'] = 'Лимиты';
$_['ms_config_file_types'] = 'Типы файлов';
$_['ms_config_shipping'] = 'Доставка';
$_['ms_config_product_fields'] = 'Поля формы товара';

$_['ms_config_product_validation'] = 'Валидация товаров';
$_['ms_config_product_validation_note'] = "'Без валидации' означает, что новые товары, созданные продавцами, будут активны сразу. 'Подтверждение вручную' означает, что они будут неактивными до тех пор, пока они не будут одобрены администратором";
$_['ms_config_product_validation_from_group_settings'] = 'Из настроек группы';
$_['ms_config_product_validation_none'] = 'Без валидации';
$_['ms_config_product_validation_approval'] = 'Подтверждение вручную';


$_['ms_config_allow_free_products'] = 'Разрешить бесплатные товары';

$_['ms_config_allow_digital_products'] = 'Разрешить цифровые товары';
$_['ms_config_allow_digital_products_note'] = "Если включено, то продавцы будут иметь возможность создавать товары, в которых доставка не обязательна";

$_['ms_config_minmax_product_price'] = 'Минимальная и максимальная цена товара';
$_['ms_config_minmax_product_price_note'] = 'Минимальная и максимальная цена товара (0 = без ограничений)';

$_['ms_config_msf_attributes_system'] = "Система атрибутов товара";
$_['ms_config_msf_attributes_system_oc'] = "OpenCart-атрибуты";
$_['ms_config_msf_attributes_system_msf'] = "MultiMerch-атрибуты";
$_['ms_config_msf_attributes_system_note'] = "Выберите систему атрибутов товара, которую хотите использовать на вашей торговой площадке:<BR><strong>MultiMerch-атрибуты</strong> могут быть использованы в разных типах, эту систему можно привязать к категориям вашей торговой площадки<BR><strong>OpenCart-атрибуты</strong> возможны только в форме текста и действуют глобально по всему сайту, их нельзя привязывать к категориям";

$_['ms_config_msf_variations'] = "Вариации товара";
$_['ms_config_msf_variations_enable'] = "Включить вариации товара";
$_['ms_config_msf_variations_enable_note'] = "Включить комментарии к вариациям товара";
$_['ms_config_msf_variations_system'] = "Система вариаций товара";
$_['ms_config_msf_variations_system_oc'] = "OpenCart-опции";
$_['ms_config_msf_variations_system_msf'] = "MultiMerch-вариации";
$_['ms_config_msf_variations_system_note'] = "Выберите систему вариаций товара, которую хотите использовать на вашей торговой площадке:<BR><strong>MultiMerch-вариации</strong> позволяют вашим продавцам создавать товары, которые будут иметь несколько вариаций, эту систему можно привязать к категориям вашей торговой площадки<BR><strong>OpenCart-опции</strong> дают продавцам возможность настраивать базовую стоимость товара, эта система общая для всего сайта, ее нельзя привязывать к категориям";

$_['ms_config_msf_seller_properties'] = "Атрибуты продавца";
$_['ms_config_msf_seller_properties_enable_custom'] = "Включить произвольные атрибуты";
$_['ms_config_msf_seller_properties_note'] = "Включить создание произвольных атрибутов продавца в дополнение к стандартным полям";
$_['ms_config_msf_seller_properties_manage'] = "Управлять атрибутами";

$_['ms_config_custom_fields'] = "Произвольные поля";
$_['ms_config_custom_fields_enable'] = "Включить произвольные поля MultiMerch (устаревшее)";

$_['ms_config_product_attributes_options'] = 'Атрибуты и вариации';
$_['ms_config_allow_attributes'] = 'Позволить продавцам создавать атрибуты';
$_['ms_config_allow_attributes_note'] = 'Разрешить продавцам создавать свои собственные атрибуты товара в дополнение к основным атрибутам торговой площадки';
$_['ms_config_allow_options'] = 'Позволить продавцам создавать опции';
$_['ms_config_allow_options_note'] = 'Разрешить продавцам создавать свои собственные опции товара в дополнение к основным опциям торговой площадки';
$_['ms_config_allowed_option_types'] = 'Разрешить типы опций';
$_['ms_config_allowed_option_types_note'] = 'Разрешить продавцам создавать только эти конкретные типы опций товара';
$_['ms_config_option_type_select'] = 'Выпадающий список';
$_['ms_config_option_type_radio'] = 'Радио-кнопка';
$_['ms_config_option_type_checkbox'] = 'Флажок';
$_['ms_config_option_type_text'] = 'Text';
$_['ms_config_option_type_textarea'] = 'Текстовая область';
$_['ms_config_option_type_file'] = 'Файл';
$_['ms_config_option_type_date'] = 'Дата';
$_['ms_config_option_type_time'] = 'Время';
$_['ms_config_option_type_datetime'] = 'Дата, время';

$_['ms_config_product_questions'] = 'Вопросы по товару';
$_['ms_config_allow_question'] = 'Включить вопросы';
$_['ms_config_allow_question_note'] = 'Разрешить клиентам задавать вопросы на странице товара';

$_['ms_config_allowed_image_types'] = 'Разрешенные расширения изображений';
$_['ms_config_allowed_image_types_note'] = 'Разрешенные расширения изображений';

$_['ms_config_images_limits'] = 'Ограничение количества изображений товара';
$_['ms_config_images_limits_note'] = 'Минимальное и максимальное количество изображений для товара (0 = без ограничений)';

$_['ms_config_downloads_limits'] = 'Ограничение количества файлов товара';
$_['ms_config_downloads_limits_note'] = 'Минимальное и максимальное количество файлов для товара (0 = без ограничений))';

$_['ms_config_allowed_download_types'] = 'Разрешенные расширения файлов';
$_['ms_config_allowed_download_types_note'] = 'Разрешенные расширения файлов';

$_['ms_config_paypal_sandbox'] = 'Режим PayPal Sandbox';
$_['ms_config_paypal_sandbox_note'] = 'Использовать PayPal в режиме Sandbox для тестирования';

$_['ms_config_paypal_address'] = 'Адрес PayPal';
$_['ms_config_paypal_address_note'] = 'Адрес счета PayPal для приема оплаты комиссий';


$_['ms_config_product_categories'] = 'Категории';
$_['ms_config_allow_seller_categories'] = 'Включить категории продавцов';
$_['ms_config_allow_seller_categories_note'] = 'Если включено, то продавцы будут иметь возможность создавать свои собственные категории товара независимо от основных категорий торговой площадки';
$_['ms_config_allow_multiple_categories'] = 'Разрешить несколько категорий';
$_['ms_config_allow_multiple_categories_note'] = 'Разрешить продавцам добавлять товары в несколько категорий';
$_['ms_config_enforce_childmost_categories'] = 'Упрощенный вид и выбор категории';
$_['ms_config_enforce_childmost_categories_note'] = 'Не позволять продавцам создавать недочерние списки товаров, чтобы упростить управление торговой площадкой с несколькими уровнями категорий. Это также отобразит товары из всех дочерних категорий для поиска по родительским категориям';
$_['ms_config_restrict_categories'] = 'Запрещенные категории';
$_['ms_config_restrict_categories_note'] = '<u>Запретить</u> продавцам добавлять товары в эти категории';

$_['ms_config_product_included_fields'] = 'Поля формы товара';
$_['ms_config_product_included_fields_note'] = 'Выберите поля товара которые будут отображаться продавцам в форме публикации товара';

$_['ms_config_seller_included_fields'] = 'Стандартные поля';
$_['ms_config_seller_included_fields_note'] = 'Выберите какие поля продавец обязан заполнить при регистрации, и какие поля он сможет редактировать в своем профиле и настройках аккаунта';
$_['ms_config_seller_field_account_info'] = 'Общая информация';
$_['ms_config_seller_field_firstname'] = 'Имя';
$_['ms_config_seller_field_lastname'] = 'Фамилия';
$_['ms_config_seller_field_email'] = 'Эл. почта';
$_['ms_config_seller_field_password'] = 'Пароль';
$_['ms_config_seller_field_store_info'] = 'Информация о магазине';
$_['ms_config_seller_field_storename'] = 'Название магазина';
$_['ms_config_seller_field_slogan'] = 'Слоган';
$_['ms_config_seller_field_description'] = 'Описание';
$_['ms_config_seller_field_website'] = 'Сайт';
$_['ms_config_seller_field_company'] = 'Компания';
$_['ms_config_seller_field_phone'] = 'Телефон';
$_['ms_config_seller_field_logo'] = 'Логотип';
$_['ms_config_seller_field_banner'] = 'Баннер';
$_['ms_config_seller_field_address_info'] = 'Адрес';
$_['ms_config_seller_field_fullname'] = 'Полное имя';
$_['ms_config_seller_field_address_1'] = 'Адрес 1';
$_['ms_config_seller_field_address_2'] = 'Адрес 2';
$_['ms_config_seller_field_city'] = 'Город';
$_['ms_config_seller_field_state'] = 'Штат/Провинция/Регион';
$_['ms_config_seller_field_zip'] = 'Почтовый индекс';
$_['ms_config_seller_field_country'] = 'Страна';

$_['ms_config_seller_terms_page'] = 'Включить условия использования';
$_['ms_config_seller_terms_page_note'] = 'Продавцы будут обязаны принять условия использования при создании учетной записи.';


$_['ms_config_finances'] = 'Финансы';
$_['ms_config_miscellaneous'] = 'Разное';
$_['ms_config_deprecated'] = 'Устаревший';
$_['ms_config_see_deprecated'] = 'Видеть устаревшие настройки';

// MM > Settings > Seller accounts
$_['ms_config_tab_sellers'] = 'Аккаунты продавцов';

// MM > Settings > Product publishing
$_['ms_config_tab_products'] = 'Публикация товаров';

// MM > Settings > Miscellaneous
$_['ms_config_misc_wishlist'] = "Список желаемых товаров";
$_['ms_config_misc_wishlist_enable'] = "Включить список желаемых товаров MultiMerch";
$_['ms_config_misc_wishlist_enable_note'] = "Выбрать систему списков желаемых товаров для ваших покупателей. Да включит список желаний MultiMerch, Нет вернет вас в списки желаний OpenCart.";

$_['ms_config_favorite_sellers'] = "Любимые продавцы";
$_['ms_config_favorite_sellers_allow'] = "Включить список любимых продавцов";
$_['ms_config_favorite_sellers_allow_note'] = "Разрешить пользователям подписываться на продавцов и добавлять их в список любимых продавцов";


// MM > Settings > Updates and licensing
$_['ms_config_updates'] = 'Обновления';
$_['ms_config_updates_license_info'] = 'MultiMerch лицензии & обновления';
$_['ms_config_updates_license_key'] = 'Лицензионный ключ';
$_['ms_config_updates_license_key_note'] = "Укажите лицензионный ключ MultiMerch, который можно найти в вашей учетной записи <a href='https://multimerch.com/' target='_blank'>MultiMerch</a>";
$_['ms_config_updates_license_activate'] = 'Активировать';
$_['ms_config_updates_updates'] = 'Обновления';
$_['ms_config_updates_updates_check'] = 'Проверить обновления';
$_['ms_config_updates_updates_not_activated'] = 'Пожалуйста, активируйте свою лицензию MultiMerch для получения обновлений!';
$_['ms_license_error_no_key'] = 'Ошибка: лицензионный ключ не указан!';
$_['ms_license_success_activated'] = 'Ваши копия MultiMerch успешно активирована!';
$_['ms_update_error_license'] = 'Ошибка: MultiMerch не активирован!';
$_['ms_update_success_no_updates'] = 'У вас последняя версия MultiMerch! (%s)';
$_['ms_update_success_available_update'] = 'Доступна новая версия MultiMerch! (%s -> %s)';
$_['ms_api_error'] = 'Ошибка: %s.';
$_['ms_api_error_license_generic'] = "Невозможно активировать лицензионный ключ! Пожалуйста, свяжитесь с MultiMerch по ссылке <a href='https://multimerch.com/' target='_blank'>https://multimerch.com/</a> для получения поддержки";
$_['ms_api_error_license_connection'] = "Невозможно подключиться к серверу лицензирования! Пожалуйста, убедитесь что Ваша конфигурация сервера не блокирует внешние запросы";
$_['ms_api_error_license_invalid'] = 'Недопустимый лицензионный ключ';
$_['ms_api_error_license_missing'] = 'Лицензионного ключа не существует';
$_['ms_api_error_license_not_activable'] = 'Лицензионный ключ не может быть активирован';
$_['ms_api_error_license_revoked'] = 'Лицензионный ключ аннулирован';
$_['ms_api_error_no_activations_left'] = 'Не осталось активаций';
$_['ms_api_error_license_expired'] = "Срок действия этой лицензии истек! Пожалуйста, свяжитесь с MultiMerch по ссылке <a href='https://multimerch.com/' target='_blank'>https://multimerch.com/</a> для получения поддержки и продления лицензии";
$_['ms_api_error_key_mismatch'] = 'Лицензионный ключ не подходит';
$_['ms_api_error_item_id'] = 'Неверный идентификатор лицензии';
$_['ms_api_error_item_name'] = 'Название товара не подходит';
$_['ms_api_error_no_site'] = 'Доменное имя не подходит';
$_['ms_api_error_unrecognized'] = 'Неизвестная ошибка';
$_['ms_api_error_request'] = 'Ошибка: произошла ошибка API -%s';
$_['ms_api_error_incorrect_response'] = 'Ошибка: неверный ответ сервера!';

// MM > Settings > Shipping
$_['ms_config_shipping'] = 'Доставка';
$_['ms_config_shipping_methods'] = 'Способы доставки продавца';
$_['ms_config_shipping_methods_manage'] = 'Управление способами доставки';
$_['ms_config_shipping_type'] = 'Тип доставки';
$_['ms_config_enable_store_shipping'] = 'Доставка магазина';
$_['ms_config_enable_vendor_shipping'] = 'Доставка продавца';
$_['ms_config_disable_shipping'] = 'Выкл.';
$_['ms_config_shipping_type_note'] = "Доставка торговой площадки позволит стандартную систему доставки OpenCart и по умолчанию расширение доставки, продавцы смогут контролировать доставку.\nДоставка продавца позволит мультивендорную MultiMerch систему доставки, где продавцы могут указать свои собственные тарифы на доставку продукции.\nОтключение доставки полностью отключит доставку и разрешит только цифровые товары в MultiMerch.";
$_['ms_config_shipping_delivery_times'] = 'Сроки доставки продавца';
$_['ms_config_shipping_delivery_time_add_btn'] = '+ Добавить время доставки';
$_['ms_config_shipping_delivery_time_comment'] = 'Нажмите дваждый на ячейку для редактирования';
$_['ms_config_shipping_delivery_times_note'] = 'Укажите набор сроков доставки, которые будут доступны для продавцов, при выборе при настройке доставки от нескольких продавцов, например, 24 ч, 3-5 дней и т.д..';

$_['ms_config_vendor_shipping_type'] = 'Тип доставки продавца';
$_['ms_config_vendor_shipping_combined'] = 'Комбинированная доставка';
$_['ms_config_vendor_shipping_per_product'] = 'Доставка каждого товара';
$_['ms_config_vendor_shipping_both'] = 'Оба способа';
$_['ms_config_vendor_shipping_type_note'] = 'Если выбрана опция \'Комбинированная доставка\', продавец сможет устанавливать только комбинированные правила доставки. Если выбрана опция \'Доставка каждого товара\', он может устанавливать только фиксированные правила доставки по каждому товару. Опция \'Обе\' позволяет устанавливать как правила \'Комбинированной\' доставки, так и правила \'Доставки каждого товара\'';

// MM > Settings > Orders
$_['ms_config_orders'] = "Заказы";
$_['ms_config_order_states'] = "Состояния <span class='ms-order-status-color'>заказов покупателя</span> OpenCart";
$_['ms_config_order_states_note'] = <<<EOT
Эта настройка привязывает <span class='ms-order-status-color'>статусы заказов</span> OpenCart к логическим состояниям заказа (например, неоплаченный, оплаченный, в просессе обработки, отправленный, отмененный) и контролирует то как MultiMerch обрабатывает <span class='ms-order-status-color'>заказы покупателей</span> OpenCart в зависимости от их статуса.<BR><BR>
Например, привязка статуса к состоянию 'В ожидании' позволит воспринимать заказы с этим статусом как неоплаченные (незавершенные), с целью недопущения отправки продавцом товара до того, как покупатель оплатил его.
EOT;
$_['ms_config_order_status_autocomplete'] = '(Автозаполнение)';
$_['ms_config_order_state_' . MsOrderData::STATE_PENDING] = 'В ожидании';
$_['ms_config_order_state_' . MsOrderData::STATE_PROCESSING] = 'В процессе';
$_['ms_config_order_state_' . MsOrderData::STATE_COMPLETED] = 'Завершено';
$_['ms_config_order_state_' . MsOrderData::STATE_FAILED] = 'Ошибка';
$_['ms_config_order_state_' . MsOrderData::STATE_CANCELLED] = 'Отменено';
$_['ms_config_order_state_note_' . MsOrderData::STATE_PENDING] = "Статус 'В ожидании' выделяет те заказы, которые были сделаны, но не оплачены. Это позволяет владельцу магазина (или продавцам) приостановить заказ до совершения платежа.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_PROCESSING] = "Статус 'В процессе' выделяет те заказы, которые были сделаны, но не были оплачены. Это позволяет владельцу магазина (или продавцам) приостановить заказ до совершения оплаты.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_COMPLETED] = "Статус 'Завершено' выделяет те заказы, которые были полностью оплачены. Это позволяет владельцу магазина (или продавцам) приступить к отправке товара.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_FAILED] = "Неисправное состояние ОС";
$_['ms_config_order_state_note_' . MsOrderData::STATE_CANCELLED] = "Отменено примечание о состоянии ОС";

$_['ms_config_suborder_states'] = "MultiMerch <span class='ms-suborder-status-color'>заказов продавцов</span> states";
$_['ms_config_suborder_states_note'] = <<<EOT
Этот настройка привязывает <span class='ms-suborder-status-color'>статусы заказов продавцов</span> MultiMerch к логическим состояниям заказа (например, неоплаченный, оплаченный, в просессе обработки, отправленный, отмененный) и контролирует то как MultiMerch обрабатывает <span class='ms-suborder-status-color'>заказы продавцов (подзаказы)</span> зависит от их статуса.<BR><BR>
Например, присвоение товару статуса "завершен" позволит продавцу начать обрабатывать заказ, а покупателю оставить отзыва как об отправленном товаре
EOT;
$_['ms_config_suborder_status_autocomplete'] = '(Автоматически завершен)';
$_['ms_config_suborder_state_' . MsSuborder::STATE_PENDING] = 'В ожидании';
$_['ms_config_suborder_state_' . MsSuborder::STATE_PROCESSING] = 'В процессе';
$_['ms_config_suborder_state_' . MsSuborder::STATE_COMPLETED] = 'Завершен';
$_['ms_config_suborder_state_' . MsSuborder::STATE_FAILED] = 'Ошибка';
$_['ms_config_suborder_state_' . MsSuborder::STATE_CANCELLED] = 'Отменен';
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_PENDING] = "Статус 'В ожидании' выделяет для продавца те товары, заказы на которые были сделаны, но не обработаны.";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_PROCESSING] = "Статус 'В процессе' выделяет те товары, которые в настоящее время находятся в обработке.";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_COMPLETED] = "Статус 'Завершено' указывает на то, что продавец завершил заказ (отправил).";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_FAILED] = "Примечание к неудачному состоянию MS";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_CANCELLED] = "Примечание к отмененному состоянию MS";

$_['ms_config_order_statuses'] = "Настройки статуса заказа";
$_['ms_config_suborder_statuses'] = "Статус заказа продавца";
$_['ms_config_suborder_status_default'] = "Статус заказа продавца по умолчанию";
$_['ms_config_suborder_status_default_note'] = "Это стандартный <span class='ms-suborder-status-color'>статус порядка продавцов,</span> которому все новые <span class='ms-suborder-status-color'>порядки продавцов</span> будут соответствовать, когда будет создан <span class='ms-order-status-color'>порядок покупателей</span>.";
$_['ms_config_order_status_credit'] = "Балансовые кредитные статусы";
$_['ms_config_order_status_credit_note'] = <<<EOT
Выберите <span class='ms-order-status-color'>статус заказа покупателя</span> OpenCart и/или <span class='ms-suborder-status-color'>статус заказа продаваца</span> MultiMerch, которые будут <span class='ms-credit-status-color'>кредитировать</span> баланс продавца доходом от проданных товаров при изменении статуса заказа.<BR><BR>
Добавление статусов OpenCart автоматически создаст транзакции для всех продавцов, которые являются частью одного и того же заказа покупателя. При выборе статуса MultiMerch транзакции для отдельных продавцов будут созданы при изменении соответствующего статуса подзаказа.
EOT;
$_['ms_config_order_status_debit'] = "Статус возврата баланса";
$_['ms_config_order_status_debit_note'] = <<<EOT
Выберите <span class='ms-order-status-color'>статус заказа покупателя</span> OpenCart и/или <span class='ms-suborder-status-color'>статус заказа продавца</span> MultiMerch, которые будут <span class='ms-refund-status-color'>возвращать сумму</span> соответствующей транзакции на баланс продавца при изменении статус заказа.<BR><BR>
Добавление статусов OpenCart будет автоматически возвращать транзакции от всех продавцов, которые являются частью одного и того же заказа покупателя. При выборе статуса MultiMerch возврат суммы транзакций для отдельных продавцов будут осуществлен при изменении соответствующего статуса подзаказа.
EOT;

// MM > Settings > Products
$_['ms_config_reviews'] = 'Отзывы о товаре';
$_['ms_config_reviews_enable'] = 'Включить отзывы';
$_['ms_config_reviews_enable_note'] = "Разрешить клиентам оставлять отзывы о приобретенных товарах. Включение этого параметра приведет к отключению системы отзывов OpenCart по умолчанию";

$_['ms_config_import'] = 'Массовый импорт товаров из CSV';
$_['ms_config_import_enable'] = 'Включить CSV импорт';
$_['ms_config_import_enable_note'] = 'Разрешить продавцам массово загружать товары из CSV файла';
$_['ms_config_import_category_type'] = 'Стиль ввода категории';
$_['ms_config_import_category_type_note'] = 'Этот параметр определяет, как MultiMerch принимает категории товаров в файлах CSV - либо все уровни категорий в одной ячейке через разделитель, либо уровень каждой категории в отдельных ячейках';
$_['ms_config_import_category_type_all_categories'] = 'Все уровни в одной ячейке (разделитель - |)';
$_['ms_config_import_category_type_categories_levels'] = 'Разные уровни в разных ячейках';
/***** INSERTED *****/$_['ms_config_feed_allow_external_source'] = "Allow external source";
/***** INSERTED *****/$_['ms_config_feed_allow_external_source_note'] = "Seller will be able to specify URL of an external source import file.";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field'] = "Primary product field";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_note'] = "Select the primary (unique) product field to be used when updating existing products during imports";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_name'] = "Name";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_model'] = "Model";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_sku'] = "SKU";
/***** INSERTED *****/$_['ms_config_feed_scheduled_import_enable'] = "Enable imports scheduling";
/***** INSERTED *****/$_['ms_config_feed_scheduled_import_enable_note'] = <<<EOT
To enable import scheduling, please add the following line to your crontab:<br>
* * * * * YOUR_PHP_INSTALLATION_PATH %ssystem/vendor/multimerchlib/Workers/Cron.php
EOT;

$_['ms_config_product_categories_type'] = 'Тип категорий товара';
$_['ms_config_product_categories_type_note'] = 'Какой тип категорий продавец разрешил использовать при перечислении его товаров';
$_['ms_config_product_category_store'] = 'Магазин';
$_['ms_config_product_category_seller'] = 'Продавец';
$_['ms_config_product_category_both'] = 'Оба';

// Sales > Order > Info > Shipping
$_['ms_sale_order_shipping_cost'] = 'Стоимость доставки';
$_['ms_sale_order_shipping_via'] = 'Доставка через %s';


$_['ms_config_status'] = 'Статус';
$_['ms_config_top'] = 'Верх страницы';
$_['ms_config_limit'] = 'Лимит:';
$_['ms_config_image'] = 'Изображение (Ш x В):';

$_['ms_config_enable_rte'] = 'Включить Rich Text Editor для описаний';
$_['ms_config_enable_rte_note'] = 'Включить редактор форматированного текста Summernote для полей описания товара и продавца.';

$_['ms_config_rte_whitelist'] = 'Разрешенный список тэгов';
$_['ms_config_rte_whitelist_note'] = 'Разрешенные рэги в редакторе (пусто = все тэги разрешены)';

$_['ms_config_image_sizes'] = 'Размеры изображений';
$_['ms_config_seller_avatar_image_size'] = 'Размер аватара';
$_['ms_config_seller_avatar_image_size_seller_profile'] = 'Профиль продавца';
$_['ms_config_seller_avatar_image_size_seller_list'] = 'Список продавцов';
$_['ms_config_seller_avatar_image_size_product_page'] = 'Страница товара';
$_['ms_config_seller_avatar_image_size_seller_dashboard'] = 'Панель продавца';
$_['ms_config_seller_banner_size'] = 'Размер баннера продавца';

$_['ms_config_image_preview_size'] = 'Размер изображения предпросмотра';
$_['ms_config_image_preview_size_seller_avatar'] = 'Аватар продавца';
$_['ms_config_image_preview_size_product_image'] = 'Изображение товара';

$_['ms_config_product_image_size'] = 'Размер изображения товара';
$_['ms_config_product_image_size_seller_profile'] = 'Профиль продавца';
$_['ms_config_product_image_size_seller_products_list'] = 'Товары в каталоге';
$_['ms_config_product_image_size_seller_products_list_account'] = 'Товары в учетной записи продавца';

$_['ms_config_description_image_size'] = 'Размер изображения описания';
$_['ms_config_description_image_size_note'] = 'Максимальный размер встроенного изображения описания на страницах товаров и продавцов (Ш x В)';

$_['ms_config_description_images'] = 'Встроенные изображения описания';
$_['ms_config_description_images_allow'] = 'Включить встроенные изображения описания';
$_['ms_config_description_images_allow_note'] = 'Разрешить продавцам использовать встроенные изображения описания в описаниях товаров и профилях';
$_['ms_config_description_images_type'] = 'Тип хранилища изображений (наследование)';
$_['ms_config_description_images_type_note'] = 'Определите метод хранения загруженных пользователями встроенных изображений';
$_['ms_config_description_images_type_upload'] = 'Серверная загрузка';
$_['ms_config_description_images_type_base64'] = 'База64';

$_['ms_config_uploaded_image_size'] = 'Ограничения на размер изображения';
$_['ms_config_uploaded_image_size_note'] = 'Ограничить загружаемые изображения (Ш x В, 0 = без ограничений).';
$_['ms_config_max'] = 'Макс.';
$_['ms_config_min'] = 'Мин.';

$_['ms_config_seo'] = 'SEO';
$_['ms_config_enable_seo_urls_seller'] = 'Генерировать SEO URL-адреса для новых продавцов';
$_['ms_config_enable_seller_generate_metatags'] = 'Создание метатегов для продавцов';
$_['ms_config_meta_for_seller_page'] = 'Страница продавца';
$_['ms_config_meta_for_seller_products_page'] = 'SСтраница товаров продавца';
$_['ms_config_meta_seller_title_template'] = 'Шаблон названия продавца';
$_['ms_config_meta_seller_h1_template'] = 'Шаблон h1 продавца';
$_['ms_config_meta_seller_description_template'] = 'Шаблон описания продавца';
$_['ms_config_meta_seller_keyword_template'] = 'Шаблон ключевых слов продавца';

$_['ms_config_enable_seo_urls_seller_note'] = 'Генерировать SEO URL-адреса для новых продавцов. Настройка SEO должна быть включена в OpenCart';
$_['ms_config_enable_seo_urls_product'] = 'Генерировать SEO URL-адреса для новых товаров (эксп.)';
$_['ms_config_enable_seo_urls_product_note'] = 'Генерировать SEO URL-адреса для новых товаров. Настройка SEO должна быть включена в OpenCart. Экспериментально, особенно для не-англоязычных магазинов.';
$_['ms_config_enable_non_alphanumeric_seo'] = 'Разрешить UTF8 в SEO URL-адресах (эксп.)';
$_['ms_config_enable_non_alphanumeric_seo_note'] = 'Включение этой настройки разрешает символы из набора UTF8 в SEO URL-адресах';
$_['ms_config_sellers_slug'] = 'Основные ключевые слова SEO URL продавца';
$_['ms_config_sellers_slug_'] = '/продавец-имя/';
$_['ms_config_sellers_slug_note'] = <<<EOT
Используйте этот параметр, чтобы указать ключевое слово URL, которое будет использоваться в качестве базового для профилей продавца в вашем магазине, <br />например, %ssellers/johndoe/store/<br>
ПРЕДУПРЕЖДЕНИЕ: Изменение этого параметра в уже рабочем магазине может сломать предыдущие SEO URL-адреса и повлиять на ваш рейтинг.
EOT;
$_['ms_config_products_slug'] = 'Основное ключевое слово SEO URL товара';
$_['ms_config_products_slug_note'] = <<<EOT
Используйте этот параметр, чтобы указать ключевое слово URL, которое будет использоваться в качестве основного для товаров в вашем магазине, <br />например, %sproducts/iphone-5s/. Оставьте это поле пустым, чтобы отключить ключевое слово базового URL.<br>
ПРЕДУПРЕЖДЕНИЕ: Изменение этого параметра в уже рабочем магазине может сломать предыдущие SEO URL-адреса и повлиять на ваш рейтинг.
EOT;

$_['ms_config_sellers_map'] = 'Карта продавцов';
$_['ms_config_sellers_map_api_key'] = 'Ключ API Карт Google';
$_['ms_config_sellers_map_api_key_note'] = 'Пожалуйста, укажите свой <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">Ключ API Карт Google</a> для отображения карты продавцов';

$_['ms_config_logging'] = 'Отлаживать';
$_['ms_config_logging_level'] = 'Уровень журнала отладки';
$_['ms_config_logging_level_note'] = 'Укажите уровень веденя журнала отладки (Ошибка - писать только ошибки, информация - писать некоторую информацию, Наладка - запись завершенных функций цепочек вызовов (Включить это только для коротких промежутков отладки))';
$_['ms_config_logging_level_error'] = 'Ошибка';
$_['ms_config_logging_level_debug'] = 'Отладка';
$_['ms_config_logging_level_info'] = 'Информация';
$_['ms_config_logging_filename'] = 'Имя файла журнала';
$_['ms_config_logging_filename_note'] = 'Укажите имя файла журнала отладки MultiMerch';

$_['ms_config_seller'] = 'Продавцы';

// Change Seller Group
$_['ms_config_change_group'] = 'Разрешить выбирать группу при регистрации';
$_['ms_config_change_group_note'] = 'Разрешить продавцу выбирать группу при регистрации';

// Change Seller Nickname
$_['ms_config_seller_change_nickname'] = 'Разрешить изменять никнейм';
$_['ms_config_seller_change_nickname_note'] = 'Разрешить продавцам изменять никнейм / название магазина';

// Seller Nickname Rules
$_['ms_config_nickname_rules'] = 'Ограничения никнейма';
$_['ms_config_nickname_rules_note'] = 'Разрешенные символы в никнеймах продавцов';
$_['ms_config_nickname_rules_alnum'] = 'Алфавитно-цифровые (латиница)';
$_['ms_config_nickname_rules_ext'] = 'Расширенная латиница';
$_['ms_config_nickname_rules_utf'] = 'Полный набор UTF-8';


$_['mxt_google_analytics'] = 'Google Analytics';
$_['mxt_google_analytics_enable'] = 'Включить Google Analytics';

$_['mxt_disqus_comments'] = 'Disqus комментарии';
$_['mxt_disqus_comments_enable'] = 'Включить Disqus комментарии';
$_['mxt_disqus_comments_shortname'] = 'Disqus id (shortname)';

$_['mmes_messaging'] = 'Личные сообщения';
$_['mmess_config_enable'] = 'Включить личные сообщения в Multimerch';
$_['ms_config_msg_allowed_file_types'] = 'Разрешенные расширения файлов';
$_['ms_config_msg_allowed_file_types_note'] = 'Разрешенные расширения файлов для загрузки в сообщениях';

$_['ms_config_coupon'] = 'Скидочные купоны';
$_['ms_config_coupon_allow'] = 'Разрешать продавцам создавать свои скидочные купоны';

//Marketplace Dashboard
$_['ms_dashboard_title'] = 'Панель управления';
$_['ms_dashboard_heading'] = 'Панель управления';

$_['ms_dashboard_total_sales'] = 'Всего продаж';
$_['ms_dashboard_total_orders'] = 'Всего заказов';
$_['ms_dashboard_total_customers'] = 'Всего покупателей';
$_['ms_dashboard_total_customers_online'] = 'Покупатели онлайн';
$_['ms_dashboard_total_sellers'] = 'Всего продавцов';
$_['ms_dashboard_total_sellers_balances'] = 'Сумма балансов продавцов';
$_['ms_dashboard_total_products'] = 'Всего товаров';
$_['ms_dashboard_total_products_views'] = 'Всего просмотров товаров';
$_['ms_dashboard_gross_sales'] = 'Суммарная выручка от продажи';

$_['ms_dashboard_sales_analytics'] = 'Аналитика продаж';
$_['ms_dashboard_top_products'] = 'Лидеры продаж';
$_['ms_dashboard_top_sellers'] = 'Лучшие продавцы';
$_['ms_dashboard_top_customers'] = 'Самые ценные клиенты';
$_['ms_dashboard_top_countries'] = 'Рейтинг стран';

$_['ms_dashboard_sales_analytics_no_results'] = "Пока нет заказов.";
$_['ms_dashboard_top_products_no_results'] = "Нет данных.";
$_['ms_dashboard_top_sellers_no_results'] = "Нет данных.";
$_['ms_dashboard_top_customers_no_results'] = "Нет данных.";
$_['ms_dashboard_top_countries_no_results'] = "Нет данных.";

$_['ms_dashboard_marketplace_activity'] = 'Активность торговой площадки';
$_['ms_dashboard_latest_orders'] = 'Последние заказы';

$_['ms_dashboard_marketplace_activity_no_results'] = 'Нет активности в магазине.';
$_['ms_dashboard_latest_orders_no_results'] = 'Еще нет заказов.';

//SEO
$_['ms_seo'] = 'SEO';
$_['ms_seo_urls'] = 'SEO URLs';
$_['ms_use_seo_urls'] = 'Используйте MultiMerch SEO URLs';
$_['ms_use_seo_urls_note'] = <<<EOT
Система MultiMerch SEO заменяет стандартные и сторонние контроллеры OpenCart для оптимизации поисковой системы MultiMerch из коробки.<br>
ПРЕДУПРЕЖДЕНИЕ: Включение этого в уже работающем магазине OpenCart с настраиваемыми структурами URL-адресов может привести к поломке существующих URL-адресов. Будьте осторожны!<br>
Если вы намерены использовать сторонние поисковые системы, вам может потребоваться отключить этот файл, а также файл multimerch_core_seo.xml вручную (НЕ РЕКОМЕНДУЕТСЯ).
EOT;

$_['ms_seo_url_tooltip'] = 'Не используйте пробелы, вместо них ставьте дефис - и следите за тем, чтобы глобальный SEO URL был уникальным.';

// Badges
$_['ms_menu_badge'] = 'Бейджи';
$_['ms_config_badge_title'] = 'Бейджи продавца';
$_['ms_config_badge_manage'] = 'Управление бейджами';
$_['ms_config_badge_enable_note'] = 'Включить функциональность бейджей продавца, позволяет торговым площадкам администрировать, создавать и назначать значки для продавцов';
$_['ms_config_badge_size'] = 'Размер бейджа';
$_['ms_catalog_badges_breadcrumbs'] = 'Бейджи';
$_['ms_catalog_badges_heading'] = 'Бейджи';
$_['ms_badges_column_id'] = 'ID';
$_['ms_badges_column_name'] = 'Название';
$_['ms_badges_image'] = 'Изображение';
$_['ms_badges_column_action'] = 'Действие';
$_['ms_catalog_insert_badge_heading'] = 'Создать бейдж';
$_['ms_catalog_edit_badge_heading'] = 'Редактировать бейдж';
$_['ms_success_badge_created'] = 'Значок создан';
$_['ms_success_badge_updated'] = 'Значок обновлён';
$_['ms_error_badge_name'] = 'Пожалуйста, укажите имя для бейджа';
$_['ms_error_badge_image'] = 'Пожалуйста, выберете изображение для бейджа';

// Social Links
$_['ms_menu_social_links'] = 'Социальные ссылки';
$_['ms_config_sl_title'] = 'Ссылка на социальные сети';
$_['ms_config_sl_enable_note'] = 'Включить функцию ссылки на социальные сети, которая позволяет продавцам показывать ссылки на их аккаунты в социальных сетях в их профилях';
$_['ms_sl_icon_size'] = 'Размер иконки';
$_['ms_sl'] = 'Социальные ссылки';
$_['ms_sl_manage'] = 'Управление каналами социальных сетей';
$_['ms_sl_create'] = 'Новый социальный канал';
$_['ms_sl_update'] = 'Обновить социальные каналы';
$_['ms_sl_column_id'] = '#';
$_['ms_sl_column_name'] = 'Название';
$_['ms_sl_image'] = 'Изображение';
$_['ms_sl_column_action'] = 'Действие';
$_['ms_success_channel_created'] = 'Социальный канал создан';
$_['ms_success_channel_updated'] = 'Социальный канал обновлён';
$_['ms_error_channel_deleting'] = 'Ошибка удаления социального канала!';
$_['ms_success_channel_deleting'] = 'Успех: социальный канал успешно удален!';
$_['ms_error_channel_name'] = 'Пожалуйста, укажите имя для социального канала.';

// Information pages
$_['ms_menu_information'] = 'Страницы';
$_['ms_information_heading'] = 'Страницы';
$_['ms_information_title'] = 'Заголовок страницы';
$_['ms_information_content'] = 'Контент';
$_['ms_information_add'] = 'Создать новую страницу';
$_['ms_information_edit'] = 'Редактировать страницу';
$_['ms_information_success'] = 'Получилось! Страница изменена!';
$_['ms_information_success_deleted'] = 'Вы удалили страницу!';
$_['ms_information_error_title'] = 'Выберите название страницы от 3-х до 64-х символов!!';
$_['ms_information_error_content'] = 'Должно быть длиннее 3-х символов';
$_['ms_information_error_meta_title'] = 'Мета-название должно быть длиннее 3-х, но короче 255 символов!';
$_['ms_information_error_account'] = 'Внимание: Эту страницу удалить нельзя, потому что сейчас она является страницей, которая содержит Правила аккаунта магазина!';
$_['ms_information_error_checkout'] = 'Внимание: Эту страницу удалить нельзя, потому что сейчас она является страницей, которая содержит Правила оформления заказа!';
$_['ms_information_error_affiliate'] = 'Внимание: Эту страницу удалить нельзя, потому что сейчас она является страницей, которая содержит Правила партнерского соглашения!';
$_['ms_information_error_return']  = 'Внимание: Эту страницу удалить нельзя, потому что сейчас она является страницей, которая содержит Правила возврата!';
$_['ms_information_error_store'] = 'Внимание: Эту страницу удалить нельзя, потому что сейчас она является страницей, которая используется %s магазинами!';

// Seller - List
$_['ms_catalog_sellers_heading'] = 'Продавцы';
$_['ms_catalog_sellers_breadcrumbs'] = 'Продавцы';
$_['ms_catalog_sellers_newseller'] = 'Новый продавец';
$_['ms_catalog_sellers_create'] = 'Создать продавца';
$_['ms_catalog_sellers_view_profile'] = 'Посмотреть профиль продавца';

$_['ms_catalog_sellers_total_balance'] = 'Общая сумма на всех балансах: <b>%s</b> (активные продавцы: <b>%s</b>)';
$_['ms_catalog_sellers_email'] = 'Эл. почта';
$_['ms_catalog_sellers_total_products'] = 'Товары';
$_['ms_catalog_sellers_total_sales'] = 'Продажи';
$_['ms_catalog_sellers_current_balance'] = 'Баланс';
$_['ms_catalog_sellers_status'] = 'Статус';
$_['ms_catalog_sellers_date_created'] = 'Дата создания';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'Активен';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'Неактивен';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'Выкл.';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'Незавершённый';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'Удалён';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'Неоплачен регистрационный сбор';

// Customer-seller form
$_['ms_catalog_sellerinfo_heading'] = 'Продавец';
$_['ms_catalog_sellerinfo_create_heading'] = "Создать нового продавца";
$_['ms_catalog_sellerinfo_update_heading'] = "Изменить продавца";
$_['ms_catalog_sellerinfo_seller_data'] = 'Данные продавца';

$_['ms_catalog_sellerinfo_customer'] = 'Покупатель';
$_['ms_catalog_sellerinfo_customer_data'] = 'Данные покупателя';
$_['ms_catalog_sellerinfo_customer_new'] = 'Новый покупатель';
$_['ms_catalog_sellerinfo_customer_existing'] = 'Существующий покупатель';
$_['ms_catalog_sellerinfo_customer_create_new'] = 'Создать нового покупателя';
$_['ms_catalog_sellerinfo_customer_firstname'] = 'Имя';
$_['ms_catalog_sellerinfo_customer_lastname'] = 'Фамилия';
$_['ms_catalog_sellerinfo_customer_email'] = 'Эл. почта';
$_['ms_catalog_sellerinfo_customer_password'] = 'Пароль';
$_['ms_catalog_sellerinfo_customer_password_confirm'] = 'Подтверждение пароля';

$_['ms_catalog_sellerinfo_nickname'] = 'Никнейм';
$_['ms_catalog_sellerinfo_keyword'] = 'SEO слэг';
$_['ms_catalog_sellerinfo_slogan'] = 'Слоган';
$_['ms_catalog_sellerinfo_description'] = 'Описание';
$_['ms_catalog_sellerinfo_zone'] = 'Регион';
$_['ms_catalog_sellerinfo_zone_select'] = 'Выберите регион';
$_['ms_catalog_sellerinfo_zone_not_selected'] = 'Не выбран регион';
$_['ms_catalog_sellerinfo_sellergroup'] = 'Группа продавца';

$_['ms_catalog_sellerinfo_avatar'] = 'Аватар';
$_['ms_catalog_sellerinfo_message'] = 'Сообщение';
$_['ms_catalog_sellerinfo_message_note'] = 'Это сообщение будет добавлено к стандартному тексту письма';
$_['ms_catalog_sellerinfo_notify'] = 'Сообщить продавцу о текущем изменении данных';
$_['ms_catalog_sellerinfo_notify_note'] = 'Отметьте это поле для того, чтобы отправлять email продавцу когда его аккаунт был отредактирован.';
$_['ms_catalog_sellerinfo_product_validation'] = 'Валидация товаров';
$_['ms_catalog_sellerinfo_product_validation_note'] = 'Валидация товаров для продавца seller';

$_['ms_error_sellerinfo_nickname_empty'] = 'Никнейм не можете быть пустым';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'Никнейм может содержать только алфавитно-цифровые символы (латиница)';
$_['ms_error_sellerinfo_nickname_utf8'] = 'Никнейм может содержать только печатные символы из набора UTF-8';
$_['ms_error_sellerinfo_nickname_latin'] = 'Никнейм может содержать только алфавитно-цифровые символы и диакритику (латиница)';
$_['ms_error_sellerinfo_nickname_length'] = 'Никнейм должен содержать от 4 до 50 символов';
$_['ms_error_sellerinfo_nickname_taken'] = 'Такой никнейм уже существует';
$_['ms_error_form_submit_error'] = 'Ошибка при отправке формы.';

// Catalog - Products
$_['ms_catalog_products_heading'] = 'Товары';
$_['ms_catalog_products_breadcrumbs'] = 'Товары';
$_['ms_catalog_products_notify_sellers'] = 'Сообщить продавцам';
$_['ms_catalog_products_bulk'] = '--Изменить статус--';
$_['ms_catalog_products_bulk_seller'] = '--Изменить продавца--';
$_['ms_catalog_products_noseller'] = '--Нет продавца--';
$_['ms_catalog_products_error_deleting'] = 'Ошибка удаления товара(ов)!';
$_['ms_catalog_products_success_deleting'] = 'Успех: товар успешно удален!';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'Активен';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'Неактивен';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'Выкл.';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'Удалён';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'Неоплачен листинговый сбор';
$_['ms_product_status_' . MsProduct::STATUS_IMPORTED] = 'Импортировано';

$_['ms_catalog_products_field_price'] = 'Цена';
$_['ms_catalog_products_field_quantity'] = 'Количество';
$_['ms_catalog_products_field_marketplace_category'] = 'Категория магазина';
$_['ms_catalog_products_field_tags'] = 'Тэги';
$_['ms_catalog_products_field_attributes'] = 'Атрибуты';
$_['ms_catalog_products_field_options'] = 'Опции';
$_['ms_catalog_products_field_special_prices'] = 'Специальные цены';
$_['ms_catalog_products_field_quantity_discounts'] = 'Скидки за количество';
$_['ms_catalog_products_field_images'] = 'Изображения';
$_['ms_catalog_products_field_files'] = 'Файлы';
$_['ms_catalog_products_field_meta_keyword'] 	 = 'Мета ключевые слова';
$_['ms_catalog_products_field_meta_description'] = 'Мета описание';
$_['ms_catalog_products_field_meta_title'] = 'Meta Title';
$_['ms_catalog_products_field_seo_url'] = 'SEO Keyword';
$_['ms_catalog_products_field_model']            = 'Model';
$_['ms_catalog_products_field_sku']              = 'SKU';
$_['ms_catalog_products_field_upc']              = 'UPC';
$_['ms_catalog_products_field_ean']              = 'EAN';
$_['ms_catalog_products_field_jan']              = 'JAN';
$_['ms_catalog_products_field_isbn']             = 'ISBN';
$_['ms_catalog_products_field_mpn']              = 'MPN';
$_['ms_catalog_products_field_manufacturer']     = 'Производитель';
$_['ms_catalog_products_field_date_available']   = 'Дата доступности';
$_['ms_catalog_products_field_stock_status']     = 'Статус "Нет в наличии"';
$_['ms_catalog_products_field_tax_class']        = 'Класс налога';
$_['ms_catalog_products_field_subtract']         = 'Уменьшать запас';
$_['ms_catalog_products_field_stores']         = 'Магазины';
$_['ms_catalog_products_filters']         = 'Фильтры';
$_['ms_catalog_products_min_order_qty']         = 'Минимальное количество заказа';
$_['ms_catalog_products_related_products']         = 'Похожие товары';
$_['ms_catalog_products_dimensions']            = 'Размеры';
$_['ms_catalog_products_weight']            = 'Вес';

// Catalog - Products - Custom fields
$_['ms_catalog_products_tab_custom_field'] = '[MM] Произвольные поля';
$_['ms_catalog_products_text_placeholder'] = 'Введите текст...';
$_['ms_catalog_products_textarea_placeholder'] = 'Введите текст...';
$_['ms_catalog_products_date_placeholder'] = 'Выберите дату...';
$_['ms_catalog_products_button_upload'] = 'Загрузить';
$_['ms_catalog_products_success_file_uploaded'] = 'Файл успешно загружен!';
$_['ms_catalog_products_success_upload_removed'] = 'Файл успешно удален!';
$_['ms_catalog_products_error_field_required'] = 'Поле, обязательное для заполнения!';
$_['ms_catalog_products_error_field_validation'] = 'Не удалось выполнить проверку поля! Шаблон: %s';

// Catalog - Imports
$_['ms_catalog_imports_heading'] = 'Импорт';
$_['ms_catalog_imports_breadcrumbs'] = 'Импорт';

$_['ms_catalog_imports_field_name'] = 'Имя';
$_['ms_catalog_imports_field_seller'] = 'Продавец';
$_['ms_catalog_imports_field_date'] = 'Дата';
$_['ms_catalog_imports_field_type'] = 'Тип';
$_['ms_catalog_imports_field_processed'] = 'Обработано';
$_['ms_catalog_imports_field_added'] = 'Добавлено';
$_['ms_catalog_imports_field_updated'] = 'Обновлено';
$_['ms_catalog_imports_field_errors'] = 'Ошибки';
$_['ms_catalog_imports_field_actions'] = 'Действия';

/***** INSERTED *****/$_['ms_feed_import_list_single'] = "Import history";
/***** INSERTED *****/$_['ms_feed_import_list_scheduled'] = "Scheduled imports";
/***** INSERTED *****/$_['ms_feed_import_list_url_path'] = "Feed URL";
/***** INSERTED *****/$_['ms_feed_import_list_date_last_run'] = "Date last run";
/***** INSERTED *****/$_['ms_feed_import_list_date_next_run'] = "Date next run";
/***** INSERTED *****/$_['ms_feed_import_list_success_deleted'] = "Scheduled import is successfully deleted!";

// Catalog - Seller Groups
$_['ms_catalog_seller_groups_heading'] = 'Группы продавцов';
$_['ms_catalog_seller_groups_breadcrumbs'] = 'Группы продавцов';

$_['ms_seller_groups_column_id'] = 'ID';
$_['ms_seller_groups_column_name'] = 'Название';
$_['ms_seller_groups_column_action'] = 'Действие';

$_['ms_catalog_insert_seller_group_heading'] = 'Создать группу продавцов';
$_['ms_catalog_edit_seller_group_heading'] = 'Изменить группу продавцов';

$_['ms_product_period'] = 'Срок публикации товара (0 = без ограничений)';
$_['ms_product_quantity'] = 'Количество товара (0 = без ограничений)';

$_['ms_seller_group_stripe_subscription_heading'] = "Подписки Stripe";
$_['ms_seller_group_stripe_subscription_enabled'] = "Использовать подписки Stripe";
$_['ms_seller_group_stripe_subscription_info'] = "Подписки обрабатываются через Stripe. Изменение правил подписки не влияет на текущих подписчиков.";
$_['ms_seller_group_stripe_subscription_plan_interval_day'] = "в день";
$_['ms_seller_group_stripe_subscription_plan_interval_month'] = "в месяц";
$_['ms_seller_group_stripe_subscription_plan_interval_year'] = "в год";
$_['ms_seller_group_stripe_subscription_plan_base'] = "Основной план";
$_['ms_seller_group_stripe_subscription_plan_base_info'] = "%s %s";
$_['ms_seller_group_stripe_subscription_plan_per_seat'] = "Потоварный план";
$_['ms_seller_group_stripe_subscription_plan_per_seat_info'] = "%s за товар %s";

$_['ms_error_seller_group_name'] = 'Ошибка: Название должно содержать от 3 до 32 символов';
$_['ms_error_seller_group_default'] = 'Ошибка: Невозможно удалить группу продавцов по умолчанию';
$_['ms_success_seller_group_created'] = 'Группа продавцов создана';
$_['ms_success_seller_group_updated'] = 'Группа продавцов изменена';
$_['ms_error_seller_group_deleting'] = 'Ошибка удаления группы продавцов!';
$_['ms_success_seller_group_deleting'] = 'Успех: Группа продавцов успешно удалена!';

// Payments
$_['ms_payment_heading'] = 'Платежи';
$_['ms_payment_breadcrumbs'] = 'Платежи';
$_['ms_payment_payout_requests'] = 'Запросы на выплату';
$_['ms_payment_payouts'] = 'Ручные выплаты';
$_['ms_payment_pending'] = 'В процессе';
$_['ms_payment_new'] = 'Новый платеж';
$_['ms_payment_paid'] = 'Оплачено';
$_['ms_payment_no_methods'] = 'Нет доступных способов оплаты!';
$_['ms_payment_multiple_invoices_no_methods'] = 'Множественные выплаты доступны только через Paypal MassPay! Пожалуйста, проверьте Paypal активирован и включен в <a href="%s">способах оплаты</a>.';

$_['ms_success_payment_created'] = 'Платеж создан';

// Shipping methods
$_['ms_shipping_method_heading'] = 'Способы доставки';
$_['ms_shipping_method_breadcrumbs'] = 'Способы доставки';
$_['ms_shipping_method_status_' . MsShippingMethod::STATUS_ENABLED] = 'Вкл.';
$_['ms_shipping_method_status_' . MsShippingMethod::STATUS_DISABLED] = 'Выкл.';
$_['ms_shipping_method_add_heading'] = 'Добавить способ доставки';
$_['ms_shipping_method_add_success'] = 'Вы успешно добавили новый способ доставки!';
$_['ms_shipping_method_edit_heading'] = 'Редактировать способ доставки';
$_['ms_shipping_method_edit_success'] = 'Вы успешно отредактировали способ доставки!';
$_['ms_shipping_method_delete_success'] = 'Вы удалили способ доставки';
$_['ms_shipping_method_delete_error'] = 'Ошибка при удалении способа доставки!';
$_['ms_shipping_method_name_error'] = 'Ошибка: имя должно быть от 3 до 32 символов';

//Suborders statuses
$_['ms_menu_suborders_statuses'] = 'Статусы заказов';
$_['ms_suborder_status_heading'] = 'Статусы заказов';
$_['ms_suborder_status_tab_oc_status'] = 'Статусы заказов магазина';
$_['ms_suborder_status_tab_ms_status'] = 'Статусы заказов продавцов';
$_['ms_suborder_status_name'] = 'Имя';
$_['ms_suborder_status_action'] = 'Действие';
$_['ms_suborder_status_add_heading'] = 'Добавить статус заказа продавца';
$_['ms_suborder_status_add_success'] = 'Вы успешно добавили новый статус заказа продавца!';
$_['ms_suborder_status_edit_heading'] = 'Изменить статус заказа продавца';
$_['ms_suborder_status_edit_success'] = 'Вы успешно изменили статус заказа продавца!';
$_['ms_suborder_status_breadcrumbs'] = 'Статусы заказов';
$_['ms_suborder_status_delete_success'] = 'Вы удалили статус заказа продавца!';
$_['ms_suborder_status_oc_delete_success'] = 'Вы удалили статус заказа!';
$_['ms_suborder_status_name_error'] = 'Ошибка: имя должно быть от 3 до 32 символов';
$_['ms_suborder_status_info_disabled_delete'] = "Этот статус связан с одним из статусов заказа MultiMerch или принадлежит одному или нескольким существующим заказам, и поэтому не может быть удален.";

// Events
$_['ms_event_heading'] = 'Активность торговой площадки';
$_['ms_event_breadcrumbs'] = 'Активность торговой площадки';
$_['ms_event_column_event'] = 'Событие';
$_['ms_event_column_description'] = 'Описание';
$_['ms_event_product'] = 'Товар';
$_['ms_event_seller'] = 'Продавец';
$_['ms_event_customer'] = 'Клиент';
$_['ms_event_order'] = 'Заказ';

$_['ms_event_user_deleted'] = '*Пользователь удален*';

// Product events
$_['ms_event_type_' . \MultiMerch\Event\Event::PRODUCT_CREATED] = 'Товар создан';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::PRODUCT_CREATED] = 'Новый товар <a href="%s" target="_blank" class="product">%s</a> был создан продавцом <a href="%s" target="_blank">%s</a>.';
$_['ms_event_type_' . \MultiMerch\Event\Event::PRODUCT_MODIFIED] = 'Товар изменен';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::PRODUCT_MODIFIED] = 'Товар <a href="%s" target="_blank" class="product">%s</a> был изменен <a href="%s" target="_blank">%s</a>.';

// Seller events
$_['ms_event_type_' . \MultiMerch\Event\Event::SELLER_CREATED] = 'Продавец создан';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::SELLER_CREATED] = 'Новый продавец <a href="%s" target="_blank" class="seller">%s</a> зарегистрирован!';
$_['ms_event_type_' . \MultiMerch\Event\Event::SELLER_MODIFIED] = 'Продавец изменен';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::SELLER_MODIFIED] = 'Продавец <a href="%s" target="_blank" class="seller">%s</a> изменил свой профиль продавца.';

// Customer events
$_['ms_event_type_' . \MultiMerch\Event\Event::CUSTOMER_CREATED] = 'События покупателя';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::CUSTOMER_CREATED] = 'Новый покупатель <a href="%s" target="_blank" class="customer">%s</a> зарегистрирован!';
$_['ms_event_type_' . \MultiMerch\Event\Event::CUSTOMER_MODIFIED] = 'Покупатель изменен';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::CUSTOMER_MODIFIED] = 'Покупатель <a href="%s" target="_blank" class="customer">%s</a> обновил свою информацию.';

// Order events
$_['ms_event_type_' . \MultiMerch\Event\Event::ORDER_CREATED] = 'Заказ создан';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::ORDER_CREATED] = 'Новый заказ <a href="%s" target="_blank" class="order">#%s</a> был оформлен покупателем <a href="%s" target="_blank">%s</a>.';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::ORDER_CREATED . '_guest'] = 'Новый заказ <a href="%s" target="_blank" class="order">#%s</a> был оформлен Гостем.';

// Notifications
$_['ms_notification_heading'] = "Оповещения";
$_['ms_notification_breadcrumbs'] = "Оповещения";
$_['ms_notification_message'] = "Сообщение";

// Debug
$_['ms_debug_heading'] = 'Отладка';
$_['ms_debug_breadcrumbs'] = 'Отладка';
$_['ms_debug_info'] = 'Отладочная информация MultiMerch';
$_['ms_debug_sub_heading_title'] = 'Системная информация';
$_['ms_debug_multimerchinfo_heading_title'] = 'Журнал отладки';
$_['ms_debug_phpinfo_heading_title'] = 'PHP информация';
$_['ms_debug_warning_vqmod_not_installed'] = 'VQMod не установлено!';
$_['ms_debug_warning_server_log_not_available'] = 'Журнал сервера недоступен';
$_['ms_debug_warning_hash_file_invalid'] = 'Хэш-файл имеет плохую структуру';
$_['ms_debug_warning_hash_file_not_find'] = 'Хэш-файл не найден';


// Finances - Transactions
$_['ms_transactions_heading'] = 'Транзакции';
$_['ms_transactions_breadcrumbs'] = 'Транзакции';
$_['ms_transactions_new'] = 'Новая транзакция';

$_['ms_error_transaction_fromto'] = 'Пожалуйста выберите источник или назначение';
$_['ms_error_transaction_fromto_same'] = 'Источник и назначение не могут быть одинаковымиsame';
$_['ms_error_transaction_amount'] = 'Пожалуйста введите положительную сумму';
$_['ms_success_transaction_created'] = 'Транзакция создана';

$_['button_cancel'] = 'Отменить';
$_['button_save'] = 'Сохранить';
$_['ms_action'] = 'Действие';


// Account - Conversations and Messages
$_['ms_account_conversations'] = 'Диалоги';
$_['ms_account_messages'] = 'Сообщения';
$_['ms_sellercontact_success'] = 'Ваше сообщение было успешно отправлено';

$_['ms_account_conversations_heading'] = 'Ваши Диалоги';
$_['ms_account_conversations_breadcrumbs'] = 'Ваши Диалоги';

$_['ms_account_conversations_status'] = 'Статус';
$_['ms_account_conversations_from'] = 'Диалог от';
$_['ms_account_conversations_from_admin_prefix'] = ' (администратор)';
$_['ms_account_conversations_to'] = 'Диалог с';
$_['ms_account_conversations_title'] = 'Заголовок';
$_['ms_account_conversations_type'] = 'Тип диалога';
$_['ms_account_conversations_date_added'] = 'Дату добавлено';
$_['ms_account_conversations_with_seller'] = 'Разговор с продавцом';

$_['ms_account_conversations_error_deleting'] = 'Ошибка удаления диалога!';
$_['ms_account_conversations_success_deleting'] = 'Успех: диалог успешно удален!';

$_['ms_account_conversations_sender_type_seller'] = 'продавец';
$_['ms_account_conversations_sender_type_buyer'] = 'покупатель';
$_['ms_account_conversations_sender_type_admin'] = 'админ';

$_['ms_conversation_title_product'] = 'Запрос о товаре: %s';
$_['ms_conversation_title_order'] = 'Запрос о заказе: %s';
$_['ms_conversation_title'] = 'Запрос от %s';

$_['ms_account_conversations_read'] = 'Прочитанный';
$_['ms_account_conversations_unread'] = 'Непрочитанный';

$_['ms_account_messages_heading'] = 'Сообщения';
$_['ms_last_message'] = 'Последнее сообщение';
$_['ms_message_text'] = 'Ваше сообщение';
$_['ms_post_message'] = 'Отправить сообщение';

$_['ms_customer_does_not_exist'] = 'Аккаунт клиента удален';
$_['ms_error_empty_message'] = 'Сообщение не может быть оставлено пустым';

$_['ms_account_conversations_textarea_placeholder'] = 'Введите ваше сообщение...';
$_['ms_account_conversations_upload'] = 'Загрузить файл';
$_['ms_account_conversations_file_uploaded'] = 'Ваш файл был успешно загружен!';
$_['ms_error_file_extension'] = 'Недопустимое расширение';

$_['ms_mail_subject_private_message'] = 'Получено новое личное сообщение';
$_['ms_mail_private_message'] = <<<EOT
Вы получили новое личное сообщение от %s!

%s

%s

Вы можете ответить в области обмена сообщениями в вашем аккаунте.
EOT;

$_['ms_account_message'] = 'Сообщение';
$_['ms_account_message_sender'] = 'Отправитель';
$_['ms_account_message_attachments'] = 'Вложения';

// Attributes
$_['ms_attribute_heading'] = 'Атрибуты';
$_['ms_attribute_breadcrumbs'] = 'Атрибуты';
$_['ms_attribute_create'] = 'Создать атрибут';
$_['ms_attribute_edit'] = 'Изменить атрибут';
$_['ms_attribute_value'] = 'Значение атрибутов';
$_['ms_error_attribute_name'] = 'Название атрибута должно содержать от 1 до 128 символов';
$_['ms_error_attribute_type'] = 'Для этого типа атрибута необходимы значения';
$_['ms_error_attribute_value_name'] = 'Название значения атрибута должно содержать от 1 до 128 символов';
$_['ms_success_attribute_created'] = 'Атрибут создан';
$_['ms_success_attribute_updated'] = 'Атрибут изменен';

$_['button_cancel'] = 'Отменить';
$_['button_save'] = 'Сохранить';
$_['ms_action'] = 'Действие';

// Mails
$_['ms_mail_greeting'] = "Здравствуйте %s,\n";
$_['ms_mail_greeting_no_name'] = "Здравствуйте,\n";
$_['ms_mail_ending'] = "\nЗдравствуйте,\n%s";
$_['ms_mail_message'] = "\nCообщение::\n%s";

$_['ms_mail_subject_seller_account_modified'] = 'Учетная запись продавца изменена';
$_['ms_mail_seller_account_modified'] = <<<EOT
Ваша учетная запись продавца в %s была изменена администратором.

Статус учетной записи: %s
EOT;

$_['ms_mail_subject_product_modified'] = 'Товар изменен';
$_['ms_mail_product_modified'] = <<<EOT
Ваш товар %s в магазине %s был изменен администратором.

Статус товара: %s
EOT;

$_['ms_mail_subject_product_purchased'] = 'Новый заказ';
$_['ms_mail_product_purchased'] = <<<EOT
Поступил новый заказ на ваши товары в магазине %s.

Покупатель: %s (%s)

Товары:
%s
Сумма: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
Поступил новый заказ на ваши товары в магазине %s.

Покупатель: %s

Товары:
%s
Сумма: %s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
Адрес доставки:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'Комментарий: %s';

$_['ms_mail_subject_product_reviewed'] = 'Новый отзыв о товаре';
$_['ms_mail_product_reviewed'] = <<<EOT
Оставлен новый отзыв о товаре %s.
Пройдите по следующей ссылке, чтобы посмотреть его: <a href="%s">%s</a>
EOT;

// Catalog - Mail
// Attributes
$_['ms_mail_subject_attribute_status_changed'] = 'Ваш статус атрибута товара обновленный';
$_['ms_mail_attribute_status_changed'] = <<<EOT
Статус атрибута вашего товара <strong>%s</strong> был обновлен: <strong>%s</strong>.
EOT;

$_['ms_mail_subject_attribute_seller_changed'] = 'Владелец атрибута изменен';
$_['ms_mail_attribute_seller_attached'] = <<<EOT
Атрибут %s был установлен на ваш аккаунт.
EOT;
$_['ms_mail_attribute_seller_detached'] = <<<EOT
Атрибут %s был снят от вашего аккаунта.
EOT;

$_['ms_mail_subject_attribute_converted_to_global'] = 'Атрибут изменен';
$_['ms_mail_attribute_converted_to_global'] = <<<EOT
Ваш атрибут "%s" был преобразован в глобальный.
EOT;

// Attribute groups
$_['ms_mail_subject_attribute_group_status_changed'] = 'Ваш статус группы атрибутов обновленный';
$_['ms_mail_attribute_group_status_changed'] = <<<EOT
Статус вашей группы атрибутов <strong>%s</strong> был обновлен: <strong>%s</strong>.
EOT;

// Options
$_['ms_mail_subject_option_status_changed'] = 'Статус ваших опций обновлено';
$_['ms_mail_option_status_changed'] = <<<EOT
Статус ваших опций <strong>%s</strong> был обновлен: <strong>%s</strong>.
EOT;

$_['ms_mail_subject_option_seller_changed'] = 'Владелец опции изменен';
$_['ms_mail_option_seller_attached'] = <<<EOT
Опция %s была назначена на ваш аккаунт.
EOT;
$_['ms_mail_option_seller_detached'] = <<<EOT
Опция %s была снята от вашего аккаунта.
EOT;

$_['ms_mail_subject_option_converted_to_global'] = 'Опция исправлена';
$_['ms_mail_option_converted_to_global'] = <<<EOT
Ваша опция "%s" была преобразована в глобальную.
EOT;

// Categories
$_['ms_mail_subject_category_status_changed'] = 'Статус вашей категории обновлен';
$_['ms_mail_category_status_changed'] = <<<EOT
Статус вашей категории <strong>%s</strong> был обновлен: <strong>%s</strong>.
EOT;

// Sales - Mail
$_['ms_transaction_order_created'] = 'Заказ создан';
$_['ms_transaction_order'] = 'Продажа: ID Заказа #%s';
$_['ms_transaction_suborder'] = 'Сбор с продажи для продавца %s по заказу #%s';
$_['ms_transaction_sale'] = 'Продажа: %s (-%s сбор)';
$_['ms_transaction_sale_no_commission'] = 'Продажа: %s';
$_['ms_transaction_sale_fee'] = 'Сбор с продажи: %s';
$_['ms_transaction_sale_fee_order'] = 'Сбор с продажи: Заказ #%s';
$_['ms_transaction_sale_fee_order_refund'] = 'Вернуть сбор с продажи: Заказ #%s';
$_['ms_transaction_refund'] = 'Возврат: %s';
$_['ms_transaction_shipping'] = 'Доставка: %s';
$_['ms_transaction_shipping_order'] = 'Доставка: Заказ #%s';
$_['ms_transaction_shipping_refund'] = 'Возврат стоимости доставки: %s';
$_['ms_transaction_shipping_order_refund'] = 'Возврат стоимости доставки: Заказ #%s';
$_['ms_transaction_coupon'] = 'Скидка по купону торговой площадки';
$_['ms_transaction_coupon_refund'] = 'Возврат средств по купону торговой площадки';
$_['ms_transaction_ms_coupon'] = 'Скидка по купону';
$_['ms_transaction_ms_coupon_refund'] = 'Возврат средств по купону';

$_['ms_payment_method'] = 'Способ оплаты';
$_['ms_payment_method_balance'] = "Вычесть из баланса продавца";
$_['ms_payment_royalty_payout'] = 'Выплата заработка %s в %s';
$_['ms_payment_completed'] = 'Платеж выполнен';
$_['ms_payment_status'] = 'Статус оплаты';

// Payment methods
$_['ms_pg_manage'] = 'Управление способами оплаты';
$_['ms_pg_heading'] = 'Способы оплаты';
$_['ms_pg_install'] = 'Успех: Вы установили способ оплаты %s!';
$_['ms_pg_uninstall'] = 'Успех: Вы удалили способ оплаты %s!';
$_['ms_pg_modify'] = 'Успех: Вы изменили способ оплаты %s!';
$_['ms_pg_modify_error'] = 'Внимание: У вас нет разрешения на изменение способов оплаты!';
$_['ms_pg_for_fee'] = 'Включить для комиссий:';
$_['ms_pg_for_payout'] = 'Включить для выплаты продавцам:';
$_['ms_pg_uninstall_warning'] = 'Внимание!\nВсе настройки способа оплаты всех продавцов будут удалены.\n\nВы уверены, что хотите продолжить?';
$_['ms_pg_fee_payment_method_name'] = 'Создать счет на оплату';

// Payments
$_['ms_pg_payment_number'] = 'Платеж #';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_PAID_REQUESTS] = 'Оплаченные счета';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_SALE] = 'Продажи';

$_['ms_pg_payment_status_' . MsPgPayment::STATUS_INCOMPLETE] = '<p style="color: red">Ожидается</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_INCOMPLETE] = 'Ожидается';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_COMPLETE] = '<p style="color: green">Завершен</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_COMPLETE] = 'Завершен';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = '<p style="color: blue">Ожидает подтверждения</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = 'Ожидает подтверждения';

$_['ms_pg_payment_error_no_method'] = 'Ошибка: выберите способ оплаты!';
$_['ms_pg_payment_error_no_methods'] = 'Вы должны выбрать хотя бы один платеж!';
$_['ms_pg_payment_error_no_requests'] = 'Ошибка: Вы должны выбрать счет!';
$_['ms_pg_payment_error_payment'] = 'Ошибка: Невозможно создать платеж!';
$_['ms_pg_payment_error_sender_data'] = 'Ошибка: Администратор не указал необходимую информацию';
$_['ms_pg_payment_error_receiver_data'] = 'Ошибка: Один или больше продавцов не указали необходимую информацию!';

// Payouts
$_['ms_payout_heading'] = 'Выплаты';
$_['ms_payout_payout'] = 'Выплаты';
$_['ms_payout_all_payouts'] = 'Прошлые выплаты';
$_['ms_payout_invoice'] = 'Счета';

$_['ms_payout_seller_list_info'] = 'Продавцы с нулевым или отрицательным балансом не могут быть оплачены и поэтому не отображаются.';
$_['ms_payout_seller_list_generate'] = 'Начните новую выплату продавцам';
$_['ms_payout_seller_list_refresh'] = 'Обновление';
$_['ms_payout_seller_list_pending'] = 'В ожидании';
$_['ms_payout_seller_list_payout_name'] = 'Название платежа';

$_['ms_payout_view_heading'] = 'Счета в платеже #%s';

$_['ms_payout_confirm'] = 'Подтвердить выплату';
$_['ms_payout_selected_sellers'] = 'Выбранные продавцы';
$_['ms_payout_date_payout_period'] = 'Период выплаты';
$_['ms_payout_date_payout_period_until'] = 'До %s';
$_['ms_payout_error_no_sellers'] = 'Ошибка: не выбраны продавцы!';
$_['ms_payout_success_payout_created'] = 'Успех: Выплата #%s была успешно завершена!';

// Validation messages
$_['ms_validate_default'] = "Поле '%s' заполено неверно";
$_['ms_validate_required'] = "Поле '%s' обязательно к заполнению";
$_['ms_validate_alpha_numeric'] = "Поле '%s' может содержать только буквенно-цифровые символы";
$_['ms_validate_latin'] = 'Поле \'%s\' может содержать только латинские символы.';
$_['ms_validate_utf8'] = 'Поле \'%s\'  может содержать только символы UTF-8';
$_['ms_validate_max_len'] = "Поле '%s' должно содержать '%s' или менее символов";
$_['ms_validate_min_len'] = "Поле '%s' должно содержать '%s' или более символов";
$_['ms_validate_phone_number'] = "Поле '%s' не является номером телефона";
$_['ms_validate_numeric'] = "Поле '%s' может содержать только цифровые символы";
$_['ms_validate_email'] = 'Поле \'%s\' должно быть адресом электронной почты.';
$_['ms_validate_email_exists'] = 'Этот адрес электронной почты уже зарегистрирован';
$_['ms_validate_password_confirm'] = 'Поля паролей не совпадают';

// Seller group settings
$_['ms_seller_group_product_number_limit'] = 'Максимальное количество товаров';

// Category-based and product-based fees
$_['ms_fees_heading'] = 'Комиссии MultiMerch';
$_['ms_config_fee_priority'] = 'Fee priority';
$_['ms_config_fee_priority_catalog'] = 'Каталог';
$_['ms_config_fee_priority_vendor'] = 'Продавец';
$_['ms_config_fee_priority_note'] = 'С включенной опцией \'Каталог\', список категорий/товаров и сбор с продаж будут иметь более высокий приоритет по отношению к продавцам / группе продавцов (и наоборот, если вы включите опцию \'Продавец\')';

// Seller attributes
$_['ms_global_attribute'] = '--Общий--';
$_['ms_catalog_attribute_attach_to_seller'] = 'Присоединить к продавцу';
$_['ms_catalog_attribute_all_sellers'] = '--Все продавцы--';
$_['ms_seller_attribute'] = 'Атрибут';
$_['ms_seller_attribute_group'] = 'Группа атрибута';
$_['ms_seller_attribute_manage'] = 'Управление атрибутами';

$_['ms_seller_attribute_tab_ocattribute'] = 'Атрибуты магазина';
$_['ms_seller_attribute_tab_msattribute'] = 'Атрибуты продавцов';

$_['ms_seller_attribute_updated'] = 'Успех: Атрибут(ы) обновлен(ы)!';
$_['ms_seller_attribute_deleted'] = 'Успех: Атрибут(ы) удален(ы)!';
$_['ms_seller_attribute_group_updated'] = 'Успех: Группа(ы) атрибутов обновлена(ы)!';
$_['ms_seller_attribute_group_deleted'] = 'Успех: Группа(ы) атрибутов удалена(ы)!';

$_['ms_seller_attribute_error_creating'] = 'Ошибка при создании атрибута!';
$_['ms_seller_attribute_error_updating'] = 'Ошибка обновленные атрибута!';
$_['ms_seller_attribute_error_deleting'] = 'Ошибка при удалении атрибута!';
$_['ms_seller_attribute_error_assigned'] = 'Внимание: Атрибут `%s` не может быть удален, так как он в настоящее время назначен %s товарам!';
$_['ms_seller_attribute_error_not_selected'] = 'Вы должны выбрать хотя бы один атрибут!';

$_['ms_seller_attribute_group_error_creating'] = 'Ошибка при создании группы атрибута!';
$_['ms_seller_attribute_group_error_updating'] = 'Ошибка при обновлении группы атрибута!';
$_['ms_seller_attribute_group_error_deleting'] = 'Ошибка при удалении группы атрибута!';
$_['ms_seller_attribute_group_error_assigned'] = 'Внимание: Группа атрибутов `%s` не может быть удалена, так как она в настоящее время назначена %s атрибутам!';
$_['ms_seller_attribute_group_error_not_selected'] = 'Вы должны выбрать хотя бы одну групу атрибутов!';

$_['ms_seller_attribute_status_' . MsAttribute::STATUS_DISABLED] = 'Выкл.';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_APPROVED] = 'Утвержденный';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_ACTIVE] = 'Активен';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_INACTIVE] = 'Неактивен';

// Seller options
$_['ms_global_option'] = '--Общий--';
$_['ms_catalog_option_attach_to_seller'] = 'Присоединить к продавцу';
$_['ms_catalog_option_all_sellers'] = '--Все продавцы--';
$_['ms_seller_option_heading'] = 'Опции';
$_['ms_seller_option_breadcrumbs'] = 'Опции';
$_['ms_seller_option'] = 'Опция';
$_['ms_seller_option_type'] = 'Тип';
$_['ms_seller_option_values'] = 'Значение опций';
$_['ms_seller_option_manage'] = 'Управление опциями';

$_['ms_seller_option_tab_ocoptions'] = 'Опции магазина';
$_['ms_seller_option_tab_msoptions'] = 'Опции продавцов';

$_['ms_seller_option_updated'] = 'Успех: Опция(и) обновлена(ы)!';
$_['ms_seller_option_deleted'] = 'Успех: Опция(и) удалена(ы)!';

$_['ms_seller_option_error_creating'] = 'Ошибка при создании опции!';
$_['ms_seller_option_error_updating'] = 'Ошибка при обновлении опции!';
$_['ms_seller_option_error_deleting'] = 'Ошибка при удалении опции';
$_['ms_seller_option_error_assigned'] = 'Внимание: Опция `%s` не может быть удалена, так как она в настоящее время назначена %s товарам!';
$_['ms_seller_option_error_not_selected'] = 'Вы должны выбрать хотя бы одну опцию!';

$_['ms_seller_option_status_' . MsOption::STATUS_DISABLED] = 'Отключено';
$_['ms_seller_option_status_' . MsOption::STATUS_APPROVED] = 'Утверждено';
$_['ms_seller_option_status_' . MsOption::STATUS_ACTIVE] = 'Активна';
$_['ms_seller_option_status_' . MsOption::STATUS_INACTIVE] = 'Неактивна';

// Seller categories
$_['ms_global_category'] = '--Глобально--';
$_['ms_catalog_category_attach_to_seller'] = 'Прикрепить к продавцу';
$_['ms_catalog_category_all_sellers'] = '--Все продавцы--';
$_['ms_seller_category_heading'] = 'Категории';
$_['ms_seller_category_breadcrumbs'] = 'Категории';
$_['ms_seller_category'] = 'Category';
$_['ms_seller_category_manage'] = 'Управление категориями';
$_['ms_categories_tab_occategories'] = 'Категории магазина';
$_['ms_categories_tab_mscategories'] = 'Категории продавцов';
/***** INSERTED *****/$_['ms_categories_view_mscategory_front_page'] = 'View seller category';
/***** INSERTED *****/$_['ms_categories_view_occategory_front_page'] = 'View marketplace category';


$_['ms_categories_button_add_occategory'] = 'Добавить новую категорию магазина';
$_['ms_categories_button_add_mscategory'] = 'Добавить новую категорию продавца';

$_['ms_seller_newcategory_heading'] = 'Добавить новую категорию продавца';
$_['ms_seller_editcategory_heading'] = 'Редактировать категорию продавца';
$_['ms_seller_category_general'] = 'Главное';
$_['ms_seller_category_name'] = 'Имя';
$_['ms_seller_category_description'] = 'Описание';
$_['ms_seller_category_meta_title'] = 'Мета название';
$_['ms_seller_category_meta_description'] = 'Мета описание';
$_['ms_seller_category_meta_keyword'] = 'Мета ключевые слова';
$_['ms_seller_category_data'] = 'Данные';
$_['ms_seller_category_seller'] = 'Продавец';
$_['ms_seller_category_parent'] = 'Родитель';
$_['ms_seller_category_filter'] = 'Фильтры';
$_['ms_seller_category_store'] = 'Магазины';
$_['ms_seller_category_keyword'] = 'SEO URL';
$_['ms_seller_category_image'] = 'Изображение';
$_['ms_seller_category_sort_order'] = 'Порядок сортировки';
$_['ms_seller_category_status'] = 'Статус';

$_['ms_seller_category_created'] = 'Успех: Категорию создано!';
$_['ms_seller_category_updated'] = 'Успех: Категорию обновлено!';
$_['ms_seller_category_deleted'] = 'Успех: Категорию удалено!';

$_['ms_oc_editcategory_heading'] = 'Редактировать категорию торговой площадки';
$_['ms_oc_newcategory_heading'] = 'Добавить новую категорию торговой площадки';
$_['ms_oc_category_error_keyword'] = 'SEO URL уже используется!';
$_['ms_oc_category_error_parent'] = 'Эта родительская категория является дочерней по отношению к текущей';
$_['ms_oc_category_error_name'] = 'Длина имени категории должна быть от 2-х до 255-и символов';

$_['ms_seller_category_error_creating'] = 'Ошибка создания категории!';
$_['ms_seller_category_error_updating'] = 'Ошибка обновления категории!';
$_['ms_seller_category_error_deleting'] = 'Ошибка удаления категории!';
$_['ms_seller_category_error_assigned'] = 'Внимание: Данная категория не может быть удалена, так как она назначена на %s товары!';
$_['ms_seller_category_error_no_sellers'] = 'Нет доступных продавцов';
$_['ms_seller_category_error_not_selected'] = 'Вы должны выбрать хотя бы одну категорию!';

$_['ms_seller_category_status_' . MsCategory::STATUS_DISABLED] = 'Отключено';
$_['ms_seller_category_status_' . MsCategory::STATUS_ACTIVE] = 'Активна';
$_['ms_seller_category_status_' . MsCategory::STATUS_INACTIVE] = 'Неактивна';

// Sale > Order > Info
$_['ms_order_details'] = "Детали заказа";
$_['ms_order_details_by_seller'] = 'Информация для заказа продавцом';
$_['ms_order_products_by'] = "Продавец: %s";
$_['ms_order_id'] = "Уникальный номер заказа продавца:";
$_['ms_order_current_status'] = "Текущий статус заказа продавца:";
$_['ms_order_sold_by'] = 'Продано:';
$_['ms_order_payment_history'] = 'История платежей';
$_['ms_order_payment_status'] = 'Статус платежа: <strong>%s</strong>';
$_['ms_order_payment_method'] = 'Платежный метод: <strong>%s</strong>';
$_['ms_order_order_status'] = 'Статус заказа: <strong>%s</strong>';
$_['ms_order_order_date'] = 'Дата заказа: <strong>%s</strong>';
$_['ms_order_change_order_status'] = 'Изменить статус заказа';
$_['ms_order_change_order_status_placeholder'] = "-- Выбрать новый статус заказа --";
$_['ms_order_change_order_status_error'] = "Вам нужно выбрать новый статус заказа";
$_['ms_order_change_payment_status'] = 'Изменить статус оплаты';
$_['ms_order_change_payment_status_placeholder'] = "-- Выберите новый статус оплаты --";
$_['ms_order_change_payment_status_error'] = "Вам нужно выбрать новый статус оплаты";
$_['ms_order_notify_customer_no'] = "Не уведомлять покупателей и продавца(-ов) об этом изменении (письмо отправлено не будет)";
$_['ms_order_notify_customer_yes'] = "Уведомлять покупателей и продавца(-ов) об этом изменении (будет отправлено письмо)";
$_['ms_order_address'] = "Адрес";
$_['ms_order_payment_address'] = "Платежный адрес";
$_['ms_order_shipping_address'] = "Адрес доставки";
$_['ms_order_comment'] = "Комментарии";

$_['ms_order_transactions'] = "Транзакции с балансом";
$_['ms_order_transactions_amount'] = 'Сумма';
$_['ms_order_transactions_description'] = 'Описание';
$_['ms_order_date_created'] = 'Дата создания';
$_['ms_order_notransactions'] = 'Продавец еще не получил никаких транзакций с балансом для этого заказа';

$_['ms_order_status_initial'] = 'Заказ создан';
$_['ms_order_history'] = "История заказа";

// Reviews
$_['ms_review_heading'] = 'Отзывы';
$_['ms_review_breadcrumbs'] = 'Отзывы';
$_['ms_review_manage'] = 'Управление отзывами';
$_['ms_review_column_product'] = 'Товар';
$_['ms_review_column_customer'] = 'Клиент';
$_['ms_review_column_seller'] = 'Продавец';
$_['ms_review_column_order'] = 'Заказ';
$_['ms_review_column_rating'] = 'Рейтинг';
$_['ms_review_column_comment'] = 'Комментарий';
$_['ms_review_column_date_added'] = 'Отправлено';

$_['ms_review_general'] = 'Отзыв';
$_['ms_review_edit_heading'] = "Отзыв клиента";
$_['ms_review_edit_product'] = 'Товар';
$_['ms_review_edit_order'] = 'ID Заказа';
$_['ms_review_edit_customer'] = 'Клиент';
$_['ms_review_edit_review'] = 'Отзыв';
$_['ms_review_edit_seller_response'] = "Ответ продавца";
$_['ms_review_edit_response'] = 'Ответ';
$_['ms_review_edit_rating'] = 'Рейтинг';
$_['ms_review_edit_customer_images'] = "Изображения клиента";
$_['ms_review_edit_images'] = 'Изображения';

$_['ms_success_review_updated'] = 'Отзыв обновлен!';
$_['ms_success_review_deleted'] = 'Отзыв удален!';
$_['ms_error_review_deleting'] = 'Ошибка удаления отзыва!';
$_['ms_error_review_id'] = 'Появились некоторые ошибки при обработке запроса!';

$_['ms_review_comments_success_added'] = 'Ваш комментарий был успешно отправлен!';
$_['ms_review_comments_error_signin'] = 'Пожалуйста, войдите на сайт, чтобы оставить комментарий!';
$_['ms_review_comments_error_review_id'] = 'Ошибка: Не указано id отзыва!';
$_['ms_review_comments_error_notext'] = 'Ошибка: Вы должны ввести сообщение!';
$_['ms_review_comments_textarea_placeholder'] = 'Введите ваше сообщение...';
$_['ms_review_comments_post_message'] = 'Оставить комментарий';
$_['ms_review_no_comments'] = 'Продавец еще не ответил на этот отзыв.';

$_['ms_review_status_' . MsReview::STATUS_ACTIVE] = 'Активен';
$_['ms_review_status_' . MsReview::STATUS_INACTIVE] = 'Неактивен';

// Questions
$_['ms_question_heading'] = 'Вопросы';
$_['ms_question_breadcrumbs'] = 'Вопросы';
$_['ms_question_manage'] = 'Управление вопросами';
$_['ms_question_column_product'] = 'Товар';
$_['ms_question_column_customer'] = 'Покупатель';
$_['ms_question_column_answer'] = 'Ответ';
$_['ms_question_column_date_added'] = 'Отправлено';

$_['ms_question_general'] = 'Вопрос';
$_['ms_question_edit_heading'] = 'Вопрос по товару';
$_['ms_question_edit_product'] = 'Товар';
$_['ms_question_edit_customer'] = 'Автор';
$_['ms_question_edit_question'] = 'Вопрос';
$_['ms_question_edit_seller_answer'] = "Ответ продавца";
$_['ms_question_edit_answer'] = 'Ответ';
$_['ms_question_no_answers'] = 'Продавец еще не ответил на этот вопрос!';
$_['ms_questions_customer_deleted'] = '*Покупатель удален*';

$_['ms_success_question_updated'] = 'Вопрос обновлен!';
$_['ms_success_question_deleted'] = 'Вопрос удален!';
$_['ms_error_question_deleting'] = 'Ошибка удаления вопроса!';
$_['ms_error_question_id'] = 'Появились некоторые ошибки при обработке запроса!';
$_['ms_error_answer_id'] = 'Появились некоторые ошибки при обработке запроса!';

// Reports
$_['ms_report_guest_checkout'] = 'Гость';
$_['ms_report_column_order'] = 'Заказ';
$_['ms_report_column_product'] = 'Товар';
$_['ms_report_column_seller'] = 'Продавец';
$_['ms_report_column_customer'] = 'Клиент';
$_['ms_report_column_email'] = 'Электронная почта';
$_['ms_report_column_gross'] = 'Брутто';
$_['ms_report_column_discount'] = 'Скидка';
$_['ms_report_column_net_total'] = 'Чистый итог';
$_['ms_report_column_net_marketplace'] = 'Без вычетов торговой площадки';
$_['ms_report_column_net_seller'] = 'Без вычетов продавца';
$_['ms_report_column_tax'] = 'Налог';
$_['ms_report_column_shipping'] = 'Доставка';
$_['ms_report_column_total'] = 'Сумма';
$_['ms_report_column_date'] = 'Дата';
$_['ms_report_column_date_month'] = 'Месяц';
$_['ms_report_column_total_sales'] = 'Всего продаж';
$_['ms_report_column_total_orders'] = 'Всего заказов';
$_['ms_report_column_transaction'] = 'Транзакция';
$_['ms_report_column_description'] = 'Описание';
$_['ms_report_column_payment'] = 'Оплата';
$_['ms_report_column_method'] = 'Способ';
$_['ms_report_column_payout'] = 'Выплата';
$_['ms_report_column_payer'] = 'Плательщик';
$_['ms_report_column_balance_in'] = 'На баланс';
$_['ms_report_column_balance_out'] = 'С баланса';
$_['ms_report_column_balance_current'] = 'Текущий баланс';
$_['ms_report_column_marketplace_earnings'] = 'Выручка торговой площадки';
$_['ms_report_column_seller_earnings'] = 'Выручка продавца';
$_['ms_report_column_payments_received'] = 'Платежи получены';
$_['ms_report_column_payouts_paid'] = 'Выплаты оплачены';
$_['ms_report_column_status'] = 'Статус';
$_['ms_report_column_amount'] = 'Сумма';
$_['ms_report_column_country'] = 'Страна';
$_['ms_report_column_period'] = 'Период';

$_['ms_report_manage_orders'] = 'Управление заказами';
$_['ms_report_manage_sellers'] = 'Управление продавцами';
$_['ms_report_manage_customers'] = 'Управление клиентами';
$_['ms_report_manage_products'] = 'Управление товарами';

$_['ms_report_date_range_today'] = 'Cегодня';
$_['ms_report_date_range_yesterday'] = 'Вчера';
$_['ms_report_date_range_last7days'] = 'Последние 7 дней';
$_['ms_report_date_range_last30days'] = 'Последние 30 дней';
$_['ms_report_date_range_thismonth'] = 'Этот месяц';
$_['ms_report_date_range_lastmonth'] = 'Прошлый месяц';
$_['ms_report_date_range_custom'] = 'Пользовательский диапазон';
$_['ms_report_date_range_apply'] = 'Применить';
$_['ms_report_date_range_cancel'] = 'Отмена';

$_['ms_report_date_range_day_mo'] = 'Пн';
$_['ms_report_date_range_day_tu'] = 'Вт';
$_['ms_report_date_range_day_we'] = 'Ср';
$_['ms_report_date_range_day_th'] = 'Чт';
$_['ms_report_date_range_day_fr'] = 'Пт';
$_['ms_report_date_range_day_sa'] = 'Сб';
$_['ms_report_date_range_day_su'] = 'Вс';

$_['ms_report_date_range_month_jan'] = 'Январь';
$_['ms_report_date_range_month_feb'] = 'Февраль';
$_['ms_report_date_range_month_mar'] = 'Март';
$_['ms_report_date_range_month_apr'] = 'Апрель';
$_['ms_report_date_range_month_may'] = 'Май';
$_['ms_report_date_range_month_jun'] = 'Июнь';
$_['ms_report_date_range_month_jul'] = 'Июль';
$_['ms_report_date_range_month_aug'] = 'Август';
$_['ms_report_date_range_month_sep'] = 'Сентябрь';
$_['ms_report_date_range_month_oct'] = 'Октябрь';
$_['ms_report_date_range_month_nov'] = 'Ноябрь';
$_['ms_report_date_range_month_dec'] = 'Декабрь';

// Custom fields and field groups common
$_['ms_custom_field_heading'] = 'Произвольное поле';
$_['ms_custom_field_manage'] = 'Управление произвольными полями';
$_['ms_custom_field_general'] = 'Общее';
$_['ms_custom_field_name'] = 'Имя';
$_['ms_custom_field_location'] = 'Место нахождения';
$_['ms_custom_field_value'] = 'Стоимость';
$_['ms_custom_field_sort_order'] = 'Порядок сортировки';
$_['ms_custom_field_validation'] = 'Проверка';
$_['ms_custom_field_validation_tooltip'] = 'Используйте регулярное выражение. Например: /^[a-zA-Z\d+ ]{2,30}$/';
$_['ms_custom_field_cf_count'] = 'Привязанные произвольные поля';
$_['ms_custom_field_status'] = 'Статус';
$_['ms_custom_field_required'] = 'Требуемый';
$_['ms_custom_field_type'] = 'Тип';
$_['ms_custom_field_note'] = 'примечание';
$_['ms_custom_field_location_' . MsCustomField::LOCATION_PRODUCT] = 'Товар';
$_['ms_custom_field_status_' . MsCustomField::STATUS_ACTIVE] = 'Активен';
$_['ms_custom_field_status_' . MsCustomField::STATUS_DISABLED] = 'Отключен';

// Custom fields
$_['ms_custom_field'] = 'Произвольное поле';
$_['ms_custom_field_breadcrumbs'] = 'Произвольное поле';
$_['ms_custom_field_new_heading'] = 'Новое произвольное поле';
$_['ms_custom_field_edit_heading'] = 'Изменить произвольное поле';
$_['ms_custom_field_create'] = 'Создать произвольное поле';
$_['ms_custom_field_confirm_delete'] = 'ПРЕДУПРЕЖДЕНИЕ: Вы собираетесь удалить произвольные поля. Вся информация о товаре, сохраненная в этих кастомных полях, также будет удалена. Эта операция не может быть отменена. Удалить произвольные поля?';
$_['ms_custom_field_error_deleting'] = 'Ошибка: невозможно удалить произвольное поле!';
$_['ms_custom_field_error_not_selected'] = 'Вы должны выбрать хотя бы одно произвольное поле!';
$_['ms_custom_field_error_values'] = 'Вы должны создать хотя бы одно произвольное поле!';
$_['ms_custom_field_success_created'] = 'Успех: Произвольное поле успешно создано!';
$_['ms_custom_field_success_updated'] = 'Успех: Произвольное поле успешно обновлено!';
$_['ms_custom_field_success_deleted'] = 'Успех: Произвольное поле успешно удалено!';

// Custom field groups
$_['ms_custom_field_group'] = 'Произвольная группа полей';
$_['ms_custom_field_group_breadcrumbs'] = 'Произвольная группа полей';
$_['ms_custom_field_group_new_heading'] = 'Новая произвольная группа полей';
$_['ms_custom_field_group_edit_heading'] = 'Изменить произвольную группу полей';
$_['ms_custom_field_group_create'] = 'Создать произвольную группу полей';
$_['ms_custom_field_group_confirm_delete'] = 'ПРЕДУПРЕЖДЕНИЕ: вы собираетесь удалить произвольные группы полей. Все произвольные поля, принадлежащие этим группам, также будут удалены вместе со всей информацией о соответствующем товаре, хранящейся в этих произвольных полях. Эта операция не может быть отменена. Удалить произвольные группы полей?';
$_['ms_custom_field_group_error_deleting'] = 'Ошибка: невозможно удалить произвольную группу полей!';
$_['ms_custom_field_group_error_locations'] = 'Вы должны выбрать хотя бы одно место для произвольной группы полей!';
$_['ms_custom_field_group_error_not_selected'] = 'Вы должны выбрать хотя бы одну кастомную группу полей!';
$_['ms_custom_field_group_success_created'] = 'Успех: произвольная группа полей успешно создана!';
$_['ms_custom_field_group_success_updated'] = 'Успех: произвольная группа полей успешно обновлена!';
$_['ms_custom_field_group_success_deleted'] = 'Успех: произвольная группа полей успешно удалена!';

// Orders
$_['ms_order_heading'] = 'Заказы';
$_['ms_order_breadcrumbs'] = 'Заказы';
$_['ms_order_tab_orders'] = 'Заказы';
$_['ms_order_tab_suborders'] = 'Заказы продавца';
$_['ms_order_column_date_added'] = 'Дата добавления';
$_['ms_order_column_date_modified'] = 'Дата изменения';
$_['ms_order_column_order_id'] = 'Заказ #';
$_['ms_order_column_suborder_id'] = 'Заказ продавца #';
$_['ms_order_column_order_status'] = 'Статус';
$_['ms_order_column_order_total'] = 'Сумма';
$_['ms_order_column_order_customer'] = 'Покупатель';
$_['ms_order_column_order_vendor'] = 'Продавец';
$_['ms_order_column_vendor_statuses'] = 'Статус заказа продавца';
$_['ms_order_column_action'] = 'Действие';

// Deletion of MultiMerch various items
$_['ms_delete_template_confirm'] = "ПРЕДУПРЕЖДЕНИЕ: вы собираетесь удалить %s. %s";
$_['ms_delete_affected'] = 'Это действие повлияет:';
$_['ms_delete_areyousure'] = 'Вы уверенны?';
$_['ms_delete_attribute'] = '%s атрибут(ы)';
$_['ms_delete_attribute_group'] = '%s группа(ы) атрибутов';
$_['ms_delete_badge'] = '%s бэйдж(и))';
$_['ms_delete_category'] = '%s категория(и)';
$_['ms_delete_occategory'] = '%s категория(и)';
$_['ms_delete_child_category'] = '%s детская категория(и)';
$_['ms_delete_conversation'] = '%s диалог(и)';
$_['ms_delete_custom_field'] = '%s произвольное(ые) поле(я)';
$_['ms_delete_custom_field_group'] = '%s группа(ы) произвольных полей';
$_['ms_delete_coupon'] = '%s купон(ы)';
$_['ms_delete_invoice'] = '%s счет(а)';
$_['ms_delete_msf_attribute'] = '%s атрибутов товара';
$_['ms_delete_msf_variation'] = '%s вариаций товара';
$_['ms_delete_msf_seller_property'] = '%s атрибутов продавца';
$_['ms_delete_option'] = '%s опция(и)';
$_['ms_delete_payment'] = '%s платеж(ы)';
$_['ms_delete_product'] = '%s товар(ы)';
$_['ms_delete_question'] = '%s вопрос(ы)';
$_['ms_delete_review'] = '%s отзыв(ы)';
$_['ms_delete_seller'] = '%s продавец(ы)';
$_['ms_delete_seller_group'] = '%s группа(ы) продавцов';
$_['ms_delete_shipping_method'] = '%s способ(ы) доставки';
$_['ms_delete_shipping_combined'] = '%s правило(а) комбинорованной доставки';
$_['ms_delete_shipping_product'] = '%s правило(а) доставки товара';
$_['ms_delete_social_channel'] = '%s социальный(ые) канал(ы)';
$_['ms_delete_seller_balance_warning'] = "Удалив продавца, баланс которого был ненулевым, Вы больше не сможете совершить выплату.";

// Coupons
$_['ms_coupon_heading'] = "Скидочные купоны";
$_['ms_coupon_breadcrumbs'] = "Купоны";
$_['ms_coupon_edit_info'] = "Купоны продавца могут быть созданы и изменены через интерфейсы продавца.";
$_['ms_coupon_code'] = "Код";
$_['ms_coupon_value'] = "Значение";
$_['ms_coupon_uses'] = "Использовано раз";
$_['ms_coupon_date_start'] = "Дата начала";
$_['ms_coupon_date_end'] = "Датта конца";
$_['ms_coupon_error_deleting'] = "Ошибка удаления товара(ов)!";
$_['ms_coupon_success_deleted'] = 'Успех: Купон успешно удален!';

// Complaints
$_['ms_config_complaints'] = 'Жалобы';
$_['ms_config_complaints_enable'] = 'Включить жалобы на товары и продавцов';
$_['ms_config_complaints_enable_note'] = 'Разрешить покупателям отправлять жалобы на товары и продавцов';

$_['ms_complaints_manage'] = 'Управление жалобами';
$_['ms_complaints_column_date_added'] = 'Дата получения';
$_['ms_complaint_column_reason'] = 'Причина';
$_['ms_complaint_column_reporter'] = 'Покупатель';
$_['ms_error_complaint_id'] = 'Ошибка совершения запроса!';

// Holiday mode
$_['ms_config_holiday_mode'] = 'Режим отпуска';
$_['ms_config_holiday_mode_note'] = 'Разрешить продавцам выставлять свои магазины в режим отпуска.';

// Invoicing system
$_['ms_invoices'] = 'Счета';
$_['ms_invoice_heading'] = "Счета";
$_['ms_invoice_breadcrumbs'] = "Счета";
$_['ms_invoice_column_invoice_id'] = "Счет #";
$_['ms_invoice_column_type'] = "Тип";
$_['ms_invoice_column_title'] = "Заголовок";
$_['ms_invoice_column_sender'] = "Отправитель";
$_['ms_invoice_column_recipient'] = "Получатель";
$_['ms_invoice_column_status'] = "Статус";
$_['ms_invoice_column_date_generated'] = "Дата создания";
$_['ms_invoice_column_total'] = "Сумма";
$_['ms_invoice_column_payment_id'] = "Платеж #";

$_['ms_invoice_success_deleted'] = 'Успех: Счет удален!';

$_['ms_invoice_error_not_selected'] = "Ошибка: Выберите счет!";
$_['ms_invoice_error_not_exists'] = "Ошибка: Не удалось получить счет #%s!";
$_['ms_invoice_error_not_created'] = "Ошибка: Не удалось создать счет (продавец %s)!";
$_['ms_invoice_error_seller_notselected'] = 'Ошибка: Вы должны выбрать хотя бы одного продавца!';
$_['ms_invoice_error_type'] = 'Ошибка: неправильный тип счета!';

$_['ms_invoice_type_listing'] = "Стоимость публикации";
$_['ms_invoice_type_signup'] = "Стоимость регистрации";
$_['ms_invoice_type_sale'] = "Продажа";
$_['ms_invoice_type_payout'] = "Выплата";

$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID] = '<p style="color: red">Ожидается</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_PAID] = '<p style="color: green">Оплачен</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_VOIDED] = '<p style="color: red">Отменен</p>';

$_['ms_invoice_title_product_listing'] = "Стоимость публикации товара <a href='%s'>%s</a>";
$_['ms_invoice_title_seller_signup'] = "Комиссия за регистрацию в %s";
$_['ms_invoice_title_sale_order_commission'] = "Комиссия платформы за товары в заказе <a href='%s'>#%s</a>";
$_['ms_invoice_title_sale_order_products'] = "Товары заказа <a href='%s'>#%s</a>";
$_['ms_invoice_title_seller_payout'] = "Выплата %s";

$_['ms_invoice_item_title_seller_payout'] = "Выплата продавцу %s";
$_['ms_invoice_item_title_product_listing'] = "Стоимость публикации товара %s";
$_['ms_invoice_item_title_seller_signup'] = "Комиссия за регистрацию в %s";
$_['ms_invoice_item_title_sale_commission'] = "Комиссия платформы за %s в заказе #%s";
$_['ms_invoice_item_title_sale_product'] = "Товар %s";

$_['ms_invoice_total_title_subtotal'] = "Промежуточная сумма";
$_['ms_invoice_total_title_shipping'] = "Доставка платформы";
$_['ms_invoice_total_title_mm_shipping_total'] = "Доставка продавца";
$_['ms_invoice_total_title_coupon'] = "Купон";
$_['ms_invoice_total_title_ms_coupon'] = "Купон продавца";
$_['ms_invoice_total_title_tax'] = "Налог";
$_['ms_invoice_total_title_total'] = "Сумма";

// Shipping system
$_['ms_shipping_mode'] = "Режим доставки";
$_['ms_shipping_mode_note'] = "Режим доставки определяет то, как товар доставляется на вашей торговой площадке";
$_['ms_shipping_fields'] = "Поля доставки";
$_['ms_shipping_methods'] = "Методы доставки";
$_['ms_shipping_methods_note'] = "Определите методы доставки (название фирм-перевозчиков) для продавцов. Из этих методов будут формироваться правила доставки";
$_['ms_shipping_methods_manage'] = "Управлять методами доставки";
$_['ms_shipping_delivery_times'] = "Время доставки";
$_['ms_shipping_delivery_times_note'] = "Определите время доставки для продавцов. Из этих вариантов будут формироваться правила доставки";

$_['ms_shipping_type'] = "Тип доставки";
$_['ms_shipping_type_note'] = <<<EOT
Режим доставки определяет то, как товар доставляется на вашей торговой площадке:<br>
<strong>Отключить</strong> - доставка отключена.<br>
<strong>Настройки OC</strong> - использовать стандартную систему доставки от OpenCart's (недоступно для продавцов).<br>
<strong>Доставка продавца (фиксированная стоимость)</strong> - позволяет продавцам определить стоимость для разных пунктов назначения.<br>
<strong>Доставка продавца (по сумме в Корзине)</strong> - позволяет продавцам определить стоимость для разных пунктов назначения исходя из суммы в Корзине.<br>
<strong>Доставка продавца (по весу в Корзине)</strong> - позволяет продавцам определить стоимость для разных пунктов назначения исходя из веса товаров в Корзине.<br>
<strong>Доставка продавца (по каждому товару)</strong> - Позволяет продавцам определить стоимость доставки по каждому товару.
EOT;

$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_DISABLED] = "Отключено";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_OC] = "Настройки OC";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_CART_WEIGHT] = "Доставка продавца (по весу в Корзине)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_CART_TOTAL] = "Доставка продавца (по сумме в Корзине)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_FLAT] = "Доставка продавца (фиксированная стоимость)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT] = "Доставка продавца (по каждому товару)";

$_['ms_config_shipping_field_from_enable'] = "Включить поле 'Доставка из'";
$_['ms_config_shipping_field_note_from_enable'] = "Позволяет продавцам в их правилах доставки определять 'Доставку из'";
$_['ms_config_shipping_field_processing_time_enable'] = "Включить время обработки";
$_['ms_config_shipping_field_note_processing_time_enable'] = "Предоставить продавцам возможность указывать их время доставки";
$_['ms_config_shipping_field_shipping_method_enable'] = "Включить методы доставки";
$_['ms_config_shipping_field_note_shipping_method_enable'] = "Предоставить продавцам возможность указывать разную стоимость доставки через разные методы доставки к одному и тому же пункту назначения";
$_['ms_config_shipping_field_delivery_time_enable'] = "Включить время доставки";
$_['ms_config_shipping_field_note_delivery_time_enable'] = "Предоставить продавцам возможность указывать приблизительное время доставки по их правилам доставки";
$_['ms_config_shipping_field_cost_ai_enable'] = "Включить стоимость за единицу товара";
$_['ms_config_shipping_field_note_cost_ai_enable'] = "Предоставить продавцам возможность указывать разную стоимость доставки за каждую дополнительную единицу товара";
$_['ms_config_shipping_field_cost_pwu_enable'] = "Включить цену по весу";
$_['ms_config_shipping_field_note_cost_pwu_enable'] = "Предоставить продавцам возможность указывать стоимость доставки по весу, в дополнение к фиксированной стоимости";
$_['ms_config_shipping_field_per_product_override'] = "Включить пересчет по товару";
$_['ms_config_shipping_field_note_per_product_override'] = "Позволяет продавцам пересчитывать стоимость доставки по умолчанию через другие методы доставки для одного и того же места назначения";

// Commissions system
$_['ms_config_fees'] = "Сбор";
$_['ms_config_signup_fee_enabled'] = "Включить сбор за регистрацию";
$_['ms_config_signup_fee_enabled_note'] = "Взимать с продавцов сбор за регистрацию аккаунта продавца. Вы можете определить сумму сбора для разных групп продавцов.";
$_['ms_config_listing_fee_enabled'] = "Включить сбор за публикацию";
$_['ms_config_listing_fee_enabled_note'] = "Взимать с продавцов сбор за публикацию товара. Вы можете определить сумму сбора для разных категорий товаров, групп продавцов и отдельных продавцов.";
$_['ms_config_sale_fee_seller_enabled'] = "Включить сбор с продажи";
$_['ms_config_sale_fee_seller_enabled_note'] = "Взимать с продавцов фиксированную сумму за каждый оформленный заказ. Вы сможете определить сумму сбора для групп продавцов и отдельных продавцов.";
$_['ms_config_sale_fee_catalog_enabled'] = "Включить сбор с продажи по категории/товару";
$_['ms_config_sale_fee_catalog_enabled_note'] = "Взимать с продавцов сбор за каждый проданный товар. Вы сможете уточнить сумму сбора для групп продавцов и отдельных продавцов.";
$_['ms_config_sale_fee_calculation_mode'] = "Режим расчетов по сбору с продажи (устаревшее)";
$_['ms_config_sale_fee_calculation_mode_note'] = "Контролирует подсчет сбора с продаж в случае, если включены обе опции (сбор с продаж по каталогу и сбор с продавца):<br>Стандарт – сбор с продавца и по каталогу включены, сбор с продавца начисляется с каждого заказа, сбор по каталогу происходит с каждого товара.<br>Наследование – сбор с продавца или сбор по каталогу начисляются исходя из предыдущих настроек, оба рассчитываются за единицу товара SKU.";
$_['ms_config_sale_fee_calculation_mode_product_and_suborder'] = "Стандарт";
$_['ms_config_sale_fee_calculation_mode_order_product'] = "Наследование";
$_['ms_config_fee_priority'] = "Selling fee priority (deprecated)";
$_['ms_config_fee_priority_note'] = "This controls the priority of selling fees in Legacy mode if both vendor and catalog selling fees are enabled:<br>Catalog – catalog selling fee is applied if both catalog and vendor fees are enabled.<br>Vendor – vendor selling fee is applied if both catalog and vendor fees are enabled.";

$_['ms_seller_group_commission_no_settings'] = "Для данной группы сборы с продаж не установлены.";
$_['ms_seller_group_commission_add_settings'] = "+ сбор с продаж для продавцов в этой группе";
$_['ms_seller_commission_override_off_note'] = "Этот продавец часть группы <strong>%s</strong> следующие ставки будут применены к: %s";
$_['ms_seller_commission_override_off_btn'] = "Переназначить стандартный сбор для этого продавца?";
$_['ms_seller_commission_override_on_note'] = "Указать сумму сбора для этого продавца и переназначить ставку из группы продавцов <a href='%s' target='_blank'></a>";
$_['ms_seller_commission_override_on_btn'] = "Удалить изменения и вернуться к ставкам данной группы продавцов?";
$_['ms_oc_category_commission_override_off_note'] = "Сумма сбора была перенесена из родительской категории <a href='%s' target='_blank'>%s</a>: %s";
$_['ms_oc_category_commission_override_off_btn'] = "Переназначить сумму сбора для этой категории?";
$_['ms_oc_category_commission_override_on_note'] = "Определить сумму сбора для данной категории и переназначить ставки родительской категории <a href='%s' target='_blank'>%s</a>";
$_['ms_oc_category_commission_override_on_btn'] = "Удалить изменения и вернуться к ставкам родительской категории?";
$_['ms_product_commission_no_category'] = "У этого товара нет сбора с продажи.";
$_['ms_product_commission_override_off_note'] = "Сумма сбора с продажи для данного товара наследована из его главной категории <strong>%s</strong>: %s";
$_['ms_product_commission_override_off_multiple_categories_note'] = "Этот товар записан в нескольких категориях. Будет применяться максимальный сбор категории: <strong>%s</strong>: %s";
$_['ms_product_commission_override_off_btn'] = "Переназначить стандартный сбор для этого товара?";
$_['ms_product_commission_override_on_note'] = "Определить сумму сбора для данного товара и переназначить сумму из <a href='%s' target='_blank'>главной категории товара</a>";
$_['ms_product_commission_override_on_btn'] = "Удалить изменения и вернуть ставки категории?";

$_['ms_customer_total_orders'] = 'Заказы';
$_['ms_customer_total_orders_value'] = 'Значение';

$_['ms_menu_zones_tab_geozones'] = 'Зоны';
$_['ms_menu_zones_tab_countries'] = 'Страны';
$_['ms_menu_zones_tab_zones'] = 'Регионы';

$_['ms_geo_zone_heading'] = 'Зоны';
$_['ms_geo_zone_list'] = 'Список зон';
$_['ms_geo_zone_name'] = 'Название зоны';
$_['ms_geo_zone_edit'] = 'Редактировать зону';
$_['ms_geo_zone_add'] = 'Добавить новую зону';
$_['ms_geo_zone_add_region_button'] = 'Добавить регион';
$_['ms_geo_zone_error_name'] = 'Название зоны должно быть от 3-х до 255-и символов в длину!';
$_['ms_geo_zone_error_description'] = 'Описание должно быть от 3-х до 255-и символов в длину';
$_['ms_geo_zone_error_delete_tax_rate'] = 'Внимание: эту зону удалить нельзя, потому что она приписана к одной или нескольким налоговым ставкам';
$_['ms_geo_zone_added'] = 'Вы добавили зону!';
$_['ms_geo_zone_deleted'] = 'Вы удалили зону!';
$_['ms_geo_zone_updated'] = 'Вы изменили зону!';

$_['ms_country_heading'] = 'Страны';
$_['ms_country_list'] = 'Список стран';

$_['ms_country_added'] = 'Вы добавили страну!';
$_['ms_country_deleted'] = 'Вы удалили страну!';
$_['ms_country_updated'] = 'Вы изменили страну!';

$_['ms_zone'] = 'Регион';
$_['ms_zone_name'] = 'Имя региона';
$_['ms_zone_code'] = 'Код региона';
$_['ms_zone_error_name'] = 'Название региона должно быть от 3-х до 128-и символов в длину';
$_['ms_zone_heading'] = 'Регионы';
$_['ms_zone_add'] = 'Добавить новый регион';
$_['ms_zone_edit'] = 'Редактировать регион';
$_['ms_zone_list'] = 'Список регионов';
$_['ms_zone_added'] = 'Вы добавили регион!';
$_['ms_zone_deleted'] = 'Вы удалили регион!';
$_['ms_zone_updated'] = 'Вы изменили регион!';

$_['ms_zones'] = 'Регионы';
$_['ms_zones_all'] = 'Все регионы';

$_['ms_country_error_delete_binded_to_zone'] = 'Внимание: Эту страну нельзя удалить, потому что в данный момент она приписана к зонам %s!';
$_['ms_country_error_delete_binded_to_regions'] = 'Внимание: Эту страну нельзя удалить, потому что в данный момент она приписана к регионам %s!';

// MultiMerch product attributes
$_['ms_menu_field_attributes'] = "Атрибуты товара";
$_['ms_field_attribute_heading'] = "Атрибуты товара";
$_['ms_field_attribute_heading_new'] = "Создать новый атрибут товара";
$_['ms_field_attribute_heading_update'] = "Модифицировать атрибут товара";
$_['ms_field_attribute_breadcrumbs'] = "Атрибуты товара";
$_['ms_field_attribute_manage'] = "Управлять атрибутами товара";
$_['ms_field_attribute_create'] = "Создать новый атрибут товара";
$_['ms_field_attribute_general'] = "Общее";
$_['ms_field_attribute_name'] = "Имя";
$_['ms_field_attribute_name_note'] = "Имя атрибута отображается пользователям по всей торговой площадке, а также продавцам во время формирования списка товаров.";
$_['ms_field_attribute_no_name'] = "Нет имени";
$_['ms_field_attribute_label'] = "Внутренний ярлык";
$_['ms_field_attribute_label_note'] = "Используйте ярлыки, чтобы облегчить управление похожими элементами. Например, Размер (одежда). Ярлыки действуют только во внутреннем режиме и не отображаются непосредственно в магазине.";
$_['ms_field_attribute_note'] = "Описание";
$_['ms_field_attribute_note_note'] = "Описание атрибута отображается продавцам как комментарий при формировании списка товаров. Составьте его так, чтобы продавцам было ясно, что это за атрибут.";
$_['ms_field_attribute_type'] = "Тип";
$_['ms_field_attribute_required'] = "Требуется";
$_['ms_field_attribute_default'] = "Стандартно";
$_['ms_field_attribute_sort_order'] = "Порядок сортировки";
$_['ms_field_attribute_status'] = "Статус";
$_['ms_field_attribute_status_note'] = "Статус позволяет вам включить или отключить этот атрибут глобально для всего сайта.";
$_['ms_field_attribute_status_0'] = "Неактивен";
$_['ms_field_attribute_status_1'] = "Активен";
$_['ms_field_attribute_value'] = "Значение";
$_['ms_field_attribute_values'] = "Значения";
$_['ms_field_attribute_values_note'] = "Продавцы смогут выбрать во время формирования списка товаров одно или более значений для этого атрибута. Например, Размер: S, M, L";
$_['ms_field_attribute_btn_create_value'] = "Создать значение";
$_['ms_field_attribute_btn_create_value_long'] = "Создать новое значение";
$_['ms_field_attribute_categories'] = "Категории";
$_['ms_field_attribute_success_created'] = "Атрибут был успешно создан!";
$_['ms_field_attribute_success_updated'] = "Атрибут был успешно обновлен!";
$_['ms_field_attribute_success_deleted'] = "Атрибут был успешно удален!";
$_['ms_field_attribute_error_not_selected'] = "Вам нужно выбрать хотя бы один атрибут!";
$_['ms_field_attribute_error_values'] = "Вам нужно добавить хотя бы одно значение атрибута!";

$_['ms_field_attribute_message_default'] = "Товары в данной категории будут использовать стандартный набор атрибутов. Это также касается дочерних категорий.";
$_['ms_field_attribute_message_override'] = "Товары в данной категории будут использовать атрибуты, указанные ниже. Это также касается дочерних категорий.";
$_['ms_field_attribute_message_inherited'] = "Атрибуты для товаров, указанные в данной категории, взяты из родительской категории <strong>%s</strong>.";
$_['ms_field_attribute_btn_override'] = "Использовать в этой категории другие атрибуты?";
$_['ms_field_attribute_btn_remove_override'] = "Отменить изменения и использовать стандартный набор атрибутов?";
$_['ms_field_attribute_select_placeholder'] = "Выбрать атрибуты для товаров в данной категории?";

// MultiMerch product variations
$_['ms_menu_field_variations'] = "Вариации";
$_['ms_product_tab_variations'] = "Вариации";
$_['ms_field_variation_heading'] = "Вариации товара";
$_['ms_field_variation_heading_new'] = "Создать новую вариацию товара";
$_['ms_field_variation_heading_update'] = "Модифицировать вариацию товара";
$_['ms_field_variation_breadcrumbs'] = "Вариация товара";
$_['ms_field_variation_manage'] = "Управлять вариациями товара";
$_['ms_field_variation_create'] = "Создать новую вариацию товара";
$_['ms_field_variation_general'] = "Общее";
$_['ms_field_variation_name'] = "Имя";
$_['ms_field_variation_name_note'] = "Имя вариации отображается пользователям по всей торговой площадке, а также продавцам во время формирования списка товаров.";
$_['ms_field_variation_no_name'] = "Нет имени";
$_['ms_field_variation_label'] = "Внутренний ярлык";
$_['ms_field_variation_label_note'] = "Используйте ярлыки, чтобы облегчить управление похожими элементами. Например, Размер (одежда). Ярлыки действуют только во внутреннем режиме и не отображаются непосредственно в магазине.";
$_['ms_field_variation_note'] = "Описание";
$_['ms_field_variation_note_note'] = "Описание вариации отображается продавцам как комментарий при формировании списка товаров. Составьте его так, чтобы продавцам было ясно, что это за вариация.";
$_['ms_field_variation_default'] = "Стандартно";
$_['ms_field_variation_status'] = "Статус";
$_['ms_field_variation_status_note'] = "Статус позволяет вам включить или отключить эту вариацию глобально для всего сайта.";
$_['ms_field_variation_status_0'] = "Неактивен";
$_['ms_field_variation_status_1'] = "Активен";
$_['ms_field_variation_value'] = "Значение";
$_['ms_field_variation_values'] = "Значения";
$_['ms_field_variation_values_note'] = "Продавцы смогут выбрать во время формирования списка товаров одно или более значений для этой вариации. Например, Размер: S, M, L";
$_['ms_field_variation_btn_create_value'] = "Создать значение";
$_['ms_field_variation_btn_create_value_long'] = "Создать новое значение";
$_['ms_field_variation_categories'] = "Категории";
$_['ms_field_variation_variants'] = "Варианты";
$_['ms_field_variation_success_created'] = "Вариация была успешно создана!";
$_['ms_field_variation_success_updated'] = "Вариация была успешно обновлена!";
$_['ms_field_variation_success_deleted'] = "Вариация была успешно удалена!";
$_['ms_field_variation_error_not_selected'] = "Вы должны выбрать хотя бы одну вариацию!";
$_['ms_field_variation_error_values'] = "Вы должны добавить хотя бы одно значение вариации!";

$_['ms_field_variation_message_default'] = "Товары в данной категории будут использовать стандартный набор вариаций. Это также касается дочерних категорий.";
$_['ms_field_variation_message_override'] = "Товары в данной категории будут использовать вариации, указанные ниже. Это также касается дочерних категорий.";
$_['ms_field_variation_message_inherited'] = "Вариации товаров, указанные в категории ниже, взяты из родительской категории <strong>%s</strong>.";
$_['ms_field_variation_btn_override'] = "Использовать другие вариации в данной категории?";
$_['ms_field_variation_btn_remove_override'] = "Отменить изменения и использовать стандартный набор вариаций?";
$_['ms_field_variation_select_placeholder'] = "Выберите вариации для товаров данной категории";

$_['ms_field_product_variation_not_selected'] = "Пожалуйста, выберите хотя бы один вариант для %s";
$_['ms_field_product_variation_select_category'] = "Пожалуйста, выберите категорию, чтобы получить доступные товару опции.";
$_['ms_field_product_variation_error_quantity'] = "Пожалуйста, установите количество для варианта товара";
$_['ms_field_product_variation_error_price_invalid'] = "Неверная цена для варианта товара %s";
$_['ms_field_product_variation_error_price_empty'] = "Пожалуйста, укажите цену для варианта товара %s";
$_['ms_field_product_variation_error_price_price_low'] = "Цена для варианта  %s слишком низкая";
$_['ms_field_product_variation_error_price_price_high'] = "Цена для варианта  %s слишком высокая";
$_['ms_field_product_variation_error_quantity'] = "Please, specify a quantity for product variant %s";

// MultiMerch seller properties
$_['ms_menu_field_seller_properties'] = "Атрибуты продавца";
$_['ms_menu_field_seller_properties_short'] = "Атрибуты";
$_['ms_field_seller_property_heading'] = "Атрибуты продавца";
$_['ms_field_seller_property_heading_new'] = "Создать новый атрибут продавца";
$_['ms_field_seller_property_heading_update'] = "Модифицировать атрибут продавца";
$_['ms_field_seller_property_breadcrumbs'] = "Атрибуты продавца";
$_['ms_field_seller_property_manage'] = "Управлять атрибутами продавца";
$_['ms_field_seller_property_create'] = "Создать новый атрибут продавца";
$_['ms_field_seller_property_general'] = "Общее";
$_['ms_field_seller_property_name'] = "Имя";
$_['ms_field_seller_property_name_note'] = "Имя атрибуты отображается продавцу при входе в аккаунт.";
$_['ms_field_seller_property_no_name'] = "Нет имени";
$_['ms_field_seller_property_label'] = "Внутренний ярлык";
$_['ms_field_seller_property_label_note'] = "Используйте ярлыки, чтобы облегчить управление похожими атрибутами. Ярлыки действуют только во внутреннем режиме и не отображаются непосредственно в магазине.";
$_['ms_field_seller_property_note'] = "Описание";
$_['ms_field_seller_property_note_note'] = "Описание атрибута отображается продавцу во время регистрации как комментарий. Используйте его, чтобы объяснить, какая информация вам нужна.";
$_['ms_field_seller_property_type'] = "Тип";
$_['ms_field_seller_property_required'] = "Требуется";
$_['ms_field_seller_property_default'] = "Стандартно";
$_['ms_field_seller_property_sort_order'] = "Порядок сортировки";
$_['ms_field_seller_property_status'] = "Статус";
$_['ms_field_seller_property_status_note'] = "Статус позволяет включать или отключать этот атрибут глобально по всему сайту.";
$_['ms_field_seller_property_status_0'] = "Неактивен";
$_['ms_field_seller_property_status_1'] = "Активен";
$_['ms_field_seller_property_value'] = "Значение";
$_['ms_field_seller_property_values'] = "Значения";
$_['ms_field_seller_property_values_note'] = "Продавцы смогут выбирать одно или более значений для этого атрибута во время регистрации. Например, Я хочу продать: Книги, Одежду, Электронику.";
$_['ms_field_seller_property_btn_create_value'] = "Создать значение";
$_['ms_field_seller_property_btn_create_value_long'] = "Создать новое значение";
$_['ms_field_seller_property_seller_groups'] = "Группы продавца";
$_['ms_field_seller_property_success_created'] = "Атрибут продавца был успешно создан!";
$_['ms_field_seller_property_success_updated'] = "Атрибут продавца был успешно обновлен!";
$_['ms_field_seller_property_success_deleted'] = "Атрибут продавца был успешно удален!";
$_['ms_field_seller_property_error_not_selected'] = "Вы должны выбрать хотя бы один атрибут продавца!";
$_['ms_field_seller_property_error_values'] = "Вы должны добавить хотя бы одно значение атрибута!";
$_['ms_field_seller_property_error_field_empty'] = "Поле '%s' необходимо";
$_['ms_field_seller_property_error_field_length'] = "Поле '%s' слишком длинное!";

$_['ms_field_seller_property_message_default'] = "Продавцы в этой группе получат стандартный набор атрибутов.";
$_['ms_field_seller_property_message_override'] = "Продавцы в этой группе получат набор атрибутов, указанный ниже.";
$_['ms_field_seller_property_btn_override'] = "Использовать другие атрибуты продавца в этой группе?";
$_['ms_field_seller_property_btn_remove_override'] = "Отменить изменения и использовать стандартные атрибуты продавца?";
$_['ms_field_seller_property_select_placeholder'] = "Выберите атрибуты для продавцов в этой группе продавцов";

// Default configurations
$_['ms_field_default_configuration_heading'] = "Стандартная конфигурация полей";
$_['ms_field_default_configuration_breadcrumbs'] = "Стандартная конфигурация полей";
$_['ms_field_default_configuration_success_updated'] = "Стандартная конфигурация полей обновлена!";
$_['ms_field_default_configuration_attribute'] = "Назначить атрибут для стандартной конфигурации";
$_['ms_field_default_configuration_msf_attribute_title'] = "Стандартные атрибуты товара";
$_['ms_field_default_configuration_msf_attribute_subtitle'] = "Выбрать стандартные атрибуты товара, которые будут доступны во всех категориях. Вы сможете изменить это потом в отдельных категориях.";
$_['ms_field_default_configuration_variation'] = "Назначить вариации стандартной конфигурации";
$_['ms_field_default_configuration_msf_variation_heading'] = "Стандартные вариации товара";
$_['ms_field_default_configuration_msf_variation_note'] = "Выбрать стандартные вариации товара, которые будут доступны во всех категориях. Вы сможете изменить это потом в отдельных категориях.";
$_['ms_field_default_configuration_seller_property'] = "Назначить атрибуты продавца для стандартной конфигурации";
$_['ms_field_default_configuration_msf_seller_property_title'] = "Стандартные атрибуты продавца";
$_['ms_field_default_configuration_msf_seller_property_subtitle'] = "Выбрать стандартные атрибуты продавца, которые будут доступны по регистрации во всех группах продавцов. Потом вы сможете изменить это для отдельных групп продавцов.";
$_['ms_field_default_configuration_mspf_block_title'] = "Фильтры";
$_['ms_field_default_configuration_mspf_block_subtitle'] = "Выбрать стандартную конфигурацию фильтра, которая будет доступна во всех категориях. Позже вы сможете изменить настройки для отдельных категорий.";

// MS product filter blocks
$_['ms_product_filter_block_heading'] = "Фильтры";
$_['ms_product_filter_block_title_general'] = "Общее";
$_['ms_product_filter_block_title_attributes'] = "Атрибуты";
$_['ms_product_filter_block_title_oc_options'] = "Опции";
$_['ms_product_filter_block_title_oc_manufacturers'] = "Производители";
$_['ms_product_filter_block_title_price'] = "Цена";
$_['ms_product_filter_block_title_category'] = "Категории";

$_['ms_product_filter_block_message_default'] = "Эта категория будет использовать стандартную конфигурацию фильтра. Это также касается и детской категории.";
$_['ms_product_filter_block_message_inherited'] = "Конфигурация фильтра для данной категории взята из родительской категории <strong>%s</strong>.";
$_['ms_product_filter_block_message_override'] = "Эта категория будет использовать конфигурацию фильтра, указанную ниже. Это также касается и детской категории.";
$_['ms_product_filter_block_btn_override'] = "Использовать другую конфигурацию фильтра для данной категории?";
$_['ms_product_filter_block_btn_remove_override'] = "Отменить изменения и использовать стандартную конфигурацию фильтра?";
$_['ms_product_filter_block_select_placeholder'] = "Выбрать фильтр для этой категории";

/***** INSERTED *****/$_['ms_print_order_packing_slip'] = "Print packing slip";
/***** INSERTED *****/$_['ms_print_order_invoice'] = "Print invoice";
