<?php

// Heading
$_['heading_title']    = '[MultiMerch] Скидочные купоны';

// Text
$_['text_success']     = 'Успех: вы изменили [MultiMerch] Скидочные купоны!';
$_['text_edit']        = 'Дополнение [MultiMerch] Скидочные купоны';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

// Error
$_['error_permission'] = 'Предупреждение: Вы не можете изменять [MultiMerch] Скидочные купоны!';