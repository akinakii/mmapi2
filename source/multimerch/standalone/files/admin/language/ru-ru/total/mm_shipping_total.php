<?php

// Heading
$_['heading_title']    = '[MultiMerch] Модуль Shipping Total';

// Text
$_['text_success']     = 'Вы успешно отредактировали сумму заказа';
$_['text_edit']        = 'Редактировать [MultiMerch] Shipping Total';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав для редактирования суммы заказа!';