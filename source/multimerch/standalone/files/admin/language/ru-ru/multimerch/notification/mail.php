<?php

$_['msn_mail_subject_admin_updated_suborder_status'] = "%s обновил ваш заказ #%s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
%s обновил заказ в %s:

Заказ#: %s

Товары:
%s

Статус: %s

Комментарий:
%s
EOT;

$_['msn_mail_subject_admin_created_message'] = "Получено новое сообщение";
$_['msn_mail_admin_created_message'] = <<<EOT
Вы получили новое сообщение от %s!

%s

%s

Вы можете ответить на него через ваш аккаунт.
EOT;

$_['msn_mail_subject_admin_created_payout'] = "Получена выплата от %s";
$_['msn_mail_admin_created_payout'] = <<<EOT
Вы получили новую выплату от %s.
EOT;

$_['msn_mail_subject_admin_updated_account_status'] = "Аккаунт продавца изменен";
$_['msn_mail_admin_updated_account_status'] = <<<EOT
Администратор платформы %s изменил Ваш аккаунт продавца.

Статус: %s
EOT;
