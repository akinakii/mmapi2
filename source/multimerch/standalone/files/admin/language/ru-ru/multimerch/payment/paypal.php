<?php
// Heading
$_['heading_title'] 		  = 'PayPal';

// Text
$_['text_method_name'] 		  = 'PayPal';
$_['text_s_method_name']	  = 'PayPal Standard';
$_['text_mp_method_name']	  = 'PayPal Masspay';

$_['text_success']			  = 'Вы успешно изменили настройки PayPal.';
$_['text_edit']               = 'Изменить PayPal';
$_['text_paypal']	  		  = '<img src="view/image/multiseller/payment/paypal.png" alt="PayPal" title="PayPal" />';

$_['text_receiver'] 		  = 'Получатель';
$_['text_sender'] 			  = 'Отправииель';

$_['text_pp_address'] 		  = 'PayPal Адресс';
$_['text_api_username'] 	  = 'API Имя пользователя';
$_['text_api_password'] 	  = 'API Пароль';
$_['text_api_signature'] 	  = 'API Подпись';
$_['text_sandbox'] 			  = 'Sandbox mode';

$_['text_pp_address_note'] 	  = 'PayPal адрес владельца магазина, который будет использоваться для приема и обработки платежей';
$_['text_api_username_note']  = 'API Имя пользователя';
$_['text_api_password_note']  = 'API Пароль';
$_['text_api_signature_note'] = 'API Подпись';
$_['text_sandbox_note'] 	  = 'Тестирование системы в рожиме Sandbox требует наличие Sandbox API доступов';

$_['text_dialog_confirm'] 	  = 'Подтвердите следующие платежи';
$_['text_dialog_ppfee'] 	  = '+ PP комиссия';

$_['text_method_one_receiver'] = 'PayPal Standard используется для одного получателя .';
$_['text_method_mutli_receiver'] = 'PayPal MassPay используется для нескольких получаетей .';
$_['text_paypal_log_filename'] = 'Error Log Filename';

// Error
$_['error_permission']		  = 'Внимание: У вас нет прав изменять PayPal!';
$_['error_admin_info'] 		  = 'Вы должны <a href="%s">задать настройки</a> для этих способов оплаты.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> не задал настройки для этого способа оплаты. Свяжитесь с продавцом.';