<?php

$_['msn_onsite_admin_updated_suborder_status'] = "Статус заказа <a href='%s' class='order'>#%s</a> был изменен на <strong>%s</strong>.";

$_['msn_onsite_customer_created_account'] = "Зарегистрирован новый покупатель: <a href='%s' class='customer'>%s</a>.";
$_['msn_onsite_customer_created_message'] = "Покупатель <a href='%s' class='customer'>%s</a> ответил на сообщение <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "Покупатель <a href='%s' class='customer'>%s</a> совершил новый заказ: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_guest_created_order'] = "Гость разметил новый заказ: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_seller_created_account'] = "Зарегистрирован новый продавец: <a href='%s' class='seller'>%s</a>.";
$_['msn_onsite_seller_updated_invoice_status'] = "Продавец <a href='%s' class='seller'>%s</a> оплатил <a href='%s' class='catalog'><strong>счет</strong></a> <strong>#%s</strong>.";
$_['msn_onsite_seller_created_message'] = "Продавец <a href='%s' class='seller'>%s</a> ответил на сообщение <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_seller_created_product'] = "Продавец <a href='%s' class='seller'>%s</a> опубликовал новый товар <a href='%s' class='product'>%s</a>.";
