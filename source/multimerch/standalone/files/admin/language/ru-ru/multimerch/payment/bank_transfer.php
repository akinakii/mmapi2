<?php
// Heading
$_['heading_title'] 		  = '[MultiMerch] Банковский перевод';

// Text
$_['text_method_name'] 		  = 'Банковский перевод';

$_['text_success']			  = 'Вы успешно изменили настройки [MultiMerch] Банковский перевод';
$_['text_edit']               = 'Изменить [MultiMerch] Банковский перевод';
$_['text_bank_transfer']	  = '<img src="view/image/multiseller/payment/bank_transfer.png" alt="[MultiMerch] Bank Transfer" title="[MultiMerch] Bank Transfer" />';

$_['text_receiver'] 		  = 'Получатель';
$_['text_sender'] 			  = 'Отправитель';

$_['text_fname'] 			  = 'Имя';
$_['text_lname'] 			  = 'Фамилия';
$_['text_bank_name'] 		  = 'Банк';
$_['text_bank_country'] 	  = 'Страна Банка';
$_['text_bic'] 				  = 'BIC';
$_['text_iban'] 			  = 'IBAN';

$_['text_full_name'] 		  = '%s %s';
$_['text_full_bank_name'] 	  = '%s, %s';

// Error
$_['error_permission']		  = 'Ошибка: У вас недостаточно прав чтобы изменить [MultiMerch] Банковский перевод!';
$_['error_email']			  = 'Введите E-Mail!';
$_['error_no_payment_id']	  = 'Ошибка: невозможно подтвердить платеж Банковского перевода !';

$_['error_admin_info'] 		  = 'Вы успешно <a href="%s">изменили настройки</a> для этого способа оплаты.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> вы не заполнили настройки для этого способа оплаты и не будете получать платежи.';