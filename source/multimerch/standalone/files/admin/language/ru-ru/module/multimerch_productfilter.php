<?php

$_['heading_title'] = "MultiMerch-модуль фильтра продуктов";
$_['text_module'] = "Модули";

$_['text_default_config'] = "Стандартная конфигурация";
$_['text_default_config_link'] = "Включить / Отключить блоки в фильтре";
$_['text_status'] = "Статус";

$_['error_status'] = "Вы должны выбрать статус модуля!";
