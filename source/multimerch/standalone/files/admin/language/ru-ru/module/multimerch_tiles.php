<?php

$_['heading_title'] = 'Плитка MultiMerch';
$_['text_module'] = 'Модули';

$_['module_type_grid'] = "Сетка";
$_['module_type_slider'] = "Слайдер";

$_['module_item_type_seller'] = "Продавцы";
$_['module_item_type_product'] = "Товары";
$_['module_item_type_oc_category'] = "Категории торговой площадки";

$_['text_module_name'] = "Имя модуля";
$_['text_title'] = "Заголовок";
$_['text_subtitle'] = "Подзаголовок";
$_['text_link'] = "Ссылка";
$_['text_appearance'] = "Внешний вид";
$_['text_item_type'] = "Тип";
$_['text_items'] = "Элементы";
/***** INSERTED *****/$_['text_items_placeholder'] = "Select items to show";
$_['text_status'] = "Статус";

$_['error_title'] = "Вам нужно указать заголовок (%s)";
$_['error_items'] = "Вам нужно выбрать элементы к показу!";
