<?php

$_['heading_title'] = '[MultiMerch] PayPal MassPay Addon by Multimerch';
$_['ms_config_masspay'] = 'PayPal MassPay';
$_['text_module'] = 'Модули';

$_['ms_config_paypal_api_username'] = 'PayPal API имя пользователя';
$_['ms_config_paypal_api_username_note'] = 'Your PayPal API имя пользователя для MassPay payouts';
$_['ms_config_paypal_api_password'] = 'PayPal API пароль';
$_['ms_config_paypal_api_password_note'] = 'Your PayPal API пароль for MassPay payouts';
$_['ms_config_paypal_api_signature'] = 'PayPal API подпись';
$_['ms_config_paypal_api_signature_note'] = 'Your PayPal API подпись для MassPay payouts';
$_['ms_config_paypal_sandbox'] = 'PayPal Sandbox режим';
$_['ms_config_paypal_sandbox_note'] = 'Использовать PayPal в режиме Sandbox для тестирования';

$_['ms_payment_confirmation'] = 'Подтверждение Платежа';
$_['ms_payment_pay'] = 'Оплатить!';
$_['ms_payment_dialog_markpaid'] = 'Следующие запросы на оплату будут отмечены, как оплаченные';
$_['ms_payment_dialog_confirm'] = 'Подтвердите платежи';
$_['ms_payment_dialog_ppfee'] = '+ комиссия PayPal';

