<?php
$_['heading_title'] = "Stripe Connect";
$_['text_ms_stripe_connect'] = "<a href='//stripe.com/connect'  target='_blank'><img src='view/image/payment/stripe_connect.png' alt='Stripe Connect' title='Stripe Connect' style='border: 1px solid #EEEEEE;' /></a>";

$_['text_payment'] = "Платеж";
$_['text_api_keys_info'] = "Информация для настройки API ключей и Connect доступна в разделах панели управления Stripe <a href='//dashboard.stripe.com/account/apikeys' target='_blank'>API ключи</a> и <a href='//dashboard.stripe.com/account/applications/settings' target='_blank'>настройки Connect</a>";

$_['text_general'] = "Общее";
$_['text_test_mode'] = "Режим тестирования";
$_['text_test_mode_note'] = "В режиме тестирования, платежи не обрабатываются и может быть использована только тестовая информация о платеже";
$_['text_debug'] = "Режим отладки";
$_['text_debug_note'] = "Детальное логгирование событий";
/***** INSERTED *****/$_['text_sca'] = "Require SCA";
/***** INSERTED *****/$_['text_sca_note'] = "As of September 2019, a regulation called Strong Customer Authentication (SCA) requires businesses in Europe to request additional customer authentication for online payments.";

$_['text_status'] = "Статус";
$_['text_account_currency'] = "Валюта";
$_['text_account_currency_note'] = "Валюта аккаунта Stripe";

$_['text_order_status'] = "Статусы заказов";
$_['text_event_charge_status_succeeded'] = "Оплата успешна";
$_['text_event_charge_status_succeeded_note'] = "Статус заказа, оплата которого была успешно обработана Stripe-ом";
$_['text_event_charge_status_failed'] = "Оплата не удалась";
$_['text_event_charge_status_failed_note'] = "Статус заказа, оплата которого не была обработана Stripe-ом";
$_['text_event_charge_status_refunded'] = "Оплата возмещена";
$_['text_event_charge_status_refunded_note'] = "Статус заказа, оплата которого была возмещена";

$_['text_api_settings'] = "Настройки API";
$_['text_public_key'] = "Публичный ключ";
$_['text_public_key_note'] = "Публичный API ключ предназначен исключительно для идентификации Вашей учетной записи с помощью Stripe";
$_['text_secret_key'] = "Секретный ключ";
$_['text_secret_key_note'] = "Секретный API ключ может выполнять любой API запрос на Stripe без ограничений";
$_['text_account_webhooks'] = "Обработчик событий аккаунта";
$_['text_account_webhooks_note'] = "Обработчик различный событий, генерируемых Вашим Stripe аккаунтом";

$_['text_connect_settings'] = "Настройки Stripe Connect";
$_['text_client_id'] = "ID клиента";
$_['text_connect_webhooks'] = "Обработчик событий Connect";
$_['text_connect_webhooks_note'] = "Обработчик различный событий, генерируемых приложениями Connect";
$_['text_signing_secret'] = "Секретная подпись";
$_['text_charging_approach'] = "Подход к оплате";
$_['text_charging_approach_note'] = "<strong>Прямые платежи</strong> позволяют взимать плату с покупателя непосредственно на подключенные аккаунты продавцов и брать комиссию в процессе.<br/><strong>Платежи с назначением</strong> позволяют взимать плату с покупателя на Ваш аккаунт от имени подключенных аккаунтов продавцов и брать комиссию в процессе.<br/><strong>Раздельные платежи и трансферы</strong> позволяет взимать плату с покупателя на Ваш аккаунт от имени подключенных аккаунтов продавцов, сохранять денежные средства на своем аккаунте и отдельно выполнять денежные переводы.<br/>Создание раздельных платежей и переводов поддерживается только в том случае, если Ваш аккаунт и связанный аккаунт продавца находятся в одном регионе: или в Европе, или в США.";
$_['text_charging_approach_direct'] = "Прямые платежи";
$_['text_charging_approach_destination'] = "Платежи с назначением";
$_['text_charging_approach_separate'] = "Раздельные платежи и трансферы";

$_['text_subscription_settings'] = "Подписки";
$_['text_enable_subscription'] = "Включить подписки";
$_['text_stripe_product_id'] = "ID Stripe продукта";
$_['text_stripe_product_id_note'] = "Создайте продукт в панели управления Stripe (Billing > Products) и скопируйте его идентификатор в это поле";
$_['text_retrieve_plans'] = "Получить планы со Stripe";
$_['text_retrieve'] = "Получить";
$_['text_plan_condition_base'] = "%s %s";
$_['text_plan_condition_per_seat'] = "%s за продукт %s";
$_['text_plan_interval_day'] = "Ежедневно";
$_['text_plan_interval_month'] = "Ежемесячно";
$_['text_plan_interval_year'] = "Ежегодно";

$_['error_account_currency'] = "Вы должны выбрать валюту аккаунта";
$_['error_public_key'] = "Вы должны указать 'Публичный ключ'";
$_['error_secret_key'] = "Вы должны указать 'Секретный ключ'";
$_['error_client_id'] = "Вы должны указать 'ID клиента'";
$_['error_signing_secret'] = "Вы должны указать 'Секретную подпись'";
$_['error_product_id'] = "Вам необходимо указать действительный 'ID Stripe-продукта'!";
$_['error_no_currencies_available'] = "Валюты недоступны";
$_['error_no_order_statuses_available'] = "Статусы заказов недоступны";
$_['error_charging_approach'] = "Вы должны выбрать метод оплаты";

$_['success_modified'] = 'Успех: Вы успешно изменили модуль Stripe Connect!';
$_['success_plans_retrieved'] = 'Успех: планы подписок были успешно получены со Stripe!';

