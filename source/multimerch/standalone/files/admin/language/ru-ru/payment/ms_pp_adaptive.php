<?php
// Heading
$_['heading_title'] = '<b>[MultiMerch]</b> PayPal Adaptive Payments for MultiMerch';

$_['text_payment'] = 'Payment';
$_['ppa_adaptive'] = 'PayPal Adaptive';

$_['ppa_api_username'] = 'API Имя пользователя';
$_['ppa_api_username_note'] = 'API Имя пользователя';
$_['ppa_api_password'] = 'API Пароль';
$_['ppa_api_password_note'] = 'API Пароль';
$_['ppa_api_signature'] = 'API Подпись';
$_['ppa_api_signature_note'] = 'API Подпись';
$_['ppa_api_appid'] = 'ID Приложения';
$_['ppa_api_appid_note'] = 'ID Приложения';
$_['ppa_secret'] = 'Общий секрет';
$_['ppa_secret_key'] = 'Ключ';
$_['ppa_secret_value'] = 'Значение';
$_['ppa_secret_note'] = 'Строки которые будут использоваться для IPN валидации. Это может быть всё что угодно';


$_['ppa_payment_type'] = 'Тип платежа';
$_['ppa_payment_type_note'] = "Смотрите <a href='https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_APIntro'>Введение в PayPal Adaptive Payments</a> для деталей";
$_['ppa_payment_type_simple'] = 'Простой (Simple)';
$_['ppa_payment_type_parallel'] = 'Параллельный (Parallel)';
$_['ppa_payment_type_chained'] = 'Последовательный (Chained)';
$_['ppa_payment_type_preapproval'] = 'Предварительное одобрение';

$_['ppa_feespayer'] = 'Плательщик сборов';
$_['ppa_feespayer_note'] = "Смотрите <a href='https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_APIntro'>Введение в PayPal Adaptive Payments</a> для деталей";
$_['ppa_feespayer_sender'] = 'Отправитель (Sender)';
$_['ppa_feespayer_primaryreceiver'] = 'Первичный получатель (Primary receiver)';
$_['ppa_feespayer_eachreceiver'] = 'Каждый получатель (Each receiver)';
$_['ppa_feespayer_secondaryonly'] = 'Вторичный получатель (Secondary only)';

$_['ppa_receiver'] = 'Получатель';
$_['ppa_receiver_note'] = 'PayPal адрес владельца магазина, который будет использован для того, чтобы принимать и обрабатывать платежи';

$_['ppa_invalid_email'] = 'Неправильное операция PayPal аккаунта';
$_['ppa_invalid_email_note'] = 'Что делать если один из продавцов указанать неверный Paypal адрес';
$_['ppa_disable_module'] = 'Отключить Adaptive Payments';
$_['ppa_balance_transaction'] = 'Создать запись в балансе вместо этого';


$_['ppa_sandbox'] = 'Режим Песочницы';
$_['ppa_sandbox_note'] = 'Для теста в режима песочницы требуются учётные данные PayPal Sandbox API.';
$_['ppa_debug'] = 'Режим отладки';
$_['ppa_debug_note'] = 'Логировать детальную информацию в PayPal log';

$_['ppa_total'] = 'Общая сумма';
$_['ppa_total_note'] = 'Общая сумма заказа после которой будет доступен этот способ платежа.';

$_['ppa_status'] = 'Статус';
$_['ppa_completed_status'] = 'Завершенный Статус';
$_['ppa_error_status'] = 'Статус ошибки/неудачи:';
$_['ppa_pending_status'] = 'Статус В ожидании';

$_['ppa_geo_zone'] = 'Гео Зона';
$_['ppa_sort_order'] = 'Порядок сортировки';
$_['text_ppa_log_filename'] = 'Имя файла ошибок';

$_['ppa_error_secondaryonly'] = 'Тип платы плательщика PRIMARYRECEIVER может быть использован только для CHAINED платежей';
$_['ppa_error_senderchained'] = 'Плательщик сборов SENDER не может быть использован для платежей типа CHAINED';
$_['ppa_error_eachchained'] = 'Плата плательщика EACHRECEIVER не может быть использована для CHAINED платежей';
$_['ppa_error_preapproval_eachreceiver'] = 'ПРЕДВАРИТЕЛЬНО ПРИНЯТА оплата может быть только использована с типом платы плательщика КАЖДОГО ПОЛУЧАТЕЛЯ';

$_['ppa_success'] = 'Вы успешно изменили вашу информацию вашего Paypal аккаунта!';
$_['ppa_error_receiver'] = 'Вам необходимо указать первичного получателя (#1)';
$_['ppa_error_credentials'] = 'Вам необходимо указать всего учётные данные API';
$_['ppa_error_secret'] = 'Оба поля Секретный ключ и Значение обязательны для заполнения';