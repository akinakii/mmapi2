<?php

// Heading
$_['heading_title']    = 'الشحن الإجمالي لـ [MultiMerch]';

// Text
$_['text_success']     = 'لقد قمت بتعديل الشحن الإجمالي لـ [MultiMerch] بنجاح!';
$_['text_edit']        = 'تعديل الشحن الإجمالي لـ [MultiMerch]';

// Entry
$_['entry_status']     = 'الحالة';
$_['entry_sort_order'] = 'ترتيب الفرز';

// Error
$_['error_permission'] = 'تحذير: ليس لديك صلاحية تعديل ترتيب فرز الشحن الإجمالي لـ [MultiMerch]!';