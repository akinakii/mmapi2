<?php

// Heading
$_['heading_title']    = 'قسائم تخفيض [MultiMerch]';

// Text
$_['text_success']     = 'لقد قمت بتعديل قسائم تخفيض [MultiMerch] بنجاح!';
$_['text_edit']        = 'تعديل قسائم تخفيض [MultiMerch]';

// Entry
$_['entry_status']     = 'الحالة';
$_['entry_sort_order'] = 'ترتيب الفرز';

// Error
$_['error_permission'] = 'تحذير: ليس لديك الصلاحية لتعديل ترتيب فرز قسائم خصم [MultiMerch]!';