<?php

// General
$_['ms_enabled'] = 'مفعل';
$_['ms_disabled'] = 'معطل';
$_['ms_apply'] = 'تطبيق';
$_['ms_type'] = 'النوع';
$_['ms_type_checkbox'] = 'صندوق اختيار';
$_['ms_type_date'] = 'تاريخ';
$_['ms_type_datetime'] = 'تاريخ ووقت';
$_['ms_type_file'] = 'ملف';
$_['ms_type_image'] = 'صورة';
$_['ms_type_radio'] = 'زر اختيار';
$_['ms_type_select'] = 'قائمة منسدلة';
$_['ms_type_text'] = 'نص (سطر واحد)';
$_['ms_type_textarea'] = 'حقل نصي (متعدد الأسطر)';
$_['ms_type_time'] = 'وقت';
$_['ms_type_choose'] = 'اختيار';
$_['ms_type_input'] = 'ادخال';
$_['ms_store'] = 'المتجر';
$_['ms_store_default'] = 'الافتراضي';
$_['ms_id'] = '#';
$_['ms_login_as_vendor'] = '<a href="%s">الدخول كبائع</a> لتعديل معلومات الشحن';
$_['ms_sort_order'] = 'ترتيب الفرز';

$_['ms_default_select_value'] = '-- لا شيء --';
$_['ms_placeholder_deleted'] = "*محذوف*";
$_['ms_seller_deleted'] = "*تم حذف البائع*";
$_['ms_product_deleted'] = "*تم حذف المنتج*";

$_['ms_button_approve'] = 'الموافقة';
$_['ms_button_decline'] = 'الرفض';

$_['ms_fixed_coupon_warning'] = "<b>تحذير:</b> لا يمكن تطبيق قسائم التخفيض ذات المبلغ الثابت على نظام تعدد البائعين [MultiMerch] مما يؤدي إلى حساب عمولات البائعين بشكل غير صحيح. القسائم ذات النسبة المئوية تعمل بشكل جيد ويمكن استخدامها.";
$_['ms_voucher_warning'] = "<b>تحذير:</b> لا يمكن تطبيق قسائم الهدايا على نظام تعدد البائعين [MultiMerch] مما يؤدي إلى حساب عمولات البائعين بشكل غير صحيح!";
$_['ms_error_directory'] = "تحذير: فشل إنشاء المسار: %s. يرجى إنشاءه يدوياً ووضعه كقابل للكتابة من قبل الخادم قبل المتابعة. <br />";
$_['ms_error_directory_notwritable'] = "تحذير: هذا المسار موجود مسبقاً ولكنه غير قابل للقراءة: %s. يرجى التأكد من كونه فارغاً ووضعه كقابل للكتابة من قبل الخادم قبل المتابعة. <br />";
$_['ms_error_directory_exists'] = "تحذير: هذا المسار موجود مسبقاً: %s. يرجى التأكد من كونه فارغاً قبل المتابعة. <br />";
$_['ms_error_product_publish'] = 'فشل نشر بعض المنتجات: لم يتم تنشيط حساب البائع.';
$_['ms_success_installed'] = 'تم تثبيت الإضافة بنجاح';
$_['ms_success_product_status'] = 'تم تغيير حالة المنتج بنجاح.';

$_['ms_db_upgrade'] = ' <a href="%s">اضغط هنا</a> لتحديث قاعدة بيانات MultiMerch Marketplace خاصتك إلى أحدث إصدار (%s -> %s) .';
$_['ms_files_upgrade'] = ' تحذير: هذه النسخة من ملفات MultiMerch (%s) أقدم من الإصدار المطلوب بواسطة بنية قاعدة البيانات الخاصة بك (%s). قد يحدث هذا بسبب تحميلك لإصدار قديم من MultiMerch على أصدار أحدث. الرجاء تحديث ملفات MultiMerch أو إعادة تثبيت MultiMerch';
$_['ms_db_success'] = 'قاعدة بيانات MultiMerch Marketplace الخاصة بك محدثة إلى آخر إصدار!';
$_['ms_db_latest'] = 'قاعدة بيانات MultiMerch Marketplace الخاصة بك حالياً محدثة إلى آخر إصدار!';
$_['ms_multimerch_not_installed'] = 'تحذير: لم يتم تثبيت MultiMerch!';

$_['ms_error_php_version'] = ' يتطلب ملتي ميرش PHP 7.0 أو أحدث. الرجاء التواصل مع مزود الإستضافة لتحديث إصدار الـ PHP أو قم بتحديثة بنفسك من خلال وحدة التحكم الخاصة بالإستضافة > إختر إصدار الـ PHP version';

$_['heading_title'] = '<b>[MultiMerch]</b> المتجر الرقمي';
$_['text_no_results'] = 'لا نتائج';
$_['error_permission'] = 'تحذير: ليس لديك الصلاحية لتعديل الموديول!';

$_['ms_error_withdraw_response'] = 'خطأ: ليس هناك رد';
$_['ms_error_withdraw_status'] = 'خطأ: العملية غير ناجحة';
$_['ms_success'] = 'نجاح';
$_['ms_success_transactions'] = 'تم إكمال العمليات بنجاح';
$_['ms_success_payment_deleted'] = 'تم حذف الدفعة';
$_['text_success']                 = 'لقد قمت بتعديل الإعدادات بنجاح!';

$_['ms_none'] = 'لا أحد';
$_['ms_seller'] = 'البائع';
$_['ms_all_sellers'] = 'كل البائعين';
$_['ms_balance'] = 'الرصيد';
$_['ms_amount'] = 'المبلغ';
$_['ms_product'] = 'المنتج';
$_['ms_quantity'] = 'الكمية';
$_['ms_sales'] = 'المبيعات';
$_['ms_price'] = 'السعر';
$_['ms_net_amount'] = 'صافي المبلغ';
$_['ms_from'] = 'من';
$_['ms_to'] = 'إلى';
$_['ms_paypal'] = 'PayPal';
$_['ms_date_created'] = 'تاريخ الإنشاء';
$_['ms_status'] = 'الحالة';
$_['ms_date_modified'] = 'تاريخ التعديل';
$_['ms_date_paid'] = 'تاريخ الدفع';
$_['ms_date_last_paid'] = 'تاريخ آخر دفع';
$_['ms_date'] = 'التاريخ';
$_['ms_description'] = 'الوصف';
$_['ms_confirm'] = 'تأكيد';
$_['ms_total'] = 'الإجمالي';
$_['ms_method'] = 'الوسيلة';

$_['ms_commission'] = 'العمولة';
$_['ms_commissions_fees'] = 'الرسوم';

$_['ms_store_settings'] = 'إعدادات المتجر';
$_['ms_seller_full_name'] = "الإسم كاملاً";
$_['ms_seller_address'] = "العنوان";
$_['ms_seller_address1'] = "العنوان 1";
$_['ms_seller_address1_placeholder'] = 'الشارع, صندوق البريد, اسم الشركة... الخ';
$_['ms_seller_address2'] = "العنوان 2";
$_['ms_seller_address2_placeholder'] = 'الشقة, الجناح, الوحدة, البناية, الطابق...الخ';
$_['ms_seller_city'] = "المدينة";
$_['ms_seller_state'] = "الولاية/المحافظة/المنطقة";
$_['ms_seller_zip'] = "الرمز البريدي";
$_['ms_seller_country'] = "الدولة";
$_['ms_seller_company'] = 'الشركة';

$_['ms_catalog_sellerinfo_information'] = 'المعلومات';
$_['ms_seller_website'] = 'الموقع الإلكتروني';
$_['ms_seller_phone'] = 'رقم الهاتف';
$_['ms_seller_error_deleting'] = 'حدث خطأ أثناء حذف البائع!';
$_['ms_seller_success_deleting'] = 'تم حذف البائع بنجاح!';

$_['ms_commission_' . MsCommission::RATE_SALE] = 'رسوم البيع';
$_['ms_commission_' . MsCommission::RATE_LISTING] = 'رسوم النشر / الطريقة';
$_['ms_commission_' . MsCommission::RATE_SIGNUP] = 'رسوم التسجيل / الطريقة';

$_['ms_commission_short_' . MsCommission::RATE_SALE] = 'بيع';
$_['ms_commission_short_' . MsCommission::RATE_LISTING] = 'نشر';
$_['ms_commission_short_' . MsCommission::RATE_SIGNUP] = 'تسجيل';
$_['ms_commission_actual'] = 'الرسوم الحالية';

$_['ms_name'] = 'الإسم';
$_['ms_config_width'] = 'العرض';
$_['ms_config_height'] = 'الإرتفاع';
$_['ms_description'] = 'الوصف';

$_['ms_enable'] = 'تفعيل';
$_['ms_disable'] = 'تعطيل';
$_['ms_create'] = 'إنشاء';
$_['ms_delete'] = 'حذف';
$_['ms_uninstall'] = 'الغاء التثبيت';
$_['ms_remove'] = 'إزالة';
$_['ms_view_in_store'] = 'العرض في المتجر';
$_['ms_view'] = 'عرض';
$_['ms_add'] = 'إضافة';
$_['ms_back'] = 'العودة';
$_['ms_enable_at_registration'] = 'السماح عند التسجيل';

$_['ms_button_pay'] = 'دفع';

$_['ms_logo'] = 'الشعار';
$_['ms_field'] = 'حقل';

// Menu
$_['ms_menu_multiseller'] = 'ملتي ميرش';
$_['ms_menu_dashboard'] = 'لوحة المراقبة';
$_['ms_menu_sellers'] = 'البائعون';
$_['ms_menu_seller_groups'] = 'مجموعات البائعين';
$_['ms_menu_seller_properties'] = 'خصائص البائع';
$_['ms_menu_catalog'] = 'الواجهة';
$_['ms_menu_attributes'] = 'المواصفات';
$_['ms_menu_categories'] = 'الأقسام';
$_['ms_menu_options'] = 'الخيارات';
$_['ms_menu_products'] = 'المنتجات';
$_['ms_menu_imports'] = 'الاستيرادات';
$_['ms_menu_custom_fields'] = 'حقول مخصصة';
$_['ms_menu_orders'] = 'الطلبات';
$_['ms_menu_finances'] = 'الحسابات';
$_['ms_menu_payment'] = 'المدفوعات';
$_['ms_menu_invoice_seller'] = 'فواتير البائع';
$_['ms_menu_invoice'] = 'الفواتير';
$_['ms_menu_payment_gateway'] = 'وسائل الدفع';
$_['ms_menu_payment_gateway_tab_seller'] = 'بوابة دفع البائع';
$_['ms_menu_payment_gateway_tab_marketplace'] = 'بوابة دفع المتجر';
$_['ms_menu_payment_gateway_tab_customer'] = 'بوابة دفع العميل';
$_['ms_menu_payment_gateway_settings'] = 'إعدادات وسيلة الدفع';
$_['ms_menu_payout'] = 'المستحقات';
$_['ms_menu_payout_generate'] = 'توليد';
$_['ms_menu_payout_view'] = 'عرض';
$_['ms_menu_transactions'] = 'العمليات';
$_['ms_menu_conversations'] = 'المحادثات';
$_['ms_menu_complaints'] = 'الشكاوى';
$_['ms_menu_reviews'] = 'التقييمات';
$_['ms_menu_questions'] = 'الأسئلة';
$_['ms_menu_shipping_method'] = 'وسائل الشحن';
$_['ms_menu_event'] = 'سجل الأحداث';
$_['ms_menu_settings'] = 'الإعدادات';
$_['ms_menu_install'] = 'تحميل';
$_['ms_menu_marketplace'] = 'المتجر';
$_['ms_menu_system'] = 'النظام';
$_['ms_menu_coupon'] = 'قسائم التخفيض';
$_['ms_menu_notification'] = 'الإشعارات';
$_['ms_menu_zones'] = 'المناطق';

$_['ms_menu_reports'] = 'التقارير';
$_['ms_menu_reports_sales'] = 'المبيعات';
$_['ms_menu_reports_sales_list'] = 'قائمة المبيعات';
$_['ms_menu_reports_sales_by_day'] = 'المبيعات اليومية';
$_['ms_menu_reports_sales_by_month'] = 'المبيعات الشهرية';
$_['ms_menu_reports_sales_by_product'] = 'المبيعات حسب المنتج';
$_['ms_menu_reports_sales_by_seller'] = 'المبيعات حسب البائع';
$_['ms_menu_reports_sales_by_customer'] = 'المبيعات حسب العميل';
$_['ms_menu_reports_finances'] = 'الحسابات';
$_['ms_menu_reports_finances_transactions'] = 'العمليات';
$_['ms_menu_reports_finances_seller'] = 'أرصدة البائعين';
$_['ms_menu_reports_finances_payouts'] = 'المستحقات';
$_['ms_menu_reports_finances_payments'] = 'المدفوعات';

$_['ms_menu_customers'] = 'العملاء';

// Settings
$_['ms_settings_heading'] = 'الإعدادات';
$_['ms_settings_breadcrumbs'] = 'الإعدادات';
$_['ms_config_seller_validation'] = 'التحقق من البائع';
$_['ms_config_seller_validation_note'] = 'إذا تم تفعيله فإن حسابات البائعين التي يتم إنشاءها سيتم وضعها كغير نشط إلى أن تتم الموافقة عليها بواسطة الإدارة';
$_['ms_config_seller_validation_none'] = 'بدون تحقق';
$_['ms_config_seller_validation_activation'] = 'التفعيل عبر البريد الإلكتروني';
$_['ms_config_seller_validation_approval'] = 'الموافقة اليدوية';

$_['ms_config_seller_landing_page'] = "الصفحة المخصصة للبائع الجديد";
$_['ms_config_seller_landing_page_note'] = "اختر صفحة ليستخدمها MultiMerch كصفحة مخصصة للبائعين المستقبليين";

$_['ms_error_htaccess'] = 'ملتي ميرش SEO يتطلب أن يكون ملف .htaccess مفعل. الرجاء إعادة تسمية ملف '. $_SERVER['DOCUMENT_ROOT'] .'/.htaccess.txt إلى '. $_SERVER['DOCUMENT_ROOT'] .'/.htaccess في مسار ملفات التثبيت وتأكد أن الخادم مضبوط ليدعم  mod_rewrite.';
$_['ms_error_htaccess_txt'] = 'ملتي ميرش SEO يطلب أن يكون ملف .htaccess مفعل. الرجاء نسخ ملف htaccess.txt من الأرشيف إلى مسار ملفات المتجر الخاص بك '. $_SERVER['DOCUMENT_ROOT'] .', إعد تسميته إلى .htaccess وتأكد أن الخادم مضبوط ليدعم mod_rewrite.';

$_['ms_settings_error_vendor_shipping_methods'] = 'يرجى <a target="_blank" href="%s">إدخال وسيلة شحن واحدة على الأقل</a> لكي تعمل وسائل شحن البائعين بشكل صحيح!';
$_['ms_settings_error_vendor_shipping_times'] = 'يرجى إدخال مدة تسليم واحدة على الأقل لكي تعمل وسائل شحن البائعين بشكل صحيح!';
$_['ms_settings_error_vendor_duplicate_seo_slug'] = 'يرجى تحديد كلمات SEO مختلفة للبائعين والمنتجات!';
$_['ms_settings_error_vendor_seo_slug_required'] = "كلمات الـ SEO الدلالية يجب أن لا تكون فارغة!";

$_['ms_config_general'] = 'عام';
$_['ms_config_limits'] = 'الحدود';
$_['ms_config_file_types'] = 'أنواع الملفات';
$_['ms_config_shipping'] = 'الشحن';
$_['ms_config_product_fields'] = 'حقول نموذج المنتج';

$_['ms_config_product_validation'] = 'التحقق من المنتج';
$_['ms_config_product_validation_note'] = "'بدون تحقق' يعني أن المنتجات الجديدة التي ينشئها البائعون ستكون نشطة مباشرةً بينما 'الموافقة اليدوية' تعني أن المنتجات لن تكون نشطة إلى بعد موافقة الإدارة";
$_['ms_config_product_validation_from_group_settings'] = 'من إعدادات المجموعة';
$_['ms_config_product_validation_none'] = 'بدون تحقق';
$_['ms_config_product_validation_approval'] = 'الموافقة اليدوية';


$_['ms_config_allow_free_products'] = 'السماح بالمنتجات المجانية';

$_['ms_config_allow_digital_products'] = 'السماح بالمنتجات الرقمية';
$_['ms_config_allow_digital_products_note'] = "إذا تم تفعيله فسيتمكن البائعون من إنشاء منتجات لا تحتاج إلى شحن";

$_['ms_config_minmax_product_price'] = 'الحد الأدنى والأعلى للسعر';
$_['ms_config_minmax_product_price_note'] = 'الحد الأدنى والأعلى لسعر المنتج (0 تعني بدون حدود)';

$_['ms_config_msf_attributes_system'] = "نظام مواصفات المنتج";
$_['ms_config_msf_attributes_system_oc'] = "مواصفات أوبن كارت";
$_['ms_config_msf_attributes_system_msf'] = "مواصفات ملتي ميرش";
$_['ms_config_msf_attributes_system_note'] = "إختر نظام الخيارات التي تود إستخدامه في متجرك:<BR><strong>خيارات ملتي ميرش</strong> قد تكون بأنواع مختلفة ويمكن ربطها بأقسام المتجر<BR><strong>خيارات أوبن كارت</strong> نصية فقط وعامة على المتجر باكمله , ولا يمكن ربطها بالأقسام.";

$_['ms_config_msf_variations'] = "إختلافات المنتج";
$_['ms_config_msf_variations_enable'] = "فعل إختلافات المنتج";
$_['ms_config_msf_variations_enable_note'] = "فعل ملاحظة إختلاف المنتج";
$_['ms_config_msf_variations_system'] = "نظام إختلاف المنتج";
$_['ms_config_msf_variations_system_oc'] = "خياريات أوبن كارت";
$_['ms_config_msf_variations_system_msf'] = "إختلافات ملتي ميرش";
$_['ms_config_msf_variations_system_note'] = "قم بإختيار نظام إختلاف المنتج التي تود إستخدامه في متجرك:<BR><strong>خيارات ملتي ميرش</strong> تسمح للبائعين إنشاء منتجات متعددة المتغيرات ويمكن ربطها بأقسام المتجر<BR><strong>خيارات أوبن كارت</strong> تسمع للبائعين بتعديل السعر الأساسي للمنتج, وهي عامة ولا يمكن ربطها بالأقسام.";

$_['ms_config_msf_seller_properties'] = "خصائص البائع";
$_['ms_config_msf_seller_properties_enable_custom'] = "فعل الخصائص القابلة للتفصيل";
$_['ms_config_msf_seller_properties_note'] = "فعل إنشاء خصائص البائع القابلة للتفصيل بالإضافة إلى الحقول الإفتراضية";
$_['ms_config_msf_seller_properties_manage'] = "إدارة الخصائص";

$_['ms_config_custom_fields'] = "حقول قابلة للتفصيل";
$_['ms_config_custom_fields_enable'] = "فعل حقول ملتي ميرش القابلة للتفصيل (deprecated)";

$_['ms_config_product_attributes_options'] = 'المواصفات والخصائص';
$_['ms_config_allow_attributes'] = 'السماح للبائعين بإنشاء مواصفات';
$_['ms_config_allow_attributes_note'] = 'السماح للبائعين بإنشاء مواصفات خاصة بهم بالإضافة إلى المواصفات الخاصة بالمتجر';
$_['ms_config_allow_options'] = 'السماح للبائعين بإنشاء خيارات';
$_['ms_config_allow_options_note'] = 'السماح للبائعين بإنشاء خيارات خاصة بهم بالإضافة إلى الخيارات الخاصة بالمتجر';
$_['ms_config_allowed_option_types'] = 'أنواع الخيارات المسموح بها';
$_['ms_config_allowed_option_types_note'] = 'السماح للبائعين بإنشاء خيارات وفق الأنواع المحددة أعلاه';
$_['ms_config_option_type_select'] = 'قائمة منسدلة';
$_['ms_config_option_type_radio'] = 'زر اختيار';
$_['ms_config_option_type_checkbox'] = 'صندوق اختيار';
$_['ms_config_option_type_text'] = 'نص (سطر واحد)';
$_['ms_config_option_type_textarea'] = 'حق نصي (عدة أسطر)';
$_['ms_config_option_type_file'] = 'ملف';
$_['ms_config_option_type_date'] = 'تاريخ';
$_['ms_config_option_type_time'] = 'وقت';
$_['ms_config_option_type_datetime'] = 'تاريخ ووقت';

$_['ms_config_product_questions'] = 'أسئلة المنتج';
$_['ms_config_allow_question'] = 'السماح بالأسئلة';
$_['ms_config_allow_question_note'] = 'السماح للعملاء بطرح أسئلة في صفحة المنتج';

$_['ms_config_allowed_image_types'] = 'امتدادات الصور المسموح بها';
$_['ms_config_allowed_image_types_note'] = 'امتدادات الصور المسموح بها';

$_['ms_config_images_limits'] = 'حدود صور المنتج';
$_['ms_config_images_limits_note'] = 'أدنى وأعلى عدد للصور - شاملاُ للصورة الأساسية - المطلوبة/المسموحة لكل منتج (0 = بلا حدود)';

$_['ms_config_downloads_limits'] = 'حدود تنزيلات المنتج';
$_['ms_config_downloads_limits_note'] = 'أدنى وأعلى عدد من ملفات التنزيل المطلوبة/المسموحة لكل منتج (0 = بلا حدود)';

$_['ms_config_allowed_download_types'] = 'امتدادات ملفات التنزيل المسموحة';
$_['ms_config_allowed_download_types_note'] = 'امتدادات ملفات التنزيل المسموحة بها';

$_['ms_config_paypal_sandbox'] = 'وضع PayPal Sandbox';
$_['ms_config_paypal_sandbox_note'] = 'استخدام PayPal في وضع Sandbox للاختبار وتصحيح الأخطاء';

$_['ms_config_paypal_address'] = 'حساب PayPal';
$_['ms_config_paypal_address_note'] = 'حساب PayPal الخاص بك لرسوم التسجيل والنشر';


$_['ms_config_product_categories'] = 'الأقسام';
$_['ms_config_allow_seller_categories'] = 'السماح بأقسام البائع';
$_['ms_config_allow_seller_categories_note'] = 'سيتمكن البائعون عند تفعيله بإنشاء مجموعات منتجات خاصة بهم منفصلة عن أقسام المتجر الرئيسية';
$_['ms_config_allow_multiple_categories'] = 'السماح بتعدد الأقسام';
$_['ms_config_allow_multiple_categories_note'] = 'السماح للبائعين بإضافة المنتج الواحد إلى أكثر من مجموعة';
$_['ms_config_enforce_childmost_categories'] = 'بسط إختيار الأقسام والعرض';
$_['ms_config_enforce_childmost_categories_note'] = 'منع البائعين من إضافة منتجات في أقسام غير القسم الأصلي لتبسيط إدارة المتجر بأقسم ذو مستويات متعددة. هذا الخيار يسمح أيضاً بإظهار المنتجات في الأقسام الغير أصلية من نفس المستوى للعملاء.';
$_['ms_config_restrict_categories'] = 'المجموعات الغير مسموح بها';
$_['ms_config_restrict_categories_note'] = '<u>منع</u> البائعين من نشر منتجاتهم في هذه المجموعات';

$_['ms_config_product_included_fields'] = 'حقول نموذج المنتج';
$_['ms_config_product_included_fields_note'] = 'اختر الحقول التي تريدها أن تظهر للبائعين عند إنشاء أو تعديل المنتجات';

$_['ms_config_seller_included_fields'] = 'الحقول الإفتراضية';
$_['ms_config_seller_included_fields_note'] = 'حدد البيانات التي يجب على البائع تعبئتها عند التسجيل وهل بإمكانه تغييرها في صفحته الشخصية والإعدادات';
$_['ms_config_seller_field_account_info'] = 'معلومات الحساب';
$_['ms_config_seller_field_firstname'] = 'الاسم الأول';
$_['ms_config_seller_field_lastname'] = 'الاسم الأخير';
$_['ms_config_seller_field_email'] = 'البريد الإلكتروني';
$_['ms_config_seller_field_password'] = 'كلمة المرور';
$_['ms_config_seller_field_store_info'] = 'معلومات المتجر';
$_['ms_config_seller_field_storename'] = 'اسم المتجر';
$_['ms_config_seller_field_slogan'] = 'الشعار النصي';
$_['ms_config_seller_field_description'] = 'الوصف';
$_['ms_config_seller_field_website'] = 'الموقع الالكتروني';
$_['ms_config_seller_field_company'] = 'الشركة';
$_['ms_config_seller_field_phone'] = 'رقم الهاتف';
$_['ms_config_seller_field_logo'] = 'الشعار';
$_['ms_config_seller_field_banner'] = 'الغلاف';
$_['ms_config_seller_field_address_info'] = 'معلومات العنوان';
$_['ms_config_seller_field_fullname'] = 'الاسم الرباعي';
$_['ms_config_seller_field_address_1'] = 'العنوان 1';
$_['ms_config_seller_field_address_2'] = 'العنوان 2';
$_['ms_config_seller_field_city'] = 'المدينة';
$_['ms_config_seller_field_state'] = 'الولاية/المحافظة';
$_['ms_config_seller_field_zip'] = 'الرمز البريدي';
$_['ms_config_seller_field_country'] = 'الدولة';

$_['ms_config_seller_terms_page'] = 'شروط حساب البائع';
$_['ms_config_seller_terms_page_note'] = 'يجب على البائعين الموافقة على هذه الشروط عند إنشاء حساب بائع.';


$_['ms_config_finances'] = 'الحسابات';
$_['ms_config_miscellaneous'] = 'متنوعة';
$_['ms_config_deprecated'] = 'منتهية';
$_['ms_config_see_deprecated'] = 'مشاهدة الإعدادات المنتهية';

// MM > Settings > Seller accounts
$_['ms_config_tab_sellers'] = 'حساب البائع';

// MM > Settings > Product publishing
$_['ms_config_tab_products'] = 'نشر المنتج';

// MM > Settings > Miscellaneous
$_['ms_config_misc_wishlist'] = "قائمة الأمنيات";
$_['ms_config_misc_wishlist_enable'] = "تفعيل قائمة الأمنيات الخاصة بملتي ميرش";
$_['ms_config_misc_wishlist_enable_note'] = "قم بإختيار نظام الأمنيات لعملائك. نعم سيقوم بتفعيل قائمة الأمنيات الخاصة بملتي ميرش, لا سيعتمد على قائمة الأمنيات الخاصة بـ أوبن كارت";

$_['ms_config_favorite_sellers'] = "البائعين المفضلين";
$_['ms_config_favorite_sellers_allow'] = "قم بتفعيل البائعين المفضلين";
$_['ms_config_favorite_sellers_allow_note'] = "إسمح للعملاء بمتابعة البائعين وإضافتهم للمفضلة";


// MM > Settings > Updates and licensing
$_['ms_config_updates'] = 'التحديثات';
$_['ms_config_updates_license_info'] = 'ترخيص وتحديثات ملتي ميرش';
$_['ms_config_updates_license_key'] = 'مفتاح الترخيص';
$_['ms_config_updates_license_key_note'] = "يرجى كتابة مفتاح ترخيص ملتي ميرش الذي يوجد في <a href='https://multimerch.com/' target='_blank'>حساب ملتي ميرش</a>";
$_['ms_config_updates_license_activate'] = 'تنشيط';
$_['ms_config_updates_updates'] = 'التحديثات';
$_['ms_config_updates_updates_check'] = 'التحديثات';
$_['ms_config_updates_updates_not_activated'] = 'يرجى تنشيط الترخيص الخاص بك لاستلام المعلومات الخاصة بتحديثات MultiMerch الجديدة!';
$_['ms_license_error_no_key'] = 'خطأ: لم يتم تحديد مفتاح الترخيص!';
$_['ms_license_success_activated'] = 'تم تنشيط نسخة ملتي ميرش الخاصة بك!';
$_['ms_update_error_license'] = 'خطأ: لم يتم تنشيط ملتي ميرش!';
$_['ms_update_success_no_updates'] = 'لديك آخر إصدار من ملتي ميرش! (%s)';
$_['ms_update_success_available_update'] = 'تحديثات ملتي ميرش متاحة! (%s -> %s)';
$_['ms_api_error'] = 'خطأ: %s.';
$_['ms_api_error_license_generic'] = "لم  نتمكن من تنشيط مفتاح الترخيص! يرجى التواصل مع فريق عمل ملتي ميرش للدعم عبر الرابط <a href='https://multimerch.com/' target='_blank'>https://multimerch.com/</a>";
$_['ms_api_error_license_connection'] = "فشل الإتصال بخادم التنشيط! يرجى التأكد من أن الخادم الخاص بك لا يقوم بحظر الطلبات الخارجية";
$_['ms_api_error_license_invalid'] = 'الترخيص غير صالح';
$_['ms_api_error_license_missing'] = 'الترخيص غير موجود';
$_['ms_api_error_license_not_activable'] = 'لا يمكن تنشيط الترخيص';
$_['ms_api_error_license_revoked'] = 'تم إلغاء مفتاح الترخيص';
$_['ms_api_error_no_activations_left'] = 'لا توجد عمليات تنشيط متبقية';
$_['ms_api_error_license_expired'] = "هذا الترخيص قد أنتهى! الرجاء التواصل مع فريق  ملتي ميرش عبر الرابط <a href='https://multimerch.com/' target='_blank'>https://multimerch.com/</a> من أجل الدعم والتجديد";
$_['ms_api_error_key_mismatch'] = 'مفتاح الترخيص غير متطابق';
$_['ms_api_error_item_id'] = 'معرف الترخيص غير صالح';
$_['ms_api_error_item_name'] = 'اسم العنصر غير متطابق';
$_['ms_api_error_no_site'] = 'اسم الدومين غير متطابق';
$_['ms_api_error_unrecognized'] = 'خطأ غير معروف';
$_['ms_api_error_request'] = 'خطأ: حدث خطأ في API - %s.';
$_['ms_api_error_incorrect_response'] = 'خطأ: استجابة الخادم غير صحيحة!';

// MM > Settings > Shipping
$_['ms_config_shipping'] = 'الشحن';
$_['ms_config_shipping_methods'] = 'وسائل شحن البائع';
$_['ms_config_shipping_methods_manage'] = 'إدارة وسائل شحن البائع';
$_['ms_config_shipping_type'] = 'نوع الشحن';
$_['ms_config_enable_store_shipping'] = 'الشحن عبر المتجر';
$_['ms_config_enable_vendor_shipping'] = 'الشحن عبر البائع';
$_['ms_config_disable_shipping'] = 'تعطيل';
$_['ms_config_shipping_type_note'] = "الشحن عبر البائع يستخدم نظام الشحن الأساسي في  أوبن كارت وإضافات الشحن الاعتيادية بينما الشحن عبر البائع يقوم بتفعل نظام شحن ملتي ميرش متعدد البائعين حيث يسمح للبائعين بتحديد رسوم الشحن الخاصة بمنتجاتهم.\nالخيار تعطيل سيقوم بتعطيل الشحن تماماً ولا يسمح سوى بالمنتجات الرقمية في  ملتي ميرش.";
$_['ms_config_shipping_delivery_times'] = 'أوقات تسليم البائع';
$_['ms_config_shipping_delivery_time_add_btn'] = '+ إضافة زمن تسليم';
$_['ms_config_shipping_delivery_time_comment'] = 'انقر مزدوجاً على الخلية لتعديلها';
$_['ms_config_shipping_delivery_times_note'] = 'حدد مجموعة من أوقات الاستلام التي ستكون متاحة للبائعين للاختيار منها عند ضبطهم لإعدادات الشحن. مثلاً: 24 ساعة، 2-3 أيام، ...إلخ.';

$_['ms_config_vendor_shipping_type'] = 'نوع شحن البائع';
$_['ms_config_vendor_shipping_combined'] = 'شحن تجميعي';
$_['ms_config_vendor_shipping_per_product'] = 'شحن بالمنتج';
$_['ms_config_vendor_shipping_both'] = 'كلاهما';
$_['ms_config_vendor_shipping_type_note'] = 'خيار \'الشحن التجميعي\' يسمح للبائع بعمل قواعد للشحن التجميعي فقط. خيار \'الشحن بالمنتج\' يسمح له بعمل قواعد للشحن عبر المنتج فقط. \'كلاهما\' يسمح له بعمل قواعد \'شحن تجميعي\' وكذلك \'شحن بالمنتج\'';

// MM > Settings > Orders
$_['ms_config_orders'] = "الطلبات";
$_['ms_config_order_states'] = "حالات <span class='ms-order-status-color'>طلب العميل</span> (OpenCart)";
$_['ms_config_order_states_note'] = <<<EOT
هذه الإعدادات تقوم بربط <span class='ms-order-status-color'>حالات الطلب</span> الخاصة بـOpenCart بحالات الطلب المنطقية (مثلاً: غير مدفوع، مدفوع، قيد المعالجة، مرسل، ملغي) وكيفية تعامل MultiMerch مع <span class='ms-order-status-color'>طلبات العملاء</span> تبعاً لحالاتها.<BR><BR>
على سبيل المثال، يؤدي تعيين الحالة إلى قيد الانتظار إلى معاملة الطلبات التي تحمل هذه الحالة على أنها غير مدفوعة (غير مكتملة) لمنع البائعين من إرسال المنتجات قبل أن يكمل العميل عملية الدفع.
EOT;
$_['ms_config_order_status_autocomplete'] = '(اكمال تلقائي)';
$_['ms_config_order_state_' . MsOrderData::STATE_PENDING] = 'قيد الانتظار';
$_['ms_config_order_state_' . MsOrderData::STATE_PROCESSING] = 'قيد المعالجة';
$_['ms_config_order_state_' . MsOrderData::STATE_COMPLETED] = 'مكتمل';
$_['ms_config_order_state_' . MsOrderData::STATE_FAILED] = 'فشل';
$_['ms_config_order_state_' . MsOrderData::STATE_CANCELLED] = 'ملغي';
$_['ms_config_order_state_note_' . MsOrderData::STATE_PENDING] = "تشير حالة قيد الانتظار إلى الطلبات التي تم إنشاءها ولكن لم يتم دقع قيمتها بعد. يتيح ذلك لمالك المتجر (والبائعين كذلك) تعليق الطلب حتى يقوم العميل بالدفع.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_PROCESSING] = "تشير حالة قيد المعالجة إلى الطلبات التي تم إنشاءها ولكن لم يتم دقع قيمتها بعد. يتيح ذلك لمالك المتجر (والبائعين كذلك) تعليق الطلب حتى يقوم العميل بالدفع.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_COMPLETED] = "تشير حالة الاكتمال إلى الطلبات التي تم دفعها بالكامل مما يسمح لمالك المتجر (وكذلك البائعين) بالبدء في إرسال المنتجات.";
$_['ms_config_order_state_note_' . MsOrderData::STATE_FAILED] = "ملاحظة حالة فشل أوبن كارت ";
$_['ms_config_order_state_note_' . MsOrderData::STATE_CANCELLED] = "ملاحظة حالة الغاء أوبن كارت";

$_['ms_config_suborder_states'] = "حالات <span class='ms-suborder-status-color'>طلب البائع</span> (ملتي ميرش)";
$_['ms_config_suborder_states_note'] = <<<EOT
هذه الإعدادات تقوم بربط <span class='ms-suborder-status-color'>حالات طلب البائع</span> الخاصة ملتي ميرش بحالات الطلب المنطقية (مثلاً: غير مدفوع، مدفوع، قيد المعالجة، مرسل، ملغي) وكيفية تعامل ملتي ميرش مع <span class='ms-suborder-status-color'>طلبات البائع (الطلبات الفرعية)</span> تبعاً لحالاتها.<BR><BR>
على سبيل المثال، يؤدي تعيين الحالة إلى مكتملة إلى معاملة طلبات البائع التي تحمل هذه الحالة على أنها مكتملة (مرسلة) مما يسمح للعميل بوضع مراجعاته وتقييماته.
EOT;
$_['ms_config_suborder_status_autocomplete'] = '(تكملة تلقائية)';
$_['ms_config_suborder_state_' . MsSuborder::STATE_PENDING] = 'قيد الانتظار';
$_['ms_config_suborder_state_' . MsSuborder::STATE_PROCESSING] = 'قيد المعالجة';
$_['ms_config_suborder_state_' . MsSuborder::STATE_COMPLETED] = 'مكتمل';
$_['ms_config_suborder_state_' . MsSuborder::STATE_FAILED] = 'فشل';
$_['ms_config_suborder_state_' . MsSuborder::STATE_CANCELLED] = 'ملغي';
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_PENDING] = "حالة قيد الانتظار تشير إلى أن طلبات البائع قد تم إنشاءها ولكن لم يتم معالجتها بعد.";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_PROCESSING] = "حالة قيد المعالجة تشير إلى أن طلبات البائع قيد المعالجة حالياً.";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_COMPLETED] = "حالة الاكتمال تشير إلى أن طلبات البائع قد تم ارسالها إلى العميل.";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_FAILED] = "ملاحظة فشل متعدد البائعين ";
$_['ms_config_suborder_state_note_' . MsSuborder::STATE_CANCELLED] = "ملاحظة إلغاء متعدد البائعين ";

$_['ms_config_order_statuses'] = "ضبط حالة الطلب";
$_['ms_config_suborder_statuses'] = "حالات طلب البائعين";
$_['ms_config_suborder_status_default'] = "حالة طلب البائع الافتراضية";
$_['ms_config_suborder_status_default_note'] = "هذه هي <span class='ms-suborder-status-color'>حالة طلب البائع</span> التي جميع <span class='ms-suborder-status-color'>طلبات البائعين</span>التي ستكون مربوطه به عندما  <span class='ms-order-status-color'>.عند إنشاء العميل للطلب</span>";
$_['ms_config_order_status_credit'] = "حالات إضافة الرصيد";
$_['ms_config_order_status_credit_note'] = <<<EOT
اختر <span class='ms-order-status-color'>حالات طلب العميل</span> الخاصة أوبن كارت و/أو <span class='ms-suborder-status-color'>حالات طلب البائع</span> االخاصة ملتي ميرش التي ستقوم ب<span class='ms-credit-status-color'>إضافة</span> قيمة المنتجات المباعة إلى رصيد البائع عند تغيير حالة الطلب.<BR><BR>
إضافة حالات OpenCart هنا سيقوم بإنشاء عمليات إضافة الأرصدة لكافة البائعين الذين يشكلون جزءاً من طلب العميل بشكل آلي. اختيار حالات ملتي ميرش سيقوم بإنشاء عمليات إضافة الأرصدة لبائعين محددين عندما يتم تغيير حالة طلب البائع المعني.
EOT;
$_['ms_config_order_status_debit'] = "حالات الإرجاع من الرصيد";
$_['ms_config_order_status_debit_note'] = <<<EOT
اختر <span class='ms-order-status-color'>حالات طلب العميل</span> الخاصة أوبن كارت و/أو <span class='ms-suborder-status-color'>حالات طلب البائع</span> الخاصة ملتي ميرش التي ستقوم ب<span class='ms-refund-status-color'>طرح</span> مبلغ العملبة الأصلية التي تمت من رصيد البائع عندما يتم تغيير حالة الطلب.<BR><BR>
إضافة حالات OpenCart هنا سيقوم بإنشاء عمليات إضافة الأرصدة لكافة البائعين الذين يشكلون جزءاً من طلب العميل بشكل آلي. اختيار حالات ملتي ميرش سيقوم بإنشاء عمليات إضافة الأرصدة لبائعين محددين عندما يتم تغيير حالة طلب البائع المعني.
EOT;

// MM > Settings > Products
$_['ms_config_reviews'] = 'تقييمات المنتج';
$_['ms_config_reviews_enable'] = 'السماح بالتقييم';
$_['ms_config_reviews_enable_note'] = "السماح للعميل بوضع ملاحظاته وتقييمه للمنتج الذي اشتراه. يؤدي تفعيل هذا إلى تعطيل نظام مراجعات أوبن كارت الافتراضي";

$_['ms_config_import'] = 'استيراد المنتجات من ملفات CSV';
$_['ms_config_import_enable'] = 'السماح باستيرادات CSV';
$_['ms_config_import_enable_note'] = 'السماح للبائعين برفع منتجاتهم عبر ملف CSV';
$_['ms_config_import_category_type'] = 'طريقة ادخال القسم';
$_['ms_config_import_category_type_note'] = 'يتحكم هذا الإعداد بكيفية قبول ملتي ميرش بأقسام المنتج عبر ملفات CSV - سواء كانت كافة مستويات الأقسام في خلية واحدة عبر فاصل أو كل مستوى قسم في خلية منفصلة';
$_['ms_config_import_category_type_all_categories'] = 'كل المستويات في خلية واحدة (الفاصل: |)';
$_['ms_config_import_category_type_categories_levels'] = 'المستويات المختلفة في خلايا منفصلة';
/***** INSERTED *****/$_['ms_config_feed_allow_external_source'] = "Allow external source";
/***** INSERTED *****/$_['ms_config_feed_allow_external_source_note'] = "Seller will be able to specify URL of an external source import file.";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field'] = "Primary product field";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_note'] = "Select the primary (unique) product field to be used when updating existing products during imports";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_name'] = "Name";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_model'] = "Model";
/***** INSERTED *****/$_['ms_config_feed_primary_product_field_sku'] = "SKU";
/***** INSERTED *****/$_['ms_config_feed_scheduled_import_enable'] = "Enable imports scheduling";
/***** INSERTED *****/$_['ms_config_feed_scheduled_import_enable_note'] = <<<EOT
To enable import scheduling, please add the following line to your crontab:<br>
* * * * * YOUR_PHP_INSTALLATION_PATH %ssystem/vendor/multimerchlib/Workers/Cron.php
EOT;

$_['ms_config_product_categories_type'] = 'نوع أقسام المنتج';
$_['ms_config_product_categories_type_note'] = 'ما هو نوع الأقسام التي يُسمح للبائع باستخدامها عند إدراج منتجه';
$_['ms_config_product_category_store'] = 'أقسام المتجر';
$_['ms_config_product_category_seller'] = 'أقسام البائع';
$_['ms_config_product_category_both'] = 'كلاهما';

// Sales > Order > Info > Shipping
$_['ms_sale_order_shipping_cost'] = 'تكلفة الشحن';
$_['ms_sale_order_shipping_via'] = 'الشحن عبر %s';


$_['ms_config_status'] = 'الحالة';
$_['ms_config_top'] = 'المحتوى العلوي';
$_['ms_config_limit'] = 'الحد:';
$_['ms_config_image'] = 'الصورة (عرض×طول):';

$_['ms_config_enable_rte'] = 'السماح بمعالج النصوص RTE للوصف';
$_['ms_config_enable_rte_note'] = 'السماح بمحرر النصوص القابل للتنسيق لحقول وصف البائع والمنتج.';

$_['ms_config_rte_whitelist'] = 'Tag whitelist';
$_['ms_config_rte_whitelist_note'] = 'Permitted tags in RTE (empty = all tags permitted)';

$_['ms_config_image_sizes'] = 'حجم الصور';
$_['ms_config_seller_avatar_image_size'] = 'حجم الصورة الشخصية';
$_['ms_config_seller_avatar_image_size_seller_profile'] = 'الصفحة الشخصية للبائع';
$_['ms_config_seller_avatar_image_size_seller_list'] = 'قائمة البائعين';
$_['ms_config_seller_avatar_image_size_product_page'] = 'صفحة المنتج';
$_['ms_config_seller_avatar_image_size_seller_dashboard'] = 'لوحة مراقبة البائع';
$_['ms_config_seller_banner_size'] = 'حجم صورة غلاف البائع';

$_['ms_config_image_preview_size'] = 'حجم معاينة الصورة';
$_['ms_config_image_preview_size_seller_avatar'] = 'الصورة الشخصية للبائع';
$_['ms_config_image_preview_size_product_image'] = 'صورة المنتج';

$_['ms_config_product_image_size'] = 'حجم صورة المنتج';
$_['ms_config_product_image_size_seller_profile'] = 'الملف الشخصي للبائع';
$_['ms_config_product_image_size_seller_products_list'] = 'فهرس المنتجات';
$_['ms_config_product_image_size_seller_products_list_account'] = 'منتجات الحساب';

$_['ms_config_description_image_size'] = 'وصف حجم الصورة';
$_['ms_config_description_image_size_note'] = 'الحجم الأقصى لوصف الصورة في صفحات المنتجات و الملف الشخصي الخاص للبائعين (ع x أ)';

$_['ms_config_description_images'] = 'وصف الصورة';
$_['ms_config_description_images_allow'] = 'إسمح بوضع وصف للصورة';
$_['ms_config_description_images_allow_note'] = 'إسمح للبائعين لإستخدام صور في وصف المنتج وفي الملفات الشخصية.';
$_['ms_config_description_images_type'] = 'نوع تخزين الصور (legacy)';
$_['ms_config_description_images_type_note'] = 'إختر كيف يتم تخزين الصور المرفوعة من قبل البائعين';
$_['ms_config_description_images_type_upload'] = 'رفع للخادم';
$_['ms_config_description_images_type_base64'] = 'Base64';

$_['ms_config_uploaded_image_size'] = 'حدود حجم الصورة';
$_['ms_config_uploaded_image_size_note'] = 'ضع حدوداً لأبعاد الصور التي يتم تحميلها (عرض×طول). ضع 0 لعدم وضع حدود.';
$_['ms_config_max'] = 'الأعلى';
$_['ms_config_min'] = 'الأدنى';

$_['ms_config_seo'] = 'روابط SEO';
$_['ms_config_enable_seo_urls_seller'] = 'توليد روابط SEO للبائعين الجدد';
$_['ms_config_enable_seller_generate_metatags'] = 'توليد كلمات الميتا للبائعين';
$_['ms_config_meta_for_seller_page'] = 'صفحة البائع';
$_['ms_config_meta_for_seller_products_page'] = 'صفحة منتجات اليائع';
$_['ms_config_meta_seller_title_template'] = 'نموذج عنوان البائع';
$_['ms_config_meta_seller_h1_template'] = 'نموذج h1 البائع';
$_['ms_config_meta_seller_description_template'] = 'نموذج وصف البائع';
$_['ms_config_meta_seller_keyword_template'] = 'نموذج كلمات البائع الرئيسية';

$_['ms_config_enable_seo_urls_seller_note'] = 'هذا الخيار سيقوم بتوليد روابط صديقة لمحسنات محركات البحث للبائعين الجدد. يجب تفعيل روابط SEO من إعدادات OpenCart لاستخدام هذه الخاصية.';
$_['ms_config_enable_seo_urls_product'] = 'توليد روابط SEO للمنتجات الجديدة (تجريبي)';
$_['ms_config_enable_seo_urls_product_note'] = 'هذا الخيار سيقوم بتوليد روابط صديقة لمحسنات محركات البحث للمنتجات الجديدة. يجب تفعيل روابط SEO من إعدادات OpenCart لاستخدام هذه الخاصية. هذه الميزة لا تزال قيد الإختبار خاصة للمتاجر الغير إنجليزية.';
$_['ms_config_enable_non_alphanumeric_seo'] = 'السماح بترميز UTF8 في روابط SEO (تجريبي)';
$_['ms_config_enable_non_alphanumeric_seo_note'] = 'سيؤدي هذا إلى عدم حذف رموز UTF8 اللا إنجليزية من روابط SEO. هذه الميزة لا تزال قيد الإختبار، استخدمها على مسؤوليتك.';
$_['ms_config_sellers_slug'] = 'كلمات روابط SEO الأساسية للبائعين';
$_['ms_config_sellers_slug_'] = '/اسم-البائع/';
$_['ms_config_sellers_slug_note'] = <<<EOT
استخدم هذا الإعداد لتحديد الكلمات الرئيسية للروابط الخاصة بصفحات البائعين الشخصية في متجرك. <br />مثلاً: %ssellers/johndoe/store/<br>
تحذير: قيامك بتعديل هذا في متجر قيد العمل بالفعل قد يسبب تعطيل روابط SEO السابقة مما يؤثر على تصنيفك في محركات البحث.
EOT;
$_['ms_config_products_slug'] = 'كلمات روابط SEO الأساسية للمنتجات';
$_['ms_config_products_slug_note'] = <<<EOT
استخدم هذا الإعداد لتحديد الكلمات الرئيسية للروابط الخاصة بمنتجات البائعين في متجرك. <br />مثلاً: %sproducts/iphone-5s/. اتركه فارغاً لتعطيل الكلمة المفتاحية للرابط.<br>
تحذير: قيامك بتعديل هذا في متجر قيد العمل بالفعل قد يسبب تعطيل روابط SEO السابقة مما يؤثر على تصنيفك في محركات البحث.
EOT;

$_['ms_config_sellers_map'] = 'خريطة البائعين';
$_['ms_config_sellers_map_api_key'] = 'مفتاح Google Maps API';
$_['ms_config_sellers_map_api_key_note'] = 'يرجى وضع <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">مفتاح Google Maps API</a> للسماح بعرض خريطة للبائعين';

$_['ms_config_logging'] = 'وضع التصحيح';
$_['ms_config_logging_level'] = 'مستوى تسجيل التصحيح';
$_['ms_config_logging_level_note'] = 'حدد مستوى التسجيل الخاص بالتصحيح (أخطاء: تسجيل الأخطاء فقط. معلومات: تسجيل بعض المعلومات. شامل: تسجيل تتبع الوظائف بشكل كامل، قم بتفعيله فقط لفترات قصيرة عند تصحيح الأخطاء.)';
$_['ms_config_logging_level_error'] = 'أخطاء';
$_['ms_config_logging_level_debug'] = 'شامل';
$_['ms_config_logging_level_info'] = 'معلومات';
$_['ms_config_logging_filename'] = 'اسم ملف التسجيل';
$_['ms_config_logging_filename_note'] = 'حدد اسماً لملف تصحيح MultiMerch';

$_['ms_config_seller'] = 'البائعون';

// Change Seller Group
$_['ms_config_change_group'] = 'السماح باختيار المجموعة عند التسجيل';
$_['ms_config_change_group_note'] = 'السماح للبائعين باختيار مجموعة البائع عند التسجيل';

// Change Seller Nickname
$_['ms_config_seller_change_nickname'] = 'السماح بتغيير الاسم المستعار';
$_['ms_config_seller_change_nickname_note'] = 'السماح للبائعين بتغيير الاسم المستعار/ اسم المحل';

// Seller Nickname Rules
$_['ms_config_nickname_rules'] = 'قواعد اسم البائع';
$_['ms_config_nickname_rules_note'] = 'نظام الأحرف المسموح به في اسم العميل';
$_['ms_config_nickname_rules_alnum'] = 'أحرف لاتينية وأرقام';
$_['ms_config_nickname_rules_ext'] = 'اللاتينية الموسعة';
$_['ms_config_nickname_rules_utf'] = 'ترميز UTF-8';


$_['mxt_google_analytics'] = 'Google Analytics';
$_['mxt_google_analytics_enable'] = 'تفعيل Google Analytics';

$_['mxt_disqus_comments'] = 'تعليقات Disqus';
$_['mxt_disqus_comments_enable'] = 'تفعيل تعليقات Disqus';
$_['mxt_disqus_comments_shortname'] = 'اسم Disqus المختصر';

$_['mmes_messaging'] = 'الرسائل الخاصة';
$_['mmess_config_enable'] = 'السماح بالرسائل الخاصة في ملتي ميرش';
$_['ms_config_msg_allowed_file_types'] = 'امتدادات الملفات المسموح بها';
$_['ms_config_msg_allowed_file_types_note'] = 'امتدادات الملفات المسموح بها عند رفعها برسالة';

$_['ms_config_coupon'] = 'قسائم التخفيض';
$_['ms_config_coupon_allow'] = 'السماح للبائعين بإنشاء قسائم تخفيض';

//Marketplace Dashboard
$_['ms_dashboard_title'] = 'لوحة المراقبة';
$_['ms_dashboard_heading'] = 'لوحة المراقبة';

$_['ms_dashboard_total_sales'] = 'إجمالي المبيعات';
$_['ms_dashboard_total_orders'] = 'إجمالي الطلبات';
$_['ms_dashboard_total_customers'] = 'إجمالي العملاء';
$_['ms_dashboard_total_customers_online'] = 'العملاء المتصلين';
$_['ms_dashboard_total_sellers'] = 'إجمالي البائعين';
$_['ms_dashboard_total_sellers_balances'] = 'الإجمالي عبر أرصدة البائعين';
$_['ms_dashboard_total_products'] = 'إجمالي المنتجات';
$_['ms_dashboard_total_products_views'] = 'إجمالي مشاهدات المنتجات';
$_['ms_dashboard_gross_sales'] = 'إجمالي أرباح المبيعات';

$_['ms_dashboard_sales_analytics'] = 'تحليلات المبيعات';
$_['ms_dashboard_top_products'] = 'المنتجات الأكثر مبيعاً';
$_['ms_dashboard_top_sellers'] = 'أفضل البائعين أداءً';
$_['ms_dashboard_top_customers'] = 'العملاء الأكثر قيمة';
$_['ms_dashboard_top_countries'] = 'أكثر الدول شراءً';

$_['ms_dashboard_sales_analytics_no_results'] = "لا توجد طلبات حتى الآن.";
$_['ms_dashboard_top_products_no_results'] = "لا توجد بيانات.";
$_['ms_dashboard_top_sellers_no_results'] = "لا توجد بيانات.";
$_['ms_dashboard_top_customers_no_results'] = "لا توجد بيانات.";
$_['ms_dashboard_top_countries_no_results'] = "لا توجد بيانات.";

$_['ms_dashboard_marketplace_activity'] = 'نشاط المتجر';
$_['ms_dashboard_latest_orders'] = 'آخر الطلبات';

$_['ms_dashboard_marketplace_activity_no_results'] = 'لا يوجد نشاط في المتجر حتى الآن.';
$_['ms_dashboard_latest_orders_no_results'] = 'لا توجد طلبات حتى الآن.';

//SEO
$_['ms_seo'] = 'روابط SEO';
$_['ms_seo_urls'] = 'روابط SEO';
$_['ms_use_seo_urls'] = 'استخدام روابط SEO الخاصة بـ ملتي ميرش';
$_['ms_use_seo_urls_note'] = <<<EOT
يقوم نظام ملتي ميرش SEO باستبدال نظام أوبن كارت الأساسي والإضافات الخاصة بمحسنات محركات البحث SEO تحكم لجعل روابط متجرك تظهر بشكل رائع.<br>
تحذير: تفعيل هذه الخاصية في متجر قيد العمل بالفعل بروابط SEO قد يؤدي إلى تعطيل الروابط الموجودة مسبقاً. خذ حذرك!<br>
إذا كنت تريد استخدام إضافة أخرى خاصة بنظام SEO ستحتاج إلى تعطيل هذا الخيار وكذلك تعديل اسم ملف multimerch_core_seo.xml الموجود في المسار /vqmod/xml (غير مستحسن).
EOT;

$_['ms_seo_url_tooltip'] = 'لا تقم بإستخدام مسافات, بل قم بإستبدالها بـ  - وتأكد أن رابط الـ SEO فريد في متجرك بالكامل.';

// Badges
$_['ms_menu_badge'] = 'الأوسمة';
$_['ms_config_badge_title'] = 'أوسمة البائع';
$_['ms_config_badge_manage'] = 'إدارة الأوسمة';
$_['ms_config_badge_enable_note'] = 'تفعيل وظيفة أوسمة البائع للسماح للإدارة بإنشاء أوسمة وتعيينها للبائعين';
$_['ms_config_badge_size'] = 'حجم الوسام';
$_['ms_catalog_badges_breadcrumbs'] = 'الأوسمة';
$_['ms_catalog_badges_heading'] = 'الأوسمة';
$_['ms_badges_column_id'] = 'المعرف';
$_['ms_badges_column_name'] = 'الإسم';
$_['ms_badges_image'] = 'الصورة';
$_['ms_badges_column_action'] = 'الإجراءات';
$_['ms_catalog_insert_badge_heading'] = 'إنشاء وسام';
$_['ms_catalog_edit_badge_heading'] = 'تعديل وسام';
$_['ms_success_badge_created'] = 'تم إنشاء الوسام';
$_['ms_success_badge_updated'] = 'تم تعديل الوسام';
$_['ms_error_badge_name'] = 'يرجى تحديد اسم لهذا الوسام';
$_['ms_error_badge_image'] = 'يرجى اختيار صورة الوسام';

// Social Links
$_['ms_menu_social_links'] = 'وسائل التواصل';
$_['ms_config_sl_title'] = 'وسائل التواصل الإجتماعي';
$_['ms_config_sl_enable_note'] = 'تمكين ميزة روابط وسائل التواصل الاجتماعي التي تتيح للبائعين عرض روابط حساباتهم على الشبكات الاجتماعية في ملفاتهم الشخصية';
$_['ms_sl_icon_size'] = 'حجم الأيقونة';
$_['ms_sl'] = 'وسائل التواصل';
$_['ms_sl_manage'] = 'إدارة قنوات التواصل الاجتماعي';
$_['ms_sl_create'] = 'إضافة قناة تواصل';
$_['ms_sl_update'] = 'تحديث قناة التواصل';
$_['ms_sl_column_id'] = '#';
$_['ms_sl_column_name'] = 'الاسم';
$_['ms_sl_image'] = 'الصورة';
$_['ms_sl_column_action'] = 'الإجراءات';
$_['ms_success_channel_created'] = 'تم إنشاء قناة تواصل';
$_['ms_success_channel_updated'] = 'تم تحديث قناة التواصل';
$_['ms_error_channel_deleting'] = 'حدث خطأ أثناء حذف قناة التواصل!';
$_['ms_success_channel_deleting'] = 'تم حذف قناة التواصل بنجاح!';
$_['ms_error_channel_name'] = 'الرجاء وضع اسم لوسيلة التواصل هذه';

// Information pages
$_['ms_menu_information'] = 'صفحات';
$_['ms_information_heading'] = 'صفحات';
$_['ms_information_title'] = 'عنوان الصفحة';
$_['ms_information_content'] = 'المحتوى';
$_['ms_information_add'] = 'إنشئ صفحة جديدة';
$_['ms_information_edit'] = 'تعديل الصفحة';
$_['ms_information_success'] = 'نجاح: لقد قمت بتعديل الصفحة!';
$_['ms_information_success_deleted'] = 'لقد قمت بحذف الصفحة!';
$_['ms_information_error_title'] = 'عنوان الصفحة يجب أن يكون 3 إلى 64 حروف!!';
$_['ms_information_error_content'] = 'المحتوى يجب أن يكون اكثر من  3 حرف!';
$_['ms_information_error_meta_title'] = 'عنوان الفوقية يجب أن يكون اكثر من 3 وأقل من 255 حرف!';
$_['ms_information_error_account'] = 'تحذير: لا يمكن حذف هذه الصفحة نظراً لإرتباطها بالشروط والأحكام.';
$_['ms_information_error_checkout'] = 'تحذير: لا يمكن حذف صفحة المعلومات هذه نظراً لإرتباطها بشروط إنهاء الطلب!';
$_['ms_information_error_affiliate'] = 'تحذير: لا يمكن حذف صفحة المعلومات هذه نظراً لإرتباطها بشروط نظام العمولة!';
$_['ms_information_error_return']  = 'تحذير: لا يمكن حذف هذه الصفحة نظراً لإرتباطها بشروط إرجاع السلع!';
$_['ms_information_error_store'] = 'تحذير: لا يمكن حذف صفحة المعلومات هذه للأنها مستخدمة من قبل  %s متاجر!';

// Seller - List
$_['ms_catalog_sellers_heading'] = 'البائعون';
$_['ms_catalog_sellers_breadcrumbs'] = 'البائعون';
$_['ms_catalog_sellers_newseller'] = 'بائع جديد';
$_['ms_catalog_sellers_create'] = 'إنشاء بائع جديد';
$_['ms_catalog_sellers_view_profile'] = 'عرض الملف الشخصي للبائع';

$_['ms_catalog_sellers_total_balance'] = 'المبلغ الإجمالي لكل الأرصدة: <b>%s</b> (البائعون النشطون: <b>%s</b>)';
$_['ms_catalog_sellers_email'] = 'البريد الالكتروني';
$_['ms_catalog_sellers_total_products'] = 'المنتجات';
$_['ms_catalog_sellers_total_sales'] = 'المبيعات';
$_['ms_catalog_sellers_current_balance'] = 'الرصيد';
$_['ms_catalog_sellers_status'] = 'الحالة';
$_['ms_catalog_sellers_date_created'] = 'تاريخ الإنشاء';

$_['ms_seller_status_' . MsSeller::STATUS_ACTIVE] = 'مفعل';
$_['ms_seller_status_' . MsSeller::STATUS_INACTIVE] = 'غير مفعل';
$_['ms_seller_status_' . MsSeller::STATUS_DISABLED] = 'معطل';
$_['ms_seller_status_' . MsSeller::STATUS_INCOMPLETE] = 'غير مكتمل';
$_['ms_seller_status_' . MsSeller::STATUS_DELETED] = 'محذوف';
$_['ms_seller_status_' . MsSeller::STATUS_UNPAID] = 'لم يدفع رسوم التسجيل';

// Customer-seller form
$_['ms_catalog_sellerinfo_heading'] = 'البائع';
$_['ms_catalog_sellerinfo_create_heading'] = "إنشاء بائع جديد";
$_['ms_catalog_sellerinfo_update_heading'] = "تعديل معلومات البائع";
$_['ms_catalog_sellerinfo_seller_data'] = 'بيانات البائع';

$_['ms_catalog_sellerinfo_customer'] = 'العميل';
$_['ms_catalog_sellerinfo_customer_data'] = 'بيانات العميل';
$_['ms_catalog_sellerinfo_customer_new'] = 'عميل جديد';
$_['ms_catalog_sellerinfo_customer_existing'] = 'عميل موجود مسبقاً';
$_['ms_catalog_sellerinfo_customer_create_new'] = 'إنشاء عميل جديد';
$_['ms_catalog_sellerinfo_customer_firstname'] = 'الاسم الأول';
$_['ms_catalog_sellerinfo_customer_lastname'] = 'الاسم الأخير';
$_['ms_catalog_sellerinfo_customer_email'] = 'البريد الالكتروني';
$_['ms_catalog_sellerinfo_customer_password'] = 'كلمة المرور';
$_['ms_catalog_sellerinfo_customer_password_confirm'] = 'تأكيد كلمة المرور';

$_['ms_catalog_sellerinfo_nickname'] = 'الاسم المستعار';
$_['ms_catalog_sellerinfo_keyword'] = 'رابط SEO';
$_['ms_catalog_sellerinfo_slogan'] = 'الشعار النصي';
$_['ms_catalog_sellerinfo_description'] = 'الوصف';
$_['ms_catalog_sellerinfo_zone'] = 'المنطقة / الولاية';
$_['ms_catalog_sellerinfo_zone_select'] = 'اختر المنطقة/الولاية';
$_['ms_catalog_sellerinfo_zone_not_selected'] = 'لم يتم اختيار منطقة/ولاية';
$_['ms_catalog_sellerinfo_sellergroup'] = 'مجموعة البائع';

$_['ms_catalog_sellerinfo_avatar'] = 'الصورة الشخصية';
$_['ms_catalog_sellerinfo_message'] = 'رسالة';
$_['ms_catalog_sellerinfo_message_note'] = 'تضمين هذه الرسالة في إشعار البريد الالكتروني المرسل إلى البائع (اختياري)';
$_['ms_catalog_sellerinfo_notify'] = 'تنبيه البائع';
$_['ms_catalog_sellerinfo_notify_note'] = 'حدد هذا المربع لإرسال بريد الكتروني إلى البائع يشعره بأنه قد تم تعديل حسابه';
$_['ms_catalog_sellerinfo_product_validation'] = 'تفعيل المنتجات';
$_['ms_catalog_sellerinfo_product_validation_note'] = 'تفعيل المنتجات لهذا البائع';

$_['ms_error_sellerinfo_nickname_empty'] = 'لا يمكن ترك الاسم المستعار فارغاً';
$_['ms_error_sellerinfo_nickname_alphanumeric'] = 'يجب أن يحوي الاسم المستعار على أحرف لاتينية وأرقام فقط';
$_['ms_error_sellerinfo_nickname_utf8'] = 'يجب أن يحوي الاسم المستعار على رموز UTF-8 قابلة للطباعة فقط';
$_['ms_error_sellerinfo_nickname_latin'] = 'يجب أن يحوي الاسم المستعار على أحرف لاتينية وأرقام وتشكيلات فقط';
$_['ms_error_sellerinfo_nickname_length'] = 'الاسم المستعار يجب أن يكون بين 4 و 50 حرفاً';
$_['ms_error_sellerinfo_nickname_taken'] = 'هذا الاسم المستعار محجوز مسبقاً';
$_['ms_error_form_submit_error'] = 'حدث خطأ أثناء تقديم النموذج.';

// Catalog - Products
$_['ms_catalog_products_heading'] = 'المنتجات';
$_['ms_catalog_products_breadcrumbs'] = 'المنتجات';
$_['ms_catalog_products_notify_sellers'] = 'تنبيه البائعين';
$_['ms_catalog_products_bulk'] = '--تغيير الحالة--';
$_['ms_catalog_products_bulk_seller'] = '--تغيير البائع--';
$_['ms_catalog_products_noseller'] = '--بدون بائع--';
$_['ms_catalog_products_error_deleting'] = 'حدث خطأ أثناء الحذف!';
$_['ms_catalog_products_success_deleting'] = 'تم حذف المنتج بنجاح!';

$_['ms_product_status_' . MsProduct::STATUS_ACTIVE] = 'مفعل';
$_['ms_product_status_' . MsProduct::STATUS_INACTIVE] = 'غير مفعل';
$_['ms_product_status_' . MsProduct::STATUS_DISABLED] = 'معطل';
$_['ms_product_status_' . MsProduct::STATUS_DELETED] = 'محذوف';
$_['ms_product_status_' . MsProduct::STATUS_UNPAID] = 'لم يتم دفع رسوم النشر';
$_['ms_product_status_' . MsProduct::STATUS_IMPORTED] = 'مستورد';

$_['ms_catalog_products_field_price'] = 'السعر';
$_['ms_catalog_products_field_quantity'] = 'الكمية';
$_['ms_catalog_products_field_marketplace_category'] = 'قسم المتجر';
$_['ms_catalog_products_field_tags'] = 'الكلمات الدلالية';
$_['ms_catalog_products_field_attributes'] = 'المواصفات';
$_['ms_catalog_products_field_options'] = 'الخيارات';
$_['ms_catalog_products_field_special_prices'] = 'عروض خاصة';
$_['ms_catalog_products_field_quantity_discounts'] = 'أسعار الجملة';
$_['ms_catalog_products_field_images'] = 'الصور';
$_['ms_catalog_products_field_files'] = 'الملفات';
$_['ms_catalog_products_field_meta_keyword'] 	 = 'كلمات الميتا الدلالية';
$_['ms_catalog_products_field_meta_description'] = 'وصف الميتا';
$_['ms_catalog_products_field_meta_title'] = 'عنوان الميتا';
$_['ms_catalog_products_field_seo_url'] = 'رابط SEO';
$_['ms_catalog_products_field_model']            = 'الموديل';
$_['ms_catalog_products_field_sku']              = 'SKU';
$_['ms_catalog_products_field_upc']              = 'UPC';
$_['ms_catalog_products_field_ean']              = 'EAN';
$_['ms_catalog_products_field_jan']              = 'JAN';
$_['ms_catalog_products_field_isbn']             = 'ISBN';
$_['ms_catalog_products_field_mpn']              = 'MPN';
$_['ms_catalog_products_field_manufacturer']     = 'الشركة';
$_['ms_catalog_products_field_date_available']   = 'تاريخ التوفر';
$_['ms_catalog_products_field_stock_status']     = 'حالة عدم توفر المنتج';
$_['ms_catalog_products_field_tax_class']        = 'نوع الضريبة';
$_['ms_catalog_products_field_subtract']         = 'الخصم من الكمية';
$_['ms_catalog_products_field_stores']         = 'المتاجر';
$_['ms_catalog_products_filters']         = 'الفلاتر';
$_['ms_catalog_products_min_order_qty']         = 'أقل كمية للطلب';
$_['ms_catalog_products_related_products']         = 'المنتجات المتعلقة';
$_['ms_catalog_products_dimensions']            = 'الأبعاد';
$_['ms_catalog_products_weight']            = 'الوزن';

// Catalog - Products - Custom fields
$_['ms_catalog_products_tab_custom_field'] = 'الحقول المخصصة';
$_['ms_catalog_products_text_placeholder'] = 'ادخل نصاً...';
$_['ms_catalog_products_textarea_placeholder'] = 'ادخل نصاً...';
$_['ms_catalog_products_date_placeholder'] = 'اختر التاريخ...';
$_['ms_catalog_products_button_upload'] = 'رفع';
$_['ms_catalog_products_success_file_uploaded'] = 'تم رفع الملف بنجاح!';
$_['ms_catalog_products_success_upload_removed'] = 'تم حذف الملف!';
$_['ms_catalog_products_error_field_required'] = 'الخانة مطلوبة!';
$_['ms_catalog_products_error_field_validation'] = 'لم يتم التحقق من الخانة! النمط: %s';

// Catalog - Imports
$_['ms_catalog_imports_heading'] = 'الإستيرادات';
$_['ms_catalog_imports_breadcrumbs'] = 'الإستيرادات';

$_['ms_catalog_imports_field_name'] = 'الاسم';
$_['ms_catalog_imports_field_seller'] = 'البائع';
$_['ms_catalog_imports_field_date'] = 'التاريخ';
$_['ms_catalog_imports_field_type'] = 'النوع';
$_['ms_catalog_imports_field_processed'] = 'تم معالجته';
$_['ms_catalog_imports_field_added'] = 'تم إضافته';
$_['ms_catalog_imports_field_updated'] = 'تم تحديثه';
$_['ms_catalog_imports_field_errors'] = 'الأخطاء';
$_['ms_catalog_imports_field_actions'] = 'الإجراءات';

/***** INSERTED *****/$_['ms_feed_import_list_single'] = "Import history";
/***** INSERTED *****/$_['ms_feed_import_list_scheduled'] = "Scheduled imports";
/***** INSERTED *****/$_['ms_feed_import_list_url_path'] = "Feed URL";
/***** INSERTED *****/$_['ms_feed_import_list_date_last_run'] = "Date last run";
/***** INSERTED *****/$_['ms_feed_import_list_date_next_run'] = "Date next run";
/***** INSERTED *****/$_['ms_feed_import_list_success_deleted'] = "Scheduled import is successfully deleted!";

// Catalog - Seller Groups
$_['ms_catalog_seller_groups_heading'] = 'مجموعات البائعين';
$_['ms_catalog_seller_groups_breadcrumbs'] = 'مجموعات البائعين';

$_['ms_seller_groups_column_id'] = '#';
$_['ms_seller_groups_column_name'] = 'الاسم';
$_['ms_seller_groups_column_action'] = 'الإجراءات';

$_['ms_catalog_insert_seller_group_heading'] = 'مجموعة بائعين جديدة';
$_['ms_catalog_edit_seller_group_heading'] = 'تعديل مجموعة البائعين';

$_['ms_product_period'] = 'فترة نشر المنتج بالأيام (0 = بدون حدود)';
$_['ms_product_quantity'] = 'كمية المنتج (0 = بدون حدود)';

$_['ms_seller_group_stripe_subscription_heading'] = "اشتراك Stripe";
$_['ms_seller_group_stripe_subscription_enabled'] = "استخدام اشتراك Stripe";
$_['ms_seller_group_stripe_subscription_info'] = "تتم معالجة الاشتراكات عبر Stripe. لن يؤثر تغيير شروط الاشتراك هنا على أعضاء المجموعة الحاليين.";
$_['ms_seller_group_stripe_subscription_plan_interval_day'] = "يومي";
$_['ms_seller_group_stripe_subscription_plan_interval_month'] = "شهري";
$_['ms_seller_group_stripe_subscription_plan_interval_year'] = "سنوي";
$_['ms_seller_group_stripe_subscription_plan_base'] = "الخطة الأساسية";
$_['ms_seller_group_stripe_subscription_plan_base_info'] = "%s %s";
$_['ms_seller_group_stripe_subscription_plan_per_seat'] = "خطة: لكل منتج";
$_['ms_seller_group_stripe_subscription_plan_per_seat_info'] = "%s لكل منتج %s";

$_['ms_error_seller_group_name'] = 'خطاً: يجب أن يكون الاسم بين 3 و 32 حرفاً';
$_['ms_error_seller_group_default'] = 'خطأ: لا يمكن حذف مجموعة البائعين الافتراضية!';
$_['ms_success_seller_group_created'] = 'تم انشاء مجموعة بائعين جديدة';
$_['ms_success_seller_group_updated'] = 'تم تعديل مجموعة البائعين';
$_['ms_error_seller_group_deleting'] = 'حدث خطأ أثناء حذف مجموعة البائعين!';
$_['ms_success_seller_group_deleting'] = 'تم حذف مجموعة البائعين بنجاح!';

// Payments
$_['ms_payment_heading'] = 'المدفوعات';
$_['ms_payment_breadcrumbs'] = 'المدفوعات';
$_['ms_payment_payout_requests'] = 'طلبات المستحقات';
$_['ms_payment_payouts'] = 'المستحقات اليدوية';
$_['ms_payment_pending'] = 'قيد الانتظار';
$_['ms_payment_new'] = 'دفع جديد';
$_['ms_payment_paid'] = 'مدفوع';
$_['ms_payment_no_methods'] = 'لا توجد وسائل دفع متاحة!';
$_['ms_payment_multiple_invoices_no_methods'] = 'دفع الاستحقاقات المتعددة متاحة فقط عبر Paypal MassPay! الرجاء التأكد من أن Paypal مفعل للدفع في <a href="%s">وسائل الدفع</a>.';

$_['ms_success_payment_created'] = 'تم الدفع بنجاح';

// Shipping methods
$_['ms_shipping_method_heading'] = 'وسائل الشحن';
$_['ms_shipping_method_breadcrumbs'] = 'وسيلة الشحن';
$_['ms_shipping_method_status_' . MsShippingMethod::STATUS_ENABLED] = 'مفعل';
$_['ms_shipping_method_status_' . MsShippingMethod::STATUS_DISABLED] = 'معطل';
$_['ms_shipping_method_add_heading'] = 'إضافة وسيلة شحن';
$_['ms_shipping_method_add_success'] = 'لقد قمت بإنشاء وسيلة شحن بنجاح!';
$_['ms_shipping_method_edit_heading'] = 'تعديل وسيلة الشحن';
$_['ms_shipping_method_edit_success'] = 'لقد قمت بتعديل وسيلة الشحن بنجاح!';
$_['ms_shipping_method_delete_success'] = 'لقد قمت بحذف وسيلة الشحن!';
$_['ms_shipping_method_delete_error'] = 'حدث خطأ أثناء حذف وسيلة الشحن!';
$_['ms_shipping_method_name_error'] = 'خطأ: الاسم يجب أن يكون بين 3 و 32 حرفاً';

//Suborders statuses
$_['ms_menu_suborders_statuses'] = 'حالات الطلب';
$_['ms_suborder_status_heading'] = 'حالات الطلب';
$_['ms_suborder_status_tab_oc_status'] = 'حالات الطلب بالنسبة للمتجر';
$_['ms_suborder_status_tab_ms_status'] = 'حالات الطلب بالنسبة للبائع';
$_['ms_suborder_status_name'] = 'الاسم';
$_['ms_suborder_status_action'] = 'الإجراءات';
$_['ms_suborder_status_add_heading'] = 'إضافة حالة طلب بائع جديدة';
$_['ms_suborder_status_add_success'] = 'لقد قمت بإضافة حالة طلب بائع جديدة بنجاح!';
$_['ms_suborder_status_edit_heading'] = 'تعديل حالة طلب البائع';
$_['ms_suborder_status_edit_success'] = 'لقد قمت بتعديل حالة طلب البائع بنجاح!';
$_['ms_suborder_status_breadcrumbs'] = 'حالات الطلب';
$_['ms_suborder_status_delete_success'] = 'لقد قمت حالة طلب بائع!';
$_['ms_suborder_status_oc_delete_success'] = 'لقد قمت بحذف حالة الطلب بالنسبة للمتجر!';
$_['ms_suborder_status_name_error'] = 'خطأ: الاسم يجب أن يكون بين 3 و 32 حرفاً';
$_['ms_suborder_status_info_disabled_delete'] = "هذه الحالة مرتبطة بواحدة من حالات MultiMerch أو تنتمي إلى واحدة أو أكثر من الطلبات لذلك لا يمكن حذفها.";

// Events
$_['ms_event_heading'] = 'أحداث المتجر';
$_['ms_event_breadcrumbs'] = 'أحداث المتجر';
$_['ms_event_column_event'] = 'الحدث';
$_['ms_event_column_description'] = 'الوصف';
$_['ms_event_product'] = 'المنتج';
$_['ms_event_seller'] = 'البائع';
$_['ms_event_customer'] = 'العميل';
$_['ms_event_order'] = 'الطلب';

$_['ms_event_user_deleted'] = '*تم حذف المستخدم*';

// Product events
$_['ms_event_type_' . \MultiMerch\Event\Event::PRODUCT_CREATED] = 'منتج جديد';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::PRODUCT_CREATED] = 'منتج جديد: <a href="%s" target="_blank" class="product">%s</a> تم إنشاءه من قبل البائع: <a href="%s" target="_blank" class="seller">%s</a>.';
$_['ms_event_type_' . \MultiMerch\Event\Event::PRODUCT_MODIFIED] = 'تعديل منتج';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::PRODUCT_MODIFIED] = 'المنتج: <a href="%s" target="_blank" class="product">%s</a> تم تعديله من قبل <a href="%s" target="_blank" class="seller">%s</a>.';

// Seller events
$_['ms_event_type_' . \MultiMerch\Event\Event::SELLER_CREATED] = 'بائع جديد';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::SELLER_CREATED] = 'تم تسجيل بائع جديد: <a href="%s" target="_blank" class="seller">%s</a>!';
$_['ms_event_type_' . \MultiMerch\Event\Event::SELLER_MODIFIED] = 'تعديل بائع';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::SELLER_MODIFIED] = 'البائع: <a href="%s" target="_blank" class="seller">%s</a> قام بتعديل صفحته الشخصية.';

// Customer events
$_['ms_event_type_' . \MultiMerch\Event\Event::CUSTOMER_CREATED] = 'عميل جديد';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::CUSTOMER_CREATED] = 'تم تسجيل عميل جديد: <a href="%s" target="_blank" class="customer">%s</a>!';
$_['ms_event_type_' . \MultiMerch\Event\Event::CUSTOMER_MODIFIED] = 'تعديل عميل';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::CUSTOMER_MODIFIED] = 'العميل: <a href="%s" target="_blank" class="customer">%s</a> قام بتعديل معلوماته.';

// Order events
$_['ms_event_type_' . \MultiMerch\Event\Event::ORDER_CREATED] = 'طلب جديد';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::ORDER_CREATED] = 'طلب جديد <a href="%s" target="_blank" class="order">#%s</a> تم وضعه بواسطة <a href="%s" target="_blank" class="customer">%s</a>.';
$_['ms_event_type_template_' . \MultiMerch\Event\Event::ORDER_CREATED . '_guest'] = 'طلب جديد <a href="%s" target="_blank" class="order">#%s</a> تم وضعه بواسطة ضيف.';

// Notifications
$_['ms_notification_heading'] = "الإشعارات";
$_['ms_notification_breadcrumbs'] = "الإشعارات";
$_['ms_notification_message'] = "رسالة";

// Debug
$_['ms_debug_heading'] = 'وضع التصحيح';
$_['ms_debug_breadcrumbs'] = 'وضع التصحيح';
$_['ms_debug_info'] = 'معلومات تصحيح MultiMerch';
$_['ms_debug_sub_heading_title'] = 'معلومات النظام';
$_['ms_debug_multimerchinfo_heading_title'] = 'سجل التصحيح';
$_['ms_debug_phpinfo_heading_title'] = 'معلومات PHP';
$_['ms_debug_warning_vqmod_not_installed'] = 'لم يتم تثبيت VQMod!';
$_['ms_debug_warning_server_log_not_available'] = 'سجل الخادم غير متاح';
$_['ms_debug_warning_hash_file_invalid'] = 'ملف الهاش غير صالح';
$_['ms_debug_warning_hash_file_not_find'] = 'ملف الهاش غير موجود';


// Finances - Transactions
$_['ms_transactions_heading'] = 'العمليات';
$_['ms_transactions_breadcrumbs'] = 'العمليات';
$_['ms_transactions_new'] = 'عملية جديدة';

$_['ms_error_transaction_fromto'] = 'يرجى تحديد البائع المرسل أو المستلم على الأقل';
$_['ms_error_transaction_fromto_same'] = 'لا يمكن أن يكون المرسل هو نفسه المستلم';
$_['ms_error_transaction_amount'] = 'يرجى تحديد قيمة موجبة صالحة';
$_['ms_success_transaction_created'] = 'تم إنشاء العملية بنجاح';

$_['button_cancel'] = 'إلغاء';
$_['button_save'] = 'حفظ';
$_['ms_action'] = 'الإجراءات';


// Account - Conversations and Messages
$_['ms_account_conversations'] = 'المحادثات';
$_['ms_account_messages'] = 'الرسائل';
$_['ms_sellercontact_success'] = 'تم إرسال رسالتك بنجاح';

$_['ms_account_conversations_heading'] = 'محادثاتك';
$_['ms_account_conversations_breadcrumbs'] = 'محادثاتك';

$_['ms_account_conversations_status'] = 'الحالة';
$_['ms_account_conversations_from'] = 'محادثة من';
$_['ms_account_conversations_from_admin_prefix'] = ' (المسؤول)';
$_['ms_account_conversations_to'] = 'محادثة إلى';
$_['ms_account_conversations_title'] = 'العنوان';
$_['ms_account_conversations_type'] = 'نوع المحادثة';
$_['ms_account_conversations_date_added'] = 'تاريخ الإنشاء';
$_['ms_account_conversations_with_seller'] = 'محادثة مع البائع';

$_['ms_account_conversations_error_deleting'] = 'حدث خطأ أثناء حذف المحادثة!';
$_['ms_account_conversations_success_deleting'] = 'تم حذف المحادثة بنجاح!';

$_['ms_account_conversations_sender_type_seller'] = 'البائع';
$_['ms_account_conversations_sender_type_buyer'] = 'المشتري';
$_['ms_account_conversations_sender_type_admin'] = 'المسؤول';

$_['ms_conversation_title_product'] = 'استفسار عن المنتج: %s';
$_['ms_conversation_title_order'] = 'استفسار عن الطلب: %s';
$_['ms_conversation_title'] = 'استفسار من %s';

$_['ms_account_conversations_read'] = 'مقروء';
$_['ms_account_conversations_unread'] = 'غير مقروء';

$_['ms_account_messages_heading'] = 'الرسائل';
$_['ms_last_message'] = 'آخر رسالة';
$_['ms_message_text'] = 'رسالتك';
$_['ms_post_message'] = 'ارسل';

$_['ms_customer_does_not_exist'] = 'تم حذف حساب العميل';
$_['ms_error_empty_message'] = 'لا يمكن ترك خانة الرسالة فارغة';

$_['ms_account_conversations_textarea_placeholder'] = 'اكتب رسالتك...';
$_['ms_account_conversations_upload'] = 'رفع ملف';
$_['ms_account_conversations_file_uploaded'] = 'تم رفع ملفك بنجاح!';
$_['ms_error_file_extension'] = 'امتداد غير صالح';

$_['ms_mail_subject_private_message'] = 'رسالة جديدة';
$_['ms_mail_private_message'] = <<<EOT
لقد استلمت رسالة جديدة من %s!

%s

%s

تستطيع الرد عليها في قسم الرسائل بحسابك.
EOT;

$_['ms_account_message'] = 'الرسالة';
$_['ms_account_message_sender'] = 'المرسل';
$_['ms_account_message_attachments'] = 'المرفقات';

// Attributes
$_['ms_attribute_heading'] = 'المواصفات';
$_['ms_attribute_breadcrumbs'] = 'المواصفات';
$_['ms_attribute_create'] = 'مواصفة جديدة';
$_['ms_attribute_edit'] = 'تعديل المواصفة';
$_['ms_attribute_value'] = 'قيمة المواصفة';
$_['ms_error_attribute_name'] = 'اسم المواصفة يجب أن يكون بين 1 و 128 حرفاً';
$_['ms_error_attribute_type'] = 'نوع هذه المواصفة يتطلب وضع قيم للمواصفة';
$_['ms_error_attribute_value_name'] = 'قيمة المواصفة يجب أن تكون بين 1 و 128 حرفاً';
$_['ms_success_attribute_created'] = 'تم إنشاء المواصفة بنجاح';
$_['ms_success_attribute_updated'] = 'تم تعديل المواصفة بنجاح';

$_['button_cancel'] = 'إلغاء';
$_['button_save'] = 'حفظ';
$_['ms_action'] = 'الإجراءات';

// Mails
$_['ms_mail_greeting'] = "مرحباً %s,\n";
$_['ms_mail_greeting_no_name'] = "مرحباً,\n";
$_['ms_mail_ending'] = "\nتحياتنا,\n%s";
$_['ms_mail_message'] = "\nالرسالة:\n%s";

$_['ms_mail_subject_seller_account_modified'] = 'تعديل حساب البائع';
$_['ms_mail_seller_account_modified'] = <<<EOT
حساب البائع الخاص بك في %s قد تم تعديله من قبل الإدارة.

حالة الحساب: %s
EOT;

$_['ms_mail_subject_product_modified'] = 'تعديل المنتج';
$_['ms_mail_product_modified'] = <<<EOT
منتجك: %s في %s قد تم تعديله من قبل الإدارة.

حالة المنتج: %s
EOT;

$_['ms_mail_subject_product_purchased'] = 'طلب جديد';
$_['ms_mail_product_purchased'] = <<<EOT
تم شراء منتج أو أكثر من منتجاتك في %s.

العميل: %s (%s)

المنتجات:
%s
الإجمالي: %s
EOT;

$_['ms_mail_product_purchased_no_email'] = <<<EOT
تم شراء منتج أو أكثر من منتجاتك في %s.

العميل: %s

المنتجات:
%s
الإجمالي: %s
EOT;

$_['ms_mail_product_purchased_info'] = <<<EOT
\n
عنوان التوصيل:

%s %s
%s
%s
%s
%s %s
%s
%s
EOT;

$_['ms_mail_product_purchased_comment'] = 'الملاحظات: %s';

$_['ms_mail_subject_product_reviewed'] = 'مراجعة جديدة لمنتج';
$_['ms_mail_product_reviewed'] = <<<EOT
تم وضع تقييم جديد لمنتج %s.
يرجى زيارة الرابط التالي للمشاهدة: <a href="%s">%s</a>
EOT;

// Catalog - Mail
// Attributes
$_['ms_mail_subject_attribute_status_changed'] = 'تحديث حالة مواصفة المنتج';
$_['ms_mail_attribute_status_changed'] = <<<EOT
حالة مواصفة المنتج الخاصة بك: <strong>%s</strong> قد تم تحديثها إلى: <strong>%s</strong>.
EOT;

$_['ms_mail_subject_attribute_seller_changed'] = 'تغيير مالك المواصفة';
$_['ms_mail_attribute_seller_attached'] = <<<EOT
تم تعيين المواصفة %s إلى حسابك.
EOT;
$_['ms_mail_attribute_seller_detached'] = <<<EOT
تم إعادة تعيين المواصفة %s من حسابك.
EOT;

$_['ms_mail_subject_attribute_converted_to_global'] = 'تحويل المواصفة';
$_['ms_mail_attribute_converted_to_global'] = <<<EOT
المواصفة الخاصة بك: "%s" تم وضعها لاستخدام الكل.
EOT;

// Attribute groups
$_['ms_mail_subject_attribute_group_status_changed'] = 'تحديث حالة مجموعة المواصفات';
$_['ms_mail_attribute_group_status_changed'] = <<<EOT
حالة مجموعة مواصفاتك: <strong>%s</strong> تم تحديثها إلى: <strong>%s</strong>.
EOT;

// Options
$_['ms_mail_subject_option_status_changed'] = 'تحديث حالة الخيار';
$_['ms_mail_option_status_changed'] = <<<EOT
حالة الخيار الخاصة بك: <strong>%s</strong> تم تحديثها إلى: <strong>%s</strong>.
EOT;

$_['ms_mail_subject_option_seller_changed'] = 'تغيير مالك الخيار';
$_['ms_mail_option_seller_attached'] = <<<EOT
تم تعيين الخيار %s إلى حسابك.
EOT;
$_['ms_mail_option_seller_detached'] = <<<EOT
تم إعادة تعيين %s من حسابك.
EOT;

$_['ms_mail_subject_option_converted_to_global'] = 'تغيير الحالة';
$_['ms_mail_option_converted_to_global'] = <<<EOT
الخيار الخاص بك: "%s" تم وضعه لاستخدام الكل.
EOT;

// Categories
$_['ms_mail_subject_category_status_changed'] = 'تحديث حالة القسم';
$_['ms_mail_category_status_changed'] = <<<EOT
حالة القسم الخاصة بك: <strong>%s</strong> تم تحديثها إلى: <strong>%s</strong>.
EOT;

// Sales - Mail
$_['ms_transaction_order_created'] = 'طلب جديد';
$_['ms_transaction_order'] = 'المبيعات: رقم الطلب #%s';
$_['ms_transaction_suborder'] = 'رسوم البيع للبائع %s في طلب #%s';
$_['ms_transaction_sale'] = 'بيع: %s (-%s رسوم)';
$_['ms_transaction_sale_no_commission'] = 'رسوم: %s';
$_['ms_transaction_sale_fee'] = 'رسوم البيع: %s';
$_['ms_transaction_sale_fee_order'] = 'رسوم البيع: طلب #%s';
$_['ms_transaction_sale_fee_order_refund'] = 'إرجاع رسوم البيع: طلب #%s';
$_['ms_transaction_refund'] = 'المرتجع: %s';
$_['ms_transaction_shipping'] = 'الشحن: %s';
$_['ms_transaction_shipping_order'] = 'شحن: طلب #%s';
$_['ms_transaction_shipping_refund'] = 'إعادة رسوم الشحن: %s';
$_['ms_transaction_shipping_order_refund'] = 'إعادة رسوم الشحن: طلب #%s';
$_['ms_transaction_coupon'] = 'كوبون خصم المتجر';
$_['ms_transaction_coupon_refund'] = 'إعادة مبلغ كوبون المتجر';
$_['ms_transaction_ms_coupon'] = 'خصم الكوبون';
$_['ms_transaction_ms_coupon_refund'] = 'إعادة مبلغ الكوبون';

$_['ms_payment_method'] = 'وسيلة الدفع';
$_['ms_payment_method_balance'] = "خصم من رصيد البائع";
$_['ms_payment_royalty_payout'] = 'مستحقات %s في %s';
$_['ms_payment_completed'] = 'تم الدفع';
$_['ms_payment_status'] = 'حالة الدفع';

// Payment methods
$_['ms_pg_manage'] = 'إدارة وسائل الدفع';
$_['ms_pg_heading'] = 'وسائل الدفع';
$_['ms_pg_install'] = 'تم تثبيت وسيلة %s بنجاح!';
$_['ms_pg_uninstall'] = 'لقد قمت بحذف وسيلة %s بنجاح!';
$_['ms_pg_modify'] = 'لقد قمت بتعديل وسيلة %s بنجاح!';
$_['ms_pg_modify_error'] = 'تحذير: ليس لديك الصلاحية لتعديل إضافات وسيلة الدفع!';
$_['ms_pg_for_fee'] = 'تمكين للرسوم:';
$_['ms_pg_for_payout'] = 'تمكين للمستحقات:';
$_['ms_pg_uninstall_warning'] = 'تحذير!\nسيتم حذف كافة إعدادات وسيلة الدفع لكل البائعين.\n\n هل أنت متأكد من المتابعة؟';
$_['ms_pg_fee_payment_method_name'] = 'إنشئ فاتورة دفع';

// Payments
$_['ms_pg_payment_number'] = '# الدفع';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_PAID_REQUESTS] = 'الفواتير المدفوعة';
$_['ms_pg_payment_type_' . MsPgPayment::TYPE_SALE] = 'المبيعات';

$_['ms_pg_payment_status_' . MsPgPayment::STATUS_INCOMPLETE] = '<p style="color: red">غير مكتمل</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_INCOMPLETE] = 'غير مكتمل';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_COMPLETE] = '<p style="color: green">مكتمل</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_COMPLETE] = 'مكتمل';
$_['ms_pg_payment_status_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = '<p style="color: blue">في انتظار التأكيد</p>';
$_['ms_pg_payment_status_no_color_' . MsPgPayment::STATUS_WAITING_CONFIRMATION] = 'إنتظار الموافقة';

$_['ms_pg_payment_error_no_method'] = 'خطأ: يجب اختيار وسيلة دفع!';
$_['ms_pg_payment_error_no_methods'] = 'يجب عليك اختيار دفع واحد على الأقل!';
$_['ms_pg_payment_error_no_requests'] = 'خطأ: يجب اختيار فاتورة!';
$_['ms_pg_payment_error_payment'] = 'خطأ: لا يمكن إنشاء الدفع!';
$_['ms_pg_payment_error_sender_data'] = 'خطأ: لم يقم المسؤول بتحديد المعلومات المطلوبة!';
$_['ms_pg_payment_error_receiver_data'] = 'خطأ: لم يقم بائع أو أكثر بتحديد المعلومات المطلوبة!';

// Payouts
$_['ms_payout_heading'] = 'المستحقات';
$_['ms_payout_payout'] = 'المستحق';
$_['ms_payout_all_payouts'] = 'المستحقات السابقة';
$_['ms_payout_invoice'] = 'الفاتورة';

$_['ms_payout_seller_list_info'] = 'البائعون الذين ليس لديهم أرصدة أو رصيدهم بالسالب لا يمكن الدفع لهم بالتالي لا يتم عرضهم.';
$_['ms_payout_seller_list_generate'] = 'بدء استحقاق جديد للبائعين';
$_['ms_payout_seller_list_refresh'] = 'تحديث';
$_['ms_payout_seller_list_pending'] = 'قيد الانتظار';
$_['ms_payout_seller_list_payout_name'] = 'اسم المستحق';

$_['ms_payout_view_heading'] = 'فواتير الاستحقاق #%s';

$_['ms_payout_confirm'] = 'تأكيد الاستحقاق';
$_['ms_payout_selected_sellers'] = 'البائعون المختارون';
$_['ms_payout_date_payout_period'] = 'فترة الاستحقاق';
$_['ms_payout_date_payout_period_until'] = 'حتى %s';
$_['ms_payout_error_no_sellers'] = 'خطأ: لم يتم اختيار بائعين!';
$_['ms_payout_success_payout_created'] = 'تم إنشاء المستحق #%s بنجاح!';

// Validation messages
$_['ms_validate_default'] = 'خانة \'%s\' غير صالحة';
$_['ms_validate_required'] = 'خانة \'%s\' إلزامية';
$_['ms_validate_alpha_numeric'] = 'خانة \'%s\' يجب أن تحوي على حروف لاتينية وأرقام فقط';
$_['ms_validate_latin'] = 'يجب أن يحتوي حقل \'%s\' على الحروف اللاتينية الموسعة فقط';
$_['ms_validate_utf8'] = 'يجب أن يحتوي حقل \'%s\' على حروف بترميز UTF-8 فقط';
$_['ms_validate_max_len'] = 'خانة \'%s\' يجب أن تكون \'%s\' أو أقصر في الطول';
$_['ms_validate_min_len'] = 'خانة \'%s\' يجب أن تكون \'%s\' أو أكبر في الطول';
$_['ms_validate_phone_number'] = 'خانة \'%s\' لا تبدو كرقم هاتف';
$_['ms_validate_numeric'] = 'خانة \'%s\' يجب أن تحوي أرقاماً فقط';
$_['ms_validate_email'] = 'حقل \'%s\' يجب أن يكون عنواناً بريدياً صالحاً';
$_['ms_validate_email_exists'] = 'البريد الالكتروني الذي أدخلته مسجل مسبقاً';
$_['ms_validate_password_confirm'] = 'تاكيد كلمة المرور وكلمة المرور غير متطابقين';

// Seller group settings
$_['ms_seller_group_product_number_limit'] = 'الحد الأقصى للمنتجات';

// Category-based and product-based fees
$_['ms_fees_heading'] = 'رسوم ملتي ميرش';
$_['ms_config_fee_priority'] = 'أولوية الرسوم';
$_['ms_config_fee_priority_catalog'] = 'القسم/المنتج';
$_['ms_config_fee_priority_vendor'] = 'البائع';
$_['ms_config_fee_priority_note'] = 'مع \'فهرس\' خيار مختار, الأقسام / المنتجات الإدراج ورسوم البيع ستكون لها الأولوية على البائع / إعدادات رسوم مجموعة البائعين (والعكس مع \'بائع\' خيار مختار)';

// Seller attributes
$_['ms_global_attribute'] = '--عام--';
$_['ms_catalog_attribute_attach_to_seller'] = 'تعيين لبائع';
$_['ms_catalog_attribute_all_sellers'] = '--كل البائعين--';
$_['ms_seller_attribute'] = 'المواصفة';
$_['ms_seller_attribute_group'] = 'مجموعة المواصفات';
$_['ms_seller_attribute_manage'] = 'إدارة المواصفات';

$_['ms_seller_attribute_tab_ocattribute'] = 'مواصفات المتجر';
$_['ms_seller_attribute_tab_msattribute'] = 'مواصفات البائع';

$_['ms_seller_attribute_updated'] = 'تم تحديث المواصفة/المواصفات بنجاح!';
$_['ms_seller_attribute_deleted'] = 'تم حذف المواصفة/المواصفات بنجاح!';
$_['ms_seller_attribute_group_updated'] = 'تم تحديث مجموعة/مجموعات المواصفات بنجاح!';
$_['ms_seller_attribute_group_deleted'] = 'تم حذف مجموعة/مجموعات المواصفات بنجاح!';

$_['ms_seller_attribute_error_creating'] = 'حدث خطأ أثناء إنشاء المواصفة!';
$_['ms_seller_attribute_error_updating'] = 'حدث خطأ أثناء تحديث المواصفة!';
$_['ms_seller_attribute_error_deleting'] = 'حدث خطأ أثناء حذف المواصفة!';
$_['ms_seller_attribute_error_assigned'] = 'تحذير: لا يمكن حذف المواصفة `%s` كونها مرتبطة بـ%s منتج/منتجات!';
$_['ms_seller_attribute_error_not_selected'] = 'يجب عليك اختيار مواصفة واحدة على الأقل!';

$_['ms_seller_attribute_group_error_creating'] = 'حدث خطأ أثناء إنشاء مجموعة المواصفات!';
$_['ms_seller_attribute_group_error_updating'] = 'حدث خطأ أثناء تعديل مجموعة المواصفات!';
$_['ms_seller_attribute_group_error_deleting'] = 'حدث خطأ أثناء حذف مجموعة المواصفات!';
$_['ms_seller_attribute_group_error_assigned'] = 'تحذير: لا يمكن حذف مجموعة المواصفات `%s` كونها مرتبطة بـ%s مواصفة/مواصفات!';
$_['ms_seller_attribute_group_error_not_selected'] = 'يجب عليك اختيار مجموعة مواصفات واحدة على الأقل!';

$_['ms_seller_attribute_status_' . MsAttribute::STATUS_DISABLED] = 'معطل';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_APPROVED] = 'موافق عليه';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_attribute_status_' . MsAttribute::STATUS_INACTIVE] = 'غير نشط';

// Seller options
$_['ms_global_option'] = '--عام--';
$_['ms_catalog_option_attach_to_seller'] = 'تعيين لبائع';
$_['ms_catalog_option_all_sellers'] = '--كل البائعين--';
$_['ms_seller_option_heading'] = 'الخيارات';
$_['ms_seller_option_breadcrumbs'] = 'الخيارات';
$_['ms_seller_option'] = 'الخيار';
$_['ms_seller_option_type'] = 'النوع';
$_['ms_seller_option_values'] = 'قيم الخيار';
$_['ms_seller_option_manage'] = 'إدارة الخيارات';

$_['ms_seller_option_tab_ocoptions'] = 'خيارات المتجر';
$_['ms_seller_option_tab_msoptions'] = 'خيارات البائع';

$_['ms_seller_option_updated'] = 'تم تحديث الخيار(ات) بنجاح!';
$_['ms_seller_option_deleted'] = 'تم حذف الخيار(ات) بنجاح!';

$_['ms_seller_option_error_creating'] = 'حدث خطأ أثناء إنشاء الخيار!';
$_['ms_seller_option_error_updating'] = 'حدث خطأ أثناء تعديل الخيار!';
$_['ms_seller_option_error_deleting'] = 'حدث خطأ أثناء حذف الخيار!';
$_['ms_seller_option_error_assigned'] = 'تحذير: لا يمكن حذف الخيار `%s` كونه  مرتبط بـ%s منتج/منتجات!';
$_['ms_seller_option_error_not_selected'] = 'يجب عليك إختيار خيار واحد على الأقل!';

$_['ms_seller_option_status_' . MsOption::STATUS_DISABLED] = 'معطل';
$_['ms_seller_option_status_' . MsOption::STATUS_APPROVED] = 'موافق عيه';
$_['ms_seller_option_status_' . MsOption::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_option_status_' . MsOption::STATUS_INACTIVE] = 'غير نشط';

// Seller categories
$_['ms_global_category'] = '--عام--';
$_['ms_catalog_category_attach_to_seller'] = 'تعيين لبائع';
$_['ms_catalog_category_all_sellers'] = '--كل البائعين--';
$_['ms_seller_category_heading'] = 'الأقسام';
$_['ms_seller_category_breadcrumbs'] = 'الأقسام';
$_['ms_seller_category'] = 'الأقسام';
$_['ms_seller_category_manage'] = 'إدارة الأقسام';
$_['ms_categories_tab_occategories'] = 'أقسام المتجر';
$_['ms_categories_tab_mscategories'] = 'أقسام البائع';
/***** INSERTED *****/$_['ms_categories_view_mscategory_front_page'] = 'View seller category';
/***** INSERTED *****/$_['ms_categories_view_occategory_front_page'] = 'View marketplace category';


$_['ms_categories_button_add_occategory'] = 'إضافة قسم للمتجر';
$_['ms_categories_button_add_mscategory'] = 'إضافة قسم للبائع';

$_['ms_seller_newcategory_heading'] = 'إضافة قسم جديد للبائع';
$_['ms_seller_editcategory_heading'] = 'تعديل قسم البائع';
$_['ms_seller_category_general'] = 'عام';
$_['ms_seller_category_name'] = 'الاسم';
$_['ms_seller_category_description'] = 'الوصف';
$_['ms_seller_category_meta_title'] = 'عنوان الميتا';
$_['ms_seller_category_meta_description'] = 'وصف الميتا';
$_['ms_seller_category_meta_keyword'] = 'كلمات الميتا';
$_['ms_seller_category_data'] = 'البيانات';
$_['ms_seller_category_seller'] = 'البائع';
$_['ms_seller_category_parent'] = 'القسم الرئيسي';
$_['ms_seller_category_filter'] = 'الفلاتر';
$_['ms_seller_category_store'] = 'المتاجر';
$_['ms_seller_category_keyword'] = 'رابط SEO';
$_['ms_seller_category_image'] = 'الصورة';
$_['ms_seller_category_sort_order'] = 'ترتيب الفرز';
$_['ms_seller_category_status'] = 'الحالة';

$_['ms_seller_category_created'] = 'تم إنشاء القسم بنجاح!';
$_['ms_seller_category_updated'] = 'تم تعديل القسم بنجاح!';
$_['ms_seller_category_deleted'] = 'تم حذف القسم بنجاح!';

$_['ms_oc_editcategory_heading'] = 'قم بتعديل أقسام المتجر';
$_['ms_oc_newcategory_heading'] = 'قم بإضافة قسم جديد للمتجر';
$_['ms_oc_category_error_keyword'] = 'روابط SEO مستخدمة بالفعل!';
$_['ms_oc_category_error_parent'] = 'القسم الأساسي الذي قمت بإختياره هو قسم فرعي لقسم حالي!';
$_['ms_oc_category_error_name'] = 'أسماء الأقسام يجب أن تكون ما بين 2 إلى 255 حروف!';

$_['ms_seller_category_error_creating'] = 'حدث خطأ أثناء إنشاء القسم!';
$_['ms_seller_category_error_updating'] = 'حدث خطأ أثناء تعديل القسم!';
$_['ms_seller_category_error_deleting'] = 'حدث خطأ أثناء حذف القسم!';
$_['ms_seller_category_error_assigned'] = 'تحذير: لا يمكن حذف هذا القسم كونه مرتبط بـ%s منتج/منتجات!';
$_['ms_seller_category_error_no_sellers'] = 'لا يوجد بائعين متاحين';
$_['ms_seller_category_error_not_selected'] = 'يجب عليك اختيار قسم واحد على الأقل!';

$_['ms_seller_category_status_' . MsCategory::STATUS_DISABLED] = 'معطل';
$_['ms_seller_category_status_' . MsCategory::STATUS_ACTIVE] = 'نشط';
$_['ms_seller_category_status_' . MsCategory::STATUS_INACTIVE] = 'غير نشط';

// Sale > Order > Info
$_['ms_order_details'] = "تفاصيل الطلب";
$_['ms_order_details_by_seller'] = 'تفاصيل الطلب عبر البائع';
$_['ms_order_products_by'] = "البائع: %s";
$_['ms_order_id'] = "رقم طلب البائع الفريد:";
$_['ms_order_current_status'] = "حالة طلب البائع الحالية:";
$_['ms_order_sold_by'] = 'مباع من قبل:';
$_['ms_order_payment_history'] = 'تاريخ الدفع';
$_['ms_order_payment_status'] = 'حالة الدفع: <strong>%s</strong>';
$_['ms_order_payment_method'] = 'طريقة الدفع: <strong>%s</strong>';
$_['ms_order_order_status'] = 'حالة الطلب: <strong>%s</strong>';
$_['ms_order_order_date'] = 'تاريخ الطلب: <strong>%s</strong>';
$_['ms_order_change_order_status'] = 'تغير حالة الطلب';
$_['ms_order_change_order_status_placeholder'] = "-- إختر الحالة الجديدة للطلب --";
$_['ms_order_change_order_status_error'] = "يجب عليك إختيار حالة الطلب";
$_['ms_order_change_payment_status'] = 'تغير حالة الدفع';
$_['ms_order_change_payment_status_placeholder'] = "-- إختر حالة الدفع الجديدة --";
$_['ms_order_change_payment_status_error'] = "يجب عليك إختيار حالة دفع";
$_['ms_order_notify_customer_no'] = "لا تقم بتبليغ العميل والبائع(ين) عن هذا التغير ( لن يتم إرسال أي بريد)";
$_['ms_order_notify_customer_yes'] = "بلغ العميل والبائع(ين) عن هذا التغير (سيتم إرسال بريد الكتروني)";
$_['ms_order_address'] = "العنوان";
$_['ms_order_payment_address'] = "عنوان الدفع";
$_['ms_order_shipping_address'] = "عنوان الشحن";
$_['ms_order_comment'] = "تعليق";

$_['ms_order_transactions'] = "عمليات الرصيد";
$_['ms_order_transactions_amount'] = 'المبلغ';
$_['ms_order_transactions_description'] = 'الوصف';
$_['ms_order_date_created'] = 'تاريخ الإنشاء';
$_['ms_order_notransactions'] = 'البائع لم يستلم بعد أي عمليات تتعلق بهذا الطلب';

$_['ms_order_status_initial'] = 'تم إنشاء طلب';
$_['ms_order_history'] = "تاريخ الطلب";

// Reviews
$_['ms_review_heading'] = 'التقييمات';
$_['ms_review_breadcrumbs'] = 'التقييمات';
$_['ms_review_manage'] = 'إدارة التقييمات';
$_['ms_review_column_product'] = 'المنتج';
$_['ms_review_column_customer'] = 'العميل';
$_['ms_review_column_seller'] = 'البائع';
$_['ms_review_column_order'] = 'الطلب';
$_['ms_review_column_rating'] = 'التقييم';
$_['ms_review_column_comment'] = 'التعليق';
$_['ms_review_column_date_added'] = 'التاريخ';

$_['ms_review_general'] = 'التقييم';
$_['ms_review_edit_heading'] = "مراجعة العميل";
$_['ms_review_edit_product'] = 'المنتج';
$_['ms_review_edit_order'] = 'رقم الطلب';
$_['ms_review_edit_customer'] = 'العميل';
$_['ms_review_edit_review'] = 'التعليق';
$_['ms_review_edit_seller_response'] = "رد البائع";
$_['ms_review_edit_response'] = 'الرد';
$_['ms_review_edit_rating'] = 'التقييم';
$_['ms_review_edit_customer_images'] = "صور العميل";
$_['ms_review_edit_images'] = 'الصور';

$_['ms_success_review_updated'] = 'تم تعديل المراجعة!';
$_['ms_success_review_deleted'] = 'تم حذف المراجعة!';
$_['ms_error_review_deleting'] = 'حدث خطأ أثناء حذف المراجعة!';
$_['ms_error_review_id'] = 'حدث خطأ أثناء معالجة طلبك!';

$_['ms_review_comments_success_added'] = 'تم وضع تعليقك بنجاح!';
$_['ms_review_comments_error_signin'] = 'يرجى تسجيل الدخول لوضع تعليق!';
$_['ms_review_comments_error_review_id'] = 'خطأ: لم يتم تحديد معرف التقييم!';
$_['ms_review_comments_error_notext'] = 'خطأ: يجب عليك إدخال الرسالة!';
$_['ms_review_comments_textarea_placeholder'] = 'ادخل رسالتك...';
$_['ms_review_comments_post_message'] = 'وضع تعليق';
$_['ms_review_no_comments'] = 'لم يقم البائع بالرد على هذه المراجعة حتى الآن.';

$_['ms_review_status_' . MsReview::STATUS_ACTIVE] = 'نشط';
$_['ms_review_status_' . MsReview::STATUS_INACTIVE] = 'غير نشط';

// Questions
$_['ms_question_heading'] = 'الأسئلة';
$_['ms_question_breadcrumbs'] = 'الأسئلة';
$_['ms_question_manage'] = 'إدارة الأسئلة';
$_['ms_question_column_product'] = 'المنتج';
$_['ms_question_column_customer'] = 'العميل';
$_['ms_question_column_answer'] = 'الإجابة';
$_['ms_question_column_date_added'] = 'التاريخ';

$_['ms_question_general'] = 'السؤال';
$_['ms_question_edit_heading'] = 'سؤال عن منتج';
$_['ms_question_edit_product'] = 'المنتج';
$_['ms_question_edit_customer'] = 'العميل';
$_['ms_question_edit_question'] = 'السؤال';
$_['ms_question_edit_seller_answer'] = "إجابة البائع";
$_['ms_question_edit_answer'] = 'الإجابة';
$_['ms_question_no_answers'] = 'لم يقم البائع بالرد على هذا السؤال حتى الآن!';
$_['ms_questions_customer_deleted'] = '*تم حذف العميل*';

$_['ms_success_question_updated'] = 'تم تعديل السؤال!';
$_['ms_success_question_deleted'] = 'تم حذف السؤال!';
$_['ms_error_question_deleting'] = 'حدث خطأ أثناء حذف السؤال!';
$_['ms_error_question_id'] = 'حدث خطأ أثناء معالجة طلبك!';
$_['ms_error_answer_id'] = 'حدث خطأ أثناء معالجة طلبك!';

// Reports
$_['ms_report_guest_checkout'] = 'ضيف';
$_['ms_report_column_order'] = 'الطلب';
$_['ms_report_column_product'] = 'المنتج';
$_['ms_report_column_seller'] = 'البائع';
$_['ms_report_column_customer'] = 'العميل';
$_['ms_report_column_email'] = 'البريد الالكتروني';
$_['ms_report_column_gross'] = 'الربح';
$_['ms_report_column_discount'] = 'الخصم';
$_['ms_report_column_net_total'] = 'الصافي';
$_['ms_report_column_net_marketplace'] = 'صافي المتجر';
$_['ms_report_column_net_seller'] = 'صافي البائع';
$_['ms_report_column_tax'] = 'الضريبة';
$_['ms_report_column_shipping'] = 'الشحن';
$_['ms_report_column_total'] = 'الإجمالي';
$_['ms_report_column_date'] = 'التاريخ';
$_['ms_report_column_date_month'] = 'الشهر';
$_['ms_report_column_total_sales'] = 'إجمالي المبيعات';
$_['ms_report_column_total_orders'] = 'إجمالي الطلبات';
$_['ms_report_column_transaction'] = 'العملية';
$_['ms_report_column_description'] = 'الوصف';
$_['ms_report_column_payment'] = 'الدفع';
$_['ms_report_column_method'] = 'الوسيلة';
$_['ms_report_column_payout'] = 'المستحق';
$_['ms_report_column_payer'] = 'الدافع';
$_['ms_report_column_balance_in'] = 'الرصيد المضاف';
$_['ms_report_column_balance_out'] = 'الرصيد المسحوب';
$_['ms_report_column_balance_current'] = 'الرصيد الحالي';
$_['ms_report_column_marketplace_earnings'] = 'أرباح المتجر';
$_['ms_report_column_seller_earnings'] = 'أرباح البائع';
$_['ms_report_column_payments_received'] = 'الدفعات المستلمة';
$_['ms_report_column_payouts_paid'] = 'المستحقات المدفوعة';
$_['ms_report_column_status'] = 'الحالة';
$_['ms_report_column_amount'] = 'المبلغ';
$_['ms_report_column_country'] = 'الدولة';
$_['ms_report_column_period'] = 'الفترة';

$_['ms_report_manage_orders'] = 'إدارة الطلبات';
$_['ms_report_manage_sellers'] = 'إدارة البائعين';
$_['ms_report_manage_customers'] = 'إدارة العملاء';
$_['ms_report_manage_products'] = 'إدارة المنتجات';

$_['ms_report_date_range_today'] = 'اليوم';
$_['ms_report_date_range_yesterday'] = 'أمس';
$_['ms_report_date_range_last7days'] = 'قبل 7 أيام';
$_['ms_report_date_range_last30days'] = 'قبل 30 أيام';
$_['ms_report_date_range_thismonth'] = 'هذا الشهر';
$_['ms_report_date_range_lastmonth'] = 'الشهر الماضي';
$_['ms_report_date_range_custom'] = 'فترة مخصصة';
$_['ms_report_date_range_apply'] = 'تطبيق';
$_['ms_report_date_range_cancel'] = 'إلغاء';

$_['ms_report_date_range_day_mo'] = 'إث';
$_['ms_report_date_range_day_tu'] = 'ث';
$_['ms_report_date_range_day_we'] = 'أر';
$_['ms_report_date_range_day_th'] = 'خ';
$_['ms_report_date_range_day_fr'] = 'ج';
$_['ms_report_date_range_day_sa'] = 'س';
$_['ms_report_date_range_day_su'] = 'أح';

$_['ms_report_date_range_month_jan'] = 'يناير';
$_['ms_report_date_range_month_feb'] = 'فبراير';
$_['ms_report_date_range_month_mar'] = 'مارس';
$_['ms_report_date_range_month_apr'] = 'أبريل';
$_['ms_report_date_range_month_may'] = 'مايو';
$_['ms_report_date_range_month_jun'] = 'يونيو';
$_['ms_report_date_range_month_jul'] = 'يوليو';
$_['ms_report_date_range_month_aug'] = 'أغسطس';
$_['ms_report_date_range_month_sep'] = 'سبتمبر';
$_['ms_report_date_range_month_oct'] = 'أكتوبر';
$_['ms_report_date_range_month_nov'] = 'نوفمبر';
$_['ms_report_date_range_month_dec'] = 'ديسمبر';

// Custom fields and field groups common
$_['ms_custom_field_heading'] = 'حقل مخصص';
$_['ms_custom_field_manage'] = 'إدارة الحقول المخصصة';
$_['ms_custom_field_general'] = 'عام';
$_['ms_custom_field_name'] = 'الاسم';
$_['ms_custom_field_location'] = 'الموقع';
$_['ms_custom_field_value'] = 'القيمة';
$_['ms_custom_field_sort_order'] = 'ترتيب الفرز';
$_['ms_custom_field_validation'] = 'التحقق من الصحة';
$_['ms_custom_field_validation_tooltip'] = 'استخدم التعبيرات الاعتيادية. مثال: /^[a-zA-Z\d+ ]{2,30}$/';
$_['ms_custom_field_cf_count'] = 'الحقول المخصصة المرتبطة';
$_['ms_custom_field_status'] = 'الحالة';
$_['ms_custom_field_required'] = 'مطلوب';
$_['ms_custom_field_type'] = 'النوع';
$_['ms_custom_field_note'] = 'ملاحظة';
$_['ms_custom_field_location_' . MsCustomField::LOCATION_PRODUCT] = 'المنتج';
$_['ms_custom_field_status_' . MsCustomField::STATUS_ACTIVE] = 'مفعل';
$_['ms_custom_field_status_' . MsCustomField::STATUS_DISABLED] = 'معطل';

// Custom fields
$_['ms_custom_field'] = 'حقل مخصص';
$_['ms_custom_field_breadcrumbs'] = 'حقل مخصص';
$_['ms_custom_field_new_heading'] = 'حقل مخصص جديد';
$_['ms_custom_field_edit_heading'] = 'تعديل الحقل المخصص';
$_['ms_custom_field_create'] = 'إضافة حقل مخصص';
$_['ms_custom_field_confirm_delete'] = 'تحذير: أنت على وشك حذف حقل/حقول مخصصة. كل معلومات المنتج المخزونة في هذه الحقول سيتم حذفها كذلك. لا يمكن التراجع عن هذه العملية بعد إتمامها. هل تريد حذف الحقول المخصصة؟';
$_['ms_custom_field_error_deleting'] = 'خطأ: غير قادر على حذف الحقل المخصص!';
$_['ms_custom_field_error_not_selected'] = 'يجب عليك اختيار حقل مخصص واحد على الأقل!';
$_['ms_custom_field_error_values'] = 'يجب عليك إنشاء قيمة واحدة للحقل المخصص على الأقل!';
$_['ms_custom_field_success_created'] = 'تم إنشاء الحقل المخصص بنجاح!';
$_['ms_custom_field_success_updated'] = 'تم تعديل الحقل المخصص بنجاح!';
$_['ms_custom_field_success_deleted'] = 'تم حذف الحقل المخصص بنجاح!';

// Custom field groups
$_['ms_custom_field_group'] = 'مجموعة الحقول المخصصة';
$_['ms_custom_field_group_breadcrumbs'] = 'مجموعة الحقول المخصصة';
$_['ms_custom_field_group_new_heading'] = 'مجموعة حقول مخصصة جديدة';
$_['ms_custom_field_group_edit_heading'] = 'تعديل مجموعة ا لحقول المخصصة';
$_['ms_custom_field_group_create'] = 'إنشاء مجموعة حقول مخصصة';
$_['ms_custom_field_group_confirm_delete'] = 'تحذير: أنت على وشك حذف مجموعة/مجموعات حقول مخصصة. كل الحقول المخصصة التي تنتمي إلى هذه المجموعات سيتم حذفها مع معلومات المنتجات المخزونة بهذه الحقول. لا يمكن التراجع عن هذه العملية بعد اتمامها. Delete هل تريد حذف مجموعات الحقول المخصصة?';
$_['ms_custom_field_group_error_deleting'] = 'خطأ: غير قادر على حذف مجموعة الحقول المخصصة!';
$_['ms_custom_field_group_error_locations'] = 'يجب عليك اختيار موقع واحد على الأقل لمجموعة الحقول المخصصة!';
$_['ms_custom_field_group_error_not_selected'] = 'يجب عليك اختيار مجموعة حقول مخصصة واحدة على الأقل!';
$_['ms_custom_field_group_success_created'] = 'تم إنشاء مجموعة الحقول المخصصة بنجاح!';
$_['ms_custom_field_group_success_updated'] = 'تم تعديل مجموعة الحقول المخصصة بنجاح!';
$_['ms_custom_field_group_success_deleted'] = 'تم حذف مجموعة الحقول المخصصة بنجاح!';

// Orders
$_['ms_order_heading'] = 'الطلبات';
$_['ms_order_breadcrumbs'] = 'الطلبات';
$_['ms_order_tab_orders'] = 'الطلبات';
$_['ms_order_tab_suborders'] = 'طلبات البائع';
$_['ms_order_column_date_added'] = 'تاريخ الإنشاء';
$_['ms_order_column_date_modified'] = 'تاريخ التعديل';
$_['ms_order_column_order_id'] = 'الطلب #';
$_['ms_order_column_suborder_id'] = 'طلب البائع #';
$_['ms_order_column_order_status'] = 'الحالة';
$_['ms_order_column_order_total'] = 'الإجمالي';
$_['ms_order_column_order_customer'] = 'العميل';
$_['ms_order_column_order_vendor'] = 'البائع';
$_['ms_order_column_vendor_statuses'] = 'حالات طلب البائع';
$_['ms_order_column_action'] = 'الإجراءات';

// Deletion of MultiMerch various items
$_['ms_delete_template_confirm'] = "تحذير: أنت على وشك حذف %s. %s";
$_['ms_delete_affected'] = 'هذا الإجراء سيؤثر على:';
$_['ms_delete_areyousure'] = 'هل أنت متأكد؟';
$_['ms_delete_attribute'] = '%s مواصفة/مواصفات';
$_['ms_delete_attribute_group'] = '%s مجموعة/مجموعات مواصفات';
$_['ms_delete_badge'] = '%s وسام/أوسمة';
$_['ms_delete_category'] = '%s قسم/أقسام';
$_['ms_delete_occategory'] = '%s قسم/أقسام';
$_['ms_delete_child_category'] = '%s قسم/أقسام فرعية';
$_['ms_delete_conversation'] = '%s محادثة/محادثات';
$_['ms_delete_custom_field'] = '%s حقل/حقول مخصصة';
$_['ms_delete_custom_field_group'] = '%s مجموعة/مجموعات حقول مخصصة';
$_['ms_delete_coupon'] = '%s قسيمة/قسائم تخفيض';
$_['ms_delete_invoice'] = '%s فاتورة/فواتير';
$_['ms_delete_msf_attribute'] = '%s مواصفات المنتج (ات)';
$_['ms_delete_msf_variation'] = '%s إختلاف(ات) المنتجات';
$_['ms_delete_msf_seller_property'] = '%s خصائص البائع(ين)';
$_['ms_delete_option'] = '%s خيار/خيارات';
$_['ms_delete_payment'] = '%s مدفوع/مدفوعات';
$_['ms_delete_product'] = '%s منتج/منتجات';
$_['ms_delete_question'] = '%s سؤال/أسئلة';
$_['ms_delete_review'] = '%s مراجعة/مراجعات';
$_['ms_delete_seller'] = '%s بائع/بائعين';
$_['ms_delete_seller_group'] = '%s مجموعة/مجموعات بائعين';
$_['ms_delete_shipping_method'] = '%s وسيلة/وسائل شحن';
$_['ms_delete_shipping_combined'] = '%s قاعدة/قواعد شحن تجميعي';
$_['ms_delete_shipping_product'] = '%s قاعدة/قواعد شحن منتج';
$_['ms_delete_social_channel'] = '%s قناة/قنوات تواصل اجتماعي';
$_['ms_delete_seller_balance_warning'] = "حذف بائع برصيد غير صفري سيمنعك من اجراء المزيد من عمليات الدفع.";

// Coupons
$_['ms_coupon_heading'] = "قسائم التخفيض";
$_['ms_coupon_breadcrumbs'] = "قسائم التخفيض";
$_['ms_coupon_edit_info'] = "قسائم البائع يمكن إنشاءها وتعديلها من خلال واجهة البائع نفسه.";
$_['ms_coupon_code'] = "الرمز";
$_['ms_coupon_value'] = "القيمة";
$_['ms_coupon_uses'] = "مرات الاستخدام";
$_['ms_coupon_date_start'] = "تاريخ البدء";
$_['ms_coupon_date_end'] = "تاريخ الانتهاء";
$_['ms_coupon_error_deleting'] = "حدث خطأ أثناء حذف القسيمة/القسائم!";
$_['ms_coupon_success_deleted'] = 'تم حذف القسيمة بنجاح!';

// Complaints
$_['ms_config_complaints'] = 'الشكاوى';
$_['ms_config_complaints_enable'] = 'السماح بالشكاوى للمنتجات والبائعين';
$_['ms_config_complaints_enable_note'] = 'السماح للعملاء بالتبليغ عن المنتجات والبائعين المخالفين للشروط';

$_['ms_complaints_manage'] = 'إدارة الشكاوى';
$_['ms_complaints_column_date_added'] = 'تم التقديم';
$_['ms_complaint_column_reason'] = 'سبب الإبلاغ';
$_['ms_complaint_column_reporter'] = 'الإبلاغ بواسطة';
$_['ms_error_complaint_id'] = 'حدث خطأ ما أثناء معالجة طلبك!';

// Holiday mode
$_['ms_config_holiday_mode'] = 'وضع العطلة/الإجازة';
$_['ms_config_holiday_mode_note'] = 'إسمح للبائعين بوضع متاجرهم في وضع العطلة/الإجازة بشكل مؤقت لمنع العملاء من وضع طلبات جديدة من البائعين.';

// Invoicing system
$_['ms_invoices'] = 'الفواتير';
$_['ms_invoice_heading'] = "الفواتير";
$_['ms_invoice_breadcrumbs'] = "الفواتير";
$_['ms_invoice_column_invoice_id'] = "رقم الفاتورة #";
$_['ms_invoice_column_type'] = "النوع";
$_['ms_invoice_column_title'] = "العنوان";
$_['ms_invoice_column_sender'] = "المرسل";
$_['ms_invoice_column_recipient'] = "المستلم";
$_['ms_invoice_column_status'] = "الحالة";
$_['ms_invoice_column_date_generated'] = "تاريخ الإنشاء";
$_['ms_invoice_column_total'] = "المجموع";
$_['ms_invoice_column_payment_id'] = "رقم الدفع #";

$_['ms_invoice_success_deleted'] = 'تم حذف الفاتورة بنجاح!';

$_['ms_invoice_error_not_selected'] = "خطاً: يجب عليك تحديد فاتورة واحدة على الأقل!";
$_['ms_invoice_error_not_exists'] = "خطاً: غير قادر على إسترداد الفاتورة #%s!";
$_['ms_invoice_error_not_created'] = "خطاً: غير قادر على إنشاء فاتورة (بائع %s )!";
$_['ms_invoice_error_seller_notselected'] = 'خطاً: يجب عليك تحديد بائع واحد على الأقل!';
$_['ms_invoice_error_type'] = 'خطاً: نوع فاتورة غير معروف!';

$_['ms_invoice_type_listing'] = "رسوم الإدراج";
$_['ms_invoice_type_signup'] = "رسوم التسجيل";
$_['ms_invoice_type_sale'] = "بيع";
$_['ms_invoice_type_payout'] = "مستحقات";

$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID] = '<p style="color: red">غير مدفوع</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_PAID] = '<p style="color: green">مدفوع</p>';
$_['ms_invoice_status_' . \MultiMerch\Core\Invoice\Invoice::STATUS_VOIDED] = '<p style="color: red">ملغي</p>';

$_['ms_invoice_title_product_listing'] = "رسوم الإدراج للمنتج <a href='%s'>%s</a>";
$_['ms_invoice_title_seller_signup'] = "رسوم الإشتراك للبائع <a href='%s'>%s</a> at %s";
$_['ms_invoice_title_sale_order_commission'] = "رسوم السوق للمنتجات بالترتيب <a href='%s'>#%s</a>";
$_['ms_invoice_title_sale_order_products'] = "طلب المنتجات <a href='%s'>#%s</a>";
$_['ms_invoice_title_seller_payout'] = "المستحقات لـ %s";

$_['ms_invoice_item_title_seller_payout'] = "صرف المستحقات للبائع %s";
$_['ms_invoice_item_title_product_listing'] = "رسوم الإدراج للمنتج %s";
$_['ms_invoice_item_title_seller_signup'] = "رسوم الإشتراك في %s";
$_['ms_invoice_item_title_sale_commission'] = "عمولة السوق لـ %s بالترتيب #%s";
$_['ms_invoice_item_title_sale_product'] = "منتج %s";

$_['ms_invoice_total_title_subtotal'] = "حاصل الجمع";
$_['ms_invoice_total_title_shipping'] = "شحن السوق";
$_['ms_invoice_total_title_mm_shipping_total'] = "شحن البائع";
$_['ms_invoice_total_title_coupon'] = "كوبون";
$_['ms_invoice_total_title_ms_coupon'] = "كوبون البائع";
$_['ms_invoice_total_title_tax'] = "الضريبة";
$_['ms_invoice_total_title_total'] = "المجموع";

// Shipping system
$_['ms_shipping_mode'] = "نظام الشحن";
$_['ms_shipping_mode_note'] = "نظام الشحن يتحكم بطريقة التوصيل في السوق الخاص بك";
$_['ms_shipping_fields'] = "حقول الشحن";
$_['ms_shipping_methods'] = "طرق الشحن";
$_['ms_shipping_methods_note'] = "حدد مجموعة من طرق الشحن (اسماء شركات الشحن) للبائعين للإختيار من بينها عند إنشاء قواعد الشحن.";
$_['ms_shipping_methods_manage'] = "إدارة طرق الشحن";
$_['ms_shipping_delivery_times'] = "وقت التوصيل";
$_['ms_shipping_delivery_times_note'] = "حدد مجموعة من أوقات التوصيل للبائعين للإختيار من بينها عند إنشاء قواعد الشحن";

$_['ms_shipping_type'] = "نوع الشحن";
$_['ms_shipping_type_note'] = <<<EOT
يتحكم وضع الشحن في طريقة التعامل مع عمليات التوصيل في السوق الخاص بك:<br>
<strong>تعطيل</strong> - جميع طرق الشحن معطلة.<br>
<strong>الرجوع لطرق شحن أوبن كارت</strong> - إستخدم نظام الشحن الإفتراضي لـ أوبن كارت (غير متاح للبائعين).<br>
<strong>شحن البائع (سعر موحد)</strong> - اسمح للبائعين بتحديد أسعار ثابتة إلى وجهات مختلفة.<br>
<strong>شحن البائع (بناء على قيمة السلة)</strong> - اسمح للبائعين بتحديد أسعار شحن إلى وجهات مختلفة بناء على قيمة إجمالي المنتجات في سلة الشراءالخاصة بالعملاء.<br>
<strong>شحن البائع (بناء على وزن السلة)</strong> - اسمح للبائعين بتحديد أسعار شحن مختلفة إلى وجهات مختلفة بناءً على الوزن للمنتجات في عربة الشراء الخاصة بالعملاء.<br>
<strong>شحن البائع (لكل منتج فقط)</strong> - اسمح للبائعين بتحديد أسعار الشحن المختلفة للمنتجات الفردية
EOT;

$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_DISABLED] = "تعطيل";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_OC] = "الرجوع لـ أوبن كارت";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_CART_WEIGHT] = "شحن البائع (على حسب الوزن)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_CART_TOTAL] = "شحن البائع (على حسب مجموع السلة)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_FLAT] = "شحن البائع (سعر ثابت)";
$_['ms_shipping_type_' . \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT] = "شحن البائع (لكل منتج فقط)";

$_['ms_config_shipping_field_from_enable'] = "تفعيل حقل 'شحن من'";
$_['ms_config_shipping_field_note_from_enable'] = "اسمح للبائعين بتحديد حقل 'شحن من' مختلف لقواعد الشحن الخاصة بهم";
$_['ms_config_shipping_field_processing_time_enable'] = "تفعيل وقت المعالجة";
$_['ms_config_shipping_field_note_processing_time_enable'] = "السماح للبائعين بتحديد وقت معالجة شحن خاص بهم";
$_['ms_config_shipping_field_shipping_method_enable'] = "تفعيل طريقة الشحن";
$_['ms_config_shipping_field_note_shipping_method_enable'] = "اسمح للبائعين بتحديد أسعار شحن مختلفة عبر طرق شحن مختلفة إلى نفس الوجهة.";
$_['ms_config_shipping_field_delivery_time_enable'] = "تفعيل أوقات التوصيل";
$_['ms_config_shipping_field_note_delivery_time_enable'] = "اسمح للبائعين بتحديد أوقات توصيل مقدرة لقواعد الشحن الخاصة بهم.";
$_['ms_config_shipping_field_cost_ai_enable'] = "تفعيل التكلفة لكل منتج إضافي";
$_['ms_config_shipping_field_note_cost_ai_enable'] = "اسمح للبائعين بتحديد تكلفة شحن مختلفة لكل منتج إضافي";
$_['ms_config_shipping_field_cost_pwu_enable'] = "تفعيل تحديد تسعيرة لكل وحدة وزنية";
$_['ms_config_shipping_field_note_cost_pwu_enable'] = "اسمح للبائعين بتحديد تكاليف شحن لكل وحدة وزنية بالإضافة إلى تكلفة الشحن الثابتة";
$_['ms_config_shipping_field_per_product_override'] = "تفعيل التجاوزات لكل منتج";
$_['ms_config_shipping_field_note_per_product_override'] = "اسمح للبائعين بتجاوز أسعار الشحن العالمية عبر طرق شحن مختلفة إلى نفس الوجهة";

// Commissions system
$_['ms_config_fees'] = "رسوم";
$_['ms_config_signup_fee_enabled'] = "تفعيل رسوم الإشتراك";
$_['ms_config_signup_fee_enabled_note'] = "فرض رسوم على البائعين لإنشاء حساب البائع. يمكنك تحديد رسوم مختلفة لمجموعات البائعين المختلفة.";
$_['ms_config_listing_fee_enabled'] = "تفعيل رسوم الإدراج";
$_['ms_config_listing_fee_enabled_note'] = "قم بالخصم من البائعين رسوم إدراج عند إدراج المنتجات. تستطيع تحديد رسوم مختلفة لكل قسم, مجموعة بائعين أو بائعين محددين.";
$_['ms_config_sale_fee_seller_enabled'] = "تفعيل رسوم البيع على البائعين";
$_['ms_config_sale_fee_seller_enabled_note'] = "أخصم من البائعين رسوم ثابتة لكل طلب. تستطيع تحديد رسوم مختلفة لمجموعة البائعين أو بائعين محددين..";
$_['ms_config_sale_fee_catalog_enabled'] = "تفعيل رسوم البيع للمنتجات/الأقسام";
$_['ms_config_sale_fee_catalog_enabled_note'] = "فرض رسوم على البائعين مقابل كل منتج من المنتجات المباعة. يمكنك تحديد معدلات رسوم مختلفة لمختلف الأقسام والمنتجات الفردية.
";
$_['ms_config_sale_fee_calculation_mode'] = "وضع حساب رسوم البيع (deprecated)";
$_['ms_config_sale_fee_calculation_mode_note'] = "هذا يتحكم في كيفية حساب رسوم البيع في حالة تفعيل كل من رسوم البيع للبائعين ورسوم البيع للأقسام:<br>تُجمع رسوم البائع والقسم مع بعضهما البعض, رسوم البائع تحتسب لكل طلب, رسوم القسم تحتسب لكل منتج.<br>يتم فرض رسوم بيع البائع أو القسم استناداً على الأعدادات الأولية, ويتم حساب كل منها وفقاً لمنتج SKU.";
$_['ms_config_sale_fee_calculation_mode_product_and_suborder'] = "عادي";
$_['ms_config_sale_fee_calculation_mode_order_product'] = "متقدم";
$_['ms_config_fee_priority'] = "Selling fee priority (deprecated)";
$_['ms_config_fee_priority_note'] = "This controls the priority of selling fees in Legacy mode if both vendor and catalog selling fees are enabled:<br>Catalog – catalog selling fee is applied if both catalog and vendor fees are enabled.<br>Vendor – vendor selling fee is applied if both catalog and vendor fees are enabled.";

$_['ms_seller_group_commission_no_settings'] = "لا توجد حاليًا أي رسوم بيع محددة لهذه المجموعة.";
$_['ms_seller_group_commission_add_settings'] = "+ حدد رسوم البيع للبائعين في هذه المجموعة";
$_['ms_seller_commission_override_off_note'] = "هذا البائع جزء من  <strong>%s</strong> مجموعة, يتم تطبيق معدلات الرسوم التالية: %s";
$_['ms_seller_commission_override_off_btn'] = "هل تريد تجاوز معدلات الرسوم الافتراضية لهذا البائع؟";
$_['ms_seller_commission_override_on_note'] = "حدد معدلات الرسوم لهذا البائع لتجاوز الأسعار من <a href='%s' target='_blank'>مجموعة البائع</a>";
$_['ms_seller_commission_override_on_btn'] = "إزالة التجاوز والعودة إلى أسعار مجموعة البائع؟";
$_['ms_oc_category_commission_override_off_note'] = "معدلات الرسوم مأخوذة من القسم الأم <a href='%s' target='_blank'>%s</a>: %s";
$_['ms_oc_category_commission_override_off_btn'] = "تجاوز معدلات الرسوم لهذا القسم؟";
$_['ms_oc_category_commission_override_on_note'] = "حدد معدلات الرسوم لهذا القسم لتجاوز الأسعار من القسم الرئيسي <a href='%s' target='_blank'>%s</a>";
$_['ms_oc_category_commission_override_on_btn'] = "إزالة التجاوز والعودة إلى رسوم القسم الأصلي؟";
$_['ms_product_commission_no_category'] = "لا توجد رسوم بيع لهذا المنتج.";
$_['ms_product_commission_override_off_note'] = "يتم أخذ رسوم البيع لهذا المنتج من الأقسم الأساسي <strong>%s</strong>: %s";
$_['ms_product_commission_override_off_multiple_categories_note'] = "يتم سرد هذا المنتج في أقسام متعددة. تطبق رسوم القصوى للقسم: <strong>%s</strong>: %s";
$_['ms_product_commission_override_off_btn'] = "تجاوز معدلات الرسوم الإفتراضية لهذا المنتج؟";
$_['ms_product_commission_override_on_note'] = "حدد معدلات الرسوم لهذا المنتج لتجاوز الأسعار من <a href='%s' target='_blank'>القسم الأساسي للمنتج</a>";
$_['ms_product_commission_override_on_btn'] = "إزالة التجاوز والعودة إلى رسوم القسم؟
";

$_['ms_customer_total_orders'] = 'طلبات';
$_['ms_customer_total_orders_value'] = 'قيمة';

$_['ms_menu_zones_tab_geozones'] = 'مناطق';
$_['ms_menu_zones_tab_countries'] = 'دول';
$_['ms_menu_zones_tab_zones'] = 'أقليم';

$_['ms_geo_zone_heading'] = 'مناطق';
$_['ms_geo_zone_list'] = 'قائمة المناطق';
$_['ms_geo_zone_name'] = 'أسم المنطقة';
$_['ms_geo_zone_edit'] = 'تعديل المنطقة';
$_['ms_geo_zone_add'] = 'أضافة منطقة جديدة';
$_['ms_geo_zone_add_region_button'] = 'إضافة إقليم';
$_['ms_geo_zone_error_name'] = 'يجب أن يتراوح اسم المنطقة بين 3 و 32 حرفًا!';
$_['ms_geo_zone_error_description'] = 'يجب أن يتراوح الوصف بين 3 و 255 حرفًا!
';
$_['ms_geo_zone_error_delete_tax_rate'] = 'تحذير: لا يمكن حذف هذه المنطقة نظرًا لتعيينها حاليًا لمعدّل ضريبي واحد أو أكثر!';
$_['ms_geo_zone_added'] = 'لقد قمت بإضافة منطقة!';
$_['ms_geo_zone_deleted'] = 'لقد قمت بحذف منطقة!';
$_['ms_geo_zone_updated'] = 'لقد قمت بتعديل منطقة!';

$_['ms_country_heading'] = 'دول';
$_['ms_country_list'] = 'قائمة الدول';

$_['ms_country_added'] = 'لقد قمت بإضافة دولة!';
$_['ms_country_deleted'] = 'لقد قمت بحذف دولة!';
$_['ms_country_updated'] = 'لقد قمت بتعديل دولة!';

$_['ms_zone'] = 'إقليم';
$_['ms_zone_name'] = 'اسم الإقليم';
$_['ms_zone_code'] = 'رمز الأقليم';
$_['ms_zone_error_name'] = 'يجب أن يتراوح اسم المنطقة بين 3 و 128 حرفًا!';
$_['ms_zone_heading'] = 'الأقاليم';
$_['ms_zone_add'] = 'إضافة إقليم جديد';
$_['ms_zone_edit'] = 'تعديل إقليم';
$_['ms_zone_list'] = 'قائمة الأقاليم';
$_['ms_zone_added'] = 'لقد قمت بإضافة إقليم جديد!';
$_['ms_zone_deleted'] = 'لقد قمت بحذف إقليم!';
$_['ms_zone_updated'] = 'لقد قمت بتعديل إقليم!';

$_['ms_zones'] = 'الأقاليم';
$_['ms_zones_all'] = 'جميع الأقاليم';

$_['ms_country_error_delete_binded_to_zone'] = 'تحذير: لا يمكن حذف هذا البلد لأنه مرتبط بـ %s منطقة/مناطق';
$_['ms_country_error_delete_binded_to_regions'] = 'تحذير: لا يمكن حذف هذا البلد لأنه مرتبط بـ %s إقليم/أقاليم!';

// MultiMerch product attributes
$_['ms_menu_field_attributes'] = "مميزات المنتج";
$_['ms_field_attribute_heading'] = "مميزات المنتج";
$_['ms_field_attribute_heading_new'] = "إنشئ مميزة منتج جديد";
$_['ms_field_attribute_heading_update'] = "تعديل صفة المنتج";
$_['ms_field_attribute_breadcrumbs'] = "مميزات المنتج";
$_['ms_field_attribute_manage'] = "قم بإدارة مميزات المنتج";
$_['ms_field_attribute_create'] = "قم بإنشاء مميزة منتج جديدة";
$_['ms_field_attribute_general'] = "عام";
$_['ms_field_attribute_name'] = "اسم";
$_['ms_field_attribute_name_note'] = "يتم عرض";
$_['ms_field_attribute_no_name'] = "لا يوجد اسم";
$_['ms_field_attribute_label'] = "التسمية الداخلية";
$_['ms_field_attribute_label_note'] = "استخدم التسميات لتسهيل إدارة المميزات المشابهة ، على سبيل المثال الحجم (الملابس). يتم استخدام التسميات داخليًا فقط ولا تظهر في أي مكان في المتجر.";
$_['ms_field_attribute_note'] = "الوصف";
$_['ms_field_attribute_note_note'] = "يتم عرض وصف المميزة كملاحظة للبائعين أثناء إدراج المنتج. استخدمها لتوضيح معنى هذه المميزة للبائعين.";
$_['ms_field_attribute_type'] = "النوع";
$_['ms_field_attribute_required'] = "مطلوب";
$_['ms_field_attribute_default'] = "افتراضي";
$_['ms_field_attribute_sort_order'] = "الترتيب";
$_['ms_field_attribute_status'] = "الحالة";
$_['ms_field_attribute_status_note'] = "تتيح لك الحالة تفعيل أو تعطيل هذه المميزة على مستوى المتجر بأكمله.";
$_['ms_field_attribute_status_0'] = "غير نشط";
$_['ms_field_attribute_status_1'] = "نشط";
$_['ms_field_attribute_value'] = "قيمة";
$_['ms_field_attribute_values'] = "القيم";
$_['ms_field_attribute_values_note'] = "سيتمكن البائعون من تحديد قيمة واحدة أو أكثر لهذه المميزة عند إدراج منتجاتهم ، على سبيل المثال الحجم: S ، M ، L";
$_['ms_field_attribute_btn_create_value'] = "إنشئ قيمة";
$_['ms_field_attribute_btn_create_value_long'] = "إنشئ قيمة جديدة";
$_['ms_field_attribute_categories'] = "الأقسام";
$_['ms_field_attribute_success_created'] = "تم إنشاء الميزة بنجاح!";
$_['ms_field_attribute_success_updated'] = "تم تحديث الميزة بنجاح!";
$_['ms_field_attribute_success_deleted'] = "تم حذف الميزة بنجاح!";
$_['ms_field_attribute_error_not_selected'] = "يجب عليك إختيار ميزة واحدة على الأقل!";
$_['ms_field_attribute_error_values'] = "يجب عليك إضافة مميزة واحدة على الأقل!";

$_['ms_field_attribute_message_default'] = "ستستخدم المنتجات المدرجة في هذا القسم مجموعة المميزات الافتراضية. وهذا ينطبق أيضا على المميزات الفرعية.";
$_['ms_field_attribute_message_override'] = "سوف تستخدم المنتجات المدرجة في هذا القسم المميزات المحددة أدناه. وهذا ينطبق أيضاً على الفئات الفرعية.";
$_['ms_field_attribute_message_inherited'] = "مميزات المنتجات المدرجة في هذه الفئة مأخوذة من الفئة الرئيسية <strong>%s</strong>.";
$_['ms_field_attribute_btn_override'] = "تريد إستخدام مميزات مختلفة لهذا القسم؟";
$_['ms_field_attribute_btn_remove_override'] = "هل تريد إلغاء التخطي واستخدام مجموعة المميزات الافتراضية؟";
$_['ms_field_attribute_select_placeholder'] = "حدد مميزات للمنتجات في هذا القسم";

// MultiMerch product variations
$_['ms_menu_field_variations'] = "الإختلافات";
$_['ms_product_tab_variations'] = "الإختلافات";
$_['ms_field_variation_heading'] = "إختلافات المنتج";
$_['ms_field_variation_heading_new'] = "قم بإنشاء متغير منتج جديد";
$_['ms_field_variation_heading_update'] = "قم بتعديل إختلاف المنتج";
$_['ms_field_variation_breadcrumbs'] = "إختلافات المنتج";
$_['ms_field_variation_manage'] = "إدارة إختلافات المنتج";
$_['ms_field_variation_create'] = "قم بإنشاء أختلاف منتج جديد";
$_['ms_field_variation_general'] = "عام";
$_['ms_field_variation_name'] = "الأسم";
$_['ms_field_variation_name_note'] = "يتم عرض اسم الاختلاف للعملاء عبر المتجر والبائعين أثناء إدراج المنتج.";
$_['ms_field_variation_no_name'] = "لا يوجد إسم";
$_['ms_field_variation_label'] = "تسمية داخلية";
$_['ms_field_variation_label_note'] = "قم بإستخدام التسميات لتسهيل إدارة الإختلافات المتشابهة بشكل أسهل على سبيل المثال الحجم (الملابس). يتم إستخدام المسميات داخلياً فقط ولا تظهر في أي مكان في المتجر.";
$_['ms_field_variation_note'] = "الوصف";
$_['ms_field_variation_note_note'] = "يتم عرض وصف الإختلافات كملاحظة للبائعين أثناء إدراج المنتج. استخدمها لتوضيح للبائعين ما هو هذا الاختلاف.";
$_['ms_field_variation_default'] = "إفتراضي";
$_['ms_field_variation_status'] = "الحالة";
$_['ms_field_variation_status_note'] = "تتيح لك الحالة تمكين هذا الاختلاف أو تعطيله على مستوى المتجر بأكمله.";
$_['ms_field_variation_status_0'] = "غير مفعل";
$_['ms_field_variation_status_1'] = "مفعل";
$_['ms_field_variation_value'] = "قيمة";
$_['ms_field_variation_values'] = "قيم";
$_['ms_field_variation_values_note'] = "سيتمكن البائعون من تحديد قيمة واحدة أو أكثر عند إنشاء خيارات مختلفة لمنتجهم ، على سبيل المثال الحجم: S ، M ، L";
$_['ms_field_variation_btn_create_value'] = "إنشئ قيمة";
$_['ms_field_variation_btn_create_value_long'] = "إنشئ قيمة جديدة";
$_['ms_field_variation_categories'] = "الأقسام";
$_['ms_field_variation_variants'] = "الإختلافات";
$_['ms_field_variation_success_created'] = "تم إنشاء الإختلاف بنجاح!";
$_['ms_field_variation_success_updated'] = "تم تحديث الإختلاف بنجاح!";
$_['ms_field_variation_success_deleted'] = "تم حذف الإختلاف بنجاح!";
$_['ms_field_variation_error_not_selected'] = "يجب عليك تحديد إختلاف واحد على الأقل!";
$_['ms_field_variation_error_values'] = "يجب عليك إضافة قيمة إختلاف واحدة على الأقل!";

$_['ms_field_variation_message_default'] = "ستستخدم المنتجات المدرجة في هذا القسم مجموعة الإختلافات الافتراضية. وهذا ينطبق أيضا على الفئات الفرعية.";
$_['ms_field_variation_message_override'] = "ستستخدم المنتجات المدرجة في هذا القسم الإختلافات المحددة أدناه. وهذا ينطبق أيضا على الفئات الفرعية.";
$_['ms_field_variation_message_inherited'] = "الإختلافات المدرجة في هذا القسم مأخوذة من القسم الرئيسي <strong>%s</strong>.";
$_['ms_field_variation_btn_override'] = "هل تود إستخدام إختلافات مختلفة في هذا القسم؟";
$_['ms_field_variation_btn_remove_override'] = "هل تريد الغاء التجاوز وإستخدام الإختلافات الإفتراضية؟";
$_['ms_field_variation_select_placeholder'] = "حدد الإختلافات للمنتجات في هذا القسم.";

$_['ms_field_product_variation_not_selected'] = "الرجاء تحديد إختلاف واحد على الأقل لـ %s";
$_['ms_field_product_variation_select_category'] = "Please, select category to get available options for a product.";
$_['ms_field_product_variation_error_quantity'] = "Please, set quantity for product variant";
$_['ms_field_product_variation_error_price_invalid'] = "Invalid price for product variant %s";
$_['ms_field_product_variation_error_price_empty'] = "Please, specify a price for product variant %s";
$_['ms_field_product_variation_error_price_price_low'] = "Price too low for product variant %s";
$_['ms_field_product_variation_error_price_price_high'] = "Price too high for product variant %s";
$_['ms_field_product_variation_error_quantity'] = "Please, specify a quantity for product variant %s";

// MultiMerch seller properties
$_['ms_menu_field_seller_properties'] = "Seller properties";
$_['ms_menu_field_seller_properties_short'] = "Properties";
$_['ms_field_seller_property_heading'] = "Seller properties";
$_['ms_field_seller_property_heading_new'] = "Create a new seller property";
$_['ms_field_seller_property_heading_update'] = "Modify the seller property";
$_['ms_field_seller_property_breadcrumbs'] = "Seller properties";
$_['ms_field_seller_property_manage'] = "Manage seller properties";
$_['ms_field_seller_property_create'] = "Create a new seller property";
$_['ms_field_seller_property_general'] = "General";
$_['ms_field_seller_property_name'] = "Name";
$_['ms_field_seller_property_name_note'] = "Property name is displayed to sellers at signup.";
$_['ms_field_seller_property_no_name'] = "No name";
$_['ms_field_seller_property_label'] = "Internal label";
$_['ms_field_seller_property_label_note'] = "Use labels to make managing similar properties easier. Labels are only used internally and don't appear anywhere in front store.";
$_['ms_field_seller_property_note'] = "Description";
$_['ms_field_seller_property_note_note'] = "Property description is displayed as a note to sellers during signup. Use it to make it clear what information you expect.";
$_['ms_field_seller_property_type'] = "Type";
$_['ms_field_seller_property_required'] = "Required";
$_['ms_field_seller_property_default'] = "Default";
$_['ms_field_seller_property_sort_order'] = "Sort order";
$_['ms_field_seller_property_status'] = "Status";
$_['ms_field_seller_property_status_note'] = "Status allows you to enable or disable this property globally.";
$_['ms_field_seller_property_status_0'] = "Inactive";
$_['ms_field_seller_property_status_1'] = "Active";
$_['ms_field_seller_property_value'] = "Value";
$_['ms_field_seller_property_values'] = "Values";
$_['ms_field_seller_property_values_note'] = "Sellers will be able to select one or more values for this property when signing up, e.g. I want to sell: Books, Clothes, Electronics.";
$_['ms_field_seller_property_btn_create_value'] = "Create value";
$_['ms_field_seller_property_btn_create_value_long'] = "Create a new value";
$_['ms_field_seller_property_seller_groups'] = "Seller groups";
$_['ms_field_seller_property_success_created'] = "Seller property was successfully created!";
$_['ms_field_seller_property_success_updated'] = "Seller property was successfully updated!";
$_['ms_field_seller_property_success_deleted'] = "Seller property was successfully deleted!";
$_['ms_field_seller_property_error_not_selected'] = "You must select at least one seller property!";
$_['ms_field_seller_property_error_values'] = "You must add at least one seller property value!";
$_['ms_field_seller_property_error_field_empty'] = "Field '%s' is required";
$_['ms_field_seller_property_error_field_length'] = "Field '%s' is too long!";

$_['ms_field_seller_property_message_default'] = "Sellers in this group will have the default set of properties.";
$_['ms_field_seller_property_message_override'] = "Sellers in this group will have the set of properties specified below.";
$_['ms_field_seller_property_btn_override'] = "Use different seller properties for this group?";
$_['ms_field_seller_property_btn_remove_override'] = "Cancel override and use the default seller properties?";
$_['ms_field_seller_property_select_placeholder'] = "Select properties for sellers in this seller group";

// Default configurations
$_['ms_field_default_configuration_heading'] = "Default field configuration";
$_['ms_field_default_configuration_breadcrumbs'] = "Default field configuration";
$_['ms_field_default_configuration_success_updated'] = "Default field configuration updated!";
$_['ms_field_default_configuration_attribute'] = "Assign attributes to the default configuration";
$_['ms_field_default_configuration_msf_attribute_title'] = "Default product attributes";
$_['ms_field_default_configuration_msf_attribute_subtitle'] = "Select the default product attributes that will be available in all categories. You can later override this for individual categories.";
$_['ms_field_default_configuration_variation'] = "Assign variations to the default configuration";
$_['ms_field_default_configuration_msf_variation_heading'] = "Default product variations";
$_['ms_field_default_configuration_msf_variation_note'] = "Select the default product variations that will be available in all categories. You can later override this for individual categories.";
$_['ms_field_default_configuration_seller_property'] = "Assign seller properties to the default configuration";
$_['ms_field_default_configuration_msf_seller_property_title'] = "Default seller properties";
$_['ms_field_default_configuration_msf_seller_property_subtitle'] = "Select the default seller properties that will be available at signup for all seller groups. You can later override this for individual seller groups.";
$_['ms_field_default_configuration_mspf_block_title'] = "Filters";
$_['ms_field_default_configuration_mspf_block_subtitle'] = "Select the default filter configuration that will be available in all categories. You can later override this for individual categories.";

// MS product filter blocks
$_['ms_product_filter_block_heading'] = "الفلاتر";
$_['ms_product_filter_block_title_general'] = "عام";
$_['ms_product_filter_block_title_attributes'] = "المواصفات";
$_['ms_product_filter_block_title_oc_options'] = "الخيارات";
$_['ms_product_filter_block_title_oc_manufacturers'] = "الشركة";
$_['ms_product_filter_block_title_price'] = "السعر";
$_['ms_product_filter_block_title_category'] = "الأقسام";

$_['ms_product_filter_block_message_default'] = "This category will use the default filter configuration. This also applies to child categories.";
$_['ms_product_filter_block_message_inherited'] = "Filter configuration for this category is taken from parent category <strong>%s</strong>.";
$_['ms_product_filter_block_message_override'] = "هذا القسم سيقوم بإستخدام إعدادات الفلتر المحددة في الأسفل. وهذا سينطبق أيضا على الأقسام الفرعية.";
$_['ms_product_filter_block_btn_override'] = "هل تريد إستخدام إعدادات فلتر مختلفة لهذا القسم؟";
$_['ms_product_filter_block_btn_remove_override'] = "هل تريد الغاء التجاوزات وأستخدام إعدادات الفلتر الإفتراضي؟";
$_['ms_product_filter_block_select_placeholder'] = "أختر فلاتر لهذه الأقسام";

/***** INSERTED *****/$_['ms_print_order_packing_slip'] = "Print packing slip";
/***** INSERTED *****/$_['ms_print_order_invoice'] = "Print invoice";
