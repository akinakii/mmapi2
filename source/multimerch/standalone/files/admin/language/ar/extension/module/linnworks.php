<?php
/***** INSERTED *****/$_['heading_title'] = 'MultiMerch Linnworks integration';
/***** INSERTED *****/$_['text_modules'] = 'Modules';
/***** INSERTED *****/$_['text_status'] = 'Enable integration for sellers';
/***** INSERTED *****/$_['channel_name'] = 'Integration channel name';
/***** INSERTED *****/$_['channel_title'] = 'Integration channel title';
/***** INSERTED *****/$_['channel_logo'] = 'Logo URL (57x57 max.)';
/***** INSERTED *****/$_['app_manifest'] = 'Integration Application manifest';
/***** INSERTED *****/$_['error_channel_name'] = 'Channel name length must be between 5 and 20 characters';
/***** INSERTED *****/$_['error_channel_title'] = 'Channel title length must be between 5 and 50 characters';
/***** INSERTED *****/$_['error_channel_logo'] = 'Specify logo url!';
/***** INSERTED *****/$_['text_success'] = 'Settings saved!';
