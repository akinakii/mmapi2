<?php
// Heading
$_['heading_title'] = '<b>[MultiMerch]</b> PayPal Adaptive Payments for MultiMerch';

$_['text_payment'] = 'Payment';
$_['ppa_adaptive'] = 'PayPal Adaptive';

$_['ppa_api_username'] = 'API Username';
$_['ppa_api_username_note'] = 'API Username';
$_['ppa_api_password'] = 'API Password';
$_['ppa_api_password_note'] = 'API Password';
$_['ppa_api_signature'] = 'API Signature';
$_['ppa_api_signature_note'] = 'API Signature';
$_['ppa_api_appid'] = 'Application ID';
$_['ppa_api_appid_note'] = 'Application ID';
$_['ppa_secret'] = 'Shared secret';
$_['ppa_secret_key'] = 'المفتاح';
$_['ppa_secret_value'] = 'القيمة';
$_['ppa_secret_note'] = 'نصوص سيتم استخدامها للتحقق من صحة IPN. يمكن أن يكون أي شيء';


$_['ppa_payment_type'] = 'نوع الدفع';
$_['ppa_payment_type_note'] = "انظر <a href='https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_APIntro'>introduction to PayPal Adaptive Payments</a> for details";
$_['ppa_payment_type_simple'] = 'Simple';
$_['ppa_payment_type_parallel'] = 'Parallel';
$_['ppa_payment_type_chained'] = 'Chained';
$_['ppa_payment_type_preapproval'] = 'Preapproval';

$_['ppa_feespayer'] = 'دافع الرسوم';
$_['ppa_feespayer_note'] = "انظر <a href='https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_APIntro'>introduction to PayPal Adaptive Payments</a> for details";
$_['ppa_feespayer_sender'] = 'المرسل';
$_['ppa_feespayer_primaryreceiver'] = 'المستلم الرئيسي';
$_['ppa_feespayer_eachreceiver'] = 'كلا المستلمين';
$_['ppa_feespayer_secondaryonly'] = 'المستلم الثانوي';

$_['ppa_receiver'] = 'المستلم';
$_['ppa_receiver_note'] = 'حساب مالك المتجر في PayPal الذي سيتم استخدامه في استلام وإرسال الأموال';

$_['ppa_invalid_email'] = 'إجراء حساب PayPal غير صالح';
$_['ppa_invalid_email_note'] = 'ما يجب فعله إذا كان أحد البائعين لديه عنوان PayPal غير صالح';
$_['ppa_disable_module'] = 'تعطيل Adaptive Payments';
$_['ppa_balance_transaction'] = 'قم بإنشاء سجل في رصيد الحساب بدلاً من ذلك';


$_['ppa_sandbox'] = 'وضع Sandbox';
$_['ppa_sandbox_note'] = 'إختبار وسيلة الدفع هذه في وضع Sandbox يتطلب صلاحيات Sandbox API';
$_['ppa_debug'] = 'وضع التصحيح';
$_['ppa_debug_note'] = 'تسجيل معلومات تفصيلية في سجل PayPal';

$_['ppa_total'] = 'الإجمالي';
$_['ppa_total_note'] = 'إجمالي المشتريات التي يجب أن يصل إليها العميل قبل أن تصبح طريقة الدفع هذه نشطة.';

$_['ppa_status'] = 'الحالة';
$_['ppa_completed_status'] = 'حالة الاكتمال';
$_['ppa_error_status'] = 'حالة الخطأ/الفشل';
$_['ppa_pending_status'] = 'حالة الانتظار';

$_['ppa_geo_zone'] = 'المنطقة الجغرافية';
$_['ppa_sort_order'] = 'ترتيب الفرز';
$_['text_ppa_log_filename'] = 'اسم ملف سجل الأخطاء';

$_['ppa_error_secondaryonly'] = 'دافع الرسوم \'المستلم الرئيسي\' لا يمكن أن يستخدم سوى مع وسيلة الدفع من نوع \'CHAINED\'';
$_['ppa_error_senderchained'] = 'دافع الرسوم \'المرسل\' لا يمكن استخدامه مع وسيلة الدفع من نوع \'CHAINED\'';
$_['ppa_error_eachchained'] = 'دافع الرسوم \'كلا المستلمين\' لا يمكن استخدامه مع وسيلة الدفع من نوع \'CHAINED\'';
$_['ppa_error_preapproval_eachreceiver'] = 'وسيلة الدفع \'PREAPPROVAL\' لا يمكن استخدامها سوى مع دافع الرسوم \'EACHRECEIVER\'';

$_['ppa_success'] = 'لقد قمت بتعديل حساب PayPal بنجاح!';
$_['ppa_error_receiver'] = 'تحتاج إلى تحديد المستلم الرئيسي (#1)';
$_['ppa_error_credentials'] = 'تحتاج إلى تحديد كافة شهادات API';
$_['ppa_error_secret'] = 'كلاً من المفتاح والقيمة مطلوبين';