<?php
// Heading
$_['heading_title'] 		  = 'تحويل بنكي';

// Text
$_['text_method_name'] 		  = 'تحويل بنكي';

$_['text_success']			  = 'لقد قمت بتعديل بيانات التحويل البنكي بنجاح.';
$_['text_edit']               = 'تعديل بيانات التحويل البنكي';
$_['text_bank_transfer']	  = '<img src="view/image/multiseller/payment/bank_transfer.png" alt="تحويل بنكي" title="تحويل بنكي" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_fname'] 			  = 'الإسم الأول';
$_['text_lname'] 			  = 'الإسم الأخير';
$_['text_bank_name'] 		  = 'البنك';
$_['text_bank_country'] 	  = 'دولة البنك';
$_['text_bic'] 				  = 'الرمز التعريفي للبنك';
$_['text_iban'] 			  = 'رقم الأيبان';

$_['text_full_name'] 		  = '%s %s';
$_['text_full_bank_name'] 	  = '%s, %s';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل بيانات التحويل البنكي!';
$_['error_email']			  = 'البريد الإلكتروني مطلوب!';
$_['error_no_payment_id']	  = 'خطأ: غير قادر على تأكيد الدفع عبر التحويل البنكي!';

$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد إعدادات</a> لطريقة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات لوسيلة الدفع هذه، يرجى التواصل معه.';