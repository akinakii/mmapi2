<?php
// Heading
$_['heading_title'] 		  = 'الكريمي';

// Text
$_['text_method_name'] 		  = 'الكريمي';

$_['text_success']			  = 'تم تعديل بيانات الكريمي بنجاح.';
$_['text_edit']               = 'تعديل بيانات الكريمي';
$_['text_kuraimi']	  = '<img src="view/image/payment/kuraimi.jpg" alt="الكريمي" title="الكريمي" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_fullname'] 			  = 'الاسم الكامل';
$_['text_momayaznum'] 				  = 'رقم المميز';
$_['text_accountnum'] 			  = 'رقم الحساب';

$_['text_full_name'] 		  = '%s';
$_['text_full_bank_name'] 	  = 'حساب الكريمي';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل بيانات الكريمي!';
$_['error_email']			  = 'البريد الالكتروني مطلوب!';
$_['error_no_payment_id']	  = 'خطأ: غير قادر على التأكد من دفع الكريمي!';

$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد الإعدادات</a> الخاصة بوسيلة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات وسيلة الدفع هذه وبالتالي لن يتم الدفع.';
