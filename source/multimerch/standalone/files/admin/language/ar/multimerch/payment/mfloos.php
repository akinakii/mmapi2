<?php
// Heading
$_['heading_title'] 		  = 'ام فلوس';

// Text
$_['text_method_name'] 		  = 'ام فلوس';

$_['text_success']			  = 'تم تعديل بيانات ام فلوس بنجاح.';
$_['text_edit']               = 'تعديل بيانات ام فلوس';
$_['text_mfloos']	  = '<img src="view/image/payment/kuraimi.jpg" alt="ام فلوس" title="ام فلوس" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_fullname'] 			  = 'الاسم الكامل';
$_['text_mfloosnum'] 			  = 'رقم ام فلوس';

$_['text_full_name'] 		  = '%s';
$_['text_full_bank_name'] 	  = 'حساب ام فلوس';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل بيانات ام فلوس!';
$_['error_email']			  = 'البريد الالكتروني مطلوب!';
$_['error_no_payment_id']	  = 'خطأ: غير قادر على التأكد من دفع ام فلوس!';

$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد الإعدادات</a> الخاصة بوسيلة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات وسيلة الدفع هذه وبالتالي لن يتم الدفع.';
