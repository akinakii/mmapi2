<?php
// Heading
$_['heading_title'] 		  = 'النجم للحوالات المالية';

// Text
$_['text_method_name'] 		  = 'النجم للحوالات المالية';

$_['text_success']			  = 'تم تعديل بيانات النجم بنجاح.';
$_['text_edit']               = 'تعديل بيانات النجم';
$_['text_kuraimi']	  = '<img src="view/image/payment/najm.jpg" alt="النجم" title="النجم" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_fullname'] 			  = 'الاسم الكامل';
$_['text_momayaznum'] 				  = 'رقم المميز';
$_['text_accountnum'] 			  = 'رقم الحساب';

$_['text_full_name'] 		  = '%s';
$_['text_full_bank_name'] 	  = 'حساب النجم';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل بيانات النجم!';
$_['error_email']			  = 'البريد الالكتروني مطلوب!';
$_['error_no_payment_id']	  = 'خطأ: غير قادر على التأكد من دفع حوالة النجم!';

$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد الإعدادات</a> الخاصة بوسيلة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات وسيلة الدفع هذه وبالتالي لن يتم الدفع.';
