<?php
// Heading
$_['heading_title'] 		  = 'PayPal';

// Text
$_['text_method_name'] 		  = 'PayPal';
$_['text_s_method_name']	  = 'PayPal Standard';
$_['text_mp_method_name']	  = 'PayPal Masspay';

$_['text_success']			  = 'لقد قمت بتعديل بيانات PayPal بنجاح.';
$_['text_edit']               = 'تعديل حساب PayPal';
$_['text_paypal']	  		  = '<img src="view/image/multiseller/payment/paypal.png" alt="PayPal" title="PayPal" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_pp_address'] 		  = 'حساب PayPal';
$_['text_api_username'] 	  = 'API Username';
$_['text_api_password'] 	  = 'API Password';
$_['text_api_signature'] 	  = 'API Signature';
$_['text_sandbox'] 			  = 'وضع Sandbox';

$_['text_pp_address_note'] 	  = 'حساب مالك المتجر في PayPal الذي سيتم استخدامه في استلام وإرسال الأموال';
$_['text_api_username_note']  = 'API Username';
$_['text_api_password_note']  = 'API Password';
$_['text_api_signature_note'] = 'API Signature';
$_['text_sandbox_note'] 	  = 'إختبار وسيلة الدفع هذه في وضع Sandbox يتطلب صلاحيات Sandbox API';

$_['text_dialog_confirm'] 	  = 'تأكيد المدفوعات التالية';
$_['text_dialog_ppfee'] 	  = '+ رسوم PayPal';

$_['text_method_one_receiver'] = 'PayPal Standard يستخدم لمستلم واحد.';
$_['text_method_mutli_receiver'] = 'PayPal MassPay يستخدم لعدة مستلمين.';
$_['text_paypal_log_filename'] = 'اسم ملف سجل الأخطاء';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل حساب PayPal!';
$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد إعدادات</a> لطريقة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات لوسيلة الدفع هذه، يرجى التواصل معه.';