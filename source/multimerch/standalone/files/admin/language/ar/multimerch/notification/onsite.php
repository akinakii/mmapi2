<?php

$_['msn_onsite_admin_updated_suborder_status'] = "حالة الطلب <a href='%s' class='order'>#%s</a> تم تغيرها إلى <strong>%s</strong>.";

$_['msn_onsite_customer_created_account'] = "تم تسجيل حساب عميل جديد: <a href='%s' class='customer'>%s</a>.";
$_['msn_onsite_customer_created_message'] = "العميل <a href='%s' class='customer'>%s</a> قام بالرد على المحادثة <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "العميل <a href='%s' class='customer'>%s</a> قام بوضع طلب جديد: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_guest_created_order'] = "قام ضيف بوضع طلب: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_seller_created_account'] = "تم تسجيل حساب بائع جديد: <a href='%s' class='seller'>%s</a>.";
$_['msn_onsite_seller_updated_invoice_status'] = "البائع <a href='%s' class='seller'>%s</a> قام بدفع <a href='%s' class='catalog'><strong>الفاتورة</strong></a> <strong>#%s</strong>.";
$_['msn_onsite_seller_created_message'] = "البائع <a href='%s' class='seller'>%s</a> قام بالرد على المحادثة <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_seller_created_product'] = "البائع <a href='%s' class='seller'>%s</a> قام بنشر منتج جديد <a href='%s' class='product'>%s</a>.";
