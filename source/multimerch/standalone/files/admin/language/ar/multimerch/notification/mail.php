<?php

$_['msn_mail_subject_admin_updated_suborder_status'] = "طلبك #%s تم تحديثة من قبل %s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
طلبك في %s تم تحديثة من قبل %s:

رقم طلب#: %s

المنتجات:
%s

حالة الطلب: %s

التعليقات على الطلب:
%s
EOT;

$_['msn_mail_subject_admin_created_message'] = "تم استلام رسالة جديدة";
$_['msn_mail_admin_created_message'] = <<<EOT
لقد استلمت رسالة جديدة من %s!

%s

%s

تستطيع الرد عليها في قسم الرسائل في حسابك.
EOT;

$_['msn_mail_subject_admin_created_payout'] = "طلب دفع جديد من %s";
$_['msn_mail_admin_created_payout'] = <<<EOT
لقد استلمت طلب دفع جديد من %s.
EOT;

$_['msn_mail_subject_admin_updated_account_status'] = "تعديل حساب البائع";
$_['msn_mail_admin_updated_account_status'] = <<<EOT
حساب البائع الخاص بك في %s تم تعديله من قبل الإدارة.

حالة الحساب: %s
EOT;
