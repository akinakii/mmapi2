<?php
// Heading
$_['heading_title'] 		  = 'محفظتي';

// Text
$_['text_method_name'] 		  = 'محفظتي';

$_['text_success']			  = 'تم تعديل بيانات محفظتي بنجاح.';
$_['text_edit']               = 'تعديل بيانات محفظتي';
$_['text_kuraimi']	  = '<img src="view/image/payment/mahfathati.jpg" alt="محفظتي" title="محفظتي" />';

$_['text_receiver'] 		  = 'المستلم';
$_['text_sender'] 			  = 'المرسل';

$_['text_fullname'] 			  = 'الاسم الكامل';
$_['text_momayaznum'] 				  = 'رقم المميز';
$_['text_accountnum'] 			  = 'رقم الحساب';

$_['text_full_name'] 		  = '%s';
$_['text_full_bank_name'] 	  = 'حساب محفظتي';

// Error
$_['error_permission']		  = 'تحذير: ليس لديك الصلاحية لتعديل بيانات محفظتي!';
$_['error_email']			  = 'البريد الالكتروني مطلوب!';
$_['error_no_payment_id']	  = 'خطأ: غير قادر على التأكد من دفع حوالة محفظتي!';

$_['error_admin_info'] 		  = 'يجب عليك <a href="%s">تحديد الإعدادات</a> الخاصة بوسيلة الدفع هذه.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> لم يقم بتحديد إعدادات وسيلة الدفع هذه وبالتالي لن يتم الدفع.';
