<?php

$_['heading_title'] = "موديل فلتر المنتجات - ملتي ميرش";
$_['text_module'] = "الموديولات";

$_['text_default_config'] = "الإعدادات الإفتراضية";
$_['text_default_config_link'] = "تفعيل / تعطيل الخانات في الفلتر";
$_['text_status'] = "الحالة";

$_['error_status'] = "يجب أن تحدد حالة الموديول!";
