<?php

$_['heading_title'] = 'عناوين ملتي مرش';
$_['text_module'] = 'الموديلات';

$_['module_type_grid'] = "شبكة";
$_['module_type_slider'] = "المنزلق";

$_['module_item_type_seller'] = "البائعين";
$_['module_item_type_product'] = "المنتجات";
$_['module_item_type_oc_category'] = "أقسام السوق";

$_['text_module_name'] = "إسم الموديل";
$_['text_title'] = "العنوان";
$_['text_subtitle'] = "العنوان الفرعي";
$_['text_link'] = "الرابط";
$_['text_appearance'] = "المظهر";
$_['text_item_type'] = "النوع";
$_['text_items'] = "المنتجات";
/***** INSERTED *****/$_['text_items_placeholder'] = "Select items to show";
$_['text_status'] = "الحالة";

$_['error_title'] = "يجب عليك تحديد عنوان (%s)";
$_['error_items'] = "يجب عليك تحديد منتجات لعرضها!";
