<?php

$_['heading_title']    = 'موديل قائمة البائعين - ملتي ميرش (Deprecated)';
$_['text_module']         = 'الموديولات';

$_['ms_config_listsellers'] = 'قائمة البائعين';

$_['ms_orderby'] = 'ترتيب حسب';
$_['ms_date_created'] = 'تاريخ الإنشاء';

$_['text_nickname'] = 'الإسم المستعار';
$_['text_earnings'] = 'الأرباح';

$_['text_ascending'] = 'تصاعدي ';
$_['text_descending'] = 'تنازلي';

$_['error_permission']  = "تحذير: ليس لديك الصلاحية لتعديل موديول 'قائمة البائعين'!";
$_['error_name'] = "الإسم يجب أن يكون بين 3 و 64 حرفاً!";
$_['error_limit'] = "الحد يجب أن يكون متغير (رقم)!";
$_['error_width'] = "العرض يجب أن يكون متغير (رقم)!";
$_['error_height'] = "الإرتفاع يجب أن يكون متغير (رقم)!";
