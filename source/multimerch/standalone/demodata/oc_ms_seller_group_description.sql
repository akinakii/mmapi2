SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_seller_group_description`;

--
-- Dumping data for table `oc_ms_seller_group_description`
--

INSERT INTO `oc_ms_seller_group_description` (`seller_group_description_id`, `seller_group_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'Individual sellers', 'Simple signup, Add products to our catalog, Manage your shipping rates', 1),
(2, 2, 'Power sellers', 'Simple signup, Add products to our catalog, Manage your shipping rates, Special offers and promotions, We handle customer service, Lower fees', 1);

--
-- AUTO_INCREMENT for table `oc_ms_seller_group_description`
--
ALTER TABLE `oc_ms_seller_group_description`
  MODIFY `seller_group_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
