SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_category_path`;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(63, 63, 0),
(64, 63, 0),
(64, 64, 1),
(75, 63, 0),
(75, 75, 1),
(77, 63, 0),
(77, 77, 1),
(97, 97, 0),
(117, 117, 0),
(118, 117, 0),
(118, 118, 1),
(140, 63, 0),
(140, 140, 1),
(174, 174, 0),
(188, 63, 0),
(188, 140, 1),
(188, 188, 2),
(219, 219, 0),
(275, 63, 0),
(275, 275, 1),
(382, 117, 0),
(382, 382, 1),
(386, 117, 0),
(386, 386, 1),
(396, 117, 0),
(396, 396, 1),
(460, 460, 0),
(461, 460, 0),
(461, 461, 1),
(510, 510, 0),
(526, 460, 0),
(526, 526, 1),
(535, 460, 0),
(535, 535, 1),
(544, 174, 0),
(544, 544, 1),
(545, 174, 0),
(545, 545, 1),
(546, 174, 0),
(546, 546, 1);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
