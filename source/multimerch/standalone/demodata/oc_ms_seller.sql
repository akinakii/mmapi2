SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_seller`;

--
-- Dumping data for table `oc_ms_seller`
--

INSERT INTO `oc_ms_seller` (`seller_id`, `nickname`, `company`, `website`, `description`, `country_id`, `zone_id`, `avatar`, `banner`, `date_created`, `seller_status`, `seller_approved`, `seller_group`, `commission_id`) VALUES
(6, 'Iselectrics Industrial Company', '', '', '', 0, 0, 'sellers/6/6c88db1708717a8cb3d4ca652660e6f9_97.png', 'sellers/6/cris-tagupa-606858-unsplash_45.png', '2019-02-13 23:14:45', 1, 1, 1, NULL),
(7, 'Codehow Computers LLC', '', '', '', 0, 0, 'sellers/7/6c88db1708717a8cb3d4ca652660e6f9_70.png', 'sellers/7/cris-tagupa-606858-unsplash_16.png', '2019-02-13 23:14:45', 1, 1, 1, NULL),
(8, 'Cancity Automotive Co', '', '', '', 0, 0, 'sellers/8/6c88db1708717a8cb3d4ca652660e6f9_90.png', 'sellers/8/cris-tagupa-606858-unsplash_80.png', '2019-02-13 23:14:46', 1, 1, 1, NULL),
(9, 'Bioplex Steel Shop', '', '', '', 0, 0, 'sellers/9/6c88db1708717a8cb3d4ca652660e6f9_40.png', 'sellers/9/cris-tagupa-606858-unsplash_23.png', '2019-02-13 23:14:46', 1, 1, 1, NULL),
(10, 'Blackzim Computers Company', '', '', '', 0, 0, 'sellers/10/6c88db1708717a8cb3d4ca652660e6f9_92.png', 'sellers/10/cris-tagupa-606858-unsplash_65.png', '2019-02-13 23:14:46', 1, 1, 2, NULL),
(11, 'Bioplex Industrial Inc', '', '', '', 0, 0, 'sellers/11/6c88db1708717a8cb3d4ca652660e6f9_92.png', 'sellers/11/cris-tagupa-606858-unsplash_65.png', '2019-02-13 23:14:46', 1, 1, 2, NULL),
(12, 'Plusstrip Chemicals Company', '', '', '', 0, 0, 'sellers/12/6c88db1708717a8cb3d4ca652660e6f9_07.png', 'sellers/12/cris-tagupa-606858-unsplash_65.png', '2019-02-13 23:14:46', 1, 1, 2, NULL),
(13, 'Statholdings Industrial Shop', '', '', '', 0, 0, 'sellers/13/6c88db1708717a8cb3d4ca652660e6f9_15.png', 'sellers/13/cris-tagupa-606858-unsplash_50.png', '2019-02-13 23:14:46', 1, 1, 2, NULL),
(14, 'Donware Electronics Company', '', '', '', 0, 0, 'sellers/14/6c88db1708717a8cb3d4ca652660e6f9_23.png', 'sellers/14/cris-tagupa-606858-unsplash_41.png', '2019-02-13 23:14:46', 1, 1, 1, NULL),
(15, 'Ron-tech Engineering', '', '', '', 0, 0, 'sellers/15/6c88db1708717a8cb3d4ca652660e6f9_96.png', 'sellers/15/cris-tagupa-606858-unsplash_42.png', '2019-02-13 23:14:46', 1, 1, 1, NULL),
(16, 'Ron-tech Electronics Company', '', '', '', 0, 0, 'sellers/16/6c88db1708717a8cb3d4ca652660e6f9_48.png', 'sellers/16/cris-tagupa-606858-unsplash_29.png', '2019-02-13 23:14:47', 1, 1, 2, NULL),
(17, 'Finhigh Manufacturing LLC', '', '', '', 0, 0, 'sellers/17/6c88db1708717a8cb3d4ca652660e6f9_34.png', 'sellers/17/cris-tagupa-606858-unsplash_12.png', '2019-02-13 23:14:47', 1, 1, 1, NULL),
(18, 'Fasehatice Computers Shop', '', '', '', 0, 0, 'sellers/18/6c88db1708717a8cb3d4ca652660e6f9_61.png', 'sellers/18/cris-tagupa-606858-unsplash_74.png', '2019-02-13 23:14:47', 1, 1, 1, NULL),
(19, 'Bioplex Equipment Store', '', '', '', 0, 0, 'sellers/19/6c88db1708717a8cb3d4ca652660e6f9_40.png', 'sellers/19/cris-tagupa-606858-unsplash_61.png', '2019-02-13 23:14:47', 1, 1, 1, NULL),
(20, 'Ron-tech Chemicals LLC', '', '', '', 0, 0, 'sellers/20/6c88db1708717a8cb3d4ca652660e6f9_91.png', 'sellers/20/cris-tagupa-606858-unsplash_14.png', '2019-02-13 23:14:47', 1, 1, 1, NULL);

--
-- AUTO_INCREMENT for table `oc_ms_seller`
--
ALTER TABLE `oc_ms_seller`
  MODIFY `seller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
