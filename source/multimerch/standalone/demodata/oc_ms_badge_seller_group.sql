SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_badge_seller_group`;

--
-- Dumping data for table `oc_ms_badge_seller_group`
--

INSERT INTO `oc_ms_badge_seller_group` (`badge_seller_group_id`, `badge_id`, `seller_id`, `seller_group_id`) VALUES
(1, 5, 6, NULL),
(2, 3, 7, NULL),
(3, 4, 7, NULL),
(4, 5, 7, NULL),
(5, 3, 8, NULL),
(6, 4, 8, NULL),
(7, 5, 8, NULL),
(8, 5, 9, NULL),
(9, 1, 10, NULL),
(10, 1, 11, NULL),
(11, 2, 11, NULL),
(12, 3, 11, NULL),
(13, 4, 11, NULL),
(14, 5, 11, NULL),
(15, 2, 12, NULL),
(16, 3, 12, NULL),
(17, 4, 12, NULL),
(18, 5, 12, NULL),
(19, 1, 13, NULL),
(20, 2, 13, NULL),
(21, 4, 13, NULL),
(22, 5, 13, NULL),
(23, 2, 14, NULL),
(24, 3, 14, NULL),
(25, 5, 14, NULL),
(26, 2, 15, NULL),
(27, 4, 15, NULL),
(28, 5, 15, NULL),
(29, 4, 16, NULL),
(30, 1, 17, NULL),
(31, 2, 17, NULL),
(32, 3, 17, NULL),
(33, 5, 17, NULL),
(34, 2, 18, NULL),
(35, 4, 18, NULL),
(36, 1, 19, NULL),
(37, 5, 19, NULL),
(38, 1, 20, NULL),
(39, 2, 20, NULL),
(40, 3, 20, NULL),
(41, 4, 20, NULL);

--
-- AUTO_INCREMENT for table `oc_ms_badge_seller_group`
--
ALTER TABLE `oc_ms_badge_seller_group`
  MODIFY `badge_seller_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
