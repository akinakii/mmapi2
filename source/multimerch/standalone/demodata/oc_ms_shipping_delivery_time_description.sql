SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_shipping_delivery_time_description`;

--
-- Dumping data for table `oc_ms_shipping_delivery_time_description`
--

INSERT INTO `oc_ms_shipping_delivery_time_description` (`delivery_time_desc_id`, `delivery_time_id`, `name`, `language_id`) VALUES
(1, 1, 'same day', 1),
(2, 2, '24h', 1),
(3, 3, '3 working days', 1),
(4, 4, 'up to a week', 1),
(5, 5, '7-30 days', 1);

--
-- AUTO_INCREMENT for table `oc_ms_shipping_delivery_time_description`
--
ALTER TABLE `oc_ms_shipping_delivery_time_description`
  MODIFY `delivery_time_desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
