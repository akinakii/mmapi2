SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_category_description`;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(63, 1, 'Accessories &amp; Supplies', '', 'Accessories &amp; Supplies', '', ''),
(64, 1, 'Audio &amp; Video Accessories', '', 'Audio &amp; Video Accessories', '', ''),
(75, 1, 'Camera &amp; Photo Accessories', '', 'Camera &amp; Photo Accessories', '', ''),
(77, 1, 'Computer Accessories', '', 'Computer Accessories', '', ''),
(97, 1, 'Home Audio', '', 'Home Audio', '', ''),
(117, 1, 'Camera &amp; Photo', '', 'Camera &amp; Photo', '', ''),
(118, 1, 'Accessories', '', 'Accessories', '', ''),
(140, 1, 'Cell Phone Accessories', '', 'Cell Phone Accessories', '', ''),
(174, 1, 'Cell Phones &amp; Accessories', '', 'Cell Phones &amp; Accessories', '', ''),
(188, 1, 'Headphones', '', 'Headphones', '', ''),
(219, 1, 'Computers &amp; Accessories', '', 'Computers &amp; Accessories', '', ''),
(275, 1, 'GPS System Accessories', '', 'GPS System Accessories', '', ''),
(382, 1, 'Bags &amp; Cases', '', 'Bags &amp; Cases', '', ''),
(386, 1, 'Binoculars &amp; Scopes', '', 'Binoculars &amp; Scopes', '', ''),
(396, 1, 'Digital Cameras', '', 'Digital Cameras', '', ''),
(460, 1, 'Car & Vehicle Vehicle GPS', '', 'Car & Vehicle Vehicle GPS', '', ''),
(461, 1, 'Aviation GPS', '', 'Aviation GPS', '', ''),
(510, 1, 'GPS, Finders &amp; Accessories', '', 'GPS, Finders &amp; Accessories', '', ''),
(526, 1, 'Marine GPS', '', 'Marine GPS', '', ''),
(535, 1, 'Powersports GPS', '', 'Powersports GPS', '', ''),
(544, 1, 'Carrier Cell Phones', '', 'Carrier Cell Phones', '', ''),
(545, 1, 'Unlocked Cell Phones', '', 'Unlocked Cell Phones', '', ''),
(546, 1, 'Mobile Broadband', '', 'Mobile Broadband', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
