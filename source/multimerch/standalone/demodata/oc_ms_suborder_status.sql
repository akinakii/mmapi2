SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_suborder_status`;

--
-- Dumping data for table `oc_ms_suborder_status`
--

INSERT INTO `oc_ms_suborder_status` (`ms_suborder_status_id`) VALUES
(2),
(3),
(6);

--
-- AUTO_INCREMENT for table `oc_ms_suborder_status`
--
ALTER TABLE `oc_ms_suborder_status`
  MODIFY `ms_suborder_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
