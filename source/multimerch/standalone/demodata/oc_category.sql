SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_category`;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(63, 'catalog/multimerch/categories/electronics - accessories and supplies.jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:32', '2019-02-12 17:19:32'),
(64, 'catalog/multimerch/categories/512InPPHzRL._AC_US218_.jpg', 63, 0, 1, 0, 1, '2019-01-29 13:12:32', '2019-02-12 17:27:01'),
(75, 'catalog/multimerch/categories/519hk2kq0RL._AC_US218_.jpg', 63, 0, 1, 0, 1, '2019-01-29 13:12:32', '2019-02-12 17:27:25'),
(77, 'catalog/multimerch/categories/41qjj66zoNL._AC_US218_.jpg', 63, 0, 1, 0, 1, '2019-01-29 13:12:32', '2019-02-12 17:28:08'),
(97, 'catalog/multimerch/categories/electronics - home audio_.jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:33', '2019-02-12 17:25:27'),
(117, 'catalog/multimerch/categories/51hUw8Ut4ZL._AC_US218_.jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:33', '2019-02-12 17:21:20'),
(118, 'catalog/multimerch/categories/41fLoPhZ8kL._AC_US160_.jpg', 117, 0, 1, 0, 1, '2019-01-29 13:12:33', '2019-02-12 17:28:43'),
(140, 'catalog/multimerch/categories/41Y5xDmEo5L._AC_US218_.jpg', 63, 0, 1, 0, 1, '2019-01-29 13:12:33', '2019-02-12 17:27:43'),
(174, 'catalog/multimerch/categories/41ibi7nQu5L._AC_US218_ (1).jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:34', '2019-02-12 17:23:12'),
(188, 'catalog/multimerch/categories/electronics - headphones.jpg', 140, 0, 1, 0, 1, '2019-01-29 13:12:35', '2019-02-12 17:25:05'),
(219, 'catalog/multimerch/categories/electronics - computers and accessories.jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:35', '2019-02-12 17:23:37'),
(275, 'catalog/multimerch/categories/31IzG65Ay3L._AC_US218_.jpg', 63, 0, 1, 0, 1, '2019-01-29 13:12:37', '2019-02-12 21:58:18'),
(382, 'catalog/multimerch/categories/41YeNb56zjL._AC_US160_.jpg', 117, 0, 1, 0, 1, '2019-01-29 13:12:40', '2019-02-12 17:29:27'),
(386, 'catalog/multimerch/categories/41lHytL-7dL._AC_US160_.jpg', 117, 0, 1, 0, 1, '2019-01-29 13:12:41', '2019-02-12 17:29:57'),
(396, 'catalog/multimerch/categories/519hk2kq0RL._AC_US218_.jpg', 117, 0, 1, 0, 1, '2019-01-29 13:12:41', '2019-02-12 17:30:21'),
(460, 'catalog/multimerch/categories/41DNogLWqpL._AC_US218_.jpg', 0, 0, 0, 0, 1, '2019-01-29 13:12:44', '2019-01-29 13:12:44'),
(461, 'catalog/multimerch/categories/41oyWS6R9UL._AC_US160_.jpg', 460, 0, 1, 0, 1, '2019-01-29 13:12:44', '2019-02-12 17:31:16'),
(510, 'catalog/multimerch/categories/31IzG65Ay3L._AC_US218_.jpg', 0, 0, 1, 0, 1, '2019-01-29 13:12:45', '2019-02-12 17:24:28'),
(526, 'catalog/multimerch/categories/41DNogLWqpL._AC_US218_.jpg', 460, 0, 1, 0, 1, '2019-01-29 13:12:45', '2019-02-12 17:32:28'),
(535, 'catalog/multimerch/categories/51+qxhsnxsL._AC_US160_.jpg', 460, 0, 1, 0, 1, '2019-01-29 13:12:45', '2019-02-12 17:32:47'),
(544, 'catalog/multimerch/categories/51bvItLxhqL._AC_US218_.jpg', 174, 0, 1, 0, 1, '2019-01-29 13:12:46', '2019-02-12 17:33:31'),
(545, 'catalog/multimerch/categories/41ibi7nQu5L._AC_US218_.jpg', 174, 0, 1, 0, 1, '2019-01-29 13:12:46', '2019-02-12 17:33:48'),
(546, 'catalog/multimerch/categories/41NGDDIL--L._AC_US160_.jpg', 174, 0, 1, 0, 1, '2019-01-29 13:12:46', '2019-02-12 17:34:02');

--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=547;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
