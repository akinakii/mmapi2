SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_layout_module`;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(204, 14, 'category', 'column_right', 1),
(69, 10, 'affiliate', 'column_right', 1),
(343, 6, 'account', 'column_left', 0),
(603, 1, 'basel_content.79', 'top', 13),
(206, 14, 'basel_instagram.48', 'column_right', 3),
(602, 1, 'basel_content.50', 'top', 14),
(301, 17, 'account', 'column_left', 1),
(302, 21, 'account', 'column_left', 1),
(303, 22, 'customer', 'column_left', 1),
(601, 1, 'multimerch_tiles.66', 'top', 12),
(600, 1, 'multimerch_tiles.64', 'top', 11),
(599, 1, 'multimerch_tiles.65', 'top', 10),
(598, 1, 'basel_categories.68', 'top', 9),
(597, 1, 'basel_content.67', 'top', 7),
(596, 1, 'multimerch_tiles.63', 'top', 6),
(595, 1, 'multimerch_tiles.62', 'top', 5),
(593, 1, 'multimerch_tiles.70', 'top', 3),
(609, 3, 'multimerch_tiles.73', 'content_top', 2),
(608, 3, 'multimerch_tiles.72', 'content_top', 1),
(607, 3, 'multimerch_tiles.64', 'column_left', 2),
(606, 3, 'multimerch_productfilter', 'column_left', 1),
(452, 19, 'multimerch_tiles.76', 'content_bottom', 1),
(604, 20, 'multimerch_productfilter', 'column_left', 0),
(453, 19, 'multimerch_tiles.77', 'content_bottom', 2),
(594, 1, 'basel_content.71', 'top', 4),
(591, 1, 'basel_layerslider.37', 'top', 1),
(592, 1, 'basel_content.44', 'top', 2),
(605, 20, 'multimerch_tiles.75', 'content_top', 1),
(610, 3, 'multimerch_tiles.74', 'content_top', 3),
(612, 2, 'multimerch_tiles.81', 'content_top', 1),
(613, 2, 'multimerch_tiles.82', 'content_bottom', 1);

--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=614;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
