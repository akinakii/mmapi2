SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_product_option`;

--
-- Dumping data for table `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(1, 1, 14, '', 1),
(2, 3, 13, '', 1),
(3, 3, 14, '', 1),
(4, 5, 13, '', 0),
(5, 5, 14, '', 1),
(6, 6, 13, '', 0),
(7, 8, 13, '', 1),
(8, 8, 14, '', 0),
(9, 10, 13, '', 1),
(10, 10, 14, '', 0),
(11, 13, 13, '', 1),
(12, 14, 13, '', 0),
(13, 14, 14, '', 1),
(14, 15, 13, '', 0),
(15, 16, 13, '', 1),
(16, 16, 14, '', 1),
(17, 17, 14, '', 1),
(18, 18, 13, '', 1),
(19, 18, 14, '', 0),
(20, 22, 14, '', 0),
(21, 24, 13, '', 1),
(22, 25, 13, '', 1),
(23, 26, 13, '', 0),
(24, 29, 13, '', 0),
(25, 33, 13, '', 0),
(26, 33, 14, '', 1),
(27, 35, 14, '', 0),
(28, 39, 13, '', 0),
(29, 39, 14, '', 0),
(30, 40, 13, '', 1),
(31, 40, 14, '', 0),
(32, 47, 13, '', 0),
(33, 47, 14, '', 1),
(34, 48, 13, '', 0),
(35, 48, 14, '', 1),
(36, 50, 13, '', 1),
(37, 50, 14, '', 1),
(38, 52, 14, '', 0),
(39, 53, 13, '', 0),
(40, 53, 14, '', 1),
(41, 55, 14, '', 1),
(42, 59, 14, '', 1),
(43, 60, 13, '', 1),
(44, 60, 14, '', 0),
(45, 61, 13, '', 1),
(46, 61, 14, '', 0),
(47, 62, 13, '', 1),
(48, 64, 13, '', 1),
(49, 64, 14, '', 1),
(50, 65, 14, '', 0),
(51, 67, 13, '', 1),
(52, 67, 14, '', 1),
(53, 68, 13, '', 1),
(54, 68, 14, '', 0),
(55, 69, 13, '', 0),
(56, 70, 13, '', 0),
(57, 70, 14, '', 0),
(58, 72, 13, '', 1),
(59, 72, 14, '', 1),
(60, 73, 13, '', 0),
(61, 73, 14, '', 1),
(62, 74, 13, '', 1),
(63, 75, 13, '', 0),
(64, 75, 14, '', 1),
(65, 76, 13, '', 0),
(66, 76, 14, '', 0),
(67, 77, 13, '', 0),
(68, 77, 14, '', 1),
(69, 78, 13, '', 0),
(70, 78, 14, '', 1),
(71, 79, 13, '', 1),
(72, 81, 13, '', 0),
(73, 81, 14, '', 1),
(74, 83, 13, '', 0),
(75, 85, 14, '', 1),
(76, 86, 13, '', 1),
(77, 86, 14, '', 1),
(78, 87, 13, '', 0),
(79, 88, 13, '', 0),
(80, 88, 14, '', 1),
(81, 89, 13, '', 0),
(82, 90, 13, '', 0),
(83, 90, 14, '', 0),
(84, 92, 13, '', 0),
(85, 92, 14, '', 0),
(86, 93, 14, '', 0),
(87, 94, 14, '', 0),
(88, 96, 13, '', 1),
(89, 96, 14, '', 1),
(90, 98, 13, '', 0),
(91, 98, 14, '', 1),
(92, 99, 13, '', 0),
(93, 99, 14, '', 1),
(94, 100, 13, '', 1),
(95, 100, 14, '', 0);

--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
