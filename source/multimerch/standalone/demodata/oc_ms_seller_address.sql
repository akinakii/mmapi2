SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_seller_address`;

--
-- Dumping data for table `oc_ms_seller_address`
--

INSERT INTO `oc_ms_seller_address` (`address_id`, `seller_id`, `fullname`, `address_1`, `address_2`, `city`, `state`, `zip`, `country_id`) VALUES
(4, 6, 'Esma Bayındır', '9629 Filistin Cd', '', 'Yalova', '', '95853', 215),
(5, 7, 'Vincenzo Seegers', '3954 Van Limburg Stirumstraat', '', 'Montferland', '', '18193', 150),
(6, 8, 'Georg Langer', 'Kirchgasse 23', '', 'Putbus', '', '20873', 81),
(7, 9, 'Pablo Hernandez', '7703 Calle De La Almudena', '', 'Elche', '', '66961', 195),
(8, 10, 'Lars Henry', '8849 Rue Louis-blanqui', '', 'Mex (vd)', '', '6509', 204),
(9, 11, 'Timmothy Peters', '2451 Cackson St', '', 'Fargo', '', '12607', 223),
(10, 12, 'علیرضا کوتی', '7318 میدان شمشیری', '', 'نجف‌آباد', '', '92885', 101),
(11, 13, 'Manon Perez', '5144 Avenue Jean-jaurès', '', 'Brest', '', '23083', 74),
(12, 14, 'Eric Morris', '4913 Boghall Road', '', 'Enniscorthy', '', '97677', 103),
(13, 15, 'Debra Chambers', '4858 King Street', '', 'Worcester', '', 'J6V 0AX', 222),
(14, 16, 'Rayan Lemoine', '5880 Avenue Debourg', '', 'Fort-de-france', '', '74023', 74),
(15, 17, 'Niklas Lepisto', '5422 Pyynikintie', '', 'Ylöjärvi', '', '27228', 72),
(16, 18, 'Julia Montgomery', '2106 Pearse Street', '', 'Ashbourne', '', '84599', 103),
(17, 19, 'Liam Chan', '8158 22nd Ave', '', 'Odessa', '', 'D7Q 3H4', 38),
(18, 20, 'Logan Sirko', '5829 St. Catherine St', '', 'Springfield', '', 'G2C 4R0', 38);

--
-- AUTO_INCREMENT for table `oc_ms_seller_address`
--
ALTER TABLE `oc_ms_seller_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
