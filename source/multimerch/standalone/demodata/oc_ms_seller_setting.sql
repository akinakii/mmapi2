SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_seller_setting`;

--
-- Dumping data for table `oc_ms_seller_setting`
--

INSERT INTO `oc_ms_seller_setting` (`id`, `seller_id`, `name`, `value`, `is_encoded`) VALUES
(49, 6, 'slr_ms_address', '4', NULL),
(50, 7, 'slr_ms_address', '5', NULL),
(51, 8, 'slr_ms_address', '6', NULL),
(52, 9, 'slr_ms_address', '7', NULL),
(53, 10, 'slr_ms_address', '8', NULL),
(54, 11, 'slr_ms_address', '9', NULL),
(55, 12, 'slr_ms_address', '10', NULL),
(56, 13, 'slr_ms_address', '11', NULL),
(57, 14, 'slr_ms_address', '12', NULL),
(58, 15, 'slr_ms_address', '13', NULL),
(59, 16, 'slr_ms_address', '14', NULL),
(60, 17, 'slr_ms_address', '15', NULL),
(61, 18, 'slr_ms_address', '16', NULL),
(62, 19, 'slr_ms_address', '17', NULL),
(63, 20, 'slr_ms_address', '18', NULL);

--
-- AUTO_INCREMENT for table `oc_ms_seller_setting`
--
ALTER TABLE `oc_ms_seller_setting`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
