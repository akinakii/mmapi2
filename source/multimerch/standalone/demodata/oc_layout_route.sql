SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_layout_route`;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(146, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(215, 1, 0, 'common/home'),
(214, 2, 0, 'product/product'),
(71, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(81, 14, 0, 'extension/blog/%'),
(52, 12, 0, 'product/compare'),
(212, 3, 0, 'product/manufacturer/info'),
(211, 3, 0, 'product/special'),
(96, 15, 0, 'account/login'),
(97, 15, 0, 'affiliate/login'),
(113, 16, 0, 'quickcheckout/checkout'),
(114, 17, 0, 'seller/account-%'),
(115, 18, 0, 'seller/catalog-seller'),
(194, 19, 0, 'seller/catalog-seller/profile'),
(208, 20, 0, 'seller/catalog-seller/products'),
(118, 21, 0, 'seller/report/%'),
(119, 22, 0, 'customer/%'),
(210, 3, 0, 'product/category'),
(209, 3, 0, 'product/manufacturer');

--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
