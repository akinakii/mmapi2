SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_badge`;

--
-- Dumping data for table `oc_ms_badge`
--

INSERT INTO `oc_ms_badge` (`badge_id`, `image`) VALUES
(1, 'catalog/multimerch/badges/best rated seller badge.png'),
(2, 'catalog/multimerch/badges/verified seller badge.png'),
(3, 'catalog/multimerch/badges/secure seller badge.png'),
(4, 'catalog/multimerch/badges/top seller badge.png'),
(5, 'catalog/multimerch/badges/favorite seller badge.png');

--
-- AUTO_INCREMENT for table `oc_ms_badge`
--
ALTER TABLE `oc_ms_badge`
  MODIFY `badge_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
