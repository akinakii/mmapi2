SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_shipping_method_description`;

--
-- Dumping data for table `oc_ms_shipping_method_description`
--

INSERT INTO `oc_ms_shipping_method_description` (`shipping_method_description_id`, `shipping_method_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'DHL', '', 1),
(2, 2, 'FedEx', '', 1),
(3, 3, 'UPS', '', 1),
(4, 4, 'USPS', '', 1);

--
-- AUTO_INCREMENT for table `oc_ms_shipping_method_description`
--
ALTER TABLE `oc_ms_shipping_method_description`
  MODIFY `shipping_method_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
