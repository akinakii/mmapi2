SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_customer`;

--
-- Dumping data for table `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `approved`, `safe`, `token`, `code`, `date_added`) VALUES
(6, 1, 0, 1, 'Esma', 'Bayındır', 'esma.bayındır@example.com', '(390)-570-3436', '', '90bc69dc3ec85a589f71e9ff6025c12377122c9b', 'cnZesLJIX', NULL, NULL, 0, 6, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:43'),
(7, 1, 0, 1, 'Vincenzo', 'Seegers', 'vincenzo.seegers@example.com', '(619)-374-0317', '', '39e5b622c5c500279349166c6333dedfd40303f6', 'CsoUFAF8h', NULL, NULL, 0, 7, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:45'),
(8, 1, 0, 1, 'Georg', 'Langer', 'georg.langer@example.com', '0166-1035648', '', '495618395490cf2fdb359e407875ded73d2fe727', 'rkjtRTaVR', NULL, NULL, 0, 8, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:45'),
(9, 1, 0, 1, 'Pablo', 'Hernandez', 'pablo.hernandez@example.com', '931-587-351', '', '80daba466453cba9809dc5ade64c4f83f390b23a', 'nGVwAWyaq', NULL, NULL, 0, 9, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(10, 1, 0, 1, 'Lars', 'Henry', 'lars.henry@example.com', '(884)-866-3344', '', '0d32668a06a934b2e7fb5bb0e1c74a3cb287b666', 'yEbaK1FLb', NULL, NULL, 0, 10, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(11, 1, 0, 1, 'Timmothy', 'Peters', 'timmothy.peters@example.com', '(516)-578-1722', '', 'e57a494765df27d312635a5b14de651326d65ecd', 'Lbg5OpdbE', NULL, NULL, 0, 11, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(12, 1, 0, 1, 'علیرضا', 'کوتی', 'علیرضا.کوتی@example.com', '020-78410881', '', 'd5a6aef0501c4c85ddd1991443ba4866dc187679', 'IQRIMkqsG', NULL, NULL, 0, 12, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(13, 1, 0, 1, 'Manon', 'Perez', 'manon.perez@example.com', '05-90-80-86-08', '', '98f461159b974e55a6e4a3bfab17084759c2d68b', 'jmUK3AegG', NULL, NULL, 0, 13, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(14, 1, 0, 1, 'Eric', 'Morris', 'eric.morris@example.com', '071-640-1508', '', '8f9c469cc74d499a5b5e4a0483d320d9246a86dd', 'JlryMK2gb', NULL, NULL, 0, 14, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(15, 1, 0, 1, 'Debra', 'Chambers', 'debra.chambers@example.com', '015395 03529', '', 'd09ea452156e92c2308f3c33bf2f0467a88e1dc1', 'toQnWvnlr', NULL, NULL, 0, 15, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(16, 1, 0, 1, 'Rayan', 'Lemoine', 'rayan.lemoine@example.com', '01-17-05-94-32', '', 'f914667b9bcb04bca3e8b630bfcfd8a65eb0777b', '4OZfcULCs', NULL, NULL, 0, 16, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:46'),
(17, 1, 0, 1, 'Niklas', 'Lepisto', 'niklas.lepisto@example.com', '05-218-983', '', 'd68a20f9f234925baee9d9f2032fce00b6915260', '2EcQKVqwR', NULL, NULL, 0, 17, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:47'),
(18, 1, 0, 1, 'Julia', 'Montgomery', 'julia.montgomery@example.com', '051-209-7943', '', '315fb392f8e8fa216345a2516fe12f3aca1b91be', 'rwl68dvWb', NULL, NULL, 0, 18, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:47'),
(19, 1, 0, 1, 'Liam', 'Chan', 'liam.chan@example.com', '300-981-7824', '', '716b147f643022d40d929fdd51195e8971abc1b5', 'i9AVCbLyw', NULL, NULL, 0, 19, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:47'),
(20, 1, 0, 1, 'Logan', 'Sirko', 'logan.sirko@example.com', '748-799-8760', '', '5d3c7a95cd6497433c127617c3d6ff04313b020e', 'evWSjwNuu', NULL, NULL, 0, 20, '', '93.95.191.215', 1, 1, 0, '', '', '2019-02-13 23:14:47');

--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
