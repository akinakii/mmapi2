SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_shipping_rule_cart_total`;

--
-- Dumping data for table `oc_ms_shipping_rule_cart_total`
--

INSERT INTO `oc_ms_shipping_rule_cart_total` (`id`, `seller_id`, `shipping_method_id`, `delivery_time_id`, `total_from`, `total_to`, `cost`, `currency_id`, `currency_code`) VALUES
(11, 6, 3, NULL, '0.0000', '99999.0000', '6.1500', 2, 'USD'),
(12, 7, 4, NULL, '0.0000', '99999.0000', '15.4900', 2, 'USD'),
(13, 8, 3, NULL, '0.0000', '99999.0000', '8.0000', 2, 'USD'),
(14, 9, 1, NULL, '0.0000', '99999.0000', '8.2500', 2, 'USD'),
(15, 10, 4, NULL, '0.0000', '99999.0000', '18.7200', 2, 'USD'),
(16, 11, 1, NULL, '0.0000', '99999.0000', '6.7400', 2, 'USD'),
(17, 12, 4, NULL, '0.0000', '99999.0000', '4.1400', 2, 'USD'),
(18, 13, 1, NULL, '0.0000', '99999.0000', '16.8800', 2, 'USD'),
(19, 14, 4, NULL, '0.0000', '99999.0000', '11.4900', 2, 'USD'),
(20, 15, 4, NULL, '0.0000', '99999.0000', '3.7000', 2, 'USD'),
(21, 16, 1, NULL, '0.0000', '99999.0000', '12.7100', 2, 'USD'),
(22, 17, 4, NULL, '0.0000', '99999.0000', '5.6000', 2, 'USD'),
(23, 18, 1, NULL, '0.0000', '99999.0000', '5.2400', 2, 'USD'),
(24, 19, 3, NULL, '0.0000', '99999.0000', '15.4700', 2, 'USD'),
(25, 20, 3, NULL, '0.0000', '99999.0000', '12.9700', 2, 'USD');

--
-- AUTO_INCREMENT for table `oc_ms_shipping_rule_cart_total`
--
ALTER TABLE `oc_ms_shipping_rule_cart_total`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
