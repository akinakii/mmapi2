SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_address`;

--
-- Dumping data for table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(6, 6, 'Esma', 'Bayındır', 'Iselectrics Industrial Company', '9629 Filistin Cd', '', 'Yalova', '95853', 215, 0, ''),
(7, 7, 'Vincenzo', 'Seegers', 'Codehow Computers LLC', '3954 Van Limburg Stirumstraat', '', 'Montferland', '18193', 150, 0, ''),
(8, 8, 'Georg', 'Langer', 'Cancity Automotive Co', 'Kirchgasse 23', '', 'Putbus', '20873', 81, 0, ''),
(9, 9, 'Pablo', 'Hernandez', 'Bioplex Steel Shop', '7703 Calle De La Almudena', '', 'Elche', '66961', 195, 0, ''),
(10, 10, 'Lars', 'Henry', 'Blackzim Computers Company', '8849 Rue Louis-blanqui', '', 'Mex (vd)', '6509', 204, 0, ''),
(11, 11, 'Timmothy', 'Peters', 'Bioplex Industrial Inc', '2451 Cackson St', '', 'Fargo', '12607', 223, 0, ''),
(12, 12, 'علیرضا', 'کوتی', 'Plusstrip Chemicals Company', '7318 میدان شمشیری', '', 'نجف‌آباد', '92885', 101, 0, ''),
(13, 13, 'Manon', 'Perez', 'Statholdings Industrial Shop', '5144 Avenue Jean-jaurès', '', 'Brest', '23083', 74, 0, ''),
(14, 14, 'Eric', 'Morris', 'Donware Electronics Company', '4913 Boghall Road', '', 'Enniscorthy', '97677', 103, 0, ''),
(15, 15, 'Debra', 'Chambers', 'Ron-tech Engineering', '4858 King Street', '', 'Worcester', 'J6V 0AX', 222, 0, ''),
(16, 16, 'Rayan', 'Lemoine', 'Ron-tech Electronics Company', '5880 Avenue Debourg', '', 'Fort-de-france', '74023', 74, 0, ''),
(17, 17, 'Niklas', 'Lepisto', 'Finhigh Manufacturing LLC', '5422 Pyynikintie', '', 'Ylöjärvi', '27228', 72, 0, ''),
(18, 18, 'Julia', 'Montgomery', 'Fasehatice Computers Shop', '2106 Pearse Street', '', 'Ashbourne', '84599', 103, 0, ''),
(19, 19, 'Liam', 'Chan', 'Bioplex Equipment Store', '8158 22nd Ave', '', 'Odessa', 'D7Q 3H4', 38, 0, ''),
(20, 20, 'Logan', 'Sirko', 'Ron-tech Chemicals LLC', '5829 St. Catherine St', '', 'Springfield', 'G2C 4R0', 38, 0, '');

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
