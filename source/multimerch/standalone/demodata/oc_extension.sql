SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_extension`;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(9, 'shipping', 'flat'),
(12, 'total', 'coupon'),
(17, 'payment', 'free_checkout'),
(20, 'theme', 'theme_default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'module', 'basel_installer'),
(30, 'module', 'blog_latest'),
(32, 'module', 'basel_megamenu'),
(33, 'module', 'basel_layerslider'),
(34, 'module', 'basel_content'),
(35, 'module', 'account'),
(36, 'module', 'category'),
(37, 'module', 'basel_instagram'),
(38, 'module', 'quickcheckout'),
(39, 'module', 'multimerch'),
(40, 'total', 'mm_shipping_total'),
(42, 'module', 'multimerch_productfilter'),
(43, 'total', 'ms_coupon'),
(44, 'module', 'multimerch_tiles'),
(46, 'payment', 'bank_transfer'),
(48, 'module', 'basel_categories'),
(49, 'payment', 'ms_stripe_connect');

--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
