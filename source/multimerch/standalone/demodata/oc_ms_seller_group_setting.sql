SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

TRUNCATE TABLE `oc_ms_seller_group_setting`;

--
-- Dumping data for table `oc_ms_seller_group_setting`
--

INSERT INTO `oc_ms_seller_group_setting` (`id`, `seller_group_id`, `name`, `value`, `is_encoded`) VALUES
(1, 1, 'slr_gr_product_number_limit', '', 0),
(2, 1, 'slr_gr_product_validation', '1', 0),
(3, 1, 'slr_gr_stripe_subscription_enabled', '0', 0),
(4, 1, 'slr_gr_stripe_plan_per_seat_id', '', 0),
(5, 1, 'slr_gr_stripe_plan_base_id', '', 0);

--
-- AUTO_INCREMENT for table `oc_ms_seller_group_setting`
--
ALTER TABLE `oc_ms_seller_group_setting`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
