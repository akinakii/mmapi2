<div id="ms-shipping-checkout">
	<div class="alert alert-info ms-no-background <?php echo !empty($shipping['errors']) ? 'hidden' : ''; ?>" id="shipping-info-holder">
		<?php echo $shipping['contains_digital'] ? $ms_shipping_checkout_info_title_with_digital : $ms_shipping_checkout_info_title; ?>
	</div>

	<div class="alert alert-danger ms-no-background <?php echo empty($shipping['errors']) ? 'hidden' : ''; ?>" id="shipping-error-holder">
		<?php if (!empty($shipping['errors'])) { ?>
		<p class="ms-error"><?php echo $ms_shipping_checkout_error_general; ?></p>
		<?php } ?>
	</div>

	<div class="carts">
		<?php foreach ($shipping['carts'] as $seller_id => $products) { ?>
		<div class="table-responsive <?php echo !empty($products['digital']) && empty($products['individual']) && empty($products['combined']['products']) && empty($products['unavailable']) ? 'hidden' : ''; ?>">
			<table class="table">
				<thead>
					<tr>
						<td colspan="2" style="padding: 8px 0;">
							<?php if ($seller_id) { ?>
								<h4><?php echo sprintf($ms_shipping_checkout_title_seller_table, $this->MsLoader->MsSeller->getSellerNickname($seller_id)); ?>:</h4>
							<?php } else { ?>
								<h4><?php echo $ms_shipping_checkout_title_marketplace_table; ?>:</h4>
							<?php } ?>
						</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($products['individual'] as $product) { ?>
						<tr class="individual <?php echo !empty($product['options']) ? 'with-options' : ''; ?>">
							<td class="col-sm-4 product-info">
								<div>
									<strong><?php echo $product['name']; ?></strong>
								</div>

								<?php if (!empty($product['options'])) { ?>
								<div>
									<ul class="options">
										<?php foreach($product['options'] as $option) { ?>
											<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
										<?php } ?>
									</ul>
								</div>
								<?php } ?>

								<div>
									<span><?php echo $mm_checkout_shipping_products_price; ?></span>
									<?php echo $product['price_formatted']; ?>
								</div>

								<div>
									<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
									<span><?php echo $product['quantity']; ?></span>
								</div>
							</td>
							<td class="col-sm-7 shipping-methods">
								<strong><?php echo $ms_shipping_checkout_title_select_option; ?>:</strong>

								<?php foreach($product['shipping_methods'] as $rule_id => $shipping_method) { ?>
									<div class="radio">
										<label>
											<input type="radio" name="shipping[<?php echo $seller_id; ?>][individual][<?php echo $product['cart_id']; ?>]" value="<?php echo $rule_id; ?>" data-cost="<?php echo $shipping_method['cost']; ?>" <?php echo $shipping_method === reset($product['shipping_methods']) ? 'checked="checked"' : ''; ?> />
											<?php echo $shipping_method['name'] ?: ''; ?> <?php echo $shipping_method['delivery_time'] ? "({$shipping_method['delivery_time']})" : ""; ?><?php echo $shipping_method['name'] || $shipping_method['delivery_time'] ? ':' : ''; ?>
											<span class="inline cost"><?php echo $shipping_method['cost_formatted']; ?></span>
										</label>
									</div>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>

					<?php foreach ($products['combined']['products'] as $product) { ?>
						<tr class="combined <?php echo !empty($product['options']) ? 'with-options' : ''; ?>">
							<td class="col-sm-4 product-info">
								<div>
									<strong><?php echo $product['name']; ?></strong>
								</div>

								<?php if (!empty($product['options'])) { ?>
								<div>
									<ul class="options">
										<?php foreach($product['options'] as $option) { ?>
											<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
										<?php } ?>
									</ul>
								</div>
								<?php } ?>

								<div>
									<span><?php echo $mm_checkout_shipping_products_price; ?></span>
									<?php echo $product['price_formatted']; ?>
								</div>

								<div>
									<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
									<span><?php echo $product['quantity']; ?></span>
								</div>

								<?php if (!empty($products['combined']['errors'])) { ?>
									<div>
										<a href="#" class="ms-remove-link" data-cart-id="<?php echo $product['cart_id']; ?>"><?php echo $ms_shipping_checkout_remove_product; ?></a>
									</div>
								<?php } ?>
							</td>

							<?php if ($product === reset($products['combined']['products'])) { ?>
							<td class="col-sm-7 shipping-methods" rowspan="<?php echo count($products['combined']['products']); ?>">

								<?php if (!empty($products['combined']['shipping_methods'])) { ?>
									<strong><?php echo $ms_shipping_checkout_title_select_option; ?>:</strong>

									<?php foreach($products['combined']['shipping_methods'] as $rule_id => $shipping_method) { ?>
										<div class="radio">
											<label>
												<input type="radio" name="shipping[<?php echo $seller_id; ?>][combined][<?php echo (int)$this->config->get('msconf_shipping_type'); ?>]" value="<?php echo $rule_id; ?>" data-cost="<?php echo $shipping_method['cost']; ?>" <?php echo $shipping_method === reset($products['combined']['shipping_methods']) ? 'checked="checked"' : ''; ?> />
												<?php echo $shipping_method['name'] ?: ''; ?> <?php echo $shipping_method['delivery_time'] ? "({$shipping_method['delivery_time']})" : ""; ?><?php echo $shipping_method['name'] || $shipping_method['delivery_time'] ? ':' : ''; ?>
												<span class="inline cost"><?php echo $shipping_method['cost_formatted']; ?></span>
											</label>
										</div>
									<?php } ?>
								<?php } elseif (!empty($products['combined']['errors'])) { ?>
									<?php foreach ($products['combined']['errors'] as $error) { ?>
										<p class="ms-error"><?php echo $error; ?></p>
									<?php } ?>
								<?php } ?>
							</td>
							<?php } ?>
						</tr>
					<?php } ?>

					<?php foreach ($products['unavailable'] as $product) { ?>
						<tr class="unavailable <?php echo !empty($product['options']) ? 'with-options' : ''; ?>">
							<td class="col-sm-4 product-info">
								<div>
									<strong><?php echo $product['name']; ?></strong>
								</div>

								<?php if (!empty($product['options'])) { ?>
								<div>
									<ul class="options">
										<?php foreach($product['options'] as $option) { ?>
											<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
										<?php } ?>
									</ul>
								</div>
								<?php } ?>

								<div>
									<span><?php echo $mm_checkout_shipping_products_price; ?></span>
									<?php echo $product['price_formatted']; ?>
								</div>

								<div>
									<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
									<span><?php echo $product['quantity']; ?></span>
								</div>

								<div>
									<a href="#" class="ms-remove-link" data-cart-id="<?php echo $product['cart_id']; ?>"><?php echo $ms_shipping_checkout_remove_product; ?></a>
								</div>
							</td>
							<td class="col-sm-7 shipping-methods">
								<p class="ms-error"><?php echo $ms_shipping_checkout_error_individual_general; ?></p>
							</td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<?php if ($products === end($shipping['carts'])) { ?>
						<tr class="total">
							<td></td>
							<td>
								<strong class="shipping-total-holder">
									<?php echo $ms_shipping_checkout_title_shipping_total; ?>:
									<?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?><span class="shipping-total"></span><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?>
								</strong>
							</td>
						</tr>
					<?php } ?>
				</tfoot>
			</table>
		</div>
		<?php } ?>
	</div>
</div>

<script>
	$(function () {
		updateTotalShippingCost();
		$(document).on('click', '.carts input[type="radio"]', updateTotalShippingCost);

		function updateTotalShippingCost() {
			var shipping_total = 0;

			var selected_options = $('.carts').find('input[type="radio"]:checked');
			$(selected_options).each(function () {
				shipping_total += parseFloat($(this).attr('data-cost'));
			});

			$('.shipping-total').html(shipping_total.toFixed(2));
		}

		function updateSessionTotal() {
			$.ajax({
				url: 'index.php?route=multimerch/checkout/shipping/jxSave',
				type: 'POST',
				data: $('#ms-shipping-checkout .carts input[type="radio"]:checked'),
				dataType: 'json',
				beforeSend: function () {
					$('#shipping-error-holder').empty().addClass('hidden');
				},
				success: function (json) {
					$('.alert, .text-danger').remove();

					if (json.errors) {
						$.each(json.errors, function (key, error) {
							$('#shipping-error-holder').append('<p class="ms-error">' + error + '</p>').removeClass('hidden');
						});
					} else {
						loadCart();
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.error(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					alert("Error: " + xhr.responseText);
				}
			});
		}

		$(document).on('change', '#ms-shipping-checkout input[type="radio"]', updateSessionTotal);

		$(document).on('click', '.ms-remove-link', function (e) {
			e.preventDefault();

			$.ajax({
				url: 'index.php?route=quickcheckout/cart/update&remove=' + $(this).attr('data-cart-id'),
				type: 'get',
				dataType: 'json',
				cache: false,
				beforeSend: function() {
					$('#cart1 .button-remove').prop('disabled', true);
				},
				success: function(json) {
					if (json['redirect']) {
						location = json['redirect'];
					} else {
						<?php if (!$this->customer->isLogged()) { ?>
							if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {
								reloadPaymentMethod();
								reloadShippingMethod('payment');
							} else {
								reloadPaymentMethod();
								reloadShippingMethod('shipping');
							}
						<?php } else { ?>
							if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {
								reloadPaymentMethod();
							} else {
								reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());
							}

							if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {
								reloadShippingMethod('shipping');
							} else {
								reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());
							}
						<?php } ?>

						loadCart();
					}
				}
			});
		});

		<?php if (!in_array((int)$this->config->get('msconf_shipping_type'), [\MultiMerch\Core\Shipping\Shipping::TYPE_DISABLED, \MultiMerch\Core\Shipping\Shipping::TYPE_OC])) { ?>
			if ($('#shipping-address #input-shipping-country').length > 0 || $('#payment-address #input-payment-country').length > 0) {
				setTimeout(updateSessionTotal, 1);
			}
		<?php } ?>
	});
</script>