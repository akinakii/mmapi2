<?php echo $header; ?>
<div class="container ms-catalog-seller-products">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
		<?php } ?>
	</ul>
	<?php if (isset($holiday_mode_message) && $holiday_mode_message) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $holiday_mode_message; ?></div>
	<?php } ?>
	<?php if (!empty($ms_shipping_rules_empty_message)) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $ms_shipping_rules_empty_message; ?></div>
	<?php } ?>
	<div class="row">
		<?php if (isset($this->request->get['ms_category_id'])) { ?>
			<div id="mm_page_title" class="col-sm-12">
				<h1 id="page-title"><?php echo $h1; ?></h1>
			</div>
		<?php } ?>

		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>

		<div id="content" class="col-sm-9"><?php echo $content_top; ?>
            <?php if (!empty($seller['products'])) { ?>
				<div id="product-view" class="row grid">
					<div class="table filter">

						<div class="table-cell nowrap hidden-sm hidden-md hidden-lg"><a class="filter-trigger-btn"></a></div>

						<div class="table-cell nowrap hidden-xs">
							<a id="grid-view" class="view-icon grid" data-toggle="tooltip" data-title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></a>
							<a id="list-view" class="view-icon list" data-toggle="tooltip" data-title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></a>
						</div>

						<div class="table-cell w100">
							<a href="<?php echo $compare; ?>" id="compare-total" class="hidden-xs"><?php echo $text_compare; ?></a>
						</div>

						<div id="mspf_sort_limit_panel">
							<div class="table-cell nowrap text-right">
								<div class="sort-select">
									<span class="hidden-xs"><?php echo $this->language->get('text_sort'); ?></span>
									<select id="input-sort" class="form-control input-sm inline">
										<?php foreach ($sorts as $sorts) { ?>
										<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
										<option value="<?php echo $sorts['href']; ?>" selected="selected"> <?php echo $sorts['text']; ?></option>
										<?php } else { ?>
										<option value="<?php echo $sorts['href']; ?>" ><?php echo $sorts['text']; ?></option>
										<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="table-cell nowrap text-right hidden-xs hidden-sm">
								<span><?php echo $this->language->get('text_limit'); ?></span>
								<select id="input-limit" class="form-control input-sm inline">
									<?php foreach ($limits as $limits) { ?>
									<?php if ($limits['value'] == $limit) { ?>
									<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
									<?php } else { ?>
									<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
									<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="grid-holder product-holder grid<?php echo $basel_prod_grid; ?>" id="mspf_products_container">
						<?php foreach ($seller['products'] as $product) { ?>
						<?php require('catalog/view/theme/basel/template/product/single_product.tpl'); ?>
						<?php } ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 text-left" id="mspf_pagination"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right" id="mspf_products_count"><?php echo $results; ?></div>
				</div>
			<?php } else { ?>
				<p><?php echo $ms_catalog_seller_products_empty; ?></p>
			<?php } ?>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
<script>
	$('.plain-menu.cat li').bind().click(function(e) {
		$(this).toggleClass("open").find('>ul').stop(true, true).slideToggle(350)
			.end().siblings().find('>ul').slideUp().parent().removeClass("open");
		e.stopPropagation();
	});
	$('.plain-menu.cat li a').click(function(e) {
		e.stopPropagation();
	});
</script>
<?php echo $footer; ?>