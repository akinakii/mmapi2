<div class="multimerch_tiles grid">
    <div class="title text-left">
        <h3><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['title']) ? $module['i18n'][$this->config->get('config_language_id')]['title'] : ''; ?></h3>
        <?php if (!empty($module['link'])) { ?>
            <span><a href="<?php echo $module['link']; ?>"><?php echo "See more"; ?></a></span>
        <?php } ?>
    </div>

    <div class="subtitle"><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['subtitle']) ? $module['i18n'][$this->config->get('config_language_id')]['subtitle'] : ''; ?></div>

    <?php if ('seller' === (string)$module['item_type']) { ?>
        <div class="row ms-sellers-panel grid-<?php echo $module['num_cols']; ?>" id="mm_store_info">
            <?php foreach ($module['items'] as $seller) { ?>
                <div class="item seller" style="margin-bottom: 15px;">
                    <div class="avatar">
                        <a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" title="<?php echo $seller['nickname']; ?>" alt="<?php echo $seller['nickname']; ?>" /></a>
                    </div>

                    <div class="name">
                        <div>
                            <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>

                            <?php if (!empty($seller['settings']['slr_country'])) { ?>
                                <i class="fa fa-map-marker"></i> <?php echo !empty($seller['settings']['slr_city']) ? $seller['settings']['slr_city'] . ',' : ''; ?> <?php echo $seller['settings']['slr_country']; ?>
                            <?php } ?>
                        </div>
                    </div>

                    <?php if ($this->config->get('msconf_reviews_enable')) { ?>
                        <div class="rating">
                            <span data-toggle="tooltip" title="<?php echo $seller['reviews']['tooltip']; ?>">
                                <div class="ms-ratings main">
                                    <div class="ms-empty-stars"></div>
                                    <div class="ms-full-stars" style="width: <?php echo $seller['reviews']['rating'] * 20; ?>%"></div>
                                </div>
                            </span>
                            (<?php echo $seller['reviews']['total']; ?>)
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } elseif ('oc_category' === (string)$module['item_type']) { ?>
        <div class="row grid-<?php echo $module['num_cols']; ?>">
            <?php foreach ($module['items'] as $category) { ?>
                <div class="text-center item oc_category">
                    <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                    <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-thumbnail" /></a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="col-sm-12">
            <?php if (!empty($module['items'])) { ?>
                <div class="widget widget-related">
                    <div class="grid grid-holder carousel grid<?php echo (int)$module['num_cols']; ?>">
                        <?php foreach ($module['items'] as $product) { ?>
                            <?php require(VQMod::modCheck('catalog/view/theme/basel/template/product/single_product.tpl')); ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
