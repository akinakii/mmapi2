<div class="multimerch_tiles slider">
    <div class="title text-left">
        <h3><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['title']) ? $module['i18n'][$this->config->get('config_language_id')]['title'] : ''; ?></h3>
        <?php if (!empty($module['link'])) { ?>
            <span><a href="<?php echo $module['link']; ?>"><?php echo "See more"; ?></a></span>
        <?php } ?>
    </div>

    <div class="subtitle"><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['subtitle']) ? $module['i18n'][$this->config->get('config_language_id')]['subtitle'] : ''; ?></div>

    <?php if ('seller' === (string)$module['item_type']) { ?>
        <div class="items-holder-<?php echo $module['identifier']; ?>" id="mm_store_info">
            <?php foreach ($module['items'] as $seller) { ?>
                <div class="item seller" style="margin-bottom: 15px;">
                    <div class="avatar">
                        <a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" title="<?php echo $seller['nickname']; ?>" alt="<?php echo $seller['nickname']; ?>" /></a>
                    </div>

                    <div class="name">
                        <div>
                            <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>

                            <?php if (!empty($seller['settings']['slr_country'])) { ?>
                                <i class="fa fa-map-marker"></i> <?php echo !empty($seller['settings']['slr_city']) ? $seller['settings']['slr_city'] . ',' : ''; ?> <?php echo $seller['settings']['slr_country']; ?>
                            <?php } ?>
                        </div>
                    </div>

                    <?php if ($this->config->get('msconf_reviews_enable')) { ?>
                        <div class="rating">
                            <span data-toggle="tooltip" title="<?php echo $seller['reviews']['tooltip']; ?>">
                                <div class="ms-ratings main">
                                    <div class="ms-empty-stars"></div>
                                    <div class="ms-full-stars" style="width: <?php echo $seller['reviews']['rating'] * 20; ?>%"></div>
                                </div>
                            </span>
                            (<?php echo $seller['reviews']['total']; ?>)
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } elseif ('oc_category' === (string)$module['item_type']) { ?>
        <div class="items-holder-<?php echo $module['identifier']; ?>">
            <?php foreach ($module['items'] as $category) { ?>
                <div class="text-center item oc_category">
                    <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                    <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-thumbnail" /></a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <?php if (!empty($module['items'])) { ?>
            <div class="widget widget-related">
                <div class="items-holder-<?php echo $module['identifier']; ?> grid grid-holder carousel grid<?php echo (int)$module['num_cols']; ?>">
                    <?php foreach ($module['items'] as $product) { ?>
                        <?php require(VQMod::modCheck('catalog/view/theme/basel/template/product/single_product.tpl')); ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>

<script>
    $(function () {
        setTimeout(function () {
            var item_type = "<?php echo $module['item_type']; ?>";
            var $items_holder = $(".items-holder-<?php echo $module['identifier']; ?>");

            var itemsToShow = parseInt("<?php echo $module['num_cols']; ?>");
            if ($items_holder.closest('aside').length) {
                itemsToShow = parseInt("<?php echo $module['num_rows']; ?>");
            }

            var params = {
                infinite: true,
                slidesToShow: itemsToShow,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
			{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: item_type == 'product' ? 4 : 5,
                            slidesToScroll: 1,
                        }
                    },
					{
                        breakpoint: 990,
                        settings: {
                            slidesToShow: item_type == 'product' ? 3 : 4,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 770,
                        settings: {
                            slidesToShow: item_type == 'product' ? 2 : 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: item_type == 'product' ? 1 : 2,
                            slidesToScroll: 1,
                        }
                    }
                ] /* basel */
            };

            if ($items_holder.closest('#column-left').length || $items_holder.closest('#column-right').length) {
                params.slidesToShow = 1;
                params.vertical = true;
                params.verticalSwiping = true;
                params.arrows = false;
                params.autoplay = true;
                params.autoplaySpeed = 2000;
            }

            $items_holder.slick(params);
        })
    });
</script>
