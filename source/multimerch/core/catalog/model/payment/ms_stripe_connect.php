<?php 
class ModelPaymentMsStripeConnect extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/ms_stripe_connect');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('ms_stripe_connect_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		
		if ((float)$this->config->get('ms_stripe_connect_min_total') > $total) {
			$status = false;
			$this->ms_logger->info('Stripe Connect disabled: insufficient order total');
		} elseif (!$this->config->get('ms_stripe_connect_geo_zone_id') || $query->num_rows) {
			$status = true;
		} else {
			$status = false;
			$this->ms_logger->info('Stripe Connect disabled: No match for geo zone');
		}

		// Check sellers' Stripe accounts are connected
		$receivers = array();

		foreach ($this->cart->getProducts() as $product) {
			$seller_id = $this->MsLoader->MsProduct->getSellerId($product['product_id']);

			if (!isset($receivers[$seller_id])) {
				// Get Stipe user id
				$stripe_user_id = $this->MsLoader->MsSetting->getSellerSettings(array(
					'seller_id' => $seller_id,
					'name' => 'slr_stripe_user_id',
					'single' => 1
				));

				$receivers[$seller_id]['stripe_user_id'] = $stripe_user_id;
			}
		}

		foreach ($receivers as $seller_id => $receiver) {
			if (empty($receiver['stripe_user_id'])) {
				$status = false;
				$this->ms_logger->info('Stripe Connect disabled: account not connected for seller #' . $seller_id);
				break;
			}
		}
		
		$method_data = array();

		if ($status) {
			$method_data = array( 
				'code'       => 'ms_stripe_connect',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('ms_stripe_connect_sort_order') ?: 1
			);
		}

		return $method_data;
	}
}