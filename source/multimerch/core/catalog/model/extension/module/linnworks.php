<?php
class ModelExtensionModuleLinnworks extends Model {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->model('account/customer');
	}

	public function addIntegration($data = []) {
		$token = md5(strrev($data['lw_uid']));

		$sql = "INSERT INTO " . DB_PREFIX . "ms_seller_linnworks
				    SET seller_id = 0,
					lw_uid = '" . $data['lw_uid'] . "',
					lw_token = '" . $token . "'";
		$this->db->query($sql);

		return $token;
	}

	public function setSeller($token, $seller_id) {
		$sql = "UPDATE " . DB_PREFIX . "ms_seller_linnworks
				    SET seller_id = " . (int) $seller_id . "
					WHERE  lw_token = '" . $token . "'";
		$this->db->query($sql);
	}

	public function removeIntegration($token = '') {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_linnworks` WHERE lw_token = '" . $token . "'");
	}

	public function getTokenByUserEmail($email = '') {

		if (!$seller_id = $this->getSellerIdByEmail($email)) {
			return false;
		}

		$query = $this->db->query("SELECT lw_token FROM " . DB_PREFIX . "ms_seller_linnworks WHERE seller_id = '" . $seller_id . "'");

		return !empty($query->row['lw_token']) ? $query->row['lw_token'] : false;
	}

	public function getTokenByLinnworksId($lw_uid = '') {

		$query = $this->db->query("SELECT lw_token FROM " . DB_PREFIX . "ms_seller_linnworks WHERE lw_uid = '" . $lw_uid . "'");

		return !empty($query->row['lw_token']) ? $query->row['lw_token'] : false;
	}

	public function getTokenByUserId($seller_id = false) {
		$query = $this->db->query("SELECT lw_token FROM " . DB_PREFIX . "ms_seller_linnworks WHERE seller_id = '" . $seller_id . "'");

		return !empty($query->row['lw_token']) ? $query->row['lw_token'] : false;
	}

	public function getSellerIdByToken($token = '') {
		if (!$token) {
			return false;
		}
		$query = $this->db->query("SELECT seller_id FROM " . DB_PREFIX . "ms_seller_linnworks WHERE lw_token = '" . $token . "'");

		return !empty($query->row['seller_id']) ? $query->row['seller_id'] : false;
	}

	public function getSellerByToken($token = '') {
		$query = $this->db->query("SELECT seller_id FROM " . DB_PREFIX . "ms_seller_linnworks WHERE lw_token = '" . $token . "'");

		if (empty($query->row['seller_id'])) {
			return false;
		}
		return $this->MsLoader->MsSeller->getSeller($query->row['seller_id']);
	}

	public function getSellerIdByEmail($email = '') {
		if (!$u = $this->model_account_customer->getCustomerByEmail($email)) {
			return false;
		}

		if (!$s = $this->MsLoader->MsSeller->getSeller($u['customer_id'])) {
			return false;
		}

		return (int) $s['seller_id'];
	}

	public function updateProductQuantity($product_id, $sku, $new_quantity) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product
				WHERE sku = '" . $sku . "' AND product_id= " . (int) $product_id;
		$r = $this->db->query($sql);

		if (empty($r->num_rows) || $r->num_rows < 1) {
			return false;
		}

		$sql = "UPDATE " . DB_PREFIX . "product
				SET quantity = " . (int) $new_quantity . "
				WHERE product_id = " . (int) $product_id;
		$r = $this->db->query($sql);

		return true;
	}

	public function getSellerProducts($seller_id, $params) {
		$products = [];
		$total_products = 0;

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$seller_products = $this->MsLoader->MsProduct->getProducts(
			[
			'seller_id' => $seller_id,
			'language_id' => $this->config->get('config_language_id'),
			], [
			'order_by' => 'pd.name',
			'order_way' => 'ASC',
			'offset' => ($params['page'] - 1) * $params['limit'],
			'limit' => $params['limit']
			]
		);

		if (!empty($seller_products)) {
			$total_products = $seller_products[0]['total_rows'];

			foreach ($seller_products as $product) {

				$product_data = $this->model_catalog_product->getProduct($product['product_id']);
				$price = $this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax'));

				$products[] = array(
					'SKU' => $product_data['sku'],
					'Title' => $product_data['name'],
					'Quantity' => (int) $product['p.quantity'],
					'Price' => $price,
					'Reference' => $product['product_id']
				);
			}
		}

		return [$products, $total_products];
	}

	public function getSellerIdByAccessKey($key = '') {
		$query = $this->db->query("SELECT seller_id  FROM " . DB_PREFIX . "ms_seller WHERE MD5(CONCAT(seller_id, date_created)) = '" . $key . "'");

		if (empty($query->row['seller_id'])) {
			return false;
		}

		if (!$s = $this->MsLoader->MsSeller->getSeller($query->row['seller_id'])) {
			return false;
		}

		return (int) $s['seller_id'];
	}

	public function getSellerAccessKey($seller_id) {
		$query = $this->db->query("SELECT MD5(CONCAT(seller_id, date_created)) as access_key FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . $seller_id . "'");
		return !empty($query->row['access_key']) ? $query->row['access_key'] : false;
	}

}