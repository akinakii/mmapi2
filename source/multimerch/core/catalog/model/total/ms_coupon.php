<?php
class ModelTotalMsCoupon extends Model {
	public function getTotal($total) {
		if (isset($this->session->data['ms_coupons'])) {
			$this->load->language('multiseller/multiseller');

			$cart_data = $this->getCartData();

			foreach ($this->session->data['ms_coupons'] as $seller_id => $code) {
				$coupon_info = $this->MsLoader->MsCoupon->getCoupons(array('seller_id' => $seller_id, 'code' => $code));

				if ($coupon_info && isset($cart_data[$seller_id])) {
					list($discount_total, $error) = $this->calculate($coupon_info, $cart_data[$seller_id]);

					if (!$error) {
						// If discount greater than total
						if ($discount_total > $total['total']) {
							$discount_total = $total['total'];
						}

						$total['totals'][] = array(
							'code'       => 'ms_coupon',
							'title'      => sprintf($this->language->get('ms_total_coupon_title'), $this->MsLoader->MsSeller->getSellerNickname($seller_id), $this->session->data['ms_coupons'][$seller_id]),
							'value'      => -$discount_total,
							'sort_order' => $this->config->get('ms_coupon_sort_order')
						);

						$total['total'] -= $discount_total;
					}
				}
			}
		}
	}

	public function confirm($order_info, $order_total) {
		$this->load->language('multiseller/multiseller');
		$coupon_code = '';

		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');

		if ($start && $end) {
			$coupon_code = substr($order_total['title'], $start, $end - $start);
		}

		if ($coupon_code) {
			// Get all sellers from session with this coupon code
			foreach ($this->session->data['ms_coupons'] as $seller_id => $code) {
				if ((string)$code === (string)$coupon_code && (string)$order_total['title'] === (string)sprintf($this->language->get('ms_total_coupon_title'), $this->MsLoader->MsSeller->getSellerNickname($seller_id), $coupon_code)) {
					$coupon_info = $this->MsLoader->MsCoupon->getCoupons(array('seller_id' => $seller_id, 'code' => $coupon_code));

					if ($coupon_info) {
						$suborder_id_query = $this->db->query("SELECT suborder_id FROM `" . DB_PREFIX . "ms_suborder` WHERE seller_id = " . (int)$seller_id . " AND order_id = " . (int)$order_info['order_id']);
						$suborder_id = isset($suborder_id_query->row['suborder_id']) ? $suborder_id_query->row['suborder_id'] : 0;

						$this->MsLoader->MsCoupon->createCouponHistory($coupon_info['coupon_id'], array(
							'order_id' => $order_info['order_id'],
							'suborder_id' => $suborder_id,
							'customer_id' => $order_info['customer_id'],
							'amount' => $order_total['value']
						));

						$this->MsLoader->MsCoupon->incrementTotalUses($coupon_info['coupon_id']);

						//unset($this->session->data['ms_coupons'][$seller_id]);
					} else {
						return $this->config->get('config_fraud_status_id');
					}
				}
			}
		}
	}

	public function unconfirm($order_id) {
		$this->MsLoader->MsCoupon->deleteCouponHistory(array('order_id' => $order_id));
	}

	public function getCartData() {
		$cart_data = array();

		$cart_products = $this->cart->getProducts();
		foreach ($cart_products as $cart_product) {
			$seller_id = $this->MsLoader->MsProduct->getSellerId($cart_product['product_id']);

			$cart_data[$seller_id][$cart_product['product_id']]['oc_category_ids'][] = $this->MsLoader->MsProduct->getProductPrimaryOcCategory($cart_product['product_id']);

			if ($this->config->get('msconf_allow_seller_categories')) {
				$cart_data[$seller_id][$cart_product['product_id']]['ms_category_ids'][] = $this->MsLoader->MsProduct->getProductPrimaryMsCategory($cart_product['product_id']);
			}

			$cart_data[$seller_id][$cart_product['product_id']]['price'] = isset($cart_data[$seller_id][$cart_product['product_id']]['price']) ? (float)$cart_data[$seller_id][$cart_product['product_id']]['price'] + (float)$cart_product['total'] : (float)$cart_product['total'];
			$cart_data[$seller_id][$cart_product['product_id']]['seller_id'] = $seller_id;

			// Product Discounts
			$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart_product['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$cart_product['quantity'] . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
			if ($product_discount_query->num_rows) {
				$cart_data[$seller_id][$cart_product['product_id']]['quantity_discount'] = true;
			}

			// Product Specials
			$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart_product['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");
			if ($product_special_query->num_rows) {
				$cart_data[$seller_id][$cart_product['product_id']]['special_price'] = true;
			}
		}

		foreach ($cart_data as $seller_id => &$products) {
			foreach ($products as &$product) {
				if (!empty($product['oc_category_ids'])) $product['oc_category_ids'] = array_unique($product['oc_category_ids']);
				if (!empty($product['ms_category_ids'])) $product['ms_category_ids'] = array_unique($product['ms_category_ids']);
			}
		}

		return $cart_data;
	}

	public function calculate($coupon_info = [], $products_data = [])
	{
		$discount = 0;
		list($error, $validated_suborder_total) = $this->validate($coupon_info, $products_data);

		if (!$error) {
			// Calculate discount
			switch ($coupon_info['type']) {
				case MsCoupon::TYPE_DISCOUNT_PERCENT:
					$discount += ($validated_suborder_total * (float)$coupon_info['value'] / 100);
					break;

				case MsCoupon::TYPE_DISCOUNT_FIXED:
					$discount += (float)$coupon_info['value'];
					break;

				default:
					$discount += 0;
					break;
			}
		}

		return [$discount, $error];
	}

	public function validate($coupon_info = [], $products_data = [])
	{
		$suborder_total = 0;
		$error = '';

		try {
			/**
			 * Validate status
			 */
			if (!empty($coupon_info['status']) && (int)$coupon_info['status'] !== (int)MsCoupon::STATUS_ACTIVE) {
				throw new \MultiMerch\Module\Errors\Generic('Error: coupon is not active');
			}

			/**
			 * Validate customer is logged in
			 */
			if (!empty($coupon_info['login_required']) && $coupon_info['login_required'] && !$this->customer->getId()) {
				throw new \MultiMerch\Module\Errors\Generic('Error: login required');
			}

			/**
			 * Validate max uses per customer
			 */
			if (!empty($coupon_info['max_uses_customer']) && $this->customer->getId()) {
				$total_uses_by_customer = $this->MsLoader->MsCoupon->getCouponHistory(['coupon_id' => $coupon_info['coupon_id'], 'customer_id' => $this->customer->getId()]);
				if (!empty($total_uses_by_customer[0]['total_rows']) && (int)$total_uses_by_customer[0]['total_rows'] >= (int)$coupon_info['max_uses_customer']) {
					throw new \MultiMerch\Module\Errors\Generic('Error: max uses per customer exceeded');
				}
			}

			/**
			 * Validate total uses / max uses
			 */
			if (!empty($coupon_info['total_uses']) && !empty($coupon_info['max_uses'])) {
				if ((int)$coupon_info['total_uses'] >= $coupon_info['max_uses']) {
					throw new \MultiMerch\Module\Errors\Generic('Error: total uses exceed max uses');
				}
			}

			/**
			 * Validate date
			 */
			$current_date = strtotime(date('Y-m-d'));

			// Validate date coupon becomes available
			if (!empty($coupon_info['date_start'])) {
				if ($current_date < strtotime($coupon_info['date_start'])) {
					throw new \MultiMerch\Module\Errors\Generic('Error: coupon not yet available');
				}
			}

			// Validate date coupon becomes unavailable
			if (!empty($coupon_info['date_end'])) {
				if ($current_date > strtotime($coupon_info['date_end'])) {
					throw new \MultiMerch\Module\Errors\Generic('Error: coupon expired');
				}
			}

			/**
			 * Validate categories and products
			 */
			if (!empty($products_data)) {
				foreach ($products_data as $product_id => $product_data) {
					/**
					 * Validate product's special price or quantity discount
					 */
					if (!empty($product_data['special_price']) || !empty($product_data['quantity_discount'])) {
						unset($products_data[$product_id]);
						continue;
					}

					/**
					 * Validate suborder products
					 */

					if ($this->config->get('msconf_allow_seller_categories')) {
						// MS CATEGORIES

						// Check whether there are MS categories restrictions
						$coupon_ms_categories = $this->MsLoader->MsCoupon->getMsCategoryByCouponId($coupon_info['coupon_id']);

						$coupon_ms_category_ids = [];
						foreach ($coupon_ms_categories as $key => $ms_categories) {
							foreach ($ms_categories as $ms_category) {
								$coupon_ms_category_ids[$key][] = $ms_category['ms_category_id'];
							}
						}

						foreach ($product_data['ms_category_ids'] as $ms_category_id) {
							// If there are 'include' restrictions - check whether product's category satisfies them
							// If there are 'exclude' restrictions - check whether product's category are in
							if (
								(!empty($coupon_ms_category_ids['include']) && !in_array($ms_category_id, $coupon_ms_category_ids['include']))
								||
								(!empty($coupon_ms_category_ids['exclude']) && in_array($ms_category_id, $coupon_ms_category_ids['exclude']))
							) {
								// If either condition applies, remove product from further calculations
								unset($products_data[$product_id]);
								continue 2;
							}
						}
					} else {
						// OC CATEGORIES

						// Check whether there are OC categories restrictions
						$coupon_oc_categories = $this->MsLoader->MsCoupon->getOcCategoryByCouponId($coupon_info['coupon_id']);

						$coupon_oc_category_ids = [];
						foreach ($coupon_oc_categories as $key => $oc_categories) {
							foreach ($oc_categories as $oc_category) {
								$coupon_oc_category_ids[$key][] = $oc_category['oc_category_id'];
							}
						}

						foreach ($product_data['oc_category_ids'] as $oc_category_id) {
							// If there are 'include' restrictions - check whether product's category satisfies them
							// If there are 'exclude' restrictions - check whether product's category are in
							if (
								(!empty($coupon_oc_category_ids['include']) && !in_array($oc_category_id, $coupon_oc_category_ids['include']))
								||
								(!empty($coupon_oc_category_ids['exclude']) && in_array($oc_category_id, $coupon_oc_category_ids['exclude']))
							) {
								// If either condition applies, remove product from further calculations
								unset($products_data[$product_id]);
								continue 2;
							}
						}
					}

					// Check whether there are products restrictions
					$coupon_products = $this->MsLoader->MsCoupon->getProductByCouponId($coupon_info['coupon_id']);

					foreach ($coupon_products as $key => $products) {
						foreach ($products as $product) {
							$coupon_product_ids[$key][] = $product['product_id'];
						}
					}

					// If there are 'include' restrictions - check whether product satisfies them
					// If there are 'exclude' restrictions - check whether product is in
					if (
						(!empty($coupon_product_ids['include']) && !in_array($product_id, $coupon_product_ids['include']))
						||
						(!empty($coupon_product_ids['exclude']) && in_array($product_id, $coupon_product_ids['exclude']))
					) {
						// If either condition applies, remove product from further calculations
						unset($products_data[$product_id]);
						continue;
					}
				}

				/**
				 * Validate minimum order total
				 */
				if (!empty($coupon_info['min_order_total'])) {
					// Calculate suborder total for validated products
					/**
					 * For example, there are 2 products:
					 * - Product1 is in Category1 - 100$
					 * - Product2 is in Category2 - 50$
					 * Coupon1 applies only on Category1, therefore, after above validations
					 * $products_data should have only Product1.
					 * So, suborder total in this case will be 100$ (instead of 150$)
					 */

					foreach ($products_data as $product_id => $product_data) {
						if (!empty($product_data['price'])) {
							$suborder_total += (float)$product_data['price'];
						}
					}

					if (
						0 === $suborder_total
						||
						$this->currency->format($suborder_total, $this->config->get('config_currency'), '', false) < $this->currency->format($coupon_info['min_order_total'], $this->config->get('config_currency'), '', false)
					) {
						throw new \MultiMerch\Module\Errors\Generic('Error: suborder total not exceeding min required');
					}
				}
			}
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$error = $e->getMessage();
		}

		return [$error, $suborder_total];
	}
}
