<?php
class ModelTotalMmShippingTotal extends Model {
	public function getTotal($total) {
		$this->load->language('multiseller/multiseller');

		$shipping_total = (float)0.0;

		if ($this->cart->hasShipping() && isset($this->session->data['ms_checkout_shipping'])) {
			// For combined shipping
			$seller_ids = [];

			// Individual shipping
			foreach ($this->cart->getProducts() as $product) {
				$shipping_data = isset($this->session->data['ms_checkout_shipping'][$product['seller_id']]) ? $this->session->data['ms_checkout_shipping'][$product['seller_id']] : [];

				if (isset($shipping_data['individual'][$product['cart_id']]['cost'])) {
					$shipping_total += $shipping_data['individual'][$product['cart_id']]['cost'];
				}

				$seller_ids[] = $product['seller_id'];
			}

			foreach (array_unique($seller_ids) as $seller_id) {
				$shipping_data = isset($this->session->data['ms_checkout_shipping'][$seller_id]) ? $this->session->data['ms_checkout_shipping'][$seller_id] : [];

				if (isset($shipping_data['combined']['cost'])) {
					$shipping_total += $shipping_data['combined']['cost'];
				}
			}

			$total['totals'][] = array(
				'code'       => 'mm_shipping_total',
				'title'      => $this->language->get('mm_account_order_shipping_total'),
				'value'      => $shipping_total,
				'sort_order' => $this->config->get('mm_shipping_total_sort_order')
			);

			$total['total'] += $shipping_total;
		}
	}
}