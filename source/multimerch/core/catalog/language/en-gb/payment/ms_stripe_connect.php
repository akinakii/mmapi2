<?php
$_['text_title']    = "Stripe";

$_['text_start_title'] = "Start accepting card payments via Stripe";
$_['text_start_description'] = "Connect your Stripe account to start accepting customer payments instantly and directly into your bank account. <a href='//stripe.com/se/payments' target='_blank'>Learn more...</a>";

$_['text_connected_account'] = "Connected Stripe account ID";
$_['text_deauthorize'] = "Deauthorize";
$_['text_click_here'] = "Click here";
$_['text_payment_description'] = "Stripe payment: Order #%s";
$_['text_checkout_confirm'] = "Place order";

$_['success_account_connected'] = "Stripe account is successfully connected!";
$_['success_account_connected_alert'] = "Your Stripe account is currently connected and you can accept customer card payments.";
$_['success_deauthorized'] = "You have been successfully deathorized from Stripe Connect account!";

$_['error_account_not_connected'] = "Error: Stripe account is not connected";
$_['error_access_denied'] = "Error: The user denied your Stripe Connect request";
$_['error_authorize'] = "Error: Your account can not be connected to Stripe";
$_['error_api_connection'] = "Error: API connection error occurred";
$_['error_deauthorize'] = "Error: You are not connected to marketplace owner's Stripe account, or your account does not exist";
$_['error_checkout_token'] = "Error: Could not retrieve Stripe token";
$_['error_no_order'] = "Error: Order does not exist";
$_['error_card'] = "Your card was declined. Please contact your card issuer or try a different card.";