<?php
$_['lw_integration_tab']    = 'Linnworks integration';
$_['lw_integration_legend']    = 'Linnworks channel integration';
$_['lw_status_active']    = 'Active';
$_['lw_status_inactive']    = 'Inactive';
$_['lw_access_key']    = 'Access key';
