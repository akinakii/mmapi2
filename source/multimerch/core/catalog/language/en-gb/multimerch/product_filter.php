<?php

$_['text_categories'] = "Categories";
$_['text_manufacturers'] = "Brands";
$_['text_price'] = "Price";
$_['text_price_range_min'] = "Min";
$_['text_price_range_max'] = "Max";
$_['text_no_products'] = "There are no products matching your filter selection.";

$_['button_go'] = "<i class='fa fa-check'></i>";
