<?php

$_['msn_mail_subject_admin_updated_order_status'] = "Your order #%s has been updated by %s";
$_['msn_mail_admin_updated_order_status'] = <<<EOT
Your order at %s has been updated by %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_admin_updated_suborder_status'] = "Your order #%s has been updated by %s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
Your order at %s has been updated by %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_customer_created_account'] = 'New customer account created';
$_['msn_mail_customer_created_account'] = <<<EOT
New customer account at %s has been created!
Customer name: %s
E-mail: %s
EOT;

$_['msn_mail_subject_customer_created_message'] = "New message received";
$_['msn_mail_customer_created_message'] = <<<EOT
You have received a new message from %s!

%s

%s

You can reply in the messaging area in your account.
EOT;

$_['msn_mail_subject_customer_created_order'] = "Customer %s has placed a new order at %s";
$_['msn_mail_customer_created_order'] = <<<EOT
Customer %s has placed a new order at %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_customer_created_ms_review'] = 'New product review';
$_['msn_mail_customer_created_ms_review'] = <<<EOT
New review has been submitted for %s.
Visit the following link to view it: <a href="%s">%s</a>
EOT;

$_['msn_mail_subject_guest_created_order'] = "Guest has placed a new order at %s";
$_['msn_mail_guest_created_order'] = <<<EOT
Guest has placed a new order at %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_seller_created_account'] = 'New seller account created';
$_['msn_mail_seller_created_account'] = <<<EOT
New seller account at %s has been created!
Seller name: %s (%s)
E-mail: %s
EOT;

$_['msn_mail_subject_seller_created_message'] = "New message received";
$_['msn_mail_seller_created_message'] = <<<EOT
You have received a new message from %s!

%s

%s

You can reply in the messaging area in your account.
EOT;

$_['msn_mail_subject_seller_created_product'] = 'New product added';
$_['msn_mail_seller_created_product'] = <<<EOT
New product %s has been added by %s.

You can view or edit it in back office.
EOT;

$_['msn_mail_subject_seller_updated_invoice_status'] = "Seller has paid the invoice";
$_['msn_mail_seller_updated_invoice_status'] = <<<EOT
Seller %s has paid the invoice #%s.
EOT;

$_['msn_mail_subject_system_created_invoice'] = "New unpaid invoice";
$_['msn_mail_system_created_invoice'] = <<<EOT
You have a new unpaid invoice #%s.
EOT;

$_['msn_mail_subject_system_finished_import'] = "Your products import is finished";
$_['msn_mail_system_finished_import'] = <<<EOT
Products import has been finished. You can now see your products in Products section of you seller panel.
EOT;
