<?php

$_['msn_onsite_admin_created_message'] = "<strong>%s</strong> (%s staff) has replied to the conversation <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_admin_updated_order_status'] = "Payment status for order <a href='%s' class='order'>#%s</a> has been changed to <strong>%s</strong>.";
$_['msn_onsite_admin_updated_suborder_status'] = "Order <a href='%s' class='order'>#%s</a> status has been changed to <strong>%s</strong>.";
$_['msn_onsite_admin_created_payout'] = "You have received a new payout for %s.";
$_['msn_onsite_admin_updated_account_status'] = "Your seller account status has been changed to <strong>%s</strong>.";

$_['msn_onsite_customer_created_message'] = "Customer <strong>%s</strong> has replied to the conversation <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "You have received a new order <a href='%s' class='order'>#%s</a>.";
$_['msn_onsite_customer_created_ms_review'] = "You have received a new <a href='%s' class='catalog'><strong>review</strong></a> from <strong>%s</strong>.";

$_['msn_onsite_guest_created_order'] = "You have received a new order <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_system_created_invoice'] = "You have a new unpaid <a href='%s' class='catalog'><strong>invoice</strong></a> <strong>#%s</strong>.";

$_['msn_onsite_system_finished_import'] = "Products imported.";