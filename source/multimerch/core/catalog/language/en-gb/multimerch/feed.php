<?php
// Text
$_['ms_feed_title_import_products'] = "Import products";

$_['ms_feed_label_file_encoding'] = "File encoding";
$_['ms_feed_label_select_file'] = "Select file";

$_['ms_feed_column_csv_caption'] = "Field caption from CSV";
$_['ms_feed_column_product_property'] = "Product property";
$_['ms_feed_column_preview_information'] = "Preview information";

$_['ms_feed_import_result_all_rows'] = "Processed rows: ";
$_['ms_feed_import_result_new_rows_count'] = "New products created: ";
$_['ms_feed_import_result_update_rows_count'] = "Existing products updated: ";
$_['ms_feed_import_result_duplicate_rows_count'] = "Duplicate rows found: ";

$_['ms_feed_not_specified'] = "Not specified";
$_['ms_feed_status_active'] = "Active";
$_['ms_feed_status_inactive'] = "Inactive";

$_['ms_feed_step_upload_title'] = "Source";
$_['ms_feed_step_mapping_title'] = "Mapping";
$_['ms_feed_step_confirm_title'] = "Preview and confirm";

$_['ms_feed_step_upload_seq_number'] = "Step 1 of 3";
$_['ms_feed_step_mapping_seq_number'] = "Step 2 of 3";
$_['ms_feed_step_confirm_seq_number'] = "Step 3 of 3";

$_['ms_feed_source_import_type'] = "Import type";
$_['ms_feed_source_import_type_note'] = "Select the way you'll import your products. You can upload a file or specify a remote product feed.";
$_['ms_feed_source_local'] = "Upload a CSV file";
$_['ms_feed_source_url'] = "Specify a remote CSV feed";
$_['ms_feed_source_local_label'] = "Upload CSV file";
$_['ms_feed_source_local_note'] = "Upload your product feed in a CSV format";
$_['ms_feed_source_url_label'] = "Remote feed URL";
$_['ms_feed_source_url_placeholder'] = "https://example.com/products.csv";
$_['ms_feed_source_url_note'] = "Specify the URL of your remote product feed. This must be a valid CSV file.";
$_['ms_feed_source_url_btn'] = "Download";

$_['ms_feed_to_step_mapping'] = "Continue to field mapping";
$_['ms_feed_to_step_confirm'] = "Continue to import";
$_['ms_feed_start_new_import'] = "Import products";

$_['ms_feed_error_source_file'] = 'Error: You must select source file!';
$_['ms_feed_error_source_url_not_specified'] = "Error: Source url is not specified!";
$_['ms_feed_error_source_url_invalid'] = "Error: Invalid source url!";
$_['ms_feed_error_source_file_invalid'] = "Error: Invalid source file!";

$_['ms_feed_error_mapping_field'] = "Required product property '%s' is not mapped.";

// CSV columns captions
$_['ms_feed_caption_model'] = "Model";
$_['ms_feed_caption_sku'] = "SKU";
$_['ms_feed_caption_price'] = "Price";
$_['ms_feed_caption_quantity'] = "Quantity";
$_['ms_feed_caption_manufacturer'] = "Manufacturer";
$_['ms_feed_caption_image_url'] = "Primary image";
$_['ms_feed_caption_images_url'] = "Additional images";
$_['ms_feed_caption_categories_imploded'] = "Categories";
$_['ms_feed_caption_currency_code'] = "Currency";
$_['ms_feed_caption_weight'] = "Weight";
$_['ms_feed_caption_weight_class'] = "Weight class";
$_['ms_feed_caption_tax'] = "Tax class";
$_['ms_feed_caption_name_single_lang'] = "Product name";
$_['ms_feed_caption_name_multi_lang'] = "Product name (%s)";
$_['ms_feed_caption_description_single_lang'] = "Product description";
$_['ms_feed_caption_description_multi_lang'] = "Product description (%s)";
$_['ms_feed_caption_category_single'] = "Category %s";

$_['ms_feed_success_import_started'] = "Your products are being imported. We will notify you when it's done.";
$_['ms_feed_import_name_template'] = "%s's products import";
$_['ms_feed_import_schedule_title'] = "Import type";
$_['ms_feed_import_schedule_yes'] = "Recurring import";
$_['ms_feed_import_schedule_no'] = "One-time import";
$_['ms_feed_import_schedule_template'] = "Run import every ";
$_['ms_feed_import_schedule_5_minutes'] = "5 minutes";
$_['ms_feed_import_schedule_30_minutes'] = "30 minutes";
$_['ms_feed_import_schedule_1_hour'] = "hour";
$_['ms_feed_import_schedule_4_hours'] = "4 hours";
$_['ms_feed_import_schedule_12_hours'] = "12 hours";
$_['ms_feed_import_schedule_24_hours'] = "day";

$_['ms_feed_import_list'] = "Imports";
$_['ms_feed_import_list_single'] = "Import history";
$_['ms_feed_import_list_scheduled'] = "Scheduled imports";
$_['ms_feed_import_list_scheduled_edit'] = "Edit scheduled import";
$_['ms_feed_import_date_run'] = "Date run";
$_['ms_feed_import_date_last_run'] = "Date last run";
$_['ms_feed_import_date_next_run'] = "Date next run";
$_['ms_feed_import_rows_processed'] = "Processed";
$_['ms_feed_import_rows_added'] = "Added";
$_['ms_feed_import_rows_updated'] = "Updated";
$_['ms_feed_import_settings'] = "Import settings";
$_['ms_feed_import_settings_url'] = "Feed URL";
$_['ms_feed_import_settings_cycle'] = "Import schedule";
$_['ms_feed_import_settings_cycle_note'] = "Cycle note";
$_['ms_feed_import_settings_reschedule'] = "Reschedule next run";
$_['ms_feed_import_settings_reschedule_note'] = "Reschedule next run note";
$_['ms_feed_import_settings_history'] = "History";
$_['ms_feed_import_status_message_0'] = "Import is queued.";
$_['ms_feed_import_status_message_1'] = "Import has started.";
$_['ms_feed_import_status_message_2'] = "Import is finished.";
$_['ms_feed_import_settings_success_updated'] = "Scheduled import is successfully updated.";
$_['ms_feed_import_settings_success_deleted'] = "Scheduled import is deleted.";
$_['ms_feed_import_settings_error_import_id'] = "Unable to retrieve import id";


