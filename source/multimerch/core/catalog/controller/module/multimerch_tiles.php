<?php

class ControllerModuleMultiMerchTiles extends ControllerSellerCatalog
{
	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->load->model('localisation/country');
		$this->load->model('tool/image');

		$this->data = array_merge($this->data, $this->load->language('module/multimerch_listsellers'));
	}

	public function index($setting)
	{
		$this->data['module'] = $setting;
		$this->data['module']['identifier'] = md5(microtime());
		$this->data['module']['items'] = [];

		switch ($setting['item_type']) {
			case 'seller':
				foreach ($setting['item_ids'] as $seller_id) {
					$item = $this->MsLoader->MsSeller->getSellers([
						'seller_id' => $seller_id,
						'seller_status' => [MsSeller::STATUS_ACTIVE],
						'single' => true
					]);

					if (!empty($item)) {
						$avatar = $this->model_tool_image->resize($item['ms.avatar'] && file_exists(DIR_IMAGE . $item['ms.avatar']) ? $item['ms.avatar'] : 'ms_no_image.jpg', 100, 100);

						$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $item['seller_id']]);
						$defaults = $this->MsLoader->MsSetting->getSellerDefaults();
						$settings = array_merge($defaults, $seller_settings);

						$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $item['seller_id'], 'address_id' => $settings['slr_ms_address'], 'single' => true]);

						if (!empty($seller_ms_address['city'])) {
							$settings['slr_city'] = utf8_strlen($seller_ms_address['city']) > 20 ? (utf8_substr($seller_ms_address['city'], 0, 18) . '..') : $seller_ms_address['city'];
						}

						if (!empty($seller_ms_address['country_id'])) {
							$country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
							$settings['slr_country'] = (isset($country['name']) ? $country['name'] : '');
						}

						// Rating
						$reviews = [
							'rating' => 0,
							'total' => 0,
							'tooltip' => ''
						];

						if ($this->config->get('msconf_reviews_enable')) {
							$seller_reviews = $this->MsLoader->MsReview->getReviews(['seller_id' => $item['seller_id']]);
							$total_reviews = isset($seller_reviews[0]) ? $seller_reviews[0]['total_rows'] : 0;

							$sum_rating = 0;
							foreach ($seller_reviews as &$seller_review) {
								$sum_rating += $seller_review['rating'];
							}

							$reviews['rating'] = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;
							$reviews['total'] = $total_reviews;
							$reviews['tooltip'] = sprintf($this->language->get('mm_review_rating_summary'), $reviews['rating'], $total_reviews, $this->language->get(1 === (int)$total_reviews ? 'mm_review_rating_review' : 'mm_review_rating_reviews'));
						}

						$this->data['module']['items'][] = [
							'seller_id' => $item['seller_id'],
							'thumb' => $avatar,
							'nickname' => $item['ms.nickname'],
							'description' => utf8_substr(strip_tags(html_entity_decode($item['ms.description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
							'href' => $this->url->link('seller/catalog-seller/products', '&seller_id=' . $item['seller_id'], 'SSL'),
							'settings' => $settings,
							'reviews' => $reviews
						];
					}
				}
				break;

			case 'oc_category':
				foreach ($setting['item_ids'] as $oc_category_id) {
					$item = $this->MsLoader->MsCategory->getOcCategories([
						'category_id' => $oc_category_id,
						'category_status' => MsCategory::STATUS_ACTIVE,
						'single' => true
					]);

					if (!empty($item)) {
						$this->data['module']['items'][] = [
							'oc_category_id' => $item['category_id'],
							'name' => $item['name'],
							'href' => $this->url->link('product/category', 'path=' . $item['category_id'], 'SSL'),
							'thumb' => $this->model_tool_image->resize($item['image'] ?: 'placeholder.png', 150, 150),
						];
					}
				}
				break;

			case 'product':
			default:
				foreach ($setting['item_ids'] as $product_id) {
					$item = $this->MsLoader->MsProduct->getProductCompact($product_id, ['oc_status' => MsProduct::STATUS_ACTIVE]);

					if (!empty($item)) {
						$this->data['module']['items'][] = [
							'product_id'  => $item['product_id'],
							'seller'	  => empty($item['seller_id']) ? false : [
								'seller_id'        => $item['seller_id'],
								'profile_link'     => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $item['seller_id'], '', 'SSL'),
								'seller_nickname'  => $item['seller_nickname'],
								'seller_avatar'    => $this->model_tool_image->resize($item['seller_avatar'] ?: "ms_no_image.jpg", 20, 20),
								'ms.rating'        => isset($item['rating']) ? $item['rating'] : 0,
								'total_reviews'	   => isset($item['total_reviews']) ? $item['total_reviews'] : 0
							],
							'thumb'       => $this->model_tool_image->resize($item['image'] ?: 'placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height')),
							'name'        => $item['name'],
							'description' => utf8_substr(strip_tags(html_entity_decode($item['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
							'price'       => $this->currency->format($this->tax->calculate($item['price'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
							'special'     => (float)$item['special'] ? $this->currency->format($this->tax->calculate($item['special'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) : false,
							'tax'         => $this->config->get('config_tax') ? $this->currency->format((float)$item['special'] ? $item['special'] : $item['price'], $this->session->data['currency']) : false,
							'href'        => $this->url->link('product/product', 'product_id=' . $item['product_id'], 'SSL')
						];
					}
				}
				break;
		}

		$this->document->addStyle('catalog/view/theme/default/stylesheet/multimerch/tiles.css');

		if (empty($this->data['module']['items'])) {
			return false;
		}

		switch ($setting['type']) {
			case 'slider':
				$this->document->addStyle('catalog/view/javascript/multimerch/slick/slick.css');
				$this->document->addStyle('catalog/view/javascript/multimerch/slick/slick-theme.css');
				$this->document->addScript('catalog/view/javascript/multimerch/slick/slick.min.js');

				return $this->load->view('module/multimerch_tiles_slider', $this->data);
				break;

			case 'grid':
			default:
				return $this->load->view('module/multimerch_tiles_grid', $this->data);
				break;
		}
    }
}
