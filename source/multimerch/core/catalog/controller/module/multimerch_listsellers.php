<?php

class ControllerModuleMultiMerchListsellers extends ControllerSellerCatalog
{
    public function index($setting)
    {
        $this->load->model('localisation/country');
        $this->load->model('tool/image');
        $this->data = array_merge($this->data, $this->load->language('module/multimerch_listsellers'));
        $this->data['heading_title'] = $this->language->get('ms_listsellers_sellers');
        if (isset($setting['limit']) && (int)$setting['limit'] > 0)
            $this->data['limit'] = (int)$setting['limit'];
        else
            $this->data['limit'] = 3;

        if (!isset($setting['width']) || (int)$setting['width'] <= 0)
            $setting['width'] = $this->config->get('config_image_category_width');

        if (!isset($setting['height']) || (int)$setting['height'] <= 0)
            $setting['height'] = $this->config->get('config_image_category_height');

        $this->data['sellers_href'] = $this->url->link('seller/catalog-seller');
        $this->data['sellers'] = array();

        if (isset($setting['sortby']))
            $this->data['sortby'] = $setting['sortby'];

        if (isset($setting['orderby']))
            $this->data['orderby'] = $setting['orderby'];

        if ($setting['sortby'] == 'earnings') {
            $results = $this->MsLoader->MsSeller->getSellers(
                array(
                    'seller_status' => array(MsSeller::STATUS_ACTIVE)
                ),
                array(
                    'order_way' => $this->data['orderby'],
                    'offset' => 0,
                    'limit' => $this->data['limit']
                )
            );

            foreach ($results as $seller) {
                $res = $this->db->query("
                    SELECT
                        GROUP_CONCAT(o.order_id) as `order_ids`,
                        EXTRACT(YEAR_MONTH FROM o.`date_added`) as `date`,
                        COUNT(mso.order_id) as `total`
                    FROM `" . DB_PREFIX . "ms_suborder` mso
                    LEFT JOIN (SELECT order_id, order_status_id, date_added FROM `" . DB_PREFIX . "order`) o
                        ON (o.order_id = mso.order_id)
                    WHERE o.order_status_id <> 0 AND mso.seller_id = " . (int)$seller['seller_id'] . "
                    GROUP BY EXTRACT(YEAR_MONTH FROM o.`date_added`)
                ");

                // Get gross total from suborder totals
                foreach ($res->rows as &$row) {
                    $gross_total = 0;

                    if ($row['order_ids']) {
                        $order_ids = explode(',', $row['order_ids']);

                        foreach ($order_ids as $order_id) {
                            $suborder_totals = $this->MsLoader->MsSuborder->getSuborderTotals($order_id, $seller['seller_id']);
                            $gross_total += isset($suborder_totals['total']['value']) ? $suborder_totals['total']['value'] : 0;
                        }
                    }

                    $top_sellers[$seller['seller_id']] = $gross_total;
                }

                arsort($top_sellers);
            }

            foreach ($results as $result) {
                $position = 1;
                foreach ($top_sellers as $seller_id => $earning) {
                    if ($result['seller_id'] == $seller_id) break;
                    else $position++;

                }
                $sort_results[$position] = $result;
            }

            ksort($sort_results);

            $i = 0;

            foreach ($sort_results as $result) {
                $results[$i] = $result;
                $i++;
            }
        } else {
            $results = $this->MsLoader->MsSeller->getSellers(
                array(
                    'seller_status' => array(MsSeller::STATUS_ACTIVE)
                ),
                array(
                    'order_by' => $this->data['sortby'],
                    'order_way' => $this->data['orderby'],
                    'offset' => 0,
                    'limit' => $this->data['limit']
                )
            );
        }

        foreach ($results as $result) {
            $banner = '';
            if ($this->config->get('msconf_enable_seller_banner')) {
                if ($result['banner'] && file_exists(DIR_IMAGE . $result['banner'])) {
                    //TODO problem view with $this->model_tool_image->resize
                    $banner = $this->model_tool_image->resize($result['banner'], $this->config->get('msconf_product_seller_banner_width'), $this->config->get('msconf_product_seller_banner_height'), 'w');
                }
            }
            $seller_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $result['seller_id']));
            $defaults = $this->MsLoader->MsSetting->getSellerDefaults();
            $settings = array_merge($defaults, $seller_settings);

            $seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $result['seller_id'], 'address_id' => $settings['slr_ms_address'], 'single' => true]);

			if (!empty($seller_ms_address['city'])) {
				$settings['slr_city'] = utf8_strlen($seller_ms_address['city']) > 20 ? (utf8_substr($seller_ms_address['city'], 0, 18) . '..') : $seller_ms_address['city'];
			}

            if (!empty($seller_ms_address['country_id'])) {
                $country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
                $settings['slr_country'] = (isset($country['name']) ? $country['name'] : '');
            }

            $products = $this->MsLoader->MsProduct->getProducts(
                array(
                    'seller_id' => $result['seller_id'],
                    'language_id' => $this->config->get('config_language_id'),
                    'product_status' => array(MsProduct::STATUS_ACTIVE),
                    'oc_status' => 1,
                    'available' => true
                ),
                array(
                    'order_by' => 'pd.name',
                    'order_way' => 'ASC',
                    'offset' => 0,
                    'limit' => 3
                )
            );

            $total_products = isset($products[0]['total_rows']) ? $products[0]['total_rows'] : 0;

            foreach ($products as $key => $product) {
                $image = $this->model_tool_image->resize($product['p.image'] && file_exists(DIR_IMAGE . $product['p.image']) ? $product['p.image'] : 'no_image.png', 100, 100);
                $product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                $product['p.image'] = $image;

                $products[$key] = $product;
            }

            $this->data['sellers'][] = array(
                'seller_id' => $result['seller_id'],
                'nickname' => $result['ms.nickname'],
                'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $result['seller_id']),
                'products_href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $result['seller_id']),
                'thumb' => !empty($result['ms.avatar']) && file_exists(DIR_IMAGE . $result['ms.avatar']) ? $this->model_tool_image->resize($result['ms.avatar'], $setting['width'], $setting['height']) : $this->model_tool_image->resize('ms_no_image.jpg', $setting['width'], $setting['height']),
                'banner' => $banner,
                'settings' => $settings,
                'products' => $products,
                'total_products' => $total_products
            );
        }

        return $this->load->view('module/multimerch_listsellers.tpl', $this->data);
    }
}
