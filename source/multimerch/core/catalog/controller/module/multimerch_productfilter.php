<?php

class ControllerModuleMultiMerchProductfilter extends Controller
{
	private $module_name = 'multimerch_productfilter';
	private $settings_code = 'multimerch_productfilter';

	private $manager;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->manager = $this->load->controller('multimerch/product_filter/manager/instance');
	}

	public function index()
	{
		// If module is disabled, return nothing
		if (!$this->config->get("{$this->settings_code}_status"))
			return false;

		if (!empty($rendered_module = $this->manager->renderModule($this->request->get))) {
			$this->document->addScript('catalog/view/javascript/multimerch/product_filter/product_filter.js');
			$this->document->addStyle('catalog/view/theme/default/stylesheet/multimerch/product_filter.css');

			return $rendered_module;
		}

		return false;
	}
}
