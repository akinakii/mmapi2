<?php

class ControllerModuleMultiMerchStoreInfo extends ControllerSellerCatalog
{
	const SELLER_PROFILE = 1;
	const SELLER_PRODUCTS = 2;

	public function index()
	{
		// Get route
		switch ($this->request->get['route']) {
			case 'seller/catalog-seller/profile':
				$flag = self::SELLER_PROFILE;
				break;

			case 'seller/catalog-seller/products':
			default:
				$flag = self::SELLER_PRODUCTS;
				break;
		}

		$seller_data = false;
		if (isset($this->request->get['seller_id'])) {
			$seller_data = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
		}

		if (empty($seller_data) || MsSeller::STATUS_ACTIVE !== (int)$seller_data['ms.seller_status']) {
			return $this->response->redirect($this->url->link('seller/catalog-seller', '', 'SSL'));
		}

		$seller_id = $this->request->get['seller_id'];
		$settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
		$default_settings = $this->MsLoader->MsSetting->getSellerDefaults();
		$settings = array_merge($default_settings, $settings);

		$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $seller_id, 'address_id' => $settings['slr_ms_address'], 'single' => true]);

		if (!empty($seller_ms_address['city'])) {
			$settings['slr_city'] = utf8_strlen($seller_ms_address['city']) > 20 ? (utf8_substr($seller_ms_address['city'], 0, 18) . '..') : $seller_ms_address['city'];
		}

		if (!empty($seller_ms_address['country_id'])) {
			$this->load->model('localisation/country');
			$country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
			$settings['slr_country'] = (isset($country['name']) ? $country['name'] : '');
		}

		// Rating
		$reviews = [
			'rating' => 0,
			'total' => 0,
			'tooltip' => ''
		];

		if ($this->config->get('msconf_reviews_enable')) {
			$seller_reviews = $this->MsLoader->MsReview->getReviews(['seller_id' => $seller_id]);
			$total_reviews = isset($seller_reviews[0]) ? $seller_reviews[0]['total_rows'] : 0;

			$sum_rating = 0;
			foreach ($seller_reviews as &$seller_review) {
				$sum_rating += $seller_review['rating'];
			}

			$reviews['rating'] = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;
			$reviews['total'] = $total_reviews;
			$reviews['tooltip'] = sprintf($this->language->get('mm_review_rating_summary'), $reviews['rating'], $total_reviews, $this->language->get(1 === (int)$total_reviews ? 'mm_review_rating_review' : 'mm_review_rating_reviews'));
		}

		$seller = [
			'seller_id' => $seller_id,
			'href' => $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller_id),
			'nickname' => $seller_data['ms.nickname'],
			'description' => html_entity_decode($seller_data['ms.description'], ENT_QUOTES, 'UTF-8'),
			'thumb' => $this->model_tool_image->resize(!empty($seller_data['ms.avatar']) && file_exists(DIR_IMAGE . $seller_data['ms.avatar']) ? $seller_data['ms.avatar'] : 'ms_no_image.jpg', 40, 40),
			'settings' => $settings,
			'reviews' => $reviews,
		];

		// Total followers
		if ($this->config->get('msconf_allow_favorite_sellers')) {
			$seller['total_followers'] = $this->MsLoader->MsFavoriteSeller->getTotalFollowers($seller_id);
			$seller['is_followed_by_customer'] = $this->MsLoader->MsFavoriteSeller->isCustomerFollowing($seller_id);
		}

		if ($flag === self::SELLER_PROFILE) {
			$seller['thumb'] = $this->model_tool_image->resize(!empty($seller_data['ms.avatar']) && file_exists(DIR_IMAGE . $seller_data['ms.avatar']) ? $seller_data['ms.avatar'] : 'ms_no_image.jpg', 100, 100);

			$seller['show_view_store_button'] = true;

			// Store stats
			$stats = [];

			$stats[] = [
				'title' => $this->language->get('ms_account_member_since'),
				'value' => date($this->language->get('date_format_short'), strtotime($seller_data['ms.date_created']))
			];

			$stats[] = [
				'title' => $this->language->get('ms_catalog_seller_profile_total_products'),
				'value' => $this->MsLoader->MsProduct->getTotalProducts([
					'seller_id' => $seller['seller_id'],
					'product_status' => [MsProduct::STATUS_ACTIVE],
					'oc_status' => 1,
					'available' => true
				])
			];

			$stats[] = [
				'title' => $this->language->get('ms_catalog_seller_profile_total_sales'),
				'value' => $this->MsLoader->MsSuborder->getTotalSuborders($seller['seller_id'])
			];

			$seller['stats'] = $stats;

			// Badges
			$badges = [];
			if ($this->config->get('msconf_badge_enabled')) {
				$badges = array_unique(array_merge(
					$this->MsLoader->MsBadge->getBadges(['seller_id' => $seller_id]),
					$this->MsLoader->MsBadge->getBadges(['seller_group_id' => $seller_data['ms.seller_group']]),
					$this->MsLoader->MsBadge->getBadges(['seller_group_id' => $this->config->get('msconf_default_seller_group_id')])
				), SORT_REGULAR);

				foreach ($badges as &$badge) {
					$badge['image'] = $this->model_tool_image->resize($badge['image'], 40, 40);
				}
			}

			$seller['badges'] = array_slice($badges, 0, 5);

			// Social links
			$social_links = [];
			if ($this->config->get('msconf_sl_status')) {
				$this->MsLoader->MsHelper->addStyle('multimerch_social_links');
				$social_links = $this->MsLoader->MsSocialLink->getSellerChannels($seller['seller_id']);
				foreach ($social_links as &$link) {
					if ($this->MsLoader->MsHelper->isValidUrl($link['channel_value'])) {
						$link['image'] = $this->model_tool_image->resize($link['image'], $this->config->get('msconf_sl_icon_width'), $this->config->get('msconf_sl_icon_height'));
					} else {
						unset($link);
					}
				}
			}

			$seller['social_links'] = $social_links;

			// Contact form
			$contact_form = '';
			if ($this->config->get('mmess_conf_enable') && (!$this->customer->getId() || (int)$seller['seller_id'] !== (int)$this->customer->getId())) {
				$this->data = array_merge($this->data, ['seller' => $seller]);
				$contact_form = $this->MsLoader->MsHelper->renderPmDialog($this->data);
			}

			$seller['contact_form'] = $contact_form;
		}

		return $this->load->view('multimerch/module/store_info', array_merge($this->load->language('multiseller/multiseller'), ['seller' => $seller]));
	}
}
