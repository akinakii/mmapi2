<?php

class ControllerModuleMultiMerchStoreCategory extends ControllerSellerCatalog
{
	public function index()
	{
		$seller_id = isset($this->request->get['seller_id']) ? $this->request->get['seller_id'] : false;

		if (!$seller_id)
			return false;

		$ms_categories = $this->MsLoader->MsSeller->getSellerMsCategories($seller_id);
		$childs = [];

		foreach ($ms_categories as &$ms_category) {
			$ms_category['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller_id . '&ms_category_id=' . $ms_category['category_id']);
			$childs[$ms_category['parent_id']][] = &$ms_category;
		}

		foreach ($ms_categories as $key => &$ms_category) {
			if (isset($childs[$ms_category['category_id']]))
				$ms_category['childs'] = $childs[$ms_category['category_id']];

			if ($ms_category['parent_id'] != 0)
				unset($ms_categories[$key]);
		}

		return $this->load->view('multimerch/module/store_category', ['ms_categories' => $ms_categories, 'request' => $this->request->get]);
	}
}
