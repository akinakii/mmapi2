<?php

use MultiMerch\Core\Shipping\Manager as ShippingManager;
use MultiMerch\Core\Shipping\Shipping as Shipping;
use MultiMerch\Module\Errors\FormData as FormDataError;

/**
 * Class ControllerMultimerchShipping
 */
class ControllerMultimerchShipping extends Controller
{
	/**
	 * @var	array	Data passed to view.
	 */
	public $data = [];

	/**
	 * @var	int		Seller id.
	 */
	public $seller_id;

	/**
	 * @var	int		Product id.
	 */
	public $product_id = 0;

	/**
	 * @var	ShippingManager		MultiMerch shipping manager.
	 */
	protected $shipping_manager;

	/**
	 * ControllerMultimerchShipping constructor.
	 *
	 * @param	$registry	Opencart registry.
	 */
	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->shipping_manager = new ShippingManager($registry);

		$this->data = array_merge($this->load->language('multiseller/multiseller'), !empty($this->data) ? $this->data : []);
		$this->seller_id = $this->customer->getId();
	}

	/**
	 * Renders shipping block for seller product form.
	 *
	 * @param	int		$product_id		Product id.
	 * @return	view					Rendered tpl view.
	 */
	public function renderProductFormShipping($product_id)
	{
		$this->product_id = $product_id;
		$this->initShipping(Shipping::TYPE_PER_PRODUCT, $product_id);

		return $this->load->view('multiseller/shipping/product_form', $this->data);
	}

	/**
	 * Renders shipping block for seller settings form.
	 *
	 * @return	view	Rendered tpl view.
	 */
	public function renderSettingsShipping()
	{
		$this->initShipping($this->config->get('msconf_shipping_type'));

		return $this->load->view('multiseller/shipping/settings', $this->data);
	}

	/**
	 * Saves shipping data for seller or his product.
	 *
	 * @param	array	$data	Shipping data.
	 * @return	array			Either 'success', or 'error' message.
	 */
	public function save($data)
	{
		if (isset($data['product_id'])) {
			$this->product_id = $data['product_id'];
			$shipping_type = Shipping::TYPE_PER_PRODUCT;
		} else {
			$shipping_type = $this->config->get('msconf_shipping_type');
		}

		// Fallback rules
		if (!isset($data['rules'])) {
			$data['rules'] = [];
		}

		try {
			switch ((int)$shipping_type) {
				case Shipping::TYPE_CART_WEIGHT:
					$this->validateCartWeightBasedFormData($data);
					$this->saveCartWeightBasedRules($data);
					break;

				case Shipping::TYPE_CART_TOTAL:
					$this->validateCartTotalBasedFormData($data);
					$this->saveCartTotalBasedRules($data);
					break;

				case Shipping::TYPE_FLAT:
					$this->validateFlatFormData($data);
					$this->saveFlatRules($data);
					break;

				case Shipping::TYPE_PER_PRODUCT:
					$this->validatePerProductFormData($data);
					$this->savePerProductRules($data);
					break;

				case Shipping::TYPE_DISABLED:
				case Shipping::TYPE_OC:
				default:
					// Do nothing
					break;
			}

			if ($this->config->get('msconf_shipping_field_from_enabled') && isset($data['from_country_id'])) {
				$this->MsLoader->MsSetting->createSellerSetting([
					'seller_id' => $this->seller_id,
					'settings' => ['slr_shipping_from_country_id' => $data['from_country_id']]
				]);
			} else {
				$this->MsLoader->MsSetting->deleteSellerSetting([
					'seller_id' => $this->seller_id,
					'code' => 'slr_shipping_from_country_id',
					'name' => 'slr_shipping_from_country_id'
				]);
			}

			if ($this->config->get('msconf_shipping_field_processing_time_enabled') && isset($data['processing_days'])) {
				$this->MsLoader->MsSetting->createSellerSetting([
					'seller_id' => $this->seller_id,
					'settings' => ['slr_shipping_processing_days' => $data['processing_days']]
				]);
			} else {
				$this->MsLoader->MsSetting->deleteSellerSetting([
					'seller_id' => $this->seller_id,
					'code' => 'slr_shipping_processing_days',
					'name' => 'slr_shipping_processing_days'
				]);
			}
		} catch (FormDataError $e) {
			return ['error' => $e->getMessage()];
		}

		return ['success' => true];
	}

	/**
	 * Saves shipping data from POST request.
	 *
	 * @return	Response	JSON response with either success or error message.
	 */
	public function jxSave()
	{
		$response = $this->save($this->request->post['shipping']);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($response));
	}

	/**
	 * Gets list of available countries.
	 *
	 * @return	Response	JSON response with a list of countries.
	 */
	public function jxGetCountries()
	{
		$json = [];

		$data = $this->request->get;

		$countries = $this->shipping_manager->getCountries();

		foreach ($countries as $country) {
			$json[] = [
				'country_id' => $country['country_id'],
				'name' => strip_tags(html_entity_decode($country['name'], ENT_QUOTES, 'UTF-8'))
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Gets list of available geo zones.
	 *
	 * @return	Response	JSON response with a list of geo zones.
	 */
	public function jxGetGeoZones()
	{
		$json = [];

		$data = $this->request->get;

		$geo_zones = $this->shipping_manager->getGeoZones();

		foreach ($geo_zones as $geo_zone) {
			$json[] = [
				'geo_zone_id' => $geo_zone['geo_zone_id'],
				'name' => strip_tags(html_entity_decode($geo_zone['name'], ENT_QUOTES, 'UTF-8'))
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Gets list of available shipping methods.
	 *
	 * @return	Response	JSON response with a list of shipping methods.
	 */
	public function jxGetShippingMethods()
	{
		$json = [];

		$data = $this->request->get;

		$shipping_methods = $this->shipping_manager->getShippingMethods();

		foreach ($shipping_methods as $shipping_method) {
			$json[] = [
				'shipping_method_id' => $shipping_method['shipping_method_id'],
				'name' => strip_tags(html_entity_decode($shipping_method['name'], ENT_QUOTES, 'UTF-8')),
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Gets list of available delivery times.
	 *
	 * @return	Response	JSON response with a list of delivery times.
	 */
	public function jxGetDeliveryTimes()
	{
		$json = [];

		$data = $this->request->get;

		$delivery_times = $this->shipping_manager->getDeliveryTimes();

		foreach ($delivery_times as $delivery_time) {
			$json[] = [
				'delivery_time_id' => $delivery_time['delivery_time_id'],
				'name' => strip_tags(html_entity_decode($delivery_time['name'], ENT_QUOTES, 'UTF-8')),
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Initializes default shipping data by shipping type.
	 *
	 * @param	int		$shipping_type	Shipping type id.
	 * @param	int		$product_id		Product id.
	 */
	protected function initShipping($shipping_type, $product_id = 0)
	{
		$this->document->addScript('catalog/view/javascript/multimerch/selectize/selectize.min.js');
		$this->document->addStyle('catalog/view/javascript/multimerch/selectize/selectize.bootstrap3.css');
		$this->document->addScript('catalog/view/javascript/multimerch/shipping.js');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/multimerch/shipping.css');

		$this->data['shipping'] = [
			'fields' => $this->getShippingFields($shipping_type),
			'rules' => $this->getShippingRules(['shipping_type' => $shipping_type, 'product_id' => $product_id]),
		];

		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $this->seller_id]);

		// Shipping from
		if (isset($seller_settings['slr_shipping_from_country_id'])) {
			$this->data['shipping']['from_country'] = $this->MsLoader->MsShipping->getCountries(['country_id' => $seller_settings['slr_shipping_from_country_id']]);
		}

		// Processing days
		if (isset($seller_settings['slr_shipping_processing_days'])) {
			$this->data['shipping']['processing_days'] = $seller_settings['slr_shipping_processing_days'];
		}
	}

	/**
	 * Gets required shipping fields by shipping type.
	 *
	 * @param	int		$shipping_type	Shipping type id.
	 * @return	array					Array of required shipping fields.
	 */
	public function getShippingFields($shipping_type)
	{
		$fields = ['destinations', 'cost'];

		if ($this->config->get('msconf_shipping_field_from_enabled')) {
			$fields[] = 'from_country_id';
		}

		if ($this->config->get('msconf_shipping_field_processing_time_enabled')) {
			$fields[] = 'processing_days';
		}

		if ($this->config->get('msconf_shipping_field_shipping_method_enabled')) {
			$fields[] = 'shipping_method_id';
		}

		if ($this->config->get('msconf_shipping_field_delivery_time_enabled')) {
			$fields[] = 'delivery_time_id';
		}

		switch ((int)$shipping_type) {
			case Shipping::TYPE_CART_WEIGHT:
				$fields[] = 'weight_from';
				$fields[] = 'weight_to';

				if ($this->config->get('msconf_shipping_field_cost_pwu_enabled')) {
					$fields[] = 'cost_per_weight_unit';
				}
				break;

			case Shipping::TYPE_CART_TOTAL:
				$fields[] = 'total_from';
				$fields[] = 'total_to';
				break;

			case Shipping::TYPE_PER_PRODUCT:
				if ($this->config->get('msconf_shipping_field_cost_ai_enabled')) {
					$fields[] = 'cost_per_additional_item';
				}
				break;

			case Shipping::TYPE_DISABLED:
			case Shipping::TYPE_OC:
			case Shipping::TYPE_FLAT:
			default:
				// No fields added
				break;
		}

		return $fields;
	}

	/**
	 * Gets shipping rules by shipping type.
	 *
	 * @param	array	$data	Conditions. Must have 'shipping_type', optional - 'seller_id', 'product_id'.
	 * @return	array			Array of existing shipping rules.
	 */
	public function getShippingRules($data)
	{
		$seller_id = isset($data['seller_id']) ? (int)$data['seller_id'] : $this->seller_id;
		$product_id = isset($data['product_id']) ? (int)$data['product_id'] : $this->product_id;
		$shipping_type = (int)$data['shipping_type'];

		switch ($shipping_type) {
			case Shipping::TYPE_CART_WEIGHT:
				$rules = $this->MsLoader->MsShipping->getCartWeightBasedRules($seller_id);
				break;

			case Shipping::TYPE_CART_TOTAL:
				$rules = $this->MsLoader->MsShipping->getCartTotalBasedRules($seller_id);
				break;

			case Shipping::TYPE_FLAT:
				$rules = $this->MsLoader->MsShipping->getFlatRules($seller_id);
				break;

			case Shipping::TYPE_PER_PRODUCT:
				$rules = $this->MsLoader->MsShipping->getPerProductRules($seller_id, $product_id);
				break;

			case Shipping::TYPE_DISABLED:
			case Shipping::TYPE_OC:
			default:
				$rules = [];
				break;
		}

		return $rules;
	}

	/**
	 * Validates default form data.
	 *
	 * @param	array	$data	Form data.
	 * @throws	FormDataError
	 */
	private function validateFormData($data)
	{
		if (!isset($data['rules'])) {
			return;
		}

		foreach ($data['rules'] as $rule) {
			// Minimum required fields for each rule
			if (empty($rule['destinations'])) {
				throw new FormDataError($this->language->get('ms_shipping_error_rule_destinations_required'));
			}

			if (!isset($rule['cost'])) {
				throw new FormDataError($this->language->get('ms_shipping_error_rule_cost_required'));
			}
		}
	}

	/**
	 * Validates form data specific for cart weight-based shipping rules.
	 *
	 * @param	array	$data	Form data.
	 * @throws	FormDataError
	 */
	public function validateCartWeightBasedFormData($data)
	{
		$this->validateFormData($data);

		foreach ($data['rules'] as $rule) {
			if (!isset($rule['weight_from']) || !isset($rule['weight_to'])) {
				throw new FormDataError($this->language->get('ms_shipping_error_rule_weight_range_required'));
			}
		}
	}

	/**
	 * Validates form data specific for cart total-based shipping rules.
	 *
	 * @param	array	$data	Form data.
	 * @throws	FormDataError
	 */
	public function validateCartTotalBasedFormData($data)
	{
		$this->validateFormData($data);

		foreach ($data['rules'] as $rule) {
			if (!isset($rule['total_from']) || !isset($rule['total_to'])) {
				throw new FormDataError($this->language->get('ms_shipping_error_rule_total_range_required'));
			}
		}
	}

	/**
	 * Validates form data specific for flat shipping rules.
	 *
	 * @param	array	$data	Form data.
	 * @throws	FormDataError
	 */
	public function validateFlatFormData($data)
	{
		$this->validateFormData($data);
	}

	/**
	 * Validates form data specific for per-product shipping rules.
	 *
	 * @param	array	$data	Form data.
	 * @throws	FormDataError
	 */
	public function validatePerProductFormData($data)
	{
		$this->validateFormData($data);
	}

	/**
	 * Saves cart weight-based shipping rules.
	 *
	 * @param	array	$data	Validated shipping data.
	 */
	public function saveCartWeightBasedRules($data)
	{
		$this->MsLoader->MsShipping->deleteCartWeightBasedRule($this->seller_id);

		foreach ($data['rules'] as $rule) {
			$this->MsLoader->MsShipping->createCartWeightBasedRule(array_merge($rule, ['seller_id' => $this->seller_id]));
		}
	}

	/**
	 * Saves cart total-based shipping rules.
	 *
	 * @param	array	$data	Validated shipping data.
	 */
	public function saveCartTotalBasedRules($data)
	{
		$this->MsLoader->MsShipping->deleteCartTotalBasedRule($this->seller_id);

		foreach ($data['rules'] as $rule) {
			$this->MsLoader->MsShipping->createCartTotalBasedRule(array_merge($rule, ['seller_id' => $this->seller_id]));
		}
	}

	/**
	 * Saves flat shipping rules.
	 *
	 * @param	array	$data	Validated shipping data.
	 */
	public function saveFlatRules($data)
	{
		$this->MsLoader->MsShipping->deleteFlatRule($this->seller_id);

		foreach ($data['rules'] as $rule) {
			$this->MsLoader->MsShipping->createFlatRule(array_merge($rule, ['seller_id' => $this->seller_id]));
		}
	}

	/**
	 * Saves per-product shipping rules.
	 *
	 * @param	array	$data	Validated shipping data.
	 */
	public function savePerProductRules($data)
	{
		$this->MsLoader->MsShipping->deletePerProductRule($this->seller_id, $this->product_id);

		foreach ($data['rules'] as $rule) {
			$this->MsLoader->MsShipping->createPerProductRule(array_merge($rule, ['seller_id' => $this->seller_id, 'product_id' => $this->product_id]));
		}
	}
}
