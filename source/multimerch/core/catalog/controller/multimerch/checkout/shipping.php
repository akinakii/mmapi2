<?php

use MultiMerch\Core\Shipping\Shipping as Shipping;
use MultiMerch\Module\Errors\Generic as GenericError;
use MultiMerch\Module\Errors\FormData as FormDataError;

class ControllerMultimerchCheckoutShipping extends \Controller
{
	protected $data;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->data = array_merge($this->load->language('checkout/checkout'), $this->load->language('multiseller/multiseller'), !empty($this->data) ? $this->data : []);

		$this->load->model('tool/image');
		$this->load->model('localisation/country');
		$this->load->model('localisation/zone');

		$cart_has_shipping = $this->cart->hasShipping();
		$this->data['text_checkout_payment_method'] = sprintf($this->data['text_checkout_payment_method'], $cart_has_shipping ? 5 : 3);
		$this->data['text_checkout_confirm'] = sprintf($this->data['text_checkout_confirm'], $cart_has_shipping ? 6 : 4);
	}

	public function index()
	{
		try {
			$shipping_address = $this->processCustomerAddress();
			list($products_by_sellers, $contains_digital, $errors) = $this->processCartProducts($shipping_address);

			$this->data['shipping'] = [
				'address' => $shipping_address,
				'carts' => $products_by_sellers,
				'contains_digital' => $contains_digital,
				'errors' => $errors
			];
		} catch (GenericError $e) {
			$this->data['error'] = $e->getMessage();
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/checkout/shipping');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	protected function processCustomerAddress()
	{
		if ($this->customer->isLogged() && isset($this->request->get['address_id'])) {
			$this->load->model('account/address');

			// Selected stored address
			$this->session->data['shipping_address_id'] = $this->request->get['address_id'];
			$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->request->get['address_id']);

			if (isset($this->session->data['guest']))
				unset($this->session->data['guest']);
		} elseif (isset($this->request->post['country_id'])) {
			// Selected new address OR is a guest
			$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
			$zone_info = isset($this->request->post['zone_id']) ? $this->model_localisation_zone->getZone($this->request->post['zone_id']) : '';

			$shipping_address['country'] = $country_info['name'];
			$shipping_address['iso_code_2'] = $country_info['iso_code_2'];
			$shipping_address['iso_code_3'] = $country_info['iso_code_3'];
			$shipping_address['address_format'] = $country_info['address_format'];
			$shipping_address['zone'] = $zone_info ? $zone_info['name'] : '';
			$shipping_address['zone_code'] = $zone_info ? $zone_info['code'] : '';
			$shipping_address['firstname'] = $this->request->post['firstname'];
			$shipping_address['lastname'] = $this->request->post['lastname'];
			$shipping_address['company'] = $this->request->post['company'];
			$shipping_address['address_1'] = $this->request->post['address_1'];
			$shipping_address['address_2'] = $this->request->post['address_2'];
			$shipping_address['postcode'] = $this->request->post['postcode'];
			$shipping_address['city'] = $this->request->post['city'];
			$shipping_address['country_id'] = $this->request->post['country_id'];
			$shipping_address['zone_id'] = $this->request->post['zone_id'];

			$this->session->data['shipping_address'] = $shipping_address;
		}

		if (!isset($this->session->data['shipping_address'])) {
			throw new GenericError($this->language->get('ms_shipping_address_session_empty'));
		}

		$shipping_address = $this->session->data['shipping_address'];
		$shipping_address['name'] = isset($shipping_address['firstname']) && isset($shipping_address['lastname']) ? sprintf("%s %s", $shipping_address['firstname'], $shipping_address['lastname']) : '';
		$shipping_address['inline'] = ($shipping_address['name'] ? "{$shipping_address['name']}," : "")
			. (!empty($shipping_address['address_1']) ? " {$shipping_address['address_1']}," : "")
			. (!empty($shipping_address['city']) ? " {$shipping_address['city']}," : "")
			. (!empty($shipping_address['postcode']) ? " {$shipping_address['postcode']}," : "")
			. (!empty($shipping_address['country']) ? " {$shipping_address['country']}" : "")
		;

		return $shipping_address;
	}

	protected function processCartProducts($shipping_address)
	{
		$cart_products = $this->cart->getProducts();

		$products_by_sellers = [];
		$contains_digital = false;
		$errors = [];

		foreach ($cart_products as &$cart_product) {
			$seller_id = $cart_product['seller_id'];

			if (empty($products_by_sellers[$seller_id])) {
				$products_by_sellers[$seller_id] = [
					'individual'	=> [], // products with custom per-product shipping rules
					'digital'		=> [], // digital products that don't require shipping
					'unavailable'	=> [], // products that can't be shipped
					'combined'		=> [   // products that are shipped in group using global shipping rules
						'products' => [],
						'shipping_methods' => [],
						'errors' => []
					]
				];
			}

			$product_info = $this->prepareProductInfo($cart_product);

			$this->determineProductShipping($product_info, $products_by_sellers[$seller_id]);
		}

		// Get appropriate shipping rules for individual products
		foreach ($products_by_sellers as $seller_id => &$products) {
			if (!empty($products['individual'])) {
				$this->getShippingMethodsForIndividualProducts($products, $shipping_address);
			}

			if (!empty($products['digital'])) {
				$contains_digital = true;
			}

			if (!empty($products['combined']['products'])) {
				$this->getShippingMethodsForCombinedProducts($products['combined'], $seller_id, $shipping_address);
			}

			$this->getErrors($products, $errors);
		}

		return [$products_by_sellers, $contains_digital, $errors];
	}

	public function jxSave()
	{
		$json = [];

		try {
			$data = $this->request->post;
			list($products_by_sellers, $contains_digital, $errors) = $this->processCartProducts($this->processCustomerAddress());

			$this->validateShippingData($data, $products_by_sellers, $contains_digital, $errors);

			foreach ($products_by_sellers as $seller_id => $products) {
				$shipping_data = [];

				foreach ($products['individual'] as $product) {
					$rule_id = $data['shipping'][$seller_id]['individual'][$product['cart_id']];
					$shipping_data['individual'][$product['cart_id']] = $product['shipping_methods'][$rule_id];
				}

				foreach ($products['digital'] as $product) {
					$shipping_data['digital'][$product['cart_id']] = true;
				}

				if (!empty($products['combined']['products'])) {
					$rule_id = $data['shipping'][$seller_id]['combined'][(int)$this->config->get('msconf_shipping_type')];
					$shipping_data['combined'] = $products['combined']['shipping_methods'][$rule_id];
				}

				$this->session->data['ms_checkout_shipping'][$seller_id] = $shipping_data;
			}

			$json['success'] = true;
		} catch (GenericError $e) {
			$json['errors'][] = $e->getMessage();
		} catch (FormDataError $e) {
			$json['errors'] = $e->getErrors();
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxRemoveProduct()
	{
		$json = [];

		try {
			$data = $this->request->post;

			if (!isset($data['cart_id'])) {
				throw new FormDataError("Error occurred when deleting product from cart");
			}

			$this->cart->remove($data['cart_id']);

			if (isset($this->session->data['vouchers'][$data['cart_id']])) {
				unset($this->session->data['vouchers'][$data['cart_id']]);
			}

			$json['success'] = true;
		} catch (FormDataError $e) {
			$json['error'] = $e->getMessage();
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function validateShippingData($data, $products_by_sellers, $contains_digital, $errors)
	{
		$validation_errors = [];

		// Refresh shipping data in session
		if (isset($this->session->data['ms_checkout_shipping'])) {
			unset($this->session->data['ms_checkout_shipping']);
		}

		// Validate products in cart require shipping
		if (!$this->cart->hasShipping()) {
			$validation_errors[] = "No products in cart require shipping.";
		}

		// Validate customer shipping address
		if (!isset($this->session->data['shipping_address'])) {
			$validation_errors[] = $this->language->get('mm_checkout_shipping_error_shipping_address');
		}

		// Validate cart has products and they are in stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$validation_errors[] = "Either cart is empty, or some of the products are out of stock.";
		}

		// Validate shipping step does not contain errors
		if (!empty($errors)) {
			$validation_errors[] = $this->language->get('ms_shipping_checkout_error_general');
		}

		// Validation for digital-only carts
		if (empty($data['shipping']) && !$contains_digital) {
			$validation_errors[] = "You must select shipping options.";
		}

		// Validate selected shipping methods are appropriate for products and sellers
		foreach ($products_by_sellers as $seller_id => $products) {
			// Validate individual products
			foreach ($products['individual'] as $product) {
				$rule_id = isset($data['shipping'][$seller_id]['individual'][$product['cart_id']]) ? $data['shipping'][$seller_id]['individual'][$product['cart_id']] : null;

				if (!$rule_id || !in_array($rule_id, array_keys($product['shipping_methods']))) {
					$validation_errors[] = "You must select a shipping option for {$product['name']}.";
					break;
				}
			}

			// Validate combined products
			if (!empty($products['combined']['products'])) {
				$rule_id = isset($data['shipping'][$seller_id]['combined'][(int)$this->config->get('msconf_shipping_type')]) ? $data['shipping'][$seller_id]['combined'][(int)$this->config->get('msconf_shipping_type')] : null;

				if (!$rule_id || !in_array($rule_id, array_keys($products['combined']['shipping_methods']))) {
					$validation_errors[] = "You must select a shipping option for products by {$this->MsLoader->MsSeller->getSellerNickname($seller_id)}.";
				}
			}
		}

		if (!empty($validation_errors)) {
			throw new FormDataError("multimerch/checkout/shipping - Validation of selected shipping options failed", 1, $validation_errors);
		}
	}

	protected function prepareProductInfo($product)
	{
		$msf_variation_data = [];

		if (!empty($product['msf_variation'])) {
			foreach ($product['msf_variation'] as $msf_variation) {
				$msf_variation_data[] = [
					'name'  => $msf_variation['name'],
					'value' => (utf8_strlen($msf_variation['value']) > 20 ? utf8_substr($msf_variation['value'], 0, 20) . '..' : $msf_variation['value'])
				];
			}
		}

		return [
			'cart_id' 			=> $product['cart_id'],
			'product_id' 		=> $product['product_id'],
			'name' 				=> $product['name'],
			'model' 			=> $product['model'],
			'msf_variation'		=> $msf_variation_data,
			'options' 			=> $product['option'],
			'price' 			=> $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'), $this->session->data['currency']),
			'price_formatted' 	=> $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
			'quantity' 			=> $product['quantity'],
			'weight' 			=> $product['weight'],
			'weight_class_id' 	=> $product['weight_class_id'],
			'image' 			=> $this->model_tool_image->resize($product['image'] ?: 'no_image.png', $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height')),
			'seller_id'			=> $product['seller_id'],
			'seller_name' 		=> $this->MsLoader->MsSeller->getSellerNickname($product['seller_id']) ?: $this->language->get('ms_store_owner'),
			'is_digital'		=> !$product['shipping']
		];
	}

	protected function determineProductShipping($product, &$products)
	{
		// Product is digital
		if ($product['is_digital']) {
			$products['digital'][] = $product;

			return true;
		}

		// Product has individual per-product shipping rules
		if ($this->config->get('msconf_shipping_per_product_override_allow') && $this->MsLoader->MsShipping->getPerProductRules($product['seller_id'], $product['product_id'])) {
			$products['individual'][] = $product;

			return true;
		}

		// Product must be shipped with other products
		if (in_array((int)$this->config->get('msconf_shipping_type'), [Shipping::TYPE_CART_WEIGHT, Shipping::TYPE_CART_TOTAL, Shipping::TYPE_FLAT])) {
			$products['combined']['products'][] = $product;

			return true;
		}

		// Product is not shippable
		$products['unavailable'][] = $product;

		return true;
	}

	protected function getErrors($products, &$errors)
	{
		foreach ($products['unavailable'] as $product) {
			$errors[] = $this->language->get('ms_shipping_checkout_error_individual_general');
		}

		foreach ($products['combined']['errors'] as $error) {
			$errors[] = $error;
		}
	}

	protected function getShippingMethodsForIndividualProducts(&$products, $address)
	{
		foreach ($products['individual'] as $key => &$product) {
			$product['shipping_methods'] = [];

			$product_id = $product['product_id'];
			$seller_id = $product['seller_id'];

			$rules = $this->MsLoader->MsShipping->getPerProductRules($seller_id, $product_id);

			if (empty($rules)) {
				continue;
			}

			foreach ($rules as $rule) {
				if ($this->MsLoader->MsShipping->isRuleAvailableForAddress($rule, $address)) {
					$cost = $rule['cost'];

					if ($this->config->get('msconf_shipping_field_cost_ai_enabled')) {
						$cost += $rule['cost_per_additional_item'] * ($product['quantity'] - 1);
					}

					$product['shipping_methods'][$rule['id']] = [
						'name' => $rule['shipping_method_name'],
						'delivery_time' => $rule['delivery_time_name'],
						'cost' => $cost,
						'cost_formatted' => $this->currency->format($cost, $this->config->get('config_currency'))
					];
				}
			}

			if (!empty($product['shipping_methods'])) {
				uasort($product['shipping_methods'], function ($method_1, $method_2) {
					return $method_1['cost'] <=> $method_2['cost'];
				});
			} else {
				unset($products['individual'][$key]);
				$products['unavailable'][] = $product;
			}
		}
	}

	protected function getShippingMethodsForCombinedProducts(&$data, $seller_id, $address)
	{
		switch ((int)$this->config->get('msconf_shipping_type')) {
			case Shipping::TYPE_CART_WEIGHT:
				list($data['shipping_methods'], $data['errors']) = $this->getCartWeightBasedShippingMethods($seller_id, $data['products'], $address);
				break;

			case Shipping::TYPE_CART_TOTAL:
				list($data['shipping_methods'], $data['errors']) = $this->getCartTotalBasedShippingMethods($seller_id, $data['products'], $address);
				break;

			case Shipping::TYPE_FLAT:
				list($data['shipping_methods'], $data['errors']) = $this->getFlatShippingMethods($seller_id, $address);
				break;

			default:
				throw new GenericError("multimerch/checkout/shipping - Incorrect shipping type: {$this->config->get('msconf_shipping_type')}");
				break;
		}

		// Sort from cheap to expensive
		uasort($data['shipping_methods'], function ($method_1, $method_2) {
			return $method_1['cost'] <=> $method_2['cost'];
		});
	}

	protected function getCartWeightBasedShippingMethods($seller_id, $products, $address)
	{
		$shipping_methods = $errors = [];

		$rules = $this->MsLoader->MsShipping->getCartWeightBasedRules($seller_id);

		$cart_weight = $this->calculateCartWeight($products);

		$min_cart_weight = $max_cart_weight = -1;

		foreach ($rules as $rule) {
			$weight_from = $this->weight->convert($rule['weight_from'], $rule['weight_class_id'], $this->config->get('config_weight_class_id'));
			$weight_to = $this->weight->convert($rule['weight_to'], $rule['weight_class_id'], $this->config->get('config_weight_class_id'));

			// Used for error generation
			$this->updateMinMaxValues($min_cart_weight, $max_cart_weight, $weight_from, $weight_to);

			if ($this->MsLoader->MsShipping->isRuleAvailableForAddress($rule, $address) && $weight_from <= $cart_weight && $cart_weight <= $weight_to) {
				$cost = $rule['cost'];

				if ($this->config->get('msconf_shipping_field_cost_pwu_enabled')) {
					$cost += $rule['cost_per_weight_unit'] * $cart_weight;
				}

				$shipping_methods[$rule['id']] = [
					'name' => $rule['shipping_method_name'],
					'delivery_time' => $rule['delivery_time_name'],
					'cost' => $cost,
					'cost_formatted' => $this->currency->format($cost, $this->config->get('config_currency'))
				];
			}
		}

		if (empty($shipping_methods)) {
			$errors[] = $this->getCartWeightBasedShippingError($cart_weight, $min_cart_weight, $max_cart_weight);
		}

		return [$shipping_methods, $errors];
	}

	protected function getCartTotalBasedShippingMethods($seller_id, $products, $address)
	{
		$shipping_methods = $errors = [];

		$rules = $this->MsLoader->MsShipping->getCartTotalBasedRules($seller_id);

		$cart_total = $this->calculateCartTotal($products);

		$min_cart_total = $max_cart_total = -1;

		foreach ($rules as $rule) {
			$total_from = $this->currency->convert($rule['total_from'], $rule['currency_code'], $this->config->get('config_currency'));
			$total_to = $this->currency->convert($rule['total_to'], $rule['currency_code'], $this->config->get('config_currency'));

			// Used for error generation
			$this->updateMinMaxValues($min_cart_total, $max_cart_total, $total_from, $total_to);

			if ($this->MsLoader->MsShipping->isRuleAvailableForAddress($rule, $address) && $total_from <= $cart_total && $cart_total <= $total_to) {
				$shipping_methods[$rule['id']] = [
					'name' => $rule['shipping_method_name'],
					'delivery_time' => $rule['delivery_time_name'],
					'cost' => $rule['cost'],
					'cost_formatted' => $this->currency->format($rule['cost'], $this->config->get('config_currency'))
				];
			}
		}

		if (empty($shipping_methods)) {
			$errors[] = $this->getCartTotalBasedShippingError($cart_total, $min_cart_total, $max_cart_total);
		}

		return [$shipping_methods, $errors];
	}

	protected function getFlatShippingMethods($seller_id, $address)
	{
		$shipping_methods = $errors = [];

		$rules = $this->MsLoader->MsShipping->getFlatRules($seller_id);

		foreach ($rules as $rule) {
			if ($this->MsLoader->MsShipping->isRuleAvailableForAddress($rule, $address)) {
				$shipping_methods[$rule['id']] = [
					'name' => $rule['shipping_method_name'],
					'delivery_time' => $rule['delivery_time_name'],
					'cost' => $rule['cost'],
					'cost_formatted' => $this->currency->format($rule['cost'], $this->config->get('config_currency'))
				];
			}
		}

		if (empty($shipping_methods)) {
			$errors[] = $this->language->get('ms_shipping_checkout_error_combined_general');
		}

		return [$shipping_methods, $errors];
	}

	private function calculateCartWeight($products)
	{
		$weight = 0;

		foreach ($products as $product) {
			$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
		}

		return $weight;
	}

	private function getCartWeightBasedShippingError($weight, $min, $max)
	{
		if ($min > -1 && $weight < $min) {
			$error = sprintf(
				$this->language->get('ms_shipping_checkout_error_cart_weight_less_than_min'),
				$this->weight->format($weight, $this->config->get('config_weight_class_id')),
				$this->weight->format($min, $this->config->get('config_weight_class_id'))
			);
		} elseif ($max > -1 && $weight > $max) {
			$error = sprintf(
				$this->language->get('ms_shipping_checkout_error_cart_weight_more_than_max'),
				$this->weight->format($weight, $this->config->get('config_weight_class_id')),
				$this->weight->format($max, $this->config->get('config_weight_class_id'))
			);
		} else {
			$error = $this->language->get('ms_shipping_checkout_error_combined_general');
		}

		return $error;
	}

	private function getCartTotalBasedShippingError($total, $min, $max)
	{
		if ($min > -1 && $total < $min) {
			$error = sprintf(
				$this->language->get('ms_shipping_checkout_error_cart_total_less_than_min'),
				$this->currency->format($total, $this->config->get('config_currency')),
				$this->currency->format($min, $this->config->get('config_currency'))
			);
		} elseif ($max > -1 && $total > $max) {
			$error = sprintf(
				$this->language->get('ms_shipping_checkout_error_cart_total_more_than_max'),
				$this->currency->format($total, $this->config->get('config_currency')),
				$this->currency->format($max, $this->config->get('config_currency'))
			);
		} else {
			$error = $this->language->get('ms_shipping_checkout_error_combined_general');
		}

		return $error;
	}

	private function calculateCartTotal($products)
	{
		$total = 0;

		foreach ($products as $product) {
			$total += $product['price'] * $product['quantity'];
		}

		return $total;
	}

	private function updateMinMaxValues(&$min, &$max, $new_min, $new_max)
	{
		if (-1 === (int)$min || $new_min < $min) {
			$min = $new_min;
		}

		if ($new_max > $max) {
			$max = $new_max;
		}
	}
}
