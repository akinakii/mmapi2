<?php

class ControllerMultimerchCheckoutPaymentMethod extends \Controller
{
	public function renderSuborderCustomerComments()
	{
		$cart_products = $this->cart->getProducts();

		$data = array_merge($this->load->language('multiseller/multiseller'), ['suborder_comments' => []]);

		foreach ($cart_products as $cart_product) {
			if (empty($cart_product['seller_id'])) {
				continue;
			}

			$seller_id = $cart_product['seller_id'];

			$data['suborder_comments'][$seller_id] = [
				'seller_name' => $this->MsLoader->MsSeller->getSellerNickname($seller_id),
				'comment' => $this->session->data['suborder_comments'][$seller_id] ?? ''
			];
		}

		return $this->load->view('multimerch/checkout/suborder_comment', $data);
	}
}
