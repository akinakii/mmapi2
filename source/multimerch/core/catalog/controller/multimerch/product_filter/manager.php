<?php

class ControllerMultimerchProductFilterManager extends Controller
{
	public $data = [];

	protected $model;
	protected $helper;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'), $this->load->language("multimerch/product_filter"));
	}

	public function instance()
	{
		return $this;
	}

	public function renderModule($request)
	{
		$url_data = $this->helper->fetchGetRequest($request);

		$this->data['blocks'] = $this->getBlocks($url_data);

		$this->data['ms_pf_config'] = [
			'query_params' => [
				'common' => implode(',', $this->helper->getCommonQueryParams()),
				'multimerch' => implode(',', $this->helper->getMultimerchQueryParams())
			],
			'category_path' => $url_data['path'],
			'seller_id' => $url_data['seller_id'],
			'ms_category_id' => $url_data['ms_category_id'],
			'DOM_selectors' => [
				'module' => '#multimerch_productfilter',
				'sort_limit_panel' => '#mspf_sort_limit_panel',
				'products_container' => '#mspf_products_container',
				'limit' => '#input-limit',
				'sort' => '#input-sort',
				'pagination' => '#mspf_pagination',
				'products_count' => '#mspf_products_count'
			],
			'urls' => [
				'render_category_page' => $this->url->link('multimerch/product_filter/manager/jxRenderCategoryPage', '', 'SSL'),
				'current_category' => $this->url->link('product/category', 'path=' . $url_data['path'], '', 'SSL'),
				'render_seller_store_page' => $this->url->link('multimerch/product_filter/manager/jxRenderSellerStorePage', '', 'SSL'),
				'current_seller_store_category' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $url_data['seller_id'] . (!empty($url_data['ms_category_id']) ? '&ms_category_id=' . $url_data['ms_category_id'] : ''), 'SSL')
			],
			'language' => $this->load->language('multimerch/product_filter')
		];

		return $this->load->view('module/multimerch_productfilter', $this->data);
	}

	public function renderCategoryPage($request)
	{
		$url_data = $this->helper->fetchGetRequest($request);
		$query_string = [
			'common' => $this->helper->getCommonQueryString($url_data),
			'multimerch' => $this->helper->getMultimerchQueryString($url_data)
		];

		$data = $this->load->language('product/category');

		list($data['products'], $total_products) = $this->getProductsContainerData($url_data);
		$data['sorts'] = $this->getSortsData($url_data, $query_string);
		$data['limits'] = $this->getLimitsData($url_data, $query_string);
		$data['pagination'] = $this->getPaginationData($url_data, $query_string, $total_products);
		$data['results'] = $this->getProductsCountData($url_data, $total_products);
		$data['sort'] = $url_data['sort'];
		$data['order'] = $url_data['order'];
		$data['limit'] = $url_data['limit'];

		$data['continue'] = $this->url->link('common/home');
		$data['compare'] = $this->url->link('product/compare');

		// Fixes notices at rendered page
		$data['breadcrumbs'] = [];
		$data['thumb'] = '';
		$data['categories'] = [];
		$data['text_compare'] = '';
		$data['heading_title'] = '';
		$data['description'] = '';
		$data['text_sort'] = '';
		$data['text_limit'] = '';
		$data['button_grid'] = '';
		$data['button_list'] = '';

		// Fixes route parameter so the modules will be correctly added to column left/right and content top/bottom sections of a page
		$this->request->get['route'] = 'product/category';

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		return $this->load->view('product/category', $data);
	}

	public function renderSellerStorePage($request)
	{
		$url_data = $this->helper->fetchGetRequest($request);

		$query_string = [
			'common' => $this->helper->getCommonQueryString($url_data),
			'multimerch' => $this->helper->getMultimerchQueryString($url_data)
		];

		$data = array_merge($this->load->language('product/category'), $this->load->language('multiseller/multiseller'));

		list($data['seller']['products'], $total_products) = $this->getProductsContainerData($url_data);
		$data['sorts'] = $this->getSortsData($url_data, $query_string, true);
		$data['limits'] = $this->getLimitsData($url_data, $query_string, true);
		$data['pagination'] = $this->getPaginationData($url_data, $query_string, $total_products, true);
		$data['results'] = $this->getProductsCountData($url_data, $total_products, true);
		$data['sort'] = $url_data['sort'];
		$data['order'] = $url_data['order'];
		$data['limit'] = $url_data['limit'];

		$data['continue'] = $this->url->link('common/home');

		// Fixes notices at rendered page
		$data['breadcrumbs'] = [];
		$data['thumb'] = '';
		$data['categories'] = [];
		$data['text_compare'] = '';
		$data['heading_title'] = '';
		$data['description'] = '';
		$data['text_sort'] = '';
		$data['text_limit'] = '';
		$data['button_grid'] = '';
		$data['button_list'] = '';
		$data['h1'] = '';

		// Fixes route parameter so the modules will be correctly added to column left/right and content top/bottom sections of a page
		$this->request->get['route'] = 'seller/catalog-seller/products';

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		return $this->load->view('multiseller/catalog-seller-products', $data);
	}

	public function jxRenderCategoryPage()
	{
		$this->response->addHeader('Content-Type: text/html;');
		$this->response->setOutput($this->renderCategoryPage($this->request->get));
	}

	public function jxRenderSellerStorePage()
	{
		$this->response->addHeader('Content-Type: text/html;');
		$this->response->setOutput($this->renderSellerStorePage($this->request->get));
	}

	private function getProductsContainerData($url_data)
	{
		$data = $this->helper->getProductsRequestData($url_data);

		$products_data = [];

		$this->load->model('tool/image');

		$products = $this->model->getProducts($data);
		$total_products = $this->model->getTotalProducts($data);

		foreach ($products as $product) {
			$image = $this->model_tool_image->resize($product['image'] ?: 'placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			$price = $this->customer->isLogged() || !$this->config->get('config_customer_price') ? $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) : false;
			$special = (float)$product['special'] ? $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) : false;
			$tax = $this->config->get('config_tax') ? $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']) : false;
			$rating = $this->config->get('config_review_status') ? (int)$product['rating'] : false;

			$products_data[] = array(
				'product_id'  => $product['product_id'],
				'seller' => empty($product['seller_id']) ? false : array(
					'seller_id'        => $product['seller_id'],
					'profile_link'     => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $product['seller_id'], '', 'SSL'),
					'seller_nickname'  => $product['seller_nickname'],
					'seller_avatar'    => $this->model_tool_image->resize($product['seller_avatar'] ?: "ms_no_image.jpg", 20, 20),
					'ms.rating'        => $product['ms.rating']
				),
				'thumb'       => $image,
				'name'        => $product['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'has_msf_variation' => !empty($this->MsLoader->MsfVariation->getProductMsfVariationsAll($product['product_id'])),
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'minimum'     => $product['minimum'] > 0 ? $product['minimum'] : 1,
				'rating'      => $rating,
				'href'        => $this->url->link('product/product', (isset($this->request->get['path']) ? ('path=' . $this->request->get['path'] . '&') : '') . 'product_id=' . $product['product_id'])
			);
		}

		return [$products_data, $total_products];
	}

	private function getSortsData($url_data, $query_string, $seller = false)
	{
		if ($seller) {
			$url = $this->helper->deleteQueryParams($query_string['common'], ['sort', 'order', 'page']) . $query_string['multimerch'];
		} else {
			$url = 'path=' . $url_data['path'] . $this->helper->deleteQueryParams($query_string['common'], ['sort', 'order', 'page']) . $query_string['multimerch'];
		}

		$sorts_data = [
			[
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', $url . 'sort=p.sort_order&order=ASC')
			],
			[
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', $url . '&sort=pd.name&order=ASC')
			],
			[
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', $url . '&sort=pd.name&order=DESC')
			],
			[
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', $url . '&sort=p.price&order=ASC')
			],
			[
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', $url . '&sort=p.price&order=DESC')
			]
		];

		if ($this->config->get('config_review_status')) {
			array_push($sorts_data,
				[
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', $url . '&sort=rating&order=DESC')
				],
				[
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', $url . '&sort=rating&order=ASC')
				]
			);
		}

		array_push($sorts_data,
			[
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', $url . '&sort=p.model&order=ASC')
			],
			[
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', $url . '&sort=p.model&order=DESC')
			]
		);

		return $sorts_data;
	}

	private function getLimitsData($url_data, $query_string, $seller = false)
	{
		$limits_data = [];

		if ($seller) {
			$url = $this->helper->deleteQueryParams($query_string['common'], ['limit', 'page']) . $query_string['multimerch'];
		} else {
			$url = 'path=' . $url_data['path'] . $this->helper->deleteQueryParams($query_string['common'], ['limit', 'page']) . $query_string['multimerch'];
		}

		$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$limits_data[] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/category', $url . '&limit=' . $value)
			);
		}

		return $limits_data;
	}

	private function getPaginationData($url_data, $query_string, $total_products, $seller = false)
	{
		if ($seller) {
			$url = $this->helper->deleteQueryParams($query_string['common'], ['page']) . $query_string['multimerch'];
		} else {
			$url = 'path=' . $url_data['path'] . $this->helper->deleteQueryParams($query_string['common'], ['page']) . $query_string['multimerch'];
		}

		$pagination = new Pagination();
		$pagination->total = $total_products;
		$pagination->page = $url_data['page'];
		$pagination->limit = $url_data['limit'];
		$pagination->url = $this->url->link('product/category', $url . '&page={page}');

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($url_data['page'] == 1) {
			$this->document->addLink($this->url->link('product/category', 'path=' . $url_data['category_id'], true), 'canonical');
		} elseif ($url_data['page'] == 2) {
			$this->document->addLink($this->url->link('product/category', 'path=' . $url_data['category_id'], true), 'prev');
		} else {
			$this->document->addLink($this->url->link('product/category', 'path=' . $url_data['category_id'] . '&page='. ($url_data['page'] - 1), true), 'prev');
		}

		if ($url_data['limit'] && ceil($total_products / $url_data['limit']) > $url_data['page']) {
			$this->document->addLink($this->url->link('product/category', 'path=' . $url_data['category_id'] . '&page='. ($url_data['page'] + 1), true), 'next');
		}

		$pagination_data = $pagination->render();

		return $pagination_data;
	}

	private function getProductsCountData($url_data, $total_products)
	{
		return sprintf($this->language->get('text_pagination'), ($total_products) ? (($url_data['page'] - 1) * $url_data['limit']) + 1 : 0, ((($url_data['page'] - 1) * $url_data['limit']) > ($total_products - $url_data['limit'])) ? $total_products : ((($url_data['page'] - 1) * $url_data['limit']) + $url_data['limit']), $total_products, ceil($total_products / $url_data['limit']));
	}

	protected function getBlocks($url_data)
	{
		$blocks = [];

		$data = $this->helper->getProductsRequestData($url_data);

		// Get all product ids available for current filters selection
		$product_ids = $this->cache->get('mspf_product_ids_'.md5(json_encode($data)));
		if (empty($product_ids)) {
			// We need to clear limit to get all possible ids
			$product_ids = $this->model->getProducts(array_merge($data, ['limit' => PHP_INT_MAX]), true);
			$this->cache->set('mspf_product_ids_'.md5(json_encode($data)), $product_ids);
		}

		// Get default MM product filter blocks
		$mspf_blocks = $this->model->getDefaultMspfBlocks();

		if (isset($data['filter_category_id'])) {
			$oc_category_mspf_blocks = $this->model->getOcCategoryMspfBlocks($data['filter_category_id']);
			if (!empty($oc_category_mspf_blocks)) {
				$mspf_blocks = $oc_category_mspf_blocks;
			}
		} elseif (isset($data['filter_ms_category_id'])) {
			$ms_category_mspf_blocks = $this->model->getMsCategoryMspfBlocks($data['filter_ms_category_id']);
			if (!empty($ms_category_mspf_blocks)) {
				$mspf_blocks = $ms_category_mspf_blocks;
			}
		}

		// Enable blocks from config
		foreach ($mspf_blocks as $mspf_block) {
			list($block_type, $block_type_id) = explode('-', $mspf_block['value']);

			switch ($block_type) {
				case MsProductFilter::TYPE_OC_ATTRIBUTE:
					$code = 'oc_attribute';
					break;

				case MsProductFilter::TYPE_OC_OPTION:
					$code = 'oc_option';
					break;

				case MsProductFilter::TYPE_OC_MANUFACTURER:
					$code = 'manufacturer';
					break;

				case MsProductFilter::TYPE_MSF_ATTRIBUTE:
					$code = $this->config->get('msconf_msf_attribute_enabled') ? 'msf_attribute' : false;
					break;

				case MsProductFilter::TYPE_PRICE:
					$code = 'price';
					break;

				case MsProductFilter::TYPE_CATEGORY:
					$code = false;

					// OC category page
					if (isset($data['filter_category_id'])) {
						$code = 'oc_category';
					}

					// MM seller store page
					if (isset($data['filter_ms_category_id']) && !empty($data['filter_seller_id'])) {
						$code = 'ms_category';
					}

					break;

				default:
					$code = false;
					break;
			}

			if (empty($code)) {
				continue;
			}

			$block = $this->load->controller("multimerch/product_filter/block/{$code}/instance");

			$blocks[] = $block->render($url_data, $product_ids, $block_type_id);
		}

		return $blocks;
	}
}
