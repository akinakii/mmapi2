<?php

class ControllerMultimerchProductFilterBlockManufacturer extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $block_type_id = 0)
	{
		$data = $this->helper->getProductsRequestData($url_data);

		$block_elements = [];

		$manufacturers = $this->model->getManufacturers($product_ids);

		foreach ($manufacturers as $manufacturer_id => $name) {
			$filter_manufacturer = !empty($data['filter_manufacturer']) ? $data['filter_manufacturer'] : [];

			// Filter by current manufacturer id
			if (!empty($filter_manufacturer)) {
				$filter_manufacturer .= ",{$manufacturer_id}";
			} else {
				$filter_manufacturer = $manufacturer_id;
			}

			$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_manufacturer' => $filter_manufacturer]));

			$block_elements[] = [
				'name' => "manufacturer[]",
				'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
				'value' => $manufacturer_id,
				'checked' => isset($data['filter_manufacturer']) && in_array($manufacturer_id, explode(',', $data['filter_manufacturer'])),
				'enabled' => !empty($total_products)
			];
		}

		if (empty($block_elements))
			return false;

		$block_data = [
			'title' => $this->language->get('text_manufacturers'),
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_manufacturer', $block_data);
	}
}
