<?php

class ControllerMultimerchProductFilterBlockMsCategory extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $block_type_id = 0)
	{
		$data = $this->helper->getProductsRequestData($url_data);
		$multimerch_query_string = $this->helper->getMultimerchQueryString($url_data, true);

		$ms_category_id = isset($data['filter_ms_category_id']) ? $data['filter_ms_category_id'] : false;

		$block_elements = [
			'current' => null,
			'parent' => null,
			'child' => null
		];

		// Current category
		$ms_category = $this->model->getMsCategory($ms_category_id, $data['filter_seller_id']);
		$total_products = $this->model->getTotalProducts($data);

		$block_elements['current'] = [
			'name' => 'ms_category_id',
			'label' => !empty($ms_category['name']) ? '<strong>' . htmlspecialchars_decode($ms_category['name']) . ' <span>(' . $total_products . ')</span></strong>' : '',
			'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $data['filter_seller_id'] . ($ms_category_id ? '&ms_category_id=' . $ms_category_id : '') .  $multimerch_query_string, 'SSL'),
			'value' => $ms_category_id,
			'enabled' => !empty($total_products)
		];

		// Parent category
		$parent_ms_category_id = !empty($ms_category['parent_id']) ? $ms_category['parent_id'] : false;

		if ($parent_ms_category_id) {
			$block_elements['parent'] = [
				'name' => 'ms_category_id',
				'label' => ' < ' . htmlspecialchars_decode($this->MsLoader->MsCategory->getMsCategoryName($parent_ms_category_id, $this->config->get('config_language_id'))),
				'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $data['filter_seller_id'] . '&ms_category_id=' . $parent_ms_category_id .  $multimerch_query_string, 'SSL'),
				'value' => $parent_ms_category_id,
				'enabled' => true
			];
		} elseif ($ms_category_id) {
			$block_elements['parent'] = [
				'name' => 'ms_category_id',
				'label' => ' < ' . $this->language->get('ms_all_products'),
				'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $data['filter_seller_id'] . $multimerch_query_string, 'SSL'),
				'value' => 0,
				'enabled' => true
			];
		}

		// Child categories
		$child_ms_categories = $this->model->getMsChildCategories($ms_category_id, $data['filter_seller_id']);

		foreach ($child_ms_categories as $child_ms_category) {
			$data['filter_ms_category_id'] = $child_ms_category['category_id'];
			$total_products = $this->model->getTotalProducts($data);

			$block_elements['child'][] = [
				'name' => 'ms_category_id',
				'label' => htmlspecialchars_decode($child_ms_category['name']) . ' <span>(' . $total_products . ')</span>',
				'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $data['filter_seller_id'] . '&ms_category_id=' . $child_ms_category['category_id'] .  $multimerch_query_string, 'SSL'),
				'value' => $child_ms_category['category_id'],
				'enabled' => !empty($total_products)
			];
		}

		$block_data = [
			'title' => $this->language->get('text_categories'),
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_ms_category', $block_data);
	}
}
