<?php

class ControllerMultimerchProductFilterBlockOcCategory extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $block_type_id = 0)
	{
		$category_id = isset($url_data['category_id']) ? $url_data['category_id'] : false;

		if (!$category_id) {
			$this->ms_logger->info('MsProductFilter - Block Oc category: No category id passed.');
			return false;
		}

		$data = $this->helper->getProductsRequestData($url_data);
		$multimerch_query_string = $this->helper->getMultimerchQueryString($url_data, true);

		$block_elements = [
			'current' => null,
			'parent' => null,
			'child' => null
		];

		// Current category
		$category = $this->model->getOcCategory($category_id);
		$total_products = $this->model->getTotalProducts($data);

		$block_elements['current'] = [
			'name' => 'category_id',
			'label' => '<strong>' . htmlspecialchars_decode($category['name']) . ' <span>(' . $total_products . ')</span></strong>',
			'href' => $this->url->link('product/category', 'path=' . $url_data['path'] .  $multimerch_query_string, 'SSL'),
			'value' => $category_id,
			'enabled' => !empty($total_products)
		];

		// Parent category
		$parent_category_id = !empty($category['parent_id']) ? $category['parent_id'] : false;

		if ($parent_category_id) {
			$block_elements['parent'] = [
				'name' => 'category_id',
				'label' => ' < ' . htmlspecialchars_decode($this->MsLoader->MsCategory->getOcCategoryName($parent_category_id, $this->config->get('config_language_id'))),
				'href' => $this->url->link('product/category', 'path=' . str_replace(',', '_',$this->MsLoader->MsCategory->getOcCategoryPath($parent_category_id)) . $multimerch_query_string, 'SSL'),
				'value' => $parent_category_id,
				'enabled' => true
			];
		}

		// Child categories
		$child_categories = $this->model->getOcChildCategories($category_id);

		foreach ($child_categories as $child_category) {
			$data['filter_category_id'] = $child_category['category_id'];
			$total_products = $this->model->getTotalProducts($data);

			$block_elements['child'][] = [
				'name' => 'category_id',
				'label' => htmlspecialchars_decode($child_category['name']) . ' <span>(' . $total_products . ')</span>',
				'href' => $this->url->link('product/category', 'path=' . $url_data['path'] . '_' . $child_category['category_id'] . $multimerch_query_string, 'SSL'),
				'value' => $child_category['category_id'],
				'enabled' => !empty($total_products)
			];
		}

		$block_data = [
			'title' => $this->language->get('text_categories'),
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_oc_category', $block_data);
	}
}
