<?php

class ControllerMultimerchProductFilterBlockOcAttribute extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $oc_attribute_id = 0)
	{
		$data = $this->helper->getProductsRequestData($url_data);

		$block_elements = [];

		$oc_attributes = $this->model->getOcAttributes($product_ids, $oc_attribute_id);

		foreach ($oc_attributes as $attribute_id => $attribute) {
			$block_elements[$attribute_id] = [
				'id' => "block_oc_attribute_$attribute_id",
				'data-id' => $attribute_id,
				'title' => htmlspecialchars_decode($attribute['name']),
				'values' => []
			];

			foreach ($attribute['text_values'] as $name) {
				$filter_oc_attribute = !empty($data['filter_oc_attribute']) ? $data['filter_oc_attribute'] : [];

				$name_with_replaced_delimiter = str_replace(',', '%d%', $name);

				if (!empty($filter_oc_attribute[$attribute_id])) {
					$filter_oc_attribute[$attribute_id] .= "," . $name_with_replaced_delimiter;
				} else {
					$filter_oc_attribute[$attribute_id] = $name_with_replaced_delimiter;
				}

				$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_oc_attribute' => $filter_oc_attribute]));

				$block_elements[$attribute_id]['values'][] = [
					'name' => "oc_attribute[$attribute_id][]",
					'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
					'value' => urlencode(htmlspecialchars_decode($name_with_replaced_delimiter)),
					'checked' => isset($data['filter_oc_attribute'][$attribute_id]) && in_array($name_with_replaced_delimiter, explode(',', $data['filter_oc_attribute'][$attribute_id])),
					'enabled' => !empty($total_products)
				];
			}
		}

		if (empty($block_elements))
			return false;

		$block_data = [
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_oc_attribute', $block_data);
	}
}
