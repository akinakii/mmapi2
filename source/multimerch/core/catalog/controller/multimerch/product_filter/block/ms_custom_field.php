<?php

class ControllerMultimerchProductFilterBlockMsCustomField extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $ms_custom_field_id = 0)
	{
		if (!$this->config->get('msconf_ms_custom_field_enabled')) {
			return false;
		}

		$data = $this->helper->getProductsRequestData($url_data);

		$block_elements = [];

		$ms_custom_fields = $this->model->getMsCustomFields($product_ids, $ms_custom_field_id);

		foreach ($ms_custom_fields as $custom_field_id => $custom_field) {
			$block_elements[$custom_field_id] = [
				'id' => "block_ms_custom_field_$custom_field_id",
				'data-id' => $custom_field_id,
				'title' => $custom_field['name'],
				'values' => []
			];

			if (in_array($custom_field['type'], ['select', 'radio', 'checkbox'])) {
				// For selects, radio and checkbox values

				foreach ($custom_field['id_values'] as $id => $name) {
					$filter_ms_custom_field = !empty($data['filter_ms_custom_field']) ? $data['filter_ms_custom_field'] : [];

					if (!empty($filter_ms_custom_field[$custom_field_id])) {
						$filter_ms_custom_field[$custom_field_id] .= ",{$id}";
					} else {
						$filter_ms_custom_field[$custom_field_id] = $id;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_ms_custom_field' => $filter_ms_custom_field]));

					$block_elements[$custom_field_id]['values'][] = [
						'name' => "ms_custom_field[$custom_field_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => $id,
						'checked' => isset($data['filter_ms_custom_field'][$custom_field_id]) && in_array($id, explode(',', $data['filter_ms_custom_field'][$custom_field_id])),
						'enabled' => !empty($total_products)
					];
				}
			} else {
				// For text values

				foreach ($custom_field['text_values'] as $name) {
					$filter_ms_custom_field = !empty($data['filter_ms_custom_field']) ? $data['filter_ms_custom_field'] : [];

					$name_with_replaced_delimiter = str_replace(',', '%d%', $name);

					if (!empty($filter_ms_custom_field[$custom_field_id])) {
						$filter_ms_custom_field[$custom_field_id] .= "," . $name_with_replaced_delimiter;
					} else {
						$filter_ms_custom_field[$custom_field_id] = $name_with_replaced_delimiter;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_ms_custom_field' => $filter_ms_custom_field]));

					$block_elements[$custom_field_id]['values'][] = [
						'name' => "ms_custom_field[$custom_field_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => urlencode(htmlspecialchars_decode($name_with_replaced_delimiter)),
						'checked' => isset($data['filter_ms_custom_field'][$custom_field_id]) && in_array($name_with_replaced_delimiter, explode(',', $data['filter_ms_custom_field'][$custom_field_id])),
						'enabled' => !empty($total_products)
					];
				}
			}
		}

		if (empty($block_elements))
			return false;

		$block_data = [
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_ms_custom_field', $block_data);
	}
}
