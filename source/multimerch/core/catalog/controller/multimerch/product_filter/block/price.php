<?php

class ControllerMultimerchProductFilterBlockPrice extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids)
	{
		$data = $this->helper->getProductsRequestData($url_data);
		$total_products = count($product_ids);

		if (!$total_products)
			return false;

		$block_data = [
			'title' => $this->language->get('text_price'),
			'price_range' => !empty($data['price']) ? $data['price'] : ['min' => '', 'max' => ''],
			'currency' => [
				'symbol_left' => $this->currency->getSymbolLeft($this->session->data['currency']),
				'symbol_right' => $this->currency->getSymbolRight($this->session->data['currency'])
			],
			'language' => $this->load->language('multimerch/product_filter')
		];

		return $this->load->view('multimerch/product_filter/block_price', $block_data);
	}
}
