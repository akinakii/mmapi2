<?php

class ControllerMultimerchProductFilterBlockOcOption extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $oc_option_id = 0)
	{
		$data = $this->helper->getProductsRequestData($url_data);

		$block_elements = [];

		$oc_options = $this->model->getOcOptions($product_ids, $oc_option_id);

		foreach ($oc_options as $option_id => $option) {
			$block_elements[$option_id] = [
				'id' => "block_oc_option_$option_id",
				'data-id' => $option_id,
				'title' => htmlspecialchars_decode($option['name']),
				'values' => []
			];

			if (in_array($option['type'], ['select', 'radio', 'checkbox'])) {
				// For selects, radio and checkbox values

				foreach ($option['id_values'] as $id => $name) {
					$filter_oc_option = !empty($data['filter_oc_option']) ? $data['filter_oc_option'] : [];

					if (!empty($filter_oc_option[$option_id])) {
						$filter_oc_option[$option_id] .= ",{$id}";
					} else {
						$filter_oc_option[$option_id] = $id;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_oc_option' => $filter_oc_option]));

					$block_elements[$option_id]['values'][] = [
						'name' => "oc_option[$option_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => $id,
						'checked' => isset($data['filter_oc_option'][$option_id]) && in_array($id, explode(',', $data['filter_oc_option'][$option_id])),
						'enabled' => !empty($total_products)
					];
				}
			} else {
				// For text values

				foreach ($option['text_values'] as $name) {
					$filter_oc_option = !empty($data['filter_oc_option']) ? $data['filter_oc_option'] : [];

					$name_with_replaced_delimiter = str_replace(',', '%d%', $name);

					if (!empty($filter_oc_option[$option_id])) {
						$filter_oc_option[$option_id] .= "," . $name_with_replaced_delimiter;
					} else {
						$filter_oc_option[$option_id] = $name_with_replaced_delimiter;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_oc_option' => $filter_oc_option]));

					$block_elements[$option_id]['values'][] = [
						'name' => "oc_option[$option_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => urlencode(htmlspecialchars_decode($name_with_replaced_delimiter)),
						'checked' => isset($data['filter_oc_option'][$option_id]) && in_array($name_with_replaced_delimiter, explode(',', $data['filter_oc_option'][$option_id])),
						'enabled' => !empty($total_products)
					];
				}
			}
		}

		if (empty($block_elements))
			return false;

		$block_data = [
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_oc_option', $block_data);
	}
}
