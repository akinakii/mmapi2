<?php

class ControllerMultimerchProductFilterBlockMsfAttribute extends Controller
{
	protected $helper;
	protected $model;

	public function __construct($registry)
	{
		parent::__construct($registry);

		// Load helper
		$this->helper = $this->load->controller('multimerch/product_filter/helper/instance');

		// Load model
		$this->model = $this->MsLoader->MsProductFilter;
	}

	public function instance()
	{
		return $this;
	}

	public function render($url_data, $product_ids, $msf_attribute_id = 0)
	{
		$data = $this->helper->getProductsRequestData($url_data);

		$block_elements = [];

		$msf_attributes = $this->model->getMsfAttributes($product_ids, $msf_attribute_id);

		foreach ($msf_attributes as $msf_attribute_id => $msf_attribute) {
			$block_elements[$msf_attribute_id] = [
				'id' => "block_msf_attribute_$msf_attribute_id",
				'data-id' => $msf_attribute_id,
				'title' => $msf_attribute['name'],
				'values' => []
			];

			if (in_array($msf_attribute['type'], ['select', 'radio', 'checkbox'])) {
				// For selects, radio and checkbox values

				foreach ($msf_attribute['id_values'] as $id => $name) {
					$filter_msf_attribute = !empty($data['filter_msf_attribute']) ? $data['filter_msf_attribute'] : [];

					if (!empty($filter_msf_attribute[$msf_attribute_id])) {
						$filter_msf_attribute[$msf_attribute_id] .= ",{$id}";
					} else {
						$filter_msf_attribute[$msf_attribute_id] = $id;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_msf_attribute' => $filter_msf_attribute]));

					$block_elements[$msf_attribute_id]['values'][] = [
						'name' => "msf_attribute[$msf_attribute_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => $id,
						'checked' => isset($data['filter_msf_attribute'][$msf_attribute_id]) && in_array($id, explode(',', $data['filter_msf_attribute'][$msf_attribute_id])),
						'enabled' => !empty($total_products)
					];
				}
			} else {
				// For text values

				foreach ($msf_attribute['text_values'] as $name) {
					$name_with_replaced_delimiter = str_replace(',', '%d%', $name);

					if (!empty($filter_msf_attribute[$msf_attribute_id])) {
						$filter_msf_attribute[$msf_attribute_id] .= "," . $name_with_replaced_delimiter;
					} else {
						$filter_msf_attribute[$msf_attribute_id] = $name_with_replaced_delimiter;
					}

					$total_products = $this->model->getTotalProducts(array_merge($data, ['filter_msf_attribute' => $filter_msf_attribute]));

					$block_elements[$msf_attribute_id]['values'][] = [
						'name' => "msf_attribute[$msf_attribute_id][]",
						'label' => htmlspecialchars_decode($name) . ' <span>(' . $total_products . ')</span>',
						'value' => urlencode(htmlspecialchars_decode($name_with_replaced_delimiter)),
						'checked' => isset($data['filter_msf_attribute'][$msf_attribute_id]) && in_array($name_with_replaced_delimiter, explode(',', $data['filter_msf_attribute'][$msf_attribute_id])),
						'enabled' => !empty($total_products)
					];
				}
			}
		}

		if (empty($block_elements))
			return false;

		$block_data = [
			'elements' => $block_elements
		];

		return $this->load->view('multimerch/product_filter/block_msf_attribute', $block_data);
	}
}
