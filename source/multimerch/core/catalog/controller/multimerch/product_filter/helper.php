<?php

class ControllerMultimerchProductFilterHelper extends Controller
{
	private $url_data = null;

	private $query_params = [
		'common' => ['sort', 'order', 'limit', 'page'],
		'multimerch' => ['price', 'ms_custom_field', 'oc_option', 'oc_attribute', 'manufacturer', 'msf_attribute']
	];

	private $values_delimiter = ',';

	public function instance()
	{
		return $this;
	}

	public function getCommonQueryParams()
	{
		return $this->query_params['common'];
	}

	public function getMultimerchQueryParams()
	{
		return $this->query_params['multimerch'];
	}

	public function getValuesDelimiter()
	{
		return $this->values_delimiter;
	}

	public function fetchGetRequest($request_data)
	{
		if (is_null($this->url_data)) {
			// OpenCart category
			$path = !empty($request_data['path']) ? explode('_', (string)$request_data['path']) : [0];
			$category_id = end($path);

			// MultiMerch category
			$ms_category_id = isset($request_data['ms_category_id']) ? $request_data['ms_category_id'] : 0;

			// MultiMerch seller
			$seller_id = isset($request_data['seller_id']) ? $request_data['seller_id'] : 0;

			// Price
			$price = [
				'min' => isset($request_data['price']['min']) ? $request_data['price']['min'] : '',
				'max' => isset($request_data['price']['max']) ? $request_data['price']['max'] : ''
			];

			// MultiMerch custom fields
			$ms_custom_field = isset($request_data['ms_custom_field']) ? $request_data['ms_custom_field'] : '';

			// MultiMerch product attributes
			$msf_attribute = isset($request_data['msf_attribute']) ? $request_data['msf_attribute'] : '';

			// OpenCart option
			$oc_option = isset($request_data['oc_option']) ? $request_data['oc_option'] : '';

			// OpenCart attribute
			$oc_attribute = isset($request_data['oc_attribute']) ? $request_data['oc_attribute'] : '';

			// Manufacturer
			$manufacturer = isset($request_data['manufacturer']) ? $request_data['manufacturer'] : '';

			// Sorts
			$sort = isset($request_data['sort']) ? (string)$request_data['sort'] : 'p.sort_order';

			// Order way
			$order = isset($request_data['order']) ? (string)$request_data['order'] : 'ASC';

			// Page
			$page = isset($request_data['page']) ? (int)$request_data['page'] : 1;

			// Limit per page
			$limit = isset($request_data['limit']) ? (int)$request_data['limit'] : $this->config->get($this->config->get('config_theme') . '_product_limit');

			$this->url_data = [
				'category_id'			=> $category_id,
				'path'					=> !empty($request_data['path']) ? $request_data['path'] : '',
				'seller_id'				=> $seller_id,
				'ms_category_id'		=> $ms_category_id,
				'ms_custom_field'		=> $ms_custom_field,
				'msf_attribute'			=> $msf_attribute,
				'oc_option'				=> $oc_option,
				'oc_attribute'			=> $oc_attribute,
				'manufacturer'			=> $manufacturer,
				'price'					=> $price,
				'sort'					=> $sort,
				'order'					=> $order,
				'page'					=> $page,
				'limit'					=> $limit
			];
		}

		return $this->url_data;
	}

	/**
	 * Generates url query for common url parameters based on selected filters.
	 *
	 * @param $url_data
	 * @return array
	 */
	public function getCommonQueryString($url_data)
	{
		return $this->getQueryString($url_data, $this->query_params['common']);
	}

	/**
	 * Generates url query for MultiMerch url parameters based on selected filters.
	 *
	 * @param $url_data
	 * @return array
	 */
	public function getMultimerchQueryString($url_data)
	{
		return $this->getQueryString($url_data, $this->query_params['multimerch']);
	}

	private function getQueryString($url_data, $params)
	{
		$url_query = '';

		foreach ($params as $p) {
			if (!empty($url_data[$p])) {
				foreach ((array)$url_data[$p] as $key => $values) {
					if (!empty($values)) {
						$url_query .= ($key ? ("&{$p}[$key]=" . $values) : ("&{$p}=" . $values));
					}
				}
			}
		}

		return $url_query;
	}

	public function deleteQueryParams($url, $params) {
		$pattern = '/(&|\?)(' . implode('|', (array)$params) . ')=(.+?(?=&|$))/';
		$new_url = preg_replace($pattern,'', $url);
		return $new_url;
	}

	public function getProductsRequestData($url_data)
	{
		return [
			'filter_seller_id'			=> !empty($url_data['seller_id']) ? $url_data['seller_id'] : false,
			'filter_sub_category'		=> $this->config->get('msconf_enforce_childmost_categories') ? true : false,
			'filter_category_id'		=> $url_data['category_id'],
			'filter_ms_category_id'		=> $url_data['ms_category_id'],
			'filter_ms_sub_category'	=> true,
			'filter_ms_custom_field'	=> $url_data['ms_custom_field'],
			'filter_msf_attribute'		=> $url_data['msf_attribute'],
			'filter_oc_option'			=> $url_data['oc_option'],
			'filter_oc_attribute'		=> $url_data['oc_attribute'],
			'filter_manufacturer'		=> $url_data['manufacturer'],
			'price'						=> $url_data['price'],
			'sort'						=> $url_data['sort'],
			'order'						=> $url_data['order'],
			'start'						=> ($url_data['page'] - 1) * $url_data['limit'],
			'limit'						=> $url_data['limit']
		];
	}
}
