<?php

class ControllerMultimerchCommon extends Controller
{
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->registry = $registry;

		if (! is_array($this->data)) {
			$this->data = [];
		}

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
	}

	public function getCustomerMenu()
	{
		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);

		$data['ms_seller_created'] = $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId());

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/common/customer_account_menu');
		$this->response->setOutput($this->load->view($template, array_merge($data, $children, $this->data)));
	}
}