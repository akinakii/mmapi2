<?php

class ControllerMultimerchProductReview extends Controller
{
	private $data;

	public function __construct($registry) {
		parent::__construct($registry);

		$this->load->model('account/customer');
		$this->load->model('tool/image');

		$this->data = array_merge(!empty($this->data) ? $this->data : array(), $this->load->language('multiseller/multiseller'));
	}

	public function index($args = []) {
		$product_id = !empty($args['product_id']) ? (int)$args['product_id'] : 0;
		$this->data['reviews'] = $rating_stats = array();
		$this->data['total_reviews'] = $this->data['rating_stats'] = $this->data['avg_rating'] = 0;

		for($i = 1; $i <= 5; $i++) {
			$rating_stats[$i] = ['votes' => 0, 'percentage' => 0];
		}

		$sum_rating = 0;

		$reviews = $this->MsLoader->MsReview->getReviews(['product_id' => $product_id]);
		$total_reviews = isset($reviews[0]) ? $reviews[0]['total_rows'] : 0;

		foreach ($reviews as &$review) {
			$sum_rating += $review['rating'];

			$rating_stats[$review['rating']]['votes'] += 1;

			$review['author'] = $this->model_account_customer->getCustomer($review['author_id']);
			$review['date_created'] = date($this->language->get('date_format_short'), strtotime($review['date_created']));

			foreach ($review['attachments'] as &$attachment) {
				$attachment['fullsize'] = $this->model_tool_image->resize($attachment['attachment'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
				$attachment['thumb'] = $this->model_tool_image->resize($attachment['attachment'], 70, 70);
			}

			$comments = $this->MsLoader->MsReview->getReviewComments($review['review_id']);
			$review['comments'] = $comments;
			$review['total_comments'] = isset($comments[0]) ? $comments[0]['total_rows'] : 0;
		}

		$avg_rating = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;

		foreach ($rating_stats as &$rating) {
			$rating['percentage'] = $total_reviews > 0 ? round($rating['votes'] / $total_reviews * 100, 1) : 0;
		}
		krsort($rating_stats);

		$this->data['reviews'] = $reviews;
		$this->data['total_reviews'] = $total_reviews;
		$this->data['rating_stats'] = $rating_stats;
		$this->data['avg_rating'] = $avg_rating;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('product/mm_review', []);
		return [$total_reviews, $avg_rating, $this->load->view($template, array_merge($this->data, $children))];
	}
}
