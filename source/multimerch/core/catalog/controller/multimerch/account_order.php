<?php
class ControllerMultimerchAccountOrder extends Controller
{
	private $data = array();
	const REFERRER_CUSTOMER = 1;
	const REFERRER_SELLER = 2;

	public function __construct($registry) {
		parent::__construct($registry);

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->data = array_merge($this->load->language('multiseller/multiseller'), $this->load->language('account/order'));
	}

	public function index() {
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->session->data['error_warning'])) {
			$this->data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->load->model('account/order');
		$this->load->model('tool/image');
		$this->load->model('catalog/product');
		
		$this->data['order_total'] = $this->model_account_order->getTotalOrders();
		$orders = $this->model_account_order->getOrders();
		$results = array(
			'orders' => array(),
			'products' => array(),
		);

		foreach($orders as $k => $order) {
			$orders[$k]['total'] = $this->currency->format($orders[$k]['total'], $order['currency_code'], $order['currency_value']);
			$orders[$k]['date_added'] = date($this->language->get('date_format_short'), strtotime($orders[$k]['date_added']));

			$products = $this->model_account_order->getOrderProducts($order['order_id']);

			foreach ($products as $key => $product) {
				$products[$key]['options'] = $this->model_account_order->getOrderOptions($product['order_id'], $product['order_product_id']);
				$products[$key]['seller'] = $this->MsLoader->MsSeller->getSeller($this->MsLoader->MsProduct->getSellerId($product['product_id']));
				$products[$key]['price'] = $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order['currency_code'], $order['currency_value']);
				$products[$key]['product'] = $this->model_catalog_product->getProduct($product['product_id']);
				$products[$key]['product']['image'] = $this->model_tool_image->resize($products[$key]['product']['image'] ? $products[$key]['product']['image'] : 'no_image.png', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
				$products[$key]['review'] = $this->MsLoader->MsReview->getReviews(array('order_product_id' => $product['order_product_id'], 'author_id' => $this->customer->getId()));
				$products[$key]['suborder_status'] = $this->MsLoader->MsSuborder->getSuborderStatus(array(
					'order_id' => $product['order_id'],
					'seller_id' => $this->MsLoader->MsProduct->getSellerId($product['product_id'])
				));
			}

			$results['products'][$order['order_id']] = $products;
		}

		$results['orders'] = $orders;

		$this->data['orders'] = $results;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/order');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function info() {

		$this->load->language('account/order');
		$order_id = $this->request->get['order_id'] ?? 0;

		if ($this->isMsInstalled()) {
			MsLoader::getInstance()->getRegistry()->get('load')->model('localisation/order_status');
			$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/multimerch/account/order_info_reworked.css');
			$this->document->setTitle($this->language->get('text_order'));

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('ms_order_column_order_id') . $order_id,
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, true)
			);

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_no_results'] = $this->language->get('text_no_results');

			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');

			$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');
			$data['button_continue'] = $this->language->get('button_continue');

			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_status_id'] = $order_info['order_status_id'];
			$data['link_print_invoice'] = $this->url->link('account/order/printInvoice', 'order_id=' . $order_id, 'SSL');
			$data['order_id'] = $this->request->get['order_id'];
			$data['date_added'] = $order_info['date_added'];

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();
			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
			$data['mm_shipping_flag'] = 0;
			$this->load->language('multiseller/multiseller');

			$cs_products = [];

			foreach ($products as $product) {
				$product_shipping_cost = $product['shipping_cost'] ?? 0;

				if ($product_shipping_cost == 0) {
					if (! isset($cs_products[$product['seller_id']])) {
						$cs_products[$product['seller_id']] = 0;
					}

					$cs_products[$product['seller_id']] += 1;
				}
			}

			foreach ($products as $product) {
				if (MsLoader::getInstance()->MsHelper->isInstalled()) {
					$seller_id = MsLoader::getInstance()->MsProduct->getSellerId($product['product_id']);

					$suborder_status_id = MsLoader::getInstance()->MsSuborder->getSuborderStatus(array(
						'order_id' => $order_id,
						'seller_id' => $seller_id
					));

					// Get product shipping cost
					$product_shipping_cost = MsLoader::getInstance()->MsOrderData->getOrderProductShippingCost($product['product_id'], $product['order_id'], $product['order_product_id']);
					if ($product_shipping_cost > 0) {
						$data['mm_shipping_flag'] += 1;
					}

					$shipping_cost_formatted = $this->currency->format($product_shipping_cost, $order_info['currency_code'], $order_info['currency_value']);

					// Get product shipping method
					$ms_order_product_data = $this->MsLoader->MsOrderData->getOrderData(array(
						'product_id' => $product['product_id'],
						'order_id' => $product['order_id'],
						'order_product_id' => $product['order_product_id'],
						'single' => 1
					));

					$shipping_method = !empty($ms_order_product_data['shipping_method']) ? " <i>(" . sprintf($this->language->get('mm_account_order_shipping_via'), $ms_order_product_data['shipping_method']) . ")</i>" : '';
				} else {
					$suborder_status_id = 0;
					$product_shipping_cost = 0;
					$shipping_cost_formatted = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
					$shipping_method = '';
				}

				$ms_order_product_data = FALSE;

				if ($this->isMsInstalled()) {
					$ms_order_product_data = $this->MsLoader->MsOrderData->getOrderData(array(
						'product_id' => $product['product_id'],
						'order_id' => $product['order_id'],
						'order_product_id' => $product['order_product_id'],
						'single' => 1
					));
					$this->load->language('multiseller/multiseller');
				}

				$msf_variation_data = [];
				$msf_variations = $this->MsLoader->MsOrderData->getOrderProductMsfVariations($order_id, $product['order_product_id']);

				foreach ($msf_variations as $msf_variation) {
					$msf_variation_data[] = [
						'name' => $msf_variation['name'],
						'value' => $msf_variation['value']
					];
				}

				$option_data = array();
				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
				} else {
					$reorder = '';
				}

				$data['products'][] = array(
					'product_id' => $product['product_id'],
					'suborder_status_id' => $suborder_status_id,
					'order_status_id' => $order_info['order_status_id'],
					'suborder_status_text' => $this->MsLoader->MsSuborderStatus->getSubStatusName(array('order_status_id' => $suborder_status_id)),
					'order_id' => $order_id,
					'seller_id' => $seller_id ?? 0,
					'total_num' => $product['total'],
					'shipping_cost' => $product_shipping_cost,
					'shipping_cost_formatted' => $shipping_cost_formatted,
					'image'    => $this->getImage($product['product_id']),
					'name'     => $product['name'],
					'model'    => $product['model'],
					'msf_variation' => $msf_variation_data,
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0) + $product_shipping_cost, $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'shipping_method' => !empty($ms_order_product_data['shipping_method']) ? " <i>(" . sprintf($this->language->get('mm_account_order_shipping_via'), $ms_order_product_data['shipping_method']) . ")</i>" : $shipping_method,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
				);
			}

			foreach ($data['products'] as &$product) {
				// Suborder shipping cost
				$suborder = $this->MsLoader->MsSuborder->getSuborders([
					'seller_id' => $product['seller_id'],
					'order_id' => $product['order_id'],
					'single' => true,
					'image' => $this->getImage($product['product_id'])
				]);

				if (!empty($suborder)) {
					$suborders_flag = true;
					$suborder_shipping = $this->MsLoader->MsShipping->getSuborderShippingData($suborder['suborder_id']);

					if (!empty($suborder_shipping) && !$product['shipping_cost'] && isset($cs_products[$product['seller_id']]) && $cs_products[$product['seller_id']] > 0) {
						$data['mm_shipping_flag'] += 1;
						$cost = $suborder_shipping['cost'] / $cs_products[$product['seller_id']];
						$product['shipping_cost'] = $cost;
						$product['shipping_cost_formatted'] = $this->currency->format($cost, $order_info['currency_code'], $order_info['currency_value']);
						$product['shipping_method'] = !empty($suborder_shipping['shipping_method_name']) ? " <i>(" . sprintf($this->language->get('mm_account_order_shipping_via'), $suborder_shipping['shipping_method_name']) . ")</i>" : "";
						$product['total'] = $this->currency->format($product['total_num'] + $cost, $order_info['currency_code'], $order_info['currency_value']);
					}
				}
			}

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			$data['comment'] = nl2br($order_info['comment']);

			// History
			$data['histories'] = array();

			if ($this->isMsInstalled()) {
				$this->document->addScript('catalog/view/javascript/multimerch/account-message.js');
				$this->MsLoader->MsHelper->addStyle('multimerch_messaging');
			}

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$data['suborders_flag'] = $suborders_flag ?? false;

			$this->order_conversation(array('referrer' => self::REFERRER_CUSTOMER));
			list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/order_info');
			$data = array_merge($this->data, $children, $data);
			$data['order_info_block'] = $this->load->view($template, $data);

			$this->response->setOutput($this->load->view('multimerch/account/order_info_reworked', $data));
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $order_id, true)
			);

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function printInvoice() {
		$this->MsLoader->MsPrinter->printOrderInvoice($this->request->get['order_id']);
	}

	protected function order_conversation($cond = array()){
		$this->data['seller_histories'] = array();

		$this->data['active_tab'] = isset($this->request->get['tab']) ? $this->request->get['tab'] : false;
		$this->data['order_id'] = isset($this->request->get['order_id']) ? (int)$this->request->get['order_id'] : 0;

		// Customer or seller
		$referrer = isset($cond['referrer']) ? (int)$cond['referrer'] : self::REFERRER_CUSTOMER;

		// Conditions for suborders
		$fetch_conditions = array('order_id' => $this->data['order_id']);

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->data['order_id']);

		// if customer is not customer - view only related suborder
		if ($this->customer->getId() != $order_info['customer_id']){
			$fetch_conditions['seller_id'] = $this->customer->getId();
		}

		$suborders = MsLoader::getInstance()->MsSuborder->getSuborders($fetch_conditions);

		foreach ($suborders as $key => $sub) {
			if (!$this->data['active_tab'] && $key == 0){
				$this->data['active_tab'] = $sub['suborder_id'];
			}

			$suborder_histories = array();

			$seller = MsLoader::getInstance()->MsSeller->getSeller($sub['seller_id']);

			$this->load->model('account/customer');
			$customer = $this->model_account_customer->getCustomer($order_info['customer_id']);
			if(empty($customer)) $customer = false;

			if($referrer == self::REFERRER_CUSTOMER) {
				// fetch histories
				$histories = MsLoader::getInstance()->MsSuborder->getSuborderHistory(array(
					'suborder_id' => $sub['suborder_id']
				));

				// format histories
				foreach ($histories as $h) {
					$suborder_histories[] = array(
						'date_added' => date($this->language->get('date_format_short'), strtotime($h['date_added'])),
						'status' => $this->MsLoader->MsSuborderStatus->getSubStatusName(array('order_status_id' => $h['order_status_id'])),
						'comment' => $h['comment']
					);
				}
			}

			$order_messages = array();

			$participant = ($referrer == self::REFERRER_CUSTOMER) ? (!empty($seller['ms.nickname']) ? $seller['ms.nickname'] : $this->language->get('ms_seller_deleted')) : (!empty($customer) ? $customer['firstname'] . ' ' . $customer['lastname'] : $order_info['firstname'] . ' ' . $order_info['lastname'] . ' ' . $this->language->get('ms_conversation_customer_deleted'));
			$conversation_title = $this->language->get('ms_account_conversations_with') . ' ' . $participant;

			if ($this->config->get('mmess_conf_enable') == 1){
				$conversation = $this->MsLoader->MsConversation->getOrderConversation((int)$this->request->get['order_id'], (int)$sub['suborder_id']);

				if (isset($conversation['conversation_id'])){
					$order_messages = $this->MsLoader->MsMessage->getMessages(
						array(
							'conversation_id' => (int)$conversation['conversation_id']
						),
						array(
							'order_by'  => 'date_created',
							'order_way' => 'ASC',
						)
					);

					foreach ($order_messages as $omk => $m) {
						$sender_type_id = $m['from_admin'] ? MsConversation::SENDER_TYPE_ADMIN : ($m['from'] == $sub['seller_id'] ? MsConversation::SENDER_TYPE_SELLER : MsConversation::SENDER_TYPE_CUSTOMER);
						$sender = $m['from_admin'] ? $m['user_sender'] : ($m['seller_sender'] && $sender_type_id == MsConversation::SENDER_TYPE_SELLER ? $m['seller_sender'] : $m['customer_sender']);

						$order_messages[$omk] = array_merge(
							$m,
							array(
								'date_created' => date($this->language->get('datetime_format'), strtotime($m['date_created'])),
								'sender_type_id' => $sender_type_id,
								'sender' => ((utf8_strlen($sender) > 20) ? utf8_substr($sender, 0, 20) . '..' : $sender) . ($m['from_admin'] ? ' (' . $this->language->get('ms_account_conversations_sender_type_' . MsConversation::SENDER_TYPE_ADMIN) . ')': '')
							)
						);
					}
				}
			}

			// assign histories
			$this->data['seller_histories'][$sub['seller_id']] = array(
				'seller_id' => $sub['seller_id'],
				'seller_nickname' => $this->MsLoader->MsSeller->getSellerNickname($sub['seller_id']),
				'suborder_id' => $sub['suborder_id'],
				'suborder_status' => $this->MsLoader->MsSuborder->getSuborderStatus(array(
					'order_id' => $this->data['order_id'],
					'seller_id' => $sub['seller_id'],
				)),
				'conversation_title' => $conversation_title,
				'order_messages' => $order_messages,
				'participant' => $participant,
				'entries' => $suborder_histories,
				'totals' => $this->MsLoader->MsSuborder->getSuborderTotals($this->data['order_id'], $sub['seller_id']
				));
		}
	}

	public function customerOrderConversation() {
		$this->order_conversation(array('referrer' => self::REFERRER_CUSTOMER));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/order_info');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function sellerOrderConversation() {
		$this->order_conversation(array('referrer' => self::REFERRER_SELLER));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-info-conversation');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxUploadAttachment() {
		$this->load->language('tool/upload');
		$this->load->language('multiseller/multiseller');

		$json = array();

		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
			// Sanitize the filename
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));

			// Validate the filename length
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
				$json['error'] = $this->language->get('error_filename');
			}

			// Validate file extension
			$json['error'] = $this->MsLoader->MsFile->checkFile($this->request->files['file'], $this->config->get('msconf_msg_allowed_file_types'));

			// Check to see if any PHP files are trying to be uploaded
			$content = file_get_contents($this->request->files['file']['tmp_name']);

			if (preg_match('/\<\?php/i', $content)) {
				$json['error'] = $this->language->get('error_filetype');
			}

			// Return any upload error
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (empty($json['error'])) {
			unset($json['error']);

			// Hide the uploaded file name so people can not link to it directly.
			$file = $filename . '.' . token(32);

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_UPLOAD . $file);

			$this->load->model('tool/upload');
			$json['code'] = $this->model_tool_upload->addUpload($filename, $file);
			$json['filename'] = $filename;
			$json['success'] = $this->language->get('text_upload');
		}

		return $this->response->setOutput(json_encode($json));
	}

	protected function getImage($productId) {
		$this->load->model('tool/image');
		$thumb = $this->MsLoader->MsProduct->getProductThumbnail($productId);

		return $this->model_tool_image->resize(!empty($thumb['image']) ? $thumb['image'] : 'no_image.png', 40, 40);
	}

	protected function isMsInstalled() {
		return MsLoader::getInstance()->MsHelper->isInstalled();
	}
}