<?php

use MultiMerch\Core\Shipping\Shipping as Shipping;

class ControllerMultimerchProductShipping extends Controller
{
	public function index() {
		$data = $this->load->language('multiseller/multiseller');

		$product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : 0;
		$product = $this->MsLoader->MsProduct->getProduct($product_id);

		$seller_id = $this->MsLoader->MsProduct->getSellerId($product_id);
		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);

		$data['shipping'] = [];

		// Shipping from
		if (isset($seller_settings['slr_shipping_from_country_id'])) {
			$data['shipping']['from_country'] = $this->MsLoader->MsShipping->getCountries(['country_id' => $seller_settings['slr_shipping_from_country_id']]);
		}

		// Processing days
		if (isset($seller_settings['slr_shipping_processing_days'])) {
			$data['shipping']['processing_days'] = $seller_settings['slr_shipping_processing_days'];
		}

		if (!empty($product['shipping'])) {
			$data['product_is_digital'] = false;

			$shipping_type = $this->config->get('msconf_shipping_per_product_override_allow') && $this->MsLoader->MsShipping->getPerProductRules($seller_id, $product_id) ? Shipping::TYPE_PER_PRODUCT : $this->config->get('msconf_shipping_type');

			$data['shipping']['fields'] = $this->load->controller('multimerch/shipping/getShippingFields', $shipping_type);
			$data['shipping']['rules'] = $this->load->controller('multimerch/shipping/getShippingRules', ['shipping_type' => $shipping_type, 'seller_id' => $seller_id, 'product_id' => $product_id]);
		} else {
			// Product is digital
			$data['product_is_digital'] = true;

			$data['shipping']['fields'] = $data['shipping']['rules'] = null;
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('product/mm_shipping', []);
		return $this->load->view($template, array_merge($data, $children));
	}
}
