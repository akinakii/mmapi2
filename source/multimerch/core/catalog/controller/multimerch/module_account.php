<?php
class ControllerMultimerchModuleAccount extends Controller {
	public function index() {
		$data = array_merge($this->load->language('multiseller/multiseller'), $this->load->language('account/login'), $this->load->language('module/account'), $this->load->language('extension/module/account'));

		$data['menus'] = [];

		if (!$this->customer->isLogged()) {
			$data['menus'][] = [
				'title' => false,
				'items' => [
					'login' => [
						'name' => $this->language->get('text_login'),
						'link' => $this->url->link('account/login', '', 'SSL'),
						'items' => []
					],
					'register_customer' => [
						'name' => $this->language->get('text_register'),
						'link' => $this->url->link('account/register', '', 'SSL'),
						'items' => []
					],
					'forgotten' => [
						'name' => $this->language->get('text_forgotten'),
						'link' => $this->url->link('account/forgotten', '', 'SSL'),
						'items' => []
					]
				]
			];
		} else {
			$is_seller = $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId());
			$ms_current_user_type = strncmp($this->request->get['route'], 'seller/', 7) === 0 || ('account/msconversation' === $this->request->get['route'] && $is_seller) ? 'seller' : 'customer';

			$data['menus']['common'] = [
				'title' => false,
				'items' => []
			];

			if ('customer' === $ms_current_user_type) {
				// Customer navigation

				/*if (!$is_seller) {
					$data['menus']['common']['items']['register_seller'] = [
						'name' => $this->language->get('ms_account_sellerinfo_new'),
						'link' => $this->config->get('msconf_seller_landing_page_id') ? $this->url->link('information/information', 'information_id=' . $this->config->get('msconf_seller_landing_page_id'), 'SSL') : $this->url->link('account/register-seller', '', 'SSL'),
						'items' => []
					];
				}*/

				$data['menus']['customer'] = [
					'title' => $this->language->get('ms_account_customer'),
					'items' => [
						'overview' => [
							'name' => $this->language->get('ms_account_overview'),
							'link' => $this->url->link('account/account', '', 'SSL'),
							'items' => []
						],
						'address' => [
							'name' => $this->language->get('text_address'),
							'link' => $this->url->link('account/address', '', 'SSL'),
							'items' => []
						],
						'wishlist' => [
							'name' => $this->language->get('text_wishlist'),
							'link' => $this->url->link($this->config->get('msconf_wishlist_enabled') ? 'account/mswishlist' : 'account/wishlist', '', 'SSL'),
							'items' => []
						],
						'order' => [
							'name' => $this->language->get('text_order'),
							'link' => $this->url->link('account/order', '', 'SSL'),
							'items' => []
						],
						'ms_conversation' => !$this->config->get('mmess_conf_enable') ? false : [
							'name' => $this->language->get('ms_account_messages'),
							'link' => $this->url->link('account/msconversation', '', 'SSL'),
							'items' => []
						],
						'download' => [
							'name' => $this->language->get('text_download'),
							'link' => $this->url->link('account/download', '', 'SSL'),
							'items' => []
						],
						'recurring' => [
							'name' => $this->language->get('text_recurring'),
							'link' => $this->url->link('account/recurring', '', 'SSL'),
							'items' => []
						],
						'favorite_sellers' => [
							'name' => $this->language->get('ms_favorite_seller_title'),
							'link' => $this->url->link('account/msfavoriteseller', '', 'SSL'),
							'items' => []
						],
						'reward' => [
							'name' => $this->language->get('text_reward'),
							'link' => $this->url->link('account/reward', '', 'SSL'),
							'items' => []
						],
						'return' => [
							'name' => $this->language->get('text_return'),
							'link' => $this->url->link('account/return', '', 'SSL'),
							'items' => []
						],
						'transaction' => [
							'name' => $this->language->get('text_transaction'),
							'link' => $this->url->link('account/transaction', '', 'SSL'),
							'items' => []
						],
						'logout' => [
							'name' => $this->language->get('ms_account_logout'),
							'link' => $this->url->link('account/logout', '', 'SSL'),
							'items' => []
						]
					]
				];
			} else {
				if ((int)MsSeller::STATUS_UNPAID === (int)$this->MsLoader->MsSeller->getStatus($this->customer->getId())) {
					// Navigation for sellers that had not paid their sign-up fee

					$data['menus']['seller'] = [
						'title' => $this->language->get('ms_seller_account_heading'),
						'items' => [
							'invoices' => [
								'name' => $this->language->get('ms_invoices'),
								'link' => $this->url->link('seller/account-invoice', '', 'SSL'),
								'items' => []
							],
							'notifications' => [
								'name' => $this->language->get('ms_seller_account_notification'),
								'link' => $this->url->link('seller/account-notification', '', 'SSL'),
								'items' => []
							],
							'profile' => [
								'name' => $this->language->get('ms_account_profile'),
								'link' => $this->url->link('seller/account-profile', '', 'SSL'),
								'items' => []
							],
							'settings' => [
								'name' => $this->language->get('ms_account_settings'),
								'link' => $this->url->link('seller/account-setting', '', 'SSL'),
								'items' => []
							],
							'logout' => [
								'name' => $this->language->get('ms_account_logout'),
								'link' => $this->url->link('account/logout', '', 'SSL'),
								'items' => []
							]
						]
					];
				} elseif ((int)MsSeller::STATUS_ACTIVE === (int)$this->MsLoader->MsSeller->getStatus($this->customer->getId())) {
					// Navigation for active sellers

					$data['menus']['seller'] = [
						'title' => $this->language->get('ms_seller_account_heading'),
						'items' => [
							'dashboard' => [
								'name' => $this->language->get('ms_account_dashboard'),
								'link' => $this->url->link('seller/account-dashboard', '', 'SSL'),
								'items' => []
							],
							'orders' => [
								'name' => $this->language->get('ms_account_orders'),
								'link' => $this->url->link('seller/account-order', '', 'SSL'),
								'items' => []
							],
							'products' => [
								'name' => $this->language->get('ms_account_products'),
								'link' => $this->url->link('seller/account-product', '', 'SSL'),
								'items' => []
							],
							'transactions' => [
								'name' => $this->language->get('ms_account_transactions'),
								'link' => $this->url->link('seller/account-transaction', '', 'SSL'),
								'items' => []
							],
							'invoices' => [
								'name' => $this->language->get('ms_invoices'),
								'link' => $this->url->link('seller/account-invoice', '', 'SSL'),
								'items' => []
							],
							'ms_conversation' => !$this->config->get('mmess_conf_enable') ? false : [
								'name' => $this->language->get('ms_account_messages'),
								'link' => $this->url->link('account/msconversation', '', 'SSL'),
								'items' => []
							],
							'notifications' => [
								'name' => $this->language->get('ms_seller_account_notification'),
								'link' => $this->url->link('seller/account-notification', '', 'SSL'),
								'items' => []
							],
							'reviews' => !$this->config->get('msconf_reviews_enable') ? false : [
								'name' => $this->language->get('ms_account_reviews'),
								'link' => $this->url->link('seller/account-review', '', 'SSL'),
								'items' => []
							],
							'questions' => !$this->config->get('msconf_allow_questions') ? false : [
								'name' => $this->language->get('ms_account_questions'),
								'link' => $this->url->link('seller/account-question', '', 'SSL'),
								'items' => []
							],
							'coupons' => !$this->config->get('msconf_allow_seller_coupons') ? false : [
								'name' => $this->language->get('ms_seller_account_coupon'),
								'link' => $this->url->link('seller/account-coupon', '', 'SSL'),
								'items' => []
							],
							'reports' => [
								'name' => $this->language->get('ms_report_report'),
								'link' => false,
								'items' => [
									'sales' => [
										'name' => $this->language->get('ms_report_sales_list'),
										'link' => $this->url->link('seller/report/sales', '', 'SSL'),
									],
									'sales-day' => [
										'name' => $this->language->get('ms_report_sales_day'),
										'link' => $this->url->link('seller/report/sales-day', '', 'SSL'),
									],
									'sales-month' => [
										'name' => $this->language->get('ms_report_sales_month'),
										'link' => $this->url->link('seller/report/sales-month', '', 'SSL'),
									],
									'sales-product' => [
										'name' => $this->language->get('ms_report_sales_product'),
										'link' => $this->url->link('seller/report/sales-product', '', 'SSL'),
									],
									'transactions' => [
										'name' => $this->language->get('ms_report_finances_transaction'),
										'link' => $this->url->link('seller/report/finances-transaction', '', 'SSL'),
									],
									'payments' => [
										'name' => $this->language->get('ms_report_finances_payment'),
										'link' => $this->url->link('seller/report/finances-payment', '', 'SSL'),
									],
									'payouts' => [
										'name' => $this->language->get('ms_report_finances_payout'),
										'link' => $this->url->link('seller/report/finances-payout', '', 'SSL'),
									],
								]
							],
							'profile' => [
								'name' => $this->language->get('ms_account_profile'),
								'link' => $this->url->link('seller/account-profile', '', 'SSL'),
								'items' => []
							],
							'settings' => [
								'name' => $this->language->get('ms_account_settings'),
								'link' => $this->url->link('seller/account-setting', '', 'SSL'),
								'items' => []
							],
							'logout' => [
								'name' => $this->language->get('ms_account_logout'),
								'link' => $this->url->link('account/logout', '', 'SSL'),
								'items' => []
							]
						]
					];
				}
			}
		}

		return $this->load->view('multimerch/module/account.tpl', $data);
	}
}