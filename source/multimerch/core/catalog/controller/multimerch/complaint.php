<?php
class ControllerMultimerchComplaint extends Controller {
	public $data = array();

	public function __construct($registry) {
		parent::__construct($registry);
		if (!$this->config->get('msconf_complaints_enable')) {
			return $this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}
		$this->data['error'] = '';
		$this->data['title_complaint_for'] = '';
		$this->data['complaint_sent'] = FALSE;
		$this->data['product'] = FALSE;
		$this->data['product_id'] = !empty($this->request->get['product_id']) ? (int) $this->request->get['product_id'] : FALSE;
		$this->data['seller_id'] = !empty($this->request->get['seller_id']) ? (int) $this->request->get['seller_id'] : FALSE;
		if (!$this->customer->isLogged()) {
			if ($this->data['product_id'] && $this->data['product'] = $this->MsLoader->MsProduct->getProduct($this->data['product_id'])) {
				$this->data['back_url'] = $this->url->link('multimerch/complaint', 'product_id=' . $this->data['product_id'], 'SSL');
			} else if ($this->data['seller_id'] && $this->data['seller'] = $this->MsLoader->MsSeller->getSeller($this->data['seller_id'])) {
				$this->data['back_url'] = $this->url->link('multimerch/complaint', 'seller_id=' . $this->data['seller_id'], 'SSL');
			} else {
				return $this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}
			$this->session->data['redirect'] = $this->data['back_url'];
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
	}

	public function index() {
		$this->_validate();
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (utf8_strlen(trim($this->request->post['comment'])) <= 5) {
				$this->data['error'] = sprintf($this->language->get('ms_complaint_form_error_comment_short'), 5);
			}
			if (utf8_strlen(trim($this->request->post['comment'])) > 5000) {
				$this->data['error'] = sprintf($this->language->get('ms_complaint_form_error_comment_long'), 5000);
			}
			if (!$this->data['error']) {

				// Add to db...
				$data = array(
					'customer_id' => $this->customer->getId(),
					'product_id' => $this->data['product'] ? $this->data['product_id'] : 0,
					'seller_id' => $this->data['product'] ? $this->data['product']['seller_id'] : $this->data['seller_id'],
					'comment' => trim($this->request->post['comment'])
				);
				$this->MsLoader->MsComplaint->createComplaint($data);

				//... and send email to admin
				$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
				$mailTransport = $serviceLocator->get('MailTransport');
				$mails = new \MultiMerch\Mail\Message\MessageCollection();
				$MailComplaintNoty = $serviceLocator->get('MailAdminComplaintReceived', false)
					->setTo($this->config->get('config_email'))
					->setData(array(
					'item' => $this->data['product'] ? $this->language->get('ms_complaint_type_product') : $this->language->get('ms_complaint_type_seller'),
					'item_name' => $this->data['title_complaint_for'],
					'comment' => $data['comment'],
					'admin_href' => $this->config->get('config_url') . 'admin/index.php?route=multimerch/complaint',
					'title' => sprintf($this->language->get('ms_mail_admin_subject_complaint_received'), $this->data['title_complaint_for'])
				));
				$mails->add($MailComplaintNoty);
				$mailTransport->sendMails($mails);

				$this->session->data['success'] = $this->language->get('ms_complaint_form_success');
				return $this->response->redirect($this->data['back_url']);
			}
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_complaint_form_breadcrumbs'),
				'href' => $this->data['this_url'],
			)
		));
		$this->data['comment'] = isset($this->request->post['comment']) ? $this->request->post['comment'] : '';


		$this->document->setTitle($this->language->get('ms_complaint_form_breadcrumbs'));
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/complaint-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	private function _validate() {
		$this->load->model('tool/image');
		$this->load->model('localisation/country');
		$this->load->model('catalog/product');

		//validate product or seller
		if ($this->data['product_id'] && $this->data['product'] = $this->model_catalog_product->getProduct($this->data['product_id'])) {

			if ($this->data['product'] && file_exists(DIR_IMAGE . $this->data['product']['image'])) {
				$product_image = $this->model_tool_image->resize($this->data['product']['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			} else {
				$product_image = $this->model_tool_image->resize('no_image.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}

			if ($this->customer->getId() == $this->data['product']['seller_id']) {
				$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}

			$this->data['product'] = array_merge($this->data['product'], array(
				'product_href' => $this->url->link('product/product', '&product_id=' . $this->data['product_id']),
				'seller_href' => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $this->data['product']['seller_id']),
				'image' => $product_image
			));

			$this->data['title_complaint_for'] = $this->data['product']['name'];
			$this->data['this_url'] = $this->url->link($this->request->get['route'], 'product_id=' . $this->data['product_id'], 'SSL');
			$this->data['back_url'] = $this->url->link('product/product', 'product_id=' . $this->data['product_id'], 'SSL');
		} else if ($this->data['seller_id'] && $this->data['seller'] = $this->MsLoader->MsSeller->getSeller($this->data['seller_id'])) {

			if ($this->customer->getId() == $this->data['seller_id']) {
				$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}

			// Lets grab some seller's info (generic stuff from catalog-seller):
			$settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $this->data['seller_id']]);
			$default_settings = $this->MsLoader->MsSetting->getSellerDefaults();
			$settings = array_merge($default_settings, $settings);
			$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $this->data['seller_id'], 'address_id' => $settings['slr_ms_address'], 'single' => true]);

			if (!empty($seller_ms_address['city'])) {
				$settings['slr_city'] = utf8_strlen($seller_ms_address['city']) > 20 ? (utf8_substr($seller_ms_address['city'], 0, 18) . '..') : $seller_ms_address['city'];
			}

			if (!empty($seller_ms_address['country_id'])) {
				$country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
				$settings['slr_country'] = (isset($country['name']) ? $country['name'] : '');
			}

			if ($this->data['seller']['ms.avatar'] && file_exists(DIR_IMAGE . $this->data['seller']['ms.avatar'])) {
				$seller_image = $this->model_tool_image->resize($this->data['seller']['ms.avatar'], $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
			} else {
				$seller_image = $this->model_tool_image->resize('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
			}

			$this->data['seller'] = array_merge($this->data['seller'], array(
				'settings' => $settings,
				'thumb' => $seller_image,
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $this->data['seller_id'])
			));

			$this->data['title_complaint_for'] = $this->data['seller']['ms.nickname'];
			$this->data['this_url'] = $this->url->link($this->request->get['route'], 'seller_id=' . $this->data['seller_id'], 'SSL');
			$this->data['back_url'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $this->data['seller_id'], 'SSL');
		} else {
			return $this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}
	}

}