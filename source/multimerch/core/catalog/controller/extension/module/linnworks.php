<?php
class ControllerExtensionModuleLinnworks extends Controller {
	private $input = '';
	private $token = null;
	private $seller = false;
	private $wizardStepsNext = [
		'ConnectSeller' => 'UserConfig',
	];
	private $wizardConfigSteps = [
		'ConnectSeller' => array(
			'StepName' => 'ConnectSeller',
			'WizardStepTitle' => 'Setting up connection',
			'WizardStepDescription' => 'Enter your access key from seller profile page',
			'ConfigItems' => [
				array("ConfigItemId" => "AccessKey",
					"Name" => "Access key",
					"Description" => "Seller's access key",
					//"GroupName" => "API Credentials",
					"SortOrder" => 1,
					"SelectedValue" => "",
					"RegExValidation" => null,
					"RegExError" => null,
					"MustBeSpecified" => true,
					"ReadOnly" => false,
					"ListValues" => [],
					"ValueType" => "STRING"
				)
			]),
		'UserConfig' => array(
			'StepName' => 'UserConfig',
			'WizardStepTitle' => 'Finish',
			'WizardStepDescription' => 'Integration completed',
			'ConfigItems' => []
		)
	];

	public function __construct($registry) {

		parent::__construct($registry);
		// LOG
		/*
		  $r = date('d.m.Y H:i:s') . PHP_EOL;
		  $r .= 'REQUEST_URI: ' . $this->request->server['REQUEST_URI'] . PHP_EOL;
		  $r .= 'php://input: ' . file_get_contents("php://input") . PHP_EOL;
		  $r .= 'GET: ' . json_encode($this->request->get) . PHP_EOL . PHP_EOL;
		  $r .= 'REQUEST: ' . json_encode($this->request->request) . PHP_EOL . PHP_EOL;
		  $r .= 'SERVER: ' . json_encode($this->request->server) . PHP_EOL . PHP_EOL;


		  $file = 'linn_' . date('d-m-Y') . '.log';
		  file_put_contents(DIR_LOGS . '/' . $file, $r, FILE_APPEND);
		 */

		$r = [];
		if (1 != $this->config->get('ms_linnworks_enabled')) {
			$r['Error'] = 'Linnworks integration disabled!';
			$this->send_json($r);
			$this->response->output();
			exit();
		}
		$this->load->model('extension/module/linnworks');
		$this->input = json_decode(file_get_contents("php://input"));
		$this->token = !empty($this->input->AuthorizationToken) ? $this->input->AuthorizationToken : null;

		if ($this->token) {
			$this->seller = $this->model_extension_module_linnworks->getSellerByToken($this->token);
		}
	}

	public function index() {
		/* empty line */ // line required for correct vqmodding via MM files
		return;
	}

	public function AddNewUserEndpoint() {
		$response = [];
		$token = null;

		$data['lw_uid'] = $this->input->LinnworksUniqueIdentifier;

		if (!$token = $this->model_extension_module_linnworks->getTokenByLinnworksId($data['lw_uid'])) {
			$token = $this->model_extension_module_linnworks->addIntegration($data);
		}

		if ($token) {
			$response['AuthorizationToken'] = $token;
		}

		$this->send_json($response);
	}

	public function UserConfigEndpoint() {

		if (!$seller_id = $this->model_extension_module_linnworks->getSellerIdByToken($this->token)) {
			$r = reset($this->wizardConfigSteps);
		} else {
			$r = $this->wizardConfigSteps['UserConfig'];
		}
		$this->send_json($r);
	}

	public function SaveConfigEndpoint() {

		$currentStepName = $this->input->StepName;
		$stepProcessResult = true;

		switch ($currentStepName) {
			case "ConnectSeller": {
					$stepProcessResult = $this->_processConnectSeller();
				}
				break;
		}

		if (!empty($this->wizardStepsNext[$currentStepName])) {
			if ($stepProcessResult === true) {
				$nextStepName = $this->wizardStepsNext[$currentStepName];
				$step = $this->wizardConfigSteps[$nextStepName];
				$r = $step;
			} else {
				$r['Error'] = $stepProcessResult;
			}
		} else {
			$r['Error'] = 'Wizard step not found!';
		}


		$r['AuthorizationToken'] = $this->token;
		$this->send_json($r);
	}

	private function _processConnectSeller() {
		$access_code = $this->input->ConfigItems[0]->SelectedValue;
		$seller_id = $this->model_extension_module_linnworks->getSellerIdByAccessKey($access_code);
		if (!$seller_id) {
			return 'Invalid access key';
		}

		$this->model_extension_module_linnworks->setSeller($this->token, $seller_id);

		return true;
	}

// test of endpoint. Return error if user not found
	public function ConfigTestEndpoint() {
		$r = [];
		if (!$this->seller) {
			$r['Error'] = 'Seller not found!';
		}
		$this->send_json($r);
	}

	public function ProductsEndpoint() {
		$limit = 100;

		list ($response['Products'], $total_products) = $this->model_extension_module_linnworks->getSellerProducts(
			$this->seller['seller_id'], [
			'limit' => $limit,
			'page' => $this->input->PageNumber
			]
		);
		$total_pages = ceil($total_products / $limit);

		if ($total_products == 0) {
			$response['HasMorePages'] = false;
		} else {
			$response['HasMorePages'] = $total_pages < $this->input->PageNumber;
		}

		$this->send_json($response);
	}

	public function InventoryUpdateEndpoint() {
		$response = [];
		if (!empty($this->input->Products) && is_array($this->input->Products)) {
			foreach ($this->input->Products as $prod) {
				$pid = $prod->Reference;
				$amt = $prod->Quantity;
				$sku = $prod->SKU;
				$isOk = $this->model_extension_module_linnworks->updateProductQuantity($pid, $sku, $amt);
				$r['Error'] = $isOk ? 'null' : 'Product not found';
				$r['SKU'] = $sku;
				$response['Products'][] = $r;
			}
		}
		$this->send_json($response);
	}

// Remove seller linnworks data from store
	public function ConfigDeletedEndpoint() {
		$this->model_extension_module_linnworks->removeIntegration($this->token);
		$this->send_json();
	}

	/*	 * ****** */
	public function ShippingTagsEndpoint() {
		$this->send_json();
	}

	public function PaymentTagsEndpoint() {
		$this->send_json();
	}

	public function OrdersEndpoint() {
		$this->send_json();
	}

	public function DespatchEndpoint() {
		$this->send_json();
	}

	public function PriceEndpoint() {
		$this->send_json();
	}

	public function GetConfiguratorSettingsEndpoint() {
		$this->send_json();
	}

	public function GetCategoriesEndpoint() {
		$this->send_json();
	}

	public function GetAttributesByCategoryEndpoint() {
		$this->send_json();
	}

	public function GetVariationsByCategoryEndpoint() {
		$this->send_json();
	}

	public function ListingUpdateEndpoint() {
		$this->send_json();
	}

	public function ListingDeleteEndpoint() {
		$this->send_json();
	}

	public function CheckFeedEndpoint() {
		$this->send_json();
	}

	public function PriceUpdateEndpoint() {
		$this->send_json();
	}

	private function send_json($json = []) {
		$json['Error'] = !empty($json['Error']) ? $json['Error'] : null;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->addHeader('Content-Length: ' . mb_strlen(json_encode($json)));
		$this->response->setOutput(json_encode($json));
	}

}