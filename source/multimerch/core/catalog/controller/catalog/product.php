<?php
/**
 * copy of appropriate controller from admin application
 */
class ControllerCatalogProduct extends Controller {

	public function jxAutocompleteRelated() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$filter_data = array(
				'filters' => array(
					'pd.name' => isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : ''
				),
				'order_by' => 'pd.name',
				'order_way' => 'ASC',
				'offset' => 0,
				'limit' => 5
			);

			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$results = $this->MsLoader->MsProduct->getProducts(array('seller_id' => $seller_id, 'available' => true, 'oc_status' => 1), $filter_data);

			foreach ($results as $key => $result) {
				if($result['product_id'] != $this->request->get['product_id']) {
					$json[] = array(
						'product_id' => $result['product_id'],
						'name'       => strip_tags(html_entity_decode($result['pd.name'], ENT_QUOTES, 'UTF-8')),
					);
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetProductMsfVariations()
	{
		$data = $this->request->get;
		$product_id = !empty($data['product_id']) ? (int)$data['product_id'] : null;

		if (!$product_id) {
			return false;
		}

		$this->load->model('catalog/product');
		$product_info = $this->model_catalog_product->getProduct($product_id);

		// Get all available product variations
		$all_variations = $this->MsLoader->MsfVariation->getProductMsfVariationsAll($product_id); // getAllProductMsfVariations

		// Get filtered product variations based on customer's selection
		$selected = [];
		if (!empty($data['msf_variations'])) {
			foreach (array_filter($data['msf_variations']) as $msf_variation_id => $msf_variation_value_id) {
				$selected[$msf_variation_id] = $this->MsLoader->MsfVariation->getValue($msf_variation_value_id);
			}
		}

		$filtered_variations = [];
		if (!empty($selected)) {
			$filtered_variations = $this->MsLoader->MsfVariation->getProductMsfVariationsFiltered($product_id, $selected); // getFilteredProductMsfVariations
		}

		list($available, $price_html, $tax_html, $cart_enabled, $error_message) = $this->detectAvailable($all_variations, $filtered_variations, $product_info);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode([
			'success' => true,
			'html' => $this->load->view('product/mm_product_variation', array_merge(
				$this->load->language('multiseller/multiseller'),
				['available' => $available, 'selected' => $selected]
			)),
			'price_html' => $price_html,
			'tax_html' => $tax_html,
			'cart_enabled' => $cart_enabled,
			'error_message' => $error_message
		]));
	}

	private function detectAvailable($all_variations, $filtered_variations, $product_info)
	{
		$this->load->language('multiseller/multiseller');

		$available = [
			'all' => [],
			'filtered' => []
		];

		$min_price = $max_price = 0;
		if (empty($filtered_variations)) {
			$min_price = $product_info['price'];
		}

		$cart_enabled = empty($all_variations) ? true : false;
		$error_message = '';

		foreach ($all_variations as $variation) {
			foreach ($variation['values'] as $value) {
				if (empty($available['all'][$value['msf_variation_id']])) {
					$available['all'][$value['msf_variation_id']] = [
						'variation' => array_merge($variation, ['name' => $value['msf_variation_name']]),
						'values' => []
					];
				}

				$available['all'][$value['msf_variation_id']]['values'][] = [
					'id' => $value['id'],
					'name' => $value['name']
				];
			}
		}

		foreach ($filtered_variations as $variation) {
			if ($min_price === 0 || (float)$variation['price'] < (float)$min_price ) {
				$min_price = $variation['price'];
			}

			if ((float)$variation['price'] > (float)$max_price) {
				$max_price = $variation['price'];
			}

			if (count($filtered_variations) === 1) {
				$min_price = $max_price = $variation['price'];

				if (!$variation['status']) {
					$error_message = $this->language->get('ms_product_msf_variation_error_status');
				} elseif (!$variation['quantity']) {
					$error_message = $this->language->get('ms_product_msf_variation_error_out_of_stock');
				} else {
					$cart_enabled = true;
				}
			}

			foreach ($variation['values'] as $value) {
				$key = 'available_ids';
				if (0 === (int)$variation['status']) {
					$key = 'hidden_ids';
				} elseif (0 === (int)$variation['quantity']) {
					$key = 'disabled_ids';
				}

				if (empty($available['filtered'][$value['msf_variation_id']])) {
					$available['filtered'][$value['msf_variation_id']] = [
						'values' => [
							'all_ids' => [],
							'available_ids' => [],
							'disabled_ids' => [],
							'hidden_ids' => [],
						]
					];
				}

				$available['filtered'][$value['msf_variation_id']]['values']['all_ids'][] = $value['id'];

				if (!in_array($value['id'], $available['filtered'][$value['msf_variation_id']]['values'][$key])) {
					$available['filtered'][$value['msf_variation_id']]['values'][$key][] = $value['id'];
				}
			}
		}

		if ($max_price < $min_price) {
			$max_price = $min_price;
		}

		if (empty($all_variations)) {
			$price_html = $this->currency->format($this->tax->calculate($min_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

			$tax_html = $this->config->get('config_tax')
				? $this->currency->format($min_price, $this->session->data['currency'])
				: false;
		} elseif (empty($filtered_variations)) {
			$price_html = sprintf($this->language->get('ms_product_msf_variation_price_from'),
				$this->currency->format($this->tax->calculate($min_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
			);

			$tax_html = $this->config->get('config_tax')
				? sprintf($this->language->get('ms_product_msf_variation_price_from'), $this->currency->format($min_price, $this->session->data['currency']))
				: false;
		} elseif ((float)$min_price === (float)$max_price) {
			$price_html = $this->currency->format($this->tax->calculate($min_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

			$tax_html = $this->config->get('config_tax')
				? $this->currency->format($min_price, $this->session->data['currency'])
				: false;
		} else {
			$price_html = sprintf(
				"%s - %s",
				$this->currency->format($this->tax->calculate($min_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
				$this->currency->format($this->tax->calculate($max_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
			);

			$tax_html = $this->config->get('config_tax')
				? sprintf(
					"%s - %s",
					$this->currency->format($min_price, $this->session->data['currency']),
					$this->currency->format($max_price, $this->session->data['currency'])
				)
				: false;
		}

		return [$available, $price_html, $tax_html, $cart_enabled, $error_message];
	}
}
