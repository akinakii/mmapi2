<?php

use MultiMerch\Core\Invoice\Invoice as Invoice;

class ControllerSellerAccountInvoice extends ControllerSellerAccount {
	public function getTableData() {
		$colMap = array();

		$seller_id = $this->customer->getId();

		$sorts = array('invoice_id', 'type', 'payment_id', 'status', 'date_generated', 'total');
		$filters = array_diff($sorts, array('type', 'status'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$invoices = $this->MsLoader->MsInvoice->get(
			[
				'recipient_type' => 'seller',
				'recipient_id' => $seller_id,
				'types' => ['listing', 'signup']
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = count($invoices);

		$columns = array();

		foreach ($invoices as $invoice) {
			$payment_info = '-';

			// If invoice is paid
			if ($invoice->getPaymentId()) {
				$payment = $this->MsLoader->MsPgPayment->getPayments(['payment_id' => $invoice->getPaymentId(), 'single' => true]);

				if (!empty($payment)) {
					$pg_name = str_replace('ms_pg_', '', $payment['payment_code']);
					$this->load->language('multimerch/payment/' . $pg_name);

					// Show payment details
					$html  = '<p>Method: ' . ($payment['payment_code'] == 'ms_pp_adaptive' ? 'PayPal Adaptive' : $this->language->get('text_title')) . '</p>';
					$html .= '<p>Date paid: ' . date($this->language->get('date_format_short'), strtotime($payment['date_created'])) . '</p>';
					$html .= '<p>Status: ' . $this->language->get('ms_pg_payment_status_no_color_' . $payment['payment_status']) . '</p>';

					$payment_info  = '<a class="paymentInfoControl" href="#" data-toggle="tooltip" data-html="true" title="' . $html . '" onclick="return false;">' . (int)$invoice->getPaymentId() . '</a>';
				}
			}

			$columns[] = array(
				'checkbox' => (Invoice::STATUS_PAID === (int)$invoice->getStatus() || $invoice->getPaymentId()) ? "" : "<input type='checkbox' name='selected[]' value='{$invoice->getInvoiceId()}' style='margin: 0;' />",
				'invoice_id' => $invoice->getInvoiceId(),
				'type' => $this->language->get('ms_invoice_type_' . $invoice->getType()),
				'title' => $invoice->getTitle(),
				'status' => $this->language->get('ms_invoice_status_' . $invoice->getStatus()),
				'payment' => $payment_info,
				'date_generated' => date($this->language->get('date_format_short'), strtotime($invoice->getDateGenerated())),
				'total' => $this->currency->format(abs($invoice->getTotal()), $invoice->getCurrencyCode())
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function index() {
		$this->data['link_back'] = $this->url->link('seller/account-dashboard', '', 'SSL');

		$this->data['action'] = 'index.php?route=seller/account-payment/create';

		$this->document->setTitle($this->language->get('ms_invoice_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_invoice_breadcrumbs'),
				'href' => $this->url->link('seller/account-invoice', '', 'SSL'),
			)
		));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-invoice');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
}
