<?php

use \MultiMerch\Event\Event as MsEvent;

class ControllerSellerAccountProduct extends ControllerSellerAccount {
	public function getTableData() {
		$this->load->model('tool/image');

		$colMap = array(
			'product_name' => 'pd.name',
			'product_status' => '`mp.product_status`',
			'quantity' => '`p.quantity`',
			'date_added' => '`p.date_added`',
			'list_until' => 'mp.list_until',
			'number_sold' => '`number_sold`',
			'product_price' => 'CASE WHEN special THEN special ELSE p.price END'
		);
		
		$sorts = array('product_name', 'product_price', 'date_added', 'list_until', 'product_status', 'product_earnings', 'number_sold', 'quantity');
		$filters = array_diff($sorts, array('product_status'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$seller_id = $this->customer->getId();
		$products = $this->MsLoader->MsProduct->getProducts(
			array(
				'seller_id' => $seller_id,
				'language_id' => $this->config->get('config_language_id'),
				'product_status' => array(MsProduct::STATUS_ACTIVE, MsProduct::STATUS_INACTIVE, MsProduct::STATUS_DISABLED, MsProduct::STATUS_UNPAID, MsProduct::STATUS_IMPORTED)
			),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'product_earnings' => 1,
				'product_sales' => 1
			)
		);
		
		$total = isset($products[0]) ? $products[0]['total_rows'] : 0;

		$columns = array();
		foreach ($products as $product) {

			// price
			$product['p.price'] = $this->currency->format($product['p.price'], $this->config->get('config_currency'));
			if ($product['special']) {
				$special = $this->currency->format($product['special'], $this->config->get('config_currency'));
				$price = "<span style='text-decoration: line-through;'>{$product['p.price']}</span><br/>";
				$price .= "<span class='special-price' style='color: #b00;'>$special</span>";
			} else {
				$price = $product['p.price'];
			}

			// Product Image
			if ($product['p.image'] && file_exists(DIR_IMAGE . $product['p.image'])) {
				$image = $this->model_tool_image->resize($product['p.image'], $this->config->get('msconf_product_seller_product_list_seller_area_image_width'), $this->config->get('msconf_product_seller_product_list_seller_area_image_height'));
			} else {
				$image = $this->model_tool_image->resize('no_image.png', $this->config->get('msconf_product_seller_product_list_seller_area_image_width'), $this->config->get('msconf_product_seller_product_list_seller_area_image_height'));
			}
			
			// actions
			$actions = "";
			if ($product['mp.product_status'] != MsProduct::STATUS_DISABLED) {
				if ($product['mp.product_status'] == MsProduct::STATUS_ACTIVE)
					$actions .= "<a class='icon-view' href='" . $this->url->link('product/product', 'product_id=' . $product['product_id'], 'SSL') ."' title='" . $this->language->get('ms_viewinstore') . "'><i class='fa fa-search'></i></a>";
	
				if ($product['mp.product_approved']) {
					if ($product['mp.product_status'] == MsProduct::STATUS_INACTIVE OR $product['mp.product_status'] == MsProduct::STATUS_IMPORTED)
						$actions .= "<a class='icon-publish' href='" . $this->url->link('seller/account-product/publish', 'product_id=' . $product['product_id'], 'SSL') ."' title='" . $this->language->get('ms_publish') . "'><i class='fa fa-plus'></i></a>";
		
					if ($product['mp.product_status'] == MsProduct::STATUS_ACTIVE)
						$actions .= "<a class='icon-unpublish' href='" . $this->url->link('seller/account-product/unpublish', 'product_id=' . $product['product_id'], 'SSL') ."' title='" . $this->language->get('ms_unpublish') . "'><i class='fa fa-minus'></i></a>";
				}
				
				$actions .= "<a class='icon-edit' href='" . $this->url->link('seller/account-product/update', 'product_id=' . $product['product_id'], 'SSL') ."' title='" . $this->language->get('ms_edit') . "'><i class='fa fa-pencil'></i></a>";
				$actions .= "<a class='icon-remove' href='" . $this->url->link('seller/account-product/delete', 'product_id=' . $product['product_id'], 'SSL') ."' title='" . $this->language->get('ms_delete') . "'><i class='fa fa-times'></i></a>";
			} else {
				if ($this->config->get('msconf_allow_relisting')) {
					$actions .= "<a href='" . $this->url->link('seller/account-product/update', 'product_id=' . $product['product_id'] . "&relist=1", 'SSL') ."' class='ms-button ms-button-relist' title='" . $this->language->get('ms_relist') . "'></a>";
				}
			}
			
			// product status
			$status = "";
			if ($product['mp.product_status'] == MsProduct::STATUS_ACTIVE) {
				$status = "<span class='active' style='color: #080;'>" . $this->language->get('ms_product_status_' . $product['mp.product_status']) . "</td></span>";
			} else {
				$status = "<span class='inactive' style='color: #b00;'>" . $this->language->get('ms_product_status_' . $product['mp.product_status']) . "</td></span>";
			}
			
			// List until
			if (isset($product['mp.list_until']) && $product['mp.list_until'] != NULL) {
				$list_until = date($this->language->get('date_format_short'), strtotime($product['mp.list_until']));
			} else {
				$list_until = $this->language->get('ms_not_defined');
			}
			$columns[] = array_merge(
				$product,
				array(
					'image' => "<img src='$image' />",
					'product_name' => $product['pd.name'],
					'product_price' => $price,
					'number_sold' => $product['number_sold'],
					'product_earnings' => $this->currency->format($product['product_earnings'], $this->config->get('config_currency')),
					'product_status' => $status,
					'date_added' => date($this->language->get('date_format_short'), strtotime($product['p.date_added'])),
					'list_until' => $list_until,
					'actions' => $actions,
					'quantity' => $product['p.quantity']
				)
			);
		}
		
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function jxAutocompleteOptions(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('tool/image');

			$data = array(
				'seller_ids' => '0,' . $this->customer->getId(),
				'option_status' => MsOption::STATUS_ACTIVE
			);

			$filter_data = array(
				'filters' => array(
					'od.name' => $this->request->get['filter_name']
				)
			);

			$options = $this->MsLoader->MsOption->getOptions($data, $filter_data);

			foreach ($options as $option) {
				$option_value_data = array();

				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
					$option_values = $this->MsLoader->MsOption->getOptionValues($option['option_id']);

					foreach ($option_values as $option_value) {
						if (is_file(DIR_IMAGE . $option_value['image'])) {
							$image = $this->model_tool_image->resize($option_value['image'], 50, 50);
						} else {
							$image = $this->model_tool_image->resize('no_image.png', 50, 50);
						}

						$option_value_data[] = array(
							'option_value_id' => $option_value['option_value_id'],
							'name'            => strip_tags(html_entity_decode($option_value['name'], ENT_QUOTES, 'UTF-8')),
							'image'           => $image
						);
					}

					$sort_order = array();

					foreach ($option_value_data as $key => $value) {
						$sort_order[$key] = $value['name'];
					}

					array_multisort($sort_order, SORT_ASC, $option_value_data);
				}

				$json[] = array(
					'option_id'    => $option['option_id'],
					'name'         => strip_tags(html_entity_decode($option['name'], ENT_QUOTES, 'UTF-8')),
					'type'         => $option['type'],
					'option_value' => $option_value_data
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxAutocompleteAttributes(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$data = array(
				'seller_ids' => '0,' . $this->customer->getId(),
				'attribute_status' => MsAttribute::STATUS_ACTIVE
			);

			$filter_data = array(
				'filters' => array(
					'ad.name' => $this->request->get['filter_name']
				)
			);

			$results = $this->MsLoader->MsAttribute->getAttributes($data, $filter_data);

			foreach ($results as $result) {
				$attribute_group_data = $this->MsLoader->MsAttribute->getAttributeGroups(array('attribute_group_id' => $result['attribute_group_id'], 'single' => 1));

				$json[] = array(
					'attribute_id'    => $result['attribute_id'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'attribute_group' => $attribute_group_data['name']
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxUpdateFile() {
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$json = array();
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}
		
		if (isset($this->request->post['file_id']) && isset($this->request->post['product_id'])) {
			$download_id = (int)substr($this->request->post['file_id'], strrpos($this->request->post['file_id'], '-')+1);
			$product_id = (int)$this->request->post['product_id'];
			$seller_id = $this->customer->getId();
			if  ($this->MsLoader->MsProduct->productOwnedBySeller($product_id,$seller_id) && $this->MsLoader->MsProduct->hasDownload($product_id,$download_id)) {
				$file = array_shift($_FILES);
				$errors = $this->MsLoader->MsFile->checkDownload($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileData = $this->MsLoader->MsFile->uploadDownload($file);
					$json['fileName'] = $fileData['fileName'];
					$json['fileMask'] = $fileData['fileMask'];
				}
			}
		}
			
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadSellerAvatar() {
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$json = array();
		$file = array();
		
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		foreach ($_FILES as $file) {
			$errors = $this->MsLoader->MsFile->checkImage($file);
			
			if ($errors) {
				$json['errors'] = array_merge($json['errors'], $errors);
			} else {
				$fileName = $this->MsLoader->MsFile->uploadImage($file);
				$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'));
				$json['files'][] = array(
					'name' => $fileName,
					'thumb' => $thumbUrl
				);
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadDescriptionImage(){
		
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);
		
		if ($json['errors']) {
			$json["uploaded"] = 0;
			$json["error"]['message'] = implode('<br>',$json['errors']);
			return $this->response->setOutput(json_encode($json));
		}
		
		$file = reset($_FILES);
		
		if (empty($this->request->get['product_id'])){
			$imageFor = false;
		} else{
			$imageFor = $this->request->get['product_id'];
		}
		
		list($json["fileName"], $json['url']) = $this->MsLoader->MsFile->uploadDescriptionImage($file, $imageFor);
		$json["uploaded"] = 1;
	
		return $this->response->setOutput(json_encode($json));
	}
	
	
	public function jxUploadImages() {
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$json = array();
		$file = array();
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N images
		$msconf_images_limits = $this->config->get('msconf_images_limits');
		foreach ($_FILES as $file) {
			if ($msconf_images_limits[1] > 0 && $this->request->post['imageCount'] > $msconf_images_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_image_maximum'),$msconf_images_limits[1]);
				$json['cancel'] = 1;
				$this->response->setOutput(json_encode($json));
				return;
			} else {
				$errors = $this->MsLoader->MsFile->checkImage($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileName = $this->MsLoader->MsFile->uploadImage($file);

					$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'));
					$json['files'][] = array(
						'name' => $fileName,
						'thumb' => $thumbUrl
					);
				}
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadDownloads() {
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$json = array();
		$file = array();

		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N files
		$msconf_downloads_limits = $this->config->get('msconf_downloads_limits');
		foreach ($_FILES as $file) {
			if ($msconf_downloads_limits[1] > 0 && $this->request->post['fileCount'] > $msconf_downloads_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_download_maximum'),$msconf_downloads_limits[1]);
				$json['cancel'] = 1;
				$this->response->setOutput(json_encode($json));
				return;
			} else {
				$errors = $this->MsLoader->MsFile->checkDownload($file);
				
				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileData = $this->MsLoader->MsFile->uploadDownload($file);

					$json['files'][] = array (
						'fileName' => $fileData['fileName'],
						'fileMask' => $fileData['fileMask'],
						'filePages' => isset($pages) ? $pages : ''
					);
				}
			}
		}
		
		return $this->response->setOutput(json_encode($json));
	}

	public function jxUploadCustomFieldFile() {
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$this->load->language('tool/upload');

		$json = array();

		// Validate max post size
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		$msconf_downloads_limits = $this->config->get('msconf_downloads_limits');
		foreach ($this->request->files as $key => $file) {
			if ($msconf_downloads_limits[1] > 0 && $this->request->post['fileCount'] > $msconf_downloads_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_download_maximum'), $msconf_downloads_limits[1]);

				return $this->response->setOutput(json_encode($json));
			} else {
				// Validate file extension and max_upload_size
				$json['errors'] = $this->MsLoader->MsFile->checkFile($file, $this->config->get('msconf_msg_allowed_file_types'));

				// Return any upload error, except UPLOAD_ERR_INI_SIZE because MsFile->checkFile already handles this
				if ($file['error'] != UPLOAD_ERR_OK && $file['error'] != UPLOAD_ERR_INI_SIZE) {
					$json['errors'][] = array($this->language->get('error_upload_' . $file['error']));
				}

				if ($json['errors']) {
					return $this->response->setOutput(json_encode($json));
				}

				if (!empty($file['name']) && is_file($file['tmp_name'])) {
					// Sanitize the filename
					$filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));

					// Validate the filename length
					if ((utf8_strlen($filename) < 3)) {
						$json['errors'][] = sprintf($this->language->get('ms_file_filename_error_less'), 3);
					}

					if ((utf8_strlen($filename) > 64)) {
						$json['errors'][] = sprintf($this->language->get('ms_file_filename_error_greater'), 64);
					}

					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($file['tmp_name']);

					if (preg_match('/\<\?php/i', $content)) {
						$json['errors'] = sprintf($this->language->get('ms_error_file_type'), $file['name']);
					}
				} else {
					$json['errors'][] = sprintf($this->language->get('ms_error_file_upload_error'), $this->request->post['name'], $this->language->get('ms_file_unclassified_error'));
				}
			}

			if (empty($json['errors'])) {
				// Get custom_field_id from key of $this->request->files array
				$p = explode('-', $key);
				$custom_field_id = isset($p[2]) ? $p[2] : 0;

				// Hide the uploaded file name so people can not link to it directly.
				$ms_file = $filename . '.' . token(32);

				move_uploaded_file($file['tmp_name'], DIR_DOWNLOAD . $ms_file);

				$download_data = array(
					'filename' => $ms_file,
					'mask' => $filename
				);

				$this->load->model('localisation/language');
				$languages = $this->model_localisation_language->getLanguages();
				foreach ($languages as  $language) {
					$download_data['download_description'][$language['language_id']]['name'] = $filename;
				}

				$json['files'][] = array (
					'download_id' => $this->MsLoader->MsHelper->addOcDownload($download_data),
					'custom_field_id' => $custom_field_id,
					'fileName' => $ms_file,
					'fileMask' => $filename,
					'filePages' => isset($pages) ? $pages : ''
				);
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	public function jxGetListingFee()
	{
		$price = isset($this->request->get['price']) ? (float)$this->request->get['price'] : 0;
		$category_ids = isset($this->request->get['category_ids']) ? explode(',', $this->request->get['category_ids']) : [];

		$payment_method = MsPgPayment::METHOD_BALANCE;

		switch ($this->config->get('msconf_sale_fee_calculation_mode')) {
			case 2:
				if (1 === (int)$this->config->get('msconf_fee_priority')) {
					// If category commission is in prior
					$commissions = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates($category_ids, MsCommission::RATE_LISTING, $price);
				} else {
					// If vendor commission is in prior or no product categories are set
					$commissions = $this->MsLoader->MsSeller->getFeeRates($this->customer->getId());
				}

				$flat = !empty($commissions[MsCommission::RATE_LISTING]['flat'])
					? (float)$commissions[MsCommission::RATE_LISTING]['flat']
					: 0;
				$percent = !empty($commissions[MsCommission::RATE_LISTING]['percent'])
					? (float)$commissions[MsCommission::RATE_LISTING]['percent'] * (float)$price / 100
					: 0;

				$fee = $this->currency->format($flat + $percent, $this->config->get('config_currency'));

				if (!empty($commissions[MsCommission::RATE_LISTING]['payment_method'])) {
					$payment_method = $commissions[MsCommission::RATE_LISTING]['payment_method'];
				}

				if (1 === (int)$this->config->get('msconf_fee_priority')) {
					$json['rate'] = sprintf($this->language->get('ms_account_product_listing_fee_category'), $fee);
				} else {
					$json['rate'] = sprintf($this->language->get('ms_account_product_listing_fee_seller'), $fee);
				}

				break;

			case 1:
			default:
				$commissions_catalog = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates($category_ids, MsCommission::RATE_LISTING, $price);

				$flat_catalog = !empty($commissions_catalog[MsCommission::RATE_LISTING]['flat'])
					? (float)$commissions_catalog[MsCommission::RATE_LISTING]['flat']
					: 0;
				$percent_catalog = !empty($commissions_catalog[MsCommission::RATE_LISTING]['percent'])
					? (float)$commissions_catalog[MsCommission::RATE_LISTING]['percent'] * (float)$price / 100
					: 0;

				$commissions_seller = $this->MsLoader->MsSeller->getFeeRates($this->customer->getId());

				$flat_seller = !empty($commissions_seller[MsCommission::RATE_LISTING]['flat'])
					? (float)$commissions_seller[MsCommission::RATE_LISTING]['flat']
					: 0;
				$percent_seller = !empty($commissions_seller[MsCommission::RATE_LISTING]['percent'])
					? (float)$commissions_seller[MsCommission::RATE_LISTING]['percent'] * (float)$price / 100
					: 0;

				$fee = $this->currency->format($flat_catalog + $percent_catalog + $flat_seller + $percent_seller, $this->config->get('config_currency'));

				// @todo 9.0: payment method from what - category or seller?
				if (!empty($commissions_seller[MsCommission::RATE_LISTING]['payment_method'])) {
					$payment_method = $commissions_seller[MsCommission::RATE_LISTING]['payment_method'];
				}

				$json['rate'] = sprintf($this->language->get('ms_account_product_listing_fee_combined'), $fee);

				break;
		}

		switch ($payment_method) {
			case MsPgPayment::METHOD_PG:
				$json['type'] = $this->language->get('ms_account_product_listing_pg');
				break;

			case MsPgPayment::METHOD_BALANCE:
			default:
				$json['type'] = $this->language->get('ms_account_product_listing_balance');
				break;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetFee() {
		$data = $this->request->get;

		if (!isset($data['price']) || !is_numeric($data['price']))
			$data['price'] = 0;

		$rates = $this->MsLoader->MsSeller->getFeeRates($this->customer->getId());

		$flat = !empty($rates[MsCommission::RATE_LISTING]['flat']) ? (float)$rates[MsCommission::RATE_LISTING]['flat'] : 0;
		$percent = !empty($rates[MsCommission::RATE_LISTING]['percent']) ? (float)$rates[MsCommission::RATE_LISTING]['percent'] : 0;

		echo $this->currency->format($flat + ($percent * $data['price'] / 100), $this->config->get('config_currency'));
	}

	public function jxGetCategoryFee() {
		$json = array();
		$data = $this->request->post;

		if (!isset($data['price']) && !is_numeric($data['price']))
			$data['price'] = 0;

		if(isset($data['category_id'])) {
			$commission_rates = $this->MsLoader->MsCategory->getOcCategoryFeeRates($data['category_id']);

			if(isset($commission_rates[MsCommission::RATE_LISTING])) {
				$flat = !empty($commission_rates[MsCommission::RATE_LISTING]['flat']) ? (float)$commission_rates[MsCommission::RATE_LISTING]['flat'] : 0;
				$percent = !empty($commission_rates[MsCommission::RATE_LISTING]['percent']) ? (float)$commission_rates[MsCommission::RATE_LISTING]['percent'] : 0;
				$calculated_fee = $this->currency->format($flat + ($percent * (float)$data['price'] / 100), $this->config->get('config_currency'));

				switch ($commission_rates[MsCommission::RATE_LISTING]['payment_method']) {
					case MsPgPayment::METHOD_PG:
						$ms_commission_payment_type = $this->language->get('ms_account_product_listing_pg');
						break;

					case MsPgPayment::METHOD_BALANCE:
					default:
						$ms_commission_payment_type = $this->language->get('ms_account_product_listing_balance');
						break;
				}
			} else {
				$calculated_fee = $this->currency->format(0, $this->config->get('config_currency'));
				$ms_commission_payment_type = '';
			}

			$json = array(
				'rate' => sprintf($data['msg'], $calculated_fee),
				'type' => $ms_commission_payment_type
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function jxSubmitProduct() {
		/** @var \MultiMerch\Module\MultiMerch $MultiMerchModule */
		$MultiMerchModule = $this->MsLoader->load('\MultiMerch\Module\MultiMerch');
		$serviceLocator = $MultiMerchModule->getServiceLocator();
		/** @var \MultiMerch\Mail\Transport\MultiMerchMail $mailTransport */
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$invoice_manager = new \MultiMerch\Core\Invoice\Manager($this->MsLoader->getRegistry());

		$this->load->model('localisation/language');
		//ob_start();
		$data = $this->request->post;
		$seller = $this->MsLoader->MsSeller->getSeller($this->customer->getId());

		// Convert array key. `Images` key is used for Pluploader universalization at other places
		if(isset($data['images'])) {
			$data['product_images'] = $data['images'];
			unset($data['images']);
		}

		// Convert array key. `Files` key is used for Pluploader universalization at other places
		if(isset($data['files'])) {
			$data['product_downloads'] = $data['files'];
			unset($data['files']);
		}

		if (!empty($data['product_id'])) {
			if ($this->MsLoader->MsProduct->productOwnedBySeller($data['product_id'], $this->customer->getId())) {
				$product = $this->MsLoader->MsProduct->getProduct($data['product_id']);
				$data['images'] = $this->MsLoader->MsProduct->getProductImages($data['product_id']);
			} else {
				return;
			}
		}

		// MSF variations: set min variation price and sum quantity of variations as main product values
		if (!empty($data['product_msf_variation'])) {
			$min_variation_price = $max_variation_price = current($data['product_msf_variation'])['price'];
			$total_variations_quantity = 0;

			foreach ($data['product_msf_variation'] as $msf_variation) {
				if ((float)$msf_variation['price'] < (float)$min_variation_price) {
					$min_variation_price = (float)$msf_variation['price'];
				}

				if ((float)$msf_variation['price'] > (float)$max_variation_price) {
					$max_variation_price = (float)$msf_variation['price'];
				}

				$total_variations_quantity += (int)$msf_variation['quantity'];
			}

			// Minimum product price (main price)
			$data['product_price'] = str_replace($this->language->get('thousand_point'), '', $min_variation_price);

			// Price for listing fee calculations
			$data['product_max_price'] = str_replace($this->language->get('thousand_point'), '', $max_variation_price);

			$data['product_quantity'] = $total_variations_quantity;
		} else {
			// Minimum product price (main price)
			$data['product_price'] = str_replace($this->language->get('thousand_point'), '', $data['product_price']);

			// Price for listing fee calculations
			$data['product_max_price'] = $data['product_price'];
		}

		$json = array();

		$validator = $this->MsLoader->MsValidator;

		// Only check default language for errors
		$i = 0;
		$defaultLanguageId = $this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'));

		
		
		$languages = $this->model_localisation_language->getLanguages();
		// validate primary language
		foreach ($languages as $language) {
			$language_id = $language['language_id'];
			$primary = true;
			if($language_id != $defaultLanguageId)
				$primary = false;
			// product name
			if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_product_name'),
					'value' => html_entity_decode($data['product_name'][$language_id])
				),
				array(
					!$primary ? array() : array('rule' => 'required'),
					array('rule' => 'max_len,100')
				)
			)) $json['errors']["product_name[$language_id]"] = $validator->get_errors();

			// @todo $description_length = $this->config->get('msconf_enable_rte') ? utf8_strlen(strip_tags(htmlspecialchars_decode($language['product_description'], ENT_COMPAT))) : utf8_strlen(htmlspecialchars_decode($language['product_description'], ENT_COMPAT));*
			// product description
			
			if ($this->config->get('msconf_allow_description_images') == 1){
				$excluded_tags = '<img>';
			} else{
				$excluded_tags = '';
			}
			
			if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_product_description'),
					// count product description length without tags (as video script tag)
					'value' => strip_tags(htmlspecialchars_decode($data['product_description'][$language_id]), $excluded_tags)
				),
			
				array(
					!$primary ? array() : array('rule' => 'required'),
					array('rule' => 'max_len,10000')
				)
			)) $json['errors']["product_description[$language_id]"] = $validator->get_errors($language_id);
			
			if(!$validator->validate(array(
					'name' => $this->language->get('ms_account_product_attribute'),
					'value' => isset($data['product_attribute'][++$i]) ? $data['product_attribute'][$i]['product_attribute_description'][$language_id]['text'] : ''
				),
				array(
					array('rule' => 'max_len, 1000')
				)
			)) $json['errors']["product_attribute[$language_id]"] = $validator->get_errors();

			// strip disallowed tags in description
			if ($this->config->get('msconf_rte_whitelist') != '') {
				$allowed_tags = explode(",", $this->config->get('msconf_rte_whitelist'));
				$allowed_tags_ready = "";
				
				if ($this->config->get('msconf_allow_description_images') != 1){
					$img_i = array_search('img', $allowed_tags);
					unset($allowed_tags[$img_i]);
				}
				
				foreach ($allowed_tags as $tag) {
					$allowed_tags_ready .= "<" . trim($tag) . ">";
				}
				$data['product_description'][$language_id] = htmlspecialchars(strip_tags(htmlspecialchars_decode($data['product_description'][$language_id], ENT_COMPAT), $allowed_tags_ready), ENT_COMPAT, 'UTF-8');
			}

			// price
			if(in_array('price', $this->config->get('msconf_product_included_fields')) && !isset($data['product_msf_variation'])) {
				if(!$validator->validate(array(
					'name' => $this->language->get('ms_account_product_price'),
					'value' => $data['product_price']
				),
					array(
						array('rule' => 'required'),
						array('rule' => 'numeric')
					)
				)) $json['errors']["product_price"] = $validator->get_errors();
			}

			// tags
			if(isset($data['product_tags'])) {
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_product_tags'),
					'value' => $data['product_tags'][$language_id]
				),
					array(
						array('rule' => 'max_len,100')
					)
				)) $json['errors']["product_tags[$language_id]"] = $validator->get_errors();
			}

			//copy fields data from main language
			if(!$primary) {
				if (empty($data['product_name'][$language_id])) $data['product_name'][$language_id] = $data['product_name'][$defaultLanguageId];
				if (empty($data['product_description'][$language_id])) $data['product_description'][$language_id] = $data['product_description'][$defaultLanguageId];
				if (isset($data['product_tags']) && empty($data['product_tags'][$language_id])) $data['product_tags'][$language_id] = $data['product_tags'][$defaultLanguageId];
				if(isset($data['product_attribute'])) {
					foreach($data['product_attribute'] as $key => $attribute) {
						if($attribute != reset($data['product_attribute'])){
							if(empty($attribute['product_attribute_description'][$language_id]['text']))
								$data['product_attribute'][$key]['product_attribute_description'][$language_id] = $attribute['product_attribute_description'][$defaultLanguageId];
						}
					}
				}
			}
			
			
		}
		
		if (
			!empty($data['product_id']) && 
			$this->config->get('msconf_allow_description_images') == 1 && 
			$this->config->get('msconf_description_images_type') == 'upload'
		) {
			
			$all_descriptions_content = implode(' ', $data['product_description']);
			$this->MsLoader->MsFile->checkDescriptionImages($all_descriptions_content, $data['product_id']);
		}

		// @todo copy to secondary languages if not empty

		if (!isset($data['product_msf_variation'])) {
			if ((float)$data['product_price'] == 0) {
				if (!is_numeric($data['product_price'])) {
					$json['errors']['product_price'] = $this->language->get('ms_error_product_price_invalid');
				} else if ($this->config->get('msconf_allow_free_products') == 0) {
					$json['errors']['product_price'] = $this->language->get('ms_error_product_price_empty');
				}
			} else if ((float)$data['product_price'] < (float)$this->config->get('msconf_minimum_product_price')) {
				$json['errors']['product_price'] = $this->language->get('ms_error_product_price_low');
			} else if (($this->config->get('msconf_maximum_product_price') != 0) && ((float)$data['product_price'] > (float)$this->config->get('msconf_maximum_product_price'))) {
				$json['errors']['product_price'] = $this->language->get('ms_error_product_price_high');
			}
		}

		// downloads
		$msconf_downloads_limits = $this->config->get('msconf_downloads_limits');
		if (!isset($data['product_downloads'])) {
			if ($msconf_downloads_limits[0] > 0) {
				$json['errors']['product_download'] = sprintf($this->language->get('ms_error_product_download_count'), $msconf_downloads_limits[0]);
			}
		} else {
			if ($msconf_downloads_limits[1] > 0 && count($data['product_downloads']) > $msconf_downloads_limits[1]) {
				$json['errors']['product_download'] = sprintf($this->language->get('ms_error_product_download_maximum'), $msconf_downloads_limits[1]);
			} else if ($msconf_downloads_limits[0] > 0 && count($data['product_downloads']) < $msconf_downloads_limits[0]) {
				$json['errors']['product_download'] = sprintf($this->language->get('ms_error_product_download_count'), $msconf_downloads_limits[0]);
			} else {
				foreach ($data['product_downloads'] as $key => $download) {
					if (!empty($download['filename'])) {
						if (!$this->MsLoader->MsFile->checkFileAgainstSession($download['filename'])) {
							$json['errors']['product_download'] = sprintf($this->language->get('ms_error_file_upload_error'), $download['filename'], $this->language->get('ms_file_cross_session_upload'));
						}
					} else if (!empty($download['download_id']) && !empty($product['product_id'])) {
						if (!$this->MsLoader->MsProduct->hasDownload($product['product_id'], $download['download_id'])) {
							$json['errors']['product_download'] = sprintf($this->language->get('ms_error_file_upload_error'), $download['filename'], $this->language->get('ms_file_cross_product_file'));
						}
					} else {
						unset($data['product_downloads'][$key]);
					}
					//str_replace($this->MsLoader->MsSeller->getNickname() . '_', '', $download);
					//$download = substr_replace($download, '.' . $this->MsLoader->MsSeller->getNickname() . '_', strpos($download,'.'), strlen('.'));
				}
			}
		}

		// images
		$msconf_images_limits = $this->config->get('msconf_images_limits');
		if (!isset($data['product_images'])) {
			if ($msconf_images_limits[0] > 0) {
				$json['errors']['product_image'] = sprintf($this->language->get('ms_error_product_image_count'), $msconf_images_limits[0]);
			}
		} else {
			if ($msconf_images_limits[1] > 0 && count($data['product_images']) > $msconf_images_limits[1]) {
				$json['errors']['product_image'] = sprintf($this->language->get('ms_error_product_image_maximum'), $msconf_images_limits[1]);
			} else if ($msconf_images_limits[0] > 0 && count($data['product_images']) < $msconf_images_limits[0]) {
				$json['errors']['product_image'] = sprintf($this->language->get('ms_error_product_image_count'), $msconf_images_limits[0]);
			} else {
				foreach ($data['product_images'] as $image) {
					if (!$this->MsLoader->MsFile->checkFileAgainstSession($image)) {
						$json['errors']['product_image'] = sprintf($this->language->get('ms_error_file_upload_error'), $image, $this->language->get('ms_file_cross_session_upload'));
					}
				}
				$data['product_thumbnail'] = array_shift($data['product_images']);
			}
		}

		// specials
		unset($data['product_specials'][0]); // Remove sample row
		if (isset($data['product_specials']) && is_array($data['product_specials'])) {
			foreach ($data['product_specials'] as $product_special) {
				if (empty($product_special['priority'])) {
					$json['errors']['specials'] = $this->language->get('ms_error_invalid_special_price_priority');
				}
				if ((!$this->MsLoader->MsHelper->isUnsignedFloat($product_special['price'])) || ((float)$product_special['price'] < (float)0)) {
					$json['errors']['specials'] = $this->language->get('ms_error_invalid_special_price_price');
				}
				if (empty($product_special['date_start']) || empty($product_special['date_end'])) {
					$json['errors']['specials'] = $this->language->get('ms_error_invalid_special_price_dates');
				}
			}
		}

		// bulk discounts
		unset($data['product_discounts'][0]); // Remove sample row
		if (isset($data['product_discounts']) && is_array($data['product_discounts'])) {
			foreach ($data['product_discounts'] as $product_discount) {
				if (!isset($product_discount['priority']) || $product_discount['priority'] == null || $product_discount['priority'] == "") {
					$json['errors']['quantity_discounts'] = $this->language->get('ms_error_invalid_quantity_discount_priority');
				}
				if ((int)$product_discount['quantity'] < (int)2) {
					$json['errors']['quantity_discounts'] = $this->language->get('ms_error_invalid_quantity_discount_quantity');
				}
				if ((!$this->MsLoader->MsHelper->isUnsignedFloat($product_discount['price'])) || ((float)$product_discount['price'] < (float)0)) {
					$json['errors']['quantity_discounts'] = $this->language->get('ms_error_invalid_quantity_discount_price');
				}
				if (!isset($product_discount['date_start']) || ($product_discount['date_start'] == NULL) || (!isset($product_discount['date_end']) || $product_discount['date_end'] == NULL)) {
					$json['errors']['quantity_discounts'] = $this->language->get('ms_error_invalid_quantity_discount_dates');
				}
			}
		}

		// Store categories
		$product_oc_categories = array();
		foreach (array_keys($data) as $k) {
			if (preg_match('/^product_oc_category/', $k, $matches)) {
				$product_oc_categories[] = $data[$k];
				unset($data[$k]);
			}
		}
		
		$data['primary_ms_category']  = 0;
		$data['primary_oc_category']  = 0;

		if (empty($product_oc_categories)) {
			$json['errors']["oc_category_error_holder"] = $this->language->get('ms_account_product_category_select');
		}

		if (!empty($product_oc_categories)) {
			$data['product_category'] = array();

			foreach ($product_oc_categories as $category_row) {
				$filtered_category_row = array_filter($category_row);

				if (!empty($filtered_category_row)) {
					array_push($data['product_category'], trim(end($filtered_category_row)));
				}
			}

			if (!$this->config->get('msconf_allow_multiple_categories') && count($data['product_category']) > 1) {
				$data['product_category'] = array($data['product_category'][0]);
			}

			if ($this->config->get('msconf_enforce_childmost_categories')) {
				foreach ($data['product_category'] as $row_id => $oc_category_id) {
					if (!empty($this->MsLoader->MsCategory->getChildCategoriesByOcCategoryId($oc_category_id))) {
						$json['errors']["product_oc_category_" . ($row_id + 1)] = $this->language->get('ms_account_product_category_select');
					}
				}
			}

			if (empty($product['primary_oc_category'])){
				$data['primary_oc_category'] = reset($data['product_category']);
			} else {
				// if primary category from POST != primary cat from product - lets refresh it again
				if ($product['primary_oc_category'] != reset($data['product_category'])){
					$data['primary_oc_category'] = reset($data['product_category']);
				} else {
					$data['primary_oc_category'] = $product['primary_oc_category'];
				}
			}
		}

	
		// Seller categories
		
		$product_ms_categories = array();
		foreach (array_keys($data) as $k) {
			if (preg_match('/^product_ms_category/', $k, $matches)) {
				$product_ms_categories[] = $data[$k];
				unset($data[$k]);
			}
		}

		if (!empty($product_ms_categories)) {
			$data['product_ms_category'] = array();
			foreach ($product_ms_categories as $category_row) {
				$filtered_category_row = array_filter($category_row);

				if (!empty($filtered_category_row)) {
					array_push($data['product_ms_category'], trim(end($filtered_category_row)));
				}
			}

			if (!$this->config->get('msconf_allow_multiple_categories') && count($data['product_ms_category']) > 1) {
				$data['product_ms_category'] = array($data['product_ms_category'][0]);
			}

			if ($this->config->get('msconf_enforce_childmost_categories')) {
				foreach ($data['product_ms_category'] as $row_id => $ms_category_id) {
					if (!empty($this->MsLoader->MsCategory->getChildCategoriesByCategoryId($ms_category_id))) {
						$json['errors']["product_ms_category_" . ($row_id + 1)] = $this->language->get('ms_account_product_category_select');
					}
				}
			}
			
			if (empty($product['primary_ms_category'])){
				$data['primary_ms_category'] = reset($data['product_ms_category']);
			} else{
				// if primary category absent in post - its changed or removed, we should update our seller primary cat 
				if ($product['primary_ms_category'] != reset($data['product_ms_category'])){
					$data['primary_ms_category'] = reset($data['product_ms_category']);
				} else{
					$data['primary_ms_category'] = $product['primary_ms_category'];
				}
			}
		}

		// model
		if (in_array('model', $this->config->get('msconf_product_included_fields'))) {
			if (empty($data['product_model'])) {
				$json['errors']['product_model'] = $this->language->get('ms_error_product_model_empty');
			} else if (utf8_strlen($data['product_model']) < 4 || utf8_strlen($data['product_model']) > 64) {
				$json['errors']['product_model'] = sprintf($this->language->get('ms_error_product_model_length'), 4, 64);
			}
		}

		// options
		//unset($data['product_option'][0]); // Remove sample row		

		// shipping
		if (isset($data['shipping'])) {
			try {
				$this->load->controller('multimerch/shipping/validatePerProductFormData', $data['shipping']);
			} catch (\MultiMerch\Module\Errors\FormData $e) {
				$json['errors']['shipping'] = $e->getMessage();
			}
		} elseif (empty($data['product_is_digital']) && (int)$this->config->get('msconf_shipping_type') === \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT) {
			$json['errors'][] = $this->language->get('ms_shipping_error_rules_required');
		}

		$data['product_enable_shipping'] = (0 === (int)$this->config->get('msconf_shipping_type')) ? 0 : 1;

		// quantities
		$data['product_quantity'] = isset($data['product_quantity']) ? (int)$data['product_quantity'] : 0;

		// minimum order quantity
		$data['minimum'] = isset($data['minimum']) ? (int)$data['minimum'] : 1;

		if (isset($data['product_name'][$this->config->get('config_language_id')])) {
			$product_name = $data['product_name'][$this->config->get('config_language_id')];
			$src_string = (is_array($this->config->get('msconf_product_included_fields')) && in_array('seoURL', $this->config->get('msconf_product_included_fields')) && (isset($data['keyword']) && $data['keyword'])) ? $data['keyword'] : $product_name;

			//test utf8 string: öäüßйȝîûηыეமிᚉ⠛
			$data['keyword'] = $this->MsLoader->MsHelper->slugify($src_string);
		}

		// custom fields
		if (isset($data['product_cf']) && $this->config->get('msconf_ms_custom_field_enabled')) {
			foreach ($data['product_cf'] as $custom_field_id => $ms_cf) {
				$ms_custom_field_data = $this->MsLoader->MsCustomField->getCustomFields(['custom_field_id' => $custom_field_id, 'single' => 1]);

				$key = 'value';
				if (in_array((string)$ms_custom_field_data['type'], ['select', 'radio', 'checkbox']))
					$key = 'custom_field_value_id';

				// Validation
				if (!empty($ms_cf['required'])) {
					if (empty($ms_cf[$key])) {
						$json['errors']["product_cf[$custom_field_id][$key][]"] = sprintf($this->language->get('ms_account_product_error_field_required'), $ms_custom_field_data['name']);
					} else {
						foreach($ms_cf[$key] as $value) {
							if (!$value)
								$json['errors']["product_cf[$custom_field_id][$key][]"] = sprintf($this->language->get('ms_account_product_error_field_required'), $ms_custom_field_data['name']);
						}
					}
				}

				$regex_validation = $this->MsLoader->MsCustomField->getCustomFieldValidation($custom_field_id);
				if (!empty($regex_validation)) {
					foreach ($ms_cf[$key] as $value) {
						if (!filter_var($value, FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => $regex_validation]])) {
							$json['errors']["product_cf[$custom_field_id][$key][]"] = sprintf($this->language->get('ms_account_product_error_field_validation'), $ms_custom_field_data['name'], $regex_validation);
						}
					}
				}
			}
		}

		// MSF product attributes
		if (isset($data['product_msf_attribute'])) {
			foreach ($data['product_msf_attribute'] as $msf_attribute_id => $values) {
				$msf_attribute = $this->MsLoader->MsfAttribute->get($msf_attribute_id);

				if (!empty($values['required'])) {
					if ('text' === $msf_attribute['type'] && empty($values['text_values'][$this->config->get('config_language_id')])) {
						$json['errors']["product_msf_attribute[$msf_attribute_id][text_values][{$this->config->get('config_language_id')}]"] = sprintf($this->language->get('ms_account_product_error_field_required'), $msf_attribute['name']);
					} elseif ('text' !== $msf_attribute['type'] && empty($values['id_values'])) {
						$json['errors']["product_msf_attribute[$msf_attribute_id][id_values][]"] = sprintf($this->language->get('ms_account_product_error_field_required'), $msf_attribute['name']);
					}
				}
			}
		}

		// MSF product variations
		if (!empty($data['msf_variation_required'])) {
			foreach ($data['msf_variation_required'] as $msf_variation_id) {
				if (empty($data['msf_variation_selected'][$msf_variation_id])) {
					$json['errors']["msf_variation_selected[$msf_variation_id][]"] = sprintf(
						$this->language->get('ms_account_product_msf_variations_not_selected'),
						$this->MsLoader->MsfVariation->getName($msf_variation_id)
					);
				}
			}
		}

		if (isset($data['product_msf_variation'])) {
			foreach ($data['product_msf_variation'] as $matrix_key => $matrix_row) {
				if (isset($matrix_row['status']) && !$matrix_row['status']) {
					continue;
				}

				$variation_values = [];

				$key_parts = explode('_', $matrix_key);
				foreach ($key_parts as $key_part) {
					list($msf_variation_id, $msf_variation_value_id) = explode('-', $key_part);

					$variation_values[] = sprintf(
						'%s - %s',
						$this->MsLoader->MsfVariation->getName($msf_variation_id),
						$this->MsLoader->MsfVariation->getValueName($msf_variation_value_id)
					);
				}

				if ((float)$matrix_row['price'] == 0) {
					if (!is_numeric($matrix_row['price'])) {
						$json['errors']["product_msf_variation[$matrix_key][price]"] = sprintf($this->language->get('ms_account_product_msf_variation_error_price_invalid'), $variation_values ? ("(" . implode(', ', $variation_values) . ")") : "");
					} elseif (!$this->config->get('msconf_allow_free_products')) {
						$json['errors']["product_msf_variation[$matrix_key][price]"] = sprintf($this->language->get('ms_account_product_msf_variation_error_price_empty'), $variation_values ? ("(" . implode(', ', $variation_values) . ")") : "");
					}
				} elseif ((float)$matrix_row['price'] < (float)$this->config->get('msconf_minimum_product_price')) {
					$json['errors']["product_msf_variation[$matrix_key][price]"] = sprintf($this->language->get('ms_account_product_msf_variation_error_price_price_low'), $variation_values ? ("(" . implode(', ', $variation_values) . ")") : "");
				} elseif (($this->config->get('msconf_maximum_product_price') != 0) && ((float)$matrix_row['price'] > (float)$this->config->get('msconf_maximum_product_price'))) {
					$json['errors']["product_msf_variation[$matrix_key][price]"] = sprintf($this->language->get('ms_account_product_msf_variation_error_price_price_high'), $variation_values ? ("(" . implode(', ', $variation_values) . ")") : "");
				}

				if (!isset($matrix_row['quantity']) || !is_numeric($matrix_row['quantity'])) {
					$json['errors']["product_msf_variation[$matrix_key][quantity]"] = sprintf($this->language->get('ms_account_product_msf_variation_error_quantity'), $variation_values ? ("(" . implode(', ', $variation_values) . ")") : "");
				}
			}
		}

		// return if errors
		if (!empty($json['errors'])) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			return;
		}

		// post-validation

		// set product status
		switch ($seller['product_validation']) {
			case MsProduct::MS_PRODUCT_VALIDATION_APPROVAL:
				$data['enabled'] = 0;
				$data['product_status'] = MsProduct::STATUS_INACTIVE;
				$data['product_approved'] = 0;

				if (!isset($data['product_id']) || empty($data['product_id'])) {
					$MailProductAwaitingModeration = $serviceLocator->get('MailProductAwaitingModeration', false)
						->setTo($this->registry->get('customer')->getEmail())
						->setData(array(
							'addressee' => $this->registry->get('customer')->getFirstname(),
							'product_name' => $data['product_name'][$defaultLanguageId],
						));
					$mails->add($MailProductAwaitingModeration);

                    $MailAdminNewProductAwaitingModeration = $serviceLocator->get('MailAdminNewProductAwaitingModeration', false)
                        ->setTo($MultiMerchModule->getNotificationEmail())
                        ->setData(array(
                            //'addressee' => $this->registry->get('customer')->getFirstname(),
                            'product_name' => $data['product_name'][$defaultLanguageId],
                            'message' => isset($data['product_message']) ? $data['product_message'] : '',
                        ));
                    $mails->add($MailAdminNewProductAwaitingModeration);
				} else {
					$MailProductAwaitingModeration = $serviceLocator->get('MailProductAwaitingModeration', false)
						->setTo($this->registry->get('customer')->getEmail())
						->setData(array(
							'addressee' => $this->registry->get('customer')->getFirstname(),
							'product_name' => $data['product_name'][$defaultLanguageId],
						));
					$mails->add($MailProductAwaitingModeration);

                    $MailAdminEditProductAwaitingModeration = $serviceLocator->get('MailAdminEditProductAwaitingModeration', false)
                        ->setTo($MultiMerchModule->getNotificationEmail())
                        ->setData(array(
                            //'addressee' => $this->registry->get('customer')->getFirstname(),
                            'product_name' => $data['product_name'][$defaultLanguageId],
                            'message' => isset($data['product_message']) ? $data['product_message'] : '',
                        ));
                    $mails->add($MailAdminEditProductAwaitingModeration);
				}
				break;

			case MsProduct::MS_PRODUCT_VALIDATION_NONE:
			default:
				$data['enabled'] = 1;
				$data['product_status'] = MsProduct::STATUS_ACTIVE;
				$data['product_approved'] = 1;

				if (!isset($data['product_id']) || empty($data['product_id'])) {
					$MailAdminProductCreated = $serviceLocator->get('MailAdminProductCreated', false)
						->setTo($MultiMerchModule->getNotificationEmail())
						->setData(array(
							//'addressee' => $this->registry->get('customer')->getFirstname(),
							'product_name' => $data['product_name'][$defaultLanguageId],
						));
					$mails->add($MailAdminProductCreated);
				} else {
					// product edited mail if needed
				}
				break;
		}

		/*if ($mails->count()) {
			$mailTransport->sendMails($mails);
		}*/

		// Get commissions
		$fee = 0;
		$payment_method = MsPgPayment::METHOD_BALANCE;

		if ($this->config->get('msconf_listing_fee_enabled')) {
			switch ($this->config->get('msconf_sale_fee_calculation_mode')) {
				case 2:
					if (1 === (int)$this->config->get('msconf_fee_priority')) {
						// If category commission is in prior
						$commissions = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates($data['product_category'], MsCommission::RATE_LISTING, $data['product_max_price']);
					} else {
						// If vendor commission is in prior or no product categories are set
						$commissions = $this->MsLoader->MsSeller->getFeeRates($this->customer->getId());
					}

					$flat = !empty($commissions[MsCommission::RATE_LISTING]['flat'])
						? (float)$commissions[MsCommission::RATE_LISTING]['flat']
						: 0;
					$percent = !empty($commissions[MsCommission::RATE_LISTING]['percent'])
						? (float)$commissions[MsCommission::RATE_LISTING]['percent'] * (float)$data['product_max_price'] / 100
						: 0;

					$fee = $flat + $percent;

					if (!empty($commissions[MsCommission::RATE_LISTING]['payment_method'])) {
						$payment_method = $commissions[MsCommission::RATE_LISTING]['payment_method'];
					}

					break;

				case 1:
				default:
					$commissions_catalog = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates($data['product_category'], MsCommission::RATE_LISTING, $data['product_max_price']);

					$flat_catalog = !empty($commissions_catalog[MsCommission::RATE_LISTING]['flat'])
						? (float)$commissions_catalog[MsCommission::RATE_LISTING]['flat']
						: 0;
					$percent_catalog = !empty($commissions_catalog[MsCommission::RATE_LISTING]['percent'])
						? (float)$commissions_catalog[MsCommission::RATE_LISTING]['percent'] * (float)$data['product_max_price'] / 100
						: 0;

					$commissions_seller = $this->MsLoader->MsSeller->getFeeRates($this->customer->getId());

					$flat_seller = !empty($commissions_seller[MsCommission::RATE_LISTING]['flat'])
						? (float)$commissions_seller[MsCommission::RATE_LISTING]['flat']
						: 0;
					$percent_seller = !empty($commissions_seller[MsCommission::RATE_LISTING]['percent'])
						? (float)$commissions_seller[MsCommission::RATE_LISTING]['percent'] * (float)$data['product_max_price'] / 100
						: 0;

					$fee = $flat_catalog + $percent_catalog + $flat_seller + $percent_seller;

					// @todo 9.0: payment method from what - category or seller?
					if (!empty($commissions_seller[MsCommission::RATE_LISTING]['payment_method'])) {
						$payment_method = $commissions_seller[MsCommission::RATE_LISTING]['payment_method'];
					}

					break;
			}
		}

		// If product is digital delete all MM shipping information about it
		if (!empty($data['product_is_digital']) && isset($data['shipping'])) {
			unset($data['shipping']);
		}

		// finish
		if (!empty($data['product_id'])) {
			$product_id = $this->MsLoader->MsProduct->editProduct($data);

			// @todo 9.0: Create listing fee invoice or deduct balance, if:
			// @todo 9.0: - the primary category of existing product has been changed
			// @todo 9.0: - the price of existing product has been changed
			if (
				false && // @todo 9.0
				$fee > 0 &&
				isset($product['primary_oc_category']) && (int)$data['primary_oc_category'] !== (int)$product['primary_oc_category'] &&
				isset($product['product_price']) && (float)$data['product_price'] !== (float)$product['product_price'] // @todo 9.0: consider variations!
			) {
				switch($payment_method) {
					case MsPgPayment::METHOD_PG:
						// change status to unpaid
						$this->MsLoader->MsProduct->changeStatus($product_id, MsProduct::STATUS_UNPAID);

						$invoice_id = $invoice_manager->createProductListingInvoice($product_id, $this->customer->getId(), $fee);
						if (!$invoice_id)
							$this->ms_logger->error("Invoice for product {$product_id} listing is not created");

						break;

					case MsPgPayment::METHOD_BALANCE:
					default:
						// deduct from balance
						$this->MsLoader->MsBalance->addBalanceEntry($this->customer->getId(),
							array(
								'product_id' => $product_id,
								'balance_type' => MsBalance::MS_BALANCE_TYPE_LISTING,
								'amount' => -$fee,
								'description' => sprintf($this->language->get('ms_invoice_title_product_listing'), $this->url->link('seller/account-product/update', 'product_id=' . $product_id, 'SSL'), $data['product_name'][$defaultLanguageId])
							)
						);

						break;
				}
			}

			$this->ms_events->add(new MsEvent(array(
				'seller_id' => $this->customer->getId(),
				'event_type' => MsEvent::PRODUCT_MODIFIED,
				'data' => array('product_id' => $product_id)
			)));

			$this->session->data['success'] = $this->language->get('ms_success_product_updated');
		} else {
			$product_id = $this->MsLoader->MsProduct->saveProduct($data);

			if ($this->config->get('msconf_allow_description_images') == 1 &&  $this->config->get('msconf_description_images_type') == 'upload'){
				
				$new_description_array = $this->MsLoader->MsFile->moveDescriptionImages($data['product_description'], $product_id);
				
				if ($new_description_array !== false){
					$this->MsLoader->MsProduct->editProductDescription($product_id, $new_description_array);
				}
			}
			 
			// Create fee requests
			if ($fee > 0) {
				switch ($payment_method) {
					case MsPgPayment::METHOD_PG:
						// set product status to unpaid
						$this->MsLoader->MsProduct->changeStatus($product_id, MsProduct::STATUS_UNPAID);

						$invoice_id = $invoice_manager->createProductListingInvoice($product_id, $this->customer->getId(), $fee);
						if (!$invoice_id)
							$this->ms_logger->error("Invoice for product {$product_id} listing is not created");

						break;

					case MsPgPayment::METHOD_BALANCE:
					default:
						// deduct from balance
						$this->MsLoader->MsBalance->addBalanceEntry($this->customer->getId(),
							array(
								'product_id' => $product_id,
								'balance_type' => MsBalance::MS_BALANCE_TYPE_LISTING,
								'amount' => -$fee,
								'description' => sprintf($this->language->get('ms_invoice_title_product_listing'), $this->url->link('seller/account-product/update', 'product_id=' . $product_id, 'SSL'), $data['product_name'][$defaultLanguageId])
							)
						);

						break;
				}
			}

			$this->ms_events->add(new MsEvent(array(
				'seller_id' => $this->customer->getId(),
				'event_type' => MsEvent::PRODUCT_CREATED,
				'data' => array('product_id' => $product_id)
			)));

			$this->MsHooks->triggerAction('seller_created_product', [
				'producer' => "seller.{$this->customer->getId()}",
				'consumers' => ["admin.0"],
				'object' => [
					'type' => 'product',
					'id' => $product_id,
					'action' => 'created'
				]
			]);

			$this->session->data['success'] = $this->language->get('ms_success_product_created');
		}

		if($this->ms_events->count()) {
			$this->ms_event_manager->create($this->ms_events);
		}

		$json['redirect'] = $this->url->link('seller/account-product', '', 'SSL');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxRenderOptionValues() {
		$this->data['option'] = $this->MsLoader->MsOption->getOptions(
			array(
				'option_id' => 	$this->request->get['option_id'],
				'single' => 1
			)
		);
		
		$this->data['values'] = $this->MsLoader->MsOption->getOptionValues($this->request->get['option_id']);
		$this->data['option_index'] = 0;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form-options-values',array());
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function jxRenderProductOptions() {
		$this->load->model('catalog/product');
		$options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
		
		$output = '';
		if ($options) {
			$this->data['option_index'] = 0;
			foreach ($options as $o) {
				$this->data['option'] = $o;
				$this->data['product_option_values'] = $o['product_option_value'];
				$this->data['values'] = $this->MsLoader->MsOption->getOptionValues($o['option_id']);
				
				list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form-options-values',array());
				$output .= $this->load->view($template, array_merge($this->data, $children));

				$this->data['option_index']++;
			}
		}
	
		$this->response->setOutput($output);
	}
	
	public function jxAutocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/manufacturer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start' => 0,
				'limit' => 5
			);

			$results = $this->MsLoader->MsProduct->getManufacturers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'manufacturer_id' => $result['manufacturer_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function index() {
		// paypal listing payment confirmation
		if (isset($this->request->post['payment_status']) && strtolower($this->request->post['payment_status']) == 'completed') {
			$this->data['success'] = $this->language->get('ms_success_product_published');
		}

		if(!$this->_canCreateProduct($this->customer->getId())) {
			$this->data['product_number_limit_exceeded'] = $this->language->get('ms_error_slr_gr_product_number_limit_exceeded');
		}
		
		// Links
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		$this->data['link_create_product'] = $this->url->link('seller/account-product/create', '', 'SSL');

		// Title and friends
		$this->document->setTitle($this->language->get('ms_account_products_heading'));		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_products_breadcrumbs'),
				'href' => $this->url->link('seller/account-product', '', 'SSL'),
			)
		));

		$this->data['import_result'] =  '';
		if (isset($this->session->data['import_result']) AND $this->session->data['import_result']){
			$this->data['import_result'] =  $this->language->get('ms_import_text_results'). '<br />';
			foreach($this->session->data['import_result'] as $import_result){
				$this->data['import_result'].= $import_result['name'] . $import_result['value'] . '<br />';
			}
			unset($this->session->data['import_result']);
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	/**
	 * Checks permissions for create new product.
	 *
	 * @param	int		$seller_id	Seller id.
	 * @return	bool				True if seller can add new product, false if not.
	 */
	private function _canCreateProduct($seller_id) {
		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroupBySellerId($seller_id);
		$seller_group_id = isset($seller_group['seller_group']) ? $seller_group['seller_group'] : NULL;

		$total_products = $this->MsLoader->MsProduct->getTotalProducts(array(
			'seller_id' => $seller_id,
			'product_status' => array(MsProduct::STATUS_ACTIVE, MsProduct::STATUS_INACTIVE, MsProduct::STATUS_DISABLED, MsProduct::STATUS_UNPAID)
		));

		$slr_gr_settings = $this->MsLoader->MsSetting->getSellerGroupSettings(array('seller_group_id' => $seller_group_id));
		if(isset($slr_gr_settings['slr_gr_product_number_limit']) && $slr_gr_settings['slr_gr_product_number_limit'] !== '' && $total_products + 1 > (int)$slr_gr_settings['slr_gr_product_number_limit']) {
			return false;
		}

		return true;
	}
	
	private function _initForm() {
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('localisation/currency');
		$this->load->model('localisation/language');
		$this->load->model('account/customer_group');

		$this->document->addScript('catalog/view/javascript/multimerch/Sortable.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');
		$this->document->addScript('catalog/view/javascript/account-product-form.js');
		$this->document->addScript('catalog/view/javascript/multimerch/account-product-form-options.js');
		$this->document->addScript('catalog/view/javascript/multimerch/selectize/selectize.min.js');
		$this->document->addStyle('catalog/view/javascript/multimerch/selectize/selectize.bootstrap3.css');

		$this->MsLoader->MsHelper->addStyle('multimerch/flags');

		// rte
		if ($this->config->get('msconf_enable_rte')) {
			$this->document->addScript('catalog/view/javascript/multimerch/ckeditor/ckeditor.js');
		}

		$this->data['seller'] = $this->MsLoader->MsSeller->getSeller($this->customer->getId());
		$this->data['seller_group'] = $this->MsLoader->MsSellerGroup->getSellerGroup($this->data['seller']['ms.seller_group']);

		$this->data['customer_groups'] = $this->model_account_customer_group->getCustomerGroups();
		$this->data['hide_customer_groups'] = (count($this->data['customer_groups']) < 2) ? true : false;

		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		if ($product_id) $product_status = $this->MsLoader->MsProduct->getStatus($product_id);

		$this->data['salt'] = $this->MsLoader->MsSeller->getSalt($this->customer->getId());
		$this->data['date_available'] = date('Y-m-d', time());
		$this->data['tax_classes'] = $this->MsLoader->MsHelper->getTaxClasses();
		$this->data['stock_statuses'] = $this->MsLoader->MsHelper->getStockStatuses();

		// fix price delimiters
		$this->data['ms_account_product_price_note'] = sprintf($this->data['ms_account_product_price_note'], $this->currency->getSymbolLeft($this->config->get('config_currency')), $this->language->get('thousand_point'), $this->language->get('decimal_point'), $this->currency->getSymbolRight($this->config->get('config_currency')));

		// product_info
		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->MsLoader->MsProduct->getProduct($this->request->get['product_id']);
		}

		//stores
		$this->load->model('setting/store');
		$stores = $this->model_setting_store->getStores();
		if (isset($this->request->get['product_id'])){
			$product_stores = $this->MsLoader->MsProduct->getProductStores($this->request->get['product_id']);
		}else{
			$product_stores = array();
		}
		$product_stores_ids = array();
		foreach ($product_stores as $product_store){
			$product_stores_ids[$product_store['store_id']] = $product_store['product_id'];
		}
		//add default store
		$this->data['stores'][0] = array(
			'store_id' => 0,
			'name' => $this->config->get('config_name'),
			'available' => isset($product_stores_ids[0]) ? 1 : 0
		);
		//add other stores
		foreach ($stores as $store){
			$this->data['stores'][$store['store_id']] = array(
				'store_id' => $store['store_id'],
				'name' => $store['name'],
				'available' => isset($product_stores_ids[$store['store_id']]) ? 1 : 0
			);
		}

		// filters
		$filters = $this->MsLoader->MsFilter->getProductFilters($product_id);

		$this->data['product_filters'] = array();
		foreach ($filters as $filter_id) {
			$filter_info = $this->MsLoader->MsFilter->getFilter($filter_id);
			if ($filter_info) {
				$this->data['product_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name' => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}
		// related products
		if (isset($this->request->post['product_related'])) {
			$products = $this->request->post['product_related'];
		} elseif (isset($this->request->get['product_id'])) {
			$products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
		} else {
			$products = array();
		}

		$this->data['product_relateds'] = array();

		foreach ($products as $product) {

			if (!empty($product)) {
				$this->data['product_relateds'][] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name']
				);
			}
		}

		// Dimensions
		if (isset($this->request->post['weight'])) {
			$this->data['weight'] = $this->request->post['weight'];
		} elseif (!empty($product_info)) {
			$this->data['weight'] = $product_info['weight'];
		} else {
			$this->data['weight'] = '';
		}

		$this->load->model('localisation/weight_class');

		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['weight_class_id'])) {
			$this->data['weight_class_id'] = $this->request->post['weight_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['weight_class_id'] = $product_info['weight_class_id'];
		} else {
			$this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
		}

		if (isset($this->request->post['length'])) {
			$this->data['length'] = $this->request->post['length'];
		} elseif (!empty($product_info)) {
			$this->data['length'] = $product_info['length'];
		} else {
			$this->data['length'] = '';
		}

		if (isset($this->request->post['width'])) {
			$this->data['width'] = $this->request->post['width'];
		} elseif (!empty($product_info)) {
			$this->data['width'] = $product_info['width'];
		} else {
			$this->data['width'] = '';
		}

		if (isset($this->request->post['height'])) {
			$this->data['height'] = $this->request->post['height'];
		} elseif (!empty($product_info)) {
			$this->data['height'] = $product_info['height'];
		} else {
			$this->data['height'] = '';
		}

		$this->load->model('localisation/length_class');

		$this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

		if (isset($this->request->post['length_class_id'])) {
			$this->data['length_class_id'] = $this->request->post['length_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['length_class_id'] = $product_info['length_class_id'];
		} else {
			$this->data['length_class_id'] = $this->config->get('config_length_class_id');
		}

		// Attributes
		$product_attributes = $this->MsLoader->MsAttribute->ocGetProductAttributes($product_id);

		$this->data['product_attributes'] = array();

		foreach ($product_attributes as $product_attribute) {
			$attribute_info = $this->MsLoader->MsAttribute->ocGetAttribute($product_attribute['attribute_id']);

			if ($attribute_info) {
				$this->data['product_attributes'][] = array(
					'attribute_id'                  => $product_attribute['attribute_id'],
					'name'                          => $attribute_info['name'],
					'product_attribute_description' => $product_attribute['product_attribute_description']
				);
			}
		}

		$this->data['product_options'] = array();

		if ($product_id) {
			$options = $this->model_catalog_product->getProductOptions($product_id);

			if ($options) {
				$d = $this->data;
				$d['option_index'] = 0;
				foreach ($options as &$o) {
					$d['option'] = $o;
					$d['product_option_values'] = $o['product_option_value'];
					$d['values'] = $this->MsLoader->MsOption->getOptionValues($o['option_id']);

					list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form-options-values',array());
					$o['val'] = $this->load->view($template, array_merge($d, $children));

					$d['option_index']++;
				}
			}

			$this->data['product_options'] = $options;
		}

		$this->data['enable_quantities'] = 0;
		if ($this->config->get('msconf_enable_quantities') == 1) {
			$this->data['enable_quantities'] = 1;
		} else if ($this->config->get('msconf_enable_quantities') == 2) {
			// shipping-dependent
			if ($this->config->get('msconf_enable_shipping') == 1) {
				$this->data['enable_quantities'] = 1;
			}
		}

		// Shipping
		$this->data['ms_shipping'] = $this->load->controller('multimerch/shipping/renderProductFormShipping', $product_id);

		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['msconf_allow_multiple_categories'] = $this->config->get('msconf_allow_multiple_categories');
		$this->data['msconf_images_limits'] = $this->config->get('msconf_images_limits');
		$this->data['msconf_downloads_limits'] = $this->config->get('msconf_downloads_limits');
		$this->data['msconf_enable_quantities'] = $this->config->get('msconf_enable_quantities');
        $this->data['ms_account_product_download_note'] = sprintf($this->language->get('ms_allowed_extensions'), $this->config->get('msconf_allowed_download_types'));
		$this->data['ms_account_product_image_note'] = sprintf($this->language->get('ms_allowed_extensions'), $this->config->get('msconf_allowed_image_types'));
		$this->data['back'] = $this->url->link('seller/account-product', '', 'SSL');

		// Custom fields
		if ($this->config->get('msconf_ms_custom_field_enabled')) {
			$ms_custom_field_groups = $this->MsLoader->MsCustomField->getCustomFieldGroups(
				array(
					'location_id' => MsCustomField::LOCATION_PRODUCT,
					'status' => MsCustomField::STATUS_ACTIVE
				),
				array(
					'order_by' => 'mscfg.sort_order',
					'order_way' => 'ASC'
				)
			);

			$ms_custom_fields_to_group = [];
			foreach($ms_custom_field_groups as $ms_cfg) {
				$ms_custom_fields_to_group[$ms_cfg['name']] = $this->MsLoader->MsCustomField->getCustomFields(
					array(
						'custom_field_group_id' => $ms_cfg['custom_field_group_id'],
						'status' => MsCustomField::STATUS_ACTIVE
					),
					array(
						'order_by' => 'mscf.sort_order',
						'order_way' => 'ASC'
					)
				);
			}

			$this->data['ms_custom_fields'] = $ms_custom_fields_to_group;
		}

		if (isset($this->request->get['product_id'])) {
			$ms_product_custom_fields_data = $this->MsLoader->MsCustomField->getProductCustomFields(['product_id' => $this->request->get['product_id']]);

			$ms_product_custom_fields = [];
			foreach($ms_product_custom_fields_data as $ms_cf) {
				$type = (string)$this->MsLoader->MsCustomField->getCustomFieldType($ms_cf['custom_field_id']);

				if ('file' === $type) {
					$download_id = $ms_cf['value'];

					if ($download_id) {
						$download = $this->MsLoader->MsHelper->getOcDownload($download_id);
						if (isset($download['mask'])) {
							$ms_product_custom_fields[$ms_cf['custom_field_id']][] = [
								'download_id' => $download_id,
								'filename' => $download['mask']
							];
						}
					}
				} elseif (in_array($type, array('text', 'textarea', 'date', 'time', 'datetime')) && $ms_cf['value']) {
					$ms_product_custom_fields[$ms_cf['custom_field_id']] = $ms_cf['value'];
				} elseif ($ms_cf['custom_field_value_id']) {
					$ms_product_custom_fields[$ms_cf['custom_field_id']][] = $ms_cf['custom_field_value_id'];
				}
			}

			$this->data['ms_product_custom_fields'] = $ms_product_custom_fields;
		}

		$this->data['ms_account_product_cf_file_allowed_ext'] = sprintf($this->language->get('ms_account_product_cf_file_allowed_ext'), $this->config->get('msconf_msg_allowed_file_types'));

		// MSF variations
		// Check whether at least one variation is created by admin (regardless available it or not)
		$data['msf_variations'] = $this->MsLoader->MsfVariation->getList();

		// Title and friends
		$this->document->setTitle($this->language->get('ms_account_products_heading'));
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_products_breadcrumbs'),
				'href' => $this->url->link('seller/account-product', '', 'SSL'),
			)
		));
	}

	public function create() {
		$this->_initForm();
		$this->data['product_attributes'] = FALSE;
		$this->data['product'] = FALSE;
		$this->data['heading'] = $this->language->get('ms_account_newproduct_heading');
		$this->document->setTitle($this->language->get('ms_account_newproduct_heading'));

		$this->data['seller_id'] = $this->customer->getId();

		if (!$this->_canCreateProduct($this->data['seller_id'])){
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function update() {
		$product_id = isset($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : 0;
		$this->data['seller_id'] = $seller_id = $this->customer->getId();
		
		if ($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id)) {
    		$product = $this->MsLoader->MsProduct->getProduct($product_id);
		} else {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		if ($product['product_status'] == MsProduct::STATUS_DELETED){
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$this->_initForm();

		$this->load->model('tool/image');

		// name, description, metas
		foreach ($product['languages'] as $id => $l) {
			$product['name'][$id] = $l['name'];
			$product['description'][$id] = $l['description'];

			$product['tags'][$id] = $l['tags'];
			$product['product_meta_keywords'][$id] = $l['meta_keyword'];
			$product['product_meta_description'][$id] = $l['meta_description'];
			$product['product_meta_title'][$id] = $l['meta_title'];
		}

		// Store categories
		$p_oc_cat_ids = explode(',', $this->MsLoader->MsProduct->getProductOcCategories($product_id));
		foreach ($p_oc_cat_ids as $cat_id) {
			$oc_cat_path = $this->MsLoader->MsCategory->getOcCategoryPath($cat_id);
			$this->categoryPathToArray($oc_cat_path, $product['oc_categories']);
		}
		
		if (!empty($product['oc_categories'])) {
			foreach ($product['oc_categories'] as $row_id => &$oc_categories) {
				foreach ($oc_categories as $oc_parent_id => $oc_category_id) {
					$oc_categories[$oc_parent_id] = array(
						'category_id' => $oc_category_id,
						'name' => $this->MsLoader->MsCategory->getOcCategoryName($oc_category_id, $this->config->get('config_language_id'))
					);
				}
			}
			// move oc primary cat to the beginning
			$product['oc_categories'] = $this->MsLoader->MsHelper->makePrimaryOcCategoryFirst($product['oc_categories'], $product['primary_oc_category']);
		}
		

		// Seller categories
		$p_ms_cat_ids = explode(',', $this->MsLoader->MsProduct->getProductMsCategories($product_id));
		foreach ($p_ms_cat_ids as $cat_id) {
			$ms_cat_path = $this->MsLoader->MsCategory->getMsCategoryPath($cat_id);
			$this->categoryPathToArray($ms_cat_path, $product['ms_categories']);
		}

		if (!empty($product['ms_categories'])) {
			foreach ($product['ms_categories'] as $row_id => &$ms_categories) {
				foreach ($ms_categories as $ms_parent_id => $ms_category_id) {
					$ms_categories[$ms_parent_id] = array(
						'category_id' => $ms_category_id,
						'name' => $this->MsLoader->MsCategory->getMsCategoryName($ms_category_id, $this->config->get('config_language_id'))
					);
				}
			}
			
			// move seller's primary cat always to the first position
			foreach ($product['ms_categories'] as $row_id => &$ms_categories) {
				if (end($ms_categories)['category_id'] == $product['primary_ms_category'] && $row_id > 0){
					unset($product['ms_categories'][$row_id]);
					array_unshift($product['ms_categories'], $ms_categories);
					break;
				}
			}
		}

		// price @todo formatting
		$currencies = $this->model_localisation_currency->getCurrencies();
  		$decimal_place = $currencies[$this->config->get('config_currency')]['decimal_place'];
		$product['price'] = $this->MsLoader->MsHelper->trueCurrencyFormat($product['price']);

		// specials
		$product['specials'] = $this->MsLoader->MsProduct->getProductSpecials($product_id);
		foreach ($product['specials'] as &$special) {
			$special['price'] = $this->MsLoader->MsHelper->trueCurrencyFormat($special['price']);
		}

		// bulk discounts
		$product['discounts'] = $this->MsLoader->MsProduct->getProductDiscounts($product_id);
		foreach ($product['discounts'] as &$discount) {
			$discount['price'] = $this->MsLoader->MsHelper->trueCurrencyFormat($discount['price']);
		}

		// tax class
		$product['tax_class_id'] = isset($product['tax_class_id']) ? $product['tax_class_id'] : 0;

		// stock status
		$product['stock_status_id'] = isset($product['stock_status_id']) ? $product['stock_status_id'] : (int)$this->MsLoader->MsProduct->getDefaultStockStatus();

		// date available
		$product['date_available'] = isset($product['date_available']) ? date('Y-m-d', strtotime($product['date_available'])) : '';

		// thumbnail
		if (!empty($product['thumbnail'])) {
			$product['images'][] = array(
				'name' => $product['thumbnail'],
				'thumb' => $this->model_tool_image->resize($product['thumbnail'], $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'))
			);
			
			if (!in_array($product['thumbnail'], $this->session->data['multiseller']['files'])) 	$this->session->data['multiseller']['files'][] = $product['thumbnail'];
		}

		// images
		$images = $this->MsLoader->MsProduct->getProductImages($product_id);
		foreach ($images as $image) {
			$product['images'][] = array(
				'name' => $image['image'],
				'thumb' => $this->model_tool_image->resize($image['image'], $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'))
			);
			
			if (!in_array($image['image'], $this->session->data['multiseller']['files'])) $this->session->data['multiseller']['files'][] = $image['image'];
		}

		// downloads
		$downloads = $this->MsLoader->MsProduct->getProductDownloads($product_id);
		$product['downloads'] = array();
		foreach ($downloads as $download) {
			$product['downloads'][] = array(
				'name' => $download['mask'],
				'src' => $download['filename'],
				'href' => $this->url->link('seller/account-product/download', 'download_id=' . $download['download_id'] . '&product_id=' . $product_id, 'SSL'),
				'id' => $download['download_id'],
			);
			
			if (!in_array($download['filename'], $this->session->data['multiseller']['files']))
				$this->session->data['multiseller']['files'][] = $download['filename'];
		}

		// manufacturer
		if(isset($product['manufacturer_id'])){
			$product['manufacturer_id'] = (int)$product['manufacturer_id'];
			$this->load->model('catalog/manufacturer');
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
			if ($manufacturer_info) {
				$product['manufacturer'] = $manufacturer_info['name'];
			} else {
				$product['manufacturer'] = '';
			}
		} else {
			$product['manufacturer_id'] = 0;
			$product['manufacturer'] = '';
		};

		// shipping
		$product['oc_shipping'] = $product['shipping'];

		// assign main data
		$this->data['product'] = $product;

		// set page heading
		$this->data['heading'] = $this->language->get('ms_account_editproduct_heading');
		$this->document->setTitle($this->language->get('ms_account_editproduct_heading'));

		// render
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-product-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function delete() {
		$product_id = (int)$this->request->get['product_id'];
		$seller_id = (int)$this->customer->getId();
		
		if ($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id)) {
			$this->MsLoader->MsProduct->changeStatus($product_id, MsProduct::STATUS_DELETED);

			// delete unpaid listing fee requests for this product
			$invoices = $this->MsLoader->MsInvoice->get([
				'type' => 'listing',
				'object_type' => 'product',
				'object_id' => $product_id,
				'recipient_type' => 'seller',
				'recipient_id' => $seller_id,
				'status' => \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID
			]);

			if (!empty($invoices)) {
				foreach ($invoices as $invoice) {
					$this->MsLoader->MsInvoice->delete($invoice->getInvoiceId());
				}
			}

			$this->session->data['success'] = $this->language->get('ms_success_product_deleted');
		}
		
		$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
	}
	
	public function publish() {
		$product_id = (int)$this->request->get['product_id'];
		$seller_id = (int)$this->customer->getId();
		
		if ($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id)
			&& ($this->MsLoader->MsProduct->getStatus($product_id) == MsProduct::STATUS_INACTIVE OR $this->MsLoader->MsProduct->getStatus($product_id) == MsProduct::STATUS_IMPORTED)) {
			$this->MsLoader->MsProduct->changeStatus($product_id, MsProduct::STATUS_ACTIVE);
			$this->session->data['success'] = $this->language->get('ms_success_product_published');
		}
		
		$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
	}	
	
	public function unpublish() {
		$product_id = (int)$this->request->get['product_id'];
		$seller_id = (int)$this->customer->getId();
		
		if ($this->MsLoader->MsProduct->productOwnedBySeller($product_id, $seller_id)
			&& $this->MsLoader->MsProduct->getStatus($product_id) == MsProduct::STATUS_ACTIVE) {
			$this->MsLoader->MsProduct->changeStatus($product_id, MsProduct::STATUS_INACTIVE);
			$this->session->data['success'] = $this->language->get('ms_success_product_unpublished');
		}
		
		$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
	}	
	
	public function download() {
		if (!$this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		if (isset($this->request->get['download_id'])) {
			$download_id = $this->request->get['download_id'];
		} else {
			$download_id = 0;
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (!$this->MsLoader->MsProduct->hasDownload($product_id,$download_id) || !$this->MsLoader->MsProduct->productOwnedBySeller($product_id,$this->customer->getId()))
			$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
			
		$download_info = $this->MsLoader->MsProduct->getDownload($download_id);
		
		if ($download_info) {
			$file = DIR_DOWNLOAD . $download_info['filename'];
			$mask = basename($download_info['mask']);

			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));
					
					readfile($file, 'rb');
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			$this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}
	}
	
	public function jxAutocompleteCategories() {
		$categories = array();

		$category_id = isset($this->request->get['category_id']) ? $this->request->get['category_id'] : 0;
		$category_type = isset($this->request->get['type']) ? $this->request->get['type'] : 'oc';

		$filter_data = isset($this->request->get['filter_name']) ? array(
			'filters' => array(
				(($category_type == 'oc' ? 'cd' : 'mscd') . '.name') => $this->request->get['filter_name']
			)
		) : array();

		if ($category_type == 'oc') {
			$categories = $this->MsLoader->MsCategory->getOcCategories(array(
				'parent_id' => $category_id,
				'category_status' => MsCategory::STATUS_ACTIVE
			), $filter_data);
		} elseif ($category_type == 'ms') {
			$categories = $this->MsLoader->MsCategory->getCategories(array(
				'seller_ids' => $this->customer->getId(),
				'parent_id' => $category_id,
				'category_status' => MsCategory::STATUS_ACTIVE
			), $filter_data);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($categories));
	}

	protected function categoryPathToArray($category_path, &$categories_array) {
		if($category_path) {
			$cs = explode(',', $category_path);
			$cs_array = array();
			for($i = 0; $i < count($cs); $i++) {
				if($i == 0) $cs_array[$i] = $cs[$i];

				// if there is categories hierarchy
				if(isset($cs[$i+1])) {
					$cs_array[$cs[$i]] = $cs[$i+1];
				}
			}

			$categories_array[] = $cs_array;
		}
	}

	public function jxGetProductMsfAttributes()
	{
		$category_id = !empty($this->request->get['category_id']) ? (int)$this->request->get['category_id'] : null;
		$product_id = !empty($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : null;

		$data = $this->load->language('multiseller/multiseller');

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		// Attributes to be set
		$data['msf_attributes'] = $this->MsLoader->MsfAttribute->getList(['default' => true, 'values' => true], [
			'order_by'  => 'sort_order',
			'order_way' => 'ASC',
		]);

		if ($category_id) {
			$msf_attributes = [];
			$category_msf_attributes_ids = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($category_id);
			foreach ($category_msf_attributes_ids as $msf_attribute_id) {
				$msf_attributes[$msf_attribute_id] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
			}

			if (!empty($msf_attributes)) {
				$data['msf_attributes'] = $msf_attributes;
			}
		}

		// Existing product attributes
		$data['product_msf_attributes'] = $product_id ? $this->MsLoader->MsfAttribute->getProductMsfAttributes($product_id) : [];

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/account-product-form-msf-attributes', $data)]));
	}

	public function jxGetProductMsfVariations()
	{
		$category_id = !empty($this->request->get['category_id']) ? (int)$this->request->get['category_id'] : null;
		$product_id = !empty($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : null;
		$form_errors = !empty($this->request->get['form_errors']) ? $this->request->get['form_errors'] : [];

		$data = $this->load->language('multiseller/multiseller');

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		// Form errors
		foreach ($form_errors as $key => $form_error) {
			if (empty($form_error)) {
				unset($form_errors[$key]);
			}
		}

		$data['errors'] = $form_errors;

		// Variations to be set
		$data['msf_variations'] = $this->MsLoader->MsfVariation->getList(['default' => true, 'values' => true], [
			'order_by'  => 'sort_order',
			'order_way' => 'ASC',
		]);

		if ($category_id) {
			$msf_variations = [];
			$category_msf_variations_ids = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($category_id);
			foreach ($category_msf_variations_ids as $key => $msf_variation_id) {
				$msf_variations[$key] = $this->MsLoader->MsfVariation->get($msf_variation_id);
			}

			if (!empty($msf_variations)) {
				$data['msf_variations'] = $msf_variations;
			}
		}

		// Product selected variation values
		foreach ($data['msf_variations'] as &$msf_variation) {
			$msf_variation['selected_values'] = $product_id ? $this->MsLoader->MsfVariation->getProductSelectedMsfVariations($product_id, $msf_variation['id']) : [];
		}

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/account-product-form-msf-variations', $data)]));
	}

	public function jxGetProductMsfVariationsMatrix()
	{
		$product_id = !empty($this->request->get['product_id']) ? (int)$this->request->get['product_id'] : null;
		$selected_msf_variations = !empty($this->request->get['msf_variations']) ? $this->request->get['msf_variations'] : [];

		// Filter array
		foreach ($selected_msf_variations as $msf_variation_id => $values) {
			if (empty($values)) {
				unset($selected_msf_variations[$msf_variation_id]);
			}
		}

		$data = $this->load->language('multiseller/multiseller');

		$msf_variations_matrix = $this->MsLoader->MsfVariation->generateEmptyVariationsMatrix($selected_msf_variations);

		foreach ($msf_variations_matrix as &$row) {
			$product_msf_variations_row = $this->MsLoader->MsfVariation->getProductMsfVariation($product_id, $row['values']);
			if (!empty($product_msf_variations_row)) {
				$row['quantity'] = $product_msf_variations_row['quantity'];
				$row['price'] = $product_msf_variations_row['price'] ? $this->MsLoader->MsHelper->trueCurrencyFormat($product_msf_variations_row['price']) : '';
				$row['image'] = $product_msf_variations_row['image'];
				$row['status'] = $product_msf_variations_row['status'];
			}
		}

		$data['msf_variations_matrix'] = $msf_variations_matrix;

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/account-product-form-msf-variations-matrix', $data)]));
	}
}
