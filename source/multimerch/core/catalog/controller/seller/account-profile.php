<?php

use \MultiMerch\Event\Event as MsEvent;

class ControllerSellerAccountProfile extends ControllerSellerAccount
{
	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->load->model('tool/image');
		$this->load->model('localisation/language');

		$seller_id = $this->customer->getId();
		if (!$this->customer->isLogged() || !$this->MsLoader->MsSeller->isCustomerSeller($seller_id) || in_array($this->MsLoader->MsSeller->getSellerStatus($seller_id), array(0, MsSeller::STATUS_DISABLED, MsSeller::STATUS_DELETED)))
			$this->response->redirect($this->config->get('msconf_seller_landing_page_id') ? $this->url->link('information/information', 'information_id=' . $this->config->get('msconf_seller_landing_page_id'), 'SSL') : $this->url->link('account/register-seller', '', 'SSL'));
	}

	public function index()
	{
		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');
		$this->document->addScript('catalog/view/javascript/account-seller-profile.js');
		$this->document->addScript('catalog/view/javascript/multimerch/ckeditor/ckeditor.js');

		$seller = $this->MsLoader->MsSeller->getSeller($this->customer->getId());

		// Social links
		if ($this->config->get('msconf_sl_status')) {
			$this->data['social_channels'] = $this->MsLoader->MsSocialLink->getChannels();
			foreach ($this->data['social_channels'] as &$c) {
				$c['image'] = $this->model_tool_image->resize($c['image'], 34, 34);
			}

			if ($seller)
				$seller['social_links'] = $this->MsLoader->MsSocialLink->getSellerChannels($this->customer->getId());
		}

		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['seller_validation'] = $this->config->get('msconf_seller_validation');
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');

		// Seller form fields enabled by admin
		$this->data['seller_included_fields'] = !empty($this->config->get('msconf_seller_included_fields')) ? $this->config->get('msconf_seller_included_fields') : false;

		$this->data['seller'] = false;
		$this->data['statusclass'] = 'warning';
		$this->data['statustext'] = '';

		if (!empty($seller)) {
			// Detect color of seller status holder
			switch ($seller['ms.seller_status']) {
				case MsSeller::STATUS_ACTIVE:
					$this->data['statusclass'] = 'success';
					break;
				case MsSeller::STATUS_DISABLED:
				case MsSeller::STATUS_DELETED:
					$this->data['statusclass'] = 'danger';
					break;
				case MsSeller::STATUS_UNPAID:
				case MsSeller::STATUS_INCOMPLETE:
				default:
					$this->data['statusclass'] = 'warning';
					break;
			}

			// Seller status info
			$this->data['statustext'] = (int)$seller['ms.seller_status'] === (int)MsSeller::STATUS_INACTIVE && !$seller['ms.seller_approved'] ?
				$this->language->get('ms_account_status_tobeapproved') :
				$this->language->get('ms_account_status') . $this->language->get('ms_seller_status_' . $seller['ms.seller_status']);

			// Logo (ex-avatar)
			$avatar = ['name' => '', 'thumb' => ''];
			if (!empty($seller['ms.avatar'])) {
				$avatar = [
					'name' => $seller['ms.avatar'],
					'thumb' => $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_preview_seller_avatar_image_width'), $this->config->get('msconf_preview_seller_avatar_image_height'))
				];

				$this->session->data['multiseller']['files'][] = $seller['ms.avatar'];
			}

			$seller['avatar'] = $avatar;

			// Banner
			$banner = ['name' => '', 'thumb' => ''];
			if (!empty($seller['banner'])) {
				$banner = [
					'name' => $seller['banner'],
					'thumb' => $this->MsLoader->MsFile->resizeImage($seller['banner'], $this->config->get('msconf_product_seller_banner_width') / 2, $this->config->get('msconf_product_seller_banner_height') / 2)
				];

				$this->session->data['multiseller']['files'][] = $seller['banner'];
			}

			$seller['banner'] = $banner;

			// Fill unset slogan and description fields with default values
			foreach ($this->data['languages'] as $language){
				if (!isset($seller['descriptions'][$language['language_id']]['description']))
					$seller['descriptions'][$language['language_id']]['description'] = '';

				if (!isset($seller['descriptions'][$language['language_id']]['slogan']))
					$seller['descriptions'][$language['language_id']]['slogan'] = '';
			}

			// Seller settings
			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $this->customer->getId()));
			$defaults = $this->MsLoader->MsSetting->getSellerDefaults();
			$seller['settings'] = array_merge($defaults, $seller_settings);

			$this->data['seller'] = $seller;
		}

		$this->document->setTitle($this->language->get('ms_account_sellerinfo_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs([
			[
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			],
			[
				'text' => $this->language->get('ms_account_sellerinfo_breadcrumbs'),
				'href' => $this->url->link('seller/account-profile', '', 'SSL')
			]
		]);

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-profile');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	/**
	 * Updates seller profile information.
	 *
	 * Accepts data passed by the POST method.
	 *
	 * Returns 'errors' if validation of form fields failed.
	 * Returns 'redirect' if seller profile information was successfully updated.
	 *
	 * @return	string			JSON with possible messages: 'errors', 'redirect'.
	 */
	public function jxSaveSellerInfo()
	{
		$json = [];
		$data = $this->request->post;

		$json['errors'] = $this->_validateFormFields($data);

		if (empty($json['errors'])) {
			$this->_updateSeller($data);

			$this->session->data['success'] = $this->language->get('ms_account_sellerinfo_saved');
			$json['redirect'] = $this->url->link('seller/account-dashboard');

			$this->ms_events->add(new MsEvent([
				'seller_id' => $this->customer->getId(),
				'event_type' => MsEvent::SELLER_MODIFIED,
				'data' => []
			]));

			if ($this->ms_events->count())
				$this->ms_event_manager->create($this->ms_events);
		}

		$this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadDescriptionImage(){
		
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);
		
		if ($json['errors']) {
			$json["uploaded"] = 0;
			$json["error"]['message'] = implode('<br>',$json['errors']);
			return $this->response->setOutput(json_encode($json));
		}
		
		$file = reset($_FILES);

		list($json["fileName"], $json['url']) = $this->MsLoader->MsFile->uploadDescriptionImage($file, 'description');
		$json["uploaded"] = 1;
	
		return $this->response->setOutput(json_encode($json));
	}

	/**
	 * Uploads seller images (avatar, banner) to the server.
	 *
	 * Returns 'errors' if upload is not possible due to the server configuration.
	 * Returns 'errors' if uploadable image does not meet the general requirements for image (extension, size etc.).
	 * Returns 'files' if image was successfully uploaded (consists full path to the uploaded image and a thumb).
	 *
	 * @return	string			JSON with possible messages: 'errors', 'files'.
	 */
	public function jxUploadSellerAvatar()
	{
		$json = [];

		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors'])
			return $this->response->setOutput(json_encode($json));

		foreach ($_FILES as $name => $file) {
			$json['errors'] = $this->MsLoader->MsFile->checkImage($file);

			if (empty($json['errors'])) {
				$image_width = $this->config->get('msconf_preview_seller_avatar_image_width');
				$image_height = $this->config->get('msconf_preview_seller_avatar_image_height');

				if ('ms-banner' === (string)$name) {
					$image_width = (int)$this->config->get('msconf_product_seller_banner_width') / 2;
					$image_height = (int)$this->config->get('msconf_product_seller_banner_height') / 2;
				}

				$fileName = $this->MsLoader->MsFile->uploadImage($file);
				$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $image_width, $image_height);
				$json['files'][] = [
					'name' => $fileName,
					'thumb' => $thumbUrl
				];
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	/**
	 * Validates form field values.
	 *
	 * @param	array	$data	Form fields values.
	 * @return	array			Errors after fields validation.
	 */
	private function _validateFormFields(&$data) {
		$errors = [];
		$validator = $this->MsLoader->MsValidator;
		$languages = $this->model_localisation_language->getLanguages();

		// Get `language_id` of language selected by user at the frontend (it may differ from `config_language_id`,
		// therefore we get it this way).
		$defaultLanguageId = $this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'));

		// Validate primary language
		foreach ($languages as $language){
			$language_id = $language['language_id'];
			$primary = (int)$language_id !== (int)$defaultLanguageId ? false : true;

			if (!empty($data['languages'][$language_id]['slogan'])) {
				// Slogan - max_len 255
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_sellerinfo_slogan'),
					'value' => html_entity_decode($data['languages'][$language_id]['slogan'])
				),
					array(
						array('rule' => 'max_len,255')
					)
				)) $errors["languages[$language_id][slogan]"] = $validator->get_errors();
			}

			if (!empty($data['languages'][$language_id]['description'])) {
				// Description - max_len 5000
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_sellerinfo_description'),
					'value' => html_entity_decode($data['languages'][$language_id]['description'])
				),
					array(
						array('rule' => 'max_len,5000')
					)
				)) $errors["languages[$language_id][description]"] = $validator->get_errors();
			}

			// Copy fields values from main language
			if(!$primary) {
				if (empty($data['languages'][$language_id]['slogan']) && !empty($data['languages'][$defaultLanguageId]['slogan']))
					$data['languages'][$language_id]['slogan'] = $data['languages'][$defaultLanguageId]['slogan'];

				if (empty($data['languages'][$language_id]['description']) && !empty($data['languages'][$defaultLanguageId]['description']))
					$data['languages'][$language_id]['description'] = $data['languages'][$defaultLanguageId]['description'];
			}

			// Strip dissallowed tags from description and slogan fields
			if (!empty($this->config->get('msconf_rte_whitelist'))) {
				$allowed_tags = explode(",", $this->config->get('msconf_rte_whitelist'));
				$allowed_tags_ready = "";
				
				foreach ($allowed_tags as $tag) {
					$allowed_tags_ready .= "<".trim($tag).">";
				}

				$data['languages'][$language_id]['slogan'] = htmlspecialchars(strip_tags(htmlspecialchars_decode(isset($data['languages'][$language_id]['slogan']) ? $data['languages'][$language_id]['slogan'] : '', ENT_COMPAT), $allowed_tags_ready), ENT_COMPAT, 'UTF-8');
				
				if ($this->config->get('msconf_allow_description_images') != 1){
					$allowed_tags_ready = str_replace('<img>', '', $allowed_tags_ready);
				}
				
				$data['languages'][$language_id]['description'] = htmlspecialchars(strip_tags(htmlspecialchars_decode($data['languages'][$language_id]['description'], ENT_COMPAT), $allowed_tags_ready), ENT_COMPAT, 'UTF-8');
			}
		}

		// Nickname
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_sellerinfo_nickname'),
			'value' => html_entity_decode($data['nickname'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'min_len,4'),
				array('rule' => 'max_len,128'),
				array('rule' => (int)$this->config->get('msconf_nickname_rules') === 1 ? 'latin' : ((int)$this->config->get('msconf_nickname_rules') === 2 ? 'utf8' : 'alpha_numeric')),
			)
		)) $errors['nickname'] = $validator->get_errors();

		// @todo 9.0: move to validator
		if (empty($errors['seller[nickname]']) && (string)$data['nickname'] !== (string)$this->MsLoader->MsSeller->getSellerNickname($this->customer->getId()) && $this->MsLoader->MsSeller->nicknameTaken($data['nickname'])) {
			$errors['nickname'] = $this->language->get('ms_error_sellerinfo_nickname_taken');
		}

		// Logo (ex-avatar)
		if (!empty($data['avatar_name']) && !$this->MsLoader->MsFile->checkFileAgainstSession($data['avatar_name'])) {
			$errors['avatar'] = sprintf($this->language->get('ms_error_file_upload_error'), $data['avatar_name'], $this->language->get('ms_file_cross_session_upload'));
		}

		// Banner
		if (!empty($data['banner_name']) && !$this->MsLoader->MsFile->checkFileAgainstSession($data['banner_name'])) {
			$errors['banner'] = sprintf($this->language->get('ms_error_file_upload_error'), $data['banner_name'], $this->language->get('ms_file_cross_session_upload'));
		}

		// Social links
		if ($this->config->get('msconf_sl_status')) {
			foreach($data['social_links'] as &$link) {
				if(!$this->MsLoader->MsHelper->isValidUrl($link))
					$link = '';
			}
		}

		return $errors;
	}

	/**
	 * Updates seller profile information.
	 *
	 * @param	array	$data	Form fields values.
	 * @return	bool			Returns true if seller was successfully updated.
	 */
	private function _updateSeller($data = []) {
		// Get existing seller data
		$seller = $this->MsLoader->MsSeller->getSeller($this->customer->getId());

		// Prepare seller data
		$seller_data = array(
			'seller_id' => $seller['seller_id'],
			'seller_group' => $seller['ms.seller_group'],
			'nickname' => isset($data['nickname']) ? $data['nickname'] : $seller['ms.nickname'],
			'keyword' => $this->MsLoader->MsHelper->slugify(isset($data['nickname']) ? $data['nickname'] : $seller['ms.nickname']),
			'avatar_name' => isset($data['avatar_name']) ? $data['avatar_name'] : $seller['ms.avatar'],
			'banner_name' => isset($data['banner_name']) ? $data['banner_name'] : $seller['banner'],
			'languages' => isset($data['languages']) ? $data['languages'] : [],
			'social_links' => isset($data['social_links']) ? $data['social_links'] : [],
			'settings' => [
				'slr_website' => isset($data['settings']['website']) ? $data['settings']['website'] : '',
				'slr_company' => isset($data['settings']['company']) ? $data['settings']['company'] : '',
				'slr_phone' => isset($data['settings']['phone']) ? $data['settings']['phone'] : ''
			]
		);

		// Update seller
		$this->MsLoader->MsSeller->editSellerProfile($seller_data);
		
		if (
			$this->config->get('msconf_allow_description_images') == 1 && 
			$this->config->get('msconf_description_images_type') == 'upload'
		) {

			$all_descriptions_content = '';
			
			foreach ($data['languages'] as $lang_id => $fields) {
				$all_descriptions_content .= $fields['description'];
			}
			
			$this->MsLoader->MsFile->checkDescriptionImages($all_descriptions_content, 'description');
		}

		// Update seller's social links
		if ($this->config->get('msconf_sl_status'))
			$this->MsLoader->MsSocialLink->editSellerChannels($this->customer->getId(), $data);

		return true;
	}
}
