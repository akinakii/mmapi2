<?php

class ControllerSellerAccountNotification extends ControllerSellerAccount
{
	public function getTableData()
	{
		$this->load->model('tool/image');

		$colMap = [];

		$sorts = ['date_created'];
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsNotification->getNotifications(
			[
				'channel' => 'onsite',
				'consumer_type' => 'seller',
				'consumer_id' => $this->customer->getId()
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];

		foreach ($results as $result) {
			$message_class = $this->MsLoader->MsNotification->getClassAlias('messages', 'onsite');

			switch ($result['producer_type']) {
				case 'admin':
					$image = $this->config->get('config_logo');
					break;

				case 'seller':
					$seller = $this->MsLoader->MsSeller->getSeller($result['producer_id']);
					$image = !empty($seller['ms.avatar']) ? $seller['ms.avatar'] : '';
					break;

				case 'customer':
				default:
					$image = '';
			}

			switch ($result['object_type']) {
				case 'message':
					$icon = '<i class="fa fa-envelope"></i>';
					break;

				case 'invoice':
				case 'payout':
					$icon = '<i class="fa fa-usd"></i>';
					break;

				case 'order':
					if ('status' === $result['object_subtype']) {
						$icon = '<i class="fa fa-credit-card"></i>';
					} else {
						$icon = '<i class="fa fa-shopping-cart"></i>';
					}
					break;

				case 'suborder':
					$icon = '<i class="fa fa-shopping-cart"></i>';
					break;

				case 'account':
					$icon = '<i class="fa fa-user"></i>';
					break;

				case 'product':
					$icon = '<i class="fa fa-briefcase"></i>';
					break;

				case 'import':
					$icon = '<i class="fa fa-upload"></i>';
					break;

				case 'review':
				default:
					$icon = '<i class="fa fa-star"></i>';
					break;
			}

			$columns[] = array_merge(
				$result,
				array(
					'avatar' => $icon,
					'message' => (new $message_class($this->registry, 'onsite',
						"{$result['producer_type']}.{$result['producer_id']}",
						"{$result['consumer_type']}.{$result['consumer_id']}",
						[
							'type' => $result['object_type'],
							'subtype' => $result['object_subtype'],
							'id' => $result['object_id'],
							'action' => $result['object_action'],
							'metadata' => $result['metadata']
						]
					))->getText(),
					'date_created' => date($this->language->get('datetime_format'), strtotime($result['date_created'])),
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function index()
	{
		// Reset new notifications counter
		$this->MsLoader->MsNotification->updateNewNotificationsCount('seller', $this->customer->getId(), true);

		$this->document->setTitle($this->language->get('ms_seller_account_notification_breadcrumbs'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_seller_account_notification_breadcrumbs'),
				'href' => $this->url->link('seller/account-notification', '', 'SSL'),
			)
		));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-notification');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxGetNewNotifications()
	{
		$json = [];

		$json['count'] = $this->MsLoader->MsNotification->getNewNotificationsCount('seller', $this->customer->getId());

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
