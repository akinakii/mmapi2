<?php

use MultiMerch\Models\Feed;
use MultiMerch\Core\Feed\Parsers\CsvParser;
use MultiMerch\Core\Feed\Mapper;
use MultiMerch\Core\Feed\Importer;

class ControllerSellerAccountFeed extends ControllerSellerAccount
{
	protected $seller_id;

	protected $oc_fields;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->seller_id = $this->customer->getId();

		if (!$this->MsLoader->MsSeller->isSeller()) {
			return $this->response->redirect($this->url->link('seller/account-profile', '', 'SSL'));
		}

		if (!$this->config->get('msconf_import_enable')) {
			return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
		}

		$this->load->model('tool/upload');

		$this->data = array_merge($this->data ?? [], $this->load->language('multimerch/feed'));

		$mapper = new Mapper($this->registry, 'product');
		$this->oc_fields = $mapper->getFields($this->seller_id, true);
	}

	/**
	 * Single product import.
	 */
	public function product_import()
	{
		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');
		$this->document->addScript('catalog/view/javascript/multimerch/account-feed.js');

		$this->MsLoader->MsHelper->addStyle('multimerch/bootstrap-nav-wizard');
		$this->MsLoader->MsHelper->addStyle('multimerch/feed/import');

		$this->document->setTitle($this->language->get('ms_feed_title_import_products'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs([
			[
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_account_products_breadcrumbs'),
				'href' => $this->url->link('seller/account-product', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_feed_title_import_products'),
				'href' => $this->url->link('seller/account-feed/product_import', '', 'SSL'),
			]
		]);

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/product_import');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function stepFeedConfig()
	{
		// Init import data in session
		$this->initImportDataInSession();
	}

	public function jxGetImportFileByUrl()
	{
		$json = [];
		$json['errors'] = $this->MsLoader->MsHelper->isFileByUrlValid($this->request->post['url'] ?? null, true);

		if (empty($json['errors'])) {
			$url = $this->request->post['url'];
			$url_parts = explode('/', $url);
			$file_mask = end($url_parts);
			$file_name = time() . '_' . md5(rand()) . '.' . $this->MsLoader->MsSeller->getNickname() . '_' . $file_mask;

			$file_contents = @file_get_contents($url);

			if ($file_contents === false) {
				$json['errors'][] = $this->language->get('ms_feed_error_source_file_invalid');
			} else {
				file_put_contents(DIR_UPLOAD . $file_name, $file_contents);

				$this->load->model('tool/upload');
				$code = $this->model_tool_upload->addUpload($file_mask, $file_name);
				$json['files'][] = array (
					'code' => $code,
					'fileName' => $file_name,
					'fileMask' => $file_mask,
					'filePages' => isset($pages) ? $pages : ''
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($json));
	}

	public function stepUpload()
	{
		// Init import data in session
		$this->initImportDataInSession();

		// Validate seller has not reached products limit for his account
		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroupBySellerId($this->seller_id);
		if (!empty($seller_group['seller_product_limit'])) {
			$new_product_limit = $seller_group['seller_product_limit'] - $seller_group['seller_product_quantity'];

			if ($new_product_limit < 0) {
				$new_product_limit = 0;
			}

			$this->session->data['import_data']['new_product_limit'] = $new_product_limit;
		}

		// Retrieve already uploaded import file
		$upload_info = false;
		if (isset($this->session->data['import_data']['attachment_code'])) {
			$upload_info = $this->model_tool_upload->getUploadByCode($this->session->data['import_data']['attachment_code']);
		}

		$this->data['upload_info'] = !empty($upload_info) ? $upload_info : [];
		$this->data['filename'] = !empty($upload_info['name']) ? $upload_info['name'] : '';
		$this->data['attachment_code'] = !empty($upload_info['code']) ? $upload_info['code'] : '';

		// @todo 8.28: detect file encoding
		$this->data['file_encoding'] = 0;
		if (isset($this->session->data['import_data']['file_encoding'])) {
			$this->data['file_encoding'] = $this->session->data['import_data']['file_encoding'];
		}

		$this->data['start_row'] = 2;
		if (isset($this->session->data['import_data']['start_row'])) {
			$this->data['start_row'] = $this->session->data['import_data']['start_row'];
		}

		$this->data['finish_row'] = '';
		if (isset($this->session->data['import_data']['finish_row'])) {
			$this->data['finish_row'] = $this->session->data['import_data']['finish_row'];
		}

		$this->data['enclosure'] = htmlspecialchars('"');
		if (isset($this->session->data['import_data']['enclosure'])) {
			$this->data['enclosure'] = htmlspecialchars($this->session->data['import_data']['enclosure']);
		}

		$this->data['delimiter'] = ';';
		if (isset($this->session->data['import_data']['delimiter'])) {
			$this->data['delimiter'] = htmlspecialchars($this->session->data['import_data']['delimiter']);
		}

		$this->data['url_path'] = '';
		if (isset($this->session->data['import_data']['url_path'])) {
			$this->data['url_path'] = $this->session->data['import_data']['url_path'];
		}

		$this->data['example_url'] = ($this->config->get('config_secure') ? HTTPS_SERVER : HTTP_SERVER) . 'demo-import.csv';
		$this->data['file_encodings'] = [
			1 => 'Windows-1251',
			2 => 'UTF-8'
		];

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/step_upload');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxValidateStepUpload()
	{
		$json = [];

		// Validate import file has been uploaded
		if (empty($this->request->post['attachment_code']) && empty($this->session->data['import_data']['attachment_code'])) {
			$json['errors']['attachment_code'] = $this->language->get('ms_feed_error_source_file');
		} else {
			$this->session->data['import_data']['attachment_code'] = $this->request->post['attachment_code'];
		}

		if (!empty($this->request->post['url_path'])) {
			$this->session->data['import_data']['url_path'] = $this->request->post['url_path'];
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($json));
	}

	public function jxDeleteUploadedFileFromSession()
	{
		$json = [];

		unset($this->session->data['import_data']['attachment_code']);
		unset($this->session->data['import_data']['url_path']);
		$json['success'] = true;

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($json));
	}

	public function stepMapping()
	{
		$upload_info = null;
		if (!empty($this->session->data['import_data']['attachment_code'])) {
			$upload_info = $this->model_tool_upload->getUploadByCode($this->session->data['import_data']['attachment_code']);
		}

		if (empty($upload_info)) {
			return;
		}

		$filename = DIR_UPLOAD . $upload_info['filename'];

		if (!empty($this->session->data['import_data']['file_encoding'])) {
			$file_encoding = $this->session->data['import_data']['file_encoding'];
		} else {
			$file_encoding = null;
		}

		$enclosure = !empty($this->session->data['import_data']['enclosure']) ? $this->session->data['import_data']['enclosure'] : '"';
		$delimiter = !empty($this->session->data['import_data']['delimiter']) ? $this->session->data['import_data']['delimiter'] : ';';

		$parser = new CsvParser($filename, $file_encoding, $enclosure, $delimiter);

		// Get field captions
		$this->data['field_captions'] = $parser->parseFieldCaptions();

		// Retrieve existing mapping
		$existing_mapping = [];
		if (!empty($this->session->data['import_data']['mapping']) && count($this->session->data['import_data']['mapping']) === count($this->data['field_captions'])) {
			$existing_mapping = $this->session->data['import_data']['mapping'];

			if (!is_array($existing_mapping)) {
				$existing_mapping = unserialize($this->session->data['import_data']['mapping']);
			}
		}

		$this->data['mapping'] = $existing_mapping;
		$this->data['attributes'] = $this->getAvailableAttributes();

		// Parse sample data (one row of uploaded import file)
		$sample_data = $parser->parseData($existing_mapping, 2, 3, false);
		$this->data['samples'] = array_shift($sample_data);

		// Get all possible fields for mapping
		$this->data['oc_fields'] = $this->oc_fields;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/step_mapping');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxValidateStepMapping()
	{
		$json = [];

		// Refresh mapping data in session
		$this->session->data['import_data']['mapping'] = [];

		$mapping_data = !empty($this->request->post['mapping_data']) ?
			json_decode(html_entity_decode($this->request->post['mapping_data'])) :
			[];

		// Validate required fields are not empty
		foreach ($this->oc_fields as $type => $data) {
			if (!empty($data['required']) && !in_array($type, array_values($mapping_data))) {
				$json['errors'][] = sprintf($this->language->get('ms_feed_error_mapping_field'), $data['field_name']);
			}
		}

		if (empty($json['errors'])) {
			foreach ($mapping_data as $caption_id => $field_name) {
				// Update mapping data in session
				$this->session->data['import_data']['mapping'][$caption_id] = $field_name;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($json));
	}

	public function stepConfirm()
	{
		$this->data['import_data'] = $this->session->data['import_data'];

		$upload_info = $this->model_tool_upload->getUploadByCode($this->session->data['import_data']['attachment_code']);
		$this->data['filename'] = $upload_info ? $upload_info['name'] : $this->language->get('ms_feed_not_specified');
		$this->data['file_encoding'] = !empty($this->session->data['import_data']['file_encoding']) ? $this->session->data['import_data']['file_encoding'] : $this->language->get('ms_feed_not_specified');
		$this->data['import_data']['default_product_status'] = $this->session->data['import_data']['default_product_status'] ?? $this->language->get('ms_feed_not_specified');
		$this->data['import_data']['fill_category'] = $this->session->data['import_data']['fill_category'] ? $this->language->get('ms_yes') : $this->language->get('ms_no');

		$stock_status = $this->MsLoader->MsHelper->getStockStatusName($this->session->data['import_data']['stock_status_id']);
		$this->data['import_data']['stock_status_name'] = $stock_status['name'] ?? '';
		$this->data['import_data']['default_product_status_name'] = $this->session->data['import_data']['default_product_status'] ? $this->language->get('ms_feed_status_active') : $this->language->get('ms_feed_status_inactive');
		$this->data['import_data']['product_approved_text'] = $this->session->data['import_data']['product_approved'] ? $this->language->get('text_yes') : $this->language->get('text_no');

		$this->data['fields'] = [];
		foreach ($this->session->data['import_data']['mapping'] as $type => $data) {
			if (isset($this->oc_fields[$type]['name'])) {
				$this->data['fields'][$type] = $this->oc_fields[$type]['name'];
			}
		}

		$filename = DIR_UPLOAD . $upload_info['filename'];

		if (!empty($this->session->data['import_data']['file_encoding'])) {
			$file_encoding = $this->session->data['import_data']['file_encoding'];
		} else {
			$file_encoding = null;
		}

		$enclosure = !empty($this->session->data['import_data']['enclosure']) ? $this->session->data['import_data']['enclosure'] : '"';
		$delimiter = !empty($this->session->data['import_data']['delimiter']) ? $this->session->data['import_data']['delimiter'] : ';';

		$parser = new CsvParser($filename, $file_encoding, $enclosure, $delimiter);

		// Retrieve existing mapping
		$existing_mapping = [];
		if (!empty($this->session->data['import_data']['mapping'])) {
			$existing_mapping = $this->session->data['import_data']['mapping'];

			if (!is_array($existing_mapping)) {
				$existing_mapping = unserialize($this->session->data['import_data']['mapping']);
			}
		}

		// Parse sample data (one row of uploaded import file)
		$sample_data = $parser->parseData($existing_mapping, 2, 3, false);
		$this->data['samples'] = array_shift($sample_data);
		$this->data['attributes_included'] = false;

		foreach ($this->getAvailableAttributes() as $attribute) {
			if (in_array($attribute['id'], array_keys($this->data['samples']))) {
				$this->data['attributes_included'] = true;
				$this->data['attributes'] = $this->getAvailableAttributes();
				break;
			}
		}

		// Get all possible fields for mapping
		$this->data['oc_fields'] = $this->oc_fields;

		$this->data['import'] = $this->url->link('seller/account-feed/startImport', '', 'SSL');
		$this->data['config_id'] = $this->session->data['import_data']['config_id'];

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/step_confirm');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxUploadFile()
	{
		$json = [];

		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N files
		$msconf_downloads_limits = $this->config->get('msconf_downloads_limits');
		foreach ($_FILES as $file) {
			if ($msconf_downloads_limits[1] > 0 && $this->request->post['fileCount'] > $msconf_downloads_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_download_maximum'),$msconf_downloads_limits[1]);
				$json['cancel'] = 1;

				return $this->response->setOutput(json_encode($json));
			} else {
				$errors = $this->MsLoader->MsFile->checkImportFile($file);

				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileData = $this->MsLoader->MsFile->uploadImportFile($file);
					$this->load->model('tool/upload');
					$fileData['code'] = $this->model_tool_upload->addUpload($fileData['fileMask'], $fileData['fileName']);
					$json['files'][] = array(
						'code' => $fileData['code'],
						'fileName' => $fileData['fileName'],
						'fileMask' => $fileData['fileMask'],
						'filePages' => isset($pages) ? $pages : ''
					);
				}
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	public function startImport()
	{
		// Store import config
		$config_id = $this->MsLoader->ModelProductImport->createImportConfig(array_merge(
			['seller_id' => $this->customer->getId(), 'config_name' => ''],
			$this->session->data['import_data']
		));

		// Create import definition
		$import_id = $this->MsLoader->ModelProductImport->createImport([
			'seller_id' => $this->customer->getId(),
			'type' => 'product',
			'import_config_id' => $config_id,
			'name' => sprintf($this->language->get('ms_feed_import_name_template'), $this->MsLoader->MsSeller->getSellerNickname($this->customer->getId())),
			'is_scheduled' => !empty($this->request->post['is_scheduled']),
		]);

		// Create cron-job to be run only once
		$this->MsLoader->ModelCron->create([
			'seller_id' => $this->customer->getId(),
			'import_id' => $import_id,
			'cycle' => $this->request->post['schedule_cycle'] ?? null,
			'status' => MM_CORE_CONST_STATUS_ACTIVE,
			'is_running' => false,
		]);

		// Clean import data in session
		unset($this->session->data['import_data']);

		$this->session->data['success'] = $this->language->get('ms_feed_success_import_started');

		return $this->response->redirect($this->url->link('seller/account-product', '', 'SSL'));
	}

	private function initImportDataInSession()
	{
		if (empty($this->session->data['import_data'])) {
			$this->session->data['import_data'] = [
				'import_type' => 'product',
				'key_field' => $this->config->get('msconf_feed_primary_product_field'),
				'config_id' => 0,
				'enclosure' => '"',
				'delimiter' => ',',
				'start_row' => 2,
				'finish_row' => '',
				'attachment_code' => '',
				'url_path' => '',
				'file_encoding' => 0,
				'new_product_limit' => false,
				'default_quantity' => 999,
				'default_product_status' => 0,
				'delimiter_category' => '>',
				'max_product_categories' => '6',
				'fill_category' => 1,
				'stock_status_id' => 7,
				'images_path' => 'import/' . $this->customer->getId() . '/',
				'product_approved' => 1
			];
		}

		// Always reset url_path
		$this->session->data['import_data']['url_path'] = '';
	}

	protected function getAvailableAttributes()
	{
		if (! MsLoader::getInstance()->MsHelper->isInstalled()) {
			return [];
		}

		if (! in_array('attributes', $this->registry->get('config')->get('msconf_product_included_fields'))) {
			return [];
		}

		return $this->MsLoader->MsfAttribute->getList();
	}

	// Imports list

	public function imports()
	{
		$this->document->setTitle($this->language->get('ms_feed_title_import_products'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs([
			[
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_account_products_breadcrumbs'),
				'href' => $this->url->link('seller/account-product', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_feed_title_import_products'),
				'href' => $this->url->link('seller/account-feed/imports', '', 'SSL'),
			]
		]);

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/import-list');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxGetImportsTableData()
	{
		$colMap = [
			'seller' => 'mss.nickname',
			'date' => 'date_added'
		];
		$sorts = ['seller', 'date'];
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->ModelProductImport->getImports(
			[
				'is_scheduled' => false,
				'seller_id' => $this->customer->getId()
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];
		foreach ($results as $result) {
			$columns[] = array_merge(
				$result,
				[
					'date' => $result['date_added'],
					'processed' => $result['processed'],
					'added' => $result['added'],
					'updated' => $result['updated'],
				]
			);
		}

		$this->response->setOutput(json_encode([
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		]));
	}

	public function jxGetScheduledImportsTableData()
	{
		$colMap = [
			'seller' => 'mss.nickname',
			'date_last_run' => 'msc.date_start',
			'date_next_run' => 'msc.date_next',
		];
		$sorts = ['seller', 'date_last_run', 'date_next_run'];
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->ModelProductImport->getImports(
			[
				'is_scheduled' => true,
				'seller_id' => $this->customer->getId()
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];
		foreach ($results as $result) {
			// actions
			$actions = "";
			$actions .= "<a class='icon-edit' href='" . $this->url->link('seller/account-feed/editScheduledImport', 'import_id=' . $result['id'], 'SSL') ."' title='" . $this->language->get('ms_edit') . "'><i class='fa fa-pencil'></i></a>";
			$actions .= "<a class='icon-remove' href='" . $this->url->link('seller/account-feed/deleteScheduledImport', 'import_id=' . $result['id'], 'SSL') ."' title='" . $this->language->get('ms_delete') . "'><i class='fa fa-times'></i></a>";

			$columns[] = array_merge(
				$result,
				[
					'url_path' => $result['url_path'] ?: '-',
					'date_last_run' => $result['date_start'],
					'date_next_run' => $result['date_next'],
					'status' => $result['status'] ? ('<p style="color: green">' . $this->language->get('ms_enabled') . '</p>') : ('<p style="color: red">' . $this->language->get('ms_disabled') . '</p>'),
					'actions' => $actions
				]
			);
		}

		$this->response->setOutput(json_encode([
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		]));
	}

	public function editScheduledImport()
	{
		$this->_validateCall();

		$import_id = isset($this->request->get['import_id']) ? (int)$this->request->get['import_id'] : 0;
		$import = $this->MsLoader->ModelProductImport->getImports(['import_id' => $import_id]);

		//check coupon owner
		if (!$import || $import['seller_id'] != $this->customer->getId()){
			return $this->response->redirect($this->url->link('seller/account-feed/imports', '', 'SSL'));
		}

		$this->document->addScript('catalog/view/javascript/multimerch/selectize/selectize.min.js');
		$this->document->addStyle('catalog/view/javascript/multimerch/selectize/selectize.bootstrap3.css');

		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['back'] = $this->url->link('seller/account-feed/imports', '', 'SSL');

		// Title and friends
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_feed_import_list'),
				'href' => $this->url->link('seller/account-feed/imports', '', 'SSL'),
			)
		));

		$this->data['import'] = $import;

		$this->data['heading'] = $this->language->get('ms_feed_import_list_scheduled_edit');
		$this->document->setTitle($this->language->get('ms_feed_import_list_scheduled_edit'));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/feed/scheduled-import-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function deleteScheduledImport()
    {
        $this->_validateCall();

        $import_id = $this->request->get['import_id'] ?? null;

        if (!$import_id) {
            return false;
        }

        $import = $this->MsLoader->ModelProductImport->getImports(['import_id' => $import_id]);
        $job = $this->MsLoader->ModelCron->get(['import_id' => $import_id]);

        $this->MsLoader->ModelProductImport->deleteImport($import_id);
        $this->MsLoader->ModelProductImport->deleteImportConfig($import['import_config_id']);
        $this->MsLoader->ModelProductImport->deleteHistory($import_id);
        $this->MsLoader->ModelCron->delete($job['id']);

        $this->session->data['success'] = $this->language->get('ms_feed_import_settings_success_deleted');
        return $this->response->redirect($this->url->link('seller/account-feed/imports', '', 'SSL'));
    }

	public function jxUpdateScheduledImport()
    {
        $this->_validateCall();

        $json = [];

        $data = $this->request->post;

        if (!isset($data['import_id'])) {
            $json['errors'][] = $this->language->get('ms_feed_import_settings_error_import_id');
        }

        if (empty($json['errors'])) {
            $this->MsLoader->ModelProductImport->updateImport($data['import_id'], [
                'url_path' => $data['url_path']
            ]);

            $cron_job_data = [
                'cycle' => $data['schedule_cycle'],
                'status' => $data['status'],
            ];

            if ($data['schedule_cycle_old'] !== $data['schedule_cycle']) {
                $cron_job_data['date_next'] = date('Y-m-d H:i:s', strtotime('+10 seconds'));
            }

            // Get related cron jobs
            $job = $this->MsLoader->ModelCron->get(['import_id' => $data['import_id']]);
            $this->MsLoader->ModelCron->update($job['id'], $cron_job_data);

            $this->session->data['success'] = $this->language->get('ms_feed_import_settings_success_updated');
            $json['redirect'] = $this->url->link('seller/account-feed/imports', '', 'SSL');
        }

        $this->response->addHeader('Content-Type: application/json');
        return $this->response->setOutput(json_encode($json));
    }

	private function _validateCall() {
		if(!$this->config->get('msconf_import_enable')) {
			return $this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		}
	}
}
