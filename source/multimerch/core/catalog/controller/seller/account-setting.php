<?php

class ControllerSellerAccountSetting extends ControllerSellerAccount
{
	use \MultiMerch\Module\Traits\Stripe;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->load->model('localisation/language');
		$this->load->model('localisation/country');
		$this->load->model('localisation/weight_class');
		$this->load->model('account/customer');
		$this->load->model('tool/image');
		$this->load->model('extension/extension');
		$this->load->model('setting/setting');

		$seller_id = $this->customer->getId();
		if (!$this->customer->isLogged() || !$this->MsLoader->MsSeller->isCustomerSeller($seller_id) || in_array($this->MsLoader->MsSeller->getSellerStatus($seller_id), [0, MsSeller::STATUS_DISABLED, MsSeller::STATUS_DELETED]))
			$this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

	}

	public function index()
	{
		$this->document->addScript('catalog/view/javascript/ms-common.js');
		$this->document->addScript('catalog/view/javascript/account-settings.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');

		$this->document->setTitle($this->language->get('ms_account_sellersetting_breadcrumbs'));

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		if (isset($this->session->data['error_warning'])) {
			$this->data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs([
			[
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_account_sellersetting_breadcrumbs'),
				'href' => $this->url->link('seller/account-setting', '', 'SSL'),
			]
		]);

		// Common
		$this->data['seller_id'] = $this->customer->getId();
		$this->data['countries'] = $this->model_localisation_country->getCountries();

		// Account tab
		$this->data['account'] = $this->model_account_customer->getCustomer($this->customer->getId());

		// Address tab
		$ms_address_id = $this->MsLoader->MsSetting->getSellerSettings([
			'seller_id' => $this->customer->getId(),
			'name' => 'slr_ms_address',
			'single' => 1
		]);
		$this->data['address'] = !empty($ms_address_id) ? $this->MsLoader->MsSeller->getSellerMsAddress(['address_id' => $ms_address_id, 'single' => 1]) : [];

		// Payment tab
		$this->data['payment_gateways'] = $this->_getPaymentGateways();

		// Shipping tab
		$this->data['ms_shipping'] = $this->load->controller('multimerch/shipping/renderSettingsShipping');

		// Get seller settings
		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $this->customer->getId()]);
		$defaults = $this->MsLoader->MsSetting->getSellerDefaults();
		$this->data['settings'] = array_merge($defaults, $seller_settings);
		
		// Holiday mode
		if ($this->config->get('msconf_holiday_mode_allow')) {
			$this->data['settings']['slr_holiday_mode_enabled'] = isset($this->data['settings']['slr_holiday_mode_enabled']) ? $this->data['settings']['slr_holiday_mode_enabled'] : 0;
			if (!empty($this->data['settings']['slr_holiday_mode_note'])) {
				$this->data['settings']['slr_holiday_mode_note'] = json_decode($this->data['settings']['slr_holiday_mode_note'], true);
			}
		}

		// Add seller's logo to session in order to fix MsFile->checkFileAgainstSession
		if (isset($this->data['settings']['slr_logo']) && !in_array($this->data['settings']['slr_logo'], $this->session->data['multiseller']['files']))
			array_push($this->session->data['multiseller']['files'], $this->data['settings']['slr_logo']);

		if ($this->config->get('ms_stripe_connect_enable_subscription')) {
			// Check Stripe subscription
			$stripe_subscription_id = !empty($this->data['settings']['slr_stripe_subscription_id']) ? $this->data['settings']['slr_stripe_subscription_id'] : null;

			if ($stripe_subscription_id) {
				try {
					$subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

					$seller = $this->MsLoader->MsSeller->getSeller($this->data['seller_id']);
					$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroupDescriptions($seller['ms.seller_group']);

					$this->data['stripe_subscription'] = [
						'name' => !empty($seller_group[$this->config->get('config_language_id')]['name']) ? $seller_group[$this->config->get('config_language_id')]['name'] : '-',
						'period' => date($this->language->get('date_format_short'), $subscription->current_period_end),
					];

					foreach ($subscription->items->data as $item) {
						$plan = $item->plan;
						$type = $plan->metadata->plan_type ?: 'base';

						$this->data['stripe_subscription']['terms'][$type] = [
							'amount_formatted' => $this->currency->format($plan->amount / 100, strtoupper($plan->currency)),
							'interval_formatted' => $this->language->get('ms_stripe_subscription_plan_interval_' . $plan->interval)
						];
					}
				} catch (\Stripe\Error\InvalidRequest $e) {
					$this->ms_logger->error($e->getMessage());
				} catch (\MultiMerch\Module\Errors\Settings $e) {
					$this->ms_logger->error($e->getMessage());
				}
			}

			// Check Stripe card details
			$stripe_customer_id = !empty($this->data['settings']['slr_stripe_customer_id']) ? $this->data['settings']['slr_stripe_customer_id'] : null;

			if ($stripe_customer_id) {
				try {
					$customer = \Stripe\Customer::retrieve([
						'id' => $stripe_customer_id,
						'expand' => ['default_source']
					]);

					// Card brand and last 4 digits
					if (!empty($customer->default_source))
						$this->data['stripe_customer_card'] = "<i class='fa fa-credit-card'></i> " . strtoupper($customer->default_source->brand) . " **** **** **** " . $customer->default_source->last4;
				} catch (\Stripe\Error\InvalidRequest $e) {
					$this->ms_logger->error($e->getMessage());
				} catch (\MultiMerch\Module\Errors\Settings $e) {
					$this->ms_logger->error($e->getMessage());
				}
			}
		}

		$this->data['settings']['slr_thumb'] = !empty($this->data['settings']['slr_logo']) ? $this->model_tool_image->resize($this->data['settings']['slr_logo'], $this->config->get('msconf_preview_seller_avatar_image_width'), $this->config->get('msconf_preview_seller_avatar_image_height')) : '';

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$seller = $this->MsLoader->MsSeller->getSeller($this->data['seller_id']);

			$this->data['msf_seller_properties'] = [
				'default' => $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($seller['ms.seller_group'], true),
				'existing' => $this->MsLoader->MsfSellerProperty->getSellerMsfSellerProperties($this->data['seller_id'])
			];
		}

		if (1 == $this->config->get('ms_linnworks_enabled')) {
			$this->load->model('extension/module/linnworks');
			$this->data = array_merge($this->data, $this->load->language('extension/module/linnworks'));
			
			$this->data['lw_access_code'] = $this->model_extension_module_linnworks->getSellerAccessKey($this->customer->getId());
			$this->data['lw_token'] = $this->model_extension_module_linnworks->getTokenByUserId($this->customer->getId());
		}
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/settings/default');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	/**
	 * Updates seller account and address information.
	 *
	 * Accepts data passed by the POST method.
	 *
	 * Returns 'errors' if validation of form(s) fields failed.
	 * Returns 'redirect' if seller account and address information was successfully updated.
	 *
	 * @return    string            JSON with possible messages: 'errors', 'redirect'.
	 */
	public function jxSaveSellerInfo()
	{
		$json = [];
		$data = $this->request->post;
		$data['seller_id'] = $this->customer->getId();

		// Get seller's Google geolocation coordinates
		$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $data['seller_id'], 'single' => 1]);
		if (
			(!empty($data['address']['country_id']) && !empty($seller_ms_address['country_id']) && (int)$data['address']['country_id'] !== (int)$seller_ms_address['country_id'])
			||
			(!empty($data['address']['city']) && !empty($seller_ms_address['city']) && (string)$data['address']['city'] !== (string)$seller_ms_address['city'])
		) {
			$position = $this->MsLoader->MsSeller->processGoogleGeoLocationChange($data['seller_id'], $data['address'], false);
			if ($position) {
				$data['settings']['slr_google_geolocation'] = $position;
			}
		}

		$json['errors'] = $this->_validateFormData($data);

		if (empty($json['errors'])) {
			$this->MsLoader->MsSeller->editSellerAccountInfo($data);
			$this->MsLoader->MsSeller->editSellerMsAddress($data);

			if ($this->config->get('msconf_holiday_mode_allow')) {
				$this->MsLoader->MsSeller->editSellerHolidayMode($data);
			}

			if ($this->config->get('msconf_msf_seller_property_enabled')) {
				$this->MsLoader->MsSeller->editMsfSellerProperties($data['seller_id'], $data);
			}

			$this->session->data['success'] = $this->language->get('ms_success_settings_saved');
			$json['redirect'] = $this->url->link('seller/account-setting', '', 'SSL');
		}

		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Uploads seller logo (avatar) to the server.
	 *
	 * Returns 'errors' if upload is not possible due to the server configuration.
	 * Returns 'errors' if uploadable image does not meet the general requirements for image (extension, size etc.).
	 * Returns 'files' if image was successfully uploaded (consists full path to the uploaded image and a thumb).
	 *
	 * @return	string			JSON with possible messages: 'errors', 'files'.
	 */
	public function jxUploadSellerLogo()
	{
		$json = [];

		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors'])
			return $this->response->setOutput(json_encode($json));

		foreach ($_FILES as $file) {
			$json['errors'] = $this->MsLoader->MsFile->checkImage($file);

			if (empty($errors)) {
				$fileName = $this->MsLoader->MsFile->uploadImage($file);
				$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_seller_avatar_image_width'), $this->config->get('msconf_preview_seller_avatar_image_height'));
				$json['files'][] = [
					'name' => $fileName,
					'thumb' => $thumbUrl
				];
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	/**
	 * Validates form(s) field values.
	 *
	 * @param	array	$data	Form(s) fields values.
	 * @return	array			Errors after fields validation.
	 */
	private function _validateFormData($data)
	{
		$errors = [];
		$validator = $this->MsLoader->MsValidator;

		// Firstname - required
		if (!$validator->validate([
			'name' => $this->language->get('ms_seller_first_name'),
			'value' => html_entity_decode($data['account']['firstname'])
		], [
			['rule' => 'required'],
			['rule' => 'max_len,64']
		])) $errors["account[firstname]"] = $validator->get_errors();

		// Lastname - required
		if (!$validator->validate([
			'name' => $this->language->get('ms_seller_last_name'),
			'value' => html_entity_decode($data['account']['lastname'])
		], [
			['rule' => 'required'],
			['rule' => 'max_len,64']
		])) $errors["account[lastname]"] = $validator->get_errors();

		// Email - required
		if (!$validator->validate([
			'name' => $this->language->get('ms_seller_email'),
			'value' => html_entity_decode($data['account']['email'])
		], [
			['rule' => 'required'],
			['rule' => 'email'],
			['rule' => 'max_len,96']
		])) $errors["account[email]"] = $validator->get_errors();

		// Country - required
		if (!$validator->validate([
			'name' => $this->language->get('ms_seller_country'),
			'value' => !empty($data['address']['country_id']) ? html_entity_decode($data['address']['country_id']) : null
		], [
			['rule' => 'required']
		])) $errors["address[country_id]"] = $validator->get_errors();
		
		if ($this->config->get('msconf_holiday_mode_allow')) {
			// Holiday mode message 
			if ($data['settings']['slr_holiday_mode_enabled']){
				$languages = $this->model_localisation_language->getLanguages();
				foreach ($languages as $language){
					$language_id = $language['language_id'];

					if (!$validator->validate([
							'name' => $this->language->get('ms_account_setting_holiday_mode_message'),
							'value' => $data['languages'][$language_id]['slr_holiday_mode_note'],
							], [
							['rule' => 'max_len,500']
						])) $errors["languages[$language_id]"]['slr_holiday_mode_note'] = $validator->get_errors();
				}
			}
		}

		// Validate MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			if (!empty($data['msf_seller_properties'])) {
				foreach ($data['msf_seller_properties'] as $msf_seller_property_id => $values) {
					$msf_seller_property = $this->MsLoader->MsfSellerProperty->get($msf_seller_property_id);

					if (!empty($msf_seller_property['required'])) {
						if ('text' === $msf_seller_property['type']) {
							if (empty($values['text_values'][$this->config->get('config_language_id')])) {
								$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$this->config->get('config_language_id')}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
								break;
							} elseif (utf8_strlen($values['text_values'][$this->config->get('config_language_id')]) > 255) {
								$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$this->config->get('config_language_id')}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_length'), $msf_seller_property['name']);
								break;
							}
						} elseif (empty($values['id_values'])) {
							$errors["msf_seller_properties[$msf_seller_property_id][id_values][]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
						}
					}
				}
			}
		}

		return $errors;
	}

	/**
	 * Gets available payment gateways.
	 *
	 * @return    array    $payment_gateways    Available payment gateways (methods).
	 */
	private function _getPaymentGateways()
	{
		$payment_gateways = [];
		$extensions = $this->model_extension_extension->getExtensions('ms_payment');

		foreach ($extensions as $extension) {
			$extension_name = str_replace('ms_pg_', '', $extension['code']);

			$this->load->language('multimerch/payment/' . $extension_name);

			$settings = $this->model_setting_setting->getSetting($extension['code']);

			foreach ($settings as $name => $value) {
				if ((strpos($name, 'payout_enabled') && $value) || (strpos($name, 'fee_enabled') && $value)) {
					$payment_gateways[] = [
						'code' => $extension['code'],
						'text_title' => $this->language->get('text_title'),
						'view' => $this->load->controller('multimerch/payment/' . $extension_name)
					];
					break;
				}
			}
		}

		// Enable `PayPal` settings if `PayPal Adaptive` extension is enabled
		$pg_paypal_enabled = false;
		foreach ($payment_gateways as $payment_gateway) {
			if ($payment_gateway['code'] == 'ms_pg_paypal') {
				$pg_paypal_enabled = true;
			}
		}

		$oc_payment_extensions = $this->model_extension_extension->getExtensions('payment');
		foreach ($oc_payment_extensions as $oc_payment_extension) {
			if (!$pg_paypal_enabled && (string)$oc_payment_extension['code'] === 'ms_pp_adaptive') {
				$this->load->language('multimerch/payment/paypal');

				$payment_gateways[] = [
					'code' => 'ms_pg_paypal',
					'text_title' => $this->language->get('text_title'),
					'view' => $this->load->controller('multimerch/payment/paypal')
				];
			}

			if ((string)$oc_payment_extension['code'] === 'ms_stripe_connect' && $this->config->get($oc_payment_extension['code'] . "_status")) {
				$this->load->language('payment/ms_stripe_connect');

				$payment_gateways[] = [
					'code' => 'ms_stripe_connect',
					'text_title' => $this->language->get('text_title'),
					'view' => $this->load->controller('payment/ms_stripe_connect/connect')
				];
			}
		}

		return $payment_gateways;
	}
}
