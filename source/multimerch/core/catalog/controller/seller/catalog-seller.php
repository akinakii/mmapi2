<?php
class ControllerSellerCatalogSeller extends ControllerSellerCatalog {
	public function __construct($registry) {
		parent::__construct($registry);
		
		$this->language->load('product/category');
		$this->load->model('localisation/country');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
	}
	
	public function index() {
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');
		
		if (isset($this->request->get['sort'])) {
			$order_by = $this->request->get['sort'];
		} else {
			$order_by = 'ms.nickname';
		}
		
		if (isset($this->request->get['order'])) {
			$order_way = $this->request->get['order'];
		} else {
			$order_way = 'ASC';
		}
		
		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
		
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}
		
		$this->data['products'] = array();

		$results = $this->MsLoader->MsSeller->getSellers(
			array(
				'seller_status' => array(MsSeller::STATUS_ACTIVE)
			),
			array(
				'order_by'	=> $order_by,
				'order_way'	=> $order_way,
				'offset'	=> ($page - 1) * $limit,
				'limit'		=> $limit
			)
		);

		$total_sellers = isset($results[0]['total_rows']) ? $results[0]['total_rows'] : 0;

		foreach ($results as $result) {
			$avatar = $this->model_tool_image->resize($result['ms.avatar'] && file_exists(DIR_IMAGE . $result['ms.avatar']) ? $result['ms.avatar'] : 'ms_no_image.jpg', 100, 100);

			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $result['seller_id']));
			$defaults = $this->MsLoader->MsSetting->getSellerDefaults();
			$settings = array_merge($defaults, $seller_settings);

			$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $result['seller_id'], 'address_id' => $settings['slr_ms_address'], 'single' => true]);

			if (!empty($seller_ms_address['city'])) {
				$settings['slr_city'] = utf8_strlen($seller_ms_address['city']) > 20 ? (utf8_substr($seller_ms_address['city'], 0, 18) . '..') : $seller_ms_address['city'];
			}

			if (!empty($seller_ms_address['country_id'])) {
				$country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
				$settings['slr_country'] = (isset($country['name']) ? $country['name'] : '');
			}

			// Rating
			$reviews = [
				'rating' => 0,
				'total' => 0,
				'tooltip' => ''
			];

			if ($this->config->get('msconf_reviews_enable')) {
				$seller_reviews = $this->MsLoader->MsReview->getReviews(['seller_id' => $result['seller_id']]);
				$total_reviews = isset($seller_reviews[0]) ? $seller_reviews[0]['total_rows'] : 0;

				$sum_rating = 0;
				foreach ($seller_reviews as &$seller_review) {
					$sum_rating += $seller_review['rating'];
				}

				$reviews['rating'] = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;
				$reviews['total'] = $total_reviews;
				$reviews['tooltip'] = sprintf($this->language->get('mm_review_rating_summary'), $reviews['rating'], $total_reviews, $this->language->get(1 === (int)$total_reviews ? 'mm_review_rating_review' : 'mm_review_rating_reviews'));
			}

			$this->data['sellers'][] = array(
				'seller_id' => $result['seller_id'],
				'thumb' => $avatar,
				'nickname' => $result['ms.nickname'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['ms.description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $result['seller_id']),
				'settings' => $settings,
				'reviews' => $reviews
			);
		}

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
						
		$this->data['sorts'] = array();
		
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_asc'),
			'value' => 'ms.nickname-ASC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.nickname&order=ASC' . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_desc'),
			'value' => 'ms.nickname-DESC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.nickname&order=DESC' . $url)
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$this->data['limits'] = array();

		$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$this->data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=' . $value)
			);
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $total_sellers;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('seller/catalog-seller', $url . '&page={page}');
	
		$this->data['pagination'] = $pagination->render();
		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($total_sellers) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($total_sellers - 5)) ? $total_sellers : ((($page - 1) * 5) + 5), $total_sellers, ceil($total_sellers / 5));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
			$this->document->addLink($this->url->link('seller/catalog-seller', $url, true), 'canonical');
		} elseif ($page == 2) {
			$this->document->addLink($this->url->link('seller/catalog-seller', $url, true), 'prev');
		} else {
			$this->document->addLink($this->url->link('seller/catalog-seller', $url . '&page='. ($page - 1), true), 'prev');
		}

		if($pagination->limit && ceil($pagination->total / $pagination->limit) > $pagination->page) {
			$this->document->addLink($this->url->link('seller/catalog-seller', $url . '&page='. ($pagination->page + 1)), 'next');
		}

		$this->data['sort'] = $order_by;
		$this->data['order'] = $order_way;
		$this->data['limit'] = $limit;		
		
		$this->data['continue'] = $this->url->link('common/home');

		$this->document->setTitle($this->language->get('ms_catalog_sellers_heading'));
		$this->document->setDescription($this->language->get('ms_catalog_sellers_description'));
		$this->data['h1'] = $this->language->get('ms_catalog_sellers_heading');
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'), 
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
		
	public function profile() {
		if (isset($this->request->get['seller_id'])) {
			$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
		}

		if (!isset($seller) || empty($seller) || $seller['ms.seller_status'] != MsSeller::STATUS_ACTIVE) {
			$this->response->redirect($this->url->link('seller/catalog-seller', '', 'SSL'));
			return;
		}
		
		if ($this->config->get('msconf_holiday_mode_allow')) {
			if ($this->MsLoader->MsSeller->isSellerOnHolidayMode($this->request->get['seller_id'])){
				$this->data['holiday_mode_message'] = $this->MsLoader->MsSeller->getSellerHolidayModeNote($this->request->get['seller_id']);
			} 
		}


		$seller_id = $this->request->get['seller_id'];
		$settings = $this->MsLoader->MsSetting->getSellerSettings(array("seller_id" => $seller_id));
		$default_settings = $this->MsLoader->MsSetting->getSellerDefaults();
		$settings = array_merge($default_settings, $settings);

		$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
		$this->document->addScript('catalog/view/javascript/dialog-sellercontact.js');

		if($this->config->get('msconf_reviews_enable')) {
			$this->MsLoader->MsHelper->addStyle('pagination');
			$this->document->addScript('catalog/view/javascript/pagination.min.js');
			$this->document->addScript('catalog/view/javascript/ms-review.js');

			$this->data['reviews'] = $rating_stats = array();
			$this->data['total_reviews'] = $this->data['rating_stats'] = $this->data['avg_rating'] = 0;

			for($i = 1; $i <= 5; $i++) {
				$rating_stats[$i] = array('votes' => 0, 'percentage' => 0);
			}

			$sum_rating = 0;

			$reviews = $this->MsLoader->MsReview->getReviews(array('seller_id' => $seller_id));
			$total_reviews = isset($reviews[0]) ? $reviews[0]['total_rows'] : 0;

			$this->load->model('account/customer');
			$this->load->model('account/order');
			$this->load->model('catalog/product');

			foreach ($reviews as &$review) {
				$sum_rating += $review['rating'];

				$rating_stats[$review['rating']]['votes'] += 1;

				$review['author'] = $this->model_account_customer->getCustomer($review['author_id']);
				$review['date_created'] = date($this->language->get('date_format_short'), strtotime($review['date_created']));

				foreach ($review['attachments'] as &$attachment) {
					$attachment['fullsize'] = $this->model_tool_image->resize($attachment['attachment'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
					$attachment['thumb'] = $this->model_tool_image->resize($attachment['attachment'], 70, 70);
				}

				$review['product'] = false;

				$reviewed_product = $this->model_account_order->getOrderProduct($review['order_id'], $review['order_product_id']);
				if (!empty($reviewed_product)) {
					$product_exists = $this->model_catalog_product->getProduct($reviewed_product['product_id']);

					$review['product'] = array(
						'name' => $reviewed_product['name'],
						'href' => $product_exists ? $this->url->link('product/product', 'product_id=' . $reviewed_product['product_id']) : false,
						'model' => $reviewed_product['model'],
						'price' => $this->currency->format($reviewed_product['price'])
					);
				}

				$comments = $this->MsLoader->MsReview->getReviewComments($review['review_id']);
				$review['total_comments'] = isset($comments[0]) ? $comments[0]['total_rows'] : 0;
			}

			foreach ($rating_stats as &$rating_stat) {
				$rating_stat['percentage'] = $total_reviews > 0 ? round($rating_stat['votes'] / $total_reviews * 100, 1) : 0;
			}
			krsort($rating_stats);

			$this->data['reviews'] = $reviews;
			$this->data['total_reviews'] = $total_reviews;
			$this->data['rating_stats'] = $rating_stats;
			$this->data['avg_rating'] = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;
			$this->data['feedback_history'] = $this->MsLoader->MsReview->getFeedbackHistory(array('seller_id' => $seller_id));
		}

		if ($seller['ms.avatar'] && file_exists(DIR_IMAGE . $seller['ms.avatar'])) {
			$this->data['seller']['thumb'] = $this->model_tool_image->resize($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
		} else {
			$this->data['seller']['thumb'] = $this->model_tool_image->resize('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
		}

		if ($this->config->get('msconf_enable_seller_banner')) {
			if ($seller['banner'] && file_exists(DIR_IMAGE . $seller['banner'])) {
				$this->data['seller']['banner'] = $this->model_tool_image->resize($seller['banner'], $this->config->get('msconf_product_seller_banner_width'), $this->config->get('msconf_product_seller_banner_height'), 'w');
			}
		}

		$this->data['seller']['settings'] = $settings;
		$this->data['seller']['nickname'] = $seller['ms.nickname'];
		$this->data['seller']['seller_id'] = $seller['seller_id'];
		$this->data['seller']['description'] = html_entity_decode($seller['ms.description'], ENT_QUOTES, 'UTF-8');
		$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id']);

		// load disqus data
		$this->data = array_merge($this->data, $this->load->language('module/multimerch_disqus'));
		$this->data['disqus_identifier'] = 'sid' . $this->request->get['seller_id'];
		$this->data['disqus_url'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $this->request->get['seller_id'], 'SSL');
				
		$products = $this->MsLoader->MsProduct->getProducts(
			array(
				'seller_id' => $seller['seller_id'],
				'language_id' => $this->config->get('config_language_id'),
				'product_status' => array(MsProduct::STATUS_ACTIVE),
				'oc_status' => 1,
				'available' => true
			),
			array(
				'order_by'	=> 'p.date_added',
				'order_way'	=> 'DESC',
				'offset'	=> 0,
				'limit'		=> 6
			)
		);

		if (!empty($products)) {
			foreach ($products as $product) {
				$product_info = $this->model_catalog_product->getProduct($product['product_id']);
				if ($product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
					$image = $this->model_tool_image->resize($product_info['image'],$this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('no_image.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
							
				$this->data['seller']['products'][] = array(
					'product_id' => $product['product_id'],				
					'thumb' => $image,
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'tax' => $tax,
					'name' => $product_info['name'],
					'price' => $price,
					'special' => $special,
					'rating' => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);				
			}
		} else {
			$this->data['seller']['products'] = array();
		}

		$this->data['seller_id'] = $this->request->get['seller_id'];

		$tags = array(
			'[seller_nickname]' => $this->data['seller']['nickname'],
			'[store_name]' => $this->config->get('config_name')
		);

		$this->document->setTitle($this->data['seller']['nickname']);
		$this->data['h1'] = $this->data['seller']['nickname'];

		if (isset($this->data['seller']['description']) AND $this->data['seller']['description']){
			$description_for_meta = $this->MsLoader->MsHelper->generateMetaDescription($this->data['seller']['description']);
			$this->document->setDescription($description_for_meta);
		}
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'), 
			),
			array(
				'text' => sprintf($this->language->get('ms_catalog_seller_profile_breadcrumbs'), $this->data['seller']['nickname']),
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id='.$seller['seller_id'], 'SSL'),
			)
		));

		$this->data['success'] = !empty($this->session->data['success']) ? $this->session->data['success'] : FALSE;
		unset($this->session->data['success']);   
		$this->document->addLink($this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']), 'canonical');

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller-profile');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function products() {
		// Url params
		$url_params = $this->getUrlParams($this->request->get);

		// Current sort, order and limit params
		$this->data['sort'] = $url_params['order_by'];
		$this->data['order'] = $url_params['order_way'];
		$this->data['limit'] = $url_params['limit'];

		// Seller data
		$this->data['seller']['seller_id'] = $this->request->get['seller_id'];
		$this->data['seller']['nickname'] = $this->MsLoader->MsSeller->getSellerNickname($this->data['seller']['seller_id']);

		// Seller products
		list($this->data['seller']['products'], $total_products) = $this->getProducts($this->request->get, $url_params);

		// Limits
		$this->data['limits'] = $this->getLimits($this->request->get);

		// Sorts
		$this->data['sorts'] = $this->getSorts($this->request->get);

		// Products pagination
		$this->data['pagination'] = $this->getPagination($this->request->get, $url_params, $total_products);

		// Products count
		$this->data['results'] = $this->getProductsCount($url_params, $total_products);

		// Breadcrumbs
		$breadcrumbs = [
			[
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'),
			],
			[
				'text' => $this->data['seller']['nickname'],
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id='.$this->data['seller']['seller_id'], 'SSL'),
			],
			[
				'text' => $this->language->get('ms_store'),
				'href' => $this->url->link('seller/catalog-seller/products', '&seller_id='.$this->data['seller']['seller_id'], 'SSL'),
			]
		];

		$this->addSeo($this->request->get, $breadcrumbs, $this->data);

		if ($this->config->get('msconf_holiday_mode_allow')) {
			if ($this->MsLoader->MsSeller->isSellerOnHolidayMode($this->data['seller']['seller_id'])){
				$this->data['holiday_mode_message'] = $this->MsLoader->MsSeller->getSellerHolidayModeNote($this->data['seller']['seller_id']);
			} 
		}

		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs($breadcrumbs);
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller-products');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	private function getUrlParams($request)
	{
		$params = [
			'order_by' => isset($request['sort']) && in_array($request['sort'], ['pd.name-ASC', 'pd.name-DESC', 'pd.name']) ? $request['sort'] : 'pd.name',
			'order_way' => isset($request['order']) ? $request['order'] : 'ASC',
			'page' => isset($request['page']) ? $request['page'] : 1,
			'limit' => isset($request['limit']) ? $request['limit'] : $this->config->get($this->config->get('config_theme') . '_product_limit'),
			'filters' => isset($request['search']) ? ['pd.name' => $request['search']] : []
		];

		return $params;
	}

	private function getProducts($request, $url_params)
	{
		$products = [];
		$total_products = 0;

		$seller_products = $this->MsLoader->MsProduct->getProducts(
			[
				'seller_id' => $request['seller_id'],
				'language_id' => $this->config->get('config_language_id'),
				'category_id' => isset($request['category_id']) ? $request['category_id'] : null,
				'ms_category_id' => isset($request['ms_category_id']) ? $request['ms_category_id'] : null,
				'product_status' => [MsProduct::STATUS_ACTIVE],
				'oc_status' => 1,
				'available' => true
			],
			[
				'order_by'	=> $url_params['order_by'],
				'order_way'	=> $url_params['order_way'],
				'offset'	=> ($url_params['page'] - 1) * $url_params['limit'],
				'limit'		=> $url_params['limit'],
				'filters'	=> $url_params['filters']
			]
		);

		if (!empty($seller_products)) {
			$total_products = $seller_products[0]['total_rows'];

			foreach ($seller_products as $product) {
				$product_data = $this->model_catalog_product->getProduct($product['product_id']);

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				$image = $this->model_tool_image->resize($product_data['image'] && file_exists(DIR_IMAGE . $product_data['image']) ? $product_data['image'] : 'no_image.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				$special = (float)$product_data['special'] ? $this->currency->format($this->tax->calculate($product_data['special'], $product_data['tax_class_id'], $this->config->get('config_tax'))) : false;
				$rating = $this->config->get('config_review_status') ? $product_data['rating'] : false;
				$tax = $this->config->get('config_tax') ? $this->currency->format((float)$product_data['special'] ? $product_data['special'] : $product_data['price']) : false;

				$products[] = array(
					'product_id'	=> $product['product_id'],
					'thumb'			=> $image,
					'name'			=> $product_data['name'],
					'price'			=> $price,
					'tax'			=> $tax,
					'description'	=> utf8_substr(strip_tags(html_entity_decode($product['pd.description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length') ? $this->config->get('config_product_description_length') : $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'special'		=> $special,
					'rating'		=> $rating,
					'reviews'		=> sprintf($this->language->get('text_reviews'), (int)$product_data['reviews']),
					'href'			=> $this->url->link('product/product', 'product_id=' . $product_data['product_id'])
				);
			}
		}

		return [$products, $total_products];
	}

	private function getSorts($request)
	{
		$sorts = [];

		// Generate query string by GET params for sorts list
		$url = '';
		foreach (['limit', 'ms_category_id'] as $param) {
			if (isset($request[$param]))
				$url .= "&{$param}={$request[$param]}";
		}

		$sort_values = [
			[
				'text'	=> $this->language->get('ms_sort_nickname_asc'),
				'value'	=> 'pd.name-ASC',
				'href'	=> $this->url->link('seller/catalog-seller/products', $url . '&sort=pd.name&order=ASC&seller_id=' . $request['seller_id'])
			],
			[
				'text'	=> $this->language->get('ms_sort_nickname_desc'),
				'value'	=> 'pd.name-DESC',
				'href'	=> $this->url->link('seller/catalog-seller/products', $url . '&sort=pd.name&order=DESC&seller_id=' . $request['seller_id'])
			],
		];

		foreach ($sort_values as $sort_value) {
			$sorts[] = [
				'text'	=> $sort_value['text'],
				'value'	=> $sort_value['value'],
				'href'	=> $sort_value['href']
			];
		}

		return $sorts;
	}

	private function getLimits($request)
	{
		$limits = [];

		// Generate query string by GET params for limits list
		$url = '';
		foreach (['sort', 'order', 'ms_category_id'] as $param) {
			if (isset($request[$param]))
				$url .= "&{$param}={$request[$param]}";
		}

		$limit_values = array_unique([$this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100]);
		sort($limit_values);

		foreach ($limit_values as $limit_value) {
			$limits[] = [
				'text'  => $limit_value,
				'value' => $limit_value,
				'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=' . $limit_value . '&seller_id=' . $request['seller_id'])
			];
		}

		return $limits;
	}

	private function getPagination($request, $url_params, $total_products)
	{
		$pagination_rendered = '';

		$url = '';
		foreach (['sort', 'order', 'limit', 'ms_category_id'] as $param) {
			if (isset($request[$param]))
				$url .= "&{$param}={$request[$param]}";
		}

		$pagination = new Pagination();
		$pagination->total = $total_products;
		$pagination->page = $url_params['page'];
		$pagination->limit = $url_params['limit'];
		$pagination->url = $this->url->link('seller/catalog-seller/products', $url . '&seller_id=' . $request['seller_id'] . '&page={page}');

		$pagination_rendered = $pagination->render();

		$this->document->addLink($this->url->link('seller/catalog-seller/products', '&seller_id=' . $request['seller_id']), 'canonical');

		if ($pagination->limit && ceil($pagination->total / $pagination->limit) > $pagination->page)
			$this->document->addLink($this->url->link('seller/catalog-seller/products', $url . '&seller_id=' . $request['seller_id'] . '&page=' . ($pagination->page + 1)), 'next');

		if ($pagination->page > 1)
			$this->document->addLink($this->url->link('seller/catalog-seller/products', $url . '&seller_id=' . $request['seller_id'] . '&page=' . ($pagination->page - 1)), 'prev');

		return $pagination_rendered;
	}

	private function getProductsCount($url_params, $total_products)
	{
		return sprintf($this->language->get('text_pagination'), ($total_products) ? (($url_params['page'] - 1) * $url_params['limit']) + 1 : 0, ((($url_params['page'] - 1) * $url_params['limit']) > ($total_products - $url_params['limit'])) ? $total_products : ((($url_params['page'] - 1) * $url_params['limit']) + $url_params['limit']), $total_products, ceil($total_products / $url_params['limit']));
	}

	private function addSeo($request, &$breadcrumbs, &$data)
	{
		if (isset($request['ms_category_id'])){
			$current_ms_category = $this->MsLoader->MsCategory->getCategories([
				'category_id' => $request['ms_category_id'],
				'single' => true,
				'ms_path' => true
			]);

			$ms_path = explode(',', isset($current_ms_category['ms_path']) ? $current_ms_category['ms_path'] : '');

			foreach ($ms_path as $category_id) {
				$ms_cat = $this->MsLoader->MsCategory->getCategories([
					'category_id' => $category_id,
					'single' => true
				]);

				if (!empty($ms_cat['name'])) {
					$breadcrumbs[] = [
						'text' => $ms_cat['name'],
						'href' => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $request['seller_id'] . '&ms_category_id=' . $category_id)
					];
				}
			}

			$data['h1'] = isset($current_ms_category['name']) ? $current_ms_category['name'] : '';

			$this->document->setTitle(!empty($current_ms_category['meta_title']) ? $current_ms_category['meta_title'] : isset($current_ms_category['name']) ? $current_ms_category['name'] : '');

			if (!empty($current_ms_category['meta_description']))
				$this->document->setDescription($current_ms_category['meta_description']);

			if (!empty($current_ms_category['meta_keyword']))
				$this->document->setKeywords($current_ms_category['meta_keyword']);

		} else {
			$data['h1'] = $data['seller']['nickname'];

			$this->document->setTitle(sprintf($this->language->get('ms_catalog_seller_products_heading'), $data['seller']['nickname']));

			if (!empty($data['seller']['description']))
				$this->document->setDescription($this->MsLoader->MsHelper->generateMetaDescription($data['seller']['description']));
		}
	}

	public function jxSubmitContactDialog() {
		if (!$this->customer->getId() || ($this->customer->getId() == $this->request->post['seller_id']))
			return;

		/*$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();*/

		$seller_id = $this->request->post['seller_id'];
		$product_id = $this->request->post['product_id'];
		$message_text = trim($this->request->post['ms-sellercontact-text']);
		$customer_name = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();

		/*$seller_email = $this->MsLoader->MsSeller->getSellerEmail($seller_id);
		$seller_name = $this->MsLoader->MsSeller->getSellerName($seller_id);
		$customer_email = $this->customer->getEmail();*/

		if ($product_id) {
			$product = $this->MsLoader->MsProduct->getProduct($product_id);
			$product_name = $product['languages'][$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'))]['name'];
		}

		$title = $product_id ? sprintf($this->language->get('ms_conversation_title_product'), $product_name) : sprintf($this->language->get('ms_conversation_title'), $customer_name);

		$json = array();
		if (empty($message_text)) {
			$json['errors'][] = $this->language->get('ms_error_contact_allfields');
		}

		if (utf8_strlen($message_text) > 2000) {
			$json['errors'][] = $this->language->get('ms_error_contact_text');
		}

		if (!isset($json['errors'])) {
			if ($this->config->get('mmess_conf_enable') == 1) {
				$conversation_id = $this->MsLoader->MsConversation->createConversation(
					array(
						'product_id' => $product_id,
						'title' => $title,
						'conversation_from' => $this->customer->getId(),
					)
				);

				$this->MsLoader->MsConversation->addConversationParticipants($conversation_id, [$this->customer->getId(), $seller_id]);

				$message_id = $this->MsLoader->MsMessage->createMessage(
					array(
						'conversation_id' => $conversation_id,
						'from' => $this->customer->getId(),
						'message' => $message_text
					)
				);

				/*$MailSellerPrivateMessage = $serviceLocator->get('MailSellerPrivateMessage', false)
					->setTo($seller_email)
					->setData(array(
						'customer_name' => $customer_name,
						'customer_message' => $message_text,
						'title' => $title,
						'product_id' => $product_id,
						'addressee' => $seller_name
					));
				$mails->add($MailSellerPrivateMessage);
				$mailTransport->sendMails($mails);*/

				$this->MsHooks->triggerAction("customer_created_message", [
					'producer' => "customer.{$this->customer->getId()}",
					'consumers' => ["seller.{$seller_id}"],
					'object' => [
						'type' => 'message',
						'id' => $message_id,
						'action' => 'created'
					]
				]);

				$json['success'] = $this->language->get('ms_sellercontact_success');
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
