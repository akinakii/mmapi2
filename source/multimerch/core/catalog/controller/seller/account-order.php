<?php

class ControllerSellerAccountOrder extends ControllerSellerAccount {
	public $data = array();

	public function __construct($registry) {
		parent::__construct($registry);

		// load default OC language file for orders
		$this->data = array_merge($this->data
			, $this->load->language('account/order')
			, $this->load->language('multiseller/multiseller'));

		$this->load->model('account/order');
		$this->load->model('tool/upload');
		$this->load->model('localisation/country');
		$this->load->model('tool/image');
		$this->load->model('localisation/order_status');
		$this->load->model('extension/total/shipping');
		$this->load->model('checkout/order');
		$this->load->model('extension/total/coupon');
	}

	public function getTableData() {
		$colMap = [
			'suborder_status' => 'status'
		];
		
		$sorts = array('order_id', 'customer_name', 'date_added', 'suborder_total');
		$filters = array_merge($sorts, array('products'));
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);
		
		$seller_id = $this->customer->getId();
		$orders = $this->MsLoader->MsSuborder->getSellerSubordersList($seller_id, [], [
			'order_by'  => $sortCol,
			'order_way' => $sortDir,
			'offset' => $this->request->get['iDisplayStart'],
			'limit' => $this->request->get['iDisplayLength'],
			'filters' => $filterParams
		]);

		$total_orders = isset($orders[0]) ? $orders[0]['total_rows'] : 0;

		$columns = array();
		foreach ($orders as $order) {
			$suborder_products = $this->MsLoader->MsSuborder->getSuborderProductsCompact($order['order_id'], $seller_id);

			$products = "";
			foreach ($suborder_products as $product) {
				$products .= "<p style='text-align:left'>";
				$products .= "<span class='name'>" . ($product['quantity'] > 1 ? "{$product['quantity']} x " : "") . "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id'], 'SSL') . "'>{$product['name']}</a></span>";

				//$products .= "<span class='total'>{$p['total']}</span>";
				$products .= "</p>";
			}

			$actions = '<a class="icon-view" href="' . $this->url->link('seller/account-order/viewOrder', 'order_id=' . $order['order_id'], 'SSL') . '" title="' . $this->language->get('ms_view_modify') . '"><i class="fa fa-search"></i></a>';
			//$actions .= '<a class="icon-invoice" target="_blank" href="' . $this->url->link('seller/account-order/invoice', 'order_id=' . $order['order_id'], 'SSL') . '" title="' . $this->language->get('ms_view_invoice') . '"><i class="fa fa-file-text-o"></i></a>';

			$columns[] = array_merge(
				$order,
				array(
					'order_id' => $order['order_id'],
					'customer_name' => $order['customer_name'],
					'products' => $products,
					'suborder_status' => $order['status'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
					'suborder_total' => $this->currency->format($order['suborder_total'], $this->config->get('config_currency')),
					'view_order' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total_orders,
			'iTotalDisplayRecords' => $total_orders,
			'aaData' => $columns
		)));
	}

	private function _getOrderCommonData() {
		$order_id = isset($this->request->get['order_id']) ? (int)$this->request->get['order_id'] : 0;
		$seller_id = $this->customer->getId();
		$order_info = $this->model_account_order->getOrder($order_id, 'seller');
		$suborder_products = $this->getSuborderProducts($order_info, $seller_id);
		$suborder_totals = $this->MsLoader->MsSuborder->getSuborderTotals($order_id, $seller_id);

		// Redirect if there is no information about order, it does not contain any products or totals were not calculated
		if (!$order_info || empty($suborder_products) || empty($suborder_totals)) {
			$this->response->redirect($this->url->link('seller/account-order', '', 'SSL'));
		}

		$this->data['order_id'] = $order_id;
		$this->data['order_info'] = $order_info;
		$this->data['products'] = $suborder_products;
		$this->data['totals'] = $suborder_totals;

		// Indicates whether suborder has ms_shipping
		$this->data['mm_shipping_flag'] = isset($suborder_totals['mm_shipping_total']['value']) && $suborder_totals['mm_shipping_total']['value'] > 0;

		// Suborder statuses names
		$this->data['order_statuses'] = $this->MsLoader->MsSuborderStatus->getMsSuborderStatuses(array('language_id' => $this->config->get('config_language_id')));

		// Suborder information
		$suborder = $this->MsLoader->MsSuborder->getSuborders(array(
			'order_id' => $order_id,
			'seller_id' => $seller_id,
			'single' => 1
		));

		$this->data['suborder_id'] = isset($suborder['suborder_id']) ? $suborder['suborder_id'] : '';
		$this->data['order_status_id'] = isset($suborder['order_status_id']) ? $suborder['order_status_id'] : 0;
		$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

		// OC way of displaying addresses and invoices
		$this->data['invoice_no'] = isset($order_info['invoice_no']) ? $order_info['invoice_prefix'] . $order_info['invoice_no'] : '';
	}
	public function printPackingSlip(){
		$this->_getOrderCommonData();
		$this->MsLoader->MsPrinter->printOrderPackingSlip($this->data['order_id'], $this->data['suborder_id']);
	}
	
	public function viewOrder() {
		$this->_getOrderCommonData();
		$this->data['customer'] = array();

		if (isset($this->data['order_info']['customer_id']) AND $this->data['order_info']['customer_id']){
			$this->load->model('account/customer');
			$this->data['customer'] = $this->model_account_customer->getCustomer($this->data['order_info']['customer_id']);
		}

		$this->_loadAddressData(array("payment", "shipping"), $this->data['order_info']);

		// Suborder transactions
		$this->data['suborder_transactions'] = $this->MsLoader->MsBalance->getBalanceEntries(array(
			'seller_id' => $this->customer->getId(),
			'order_id' => $this->data['order_id']
		));

		// Suborder history entries
		$this->data['order_history'] = $this->MsLoader->MsSuborder->getSuborderHistory(array('suborder_id' => $this->data['suborder_id']));

		// render
		$this->data['link_back'] = $this->url->link('seller/account-order', '', 'SSL');
		$this->data['link_print'] = $this->url->link('seller/account-order/printPackingSlip', 'order_id=' . $this->data['order_id'], 'SSL');
		$this->data['continue'] = $this->url->link('account/order', '', 'SSL');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_orders_breadcrumbs'),
				'href' => $this->url->link('seller/account-order', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_order_column_order_id') . $this->data['suborder_id'],
				'href' => $this->url->link('seller/account-order/viewOrder', ['order_id' => $this->data['order_id']], 'SSL')
			)
		));

		$this->document->setTitle($this->language->get('text_order'));
		$this->document->addScript('catalog/view/javascript/multimerch/account-message.js');
		$this->MsLoader->MsHelper->addStyle('multimerch_messaging');

		list($customerBlockTemplate) = $this->MsLoader->MsHelper->loadTemplate('account-order-info-customer-block', null);
		$this->data['customer_block'] = $this->load->view($customerBlockTemplate, $this->data);
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order-info');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}
	
	public function invoice() {
		$this->_getOrderCommonData();

		// Get seller settings
		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $this->customer->getId()));
		$defaults = $this->MsLoader->MsSetting->getSellerDefaults();
		$this->data['settings'] = array_merge($defaults, $seller_settings);

		$seller_ms_address = $this->MsLoader->MsSeller->getSellerMsAddress(['seller_id' => $this->customer->getId(), 'address_id' => $this->data['settings']['slr_ms_address'], 'single' => true]);
		if (!empty($seller_ms_address['country_id']))
			$this->data['settings']['slr_country'] = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);

		$this->data['logo'] = is_file(DIR_IMAGE . $this->data['settings']['slr_logo']) ? $this->MsLoader->MsFile->resizeImage($this->data['settings']['slr_logo'], 80, 80) : '';
		$this->data['logo'] = is_file(DIR_IMAGE . $this->data['settings']['slr_logo']) ? $this->model_tool_image->resize($this->data['settings']['slr_logo'], 80, 80) : '';

		$this->_loadAddressData(array("payment"), $this->data['order_info']);

		// custom styles
		$this->MsLoader->MsHelper->addStyle('multimerch/invoice/default');
		$this->MsLoader->MsHelper->addStyle('stylesheet');

		// OC's default header things
		$this->data['base'] = $this->request->server['HTTPS'] ? $this->config->get('config_ssl') : $this->config->get('config_url');
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		$this->data['title'] = $this->language->get('heading_invoice_title');

		// load template parts
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/invoice/header',array());
		$head = $this->load->view($template, array_merge($this->data, $children));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/invoice/body-default',array());
		$body = $this->load->view($template, array_merge($this->data, $children));

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multiseller/invoice/footer',array());
		$foot = $this->load->view($template, array_merge($this->data, $children));

		// render
		$this->response->setOutput($head . $body . $foot);
	}
		
	public function index() {
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('ms_account_order_information'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_orders_breadcrumbs'),
				'href' => $this->url->link('seller/account-order', '', 'SSL'),
			)
		));
		
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account-order');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxAddHistory() {
		if (empty($this->request->post['order_comment']) && empty($this->request->post['order_status']) || !isset($this->request->post['suborder_id']))
			return false;

		if (!$this->_validate($this->request->post['suborder_id']))
			return false;

		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		// keep current status if not changing explicitly
		$suborderData = $this->MsLoader->MsSuborder->getSuborders(array(
			'suborder_id' => (int)$this->request->post['suborder_id'],
			'single' => 1
		));

		$suborder_status_id = $this->request->post['order_status'] ? (int)$this->request->post['order_status'] : $suborderData['order_status_id'];

		$this->MsLoader->MsSuborder->updateSuborderStatus(array(
			'suborder_id' => (int)$this->request->post['suborder_id'],
			'order_status_id' => $suborder_status_id
		));

		$this->MsLoader->MsSuborder->addSuborderHistory(array(
			'suborder_id' => (int)$this->request->post['suborder_id'],
			'comment' => $this->request->post['order_comment'],
			'order_status_id' => $suborder_status_id
		));

		// get customer information
		$order_info = $this->model_checkout_order->getOrder($suborderData['order_id']);

		$seller = $this->MsLoader->MsSeller->getSeller($this->customer->getId());
		$MailOrderUpdated = $serviceLocator->get('MailOrderUpdated', false)
			->setTo($order_info['email'])
			->setData(array(
				//'addressee' => $this->registry->get('customer')->getFirstname(),
				'order_status' => $this->MsLoader->MsSuborderStatus->getSubStatusName(array('order_status_id' => $suborder_status_id)),
				'order_comment' => $this->request->post['order_comment'],
				'order_id' => $suborderData['order_id'],
				'seller_nickname' => $seller['ms.nickname'],
				'order_products' => $this->MsLoader->MsOrderData->getOrderProducts(array('order_id' => $suborderData['order_id'], 'seller_id' => $this->customer->getId()))
			));
		$mails->add($MailOrderUpdated);

		$mailTransport->sendMails($mails);

		// Create balance entries for seller if needed
		$this->MsLoader->MsTransaction->processSuborderTransactions($suborderData['order_id'], $this->customer->getId(), $suborder_status_id);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('success' => 1)));
	}

	private function _loadAddressData($types, $order_info) {
		foreach ($types as $key => $type) {
			$address_data_keys = array(
				'_firstname',
				'_lastname',
				'_company',
				'_address_1',
				'_address_2',
				'_city',
				'_postcode',
				'_zone',
				'_zone_code',
				'_country',
			);

			foreach ($address_data_keys as $address_data_key) {
				$this->data[$type . $address_data_key] = $order_info[$type . $address_data_key];
			}

			$this->data[$type . '_method'] = $order_info[$type . '_method'];
			$this->data[$type . '_address'] = $this->MsLoader->MsHelper->formatAddressByOrderInfo($type, $order_info);
		}

		$this->data['telephone'] = $order_info['telephone'];
	}

	private function _validate($suborder_id) {
		return $this->MsLoader->MsSuborder->isValidSeller($suborder_id, $this->customer->getId());
	}

	private function getSuborderProducts($order_info, $seller_id)
	{
		$this->load->model('tool/image');
		$order_id = $order_info['order_id'];

		$products = $this->MsLoader->MsSuborder->getSuborderProductsCompact($order_id, $seller_id, ['full' => true]);

		$result = [];

		foreach ($products as $product) {
			// Order product MSF variations
			$msf_variation_data = [];

			$msf_variations = $this->MsLoader->MsOrderData->getOrderProductMsfVariations($order_id, $product['order_product_id']);
			foreach ($msf_variations as $msf_variation) {
				$msf_variation_data[] = [
					'name' => $msf_variation['name'],
					'value' => $msf_variation['value']
				];
			}

			// Order product options
			$option_data = [];

			$options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);
			foreach ($options as $option) {
				$o = array(
					'name' => $option['name'],
					'type' => $option['type'],
					'value' => $option['value']
				);

				if ($option['type'] == 'file') {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
					if ($upload_info) {
						$o['value'] = $upload_info['name'];
						$o['href'] = $this->url->link('account/msconversation/downloadAttachment', 'code=' . $upload_info['code'], true);
					}
				}

				$option_data[] = $o;
			}

			$shipping_cost = !empty($product['individual_shipping_cost']) ? (float)$product['individual_shipping_cost'] : (float)$product['combined_shipping_cost'];
			$shipping_method = !empty($product['individual_shipping_method_name']) ? (string)$product['individual_shipping_method_name'] : (string)$product['combined_shipping_method_name'];

			// Product image
			$thumb = $this->MsLoader->MsProduct->getProductThumbnail($product['product_id']);
			$result[] = array(
				'product_id' => $product['product_id'],
				'name' => $product['name'],
				'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'], 'SSL'),
				'model' => $product['model'],
				'msf_variation' => $msf_variation_data,
				'option' => $option_data,
				'quantity' => $product['quantity'],
				'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
				'shipping_cost' => $this->currency->format($shipping_cost, $order_info['currency_code'], $order_info['currency_value']),
				'shipping_method' => !empty($shipping_method) ? " <i>(" . sprintf($this->language->get('mm_account_order_shipping_via'), $shipping_method) . ")</i>" : '',
				'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0) + $shipping_cost, $order_info['currency_code'], $order_info['currency_value']),
				'return' => $this->url->link('account/return/insert', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL'),
				'image' => $this->model_tool_image->resize(isset($thumb['image']) && !empty($thumb['image']) ? $thumb['image'] : 'no_image.png', 40, 40)
			);
		}

		return $result;
	}
}
