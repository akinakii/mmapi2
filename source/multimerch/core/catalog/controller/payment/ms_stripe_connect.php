<?php
class ControllerPaymentMsStripeConnect extends Controller {
	use \MultiMerch\Module\Traits\Stripe;

	// @todo 9.0: static var
	protected $module_name = 'ms_stripe_connect';

	/**
	 * @var array Module data.
	 */
	protected $data = [];

	/**
	 * @var array Module settings.
	 */
	protected $settings = [];

	/**
	 * @var int Detects whether Stripe Connect is in test or live mode. By default, 0 means live, 1 means test.
	 */
	protected $env = 0;

	/**
	 * @var null Current seller id.
	 */
	protected $seller_id;

	/**
	 * @var \MultiMerch\Core\Invoice\Manager Invoice manager.
	 */
	protected $invoice_manager;

	/**
	 * ControllerPaymentMsStripeConnect constructor.
	 *
	 * @param $registry
	 */
	public function __construct($registry) {
		parent::__construct($registry);

		$this->validateModuleStatus();

		$this->seller_id = $this->customer->getId() ?: null;

		$this->invoice_manager = new \MultiMerch\Core\Invoice\Manager($registry);

		$this->load->model('tool/image');
		$this->load->model('setting/setting');
		$this->load->model('checkout/order');

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'), $this->load->language('payment/ms_stripe_connect'));

		try {
			list($this->settings, $this->env) = $this->initStripe();
		} catch (\MultiMerch\Module\Errors\Settings $e) {
			$this->ms_logger->error($e->getMessage());
		}
	}

	/**
	 * Renders Stripe Checkout button at the checkout confirm step.
	 *
	 * @return view
	 */
	public function index() {
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		if ($this->config->get('ms_stripe_connect_sca_required')) {
			return $this->init_checkout_sca($order_info);
		} else {
			$this->data['pay_link'] = $this->url->link('payment/ms_stripe_connect/pay', '', 'SSL');
			$this->data['public_key'] = $this->settings['environments'][$this->env]['public_key'];
			$this->data['store_name'] = $this->config->get('config_name');
			$this->data['store_description'] = $this->config->get('config_meta_description');

			$this->data['order_info'] = $order_info;
			$this->data['total'] = !empty($order_info['total']) ? $this->currency->format((float)$order_info['total'], $this->config->get('ms_stripe_connect_account_currency'), '', false) * 100 : 0;
			$this->data['currency'] = $this->config->get('ms_stripe_connect_account_currency');

			/*$this->data['cards'] = [];

			if ($this->customer->isLogged()) {
				// Stripe customer
				$stripe_customer_id = $this->MsLoader->MsSetting->getCustomerSettings([
					'customer_id' => $this->customer->getId(),
					'name' => 'cst_stripe_customer_id'
				]);

				if ($stripe_customer_id) {
					$customer = \Stripe\Customer::retrieve($stripe_customer_id);

					if (!empty($customer->sources)) {
						foreach ($customer->sources as $source) {
							$this->data['cards'][] = [
								'last4' => $source->last4,
								'exp_month' => $source->exp_month,
								'exp_year' => $source->exp_year,
								'brand' => $source->brand
							];
						}
					}
				}
			}

			return $this->load->view('payment/ms_stripe_connect_new', $this->data);*/

			return $this->load->view('payment/ms_stripe_connect', $this->data);
		}
	}

	private function init_checkout_sca($order_info)
	{
		$order_id = $this->session->data['order_id'];

		// Line items in order
		$line_items = [];

		// Products as line items
		$order_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);
		foreach ($order_products as $order_product) {
			$line_items[] = [
				'name' => $order_product['name'],
				'amount' => round($order_product['price'], 2) * 100,
				'currency' => $order_info['currency_code'],
				'quantity' => $order_product['quantity']
			];
		}

		// Some order totals as line items
		$this->load->model('account/order');
		$order_totals = $this->model_account_order->getOrderTotals($order_id);
		foreach ($order_totals as $order_total) {
			// Total as line item only available if it is OC tax, OC/MM shipping, OC/MM coupons
			if (!in_array($order_total['code'], ['tax', 'shipping', 'mm_shipping_total', 'coupon', 'ms_coupon'])) {
				continue;
			}

			$line_items[] = [
				'name' => $order_total['title'],
				'amount' => round($order_total['value'], 2) * 100,
				'currency' => $order_info['currency_code'],
				'quantity' => 1
			];
		}

		// Metadata for invoices
		$metadata = [
			'order_id' => $order_id,
			'mm_order_info_sca' => ""
		];

		$mm_order_info_sca = [];
		$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id, 'include_abandoned' => true]);
		foreach ($suborders as $suborder) {
			// Get seller amount excluding commissions
			$suborder_total = !empty($suborder['totals']['sub_total']['value']) ? (float)$suborder['totals']['sub_total']['value'] : 0;

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// Include seller shipping
			if (!empty($suborder['totals']['mm_shipping_total']['value'])) {
				$suborder_total += (float)$suborder['totals']['mm_shipping_total']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			// Subtract suborder (per-seller) commissions
			$amount = $suborder_total - (float)$suborder['store_commission_flat'] - (float)$suborder['store_commission_pct'];
			$commission_total = (float)$suborder['store_commission_flat'] + (float)$suborder['store_commission_pct'];

			$suborder_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $suborder['seller_id']]);
			foreach ($suborder_products as $suborder_product) {
				// Subtract suborder product (per-product) commissions
				$amount -= ((float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct']);
				$commission_total += (float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct'];
			}

			$mm_order_info_sca[] = "{$suborder['seller_id']}_{$amount}_{$commission_total}_{$order_info['currency_code']}";
		}

		if (!empty($mm_order_info_sca)) {
			$metadata['mm_order_info_sca'] = implode('|', $mm_order_info_sca);
		}

		// Create checkout session
		$session = \Stripe\Checkout\Session::create([
			'payment_method_types' => ['card'],
			'line_items' => $line_items,
			'customer_email' => $this->customer->getEmail(),
			'payment_intent_data' => [
				'metadata' => $metadata
			],
			'success_url' => $this->url->link('checkout/success', '', 'SSL'),
			'cancel_url' => $this->url->link('checkout/checkout', '', 'SSL'),
		]);

		$this->data['stripe_checkout_session_id'] = $session->id;
		$this->data['stripe_pk'] = $this->settings['environments'][$this->env]['public_key'];

		return $this->load->view('payment/ms_stripe_connect_sca', $this->data);
	}

	/**
	 * Charges customer's card and creates separate transfers to sellers.
	 * @todo 9.0: save customer's card information in checkout into \Stripe\Customer object and create `ms_customer_settings` table
	 *
	 * @return void
	 */
	public function pay() {
		$data = $this->request->post;

		$this->ms_logger->debug("payment/ms_stripe_connect/pay - Response from Stripe API after submitting checkout form: " . print_r($data, true));

		$order_id = !empty($this->session->data['order_id']) ? (int)$this->session->data['order_id'] : null;

		try {
			if (empty($data['stripeToken']))
				throw new \MultiMerch\Module\Errors\Generic($this->language->get('error_checkout_token'));

			if (!$order_id)
				throw new \MultiMerch\Module\Errors\Generic($this->language->get('error_no_order'));

			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Order id: " . $order_id);

			$order_info = $this->model_checkout_order->getOrder($order_id);

			if (empty($order_info))
				throw new \MultiMerch\Module\Errors\Generic($this->language->get('error_no_order'));

			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Order info: " . print_r($order_info, true));

			/**
			 * Flows of funds using different charging approaches:
			 * - direct - https://stripe.com/docs/connect/direct-charges#flow-of-funds-with-fees
			 * - destination - https://stripe.com/docs/connect/destination-charges#flow-of-funds-with-fees
			 * - separate charge and transfers - https://stripe.com/docs/connect/charges-transfers#collecting-fees
			 *
			 * Using destination and separate charging approaches means charged funds are initially moving to the platform,
			 * and then are transferred to the connected accounts (`charge.succedeed` + `transfer.created` events).
			 * Platform is responsible for Stripe fees.
			 *
			 * Using direct charging approach means charged funds are moving straight to the connected accounts
			 * (`charge.succedeed` event). Connected account is responsible for Stripe fees.
			 */

			switch ($this->config->get('ms_stripe_connect_charging_approach')) {
				case 'destination':
					$this->create_destination_charges($data['stripeToken'], $order_info);
					break;

				case 'separate':
					$this->create_separate_charge_and_transfers($data['stripeToken'], $order_info);
					break;

				case 'direct':
				default:
					$this->create_direct_charges($data['stripeToken'], $order_info);
					break;
			}

			// Set initial main order status
			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$this->ms_logger->error('payment/ms_stripe_connect/pay - Generic - ' . $e->getMessage());
			$this->session->data['error_warning'] = $e->getMessage();

			return $this->response->redirect($this->url->link('account/order', '', 'SSL'));
		} catch (\Stripe\Error\InvalidRequest $e) {
			$this->ms_logger->error('payment/ms_stripe_connect/pay - InvalidRequest - ' . $e->getMessage());
			$this->session->data['error_warning'] = $e->getMessage();

			// Change main order status to failed
			$this->model_checkout_order->addOrderHistory($order_id, isset($this->settings['event_to_order_status']['charge.failed']) ? $this->settings['event_to_order_status']['charge.failed'] : $this->config->get('config_order_status_id'));

			return $this->response->redirect($this->url->link('account/order', '', 'SSL'));
		} catch (\Stripe\Error\Card $e) {
			$this->ms_logger->error('payment/ms_stripe_connect/pay - \Stripe\Error\Card - ' . $e->getMessage());
			$this->session->data['error_warning'] = $e->getMessage();

			// Change main order status to failed
			$this->model_checkout_order->addOrderHistory($order_id, isset($this->settings['event_to_order_status']['charge.failed']) ? $this->settings['event_to_order_status']['charge.failed'] : $this->config->get('config_order_status_id'));

			return $this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$this->ms_logger->error('payment/ms_stripe_connect/pay - ApiErrorException - ' . $e->getMessage());
			$this->session->data['error'] = $this->language->get('error_card');

			// Change main order status to failed
			$this->model_checkout_order->addOrderHistory($order_id, isset($this->settings['event_to_order_status']['charge.failed']) ? $this->settings['event_to_order_status']['charge.failed'] : $this->config->get('config_order_status_id'));

			return $this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
		}

		$this->ms_logger->debug("payment/ms_stripe_connect/pay - Finished creating charge and transfers. Waiting for webhooks.");

		$this->response->redirect($this->url->link('checkout/success', '', true));
	}

	/**
	 * Allows to make charges on your platform account on behalf of connected accounts, perform transfers separately,
	 * and retain funds in the process.
	 *
	 * Creates single charge and multiple transfers to the connected accounts.
	 *
	 * Creating separate charges and transfers is only supported when both the platform and the connected account are
	 * in the same region: both in Europe or both in the U.S.
	 *
	 * @param	string	$stripeToken	Card token retrieved from Stripe API.
	 * @param	array	$order_info		Order information.
	 */
	private function create_separate_charge_and_transfers($stripeToken, $order_info)
	{
		$order_id = $order_info['order_id'];

		// Create a Charge
		$charge_params = [
			"amount" => $order_info['total'] * 100, // total in cents, therefore multiply by 100
			"currency" => $order_info['currency_code'],
			"description" => "Charge for order #$order_id",
			"source" => $stripeToken,
			"transfer_group" => "ORDER$order_id",
			"metadata" => ['order_id' => $order_id, 'invoice_type' => 'separate']
		];

		$this->ms_logger->debug("payment/ms_stripe_connect/pay - Creating a Charge with parameters: " . print_r($charge_params, true));

		$charge = \Stripe\Charge::create($charge_params);

		$this->ms_logger->debug("payment/ms_stripe_connect/pay - Charge created: " . print_r($charge, true));

		// Get order suborders
		$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id, 'include_abandoned' => true]);
		$this->ms_logger->debug("payment/ms_stripe_connect/pay - Suborders: " . print_r($suborders, true));

		foreach ($suborders as $suborder) {
			// Get seller id
			$seller_id = !empty($suborder['seller_id']) ? (int)$suborder['seller_id'] : null;

			if (!$seller_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/pay - Cannot retrieve seller_id for suborder #".$suborder['suborder_id'].". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Seller id for suborder #".$suborder['suborder_id'].": " . $seller_id);

			// Get seller settings
			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Seller settings: " . print_r($seller_settings, true));

			// Get Stripe user id from settings. Determines whether seller is connected to store owner's Stripe Connect account
			$stripe_user_id = !empty($seller_settings['slr_stripe_user_id']) ? $seller_settings['slr_stripe_user_id'] : NULL;

			if (!$stripe_user_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/pay - Cannot retrieve Stripe user id for seller #".$seller_id.". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Stripe user id: " . $stripe_user_id);

			// Get seller amount excluding commissions
			$suborder_total = !empty($suborder['totals']['sub_total']['value']) ? (float)$suborder['totals']['sub_total']['value'] : 0;

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// Include seller shipping
			if (!empty($suborder['totals']['mm_shipping_total']['value'])) {
				$suborder_total += (float)$suborder['totals']['mm_shipping_total']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			// Subtract suborder (per-seller) commissions
			$amount = $suborder_total - (float)$suborder['store_commission_flat'] - (float)$suborder['store_commission_pct'];

			$suborder_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $seller_id]);
			foreach ($suborder_products as $suborder_product) {
				// Subtract suborder product (per-product) commissions
				$amount -= ((float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct']);
			}

			if ((float)$amount <= 0) {
				$this->ms_logger->debug("payment/ms_stripe_connect/create_separate_charge_and_transfers - Suborder #{$suborder['suborder_id']} (seller #{$seller_id}) total amount (excluding store commissions) is invalid ({$amount}).");
				continue;
			}

			// Create a Transfer to a connected account
			$transfer_params = [
				"amount" => $amount * 100,
				"currency" => $order_info['currency_code'],
				"destination" => "$stripe_user_id",
				"transfer_group" => "ORDER$order_id",
				"source_transaction" => $charge->id,
				"metadata" => ['seller_id' => $seller_id, 'order_id' => $order_id]
			];

			$this->ms_logger->debug("payment/ms_stripe_connect/pay - Creating transfer to seller $seller_id with params: " . print_r($transfer_params, true));

			$transfer = \Stripe\Transfer::create($transfer_params);
		}
	}

	/**
	 * Allows to make charges through the platform account on behalf of connected accounts and take fees in the process.
	 *
	 * Creates multiple charges and multiple transfers to the connected accounts (charge:transfer - 1:1).
	 *
	 * @param	string	$stripeToken	Card token retrieved from Stripe API.
	 * @param	array	$order_info		Order information.
	 */
	private function create_destination_charges($stripeToken, $order_info)
	{
		$order_id = $order_info['order_id'];

		// Create temp Stripe customer
		$customer = \Stripe\Customer::create([
			'email' => $this->customer->getEmail(),
			'source' => $stripeToken,
			'metadata' => ['customer_id' => $this->customer->getId()]
		]);

		$stripe_customer_id = $customer->id;

		// Get order suborders
		$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id, 'include_abandoned' => true]);
		$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Suborders: " . print_r($suborders, true));

		$gross_total = 0;

		// Charge amounts transferred to sellers
		foreach ($suborders as $suborder) {
			// Get seller id
			$seller_id = !empty($suborder['seller_id']) ? (int)$suborder['seller_id'] : null;

			if (!$seller_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_destination_charges - Cannot retrieve seller_id for suborder #".$suborder['suborder_id'].". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Seller id for suborder #".$suborder['suborder_id'].": " . $seller_id);

			// Get seller settings
			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Seller settings: " . print_r($seller_settings, true));

			// Get Stripe user id from settings. Determines whether seller is connected to store owner's Stripe Connect account
			$stripe_user_id = !empty($seller_settings['slr_stripe_user_id']) ? $seller_settings['slr_stripe_user_id'] : NULL;

			if (!$stripe_user_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_destination_charges - Cannot retrieve Stripe user id for seller #".$seller_id.". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Stripe user id: " . $stripe_user_id);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Suborder totals for suborder #{$suborder['suborder_id']}:" . print_r(!empty($suborder['totals']) ? $suborder['totals'] : [], true));

			// Get seller amount excluding commissions
			$suborder_total = !empty($suborder['totals']['sub_total']['value']) ? (float)$suborder['totals']['sub_total']['value'] : 0;

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// Include seller shipping
			if (!empty($suborder['totals']['mm_shipping_total']['value'])) {
				$suborder_total += (float)$suborder['totals']['mm_shipping_total']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			// Subtract suborder (per-seller) commissions
			$amount = $suborder_total - (float)$suborder['store_commission_flat'] - (float)$suborder['store_commission_pct'];

			$suborder_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $seller_id]);
			foreach ($suborder_products as $suborder_product) {
				// Subtract suborder product (per-product) commissions
				$amount -= ((float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct']);
			}

			if ((float)$amount <= 0) {
				$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Suborder #{$suborder['suborder_id']} (seller #{$seller_id}) total amount (excluding store commissions) is invalid ({$amount}).");
				continue;
			}

			$gross_total += $amount;

			// Create a Charge
			$charge_params = [
				"amount" => round(isset($suborder['totals']['total']['value']) ? $suborder['totals']['total']['value'] : 0, 2) * 100,
				"currency" => $order_info['currency_code'],
				"description" => "Charge for order #$order_id and transfer to seller #$seller_id",
				"customer" => $stripe_customer_id,
				"destination" => [
					"account" => "$stripe_user_id",
					"amount" => round($amount, 2) * 100 // @todo 9.0: subtract stripe fees
				],
				"metadata" => ['order_id' => $order_id, 'seller_id' => $seller_id, 'suborder_id' => $suborder['suborder_id'], 'invoice_type' => 'destination_s']
			];

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Creating a Charge with parameters: " . print_r($charge_params, true));

			$charge = \Stripe\Charge::create($charge_params);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Charge created: " . print_r($charge, true));
		}

		// Charge amount for shipping etc.
		$misc_total = round($order_info['total'] - $gross_total, 2);
		if ($misc_total > 0) {
			$charge = \Stripe\Charge::create([
				"amount" => round($order_info['total'] - $gross_total, 2) * 100,
				"currency" => $order_info['currency_code'],
				"description" => "Charge for order #$order_id and transfer to platform",
				"customer" => $stripe_customer_id,
				"metadata" => ['order_id' => $order_id, 'invoice_type' => 'destination_p']
			]);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges - Charge created: " . print_r($charge, true));
		}

		$customer->delete();
	}

	/**
	 * Allows to make charges directly on the connected account and take fees in the process.
	 *
	 * Creates multiple charges.
	 *
	 * @param	string	$stripeToken	Card token retrieved from Stripe API.
	 * @param	array	$order_info		Order information.
	 */
	private function create_direct_charges($stripeToken, $order_info)
	{
		$order_id = $order_info['order_id'];

		// Create temp Stripe customer
		$customer = \Stripe\Customer::create([
			'email' => $this->customer->getEmail(),
			'source' => $stripeToken,
			'metadata' => ['customer_id' => $this->customer->getId()]
		]);

		$stripe_customer_id = $customer->id;

		// Get order suborders
		$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id, 'include_abandoned' => true]);
		$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Suborders: " . print_r($suborders, true));

		$sellers_amounts_total = 0;

		// Charge amounts transferred to sellers
		foreach ($suborders as $suborder) {
			// Get seller id
			$seller_id = !empty($suborder['seller_id']) ? (int)$suborder['seller_id'] : null;

			if (!$seller_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_direct_charges - Cannot retrieve seller_id for suborder #".$suborder['suborder_id'].". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Seller id for suborder #".$suborder['suborder_id'].": " . $seller_id);

			// Get seller settings
			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Seller settings: " . print_r($seller_settings, true));

			// Get Stripe user id from settings. Determines whether seller is connected to store owner's Stripe Connect account
			$stripe_user_id = !empty($seller_settings['slr_stripe_user_id']) ? $seller_settings['slr_stripe_user_id'] : NULL;

			if (!$stripe_user_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_direct_charges - Cannot retrieve Stripe user id for seller #".$seller_id.". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Stripe user id: " . $stripe_user_id);

			// Get seller amount excluding commissions
			$suborder_total = !empty($suborder['totals']['sub_total']['value']) ? (float)$suborder['totals']['sub_total']['value'] : 0;

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// Include seller shipping
			if (!empty($suborder['totals']['mm_shipping_total']['value'])) {
				$suborder_total += (float)$suborder['totals']['mm_shipping_total']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			// Subtract suborder (per-seller) commissions
			$amount = $suborder_total - (float)$suborder['store_commission_flat'] - (float)$suborder['store_commission_pct'];

			$suborder_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $seller_id]);
			foreach ($suborder_products as $suborder_product) {
				// Subtract suborder product (per-product) commissions
				$amount -= ((float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct']);
			}

			if ((float)$amount <= 0) {
				$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Suborder #{$suborder['suborder_id']} (seller #{$seller_id}) total amount (excluding store commissions) is invalid ({$amount}).");
				continue;
			}

			$sellers_amounts_total += $amount;

			// Share Customer with connected account
			$token = \Stripe\Token::create(["customer" => $stripe_customer_id], ["stripe_account" => $stripe_user_id]);

			// Create a Charge
			$charge_params = [
				"amount" => round($amount, 2) * 100,
				"currency" => $order_info['currency_code'],
				"description" => "Charge for order #$order_id straight to seller #$seller_id",
				"source" => $token->id,
				"metadata" => ['order_id' => $order_id, 'seller_id' => $seller_id, 'suborder_id' => $suborder['suborder_id'], 'invoice_type' => 'direct_s']
			];

			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Creating a Charge with parameters: " . print_r($charge_params, true));

			$charge = \Stripe\Charge::create($charge_params, ["stripe_account" => "$stripe_user_id"]);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Charge created: " . print_r($charge, true));
		}

		// Charge amount for shipping, store commissions etc.
		$misc_total = round($order_info['total'] - $sellers_amounts_total, 2);
		if ($misc_total > 0) {
			$charge = \Stripe\Charge::create([
				"amount" => round($order_info['total'] - $sellers_amounts_total, 2) * 100,
				"currency" => $order_info['currency_code'],
				"description" => "Charge for order #$order_id straight to the platform",
				"customer" => $stripe_customer_id,
				"metadata" => ['order_id' => $order_id, 'invoice_type' => 'direct_p']
			]);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_direct_charges - Charge created: " . print_r($charge, true));
		}

		$customer->delete();
	}

	private function create_transfer_to_sellers_sca($order_info)
	{
		$order_id = $order_info['order_id'];

		// Get order suborders
		$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id, 'include_abandoned' => true]);
		$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Suborders: " . print_r($suborders, true));

		// Transfer corresponding amounts for each seller
		foreach ($suborders as $suborder) {
			// Get seller id
			$seller_id = !empty($suborder['seller_id']) ? (int)$suborder['seller_id'] : null;

			if (!$seller_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_destination_charges_sca - Cannot retrieve seller_id for suborder #".$suborder['suborder_id'].". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Seller id for suborder #".$suborder['suborder_id'].": " . $seller_id);

			// Get seller settings
			$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Seller settings: " . print_r($seller_settings, true));

			// Get Stripe user id from settings. Determines whether seller is connected to store owner's Stripe Connect account
			$stripe_user_id = !empty($seller_settings['slr_stripe_user_id']) ? $seller_settings['slr_stripe_user_id'] : NULL;

			if (!$stripe_user_id) {
				$this->ms_logger->error("payment/ms_stripe_connect/create_destination_charges_sca - Cannot retrieve Stripe user id for seller #".$seller_id.". Proceeding to the next suborder.");
				continue;
			}

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Stripe user id: " . $stripe_user_id);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Suborder totals for suborder #{$suborder['suborder_id']}:" . print_r(!empty($suborder['totals']) ? $suborder['totals'] : [], true));

			// Get seller amount excluding commissions
			$suborder_total = !empty($suborder['totals']['sub_total']['value']) ? (float)$suborder['totals']['sub_total']['value'] : 0;

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// Include seller shipping
			if (!empty($suborder['totals']['mm_shipping_total']['value'])) {
				$suborder_total += (float)$suborder['totals']['mm_shipping_total']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			// Subtract suborder (per-seller) commissions
			$amount = $suborder_total - (float)$suborder['store_commission_flat'] - (float)$suborder['store_commission_pct'];

			$suborder_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $seller_id]);
			foreach ($suborder_products as $suborder_product) {
				// Subtract suborder product (per-product) commissions
				$amount -= ((float)$suborder_product['store_commission_flat'] + (float)$suborder_product['store_commission_pct']);
			}

			if ((float)$amount <= 0) {
				$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Suborder #{$suborder['suborder_id']} (seller #{$seller_id}) total amount (excluding store commissions) is invalid ({$amount}).");
				continue;
			}

			$transfer = \Stripe\Transfer::create([
				"amount" => round($amount, 2) * 100,
				"currency" => $order_info['currency_code'],
				"destination" => "$stripe_user_id",
				"transfer_group" => "ORDER_$order_id"
			]);

			$this->ms_logger->debug("payment/ms_stripe_connect/create_destination_charges_sca - Transfer created: " . print_r($transfer, true));
		}
	}


	/* ======================================= Stripe Connect webhooks ============================================== */


	/**
	 * Renders Stripe Connect button.
	 * Renders deauthorization button if seller's account is already connected
	 *
	 * @return view
	 */
	public function connect() {
		$this->validateSellerLoggedIn();

		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $this->seller_id]);

		$stripe_user_id = !empty($seller_settings['slr_stripe_user_id']) ? $seller_settings['slr_stripe_user_id'] : NULL;
		$this->data['account_connected'] = $stripe_user_id ? true : false;

		if (!$this->data['account_connected']) {
			$this->data['oauth_link'] = \Stripe\OAuth::authorizeUrl([
				'client_id' => $this->settings['environments'][$this->env]['client_id'],
				'scope' => 'read_write'
			]);

			$this->data['button_image'] = $this->model_tool_image->resize('catalog/multimerch/stripe/blue-on-light.png', 190, 33);
		} else {
			$this->data['deauth_link'] = $this->url->link('payment/ms_stripe_connect/connect_webhooks', 'deauth=' . $stripe_user_id, 'SSL');
			$this->data['stripe_user_id'] = $stripe_user_id;
		}

		return $this->load->view('payment/ms_stripe_connect_oauth', $this->data);
	}

	/**
	 * Endpoint receiving and processing events from Stripe Connect.
	 *
	 * @return void
	 */
	public function connect_webhooks() {
		$request_get = $this->request->get;

		if (!empty($request_get['error'])) {
			$error_description = !empty($request_get['error_description']) ? $request_get['error_description'] : $this->language->get('error_access_denied');
			$this->ms_logger->error('Stripe Connect error - ' . $request_get['error'] . ' - ' . $error_description);
			$this->session->data['error_warning'] = $error_description;
		} elseif (!empty($request_get['code'])) {
			$this->authorize($request_get['code']);
		} elseif (!empty($request_get['deauth'])) {
			$this->deauthorize($request_get['deauth']);
		}

		$this->response->redirect($this->url->link('seller/account-setting', '', 'SSL'));
	}

	/**
	 * Processes returned authorization code.
	 *
	 * @param $code
	 */
	protected function authorize($code) {
		try {
			$response = \Stripe\OAuth::token([
				'client_secret' => $this->settings['environments'][$this->env]['secret_key'],
				'code' => $code,
				'grant_type' => 'authorization_code'
			]);

			$this->ms_logger->debug('Response from Stripe after retrieving OAuth token: ' . print_r($response, true));
			$this->ms_logger->debug('Stripe user id: ' . $response->stripe_user_id);

			if ($response->error) {
				$this->ms_logger->error($response->error . ' - ' . ($response->error_description ?: 'No description'));
				$this->session->data['error_warning'] = $this->language->get('error_account_not_connected');
			} elseif ($response->stripe_user_id) {
				$this->MsLoader->MsSetting->createSellerSetting([
					'seller_id' => $this->seller_id,
					'settings' => [
						'slr_stripe_user_id' => $response->stripe_user_id
					]
				]);

				$this->ms_logger->debug('Seller Stripe account connected');

				$this->session->data['success'] = $this->language->get('success_account_connected');
			} else {
				$this->ms_logger->error('Stripe response includes neither success, nor error messages');
				$this->session->data['error_warning'] = $this->language->get('error_account_not_connected');
			}
		} catch (\Stripe\Error\OAuth\OAuthBase $e) {
			$this->ms_logger->error($e->getMessage());
			$this->session->data['error_warning'] = $this->language->get('error_authorize');
		} catch (\Stripe\Error\ApiConnection $e) {
			$this->ms_logger->error($e->getMessage());
			$this->session->data['error_warning'] = $this->language->get('error_api_connection');
		}
	}

	/**
	 * Deauthorizes stripe user from marketplace owner's Stripe account.
	 *
	 * @param int $stripe_user_id
	 */
	protected function deauthorize($stripe_user_id) {
		try {
			\Stripe\OAuth::deauthorize([
				'client_secret' => $this->settings['environments'][$this->env]['secret_key'],
				'client_id' => $this->settings['environments'][$this->env]['client_id'],
				'stripe_user_id' => $stripe_user_id
			]);

			$this->session->data['success'] = $this->language->get('success_deauthorized');
		} catch (\Stripe\Error\OAuth\OAuthBase $e) {
			$this->ms_logger->error($e->getMessage());
			$this->session->data['error_warning'] = $this->language->get('error_deauthorize');
		}

		$this->MsLoader->MsSetting->deleteSellerSetting([
			'seller_id' => $this->seller_id,
			'code' => 'slr_stripe_user_id',
			'name' => 'slr_stripe_user_id'
		]);
	}


	/* ======================================= Stripe account webhooks ============================================== */


	/**
	 * Endpoint receiving and processing events from Stripe.
	 *
	 * @return void
	 */
	public function account_webhooks() {
		$response = @file_get_contents('php://input');

		try {
			if (!$response)
				throw new Exception('Empty request body');

			$event = json_decode($response);

			if (!$event)
				throw new Exception('Unable to parse JSON request body');

			if (!is_object($event) || (string)$event->object !== 'event')
				throw new Exception('Event object missing');

			if ((bool)$this->env === (bool)$event->livemode)
				throw new Exception('Extension has different mode then event');

			$this->ms_logger->debug(sprintf('ms_stripe_connect/account_webhooks - Event with type "%s" received', $event->type));

			switch ($event->type) {
				// Orders and suborders
				case 'charge.succeeded':
					$this->webhook_charge_succeeded($event);
					break;
				case 'charge.failed':
					$this->webhook_charge_failed($event);
					break;
				case 'charge.refunded' :
					$this->webhook_charge_refunded($event);
					break;
				case 'transfer.created':
					$this->webhook_transfer_created($event);
					break;
				case 'transfer.reversed':
					$this->webhook_transfer_reversed($event);
					break;
				case 'checkout.session.completed':
					$this->webhook_checkout_session_completed($event);
					break;

				// Subscriptions
				case 'customer.created':
					$this->webhook_customer_created($event);
					break;
				case 'customer.deleted':
					$this->webhook_customer_deleted($event);
					break;
				case 'customer.subscription.created':
					$this->webhook_customer_subscription_created($event);
					break;
				case 'customer.subscription.deleted':
					$this->webhook_customer_subscription_deleted($event);
					break;
				case 'invoice.payment_succeeded':
					$this->webhook_invoice_payment_succeeded($event);
					break;
			}

			http_response_code(200);
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$this->ms_logger->error($e->getMessage());
			$this->session->data['error_warning'] = $e->getMessage();
		} catch (Exception $e) {
			$this->ms_logger->error(sprintf("Stripe's account_webhook error: %s", $e->getMessage()));
			$this->session->data['error_warning'] = $e->getMessage();
		}
	}

	/**
	 * Processes `charge.succeeded` event from Stripe.
	 * Changes order status to according to the settings set by admin.
	 *
	 * @param $event
	 */
	protected function webhook_charge_succeeded($event)	{
		// Retrieve charge so it can't be compromised
		//$charge = \Stripe\Charge::retrieve($event->data->object->id);

		// Get charge object from event object
		// We do not retrieve it by id, because in case of `direct` charging approach, Stripe needs to be
		// reinitialized with participants API credentials.
		$charge = $event->data->object;

		$this->ms_logger->debug('ms_stripe_connect/webhook_charge_succeeded - Charge retrieved: ' . print_r($charge, true));

		$order_id = !empty($charge->metadata->order_id) ? $charge->metadata->order_id : null;
		$seller_id = !empty($charge->metadata->seller_id) ? $charge->metadata->seller_id : null;
		$invoice_type = !empty($charge->metadata->invoice_type) ? $charge->metadata->invoice_type : null;

		// When SCA required, order status and invoices are being handled in webhook_checkout_session_completed
		if (!$order_id || $this->config->get('ms_stripe_connect_sca_required'))
			return;

		$order_info = $this->model_checkout_order->getOrder($order_id);

		// Create invoices and payment
		$this->processInvoice($invoice_type, $order_info, $charge, $seller_id);

		// Order status is not changed using `destination` charging approach
		if (in_array($invoice_type, ['destination_s', 'destination_p']))
			return;

		// Update order status
		$order_status_id = isset($this->settings['event_to_order_status']['charge.succeeded']) ? $this->settings['event_to_order_status']['charge.succeeded'] : $this->config->get('config_order_status_id');

		if ((int)$order_info['order_status_id'] !== (int)$order_status_id)
			$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_charge_succeeded - Updated order status to #' . $order_status_id);
	}

	/**
	 * Creates invoices and payments depending on charging approach (separate, destination, direct).
	 *
	 * @param	string		$invoice_type	Type of invoice to be created. Possible values: `separate`, `destination_p`
	 * 										(to platform), `destination_s` (to seller), `direct_p` (to platform),
	 * 										`direct_s` (to seller).
	 * @param	array		$order_info		Info of an order for which invoice is created.
	 * @param	\Stripe\Charge|\Stripe\Transfer		$object		Stripe object, containing data needed for invoice creation.
	 * @param	int|null	$seller_id		Seller id if invoice is related to seller.
	 * @return	int|bool					If invoice is created - invoice_id from database, else false..
	 */
	private function processInvoice($invoice_type, $order_info, $object, $seller_id = null)
	{
		$order_id = $order_info['order_id'];
		$customer_id = $order_info['customer_id'];

		$invoice_id = false;

		switch ($invoice_type) {
			case 'separate': // Separate charge to platform (invoice from platform to customer for products + commissions)
				$this->ms_logger->debug("ms_stripe_connect/processInvoice - Creating Stripe sale invoices for separate charge and transfers approach (from platform to customer for everything)");
				$invoice_id = $this->invoice_manager->createSaleInvoiceFromPlatformToCustomer($order_id, $customer_id);
				$payment_data = ['customer_id' => $customer_id];
				break;

			case 'destination_p': // Destination charge to platform (invoice from platform to customer for commissions)
			case 'direct_p': // Direct charge to platform (invoice from platform to customer for commissions)
				$this->ms_logger->debug("ms_stripe_connect/processInvoice - Creating Stripe sale invoices for direct charges approach (from platform to customer for commissions)");
				$invoice_id = $this->invoice_manager->createStripeSaleInvoiceFromPlatformToCustomerForCommissions($order_id, $customer_id);
				$payment_data = ['customer_id' => $customer_id];
				break;

			case 'destination_s': // Destination charge to seller (invoice from seller to customer for products)
			case 'direct_s': // Direct charge to seller (invoice from seller to customer for products)
			default:
				if (!$seller_id)
					return;

				$this->ms_logger->debug("ms_stripe_connect/processInvoice - Creating Stripe sale invoices for direct charges approach (from seller to customer for products)");
				$invoice_id = $this->invoice_manager->createStripeSaleInvoiceFromSellerToCustomerForProducts($order_id, $customer_id, $seller_id);
				$payment_data = ['seller_id' => $seller_id, 'customer_id' => $customer_id];
				break;
		}

		// Create payment and assign it to created invoice
		if ($invoice_id) {
			$this->ms_logger->debug("ms_stripe_connect/processInvoice - Invoice #{$invoice_id} is created");

			$this->ms_logger->debug("ms_stripe_connect/processInvoice - Creating payment");

			// Create payment
			$payment_id = $this->MsLoader->MsPgPayment->createPayment(array_merge($payment_data, [
				'payment_type' => MsPgPayment::TYPE_SALE,
				'payment_code' => $this->module_name,
				'payment_status' => MsPgPayment::STATUS_COMPLETE,
				'amount' => $object->amount / 100,
				'currency_id' => $this->currency->getId(strtoupper($object->currency)),
				'currency_code' => strtoupper($object->currency),
				'sender_data' => [],
				'receiver_data' => [
					'charging_approach' => $invoice_type,
					'id' => $object->id ?? 'sca',
					'object' => $object->object ?? 'sca',
					'balance_transaction' => $object->balance_transaction ?? 'sca',
					'description' => $object->description ?? 'sca',
					'destination' => $object->destination ?? 'sca',
					'livemode' => $object->livemode ?? 'sca'
				],
				'description' => [$invoice_id => sprintf($this->language->get('text_payment_description'), $order_id)]
			]));

			// Assign payment to invoice
			$this->MsLoader->MsInvoice->update($invoice_id, [
				'payment_id' => $payment_id,
				'status' => \MultiMerch\Core\Invoice\Invoice::STATUS_PAID
			]);

			$this->ms_logger->debug("ms_stripe_connect/processInvoice - Payment #{$payment_id} created and assigned to invoice #{$invoice_id}");
		}

		return $invoice_id;
	}

	/**
	 * Processes `charge.failed` event from Stripe.
	 * Changes order status to `failed`.
	 *
	 * @param $event
	 */
	protected function webhook_charge_failed($event) {
		// Retrieve charge so it can't be compromised
		$charge = \Stripe\Charge::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_charge_failed - Charge retrieved: ' . print_r($charge, true));

		$order_id = !empty($charge->metadata->order_id) ? $charge->metadata->order_id : null;

		if ($order_id) {
			$order_status_id = isset($this->settings['event_to_order_status']['charge.failed']) ? $this->settings['event_to_order_status']['charge.failed'] : $this->config->get('config_order_status_id');
			$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
		}
	}

	/**
	 * Processes `charge.refunded` event from Stripe.
	 * Changes order status to `refunded`.
	 *
	 * @param $event
	 */
	protected function webhook_charge_refunded($event) {
		// Retrieve charge so it can't be compromised
		$charge = \Stripe\Charge::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_charge_refunded - Charge retrieved: ' . print_r($charge, true));

		$refund = \Stripe\Refund::create(["charge" => "$charge->id"]);

		$order_id = !empty($charge->metadata->order_id) ? $charge->metadata->order_id : null;

		if ($order_id) {
			$order_status_id = isset($this->settings['event_to_order_status']['charge.refunded']) ? $this->settings['event_to_order_status']['charge.refunded'] : $this->config->get('config_order_status_id');
			$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
		}
	}

	/**
	 * Processes `transfer.created` event from Stripe.
	 * Processes seller `ms_invoices` and `ms_payments`.
	 *
	 * @param $event
	 */
	protected function webhook_transfer_created($event) {
		// Retrieve transfer so it can't be compromised
		$transfer = \Stripe\Transfer::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_transfer_created - Transfer retrieved: ' . print_r($transfer, true));
	}

	/**
	 * Processes `transfer.reversed` event from Stripe.
	 *
	 * @param $event
	 */
	protected function webhook_transfer_reversed($event) {
		// @todo 8.15
	}

	protected function webhook_checkout_session_completed($event)
	{
		// Retrieve payment intent object
		$payment_intent = \Stripe\PaymentIntent::retrieve($event->data->object->payment_intent);

		$this->ms_logger->debug('ms_stripe_connect/webhook_checkout_session_completed - Payment intent retrieved: ' . print_r($payment_intent, true));

		$order_id = $payment_intent->metadata->order_id ?? null;

		if ($order_id) {
			$order_info = $this->model_checkout_order->getOrder($order_id);

			// Create transfers to sellers in order
			$this->create_transfer_to_sellers_sca($order_info);

			// Also process invoices
			$mm_order_info_sca = $payment_intent->metadata->mm_order_info_sca ?? null;
			if ($mm_order_info_sca) {
				foreach (explode('|', $mm_order_info_sca) as $suborder_info) {
					list($seller_id, $suborder_total_wo_commissions, $commission_total, $currency) = explode('_', $suborder_info);

					// Invoice from seller to customer for products
					$this->processInvoice('destination_s', $order_info, (object)[
						'amount' => $suborder_total_wo_commissions,
						'currency' => $currency
					], $seller_id);

					// Invoice from platform to customer for commissions
					$this->processInvoice('destination_p', $order_info, (object)[
						'amount' => $commission_total,
						'currency' => $currency
					], $seller_id);
				}
			}
		}
	}

	/**
	 * Processes `customer.created` event from Stripe.
	 * Creates `slr_stripe_customer_id` setting for seller if `seller_id` is passed in event's metadata.
	 * Creates Subscription if `subscription_items` is passed in event's metadata.
	 *
	 * @param $event
	 */
	protected function webhook_customer_created($event)
	{
		$customer = \Stripe\Customer::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_customer_created - Customer retrieved: ' . print_r($customer, true));

		$seller_id = !empty($customer->metadata->seller_id) ? $customer->metadata->seller_id : null;

		if ($seller_id) {
			$this->MsLoader->MsSetting->createSellerSetting([
				'seller_id' => $seller_id,
				'settings' => [
					'slr_stripe_customer_id' => $customer->id
				]
			]);

			$this->ms_logger->debug('ms_stripe_connect/webhook_customer_created - `slr_stripe_customer_id` setting created for seller #' . $seller_id);

			// Create Subscription if subscription items are passed in metadata
			$subscription_items = !empty($customer->metadata->subscription_items) ? $customer->metadata->subscription_items : null;
			if ($subscription_items) {
				try {
					$items = [];
					foreach (explode(',', $subscription_items) as $plan_id) {
						$items[] = ['plan' => $plan_id];
					}

					$subscription = \Stripe\Subscription::create([
						'customer' => $customer->id,
						'items' => $items,
						'metadata' => [
							'seller_id' => $seller_id
						]
					]);

					$this->ms_logger->debug('ms_stripe_connect/webhook_customer_created - Subscription created for seller #' . $seller_id);
				} catch (\Stripe\Error\InvalidRequest $e) {
					$this->ms_logger->error($e->getMessage());
				}
			}
		}

		/*$customer_id = !empty($customer->metadata->customer_id) ? $customer->metadata->customer_id : null;

		if ($customer_id) {
			$this->MsLoader->MsSetting->createSellerSetting([
				'customer_id' => $customer_id,
				'settings' => [
					'cst_stripe_customer_id' => $customer->id
				]
			]);

			$this->ms_logger->debug('ms_stripe_connect/webhook_customer_created - `cst_stripe_customer_id` setting created for customer #' . $customer_id);
		}*/
	}

	/**
	 * Processes `customer.deleted` event from Stripe.
	 * Deletes `slr_stripe_customer_id` setting for seller if `seller_id` is passed in event's metadata.
	 *
	 * @param $event
	 */
	protected function webhook_customer_deleted($event)
	{
		$customer = \Stripe\Customer::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_customer_deleted - Customer retrieved: ' . print_r($customer, true));

		$seller_id = !empty($customer->metadata->seller_id) ? $customer->metadata->seller_id : null;

		if ($seller_id) {
			$this->MsLoader->MsSetting->deleteSellerSetting([
				'seller_id' => $seller_id,
				'code' => 'slr_stripe_customer_id'
			]);

			$this->ms_logger->debug('ms_stripe_connect/webhook_customer_deleted - `slr_stripe_customer_id` setting deleted for seller #' . $seller_id);

			// @todo 9.0: consider canceling subscription
		}
	}

	/**
	 * Processes `customer.subscription.created` event from Stripe.
	 * Creates `slr_stripe_subscription_id` setting for seller if `seller_id` is passed in event's metadata.
	 *
	 * @param $event
	 */
	protected function webhook_customer_subscription_created($event)
	{
		$subscription = \Stripe\Subscription::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_customer_subscription_created - Subscription retrieved: ' . print_r($subscription, true));

		$seller_id = !empty($subscription->metadata->seller_id) ? $subscription->metadata->seller_id : null;

		if ($seller_id) {
			$this->MsLoader->MsSetting->createSellerSetting([
				'seller_id' => $seller_id,
				'settings' => [
					'slr_stripe_subscription_id' => $subscription->id
				]
			]);

			$this->ms_logger->debug('ms_stripe_connect/webhook_customer_subscription_created - `slr_stripe_subscription_id` setting created for seller #' . $seller_id);
		}
	}

	/**
	 * Processes `customer.subscription.deleted` event from Stripe.
	 * Deletes `slr_stripe_subscription_id` setting for seller if `seller_id` is passed in event's metadata.
	 *
	 * @param $event
	 */
	protected function webhook_customer_subscription_deleted($event)
	{
		$subscription = \Stripe\Subscription::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_customer_subscription_deleted - Subscription retrieved: ' . print_r($subscription, true));

		$seller_id = !empty($subscription->metadata->seller_id) ? $subscription->metadata->seller_id : null;

		if ($seller_id) {
			$this->MsLoader->MsSetting->deleteSellerSetting([
				'seller_id' => $seller_id,
				'code' => 'slr_stripe_subscription_id'
			]);

			$this->ms_logger->debug('ms_stripe_connect/webhook_customer_subscription_deleted - `slr_stripe_subscription_id` setting deleted for seller #' . $seller_id);
		}
	}

	/**
	 * Processes `invoice.payment_succeeded` event from Stripe.
	 * Event is fired after recurring payment (in subscription billing cycle) was successfully paid by Stripe customer
	 * (MultiMerch seller).
	 *
	 * @param $event
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	protected function webhook_invoice_payment_succeeded($event)
	{
		$invoice = \Stripe\Invoice::retrieve($event->data->object->id);

		$this->ms_logger->debug('ms_stripe_connect/webhook_invoice_payment_succeeded - Invoice retrieved: ' . print_r($invoice, true));

		if ('subscription_cycle' === (string)$invoice->billing_reason) {
			$customer = \Stripe\Customer::retrieve($invoice->customer);

			$seller_id = !empty($customer->metadata->seller_id) ? $customer->metadata->seller_id : null;

			if ($seller_id) {
				// Get seller Stripe subscription
				$stripe_subscription_id = $this->MsLoader->MsSetting->getSellerSettings([
					'seller_id' => $seller_id,
					'name' => 'slr_stripe_subscription_id',
					'single' => true
				]);

				if (empty($stripe_subscription_id))
					throw new \MultiMerch\Module\Errors\Generic('Seller has no subscriptions');

				// Retrieve Stripe subscription
				$subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

				if (empty($subscription))
					throw new \MultiMerch\Module\Errors\Generic("Stripe Subscription retrieved but is empty!");

				$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
				$seller_group_id = $seller['ms.seller_group'];

				// Retrieve per-seat plan
				$plan_per_seat_id = $this->MsLoader->MsSetting->getSellerGroupSettings([
					'seller_group_id' => $seller_group_id,
					'name' => 'slr_gr_stripe_plan_per_seat_id',
					'single' => true
				]);

				if (!empty($plan_per_seat_id)) {
					// Get all active seller products
					$seller_products = $this->MsLoader->MsProduct->getProducts([
						'seller_id' => $seller_id,
						'language_id' => $this->config->get('config_language_id'),
						'product_status' => [MsProduct::STATUS_ACTIVE],
						'oc_status' => 1
						// @todo 9.0: consider 'available' => true
					]);

					$billed_product_ids = $this->config->get('ms_stripe_connect_current_subscription_period_billed_product_ids');

					// Clear all product ids for seller
					$billed_product_ids[$seller_id] = [];

					foreach ($seller_products as $product) {
						foreach ($subscription->items->data as $item) {
							if ('metered' === (string)$item->plan->usage_type && !in_array($product['product_id'], $billed_product_ids[$seller_id])) {
								\Stripe\UsageRecord::create([
									'quantity' => 1,
									'timestamp' => time(),
									'subscription_item' => $item->id,
									'action' => 'increment'
								]);

								$billed_product_ids[$seller_id][] = $product['product_id'];
							}
						}
					}

					// Refresh `billed_product_ids` setting for current period
					$this->MsLoader->MsHelper->createOCSetting([
						'code' => 'ms_stripe_connect',
						'key' => 'ms_stripe_connect_current_subscription_period_billed_product_ids',
						'value' => $billed_product_ids
					]);

					$this->ms_logger->debug('ms_stripe_connect/webhook_invoice_payment_succeeded - `ms_stripe_connect_current_subscription_period_billed_product_ids` setting is refreshed');
				}
			}
		}
	}


	/* ============================================== Helpers ======================================================= */


	/**
	 * Validates whether user is logged in.
	 * If not, redirects to login page.
	 *
	 * @return mixed
	 */
	protected function validateSellerLoggedIn() {
		if (!$this->seller_id)
			return $this->response->redirect($this->url->link('account/login', '', 'SSL'));
	}

	/**
	 * Validates whether Stripe Connect module's status is `Enabled`.
	 * If not, redirects to main page.
	 *
	 * @return mixed
	 */
	protected function validateModuleStatus() {
		if (!$this->config->get($this->module_name."_status")) {
			$this->ms_logger->error("Stripe Connect module is disabled (status: " . (int)$this->config->get($this->module_name."_status") . ")");
			return $this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}
	}
}
