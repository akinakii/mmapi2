<?php

class ControllerAccountMsFavoriteSeller extends Controller
{
	public $data;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->data = $this->load->language('multiseller/multiseller');

		$this->load->model('tool/image');
	}

	public function index()
	{
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/msfavoriteseller', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->MsLoader->MsHelper->addStyle('multiseller');

		$this->data['heading_title'] = $this->language->get('ms_favorite_seller_title');
		$this->document->setTitle($this->language->get('ms_favorite_seller_title'));

		$sellers = $this->MsLoader->MsFavoriteSeller->getFollowedSellers();

		foreach ($sellers as $seller) {
			$avatar = $this->model_tool_image->resize($seller['ms.avatar'] && file_exists(DIR_IMAGE . $seller['ms.avatar']) ? $seller['ms.avatar'] : 'ms_no_image.jpg', 100, 100);

			// Rating
			$reviews = [
				'rating' => 0,
				'total' => 0,
				'tooltip' => ''
			];

			if ($this->config->get('msconf_reviews_enable')) {
				$seller_reviews = $this->MsLoader->MsReview->getReviews(['seller_id' => $seller['seller_id']]);
				$total_reviews = isset($seller_reviews[0]) ? $seller_reviews[0]['total_rows'] : 0;

				$sum_rating = 0;
				foreach ($seller_reviews as &$seller_review) {
					$sum_rating += $seller_review['rating'];
				}

				$reviews['rating'] = $total_reviews > 0 ? round($sum_rating / $total_reviews, 1) : 0;
				$reviews['total'] = $total_reviews;
				$reviews['tooltip'] = sprintf($this->language->get('mm_review_rating_summary'), $reviews['rating'], $total_reviews, $this->language->get(1 === (int)$total_reviews ? 'mm_review_rating_review' : 'mm_review_rating_reviews'));
			}

			$this->data['sellers'][] = [
				'seller_id' => $seller['seller_id'],
				'thumb' => $avatar,
				'nickname' => $seller['ms.nickname'],
				'description' => utf8_substr(strip_tags(html_entity_decode($seller['ms.description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
				'href' => $this->url->link('seller/catalog-seller/products', '&seller_id=' . $seller['seller_id']),
				'reviews' => $reviews,
				'date_followed' => sprintf($this->language->get('ms_favorite_seller_date_added'), date($this->language->get('date_format_short'), strtotime($seller['date_followed'])))
			];
		}

		// Breadcrumbs
		$breadcrumbs = [
			[
				'text' => $this->language->get('ms_seller_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_favorite_seller_breadcrumbs'),
				'href' => $this->url->link('account/msfavoriteseller', '', 'SSL'),
			]
		];

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs($breadcrumbs);

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/favorite_sellers');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));

	}

	public function follow()
	{
		$json = [];

		$seller_id = isset($this->request->get['seller_id']) ? $this->request->get['seller_id'] : null;

		if (!$seller_id) {
			return $this->response->setOutput(json_encode(['success' => false, 'error' => 'No seller']));
		}

		$this->data['is_authorized'] = false;
		$this->data['seller_nickname'] = $this->MsLoader->MsSeller->getSellerNickname($seller_id);

		$json['success'] = true;

		if ($this->customer->isLogged()) {
			$this->data['is_authorized'] = true;

			if ($this->MsLoader->MsFavoriteSeller->isCustomerFollowing($seller_id)) {
				$this->MsLoader->MsFavoriteSeller->unfollowSeller($seller_id);

				$json['text'] = $this->language->get('ms_favorite_seller_follow_button');
				$this->data['unfollow'] = true;
			} else {
				$this->MsLoader->MsFavoriteSeller->followSeller($seller_id);

				$json['text'] = $this->language->get('ms_favorite_seller_unfollow_button');
				$this->data['follow'] = true;
			}

			$json['count'] = $this->MsLoader->MsFavoriteSeller->getTotalFollowers($seller_id);
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/follow_seller_popup', []);
		$json['html'] = $this->load->view($template, array_merge($this->data, $children));

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
