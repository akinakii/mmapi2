<?php

class ControllerAccountMsWishlist extends Controller
{
	public $data;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->data = array_merge($this->load->language('account/wishlist'), $this->load->language('multiseller/multiseller'));

		$this->load->model('account/wishlist');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		if (!$this->config->get('msconf_wishlist_enabled')) {
			return $this->response->redirect($this->url->link('account/account', '', true));
		}
	}

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/mswishlist', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->MsLoader->MsHelper->addStyle('multiseller');
		$this->MsLoader->MsHelper->addStyle('multimerch/seller_info');
		$this->MsLoader->MsHelper->addStyle('pagination');
		$this->document->addScript('catalog/view/javascript/pagination.min.js');

		$this->data['heading_title'] = $this->language->get('ms_wishlist_title');
		$this->document->setTitle($this->language->get('ms_wishlist_title'));

		$this->data['products'] = [];

		if (isset($this->request->get['wishlist_id'])) {
			// If customer adds product to a specific wishlist
			$wishlist = $this->MsLoader->MsWishlist->getWishlist(['id' => $this->request->get['wishlist_id']]);
		} else {
			// Otherwise, add product to the default list
			$wishlist = $this->MsLoader->MsWishlist->getWishlist(['name' => MsWishlist::NAME_DEFAULT]);
		}

		if (!empty($wishlist['products'])) {
			foreach ($wishlist['products'] as $product) {
				$product_info = $this->MsLoader->MsProduct->getProductCompact($product['product_id']);

				if (!empty($product_info)) {
					$this->data['products'][] = [
						'product_id' => $product_info['product_id'],
						'thumb'      => $this->model_tool_image->resize($product_info['image'] ?: 'no_image.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_width')),
						'name'       => $product_info['name'],
						'minimum'	 => $product_info['minimum'],
						'price'      => $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
						'special'    => !empty($product_info['special']) ? $this->currency->format($this->tax->calculate((float)$product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']) : false,
						'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
						'remove'     => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id']),
						'seller' => empty($product_info['seller_id']) ? false : array(
							'seller_id'        => $product_info['seller_id'],
							'profile_link'     => $this->url->link('seller/catalog-seller/products', 'seller_id=' . $product_info['seller_id'], '', 'SSL'),
							'seller_nickname'  => $product_info['seller_nickname'],
							'seller_avatar'    => $this->model_tool_image->resize($product_info['seller_avatar'] ?: "ms_no_image.jpg", 20, 20),
							'ms.rating'        => $product_info['rating'],
							'total_reviews'	   => $product_info['total_reviews']
						),
						'date_added' => sprintf($this->language->get('ms_wishlist_date_added'), date($this->language->get('date_format_short'), strtotime($product['date_added'])))
					];
				} else {
					$this->MsLoader->MsWishlist->removeProductFromWishlist($wishlist['id'], $product['product_id']);
				}
			}
		}

		// Breadcrumbs
		$breadcrumbs = [
			[
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_wishlist_breadcrumbs'),
				'href' => $this->url->link('account/mswishlist', '', 'SSL'),
			]
		];

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs($breadcrumbs);

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/wishlist');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function add()
	{
		$json = [];

		$product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : 0;
		$product_info = $this->MsLoader->MsProduct->getProductCompact($product_id);

		if ($product_info && $this->customer->isLogged()) {
			$this->data['is_authorized'] = true;

			if (isset($this->request->post['wishlist_id'])) {
				// If customer adds product to a specific wishlist
				$wishlist = $this->MsLoader->MsWishlist->getWishlist(['id' => $this->request->post['wishlist_id']]);
			} else {
				// Otherwise, add product to the default list
				$wishlist = $this->MsLoader->MsWishlist->getWishlist(['name' => MsWishlist::NAME_DEFAULT]);
			}

			if (empty($wishlist)) {
				// If customer doesn't have any wishlists, create a new default one
				$wishlist_id = $this->MsLoader->MsWishlist->createWishlist([
					'products' => [$product_id]
				]);

				$wishlist = $this->MsLoader->MsWishlist->getWishlist(['id' => $wishlist_id]);
			}

			// Add product to customer's wishlist (if it is not already in it)
			if (!empty($wishlist['products']) && !in_array($product_id, array_column($wishlist['products'], 'product_id'))) {
				$this->MsLoader->MsWishlist->addProductToWishlist($wishlist['id'], $product_id);
			}

			$this->data['product'] = $product_info;
			$this->data['wishlist'] = $wishlist;
		} else {
			$this->data['is_authorized'] = false;
		}

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('multimerch/account/wishlist_popup', []);
		$json['html'] = $this->load->view($template, array_merge($this->data, $children));

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove()
	{
		$wishlist_id = null;

		if (isset($this->request->get['wishlist_id'])) {
			$wishlist_id = $this->request->get['wishlist_id'];
		} elseif ($wishlist = $this->MsLoader->MsWishlist->getWishlist(['name' => MsWishlist::NAME_DEFAULT])) {
			$wishlist_id = $wishlist['id'];
		}

		if (!$wishlist_id) {
			return $this->response->setOutput(json_encode(['success' => false, 'error' => 'No wishlist']));
		}

		$product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : null;

		if (!$product_id) {
			return $this->response->setOutput(json_encode(['success' => false, 'error' => 'No product']));
		}

		$this->MsLoader->MsWishlist->removeProductFromWishlist($wishlist_id, $product_id);

		return $this->response->setOutput(json_encode(['success' => true]));
	}
}