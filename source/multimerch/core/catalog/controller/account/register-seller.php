<?php

use \MultiMerch\Event\Event as MsEvent;

class ControllerAccountRegisterSeller extends Controller
{
	use \MultiMerch\Module\Traits\Stripe;

	public $data = array();

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->language->load('account/register');
		$this->load->model('account/customer');
		$this->load->model('localisation/country');
		$this->load->model('localisation/language');

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));

		$this->document->addScript('catalog/view/javascript/multimerch/account-register-seller.js');
	}

	public function index()
	{
		if ($this->customer->isLogged() && $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId()))
			return $this->response->redirect($this->url->link('seller/account-dashboard', '', 'SSL'));

		$this->data['seller_group_id'] = !empty($this->request->get['seller_group_id']) ? $this->request->get['seller_group_id'] : $this->config->get('msconf_default_seller_group_id');

		// Check if seller group exists
		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroup($this->data['seller_group_id']);
		if (empty($seller_group))
			$this->data['seller_group_id'] = $this->config->get('msconf_default_seller_group_id');

		$seller_group_settings = $this->MsLoader->MsSetting->getSellerGroupSettings(['seller_group_id' => $this->data['seller_group_id']]);
		$seller_group_description = $this->MsLoader->MsSellerGroup->getSellerGroupDescriptions($this->data['seller_group_id']);

		$this->data['seller_group'] = [
			'stripe_subscription_enabled' => !empty($seller_group_settings['slr_gr_stripe_subscription_enabled']),
			'name' => !empty($seller_group_description[$this->config->get('config_language_id')]['name']) ? $seller_group_description[$this->config->get('config_language_id')]['name'] : ''
		];

		if (!empty($seller_group_settings['slr_gr_stripe_subscription_enabled'])) {
			$this->document->addScript('https://checkout.stripe.com/checkout.js');

			try {
				$this->initStripe();

				$stripe_plan_terms = [];

				foreach (['base', 'per_seat'] as $type) {
					if (!empty($seller_group_settings['slr_gr_stripe_plan_' . $type . '_id'])) {
						$plan = \Stripe\Plan::retrieve($seller_group_settings['slr_gr_stripe_plan_' . $type . '_id']);

						$stripe_plan_terms[] = sprintf($this->language->get('ms_stripe_subscription_plan_' . $type . '_info'), $this->currency->format($plan->amount / 100, strtoupper($plan->currency)), $this->language->get('ms_stripe_subscription_plan_interval_' . $plan->interval));
					}
				}

				$this->data['stripe_plan_terms'] = implode(' + ', $stripe_plan_terms);
			} catch (\Stripe\Error\InvalidRequest $e) {
				$this->ms_logger->error($e->getMessage());
			}
		}

		$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));

		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');
		$this->document->addScript('catalog/view/javascript/ms-common.js');
		$this->document->addScript('catalog/view/javascript/multimerch/account-register-seller.js');
		$this->document->addScript('catalog/view/javascript/multimerch/ckeditor/ckeditor.js');

		if (!isset($this->session->data['multiseller']['files']))
			$this->session->data['multiseller']['files'] = array();

		$this->document->setTitle($this->language->get('ms_account_register_seller'));
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
				'separator' => $this->language->get('text_separator')
			),
			array(
				'text' => $this->language->get('ms_account_register_seller'),
				'href' => $this->url->link('account/register-seller', '', 'SSL')
			)
		));

		$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
		$this->data['countries'] = $this->model_localisation_country->getCountries();
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// Seller form fields enabled by admin
		$this->data['seller_included_fields'] = !empty($this->config->get('msconf_seller_included_fields')) ? $this->config->get('msconf_seller_included_fields') : false;

		// @todo 9.0: maybe include social links at this page

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->data['msf_seller_properties'] = [
				'default' => $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($this->data['seller_group_id'], true),
				'existing' => []
			];
		}

		// If customer is already created and logged in, we pull his information
		if ($this->customer->isLogged()) {
			$this->data['customer'] = $this->model_account_customer->getCustomer($this->customer->getId());

			// MSF seller properties
			if ($this->config->get('msconf_msf_seller_property_enabled')) {
				$this->data['msf_seller_properties']['existing'] = $this->MsLoader->MsfSellerProperty->getSellerMsfSellerProperties($this->customer->getId());
			}
		}
		
		if ($this->config->get('msconf_seller_terms_page')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('msconf_seller_terms_page'));

			if ($information_info) {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('msconf_seller_terms_page'), true), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}

		$this->data['agree'] = false;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('account/register-seller');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxSaveSellerInfo()
	{
		$json = array();
		$data = $this->request->post;

		$json['errors'] = $this->_validateFormFields($data);

		if (empty($json['errors'])) {
			$this->_createOrUpdateCustomer($data);
			$this->_createSeller($data);

			// Add to MM activity log
			$this->ms_events->add(new MsEvent(array(
				'seller_id' => $this->customer->getId(),
				'event_type' => MsEvent::SELLER_CREATED,
				'data' => array()
			)));

			$this->MsHooks->triggerAction('seller_created_account', [
				'producer' => "seller.{$this->customer->getId()}",
				'consumers' => ["admin.0"],
				'object' => [
					'type' => 'account',
					'id' => $this->customer->getId(),
					'action' => 'created'
				]
			]);

			$this->session->data['success'] = $this->language->get('ms_account_register_success_created');

			$json['redirect'] = $this->url->link('seller/account-dashboard');
		}

		if ($this->ms_events->count()) {
			$this->ms_event_manager->create($this->ms_events);
		}

		$this->response->setOutput(json_encode($json));
	}
	
	public function jxUploadDescriptionImage(){
		
		if (empty($this->request->post)) {
			return $this->response->redirect($this->url->link('seller/account-profile', '', 'SSL'));
		}
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);
		
		if ($json['errors']) {
			$json["uploaded"] = 0;
			$json["error"]['message'] = implode('<br>',$json['errors']);
			return $this->response->setOutput(json_encode($json));
		}
		
		$file = reset($_FILES);

		list($json["fileName"], $json['url']) = $this->MsLoader->MsFile->uploadDescriptionImage($file);
		$json["uploaded"] = 1;
	
		return $this->response->setOutput(json_encode($json));
	}

	public function jxUploadImage()
	{
		$json = [];

		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors'])
			return $this->response->setOutput(json_encode($json));

		foreach ($_FILES as $name => $file) {
			$json['errors'] = $this->MsLoader->MsFile->checkImage($file);

			if (empty($json['errors'])) {
				$image_width = $this->config->get('msconf_preview_seller_avatar_image_width');
				$image_height = $this->config->get('msconf_preview_seller_avatar_image_height');

				if ('ms-banner' === (string)$name) {
					$image_width = (int)$this->config->get('msconf_product_seller_banner_width') / 2;
					$image_height = (int)$this->config->get('msconf_product_seller_banner_height') / 2;
				}

				$fileName = $this->MsLoader->MsFile->uploadImage($file);
				$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $image_width, $image_height);
				$json['files'][] = [
					'name' => $fileName,
					'thumb' => $thumbUrl
				];
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	private function _validateFormFields(&$data)
	{
		$errors = array();
		$validator = $this->MsLoader->MsValidator;

		$defaultLanguageId = $this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'));
		$languages = $this->model_localisation_language->getLanguages();

		// List allowed tags in description fields
		$allowed_tags_ready = "";
		if (!empty($this->config->get('msconf_rte_whitelist'))) {
			$allowed_tags = explode(",", $this->config->get('msconf_rte_whitelist'));
			foreach ($allowed_tags as $tag) {
				$allowed_tags_ready .= "<" . trim($tag) . ">";
			}
		}

		// Validate primary language
		foreach ($languages as $language) {
			$language_id = $language['language_id'];
			$primary = (int)$language_id !== (int)$defaultLanguageId ? false : true;

			if (!empty($data['languages'][$language_id]['slogan'])) {
				// Slogan - max_len 255
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_register_slogan'),
					'value' => html_entity_decode($data['languages'][$language_id]['slogan'])
				),
					array(
						array('rule' => 'max_len,255')
					)
				)) $errors["languages[$language_id][slogan]"] = $validator->get_errors();

				$data['languages'][$language_id]['slogan'] = htmlspecialchars(strip_tags(htmlspecialchars_decode($data['languages'][$language_id]['slogan'], ENT_COMPAT), $allowed_tags_ready), ENT_COMPAT, 'UTF-8');
			}

			if (!empty($data['languages'][$language_id]['description'])) {
				// Description - max_len 5000
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_account_register_description'),
					'value' => html_entity_decode($data['languages'][$language_id]['description'])
				),
					array(
						array('rule' => 'max_len,5000')
					)
				)) $errors["languages[$language_id][description]"] = $validator->get_errors();

				if ($this->config->get('msconf_allow_description_images') != 1){
					$allowed_tags_ready = str_replace('<img>', '', $allowed_tags_ready);
				}
				// strip disallowed tags in description
				$data['languages'][$language_id]['description'] = htmlspecialchars(strip_tags(htmlspecialchars_decode($data['languages'][$language_id]['description'], ENT_COMPAT), $allowed_tags_ready), ENT_COMPAT, 'UTF-8');
			}

			// Copy fields values from main language
			if (!$primary) {
				if (empty($data['languages'][$language_id]['slogan']) && !empty($data['languages'][$defaultLanguageId]['slogan']))
					$data['languages'][$language_id]['slogan'] = $data['languages'][$defaultLanguageId]['slogan'];

				if (empty($data['languages'][$language_id]['description']) && !empty($data['languages'][$defaultLanguageId]['description']))
					$data['languages'][$language_id]['description'] = $data['languages'][$defaultLanguageId]['description'];
			}
		}

		// Firstname - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_register_firstname'),
			'value' => html_entity_decode($data['firstname'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'max_len,64')
			)
		)) $errors["firstname"] = $validator->get_errors();

		// Lastname - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_register_lastname'),
			'value' => html_entity_decode($data['lastname'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'max_len,64')
			)
		)) $errors["lastname"] = $validator->get_errors();

		// Email - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_register_email'),
			'value' => html_entity_decode($data['email'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'email'),
				array('rule' => 'max_len,96')
			)
		)) $errors["email"] = $validator->get_errors();

		// Email - validates if email already used (skipped if already registered customer wants to become a seller)
		if (empty($errors["email"]) && $this->customer->getEmail() !== $data['email'] && $this->model_account_customer->getTotalCustomersByEmail($data['email'])) {
			$errors["email"] = $this->language->get('ms_validate_email_exists');
		}

		// Password - required if a new user creates seller account
		if (isset($data['password']) && isset($data['password_confirm'])) {
			// Password
			if (!$validator->validate(array(
				'name' => $this->language->get('ms_account_register_password'),
				'value' => html_entity_decode($data['password'])
			),
				array(
					array('rule' => 'required'),
					array('rule' => 'min_len,4'),
					array('rule' => 'max_len,20')
				)
			)) $errors["password"] = $validator->get_errors();

			// Password confirm
			if (!$validator->validate(array(
				'name' => $this->language->get('ms_account_register_password_confirm'),
				'value' => html_entity_decode($data['password_confirm'])
			),
				array(
					array('rule' => 'required')
				)
			)) $errors["password_confirm"] = $validator->get_errors();

			// @todo 9.0: move to validator
			if ($data['password_confirm'] != $data['password']) {
				$errors["password_confirm"] = $this->language->get('ms_validate_password_confirm');
			}
		}

		// Nickname - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_register_store_name'),
			'value' => html_entity_decode($data['nickname'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'min_len,4'),
				array('rule' => 'max_len,128'),
				array('rule' => (int)$this->config->get('msconf_nickname_rules') === 1 ? 'latin' : ((int)$this->config->get('msconf_nickname_rules') === 2 ? 'utf8' : 'alpha_numeric')),
			)
		)) $errors["nickname"] = $validator->get_errors();

		// @todo 9.0: move to validator
		if (empty($errors["nickname"]) && $this->MsLoader->MsSeller->nicknameTaken($data['nickname'])) {
			$errors["nickname"] = $this->language->get('ms_error_sellerinfo_nickname_taken');
		}

		// Country - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_account_register_country'),
			'value' => !empty($data['address']['country_id']) ? html_entity_decode($data['address']['country_id']) : null
		),
			array(
				array('rule' => 'required')
			)
		)) $errors["address[country_id]"] = $validator->get_errors();

		// Logo (ex-avatar)
		if (!empty($data['avatar']) && !$this->MsLoader->MsFile->checkFileAgainstSession($data['avatar'])) {
			$errors["avatar"] = sprintf($this->language->get('ms_error_file_upload_error'), $data['avatar'], $this->language->get('ms_file_cross_session_upload'));
		}

		// Banner
		if (!empty($data['banner']) && !$this->MsLoader->MsFile->checkFileAgainstSession($data['banner'])) {
			$errors["banner"] = sprintf($this->language->get('ms_error_file_upload_error'), $data['banner'], $this->language->get('ms_file_cross_session_upload'));
		}

		// Validate seller group exists, if not - set default seller group
		if (!empty($data['seller_group_id'])) {
			$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroup($data['seller_group_id']);
			if (empty($seller_group))
				$data['seller_group_id'] = $this->config->get('msconf_default_seller_group_id');
		}

		// Get seller group settings
		$seller_group_settings = $this->MsLoader->MsSetting->getSellerGroupSettings(['seller_group_id' => $data['seller_group_id']]);

		// Stripe card token
		if ($this->config->get('ms_stripe_connect_enable_subscription') && !empty($seller_group_settings['slr_gr_stripe_subscription_enabled']) && empty($data['stripe_card_token'])) {
			$errors[] = $this->language->get('ms_stripe_subscription_error_card_details');
		}

		// Validate MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			if (!empty($data['msf_seller_properties'])) {
				foreach ($data['msf_seller_properties'] as $msf_seller_property_id => $values) {
					$msf_seller_property = $this->MsLoader->MsfSellerProperty->get($msf_seller_property_id);

					if (!empty($msf_seller_property['required'])) {
						if ('text' === $msf_seller_property['type']) {
							if (empty($values['text_values'][$this->config->get('config_language_id')])) {
								$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$this->config->get('config_language_id')}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
								break;
							} elseif (utf8_strlen($values['text_values'][$this->config->get('config_language_id')]) > 255) {
								$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$this->config->get('config_language_id')}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_length'), $msf_seller_property['name']);
								break;
							}
						} elseif (empty($values['id_values'])) {
							$errors["msf_seller_properties[$msf_seller_property_id][id_values][]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
						}
					}
				}
			}
		}
		
		// Agree to terms
		if ($this->config->get('msconf_seller_terms_page')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('msconf_seller_terms_page'));

			if ($information_info && !isset($data['agree'])) {
				$errors['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return $errors;
	}

	private function _createOrUpdateCustomer($data)
	{
		$customer_data = array(
			'firstname' => $data['firstname'],
			'lastname' => $data['lastname'],
			'email' => $data['email'],
			'telephone' => isset($data['phone']) ? $data['phone'] : '',
			'fax' => ''
		);

		if (!empty($data['customer_id'])) {
			$this->model_account_customer->editCustomer($customer_data);
		} else {
			$customer_data = array_merge($customer_data, array(
				'customer_group_id' => $this->config->get('config_customer_group_id'),
				'password' => $data['password'],
				'newsletter' => 0,
				'company' => isset($data['company']) ? $data['company'] : '',
				'address_1' => isset($data['address']['address_1']) ? $data['address']['address_1'] : '',
				'address_2' => isset($data['address']['address_2']) ? $data['address']['address_2'] : '',
				'city' => isset($data['address']['city']) ? $data['address']['city'] : '',
				'postcode' => isset($data['address']['zip']) ? $data['address']['zip'] : '',
				'country_id' => isset($data['address']['country_id']) ? $data['address']['country_id'] : '',
				'zone_id' => 0
			));

			$this->model_account_customer->addCustomer($customer_data);

			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($customer_data['email']);

			$this->customer->login($customer_data['email'], $customer_data['password'], true);
			unset($this->session->data['guest']);

			// Add to activity log
			$this->load->model('account/activity');
			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name' => $customer_data['firstname'] . ' ' . $customer_data['lastname']
			);
			$this->model_account_activity->addActivity('register', $activity_data);
		}

		return true;
	}

	private function _createSeller($data)
	{
		$MultiMerchModule = $this->MsLoader->load('\MultiMerch\Module\MultiMerch');
		$serviceLocator = $MultiMerchModule->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$seller_data = array(
			'avatar_name' => isset($data['avatar']) ? $data['avatar'] : '',
			'banner_name' => isset($data['banner']) ? $data['banner'] : '',
			'seller_id' => $this->customer->getId(),
			'status' => MsSeller::STATUS_ACTIVE,
			'approved' => 1,
			'seller_group' => $data['seller_group_id'],
			'nickname' => $data['nickname'],
			'languages' => isset($data['languages']) ? $data['languages'] : array(),
			'settings' => array(
				'slr_website' => isset($data['website']) ? $data['website'] : '',
				'slr_company' => isset($data['company']) ? $data['company'] : '',
				'slr_phone' => isset($data['phone']) ? $data['phone'] : ''
			),
			'address' => isset($data['address']) ? $data['address'] : array(),
			'keyword' => $this->MsLoader->MsHelper->slugify($data['nickname']),
			'msf_seller_properties' => !empty($data['msf_seller_properties']) ? $data['msf_seller_properties'] : []
		);

		/**
		 * Check seller validation and create notification emails.
		 */
		switch ($this->config->get('msconf_seller_validation')) {
			case MsSeller::MS_SELLER_VALIDATION_APPROVAL:
				$MailSellerAwaitingModeration = $serviceLocator->get('MailSellerAwaitingModeration', false)
					->setTo($this->customer->getEmail())
					->setData(array('addressee' => $this->customer->getFirstname()));
				$mails->add($MailSellerAwaitingModeration);

				$MailAdminSellerAwaitingModeration = $serviceLocator->get('MailAdminSellerAwaitingModeration', false)
					->setTo($MultiMerchModule->getNotificationEmail())
					->setData(array(
						'seller_name' => $seller_data['nickname'],
						'customer_name' => $this->customer->getFirstname() . ' ' . $this->customer->getLastname(),
						'customer_email' => $this->customer->getEmail() //$this->MsLoader->MsSeller->getSellerEmail($this->customer->getId()),
					));
				$mails->add($MailAdminSellerAwaitingModeration);

				$seller_data['status'] = MsSeller::STATUS_INACTIVE;
				$seller_data['approved'] = 0;

				break;

			case MsSeller::MS_SELLER_VALIDATION_NONE:
			default:
				$MailSellerAccountCreated = $serviceLocator->get('MailSellerAccountCreated', false)
					->setTo($this->customer->getEmail())
					->setData(array('addressee' => $this->customer->getFirstname()));
				$mails->add($MailSellerAccountCreated);

				$MailAdminSellerAccountCreated = $serviceLocator->get('MailAdminSellerAccountCreated', false)
					->setTo($MultiMerchModule->getNotificationEmail())
					->setData(array(
						'seller_name' => $seller_data['nickname'],
						'customer_name' => $this->customer->getFirstname() . ' ' . $this->customer->getLastname(),
						'customer_email' => $this->customer->getEmail() //$this->MsLoader->MsSeller->getSellerEmail($this->customer->getId()),
					));
				$mails->add($MailAdminSellerAccountCreated);

				$seller_data['status'] = MsSeller::STATUS_ACTIVE;
				$seller_data['approved'] = 1;

				break;
		}

		/**
		 * Process Stripe subscription
		 */
		// Get seller group settings
		$seller_group_settings = $this->MsLoader->MsSetting->getSellerGroupSettings(['seller_group_id' => $data['seller_group_id']]);

		if ($this->config->get('ms_stripe_connect_enable_subscription') && !empty($seller_group_settings['slr_gr_stripe_subscription_enabled'])) {
			$stripe_subscription_id = $this->MsLoader->MsSetting->getSellerSettings([
				'seller_id' => $this->customer->getId(),
				'name' => 'slr_stripe_subscription_id',
				'single' => true
			]);

			if (empty($stripe_subscription_id)) {
				try {
					$this->initStripe();

					$subscription_items = [];

					if (!empty($seller_group_settings['slr_gr_stripe_plan_base_id']))
						$subscription_items[] = $seller_group_settings['slr_gr_stripe_plan_base_id'];

					if (!empty($seller_group_settings['slr_gr_stripe_plan_per_seat_id']))
						$subscription_items[] = $seller_group_settings['slr_gr_stripe_plan_per_seat_id'];

					// Get Stripe customer id for seller
					$stripe_customer_id = $this->MsLoader->MsSetting->getSellerSettings([
						'seller_id' => $this->customer->getId(),
						'name' => 'slr_stripe_customer_id',
						'single' => true
					]);

					if (empty($stripe_customer_id)) {
						// If Stripe customer does not exist, we create customer and subscription is automatically created
						// on receiving `customer.created` event if `subscription_items` is passed in metadata and not empty

						$customer = \Stripe\Customer::create([
							'email' => $this->customer->getEmail(),
							'source' => $data['stripe_card_token'],
							'metadata' => [
								'subscription_items' => implode(',', $subscription_items),
								'seller_id' => $this->customer->getId()
							]
						]);

						$this->ms_logger->debug('Stripe customer created');
					} elseif (!empty($subscription_items)) {
						// If Stripe customer exists and Stripe plan(s) are set for seller group, create subscription

						// Prepare subscription items array
						$items = [];
						foreach ($subscription_items as $plan_id) {
							$items[] = ['plan' => $plan_id];
						}

						$subscription = \Stripe\Subscription::create([
							'customer' => $stripe_customer_id,
							'items' => $items,
							'metadata' => [
								'seller_id' => $this->customer->getId()
							]
						]);

						$this->ms_logger->debug('Stripe subscription created');
					}
				} catch (\Stripe\Error\InvalidRequest $e) {
					$this->ms_logger->error('seller/account-profile/jxSaveSellerInfo - ' . $e->getMessage());
					$json['errors'][] = $this->language->get('ms_stripe_subscription_error_subscription');
				} catch (\MultiMerch\Module\Errors\Settings $e) {
					$this->ms_logger->error($e->getMessage());
					$json['errors'][] = $this->language->get('ms_stripe_subscription_error_subscription');
				}

				if (!empty($json['errors']))
					return $this->response->setOutput(json_encode($json));
			}
		}

		if (
			$this->config->get('msconf_allow_description_images') == 1 && 
			$this->config->get('msconf_description_images_type') == 'upload'
		) {
				
			$description = [];
			foreach ($seller_data['languages'] as $lang_id => $fields){
				$description[$lang_id] = $fields['description'];
			}

			$new_description_array = $this->MsLoader->MsFile->moveDescriptionImages($description, 'description');
			
			if ($new_description_array !== false){
				foreach ($seller_data['languages'] as $lang_id => &$fields){
					$fields['description'] = $new_description_array[$lang_id];
				}
			}
		}
		
		/**
		 * Create seller records in database.
		 */
		$this->MsLoader->MsSeller->createSeller($seller_data);
		

		/**
		 * Process commissions.
		 */
		$commissions = $this->MsLoader->MsSellerGroup->getFeeRates($seller_data['seller_group']);
		$fee = isset($commissions[MsCommission::RATE_SIGNUP]['flat']) ? (float)$commissions[MsCommission::RATE_SIGNUP]['flat'] : 0;

		if ($fee > 0) {
			switch ($commissions[MsCommission::RATE_SIGNUP]['payment_method']) {
				case MsPgPayment::METHOD_PG:
					// set seller status to unpaid
					$this->MsLoader->MsSeller->changeStatus($this->customer->getId(), MsSeller::STATUS_UNPAID);

					$invoice_manager = new \MultiMerch\Core\Invoice\Manager($this->MsLoader->getRegistry());
					$invoice_manager->createSellerSignupInvoice($this->customer->getId(), $fee);

					break;

				case MsPgPayment::METHOD_BALANCE:
				default:
					// deduct from balance
					$this->MsLoader->MsBalance->addBalanceEntry($this->customer->getId(),
						array(
							'balance_type' => MsBalance::MS_BALANCE_TYPE_SIGNUP,
							'amount' => -$fee,
							'description' => sprintf($this->language->get('ms_invoice_title_seller_signup'), $this->config->get('config_name'))
						)
					);

					break;
			}
		}

//		$mailTransport->sendMails($mails);

		return true;
	}
}
