<?php
class ControllerCustomerReview extends Controller {
	public $data;
	protected $ms_logger;

	public function __construct($registry) {
		parent::__construct($registry);

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->data = array_merge($this->load->language('multiseller/multiseller'), $this->load->language('account/account'));

		$this->ms_logger = new MultiMerch\Logger\Logger();

		if (!isset($this->session->data['multiseller']['files']))
			$this->session->data['multiseller']['files'] = array();
	}

	private function _initForm() {
		// Script and style for star ratings
		$this->MsLoader->MsHelper->addStyle('star-rating');
		$this->document->addScript('catalog/view/javascript/star-rating.js');
		$this->document->addScript('catalog/view/javascript/multimerch/account-customer-product-review.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.js');
		$this->document->addScript('catalog/view/javascript/plupload/plupload.html5.js');
		$this->document->addScript('catalog/view/javascript/ms-common.js');

		$order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : 0;
		$product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : 0;
		$order_product_id = isset($this->request->get['order_product_id']) ? $this->request->get['order_product_id'] : 0;

		if (!$this->config->get('msconf_reviews_enable') ||
			!$this->MsLoader->MsOrderData->isOrderCreatedByCustomer($order_id, $this->customer->getId()) ||
			!$order_id || !$product_id || !$order_product_id ||
			!empty($this->MsLoader->MsReview->getReviews(array('product_id' => $product_id, 'order_id' => $order_id, 'order_product_id' => $order_product_id, 'author_id' => $this->customer->getId())))
		) {
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
			return;
		}

		$this->document->setTitle($this->language->get('ms_customer_product_rate_heading'));

		$this->data['breadcrumbs'] = array(
			array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			),
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			),
			array(
				'text' => $this->language->get('ms_account_order_history'),
				'href' => $this->url->link('account/order', '', 'SSL')
			),
			array(
				'text' => $this->language->get('ms_order_feedback'),
				'href' => $this->url->link('customer/review/create', 'product_id=' . $product_id . '&order_id=' . $order_id . '&order_product_id=' . $order_product_id, 'SSL')
			)
		);

		$this->load->model('account/order');
		$this->load->model('tool/image');
		$this->load->model('catalog/product');

		$this->data['order_id'] = $order_id;
		$this->data['product_id'] = $product_id;
		$this->data['order_product_id'] = $order_product_id;

		$products = $this->model_account_order->getOrderProducts($order_id);
		$order_products_ids = array();

		foreach($products as &$product) {
			$order_products_ids[] = $product['product_id'];

			$product['options'] = $this->model_account_order->getOrderOptions($product['order_id'], $product['order_product_id']);
			$product['seller'] = $this->MsLoader->MsSeller->getSeller($this->MsLoader->MsProduct->getSellerId($product['product_id']));
			$product['price'] = $this->currency->format($this->tax->calculate($product['price'], $this->config->get('config_tax')));
			$product['product'] = $this->model_catalog_product->getProduct($product['product_id']);
			$product['product']['image'] = $this->model_tool_image->resize($product['product']['image'] ?: 'no_image.png', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
			$product['review'] = $this->MsLoader->MsReview->getReviews(array('order_product_id' => $product['order_product_id'], 'author_id' => $this->customer->getId()));
		}

		if(!in_array($product_id, $order_products_ids)) {
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
			return;
		}

		$this->data['products'] = $products;
	}

	public function create() {
		$this->_initForm();

		$this->data['review'] = false;

		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('customer/review');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxSubmitReview() {
		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$data = $this->request->post;
		$error = $this->_validateFormData($data);

		if ($error) {
			$this->ms_logger->error($error);
			$this->response->redirect($this->url->link('account/order', '', 'SSL'));
		}

		$review_data = array(
			'author_id' => $this->customer->getId(),
			'product_id' => $data['product_id'],
			'order_product_id' => $data['order_product_id'],
			'order_id' => $data['order_id'],
			'seller_id' => $this->MsLoader->MsProduct->getSellerId($data['product_id']),
			'rating' => $data['rating'],
			'title' => '',
			'comment' => $data['rating_comment'],
			'attachments' => !empty($data['images']) ? $data['images'] : array()
		);

		$review_id = $this->MsLoader->MsReview->createOrUpdateReview($review_data);

		// Create new review and send mails
		$this->load->model('account/order');
		$order_product = $this->model_account_order->getOrderProduct($review_data['order_id'], $review_data['order_product_id']);

		$MailProductReviewedAdmin = $serviceLocator->get('MailProductReviewed', false)
			->setTo($this->config->get('config_email'))
			->setData(array(
				'product_name' => $order_product['name'] ?: $this->language->get('ms_product_deleted'),
				'product_href' => $this->url->link('product/product', 'product_id=' . $review_data['product_id'])
			));
		$mails->add($MailProductReviewedAdmin);

		$consumers = [];

		if ($review_data['seller_id']) {
			$MailProductReviewedSeller = clone $MailProductReviewedAdmin;
			$MailProductReviewedSeller->setTo($this->MsLoader->MsSeller->getSellerEmail($review_data['seller_id']));
			$mails->add($MailProductReviewedSeller);

			$consumers[] = "seller.{$review_data['seller_id']}";
		}

		$this->MsHooks->triggerAction('customer_created_ms_review', [
			'producer' => "customer.{$this->customer->getId()}",
			'consumers' => $consumers,
			'object' => [
				'type' => 'ms_review',
				'id' => $review_id,
				'action' => 'created'
			]
		]);

		/*if ($mails->count()) {
			$mailTransport->sendMails($mails);
		}*/

		$this->session->data['success'] = $this->language->get('mm_review_submit_success');
		$this->response->redirect($this->url->link('account/order', '', 'SSL'));
	}

	public function jxAddUpload() {
		$json = array();
		$json['errors'] = $this->MsLoader->MsFile->checkPostMax($_POST, $_FILES);

		if ($json['errors']) {
			return $this->response->setOutput(json_encode($json));
		}

		// allow a maximum of N images
		$msconf_images_limits = $this->config->get('msconf_images_limits');
		foreach ($_FILES as $file) {
			if ($msconf_images_limits[1] > 0 && $this->request->post['imageCount'] > $msconf_images_limits[1]) {
				$json['errors'][] = sprintf($this->language->get('ms_error_product_image_maximum'),$msconf_images_limits[1]);
				$json['cancel'] = 1;
				return $this->response->setOutput(json_encode($json));
			} else {
				$errors = $this->MsLoader->MsFile->checkImage($file);

				if ($errors) {
					$json['errors'] = array_merge($json['errors'], $errors);
				} else {
					$fileName = $this->MsLoader->MsFile->uploadImage($file);

					$thumbUrl = $this->MsLoader->MsFile->resizeImage($this->config->get('msconf_temp_image_path') . $fileName, $this->config->get('msconf_preview_product_image_width'), $this->config->get('msconf_preview_product_image_height'));
					$json['files'][] = array(
						'name' => $fileName,
						'thumb' => $thumbUrl
					);
				}
			}
		}

		return $this->response->setOutput(json_encode($json));
	}

	protected function _validateFormData(&$data) {
		// Form array of the params from POST data. Single means we only get only one record from db
		$params = array('single' => 1);
		foreach (array('review_id', 'author_id', 'product_id', 'order_id', 'order_product_id') as $name) {
			if (!empty($data[$name]))
				$params[$name] = $data[$name];
		}

		// Checks whether review already exists and if so return error
		$review_exists = $this->MsLoader->MsReview->getReviews($params);
		if (!empty($review_exists))
			return 'Review already exists and can not be modified.';

		// If review doesn't exist and required parameters for its creation are not passed, return error
		if (empty($data['product_id']) || empty($data['order_id']) || empty($data['order_product_id']) || empty($data['rating']) || empty($data['rating_comment']))
			return 'Not all required params are set.';

		// Forbid author_id passing and always set it to current user_id
		if (!$this->MsLoader->MsOrderData->isOrderCreatedByCustomer($data['order_id'], $this->customer->getId()))
			return 'An order does not belong to customer trying to create review.';

		return false;
	}
}