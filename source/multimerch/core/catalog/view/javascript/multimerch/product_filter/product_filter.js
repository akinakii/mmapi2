$(function () {
	function getQueryString(link) {
		var link = link || window.location.href;

		// Remove empty params
		if (typeof link === 'string') {
			var result = link.match(/(?:(?:.*&)|^)(.*?=)(?:(?=&)|$)/);

			//if group match
			if (result !== null && typeof result[1] !== 'undefined') {
				link = link.replace(result[1], '');
				link = link.replace(/&&/, '&');
				link = link.replace(/&(?=$)/, '');
			}
		}

		var query_string = '';

		// Add category path
		if (ms_pf_config.category_path) {
			query_string += '&path=' + ms_pf_config.category_path;
		}

		if (ms_pf_config.ms_category_id > 0) {
			query_string += '&ms_category_id=' + ms_pf_config.ms_category_id;
		}

		// Add seller id
		if (ms_pf_config.seller_id > 0) {
			query_string += '&seller_id=' + ms_pf_config.seller_id;
		}

		// Add common query params
		ms_pf_config.query_params.common.split(',').forEach(function (param) {
			query_string += getUrlParam(link, param);
		});

		var price_min = $(document).find('input[name="price[min]"]').val();
		var price_max = $(document).find('input[name="price[max]"]').val();

		if (parseFloat(price_min) > parseFloat(price_max)) {
			var new_price_min = price_max;
			var new_price_max = price_min;

			price_min = new_price_min;
			price_max = new_price_max;
		}

		if (price_min) {
			query_string += '&price[min]=' + price_min.toString();
		}

		if (price_max) {
			query_string += '&price[max]=' + price_max.toString();
		}

		$.map($('.multimerch_productfilter_block'), function (block) {
			var block_values = [],
				block_values_assoc = [];

			var id = $(block).attr('data-id');
			var type = $(block).attr('data-type');

			$.each($(block).find('input[type="checkbox"]:checked'), function (key, item) {
				if (id != null && type != null) {
					if (block_values_assoc[id] == null) {
						block_values_assoc[id] = [];
					}

					block_values_assoc[id].push($(item).val());
				} else {
					block_values.push($(item).val());
				}
			});

			if (block_values_assoc.length) {
				$.each(block_values_assoc, function (id, values) {
					if (values == null) {
						return;
					}

					query_string += '&' + type + '[' + id + ']=' + values.join(',');
				});
			}

			if (block_values.length) {
				query_string += '&' + type + '=' + block_values.join(',');
			}
		});

		return query_string;
	}

	function getUrlParam(link, name) {
		var pattern = new RegExp('(&|\\?)' + name + '.*?(?=&|$)', 'g');
		var result = link.match(pattern);

		if (result === null) {
			return '';
		} else {
			var output = '';

			result.forEach(function(item, i, arr) {
				output += item.replace(/\?/, '&');
			});
			return output;
		}
	}

	function removeUrlParams(link, names) {
		// Escape every param name
		names.forEach(function (name, i) {
			names[i] = escapeRegExp(name);
		});

		var pattern = new RegExp('(&|\\?)(' + names.join('|') + ').*?(?=&|$)', 'g');
		return link.replace(pattern, '');
	}

	function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}

	function renderCategoryPage(query_string) {
		var selectors = ms_pf_config.DOM_selectors;

		$.ajax({
			url: ms_pf_config.urls.render_category_page + query_string,
			dataType: 'html',
			beforeSend: function () {
				// Block UI
				['products_container', 'pagination', 'products_count', 'sort_limit_panel', 'module'].forEach(function (name) {
					$(selectors[name]).append('<div class="ms-block-ui" id="ms-block-' + name + '"></div>').show();
				});
			},
			success: function (html) {
				var rendered_html = {};

				// N.B.: do not refresh 'module' block in order to show initial filters.
				['products_container', 'pagination', 'products_count', 'module'].forEach(function (name) {
					rendered_html[name] = $(html).find(selectors[name]).html();
				});

				$.each(rendered_html, function (name, r_html) {
					$(selectors[name]).empty().hide();

					if (r_html) {
						$(selectors[name]).html(r_html).show();
					} else if (name == 'products_container') {
						$(selectors[name]).html(ms_pf_config.language.text_no_products);

						$(selectors['pagination']).hide();
						$(selectors['products_count']).hide();
						$(selectors['sort_limit_panel']).hide();
					}
				});

				$(localStorage.getItem('display') == 'list' ? '#list-view' : '#grid-view').addClass('active').trigger('click');

				// Change page url
				var new_url = (ms_pf_config.urls.current_category + removeUrlParams(query_string, ['path'])).replace('&amp;', '&');
				if (new_url.indexOf('?') == -1) {
					// Replace first & with ?
					new_url = new_url.replace('&', '?');
				}

				window.history.pushState('', '', new_url);

				// Unblock UI
				['products_container', 'pagination', 'products_count', 'sort_limit_panel', 'module'].forEach(function (name) {
					$(selectors[name]).find('.ms-block-ui').remove();
				});
			}
		});
	}

	function renderSellerStorePage(query_string) {
		var selectors = ms_pf_config.DOM_selectors;

		$.ajax({
			url: ms_pf_config.urls.render_seller_store_page + query_string,
			dataType: 'html',
			beforeSend: function () {
				// Block UI
				['products_container', 'pagination', 'products_count', 'sort_limit_panel', 'module'].forEach(function (name) {
					$(selectors[name]).append('<div class="ms-block-ui" id="ms-block-' + name + '"></div>').show();
				});
			},
			success: function (html) {
				var rendered_html = {};

				// N.B.: do not refresh 'module' block in order to show initial filters.
				['products_container', 'pagination', 'products_count', 'module'].forEach(function (name) {
					rendered_html[name] = $(html).find(selectors[name]).html();
				});

				$.each(rendered_html, function (name, r_html) {
					$(selectors[name]).empty().hide();

					if (r_html) {
						$(selectors[name]).html(r_html).show();
					} else if (name == 'products_container') {
						$(selectors[name]).html(ms_pf_config.language.text_no_products);

						$(selectors['pagination']).hide();
						$(selectors['products_count']).hide();
						$(selectors['sort_limit_panel']).hide();
					}
				});

				$(localStorage.getItem('display') == 'list' ? '#list-view' : '#grid-view').addClass('active').trigger('click');

				// Change page url
				var new_url = (ms_pf_config.urls.current_seller_store_category + removeUrlParams(query_string, ['seller_id', 'ms_category_id'])).replace('&amp;', '&');
				if (new_url.indexOf('?') == -1) {
					// Replace first & with ?
					new_url = new_url.replace('&', '?');
				}

				window.history.pushState('', '', new_url);

				// Unblock UI
				['products_container', 'pagination', 'products_count', 'sort_limit_panel', 'module'].forEach(function (name) {
					$(selectors[name]).find('.ms-block-ui').remove();
				});
			}
		});
	}

	// On page loaded
	if (ms_pf_config.seller_id > 0) {
		// Seller store page
		renderSellerStorePage(getQueryString());
	} else {
		// OpenCart category page
		renderCategoryPage(getQueryString());
	}

	// Adjust price inputs
	setTimeout(function () {
		if ($('.multimerch_productfilter_block[data-type="price"]').length) {
			$.map($('.multimerch_productfilter_block[data-type="price"] .price-input'), function (price_input) {
				var symbol_left = $(price_input).find('.currency.left');
				var symbol_right = $(price_input).find('.currency.right');

				// Get required padding, which is:
				// number of currency symbols multiplied by 7px
				// + 7px for initial indent
				// + 3px for little indent before numbers
				var padding = symbol_left.text().length * 7 + 10;

				if (symbol_left.length && !symbol_right.length) {
					$(price_input).find('input').css({'padding-left': padding + 'px', 'padding-right': '2px'});
				} else if (!symbol_right.length && symbol_right.length) {
					padding = symbol_right.text().length * 7 + 2;

					$(price_input).find('input').css({'padding-left': '2px', 'padding-right': padding + 'px'});
				} else {
					$(price_input).find('input').css({'padding-left': padding + 'px', 'padding-right': padding + 'px'});
				}
			});
		}
	}, 1000)

	$(document).on('click', '#ms_filter_price_range', function (e) {
		e.preventDefault();

		if (ms_pf_config.seller_id > 0) {
			renderSellerStorePage(removeUrlParams(getQueryString(), ['page']));
		} else {
			renderCategoryPage(removeUrlParams(getQueryString(), ['page']));
		}
	});

	$(document).on('click', '#mspf_pagination ul.pagination li a', function (e) {
		e.preventDefault();

		if (ms_pf_config.seller_id > 0) {
			renderSellerStorePage(getQueryString($(this).attr('href')));
		} else {
			renderCategoryPage(getQueryString($(this).attr('href')));
		}
	});

	$(document).on('change', '#input-sort, #input-limit', function (e) {
		e.preventDefault();

		if (ms_pf_config.seller_id > 0) {
			renderSellerStorePage(removeUrlParams(getQueryString($(this).val()), ['page']));
		} else {
			renderCategoryPage(removeUrlParams(getQueryString($(this).val()), ['page']));
		}
	});

	$(document).on('click', '#multimerch_productfilter .checkbox-inline input[type="checkbox"]', function () {
		if (ms_pf_config.seller_id > 0) {
			renderSellerStorePage(removeUrlParams(getQueryString(), ['page']));
		} else {
			renderCategoryPage(removeUrlParams(getQueryString(), ['page']));
		}
	});
});