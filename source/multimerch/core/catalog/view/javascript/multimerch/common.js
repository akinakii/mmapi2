$(function() {
	$.fn.dataTableExt.sErrMode = 'throw';

	if (typeof config_language != 'undefined') {
		$.extend($.fn.dataTable.defaults, {
			"oLanguage": {
				"sUrl": config_language
			}
		});
	}
	
	$.extend($.fn.dataTable.defaults, {
		"bProcessing": true,
		"bSortCellsTop": true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"bAutoWidth": false,
		"bLengthChange": false,
		"sDom": 'rt<"pagination"pi><"clear">',
		"asStripeClasses": [],
		"iDisplayLength": parseInt(msconf_account_pagination_limit),
		"fnPreDrawCallback": function( oSettings ) {
			var property = 'ms_empty_datatable_' + oSettings.sTableId.replace(/-/g, '_');
			oSettings.oLanguage.sEmptyTable = (typeof msGlobals != 'undefined' && msGlobals.hasOwnProperty(property)) ? msGlobals[property] : 'No data available in table';
		},
		"initComplete": function (oSettings) {
			var table_id = '#' + oSettings.sTableId;

			if (oSettings.aoData.length === 0) {
				$(table_id + ' > thead > tr.filter').remove();
				$(table_id).siblings('.pagination').remove();
			}
		}
	});
	
	$("body").delegate(".dataTable .filter input[type='text']", "keyup",  function() {
		$(this).parents(".dataTable").dataTable().fnFilter(this.value, $(this).parents(".dataTable").find("thead tr.filter td").index($(this).parent("td")));
	});

	$(document).ready(function() {
		if ($('.input-date-datepicker').length) {
			$('.input-date-datepicker').datetimepicker({
				format: 'YYYY-MM-DD',
				pickTime: false,
				useCurrent: false,
				focusOnShow: false,
				showClear: true
			})
				.on('dp.change', function() {
					$(this).trigger('keyup');
				});
				//.attr('readonly', 'readonly');
		}


		$('.daterangepicker div.ranges div.range_inputs .applyBtn').removeClass('btn-success');
		$('.daterangepicker div.ranges div.range_inputs .applyBtn').addClass('btn-primary');
	});
});

// Language switcher for inputs
function initLangSwitcher(){
    
    var lang_inputs = $('.lang-select-field');
    for(var i = 0; i < lang_inputs.length; i++) {
        if($(lang_inputs[i]).data('lang') != msGlobals.config_language) {
            $(lang_inputs[i]).hide();
        } else {
            $(lang_inputs[i]).show();
        }
    }

    $(".select-input-lang").on( "click", function() {
            var selectedLang = $(this).data('lang');
            $('.lang-select-field').each(function() {
                    if ($(this).data('lang') == selectedLang) {
                            $(this).show();
                    } else {
                            $(this).hide();
                    }
            });
            $('.lang-chooser img').each(function() {
                    if ($(this).data('lang') == selectedLang) {
                            $(this).addClass('active');
                    } else {
                            $(this).removeClass('active');
                    }
            });
    });
}