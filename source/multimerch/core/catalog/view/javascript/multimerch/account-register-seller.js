$(function() {
	var lang_inputs = $('.lang-select-field');
	for(var i = 0; i < lang_inputs.length; i++) {
		if($(lang_inputs[i]).data('lang') != msGlobals.config_language) {
			$(lang_inputs[i]).hide();
		} else {
			$(lang_inputs[i]).show();
		}
	}

	// language select
	$(".select-input-lang").on( "click", function() {
		var selectedLang = $(this).data('lang');
		$('.lang-select-field').each(function() {
			if ($(this).data('lang') == selectedLang) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		$('.lang-chooser img').each(function() {
			if ($(this).data('lang') == selectedLang) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});

	var cfg = {};
	if (msGlobals.config_allow_description_images == 1){
		if (msGlobals.config_description_images_type == 'upload'){
			cfg = {
				extraPlugins: 'videodetector,imageCustomUploader',
				uploadUrl: 'index.php?route=account/register-seller/jxUploadDescriptionImage',
				removeButtons: 'base64image,Anchor,Subscript,Superscript'
			}
		}
	} else {
		cfg = {
			removeButtons: 'base64image,Anchor,Image,Subscript,Superscript'
		}
	}

	$('.ckeditor').each(function () {
		CKEDITOR.replace(this, cfg);
	});

	var avatar = initUploader('ms-avatar', 'index.php?route=account/register-seller/jxUploadImage', 'mini', false, false);
	var banner = initUploader('ms-banner', 'index.php?route=account/register-seller/jxUploadImage', 'mini', false, false);

	$("#sellerinfo_avatar_files, #sellerinfo_banner_files").delegate(".ms-remove", "click", function() {
		var par = $(this).parent();
		par.addClass('hidden');
		par.find('input').val('');
		par.find('img').attr('src', '');
		par.parent().find('.dragndropmini').show();
		par.parent().find('.dragndropmini').removeClass('hidden');
	});

	$("#ms-submit-button").click(function() {
		var button = $(this);

		for (var instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}

		if (parseInt(msGlobals.stripeSubscriptionEnabled) && parseInt(msGlobals.stripeSubscriptionEnabledForSellerGroup) && !$('input[name="stripe_card_token"]').val()) {
			button.attr("data-loading-text","<i class='fa fa-spinner fa-spin '></i>")
			button.button('loading');

			var stripeCheckoutHandler = StripeCheckout.configure({
				key: msGlobals.stripePublicKey,
				locale: 'auto',
				token: function(token) {
					$('input[name="stripe_card_token"]').val(token.id);
					saveSellerInfo(button);
				},
				closed: function() {
					$(".ms-spinner" ).button('reset');
				}
			});

			stripeCheckoutHandler.open({
				name: msGlobals.stripeName,
				email: $('input[name="email"]').val(),
				panelLabel: msGlobals.stripePanelLabel,
				description: msGlobals.stripeDescription,
				allowRememberMe: false
			});

			// Close Checkout on page navigation:
			window.addEventListener('popstate', function() {
				stripeCheckoutHandler.close();
			});
		} else {
			saveSellerInfo(button);
		}
	});

	function saveSellerInfo(button) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: $('base').attr('href') + 'index.php?route=account/register-seller/jxSaveSellerInfo',
			data: $("form#seller-registration-form").serialize(),
			beforeSend: function() {
				$('p.error').remove();
				$('.warning.main').empty().hide();
				$('.form-group').removeClass("has-error");
			},
			complete: function(jqXHR, textStatus) {
				if (textStatus != 'success') {
					$(".ms-spinner" ).button('reset');
					$(".warning.main").text(msGlobals.formError).show();
					window.scrollTo(0,0);
				}
			},
			success: function(jsonData) {
				if (!jQuery.isEmptyObject(jsonData.errors)) {
					$(".ms-spinner" ).button('reset');
					$('.error').text('');

					for (error in jsonData.errors) {
						if ($('#error_' + error).length > 0) {
							$('#error_' + error).text(jsonData.errors[error]);
							$('#error_' + error).parents('.form-group').addClass('has-error');
						} else if ($('[name="'+error+'"]').length > 0) {
							$('[name="' + error + '"]').parents('.form-group').addClass('has-error');
							$('[name="' + error + '"]').parents('div:first').append('<p class="error">' + jsonData.errors[error] + '</p>');
						}

						$(".warning.main").append("<p>" + jsonData.errors[error] + "</p>").show();
					}
					window.scrollTo(0,0);
				} else if (jsonData.redirect) {
					window.location = jsonData.redirect;
				}
			}
		});
	}
});