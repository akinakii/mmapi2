$(function() {


    // Latest Reviews list

    $('#list-reviews').dataTable( {
        "sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-review/getTableData",
        "aoColumns": [
            { "mData": "product_name" },
            { "mData": "rating" },
            { "mData": "comment", "bSortable": false },
            { "mData": "date_created" },
            { "mData": "status" },
            { "mData": "actions", "bSortable": false, "sClass": "text-right" }
        ],
        "aaSorting":  [[3,'desc']]
    });

    // Review edit form
    if($('input[name="review_id"]').length > 0) {
        $('.thumbnails').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery: {
                enabled:true
            }
        });

        var review_id = $('input[name="review_id"]').val();

        $(document).on('click', '#comment-btn', function(e) {
            e.preventDefault();

            var $form = $(this).closest('form');

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: "index.php?route=seller/account-review/jxAddReviewComment",
                data: {review_id: review_id, text: $form.find('textarea[name="text"]').val()},
                success: function (json) {
                    if(json.errors) {
                        for(var i = 0, length = json.errors.length; i < length; i++) {
                            console.error(json.errors[i]);
                        }
                    } else if (json.success) {
                        window.location.reload();
                    }
                }
            });
        });
    }
});