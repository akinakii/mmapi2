$(function () {
	var shipping_data = [];

	var jxGetCountries = new Promise(function (resolve, reject) {
		if (msGlobals.field_from_enabled) {
			$.get('index.php?route=multimerch/shipping/jxGetCountries', function (json) {
				resolve(json);
			});
		} else {
			reject("Field `Shipping from` is disabled!");
		}
	});

	var jxGetGeoZones = new Promise(function (resolve, reject) {
		$.get('index.php?route=multimerch/shipping/jxGetGeoZones', function (json) {
			resolve(json);
		});
	});

	var jxGetShippingMethods = new Promise(function (resolve, reject) {
		if (msGlobals.field_shipping_method_enabled) {
			$.get('index.php?route=multimerch/shipping/jxGetShippingMethods', function (json) {
				resolve(json);
			});
		} else {
			reject("Field `Shipping method` is disabled!");
		}
	});

	var jxGetDeliveryTimes = new Promise(function (resolve, reject) {
		if (msGlobals.field_delivery_time_enabled) {
			$.get('index.php?route=multimerch/shipping/jxGetDeliveryTimes', function (json) {
				resolve(json);
			});
		} else {
			reject("Field `Delivery time` is disabled!");
		}
	});

	jxGetCountries.then(function (countries) {
		shipping_data['country'] = countries;
		if ($('#selectize-country-from').length) {
			initShippingSelectize('#selectize-country-from', 'country');
		}
	}, function (message) {
		alert(message);
	});

	jxGetGeoZones.then(function (geo_zones) {
		shipping_data['destinations'] = geo_zones;

		$('input[id^="selectize-destinations"]').each(function () {
			if (!$(this).closest('tr').hasClass('ffSample')) {
				initShippingSelectize('#' + $(this).attr('id'), 'destinations');
			}
		});
	});

	jxGetShippingMethods.then(function (shipping_methods) {
		shipping_data['shipping-method'] = shipping_methods;

		$('input[id^="selectize-shipping-method"]').each(function () {
			if (!$(this).closest('tr').hasClass('ffSample')) {
				initShippingSelectize('#' + $(this).attr('id'), 'shipping-method');
			}
		});
	}, function (message) {
		alert(message);
	});

	jxGetDeliveryTimes.then(function (delivery_times) {
		shipping_data['delivery-time'] = delivery_times;

		$('input[id^="selectize-delivery-time"]').each(function () {
			if (!$(this).closest('tr').hasClass('ffSample')) {
				initShippingSelectize('#' + $(this).attr('id'), 'delivery-time');
			}
		});
	}, function (message) {
		alert(message);
	});

	function initShippingSelectize(dom_element_id, type) {
		var $this = $(dom_element_id);
		var $initialized = false;

		var delimiter = '~',
			maxOptions = 1000,
			valueField = '',
			labelField = 'name',
			searchField = 'name',
			sortField = 'name',
			maxItems = 1,
			maxOptions = 1000,
			initial_items = [],
			row_id = parseInt($this.attr('id').match(/[0-9]+/g));

		switch (type) {
			case 'country':
				valueField = 'country_id';
				initial_items.push($('input[name="shipping[from_country_id]"]').data('name'));
				break;

			case 'destinations':
				valueField = 'geo_zone_id';

				if (row_id > 0) {
					$.map($('input[name="shipping[rules][' + row_id + '][destinations][]"]'), function (initial_item) {
						if ($(initial_item).data('name')) {
							initial_items.push($(initial_item).data('name'));
						}
					});
				}

				break;

			case 'shipping-method':
				valueField = 'shipping_method_id';

				if (row_id > 0) {
					$.map($('input[name="shipping[rules][' + row_id + '][shipping_method_id]"]'), function (initial_item) {
						if ($(initial_item).data('name')) {
							initial_items.push($(initial_item).data('name'));
						}
					});
				}

				break;

			case 'delivery-time':
				valueField = 'delivery_time_id';

				if (row_id > 0) {
					$.map($('input[name="shipping[rules][' + row_id + '][delivery_time_id]"]'), function (initial_item) {
						if ($(initial_item).data('name')) {
							initial_items.push($(initial_item).data('name'));
						}
					});
				}

				break;

			default:
				console.error('Type not supported');
				return;
				break;
		}

		$this.selectize({
			valueField: valueField,
			labelField: labelField,
			searchField: searchField,
			sortField: sortField,
			maxItems: maxItems,
			maxOptions: maxOptions,
			preload: true,
			delimiter: delimiter,
			create: false,
			createOnBlur: false,
			selectOnTab: true,
			render: {
				option: function (item, escape) {
					return '<div>' + escape(item.name) + '</div>';
				}
			},
			load: function (query, callback) {
				if (shipping_data[type] != 'undefined') {
					callback(shipping_data[type]);
				} else {
					console.error('No source data for shipping field "' + type + '"')
				}
			},
			onItemAdd: function(value, item) {
				var itemValue = item[0].dataset.value;
				var itemName = item[0].innerText;

				// @todo 9.0 Shipping: && !$('input[name="shipping[rules][' + row_id + '][destinations][]"][value="' + itemValue + '"]').length
				if (type == 'country') {
					$('input[name="shipping[from_country_id]"]').val(itemValue);
				} else if (type == 'destinations') {
					if ($('input[name="shipping[rules][' + row_id + '][destinations][]"]').length) {
						$('input[name="shipping[rules][' + row_id + '][destinations][]"]').val(itemValue).attr('data-name', itemName);
					} else {
						$this.closest('td').append('<input type="hidden" name="shipping[rules][' + row_id + '][destinations][]" value="' + itemValue + '" data-name="' + itemName + '" />');
					}
				} else {
					if ($('input[name="shipping[rules][' + row_id + '][' + valueField + ']"]').length) {
						$('input[name="shipping[rules][' + row_id + '][' + valueField + ']"]').val(itemValue).attr('data-name', itemName);
					} else {
						$this.closest('td').append('<input type="hidden" name="shipping[rules][' + row_id + '][' + valueField + ']" value="' + itemValue + '" data-name="' + itemName + '" />');
					}
				}
			},
			onItemRemove: function(value) {
				if (type == 'destinations' && $('input[name="shipping[rules][' + row_id + '][destinations][]"][value="' + value + '"]').length) {
					$('input[name="shipping[rules][' + row_id + '][destinations][]"][value="' + value + '"]').remove();
				}
			},
			onLoad: function(data) {
				if (!$initialized) {
					var selectize = $this[0].selectize;

					$.each(initial_items, function (key, item) {
						selectize.addItem(selectize.search(item).items[0].id);
					});

					$initialized = true;
				}
			}
		});
	}

	$(document).on('click', '#ms-shipping .global-shipping-override', function (e) {
		e.preventDefault();

		var override = parseInt($(this).closest('div').attr('data-override'));

		if (override == 1) {
			$('#shipping-container').addClass('hidden');
			$('#shipping-container tr:not(.ffSample) input').attr('disabled', true);
			$('.override-note[data-override="0"]').removeClass('hidden');
			$('.override-note[data-override="1"]').addClass('hidden');
		} else {
			$('#shipping-container').removeClass('hidden');
			$('#shipping-container tr:not(.ffSample) input').attr('disabled', false);
			$('.override-note[data-override="0"]').addClass('hidden');
			$('.override-note[data-override="1"]').removeClass('hidden');
		}
	})

	$("#ms-shipping .ffClone").click(function(e) {
		e.preventDefault();

		var $shipping_rules_table = $(this).closest('#shipping_rules').find('table');

		var row_id = 1;

		var lastRow = $shipping_rules_table.find('tbody > tr:last input[name^="shipping[rules]"]:first');
		if (typeof lastRow != 'undefined') {
			row_id = parseInt(lastRow.attr('name').match(/[0-9]+/)) + 1;
		}

		var newRow = $shipping_rules_table.find('tr.ffSample').clone();

		var selectize_shipping_inputs = [];

		$(newRow).find(':input').each(function () {
			var $input = $(this);

			// Form fields
			if ($input.attr('name') != 'undefined' && $input.attr('name')) {
				var $name = $input.attr('name');
				$input.attr('name', $name.replace('[0]', '[' + row_id + ']'));
				$input.prop('disabled', false);
			}

			// Selectize fields
			if ($input.attr('id') != 'undefined' && $input.attr('id') && $input.attr('id').match(/^selectize\-[a-z\-]+\-0$/g)) {
				var $id = $input.attr('id').replace('0', row_id);
				$input.attr('id', $id);

				var reg = /^selectize\-(.*)\-[0-9]+$/g;
				var type = reg.exec($id);

				selectize_shipping_inputs.push({
					id: '#' + $id,
					type: type.length && type[1] ? type[1] : ""
				});
			}
		});

		$shipping_rules_table.find('tbody').append(newRow.removeClass('ffSample'));

		$(selectize_shipping_inputs).each(function () {
			initShippingSelectize(this.id, this.type);
		});

		toggleSubmitButton();
	});

	$(document).on('click', '#ms-shipping .mm_remove', function(e) {
		e.preventDefault();
		$(this).closest('tr').remove();
		toggleSubmitButton();
	});

	// Hide submit button if no rules added
	toggleSubmitButton();

	function toggleSubmitButton() {
		if ($('#shipping_rules').find('tbody tr:not(.ffSample)').length) {
			$("#ms-shipping-submit").show();
		} else {
			$("#ms-shipping-submit").hide();
		}
	}

	$("#ms-shipping-submit").click(function() {
		var $data = $('#ms-shipping').serializeArray();

		$.post('index.php?route=multimerch/shipping/jxSave', $data, function (json) {
			if (json.error) {
				$('#ms-shipping-error-holder').html(json.error).removeClass('hidden');
			} else if (json.success) {
				window.location.reload();
			}
		}, 'json');
	});
});