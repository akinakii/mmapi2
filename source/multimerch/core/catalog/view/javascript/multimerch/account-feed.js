$(function () {

    /**
	 * Validate source file url
     */
    $(document).on('input', 'input[name="import_file_source_url"]', function() {
		$('#proceed_to_step_mapping').attr('data-upload-info-exists', 1);
		$('#proceed_to_step_mapping').show();

        /*$.post($('base').attr('href') + "index.php?route=seller/account-feed/isImportFileByUrlValid", {url: this.value}, function (data) {
            var error_holder = $('#error-holder');
            error_holder.empty();

            if (data.errors.length) {
                $('#proceed_to_step_mapping').hide();

                for(error in data.errors) {
                    error_holder.append(data.errors[error] + '<BR>');
                }
                error_holder.show();
			} else {
				$('#proceed_to_step_mapping').attr('data-upload-info-exists', 1);
				$('#proceed_to_step_mapping').show();
                error_holder.hide();
			}
        }, 'json');*/
	});

	$('#step_upload').load('index.php?route=seller/account-feed/stepUpload', function() {
		$('.import-file-source').change(function () {
			var source = $(this).val();
			if (source == 'local') {
				if ($('#proceed_to_step_mapping').data('upload-info-exists')) {
                    $('#proceed_to_step_mapping').show();
				} else {
                    $('#proceed_to_step_mapping').hide();
				}
				$('#import-file-local').show();
				$('#import-file-url').hide();
			} else {
                $('#proceed_to_step_mapping').hide();
				$('#import-file-local').hide();
				$('#import-file-url').show();
			}
		});

		$('#retrieve-file-by-url').click(function () {

		});

		initUploader('ms-import-dragndrop', 'index.php?route=seller/account-feed/jxUploadFile', 'large', false, true);
	});

	// Mapping

	$(document).on('click', '#proceed_to_step_mapping', function() {
		$(this).attr('disabled', true).html('<i class="fa fa-spinner fa-spin"></i>');

		var is_url_source = $('input[name="import_file_source_url"]').is(':visible');

		if (is_url_source) {
			var source_url = $('input[name="import_file_source_url"]').val();
			$('#url_path').val(source_url);

			$.post($('base').attr('href') + "index.php?route=seller/account-feed/jxGetImportFileByUrl", {url: source_url}, function (json) {
				if (json.errors.length) {
					for (error in json.errors) {
						$('#error-holder').empty().append(json.errors[error] + '<BR>').show();
					}

					$('#proceed_to_step_mapping').attr('disabled', false).html(msGlobals.text.ms_feed_to_step_mapping);
				} else if (json.files.length && json.files[0].code != null) {
					$('#attachment_code').val(json.files[0].code);
					validateUploadAndProceed();
				}
			}, 'json');
		} else {
			validateUploadAndProceed();
		}
	});

	function validateUploadAndProceed() {
		var attachment_code = $('#attachment_code').val();
		var file_encoding = $('#file_encoding').val();

		$.ajax({
			type: 'post',
			url: 'index.php?route=seller/account-feed/jxValidateStepUpload',
			data: $('#step_upload_data input, #step_upload_data select'),
			dataType: 'json',
			success: function(json) {
				setTimeout(function () {
					$('#proceed_to_step_mapping').attr('disabled', false).html(msGlobals.text.ms_feed_to_step_mapping);
				}, 1000);

				if(json.errors) {
					error_text = '';
					for (error in json.errors) {
						error_text+= json.errors[error] + '<BR>';
					}
					$('#error-holder').empty().append(error_text).show();
				} else {
					$('#error-holder').hide();
					$('#step_mapping').load('index.php?route=seller/account-feed/stepMapping', function () {
						var selectors = $('#import_fields .property_selector');

						//delete selected options for other fields
						selectors.each(function(){
							if (this.value != 0){
								$(".property_selector option[value='"+this.value+"']").attr("disabled","disabled");
								$(".property_selector option[value='"+this.value+"']").attr("hidden","hidden");
							}
						});

						//if option is selected - delete this option for other fields
						var previous;
						selectors.on('focus', function () {
							selectors.removeClass('active');
							previous = this.value;
							$(".property_selector option[value='"+previous+"']").removeAttr("disabled");
							$(".property_selector option[value='"+previous+"']").removeAttr("hidden");
						}).on('change click',function() {
							$(".property_selector option[value='"+previous+"']").removeAttr("disabled");
							$(".property_selector option[value='"+previous+"']").removeAttr("hidden");
							$(this).addClass('active');
							var current_value = $(this).val();
							if (current_value){
								$(".property_selector:not(.active) option[value='"+current_value+"']").attr("disabled","disabled");
								$(".property_selector:not(.active) option[value='"+current_value+"']").attr("hidden","hidden");
							}
							previous = $(this).val();
						});

						nextImportStep(2);
					});
				}
			}
		});
	}

	$(document).on('click', '#proceed_to_step_confirm', function() {
		var mapping_data = [];
		$('#import_fields select option:selected').each(function(i,elem) {
			mapping_data.push($(this).val());
		});
		// console.log(mapping_data);
		$.ajax({
			type: 'post',
			url: 'index.php?route=seller/account-feed/jxValidateStepMapping',
			data: 'mapping_data='+JSON.stringify(mapping_data),
			dataType: 'json',
			success: function(json) {
				if(json.errors) {
					error_text = '';
					for (error in json.errors) {
						error_text+= json.errors[error] + '<BR>';
					}
					$('#error-holder').empty().append(error_text).show();
				} else {
					$('#error-holder').hide();
					$('#step_confirm').load('index.php?route=seller/account-feed/stepConfirm');
					nextImportStep(3);
				}

				$('html, body').animate({
					scrollTop: 0
				}, 1000);
			}
		});
	});

	$('body').on('click', '.tabslide .tabs .active, .prev_step', function(e){
		var next_id = $(this).data('next_id');
		nextImportStep(next_id);

		$('html, body').animate({
			scrollTop: 0
		}, 1000);
	});

	$('body').on('click', '.tabslide .tabs .tab', function(e){
		e.preventDefault();
	});
});


function nextImportStep(id) {
	$('.tabslide .tabs .tab').removeClass('active');
	$('.tabslide .tabs .tab').eq( id-1 ).addClass('active');

	$('#error-holder').hide();
	var tab_width = $('.tabs').children('.tab:first').width();
	var slider_width = $('#slider').width();

	$('#pointer').stop().animate({'left':((id - 1)*tab_width)+'px'});
	$('#slider').stop().animate({'left':-((id - 1)*slider_width)+'px'});
	$('.alert-success').hide();
}

function startImport() {
	$('#ms-import-form').submit();

	/*$.ajax({
		type: 'post',
		url: 'index.php?route=seller/account-feed/validate_import_data',
		//data: $('#step2_data input, #step2_data select'),
		dataType: 'json',
		success: function(json) {
			if(json.errors) {
				error_text = '';
				for (error in json.errors) {
					error_text+= json.errors[error] + '<BR>';
				}
				$('#error-holder').empty().append(error_text).show();

				$('html, body').animate({
					scrollTop: 0
				}, 1000);
			} else {
				$('#error-holder').hide();
				$('#ms-import-form').submit();
			}
		}
	});*/
};

$(document).on('click', '.file-holder .ms-remove, .remove-uploaded-file', function() {
	$(this).parent().remove();
	$('#ms-import-dragndrop').show();
	$('#ms-import-dragndrop ~ .ms-note').show();
	$('#attachment_code').val('');
	$.get($('base').attr('href') + "index.php?route=seller/account-feed/jxDeleteUploadedFileFromSession", function () {
        $('#proceed_to_step_mapping').data('upload-info-exists', 0);
        $('#proceed_to_step_mapping').hide();
    });
});

$(document).on('change', '[name="is_scheduled"]', function () {
	if ($(this).val() == 1) {
		$('.schedule_settings').show();
	} else {
		$('.schedule_settings').hide();
	}
});
$('[name="is_scheduled"]:first').trigger('change');