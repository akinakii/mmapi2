// Get seller reviews
$(function() {
	setTimeout(function() {
		createPagination();
		setTimeout(popupImages);
	}, 1500);

	// Create pagination for reviews
	function createPagination() {
		var container = $('#reviews-pag');

		var sources = [];
		$.each($('.review') || $(), function (key, review) {
			sources.push($(review)[0].outerHTML);
		});

		if (sources.length < 6)
			return;

		container.pagination({
			dataSource: sources,
			pageSize: 5,
			className: 'paginationjs-theme-blue paginationjs-small',
			hideWhenLessThanOnePage: true,
			showPrevious: false,
			showNext: false,
			callback: function(response){
				container.prev().html(response);

				// Call again here for correctly working popups and replies
				popupImages();
				// toggleComments();
			}
		});

		return container;
	}

	function popupImages() {
		$.each($('.review-thumbnails'), function(key, item) {
			$(item).magnificPopup({
				type: 'image',
				delegate: 'a',
				gallery: {
					enabled: true
				}
			});
		});
	}

	$(document).on('click', '.review .read-more a', function() {
		var totalHeight = 0;

		var $p  = $(this).parent();
		var $up = $p.closest('div');
		var $ps = $up.find("p:not('.read-more')");

		$ps.each(function() {
			totalHeight += $(this).outerHeight();
		});

		$up.css({
			"height": $up.height(),
			"max-height": 9999
		}).animate({
			"height": totalHeight
		});

		$p.fadeOut();
		$p.siblings('.read-less').fadeIn();

		return false;
	 });

	$(document).on('click', '.review .read-less a', function() {
		var $p  = $(this).parent();
		var $up = $p.closest('div');

		$up.css({
			"height": $up.height()
		}).animate({
			"height": 140
		});

		$p.fadeOut();
		$p.siblings('.read-more').fadeIn();

		return false;
	});
});