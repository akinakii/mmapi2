$(function() {
	$("#ms-submit-button").click(function() {
		var button = $(this);
		$('.success').remove();

		if (msGlobals.config_enable_rte == 1) {
			for (var instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}
		}

		saveSellerInfo(button);
	});

	$("#sellerinfo_avatar_files, #sellerinfo_banner_files").delegate(".ms-remove", "click", function() {
		var par = $(this).parent();
		par.addClass('hidden');
		par.find('input').val('');
		par.parent().find('.dragndropmini').show();
		par.parent().find('.dragndropmini').removeClass('hidden');
	});

	
	var avatar = initUploader('ms-avatar', 'index.php?route=seller/account-profile/jxUploadSellerAvatar', 'mini', false, false);
	var banner = initUploader('ms-banner', 'index.php?route=seller/account-profile/jxUploadSellerAvatar', 'mini', false, false);
        
    initLangSwitcher();
    
    function saveSellerInfo(button) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: $('base').attr('href') + 'index.php?route=seller/account-profile/jxsavesellerinfo',
			data: $("form#ms-sellerinfo").serialize(),
			beforeSend: function() {
				$('p.error').remove();
			},
			complete: function(jqXHR, textStatus) {
				if (textStatus != 'success') {
					button.show().prev('span.wait').remove();
					$(".warning.main").text(msGlobals.formError).show();
				}
			},
			success: function(jsonData) {
				if (!jQuery.isEmptyObject(jsonData.errors)) {
					$('#ms-submit-button').show().prev('span.wait').remove();
					$('.error').text('');

					for (var error in jsonData.errors) {
						if ($('#error_' + error).length > 0) {
							$('#error_' + error).text(jsonData.errors[error]);
							$('#error_' + error).parents('.form-group').addClass('has-error');
						} else if ($('[name="'+error+'"]').length > 0) {
							$('[name="' + error + '"]').parents('.form-group').addClass('has-error');
							$('[name="' + error + '"]').parents('div:first').append('<p class="error">' + jsonData.errors[error] + '</p>');
						} else $(".warning.main").append("<p>" + jsonData.errors[error] + "</p>").show();
					}

					$("html, body").animate({ scrollTop: 0 }, "slow");
				} else if (!jQuery.isEmptyObject(jsonData.data) && jsonData.data.amount) {
					$(".ms-payment-form form input[name='custom']").val(jsonData.data.custom);
					$(".ms-payment-form form input[name='amount']").val(jsonData.data.amount);
					$(".ms-payment-form form").submit();
					window.location.reload();
				} else if (jsonData.redirect) {
					window.location = jsonData.redirect;
				}
			}
		});
	}
	
	var cfg = {};
	if (msGlobals.config_allow_description_images == 1){
		if (msGlobals.config_description_images_type == 'upload'){
			cfg = {
				extraPlugins: 'videodetector,imageCustomUploader',
				uploadUrl: 'index.php?route=seller/account-profile/jxUploadDescriptionImage',
				removeButtons: 'base64image,Anchor,Subscript,Superscript'
			}
		}
	} else {
		cfg = {
			removeButtons: 'base64image,Anchor,Image,Subscript,Superscript'
		}
	}
		
	if (msGlobals.config_enable_rte == 1) {
		$('.ckeditor').each(function () {
			CKEDITOR.replace(this, cfg);
		
		});
	}
});
