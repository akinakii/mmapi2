$(function() {
	/**
	 * Common
	 */
	// Hide submit button for Payments and Shipping tabs
	$('.ms-seller-settings > ul.topbar > li > a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr('href'); // activated tab

		if (target == '#tab-payment-gateways' || target == '#tab-seller-shipping') {
			$('#ms-submit-button').closest('.buttons').hide();
		} else {
			$('#ms-submit-button').closest('.buttons').show();
		}
	});

	// Submit Account and Address tabs data
	$("#ms-submit-button").click(function (e) {
		$.ajax({
			url: $('base').attr('href') + 'index.php?route=seller/account-setting/jxSaveSellerInfo',
			type: 'POST',
			data: $('#ms-seller-account, #ms-seller-address, #ms-seller-holiday-mode').serialize(),
			dataType: 'json',
			success: function (jsonData) {
				if (!jQuery.isEmptyObject(jsonData.errors)) {
					$('.error').text('');

					for (error in jsonData.errors) {
						if ($('#error_' + error).length > 0) {
							$('#error_' + error).text(jsonData.errors[error]);
							$('#error_' + error).parents('.form-group').addClass('has-error');
						} else if ($('[name="settings[' + error + ']"]').length > 0) {
							$('[name="settings[' + error + ']"]').parents('.form-group').addClass('has-error');
							$('[name="settings[' + error + ']"]').parents('div:first').append('<p class="error" id="error_' + error + '">' + jsonData.errors[error] + '</p>');
						} else if ($('[name="' + error + '"]').length > 0) {
							$('[name="' + error + '"]').parents('.form-group').addClass('has-error');
							$('[name="' + error + '"]').parents('div:first').append('<p class="error" id="error_' + error + '">' + jsonData.errors[error] + '</p>');
						} else {
							$(".warning.main").append("<p>" + jsonData.errors[error] + "</p>").show();
						}
					}
				} else if (jsonData.redirect) {
					window.location = jsonData.redirect;
				}
			},
			error: function (error) {
				console.log(error);
			}
		});
	});

	/**
	 * Seller settings > Payments tab
	 */
	$("ul.pg-topbar li").click(function(e) {
		if($(document).find('.pg-message').length !== 0) {
			$(".pg-message").empty().removeClass('alert alert-danger alert-success').hide();
		}
	});

	$(".ms-pg-submit").click(function (e) {
		var form = $(this).closest('form');
		var pg_name = $(form).find('#pg-name').val();
		var data = $(form).serialize();

		$.ajax({
			url: 'index.php?route=multimerch/payment/' + pg_name + '/jxSaveSettings',
			data: data,
			dataType: 'json',
			type: 'post',
			success: function (jsonData) {
				if(jsonData['success']) {
					$('.pg-message').addClass('alert alert-success').html(jsonData['success']);
					$('.pg-message').show();
				}

				if(jsonData['error']) {
					var html = '';
					html += '<ul>';
					$.map(jsonData['error'], function(value, index) {
						html += '<li>' + value + '</li>';
					});
					html += '</ul>';
					$('.pg-message').addClass('alert alert-danger').html(html).show();
				}
			},
			error: function (error) {
				console.error(error);
			}
		});
	});
        
	initLangSwitcher();
});