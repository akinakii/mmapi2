// Get seller questions
$(function() {

	// Create pagination for questions
	setTimeout(createPagination, 1500);

	function createPagination() {
		var container = $('#questions-pag');

		var sources = [];
		$.each($('.question') || $(), function (key, question) {
			sources.push($(question)[0].outerHTML);
		});

		if (sources.length < 6)
			return;

		container.pagination({
			dataSource: sources,
			pageSize: 5,
			className: 'paginationjs-theme-blue paginationjs-small',
			hideWhenLessThanOnePage: true,
			showPrevious: false,
			showNext: false,
			callback: function(response){
				// script not entering here if less than one page, therefore double toggleAnswers() is needed
				container.closest('form').find('.questions').html(response);
			}
		});

		return container;
	}

	$(document).on('click', "#addQuestion", function(e) {
		e.preventDefault();

		var product_id = $(document).find('input[name="product_id"]').val();

		var data = $('#question-form').serialize();
		data['product_id'] = product_id;

		$.ajax({
			url: 'index.php?route=multimerch/product_question/jxAddQuestion',
			type: 'post',
			data: data,
			beforeSend: function() {
				$('#error-holder').empty().hide();
				$('#success-holder').empty().hide();
			},
			success: function(json) {
				if(json.errors) {
					$('textarea[name="question"]').parent().addClass('has-error');

					$.each(json.errors, function(key, val) {
						$('#error-holder').append(val + '<br>');
					});

					$('#error-holder').show();
				} else if (json.success) {
					$('textarea[name="question"]').parent().removeClass('has-error').val('');

					if (json.tab_content) {
						$('#tab-mm-questions').html(json.tab_content);
						$('#success-holder').append(json.success).show();
						createPagination();
					}

					if (json.tab_header) {
						$('a[href="#tab-mm-questions"]').html(json.tab_header);
					}
				}
			},
			error: function(error) {
				console.error(error);
			}
		});
	});
});