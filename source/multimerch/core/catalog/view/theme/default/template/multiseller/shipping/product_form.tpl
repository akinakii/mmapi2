<div id="ms-shipping">
	<div class="alert alert-danger hidden" id="ms-shipping-error-holder"></div>

	<?php if (!$this->config->get('msconf_shipping_per_product_override_allow')) { ?>
		<p><?php echo sprintf($ms_shipping_per_product_override_off_note, $this->url->link('seller/account-setting', '', 'SSL')); ?></p>
	<?php } else { ?>
		<?php if ((int)$this->config->get('msconf_shipping_type') !== \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT) { ?>
			<div class="override-note <?php echo empty($shipping['rules']) ? '' : 'hidden'; ?>" data-override="0">
				<p><?php echo sprintf($ms_shipping_per_product_override_off_note, $this->url->link('seller/account-setting', '', 'SSL')); ?></p>
				<a href="#" class="global-shipping-override"><?php echo $ms_shipping_per_product_override_off_btn; ?></a>
			</div>
			<div class="override-note <?php echo empty($shipping['rules']) ? 'hidden' : ''; ?>" data-override="1">
				<p><?php echo sprintf($ms_shipping_per_product_override_on_note, $this->url->link('seller/account-setting', '', 'SSL')); ?></p>
				<a href="#" class="global-shipping-override"><?php echo $ms_shipping_per_product_override_on_btn; ?></a>
			</div>
		<?php } ?>

		<div id="shipping-container" class="<?php echo (int)$this->config->get('msconf_shipping_type') !== \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT && empty($shipping['rules']) ? 'hidden' : ''; ?>">
			<div id="shipping_rules" class="table-responsive">
				<table class="table table-borderless table-hover">
					<thead>
						<tr>
							<td class="col-sm-3"><?php echo $ms_shipping_field_destinations; ?></td>

							<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
								<td class="col-sm-2"><?php echo $ms_shipping_field_shipping_method; ?></td>
							<?php } ?>

							<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
								<td class="col-sm-2"><?php echo $ms_shipping_field_delivery_time; ?></td>
							<?php } ?>

							<td class="col-sm-2"><?php echo $ms_shipping_field_cost; ?></td>

							<?php if (in_array('cost_per_additional_item', $shipping['fields'])) { ?>
								<td class="col-sm-3"><?php echo $ms_shipping_field_cost_ai; ?></td>
							<?php } ?>

							<td></td>
						</tr>
					</thead>

					<tbody>
						<tr class="ffSample">
							<td><input type="text" id="selectize-destinations-0" class="form-control" /></td>

							<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
								<td><input type="text" id="selectize-shipping-method-0" class="form-control" /></td>
							<?php } ?>

							<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
								<td><input type="text" id="selectize-delivery-time-0" class="form-control" /></td>
							<?php } ?>

							<td>
								<div class="form-inline">
									<div class="input-group">
										<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
										<?php } ?>

										<input type="text" class="form-control inline small mm_price" name="shipping[rules][0][cost]" value="0" size="3" disabled />

										<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
										<?php } ?>
									</div>
								</div>
							</td>

							<?php if (in_array('cost_per_additional_item', $shipping['fields'])) { ?>
								<td>
									<div class="form-inline">
										<div class="input-group">
											<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
												<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
											<?php } ?>

											<input type="text" class="form-control inline small mm_price" name="shipping[rules][0][cost_per_additional_item]" value="0" size="3" disabled />

											<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
												<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
											<?php } ?>
										</div>
									</div>
								</td>
							<?php } ?>

							<td><a class="icon-remove mm_remove" title="Delete"><i class="fa fa-times"></i></a></td>
						</tr>

						<?php if (!empty($shipping['rules'])) { ?>
							<?php $index = 1; ?>
							<?php foreach ($shipping['rules'] as $rule) { ?>
								<tr>
									<td>
										<input type="text" id="selectize-destinations-<?php echo $index; ?>" class="form-control" />
										<?php foreach ($rule['destinations'] as $geo_zone) { ?>
											<input type="hidden" name="shipping[rules][<?php echo $index; ?>][destinations][]" value="<?php echo $geo_zone['id']; ?>" data-name="<?php echo $geo_zone['name']; ?>" />
										<?php } ?>
									</td>

									<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
										<td>
											<input type="text" id="selectize-shipping-method-<?php echo $index; ?>" class="form-control" />
											<input type="hidden" name="shipping[rules][<?php echo $index; ?>][shipping_method_id]" value="<?php echo $rule['shipping_method_id']; ?>" data-name="<?php echo $rule['shipping_method_name']; ?>" />
										</td>
									<?php } ?>

									<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
										<td>
											<input type="text" id="selectize-delivery-time-<?php echo $index; ?>" class="form-control" />
											<input type="hidden" name="shipping[rules][<?php echo $index; ?>][delivery_time_id]" value="<?php echo $rule['delivery_time_id']; ?>" data-name="<?php echo $rule['delivery_time_name']; ?>" />
										</td>
									<?php } ?>

									<td>
										<div class="form-inline">
											<div class="input-group">
												<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
													<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
												<?php } ?>

												<input type="text" class="form-control inline small mm_price" name="shipping[rules][<?php echo $index; ?>][cost]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['cost']); ?>" size="3" />

												<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
													<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
												<?php } ?>
											</div>
										</div>
									</td>

									<?php if (in_array('cost_per_additional_item', $shipping['fields'])) { ?>
										<td>
											<div class="form-inline">
												<div class="input-group">
													<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
														<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
													<?php } ?>

													<input type="text" class="form-control inline small mm_price" name="shipping[rules][<?php echo $index; ?>][cost_per_additional_item]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['cost_per_additional_item']); ?>" size="3" />

													<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
														<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
													<?php } ?>
												</div>
											</div>
										</td>
									<?php } ?>

									<td><a class="icon-remove mm_remove" title="Delete"><i class="fa fa-times"></i></a></td>
								</tr>

								<?php $index++; ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>

				<div class="buttons">
					<label class="mm_label col-sm-2">
						<a class="ffClone"><?php echo $ms_shipping_btn_add_destination; ?></a>
					</label>
					<div class="col-sm-10"></div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>