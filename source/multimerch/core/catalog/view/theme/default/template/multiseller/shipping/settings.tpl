<form id="ms-shipping">
	<div class="alert alert-danger hidden" id="ms-shipping-error-holder"></div>

	<div style="margin-top: -35px;">
		<?php if (in_array('from_country_id', $shipping['fields'])) { ?>
			<div class="form-group">
				<label class="mm_label col-sm-3">
					<p class="ms-note"><?php echo $ms_shipping_field_shipping_from; ?></p>
					<input type="text" id="selectize-country-from" class="form-control" />
					<input type="hidden" name="shipping[from_country_id]" value="<?php echo isset($shipping['from_country']['country_id']) ? $shipping['from_country']['country_id'] : ''; ?>" data-name="<?php echo isset($shipping['from_country']['name']) ? $shipping['from_country']['name'] : ''; ?>" />
				</label>
				<div class="col-sm-9"></div>
			</div>
		<?php } else { ?>
			<input type="hidden" name="shipping[from_country_id]" value="" />
		<?php } ?>

		<div class="clearfix"></div>

		<?php if (in_array('processing_days', $shipping['fields'])) { ?>
			<div class="form-group">
				<label class="mm_label col-sm-3">
					<p class="ms-note"><?php echo $ms_shipping_field_processing_days; ?></p>
					<div class = "input-group">
						<span class="input-group-addon"><?php echo $ms_up_to; ?></span>
						<input type="text" class="form-control text-center" name="shipping[processing_days]" value="<?php echo isset($shipping['processing_days']) ? $shipping['processing_days'] : 0 ;?>" />
						<span class="input-group-addon"><?php echo $ms_days; ?></span>
					</div>
				</label>
				<div class="col-sm-9"></div>
			</div>
		<?php } else { ?>
			<input type="hidden" name="shipping[processing_days]" value="" />
		<?php } ?>
	</div>

	<div class="clearfix"></div>

	<div id="shipping_rules" class="table-responsive">
		<table class="table table-borderless table-hover">
			<thead>
				<tr>
					<td class="col-sm-3"><?php echo $ms_shipping_field_destinations; ?></td>

					<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
						<td class="col-sm-2"><?php echo $ms_shipping_field_shipping_method; ?></td>
					<?php } ?>

					<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
						<td class="col-sm-2"><?php echo $ms_shipping_field_delivery_time; ?></td>
					<?php } ?>

					<?php if (in_array('weight_from', $shipping['fields']) && in_array('weight_to', $shipping['fields'])) { ?>
						<td class="col-sm-2"><?php echo $ms_shipping_field_cart_weight; ?></td>
					<?php } ?>

					<?php if (in_array('total_from', $shipping['fields']) && in_array('total_to', $shipping['fields'])) { ?>
						<td class="col-sm-3"><?php echo $ms_shipping_field_cart_total; ?></td>
					<?php } ?>

					<td class="col-sm-3"><?php echo $ms_shipping_field_cost; ?></td>
					<td></td>
				</tr>
			</thead>

			<tbody>
				<tr class="ffSample">
					<td><input type="text" id="selectize-destinations-0" class="form-control" /></td>

					<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
						<td><input type="text" id="selectize-shipping-method-0" class="form-control" /></td>
					<?php } ?>

					<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
						<td><input type="text" id="selectize-delivery-time-0" class="form-control" /></td>
					<?php } ?>

					<?php if (in_array('weight_from', $shipping['fields']) && in_array('weight_to', $shipping['fields'])) { ?>
						<td>
							<input type="hidden" name="shipping[rules][0][weight_class_id]" value="<?php echo $this->config->get('config_weight_class_id'); ?>" disabled />

							<div class="form-inline">
								<div class="input-group">
									<input type="text" class="form-control inline small" name="shipping[rules][0][weight_from]" value="0" size="4" disabled />
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control inline small" name="shipping[rules][0][weight_to]" value="999" size="4" disabled />
								</div>
							</div>
						</td>
					<?php } ?>

					<?php if (in_array('total_from', $shipping['fields']) && in_array('total_to', $shipping['fields'])) { ?>
						<td>
							<div class="form-inline">
								<div class="input-group">
									<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
										<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
									<?php } ?>

									<input type="text" class="form-control inline small" name="shipping[rules][0][total_from]" value="0" size="4" disabled />
									<span class="input-group-addon">-</span>
									<input type="text" class="form-control inline small" name="shipping[rules][0][total_to]" value="999" size="4" disabled />

									<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
										<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
									<?php } ?>
								</div>
							</div>
						</td>
					<?php } ?>

					<td>
						<div class="form-inline">
							<div class="input-group">
								<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
									<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
								<?php } ?>

								<input type="text" class="form-control inline small mm_price" name="shipping[rules][0][cost]" value="0" size="3" disabled />

								<?php if (in_array('cost_per_weight_unit', $shipping['fields'])) { ?>
									<span class="input-group-addon">+</span>
									<input type="text" class="form-control inline small mm_price" name="shipping[rules][0][cost_per_weight_unit]" value="0" size="3" disabled />
								<?php } ?>

								<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
									<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
								<?php } ?>
							</div>
						</div>
					</td>

					<td><a class="icon-remove mm_remove" title="Delete"><i class="fa fa-times"></i></a></td>
				</tr>

				<?php if (!empty($shipping['rules'])) { ?>
					<?php $index = 1; ?>
					<?php foreach ($shipping['rules'] as $rule) { ?>
						<tr>
							<td>
								<input type="text" id="selectize-destinations-<?php echo $index; ?>" class="form-control" />
								<?php foreach ($rule['destinations'] as $geo_zone) { ?>
									<input type="hidden" name="shipping[rules][<?php echo $index; ?>][destinations][]" value="<?php echo $geo_zone['id']; ?>" data-name="<?php echo $geo_zone['name']; ?>" />
								<?php } ?>
							</td>

							<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
								<td>
									<input type="text" id="selectize-shipping-method-<?php echo $index; ?>" class="form-control" />
									<input type="hidden" name="shipping[rules][<?php echo $index; ?>][shipping_method_id]" value="<?php echo $rule['shipping_method_id']; ?>" data-name="<?php echo $rule['shipping_method_name']; ?>" />
								</td>
							<?php } ?>

							<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
								<td>
									<input type="text" id="selectize-delivery-time-<?php echo $index; ?>" class="form-control" />
									<input type="hidden" name="shipping[rules][<?php echo $index; ?>][delivery_time_id]" value="<?php echo $rule['delivery_time_id']; ?>" data-name="<?php echo $rule['delivery_time_name']; ?>" />
								</td>
							<?php } ?>

							<?php if (in_array('weight_from', $shipping['fields']) && in_array('weight_to', $shipping['fields'])) { ?>
								<td>
									<div class="form-inline">
										<div class="input-group">
											<input type="text" class="form-control inline small" name="shipping[rules][<?php echo $index; ?>][weight_from]" value="<?php echo $this->MsLoader->MsHelper->trueWeightFormat($rule['weight_from']); ?>" size="4" />
											<span class="input-group-addon">-</span>
											<input type="text" class="form-control inline small" name="shipping[rules][<?php echo $index; ?>][weight_to]" value="<?php echo $this->MsLoader->MsHelper->trueWeightFormat($rule['weight_to']); ?>" size="4" />
											<input type="hidden" name="shipping[rules][<?php echo $index; ?>][weight_class_id]" value="<?php echo $this->config->get('config_weight_class_id'); ?>" />
										</div>
									</div>
								</td>
							<?php } ?>

							<?php if (in_array('total_from', $shipping['fields']) && in_array('total_to', $shipping['fields'])) { ?>
								<td>
									<div class="form-inline">
										<div class="input-group">
											<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
												<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
											<?php } ?>

											<input type="text" class="form-control inline small" name="shipping[rules][<?php echo $index; ?>][total_from]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['total_from']); ?>" size="4" />
											<span class="input-group-addon">-</span>
											<input type="text" class="form-control inline small" name="shipping[rules][<?php echo $index; ?>][total_to]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['total_to']); ?>" size="4" />

											<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
												<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
											<?php } ?>
										</div>
									</div>
								</td>
							<?php } ?>

							<td>
								<div class="form-inline">
									<div class="input-group">
										<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
										<?php } ?>

										<input type="text" class="form-control inline small mm_price" name="shipping[rules][<?php echo $index; ?>][cost]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['cost']); ?>" size="3" />

										<?php if (in_array('cost_per_weight_unit', $shipping['fields'])) { ?>
											<span class="input-group-addon">+</span>
											<input type="text" class="form-control inline small mm_price" name="shipping[rules][<?php echo $index; ?>][cost_per_weight_unit]" value="<?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['cost_per_weight_unit']); ?>" size="3" />
										<?php } ?>

										<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
										<?php } ?>
									</div>
								</div>
							</td>

							<td><a class="icon-remove mm_remove" title="Delete"><i class="fa fa-times"></i></a></td>
						</tr>

						<?php $index++; ?>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>

		<div class="buttons">
			<label class="mm_label col-sm-2">
				<a class="ffClone"><?php echo $ms_shipping_btn_add_destination; ?></a>
			</label>
			<div class="col-sm-10"></div>
		</div>
	</div>

	<div class="pull-right">
		<a class="btn btn-primary" id="ms-shipping-submit"><span><?php echo $ms_button_save; ?></span></a>
	</div>
</form>