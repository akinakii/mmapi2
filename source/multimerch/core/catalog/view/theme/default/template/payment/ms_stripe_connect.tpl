<div class="text-center">
	<form action="<?php echo $pay_link; ?>" method="POST">
		<script
			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			data-key="<?php echo $public_key; ?>"
			data-amount="<?php echo $total; ?>"
			data-currency="<?php echo $currency; ?>"
			data-name="<?php echo $store_name; ?>"
			data-description="<?php echo $store_description; ?>"
			data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
			data-label="<?php echo $text_checkout_confirm; ?>"
			data-allow-remember-me="false"
			data-locale="auto">
		</script>
	</form>
</div>