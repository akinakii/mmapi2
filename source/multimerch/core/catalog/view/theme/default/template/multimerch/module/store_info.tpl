<div id="mm_store_info" class="row">
	<div class="col-sm-12">
		<div class="ms-box">
			<div class="avatar">
				<img src="<?php echo $seller['thumb']; ?>" alt="" />
			</div>
			<div class="name">
				<div>
					<?php if (!isset($this->request->get['ms_category_id'])) { ?>
						<h1><a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a></h1>
					<?php } else { ?>
						<a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>
					<?php } ?>

					<?php if (!empty($seller['settings']['slr_country'])) { ?>
						<i class="fa fa-map-marker"></i> <?php echo !empty($seller['settings']['slr_city']) ? $seller['settings']['slr_city'] . ',' : ''; ?> <?php echo $seller['settings']['slr_country']; ?>
					<?php } ?>
				</div>
			</div>

			<?php if ($this->config->get('msconf_reviews_enable')) { ?>
				<div class="rating">
					<span data-toggle="tooltip" title="<?php echo $seller['reviews']['tooltip']; ?>">
						<div class="ms-ratings main">
							<div class="ms-empty-stars"></div>
							<div class="ms-full-stars" style="width: <?php echo $seller['reviews']['rating'] * 20; ?>%"></div>
						</div>
					</span>
					(<?php echo $seller['reviews']['total']; ?>)
				</div>
			<?php } ?>

			<?php if (!empty($seller['show_view_store_button'])) { ?>
				<div id="mm_store_view">
					<a href="<?php echo $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id'], 'SSL'); ?>" class="btn btn-default btn-block" style="clear: both">
						<span><?php echo $ms_catalog_seller_profile_view_products; ?></span>
					</a>
				</div>
			<?php } ?>

			<?php if ($this->config->get('msconf_allow_favorite_sellers')) { ?>
				<div id="mm_store_follow">
					<div class="button-group">
						<button class="btn btn-default btn-block ms-follow-seller" data-seller_id="<?php echo $seller['seller_id']; ?>">
							<i class="fa fa-heart"></i> <span class="text"><?php echo !$seller['is_followed_by_customer'] ? $ms_favorite_seller_follow_button : $ms_favorite_seller_unfollow_button; ?></span> <span class="count"><?php if (!empty($seller['total_followers'])) { ?>(<?php echo $seller['total_followers']; ?>)<?php } ?></span>
						</button>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php if (!empty($seller['stats'])) { ?>
	<div id="mm_store_stats" class="row">
		<div class="col-sm-12">
			<div class="ms-box">
				<ul class="mm_stats">
					<?php foreach ($seller['stats'] as $stat) { ?>
						<li><b><?php echo $stat['title']; ?>:</b> <?php echo $stat['value']; ?></li>
					<?php } ?>

					<?php if ($this->config->get('msconf_complaints_enable')) { ?>
						<li><a href="<?php echo $this->url->link('multimerch/complaint', 'seller_id=' . $seller['seller_id'], 'SSL') ?>"><?php echo $ms_complaint_seller_link_title; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if (!empty($seller['badges'])) { ?>
	<div id="mm_store_badges" class="row">
		<div class="col-sm-12">
			<div class="ms-box badges">
				<?php foreach($seller['badges'] as $badge) { ?>
					<span data-toggle="tooltip" title="<?php echo $badge['name']; ?>">
						<img src="<?php echo $badge['image']; ?>" title="<?php echo $badge['description']; ?>" />
					</span>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php if (!empty($seller['social_links'])) { ?>
	<div id="mm_store_social_links" class="row">
		<div class="col-sm-12">
			<div class="ms-box">
				<!-- mm catalog seller profile social start -->
				<div class="ms-social-links">
					<ul>
						<?php foreach($seller['social_links'] as $link) { ?>
							<li><a target="_blank" href="<?php echo $this->MsLoader->MsHelper->addScheme($link['channel_value']); ?>"><img src="<?php echo $link['image']; ?>" /></a></li>
						<?php } ?>
					</ul>
				</div>
				<!-- mm catalog seller profile social end -->
			</div>
		</div>
	</div>
<?php } ?>

<?php if (!empty($seller['contact_form'])) { ?>
	<div id="mm_store_messages" class="row">
		<?php echo $seller['contact_form']; ?>
		<div class="col-sm-12">
			<div class="ms-box">
				<!-- mm catalog seller profile messaging start -->
				<div class="contact">
					<?php if ($this->customer->getId()) { ?>
					<div class="button-group">
						<button type="button" class="btn btn-default btn-block ms-sellercontact" data-toggle="modal" data-target="#contactDialog"><span><?php echo $ms_catalog_product_contact; ?></span></button>
					</div>
					<?php } else { ?>
						<?php echo sprintf($this->language->get('ms_sellercontact_signin'), $this->url->link('account/login', '', 'SSL'), $seller['nickname']); ?>
					<?php } ?>
				</div>
				<!-- mm catalog seller profile messaging end -->
			</div>
		</div>
	</div>
<?php } ?>