<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<?php if (isset($success) && $success) { ?>
	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
	<?php } ?>

	<div class="alert alert-danger" id="error-holder" style="display: none;"></div>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
			<div class="mm_dashboard">
				<h1><?php echo $heading; ?></h1>

				<form id="ms-import" class="tab-content ms-coupon">
					<input type="hidden" name="import_id" value="<?php echo $import['id']; ?>" />
					<input type="hidden" name="schedule_cycle_old" value="<?php echo $import['cycle']; ?>" />

					<fieldset id="mm_general">
						<legend><?php echo $ms_feed_import_settings; ?></legend>

						<div class="form-group">
							<label class="mm_label col-sm-2"><?php echo $ms_feed_import_settings_url; ?></label>
							<div class="col-sm-10">
								<input type="text" name="url_path" value="<?php echo $import['url_path']; ?>" class="form-control" />
							</div>
						</div>

						<div class="form-group">
							<label class="mm_label col-sm-2"><?php echo $ms_feed_import_settings_cycle; ?></label>
							<div class="col-sm-10">
								<select name="schedule_cycle" class="form-control" style="width: 120px;">
									<option value="5_minutes" <?php if ('5_minutes' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_5_minutes; ?></option>
									<option value="30_minutes" <?php if ('30_minutes' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_30_minutes; ?></option>
									<option value="1_hour" <?php if ('1_hour' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_1_hour; ?></option>
									<option value="4_hours" <?php if ('4_hours' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_4_hours; ?></option>
									<option value="12_hours" <?php if ('12_hours' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_12_hours; ?></option>
									<option value="24_hours" <?php if ('24_hours' === $import['cycle']) { ?>selected="selected"<?php } ?>><?php echo $ms_feed_import_schedule_24_hours; ?></option>
								</select>
								<!--<p class="ms-note"><?php echo $ms_feed_import_settings_cycle_note; ?></p>-->
							</div>
						</div>

						<div class="form-group">
							<label class="mm_label col-sm-2"><?php echo $ms_status; ?></label>
							<div class="col-sm-10">
								<label class="radio-inline">
									<input type="radio" name="status" value="1" <?php if (1 === (int)$import['status']) { ?>checked="checked"<?php } ?> />
									<?php echo $ms_enabled; ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" value="0" <?php if (0 === (int)$import['status']) { ?>checked="checked"<?php } ?> />
									<?php echo $ms_disabled; ?>
								</label>
							</div>
						</div>

						<div class="form-group">
							<label class="mm_label col-sm-2"><?php echo $ms_feed_import_settings_history; ?></label>
							<div class="col-sm-10">
								<div class="table-responsive">
									<table class="mm_dashboard_table table table-borderless table-hover" id="import-history">
										<tbody>
										<?php foreach ($import['history'] as $history) { ?>
											<tr>
												<td class="text-left mm_size_large"><?php echo $history['date_added']; ?></td>
												<td class="text-left"><?php echo $this->language->get('ms_feed_import_status_message_' . $history['status']); ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</fieldset>
				</form>

				<div class="buttons">
					<div class="pull-left"><a href="<?php echo $back; ?>"><span><?php echo $ms_button_cancel; ?></span></a></div>
					<div class="pull-right"><a class="btn btn-primary ms-spinner" id="ms-submit-button"><span><?php echo $ms_button_save; ?></span></a></div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	$(function () {
		$('#ms-submit-button').click(function () {
            $.ajax({
                url: $('base').attr('href') + 'index.php?route=seller/account-feed/jxUpdateScheduledImport',
                type: 'POST',
                data: $('#ms-import').serialize(),
                dataType: 'json',
				beforeSend: function () {
                    $("#error-holder").empty().hide();
                },
                success: function (jsonData) {
                    if (jsonData.errors) {
                        $('.error').text('');

                        for (error in jsonData.errors) {
                            $("#error-holder").append("<p>" + jsonData.errors[error] + "</p>").show();
                        }
                        $(window).scrollTop(0,0);
                    } else if (jsonData.redirect) {
                        window.location = jsonData.redirect;
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });
</script>
<?php echo $footer; ?>
