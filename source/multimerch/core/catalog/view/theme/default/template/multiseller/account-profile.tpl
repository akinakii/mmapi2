<?php echo $header; ?>

<div class="container account-seller-profile">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<?php if (isset($success) && $success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
	<?php } ?>

	<div class="alert alert-danger warning main error"></div>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>

		<div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
			<div class="mm_dashboard">
				<?php if (!empty($statustext)) { ?>
					<div class="alert alert-<?php echo $statusclass; ?>"><?php echo $statustext; ?></div>
				<?php } ?>

				<h1><?php echo $ms_account_sellerinfo_heading; ?></h1>
				<div class="tab-content">
					<form id="ms-sellerinfo">
						<fieldset>
							<legend><?php echo $ms_account_profile_general ;?></legend>
							<div class="lang-chooser">
								<?php $i = 0; foreach ($languages as $language) { ?>
									<img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo $i == 0 ? 'active' : ''; ?>" data-lang="<?php echo $language['code'] ?>">
									<?php $i++; ?>
								<?php } ?>
							</div>

							<!-- todo status check update -->
							<?php if ($seller['ms.seller_status'] == MsSeller::STATUS_DISABLED || $seller['ms.seller_status'] == MsSeller::STATUS_DELETED) { ?>
								<div class="ms-overlay"></div>
							<?php } ?>

							<div class="form-group required">
								<label class="mm_label col-sm-2 mm_req"><?php echo $ms_account_sellerinfo_nickname; ?></label>
								<div class="mm_form col-sm-10">
									<input type="text" class="form-control"  name="nickname" value="<?php echo $seller['ms.nickname']; ?>" />
									<p class="ms-note"><?php echo $ms_account_sellerinfo_nickname_note; ?></p>
								</div>
							</div>

							<?php if (!empty($seller_included_fields['slogan']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_slogan; ?></label>
									<div class="mm_form col-sm-10">
										<?php foreach ($languages as $language) { ?>
											<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
											<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
												<div class="lang-img-icon-input-text"><img src="<?php echo $img; ?>"></div>
											</div>
											<input type="text" name="languages[<?php echo $language['language_id']; ?>][slogan]" value="<?php echo isset($seller['descriptions'][$language['language_id']]['slogan']) ? htmlspecialchars_decode($seller['descriptions'][$language['language_id']]['slogan']) : ''; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
										<?php } ?>
										<p class="ms-note"><?php echo $ms_account_sellerinfo_slogan_note; ?></p>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['description']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_description; ?></label>
									<div class="col-sm-10">
										<?php $i = 0; foreach ($languages as $language) { ?>
											<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
											<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo $i == 0 ? 'true' : 'false'; ?>">
												<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
												<textarea name="languages[<?php echo $language['language_id']; ?>][description]" class="form-control ckeditor mm_input_language mm_flag_<?php echo $language['code']; ?>"><?php echo htmlspecialchars_decode($seller['descriptions'][$language['language_id']]['description']); ?></textarea>
											</div>
											<?php $i++; ?>
										<?php } ?>
										<p class="ms-note"><?php echo $ms_account_sellerinfo_description_note; ?></p>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['logo']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_avatar; ?></label>
									<div class="mm_form col-sm-10">
										<div id="sellerinfo_avatar_files">
											<div class="ms-image <?php if (!$seller['avatar']['name']) { ?>hidden<?php } ?>">
												<input type="hidden" name="avatar_name" value="<?php echo $seller['avatar']['name']; ?>" />
												<img src="<?php echo $seller['avatar']['thumb']; ?>" />
												<span class="ms-remove"><i class="fa fa-times"></i></span>
											</div>

											<div class="dragndropmini <?php if ($seller['avatar']['name']) { ?>hidden<?php } ?>" id="ms-avatar"><p class="mm_drophere"><?php echo $ms_drag_drop_click_here; ?></p></div>
											<p class="ms-note"><?php echo $ms_account_sellerinfo_avatar_note; ?></p>
											<div class="alert alert-danger" style="display: none;"></div>
											<div class="ms-progress progress"></div>
										</div>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['logo']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_banner; ?></label>
									<div class="mm_form col-sm-10">
										<div id="sellerinfo_banner_files">
											<div class="ms-image <?php if (!$seller['banner']['name']) { ?>hidden<?php } ?>">
												<input type="hidden" name="banner_name" value="<?php echo $seller['banner']['name']; ?>" />
												<img src="<?php echo $seller['banner']['thumb']; ?>" />
												<span class="ms-remove"><i class="fa fa-times"></i></span>
											</div>

											<div class="dragndropmini <?php if ($seller['banner']['name']) { ?>hidden<?php } ?>" id="ms-banner"><p class="mm_drophere"><?php echo $ms_drag_drop_click_here; ?></p></div>
											<p class="ms-note"><?php echo $ms_account_sellerinfo_banner_note; ?></p>
											<div class="alert alert-danger" style="display: none;"></div>
											<div class="ms-progress progress"></div>
										</div>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['website']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_website; ?></label>
									<div class="mm_form col-sm-10">
										<input type="text" class="form-control" name="settings[website]" value="<?php echo isset($seller['settings']['slr_website']) ? $seller['settings']['slr_website'] : ''; ?>">
										<p class="ms-note"><?php echo $ms_account_sellerinfo_website_note; ?></p>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['company']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_company; ?></label>
									<div class="mm_form col-sm-10">
										<input type="text" class="form-control" name="settings[company]" value="<?php echo isset($seller['settings']['slr_company']) ? $seller['settings']['slr_company'] : ''; ?>">
										<p class="ms-note"><?php echo $ms_account_sellerinfo_company_note; ?></p>
									</div>
								</div>
							<?php } ?>

							<?php if (!empty($seller_included_fields['phone']['enabled'])) { ?>
								<div class="form-group">
									<label class="mm_label col-sm-2"><?php echo $ms_account_sellerinfo_phone; ?></label>
									<div class="mm_form col-sm-10">
										<input type="text" class="form-control" name="settings[phone]" value="<?php echo isset($seller['settings']['slr_phone']) ? $seller['settings']['slr_phone'] : ''; ?>">
										<p class="ms-note"><?php echo $ms_account_sellerinfo_phone_note; ?></p>
									</div>
								</div>
							<?php } ?>
						</fieldset>

						<?php if ($this->config->get('msconf_sl_status')) { ?>
							<fieldset>
								<legend><?php echo $ms_sl_social_media; ?></legend>
								<?php foreach($social_channels as $channel) { ?>
									<div class="form-group social_links">
										<label class="col-sm-2 control-label">
											<img src="<?php echo $channel['image']; ?>" title="<?php echo $channel['name']; ?>" />
										</label>
										<div class="col-sm-10">
											<input type="text" class="form-control"  name="social_links[<?php echo $channel['channel_id'] ?>]" value="<?php echo isset($seller['social_links'][$channel['channel_id']]) ? $seller['social_links'][$channel['channel_id']]['channel_value'] : ''; ?>" />
											<p class="ms-note"><?php echo $channel['description']; ?></p>
										</div>
									</div>
								<?php } ?>
							</fieldset>
						<?php } ?>
					</form>
				</div>

				<div class="buttons">
					<?php if (!in_array($seller['ms.seller_status'], [MsSeller::STATUS_DISABLED, MsSeller::STATUS_DELETED])) { ?>
						<div class="pull-right">
							<a class="btn btn-primary ms-spinner" id="ms-submit-button">
								<span><?php echo $ms_button_submit; ?></span>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	var msGlobals = {
		config_language: '<?php echo $this->config->get("config_language") ;?>',
		uploadError: "<?php echo htmlspecialchars(sprintf($ms_error_file_upload_error, $ms_file_default_filename, $ms_file_unclassified_error), ENT_QUOTES, 'UTF-8'); ?>",
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
		config_enable_rte: '<?php echo $this->config->get('msconf_enable_rte'); ?>',
		config_allow_description_images: "<?php echo $this->config->get('msconf_allow_description_images'); ?>",
		config_description_images_type: "<?php echo $this->config->get('msconf_description_images_type'); ?>",
	};
</script>

<?php echo $footer; ?>