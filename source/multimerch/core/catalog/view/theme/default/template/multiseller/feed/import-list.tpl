<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <?php if (!empty($success)) { ?>
    <div class="alert alert-success"><?php echo $success; ?></div>
    <?php } ?>

    <?php if (!empty($error_warning)) { ?>
    <div class="alert alert-danger"><?php echo $error_warning; ?></div>
    <?php } ?>

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
            <div class="mm_dashboard ms-seller-settings">
                <h1><i class="fa fa-briefcase"></i><?php echo $ms_feed_import_list ;?></h1>

                <ul class="nav nav-tabs topbar">
                    <li class="active"><a href="#tab-imports-single" data-toggle="tab"><?php echo $ms_feed_import_list_single; ?></a></li>

                    <?php if ($this->config->get('msconf_scheduled_import_enabled')) { ?>
                        <li><a href="#tab-imports-scheduled" data-toggle="tab"><?php echo $ms_feed_import_list_scheduled; ?></a></li>
                    <?php } ?>
                </ul>

                <div class="tab-content">
                    <div id="tab-imports-single" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="mm_dashboard_table table table-borderless table-hover" id="list-seller-imports-single">
                                <thead>
                                    <tr>
                                        <td class="mm_size_large"><?php echo $ms_feed_import_date_run; ?></td>
                                        <td class="mm_size_small"><?php echo $ms_feed_import_rows_processed; ?></td>
                                        <td class="mm_size_small"><?php echo $ms_feed_import_rows_added; ?></td>
                                        <td class="mm_size_small"><?php echo $ms_feed_import_rows_updated; ?></td>
                                    </tr>
                                    <tr class="filter">
                                        <td><input type="text" class="input-date-datepicker"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="tab-imports-scheduled" class="tab-pane">
                        <div class="table-responsive">
                            <table class="mm_dashboard_table table table-borderless table-hover" id="list-seller-imports-scheduled">
                                <thead>
                                    <tr>
                                        <td><?php echo $ms_feed_import_settings_url; ?></td>
                                        <td class="mm_size_large"><?php echo $ms_feed_import_date_last_run; ?></td>
                                        <td class="mm_size_large"><?php echo $ms_feed_import_date_next_run; ?></td>
                                        <td class="mm_size_small"><?php echo $ms_status; ?></td>
                                        <td class="mm_size_small"></td>
                                    </tr>
                                    <tr class="filter">
                                        <td></td>
                                        <td><input type="text" class="input-date-datepicker"/></td>
                                        <td><input type="text" class="input-date-datepicker"/></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>

<?php $timestamp = time(); ?>
<script>
    $(document).ready(function() {
        $('#list-seller-imports-single').dataTable( {
            "sAjaxSource": "index.php?route=seller/account-feed/jxGetImportsTableData",
            "aoColumns": [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "processed", "sClass": "text-right" },
                { "mData": "added", "sClass": "text-right" },
                { "mData": "updated", "sClass": "text-right" },
            ],
            "aaSorting": [[0, 'desc']]
        });

        $('#list-seller-imports-scheduled').dataTable( {
            "sAjaxSource": "index.php?route=seller/account-feed/jxGetScheduledImportsTableData",
            "aoColumns": [
                { "mData": "url_path", "sClass": "text-left" },
                { "mData": "date_last_run", "sClass": "text-right" },
                { "mData": "date_next_run", "sClass": "text-right" },
                { "mData": "status", "sClass": "text-right" },
                { "mData": "actions", "bSortable": false, "sClass": "text-right" }
            ],
            "aaSorting": [[1, 'desc']]
        });


    });
</script>
<?php echo $footer; ?>