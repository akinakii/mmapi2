<?php foreach ($elements as $msf_attribute_id => $msf_attribute) { ?>
	<div class="multimerch_productfilter_block" id="<?php echo $msf_attribute['id']; ?>" data-type="msf_attribute" data-id="<?php echo $msf_attribute['data-id']; ?>">
		<h4><?php echo $msf_attribute['title']; ?></h4>
		<?php foreach ($msf_attribute['values'] as $v) { ?>
			<label class="checkbox-inline">
				<input
					type="checkbox"
					name="<?php echo $v['name']; ?>"
					value="<?php echo $v['value']; ?>"
					<?php echo $v['checked'] ? 'checked="checked"' : ''; ?>
					<?php echo !$v['enabled'] ? 'disabled="disabled"' : ''; ?>
				/>
				<span><?php echo $v['label']; ?></span>
			</label>
		<?php } ?>
	</div>
<?php } ?>
