<?php if (!empty($msf_variations_matrix)) { ?>
	<div class="form-group">
		<label class="mm_label col-sm-2 mm_req"><?php echo $ms_account_product_msf_variations_variants; ?></label>
		<div class="col-sm-10">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<?php foreach ($msf_variations_matrix[0]['values'] as $msf_variation_id => $value) { ?>
								<td><?php echo $value['msf_variation_name']; ?></td>
							<?php } ?>
							<td class="col-sm-3"><?php echo $ms_account_product_msf_variations_price; ?></td>
							<td class="col-sm-3"><?php echo $ms_account_product_msf_variations_quantity; ?></td>
							<td class="col-sm-2"><?php echo $ms_account_product_msf_variations_status; ?></td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($msf_variations_matrix as $row_id => $row) { ?>
							<tr>
								<?php foreach ($row['values'] as $msf_variation_id => $value) { ?>
									<td class="<?php if ($row['status'] == 0) { ?>ms-disabled<?php } ?>"><?php echo $value['name']; ?></td>
								<?php } ?>
								<td class="<?php if ($row['status'] == 0) { ?>ms-disabled<?php } ?>">
									<div class="input-group">
										<?php if($this->currency->getSymbolLeft($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?></span>
										<?php } ?>

										<input type="text" name="product_msf_variation[<?php echo $row['key']; ?>][price]" value="<?php echo $row['price']; ?>" class="form-control" <?php if ($row['status'] == 0) { ?>readonly tabIndex="-1"<?php } ?> />

										<?php if($this->currency->getSymbolRight($this->config->get('config_currency'))) { ?>
											<span class="input-group-addon"><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?></span>
										<?php } ?>
									</div>
								</td>
								<td class="<?php if ($row['status'] == 0) { ?>ms-disabled<?php } ?>">
									<input type="text" name="product_msf_variation[<?php echo $row['key']; ?>][quantity]" value="<?php echo $row['quantity']; ?>" class="form-control msf-variation-quantity" <?php if ($row['status'] == 0) { ?>readonly tabIndex="-1"<?php } ?> />
								</td>
								<td style="opacity: 1;">
									<select name="product_msf_variation[<?php echo $row['key']; ?>][status]" class="form-control">
										<option value="1" <?php if($row['status'] == 1) { ?>selected<?php } ?>><?php echo $text_yes; ?></option>
										<option value="0" <?php if($row['status'] == 0) { ?>selected<?php } ?>><?php echo $text_no; ?></option>
									</select>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php } ?>

<script>
	$(function () {
		$(document).on('change', '[name$="[status]"]', function () {
			if ($(this).val() == 0) {
				$(this).closest('tr').find('td').addClass('ms-disabled');
				$(this).closest('tr').find('input').attr('readonly', true).attr('tabIndex', '-1');
			} else {
				$(this).closest('tr').find('td').removeClass('ms-disabled');
				$(this).closest('tr').find('input').removeAttr('readonly').removeAttr('tabIndex');
			}
		});

		$(document).on('change', '.msf-variation-quantity', function () {
			if ($(this).val() <= 0) {
				$(this).addClass('has-warning');
			} else {
				$(this).removeClass('has-warning');
			}
		});

		$('.msf-variation-quantity').trigger('change');
	})
</script>

