<?php if ($logged) { ?>
    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
    <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
        <li role="separator" class="divider"></li>
        <li class="menu-header"><?php echo $ms_account_orders_customer ;?></li>
    <?php } ?>
    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
    <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
    <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
    <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
        <li role="separator" class="divider"></li>
        <li class='menu-header'><?php echo $ms_seller ;?></li>

        <?php if ($ms_seller_created && $this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_ACTIVE) { ?>
            <li><a href="<?php echo $this->url->link('seller/account-dashboard', '', 'SSL'); ?>"><?php echo $ms_account_dashboard; ?></a></li>
            <li><a href="<?php echo $this->url->link('seller/account-order', '', 'SSL'); ?>"><?php echo $ms_account_orders; ?></a></li>
            <li><a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>"><?php echo $ms_account_products; ?></a></li>
            <li><a href="<?php echo $this->url->link('seller/account-transaction', '', 'SSL'); ?>"><?php echo $ms_account_transactions; ?></a></li>
            <li><a href="<?php echo $this->url->link('seller/account-invoice', '', 'SSL'); ?>"><?php echo $ms_invoices; ?></a></li>

            <?php if ($this->config->get('msconf_allow_seller_coupons')) { ?>
                <li><a href="<?php echo $this->url->link('seller/account-coupon', '', 'SSL'); ?>"><?php echo $ms_seller_account_coupon; ?></a></li>
            <?php } ?>

            <li><a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>"><?php echo $ms_account_sellerinfo; ?></a></li>

            <?php if ($this->config->get('msconf_reviews_enable')) { ?>
                <li><a href="<?php echo $this->url->link('seller/account-review', '', 'SSL'); ?>"><?php echo $ms_account_reviews; ?></a></li>
            <?php } ?>

            <?php if ($this->config->get('msconf_allow_questions')) { ?>
                <li><a href="<?php echo $this->url->link('seller/account-question', '', 'SSL'); ?>"><?php echo $ms_account_questions; ?></a></li>
            <?php } ?>

            <li>
                <a href="<?php echo $this->url->link('seller/account-notification', '', 'SSL'); ?>" class="pull-left"><?php echo $ms_seller_account_notification; ?></a>
                <span class="label label-danger pull-right ms-notifications-dropdown-count"></span>
            </li>

            <li><a href="<?php echo $this->url->link('seller/account-setting', '', 'SSL'); ?>"><?php echo $ms_account_settings; ?></a></li>
        <?php } else if ($ms_seller_created && $this->MsLoader->MsSeller->getStatus($this->customer->getId()) == MsSeller::STATUS_UNPAID) { ?>
            <li><a href="<?php echo $this->url->link('seller/account-invoice', '', 'SSL'); ?>"><?php echo $ms_invoices; ?></a></li>

            <li>
                <a href="<?php echo $this->url->link('seller/account-notification', '', 'SSL'); ?>" class="pull-left"><?php echo $ms_seller_account_notification; ?></a>
                <span class="label label-danger pull-right ms-notifications-dropdown-count"></span>
            </li>

            <li><a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>"><?php echo $ms_account_sellerinfo; ?></a></li>
            <li><a href="<?php echo $this->url->link('seller/account-setting', '', 'SSL'); ?>"><?php echo $ms_account_settings; ?></a></li>
        <?php } else { ?>
            <?php if ($this->config->get('msconf_seller_landing_page_id')) { ?>
                <li><a href="<?php echo $this->url->link('information/information', 'information_id=' . $this->config->get('msconf_seller_landing_page_id'), 'SSL'); ?>"><?php echo $ms_account_sellerinfo_new_short; ?></a></li>
            <?php } else { ?>
                <li><a href="<?php echo $this->url->link('seller/account-profile', '', 'SSL'); ?>"><?php echo $ms_account_sellerinfo_new_short; ?></a></li>
            <?php } ?>
        <?php } ?>

        <li role="separator" class="divider"></li>
    <?php } ?>
    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
<?php } else { ?>
    <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
    <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
<?php } ?>