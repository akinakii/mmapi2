<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-body">
			<?php if ($is_authorized) { ?>
				<i class="fa fa-check success"></i>
				<div class="title">
					<?php if (!empty($follow)) { ?>
						<?php echo sprintf($ms_favorite_seller_success_follow, $seller_nickname); ?>
					<?php } elseif (!empty($unfollow)) { ?>
						<?php echo sprintf($ms_favorite_seller_success_unfollow, $seller_nickname); ?>
					<?php } ?>
				</div>
				<div class="buttons">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
						<?php echo $ms_favorite_seller_btn_continue; ?>
					</button>
					<a href="<?php echo $this->url->link('account/msfavoriteseller', '', 'SSL'); ?>" class="btn btn-default"><?php echo $ms_favorite_seller_btn_to_favorite_sellers; ?></a>
				</div>
			<?php } else { ?>
				<i class="fa fa-times error"></i>
				<div class="title"><?php echo sprintf($ms_favorite_seller_error_login, $seller_nickname); ?></div>
				<div class="buttons">
					<a href="<?php echo $this->url->link('account/login', '', 'SSL'); ?>" class="btn btn-primary"><?php echo $ms_favorite_seller_btn_login; ?></a>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						<?php echo $ms_favorite_seller_btn_continue; ?>
					</button>
				</div>
			<?php } ?>
		</div>
	</div>
</div>