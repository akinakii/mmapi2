<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<?php if (isset($import_result) && $import_result) { ?>
	<div class="alert alert-success"><?php echo $import_result; ?></div>
	<?php } ?>
	<div class="alert alert-danger" id="error-holder" style="display: none;"></div>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> import_content"><?php echo $content_top; ?>
			<div class="mm_dashboard">
				<h1><?php echo $this->language->get('ms_feed_title_import_products'); ?></h1>
				<div class="tabslide">
					<div class="slider">
						<div id="slider">
							<div id="step_upload"></div>
							<div id="step_mapping"></div>
							<div id="step_confirm"></div>
						</div>
					</div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	var msGlobals = {
		text: {
			ms_feed_to_step_mapping: "<?php echo $ms_feed_to_step_mapping; ?>"
		}
	};
</script>
<?php echo $footer; ?>