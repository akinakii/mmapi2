<div class="col-md-12">
	<div class="row">
		<div class="col-sm-12 step_header">
			<div class="col-md-4"></div>
			<div class="col-md-4 text-center step_header_text"><?php echo $ms_feed_step_upload_title; ?></div>
			<div class="col-md-4 text-right step_header_steps"><?php echo $ms_feed_step_upload_seq_number;?></div>
		</div>
		<div class="col-md-12">
			<form id="step_upload_data" class="form-horizontal">
				<div class="form-group" <?php if (!$this->config->get('msconf_feed_external_source_enabled')) { ?>style="display: none;"<?php } ?>>
					<label class="col-sm-2"><?php echo $ms_feed_source_import_type; ?></label>
					<div class="col-sm-10">
						<select class="form-control import-file-source">
							<option value="local"><?php echo $ms_feed_source_local; ?></option>
							<option value="url"><?php echo $ms_feed_source_url; ?></option>
						</select>
						<p class="ms-note"><?php echo $ms_feed_source_import_type_note; ?></p>
					</div>
				</div>
				<div class="form-group" id="import-file-local">
					<label class="col-sm-2"><?php echo $ms_feed_source_local_label; ?></label>
					<div class="col-sm-10">
						<div id="ms-import-dragndrop" <?php if (!empty($upload_info)) { ?>style="display: none;"<?php } ?>>
							<div class="alert alert-danger" style="display: none;"></div>
							<a class="btn btn-default" href="javascript:;" id="ms-import"><span><?php echo $ms_feed_label_select_file; ?></span></a>
							<div class="ms-progress progress"></div>
						</div>
						<p class="ms-note" <?php if (!empty($upload_info)) { ?>style="display: none;"<?php } ?>><?php echo $ms_feed_source_local_note; ?></p>

						<div id="import_file_result">
							<?php if (!empty($upload_info)) { ?>
								<div class="file-holder">
									<span class="filename"><?php echo (utf8_strlen($filename) > 100 ? mb_substr($filename, 0, 100) . '...' : $filename); ?></span>
									<span class="remove-uploaded-file"><i class="fa fa-times"></i></span>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="form-group" id="import-file-url" style="display: none;">
					<label class="col-sm-2"><?php echo $ms_feed_source_url_label; ?></label>
					<div class="col-sm-10">
						<input type="text" name="import_file_source_url" class="form-control" placeholder="<?php echo $ms_feed_source_url_placeholder; ?>" />
						<p class="ms-note"><?php echo $ms_feed_source_url_note; ?></p>
					</div>
				</div>

				<div class="form-group" style="display: none">
					<label class="col-sm-3 control-label"><?php echo $ms_feed_label_file_encoding; ?></label>
					<div class="col-sm-5">
						<select name="file_encoding" class="form-control">
							<?php foreach ($file_encodings as $file_encoding_key => $file_encoding_value) { ?>
								<?php if($file_encoding_key == $file_encoding) { ?>
									<option selected value="<?php echo $file_encoding_key; ?>"><?php echo $file_encoding_value; ?></option>
								<?php } else { ?>
									<option value="<?php echo $file_encoding_key; ?>"><?php echo $file_encoding_value; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>

				<input type="hidden" name="start_row" class="form-control" value="<?php echo $start_row; ?>" />
				<input type="hidden" name="finish_row" class="form-control" value="<?php echo $finish_row; ?>" />
				<input type="hidden" name="enclosure" class="form-control" value="<?php echo $enclosure; ?>" />
				<input type="hidden" name="delimiter" class="form-control" value="<?php echo $delimiter; ?>" />
				<input type="hidden"  name="attachment_code" value="<?php echo $attachment_code; ?>" id="attachment_code" />
				<input type="hidden"  name="url_path" value="<?php echo $url_path; ?>" id="url_path" />
			</form>
		</div>

		<div class="col-sm-12 step_footer">
			<div class="pull-right">
				<button id="proceed_to_step_mapping"
                        class="btn btn-primary"
                        data-upload-info-exists="<?php echo (int)!empty($upload_info) ?>"
                    <?php if (empty($upload_info)): ?> style="display: none" <?php endif; ?>>
                    <?php echo $ms_feed_to_step_mapping; ?>
                </button>
			</div>

			<a href="<?php echo $this->url->link('seller/account-product', '', 'SSL'); ?>" class="btn btn-default"><?php echo $ms_button_cancel; ?></a>
		</div>
	</div>
</div>