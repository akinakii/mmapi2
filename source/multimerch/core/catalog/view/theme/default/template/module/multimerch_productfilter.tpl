<div id="multimerch_productfilter" class="filter-container">
	<?php foreach ($blocks as $block) { ?>
		<?php echo $block; ?>
	<?php } ?>
</div>

<script>
	var ms_pf_config = {
		query_params: {
			common: "<?php echo $ms_pf_config['query_params']['common']; ?>",
			multimerch: "<?php echo $ms_pf_config['query_params']['multimerch']; ?>"
		},
		category_path: "<?php echo $ms_pf_config['category_path']; ?>",
		seller_id: "<?php echo $ms_pf_config['seller_id']; ?>",
		ms_category_id: "<?php echo $ms_pf_config['ms_category_id']; ?>",
		DOM_selectors: {
			module: "<?php echo $ms_pf_config['DOM_selectors']['module']; ?>",
			sort_limit_panel: "<?php echo $ms_pf_config['DOM_selectors']['sort_limit_panel']; ?>",
			products_container: "<?php echo $ms_pf_config['DOM_selectors']['products_container']; ?>",
			limit: "<?php echo $ms_pf_config['DOM_selectors']['limit']; ?>",
			sort: "<?php echo $ms_pf_config['DOM_selectors']['sort']; ?>",
			pagination: "<?php echo $ms_pf_config['DOM_selectors']['pagination']; ?>",
			products_count: "<?php echo $ms_pf_config['DOM_selectors']['products_count']; ?>"
		},
		urls: {
			render_category_page: "<?php echo $ms_pf_config['urls']['render_category_page']; ?>",
			current_category: "<?php echo $ms_pf_config['urls']['current_category']; ?>",
			render_seller_store_page: "<?php echo $ms_pf_config['urls']['render_seller_store_page']; ?>",
			current_seller_store_category: "<?php echo $ms_pf_config['urls']['current_seller_store_category']; ?>",
		},
		language: {
			text_no_products: "<?php echo $ms_pf_config['language']['text_no_products']; ?>"
		}
	};

	$(document).ready(function() {
		if ($('.filter-trigger-btn').length) {
			$('.filter-trigger-btn').addClass('active').html('<i class="icon-line-menu icon"></i> <b>Filter</b>');
			$(".filter-trigger-btn").click(function(){
				$('html').addClass('no-scroll side-filter-open');
				$('.body-cover').addClass('active');
			});
		}
	});
</script>