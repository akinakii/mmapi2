<?php echo $header; ?>
<div class="container ms-wishlist">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<h2><?php echo $heading_title; ?></h2>

			<?php if ($products) { ?>
				<div class="products-holder">
					<?php foreach ($products as $product) { ?>
						<div class="row ms-product-card">
							<div class="col-md-3 col-xs-12 image">
								<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
							</div>
							<div class="col-md-6 col-xs-12 caption">
								<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>

								<?php if ($product['seller']) { ?>
									<div class="seller">
										<div class="logo">
											<img src="<?php echo $product['seller']['seller_avatar']; ?>" alt="" />
										</div>
										<div class="info">
											<span class="name">
												<a href="<?php echo $product['seller']['profile_link']; ?>"><?php echo $product['seller']['seller_nickname']; ?></a>
											</span>
										</div>
									</div>
								<?php } ?>

								<?php if ($product['price']) { ?>
									<p class="price">
										<?php if (!$product['special']) { ?>
											<span class="price-new"><?php echo $product['price']; ?></span>
										<?php } else { ?>
											<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
										<?php } ?>
										<?php if (!empty($product['tax'])) { ?>
											<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
										<?php } ?>
									</p>
								<?php } ?>

								<?php if ($product['seller']['ms.rating']) { ?>
									<div class="rating">
										<?php for ($i = 1; $i <= 5; $i++) { ?>
											<?php if ($product['seller']['ms.rating'] < $i) { ?>
												<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											<?php } else { ?>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											<?php } ?>
										<?php } ?>

										<?php if (!$product['seller']['total_reviews']) { ?>
											<span>(<?php echo $mm_review_total_reviews_empty; ?>)</span>
										<?php } else { ?>
											<span>(<a class="bottom-dotted-link" href="<?php echo $this->url->link('product/product', 'product_id=' . $product['product_id'] . '#tab-reviews', 'SSL'); ?>"><?php echo sprintf((1 === (int)$product['seller']['total_reviews']) ? $mm_review_total_reviews_one : $mm_review_total_reviews_multiple, $product['seller']['total_reviews']); ?></a>)</span>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
							<div class="col-md-3 col-xs-12 actions">
								<div><?php echo $product['date_added']; ?></div>
								<div><a href="javascript:;" class="ms-wishlist-remove bottom-dotted-link" data-product_id="<?php echo $product['product_id']; ?>"><?php echo $ms_remove; ?></a></div>
							</div>
						</div>
					<?php } ?>
				</div>

				<div id="ms-wishlist-paginator"></div>
			<?php } else { ?>
				<p><?php echo $text_empty; ?></p>
			<?php } ?>

			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	$(function() {
		setTimeout(function () {
			if ($('#ms-wishlist-paginator').length) {
				createPagination();
			}
		}, 500);

		// Create pagination for reviews
		function createPagination() {
			var container = $('#ms-wishlist-paginator');

			var sources = [];
			$.each($('.ms-product-card') || $(), function (key, review) {
				sources.push($(review)[0].outerHTML);
			});

			if (sources.length < 6)
				return;

			container.pagination({
				dataSource: sources,
				pageSize: 5,
				className: 'paginationjs-theme-blue paginationjs-small',
				hideWhenLessThanOnePage: true,
				showPrevious: false,
				showNext: false,
				callback: function (response) {
					container.prev().html(response);
				}
			});

			return container;
		}

		$(document).on('click', '.ms-wishlist-remove', function () {
			var product_id = $(this).data('product_id');

			$.get("index.php?route=account/mswishlist/remove&product_id=" + product_id, function (json) {
				if (json.success) {
					window.location.reload();
				} else if (json.error) {
					alert(json.error);
				}
			}, 'json')
		});
	});
</script>

<?php echo $footer; ?>