<?php if (!empty($suborder_comments)) { ?>
	<a href="javascript:;" id="toggle-suborder-comments" class="bottom-dotted-link"><?php echo $mm_checkout_payment_method_add_comments; ?> <i class="fa fa-caret-down"></i></a>

	<div id="suborder-comments" style="display: none;">
		<?php foreach ($suborder_comments as $seller_id => $suborder_comment) { ?>
			<div class="suborder-comment">
				<p><strong><?php echo sprintf($mm_checkout_payment_method_suborder_comment_title, $suborder_comment['seller_name']); ?></strong></p>
				<textarea name="suborder_comments[<?php echo $seller_id; ?>]" rows="8" class="form-control"><?php echo $suborder_comment['comment']; ?></textarea>
			</div>
		<?php } ?>
	</div>

	<script>
		$('#toggle-suborder-comments').click(function () {
			$('#suborder-comments').toggle();

			if ($(this).find('i').hasClass('fa-caret-down')) {
				$(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		});
	</script>
<?php } ?>