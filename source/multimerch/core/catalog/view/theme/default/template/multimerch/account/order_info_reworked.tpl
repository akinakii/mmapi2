<?php echo $header; ?>
    <div class="container">
        <ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
        </ul>
		<?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
		<?php } ?>
		<?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
		<?php } ?>
        <div class="row"><?php echo $column_right; ?>
			<?php if ($column_left && $column_right) { ?>
				<?php $class = 'col-sm-6'; ?>
			<?php } elseif ($column_left || $column_right) { ?>
				<?php $class = 'col-sm-8 col-md-9'; ?>
			<?php } else { ?>
				<?php $class = 'col-sm-12'; ?>
			<?php } ?>
			<?php echo $column_left; ?>
			<div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
	            <?php echo $order_info_block ?>
				<?php echo $content_bottom; ?>
			</div>
			<?php echo $column_right; ?>
		</div>
    </div>
<?php echo $footer; ?>