<?php foreach ($elements as $option_id => $option) { ?>
	<div class="multimerch_productfilter_block" id="<?php echo $option['id']; ?>" data-type="oc_option" data-id="<?php echo $option['data-id']; ?>">
		<h4><?php echo $option['title']; ?></h4>
		<?php foreach ($option['values'] as $v) { ?>
			<label class="checkbox-inline">
				<input
					type="checkbox"
					name="<?php echo $v['name']; ?>"
					value="<?php echo $v['value']; ?>"
					<?php echo $v['checked'] ? 'checked="checked"' : ''; ?>
					<?php echo !$v['enabled'] ? 'disabled="disabled"' : ''; ?>
				/>
				<span><?php echo $v['label']; ?></span>
			</label>
		<?php } ?>
	</div>
<?php } ?>
