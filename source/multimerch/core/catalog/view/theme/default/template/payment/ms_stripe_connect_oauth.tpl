<div class="stripe-connect-settings">
	<?php if (!$account_connected) { ?>
		<div class="message">
			<p class="title"><?php echo $text_start_title; ?></p>
			<br/>
			<p class="description"><?php echo $text_start_description; ?></p>
		</div>
		<div class="button">
			<a id="ms-stripe-connect" href="<?php echo $oauth_link; ?>">
				<img src="<?php echo $button_image; ?>" alt="<?php echo $text_title; ?>">
			</a>
		</div>
	<?php } else { ?>
		<div class="alert alert-success">
			<i class="fa fa-check-circle"></i> <?php echo $success_account_connected_alert; ?>
		</div>
		<div class="message">
			<span style="font-weight: bold;"><?php echo $text_connected_account; ?></span>: <?php echo $stripe_user_id; ?>
		</div>
		<div class="button">
			<a class="btn btn-danger" href="<?php echo $deauth_link; ?>"><?php echo $text_deauthorize; ?></a>
		</div>
	<?php } ?>
</div>