<div class="multimerch_productfilter_block" data-type="price">
	<h4><?php echo $title; ?></h4>
	<?php foreach ($price_range as $type => $value) { ?>
		<div class="price-input">
			<?php if ($currency['symbol_left']) { ?>
				<span class="currency left"><?php echo $currency['symbol_left']; ?></span>
			<?php } ?>

			<input type="text" class="form-control" name="price[<?php echo $type; ?>]" placeholder="<?php echo $language['text_price_range_'.$type]; ?>" value="<?php echo !empty($value) ? $value : ''; ?>" maxlength="5" />

			<?php if ($currency['symbol_right']) { ?>
				<span class="currency right"><?php echo $currency['symbol_right']; ?></span>
			<?php } ?>
		</div>
	<?php } ?>

	<div class="price-search-button">
		<button type="button" class="btn btn-default" id="ms_filter_price_range"><?php echo $language['button_go']; ?></button>
	</div>
</div>