<div class="multimerch_tiles grid">
    <div class="title text-left">
        <h3><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['title']) ? $module['i18n'][$this->config->get('config_language_id')]['title'] : ''; ?></h3>
        <?php if (!empty($module['link'])) { ?>
            <span><a href="<?php echo $module['link']; ?>"><?php echo $ms_see_more; ?></a></span>
        <?php } ?>
    </div>

    <div class="subtitle"><?php echo !empty($module['i18n'][$this->config->get('config_language_id')]['subtitle']) ? $module['i18n'][$this->config->get('config_language_id')]['subtitle'] : ''; ?></div>

    <?php if ('seller' === (string)$module['item_type']) { ?>
        <div class="row ms-sellers-panel grid-<?php echo $module['num_cols']; ?>" id="mm_store_info">
            <?php foreach ($module['items'] as $seller) { ?>
                <div class="item seller" style="margin-bottom: 15px;">
                    <div class="avatar">
                        <a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" title="<?php echo $seller['nickname']; ?>" alt="<?php echo $seller['nickname']; ?>" /></a>
                    </div>

                    <div class="name">
                        <div>
                            <a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>

                            <?php if (!empty($seller['settings']['slr_country'])) { ?>
                                <i class="fa fa-map-marker"></i> <?php echo !empty($seller['settings']['slr_city']) ? $seller['settings']['slr_city'] . ',' : ''; ?> <?php echo $seller['settings']['slr_country']; ?>
                            <?php } ?>
                        </div>
                    </div>

                    <?php if ($this->config->get('msconf_reviews_enable')) { ?>
                        <div class="rating">
                            <span data-toggle="tooltip" title="<?php echo $seller['reviews']['tooltip']; ?>">
                                <div class="ms-ratings main">
                                    <div class="ms-empty-stars"></div>
                                    <div class="ms-full-stars" style="width: <?php echo $seller['reviews']['rating'] * 20; ?>%"></div>
                                </div>
                            </span>
                            (<?php echo $seller['reviews']['total']; ?>)
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    <?php } elseif ('oc_category' === (string)$module['item_type']) { ?>
        <div class="row grid-<?php echo $module['num_cols']; ?>">
            <?php foreach ($module['items'] as $category) { ?>
                <div class="text-center item oc_category">
                    <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                    <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-thumbnail" /></a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row grid-<?php echo $module['num_cols']; ?>">
            <?php foreach ($module['items'] as $product) { ?>
                <div class="item product-layout">
                    <div class="product-thumb transition">
                        <div class="image">
                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                        </div>
                        <div class="caption">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <p><?php echo $product['description']; ?></p>
                            <?php if ($product['seller']['ms.rating']) { ?>
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($product['seller']['ms.rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if ($product['price']) { ?>
                                <p class="price">
                                    <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                    <?php } else { ?>
                                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                    <?php } ?>
                                </p>
                            <?php } ?>
                        </div>
                        <div class="button-group">
                            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
