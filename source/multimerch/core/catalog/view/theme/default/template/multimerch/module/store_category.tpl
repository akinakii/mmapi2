<div id="mm_store_categories" class="row">
	<div class="col-sm-12">
		<div class="list-group">
			<a href="<?php echo $this->url->link('seller/catalog-seller/products', 'seller_id=' . $request['seller_id']); ?>" class="list-group-item"><b><?php echo $this->language->get('ms_all_products'); ?></b></a>

			<?php foreach ($ms_categories as $ms_category) { ?>
				<a href="<?php echo $ms_category['href'] ;?>" class="list-group-item <?php echo isset($request['ms_category_id']) && (int)$request['ms_category_id'] === (int)$ms_category['category_id']  ? 'active' : ''; ?>"><?php echo $ms_category['name'] ;?><span class="badge"><?php echo $ms_category['total'] ;?></span></a>

				<?php if (isset($ms_category['childs'])) { ?>
					<?php foreach ($ms_category['childs'] as $child) { ?>
						<a href="<?php echo $child['href']; ?>" class="list-group-item <?php echo isset($request['ms_category_id']) && (int)$request['ms_category_id'] === (int)$child['category_id'] ? 'active' : ''; ?>">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?><span class="badge"><?php echo $child['total'] ;?></span></a>

						<?php if (isset($child['childs'])) { ?>
							<?php foreach ($child['childs'] as $child_2) { ?>
								<a href="<?php echo $child_2['href']; ?>" class="list-group-item <?php echo isset($request['ms_category_id']) && (int)$request['ms_category_id'] === (int)$child_2['category_id'] ? 'active' : ''; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <?php echo $child_2['name']; ?><span class="badge"><?php echo $child_2['total'] ;?></span></a>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>