<?php echo $header; ?>
<div class="container register-seller">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<div class="alert alert-danger warning main"></div>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<h1><?php echo $ms_account_register_seller; ?></h1>
			<p><?php echo $text_account_already; ?></p>

			<form id="seller-registration-form" class="form-horizontal tab-content">
				<?php if (!empty($customer['customer_id'])) { ?>
					<input type="hidden" name="customer_id" value="<?php echo $customer['customer_id']; ?>" />
				<?php } ?>

				<div class="lang-chooser">
					<?php $i = 0; foreach ($languages as $language) { ?>
						<img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo $i == 0 ? 'active' : ''; ?>" data-lang="<?php echo $language['code'] ?>">
					<?php $i++; } ?>
				</div>

				<fieldset id="account-information">
					<legend><?php echo $ms_account_register_account_information; ?></legend>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $ms_account_register_firstname; ?></label>
						<div class="col-sm-10">
							<input type="text" name="firstname" placeholder="<?php echo $ms_account_register_firstname; ?>" class="form-control" value="<?php echo !empty($customer['firstname']) ? $customer['firstname'] : ''; ?>" />
						</div>
					</div>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $ms_account_register_lastname; ?></label>
						<div class="col-sm-10">
							<input type="text" name="lastname" placeholder="<?php echo $ms_account_register_lastname; ?>" class="form-control" value="<?php echo !empty($customer['lastname']) ? $customer['lastname'] : ''; ?>" />
						</div>
					</div>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $ms_account_register_email; ?></label>
						<div class="col-sm-10">
							<input type="email" name="email" placeholder="<?php echo $ms_account_register_email; ?>" class="form-control" value="<?php echo !empty($customer['email']) ? $customer['email'] : ''; ?>" />
						</div>
					</div>

					<?php if (!isset($customer['password'])) { ?>
						<div class="form-group required">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_password; ?></label>
							<div class="col-sm-10">
								<input type="password" name="password" placeholder="<?php echo $ms_account_register_password; ?>" class="form-control" />
							</div>
						</div>

						<div class="form-group required">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_password_confirm; ?></label>
							<div class="col-sm-10">
								<input type="password" name="password_confirm" placeholder="<?php echo $ms_account_register_password_confirm; ?>" class="form-control" />
							</div>
						</div>
					<?php } ?>
				</fieldset>

				<fieldset id="store-information">
					<legend><?php echo $ms_account_register_store_information; ?></legend>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $ms_stripe_subscription_plan_selected; ?></label>
						<div class="col-sm-10" style="padding-top: 7px;">
							<input type="hidden" name="seller_group_id" value="<?php echo $seller_group_id; ?>" />
							<input type="hidden" name="stripe_card_token" value="" />

							<strong><?php echo $seller_group['name']; ?></strong> <?php if (!empty($stripe_plan_terms)) { ?>(<?php echo $stripe_plan_terms; ?>)<?php } ?>
						</div>
					</div>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $ms_account_register_store_name; ?></label>
						<div class="col-sm-10">
							<input type="text" name="nickname" placeholder="<?php echo $ms_account_register_store_name; ?>" class="form-control" />
						</div>
					</div>

					<?php if (!empty($seller_included_fields['slogan']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_slogan; ?></label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
									<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
										<div class="lang-img-icon-input-text"><img src="<?php echo $img; ?>"></div>
									</div>
									<input type="text" name="languages[<?php echo $language['language_id']; ?>][slogan]" placeholder="<?php echo $ms_account_register_slogan; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
								<?php } ?>
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['description']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_description; ?></label>
							<div class="col-sm-10">
								<?php $i = 0; foreach ($languages as $language) { ?>
									<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
									<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo $i == 0 ? 'true' : 'false'; ?>">
										<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
										<textarea name="languages[<?php echo $language['language_id']; ?>][description]" class="form-control ckeditor mm_input_language mm_flag_<?php echo $language['code']; ?>"></textarea>
									</div>
									<?php $i++; ?>
								<?php } ?>
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['website']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_website; ?></label>
							<div class="col-sm-10">
								<input type="text" name="website" placeholder="<?php echo $ms_account_register_website; ?>" class="form-control" />
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['company']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_company; ?></label>
							<div class="col-sm-10">
								<input type="text" name="company" placeholder="<?php echo $ms_account_register_company; ?>" class="form-control" />
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['phone']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_phone; ?></label>
							<div class="col-sm-10">
								<input type="text" name="phone" placeholder="<?php echo $ms_account_register_phone; ?>" class="form-control" />
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['logo']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_avatar; ?></label>
							<div class="col-sm-10">
								<div id="sellerinfo_avatar_files">
									<div class="ms-image hidden">
										<input type="hidden" name="avatar" />
										<img src="" />
										<span class="ms-remove"><i class="fa fa-times"></i></span>
									</div>

									<div class="dragndropmini" id="ms-avatar"><p class="mm_drophere"><?php echo $ms_drag_drop_click_here; ?></p></div>
									<p class="ms-note"><?php echo $ms_account_sellerinfo_avatar_note; ?></p>
									<div class="alert alert-danger" style="display: none;"></div>
									<div class="ms-progress progress"></div>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['banner']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_banner; ?></label>
							<div class="col-sm-10">
								<div id="sellerinfo_banner_files">
									<div class="ms-image hidden">
										<input type="hidden" name="banner" />
										<img src="" />
										<span class="ms-remove"><i class="fa fa-times"></i></span>
									</div>

									<div class="dragndropmini" id="ms-banner"><p class="mm_drophere"><?php echo $ms_drag_drop_click_here; ?></p></div>
									<p class="ms-note"><?php echo $ms_account_sellerinfo_banner_note; ?></p>
									<div class="alert alert-danger" style="display: none;"></div>
									<div class="ms-progress progress"></div>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if ($this->config->get('msconf_msf_seller_property_enabled') && !empty($msf_seller_properties['default'])) { ?>
						<?php foreach ($msf_seller_properties['default'] as $msf_seller_property) { ?>
							<div class="form-group <?php if ($msf_seller_property['required']) { ?>required<?php } ?>">
								<label class="col-sm-2 control-label"><?php echo $msf_seller_property['name']; ?></label>
								<div class="col-sm-10">
									<?php if ('select' === $msf_seller_property['type']) { ?>
										<select name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" class="form-control">
											<?php foreach ($msf_seller_property['values'] as $value) { ?>
												<option value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>selected="selected"<?php } ?>><?php echo $value['name']; ?></option>
											<?php } ?>
										</select>
									<?php } elseif ('checkbox' === $msf_seller_property['type']) { ?>
										<div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
											<?php foreach ($msf_seller_property['values'] as $value) { ?>
												<div class="checkbox">
													<label>
														<input type="checkbox" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>checked="checked"<?php } ?> />
														<?php echo $value['name'] ?: ''; ?>
													</label>
												</div>
											<?php } ?>
										</div>
									<?php } elseif ('text' === $msf_seller_property['type']) { ?>
										<?php foreach ($languages as $language) { ?>
											<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
											<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
												<div class="lang-img-icon-input-text"><img src="<?php echo $img; ?>"></div>
											</div>
											<input type="text" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][text_values][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']]) ? $msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']] : ''; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
										<?php } ?>
									<?php } ?>

									<?php if (!empty($msf_seller_property['note'])) { ?>
										<div class="ms-note">
											<?php echo $msf_seller_property['note']; ?>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				</fieldset>

				<fieldset>
					<legend><?php echo $ms_account_register_address_information; ?></legend>

					<?php if (!empty($seller_included_fields['fullname']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_address_fullname; ?></label>

							<div class="mm_form col-sm-10">
								<input type="text" class="form-control" name="address[fullname]" placeholder="<?php echo $ms_account_register_address_fullname; ?>">
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['address_1']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_address_line1; ?></label>

							<div class="col-sm-10">
								<input type="text" class="form-control" name="address[address_1]" placeholder="<?php echo $ms_account_register_address_line1 ;?>">
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['address_2']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_address_line2; ?></label>

							<div class="col-sm-10">
								<input type="text" class="form-control" name="address[address_2]" placeholder="<?php echo $ms_account_register_address_line2 ;?>">
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['city']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_city; ?></label>

							<div class="col-sm-10">
								<input type="text" class="form-control" name="address[city]" placeholder="<?php echo $ms_account_register_city; ?>">
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['state']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_state; ?></label>

							<div class="col-sm-10">
								<input type="text" class="form-control" name="address[state]" placeholder="<?php echo $ms_account_register_state ;?>">
							</div>
						</div>
					<?php } ?>

					<?php if (!empty($seller_included_fields['zip']['enabled_reg'])) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_account_register_zip; ?></label>

							<div class="col-sm-10">
								<input type="text" class="form-control" name="address[zip]" placeholder="<?php echo $ms_account_register_zip ;?>">
							</div>
						</div>
					<?php } ?>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $ms_account_register_country; ?></label>

						<div class="col-sm-10">
							<select class="form-control" name="address[country_id]">
								<option value="0" disabled="disabled" selected="selected"><?php echo $ms_seller_country_select; ?></option>

								<?php foreach($countries as $country) { ?>
									<option value="<?php echo $country['country_id'] ;?>"><?php echo $country['name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</fieldset>

				<div class="buttons">
					<div class="pull-right">
						<?php if ($text_agree) { ?>
							<?php echo $text_agree; ?>
							<?php if ($agree) { ?>
							<input type="checkbox" name="agree" value="1" checked="checked" />
							<?php } else { ?>
							<input type="checkbox" name="agree" value="1" />
							<?php } ?>
							&nbsp;
						<?php } ?>
						<a class="btn btn-primary ms-spinner" id="ms-submit-button" value="<?php echo $button_continue; ?>"><span><?php echo $button_continue; ?></span></a>
					</div>
				</div>
			</form>

			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	var msGlobals = {
		config_language: '<?php echo $this->config->get("config_language") ;?>',
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
        stripeSubscriptionEnabled: "<?php echo (int)$this->config->get('ms_stripe_connect_enable_subscription'); ?>",
        stripeSubscriptionEnabledForSellerGroup: "<?php echo (int)!empty($seller_group['stripe_subscription_enabled']); ?>",
        stripePublicKey: "<?php echo !empty($this->config->get('ms_stripe_connect_environments')[$this->config->get('ms_stripe_connect_test_mode')]['public_key']) ? $this->config->get('ms_stripe_connect_environments')[$this->config->get('ms_stripe_connect_test_mode')]['public_key'] : ''; ?>",
        stripeDescription: "<?php echo !empty($stripe_plan_terms) ? $stripe_plan_terms : ''; ?>",
        stripeName: "<?php echo $this->config->get('config_name'); ?>",
        stripePanelLabel: "<?php echo $ms_stripe_subscription_button_signup; ?>",
		config_allow_description_images: "<?php echo $this->config->get('msconf_allow_description_images'); ?>",
		config_description_images_type: "<?php echo $this->config->get('msconf_description_images_type'); ?>",
	};
</script>
<?php echo $footer; ?>