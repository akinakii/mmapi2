<h3><?php echo $mm_product_shipping_title; ?></h3>

<?php if (!empty($product_is_digital)) { ?>
	<div><?php echo $mm_product_shipping_digital_product; ?></div>
<?php } elseif (!empty($shipping['from_country']) || isset($shipping['processing_days']) || !empty($shipping['rules'])) { ?>
	<?php if (!empty($shipping['from_country'])) { ?>
		<p><?php echo $ms_shipping_field_shipping_from; ?>: <?php echo $shipping['from_country']['name']; ?></p>
	<?php } ?>
	<?php if (isset($shipping['processing_days'])) { ?>
		<p><?php echo $ms_shipping_field_processing_days; ?>: <?php echo $shipping['processing_days']; ?></p>
	<?php } ?>
	<?php if (!empty($shipping['rules'])) { ?>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<td class="col-sm-3"><?php echo $ms_shipping_field_destinations; ?></td>

						<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
							<td class="col-sm-2"><?php echo $ms_shipping_field_shipping_method; ?></td>
						<?php } ?>

						<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
							<td class="col-sm-2"><?php echo $ms_shipping_field_delivery_time; ?></td>
						<?php } ?>

						<?php if (in_array('weight_from', $shipping['fields']) && in_array('weight_to', $shipping['fields'])) { ?>
							<td class="col-sm-2"><?php echo $ms_shipping_field_cart_weight; ?></td>
						<?php } ?>

						<?php if (in_array('total_from', $shipping['fields']) && in_array('total_to', $shipping['fields'])) { ?>
							<td class="col-sm-3"><?php echo $ms_shipping_field_cart_total; ?></td>
						<?php } ?>

						<td class="col-sm-3"><?php echo $ms_shipping_field_cost; ?></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($shipping['rules'] as $rule) { ?>
						<tr>
							<td>
								<?php foreach ($rule['destinations'] as $geo_zone) { ?>
									<?php echo $geo_zone['name']; ?>
								<?php } ?>
							</td>

							<?php if (in_array('shipping_method_id', $shipping['fields'])) { ?>
								<td><?php echo $rule['shipping_method_name']; ?></td>
							<?php } ?>

							<?php if (in_array('delivery_time_id', $shipping['fields'])) { ?>
								<td><?php echo $rule['delivery_time_name']; ?></td>
							<?php } ?>

							<?php if (in_array('weight_from', $shipping['fields']) && in_array('weight_to', $shipping['fields'])) { ?>
								<td><?php echo $this->MsLoader->MsHelper->trueWeightFormat($rule['weight_from']); ?> - <?php echo $this->MsLoader->MsHelper->trueWeightFormat($rule['weight_to']); ?></td>
							<?php } ?>

							<?php if (in_array('total_from', $shipping['fields']) && in_array('total_to', $shipping['fields'])) { ?>
								<td><?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['total_from']); ?> - <?php echo $this->MsLoader->MsHelper->trueCurrencyFormat($rule['total_to']); ?></td>
							<?php } ?>

							<td>
								<?php echo $rule['cost_formatted']; ?>
								<?php echo isset($rule['cost_per_weight_unit']) && (float)$rule['cost_per_weight_unit'] ? sprintf($ms_shipping_placeholder_cost_pwu, $rule['cost_per_weight_unit_formatted']) : ''; ?>
								<?php echo isset($rule['cost_per_additional_item']) && (float)$rule['cost_per_additional_item'] ? sprintf($ms_shipping_placeholder_cost_ai, $rule['cost_per_additional_item_formatted']) : ''; ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	<?php } ?>
<?php } else { ?>
	<div><?php echo $mm_product_shipping_not_specified; ?></div>
<?php } ?>
