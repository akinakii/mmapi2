<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
			<div class="mm_dashboard">
				<h1><i class="fa fa-bell"></i><?php echo $ms_seller_account_notification; ?></h1>

				<div id="tab-notification">
					<div class="table-responsive">
						<table class="mm_dashboard_table table table-borderless table-hover" id="list-notifications">
							<thead>
								<tr>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	$(function () {
		$('#list-notifications').dataTable( {
			"sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-notification/getTableData",
			"aoColumns": [
				{ "mData": "avatar", "bSortable": false },
				{ "mData": "message", "bSortable": false, "sClass": "text-left" },
				{ "mData": "date_created", "bSortable": false }
			],
			"aaSorting":  [[2,'desc']],
			"fnDrawCallback": function ( oSettings ) {
				$(oSettings.nTHead).hide();
			}
		});
	});
</script>

<?php echo $footer; ?>