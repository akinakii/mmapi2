<div class="multimerch_productfilter_block" data-type="oc_category">
	<h4><?php echo $title; ?></h4>
	<div class="category_rows">
		<ul>
			<?php foreach (['parent', 'current'] as $type) { ?>
				<?php if (!empty($elements[$type])) { ?>
					<li class="<?php echo $type; ?>"><a class="<?php echo !$elements[$type]['enabled'] ? 'noclick' : ''; ?>" href="<?php echo $elements[$type]['href']; ?>"><?php echo $elements[$type]['label']; ?></a></li>
				<?php } ?>
			<?php } ?>

			<?php if (!empty($elements['child'])) { ?>
				<?php foreach ($elements['child'] as $child) { ?>
					<li class="child"><a class="<?php echo !$child['enabled'] ? 'noclick' : ''; ?>" href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;- <?php echo $child['label']; ?></a></li>
				<?php } ?>
			<?php } ?>
		</ul>
	</div>
</div>