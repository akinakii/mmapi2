<?php echo $header; ?>
<div class="container ms-catalog-seller-products">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
		<?php } ?>
	</ul>
	<?php if (isset($holiday_mode_message) && $holiday_mode_message) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $holiday_mode_message; ?></div>
	<?php } ?>
	<?php if (!empty($ms_shipping_rules_empty_message)) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $ms_shipping_rules_empty_message; ?></div>
	<?php } ?>
	<div class="row">
		<?php if (isset($this->request->get['ms_category_id'])) { ?>
			<div id="mm_page_title" class="col-sm-12">
				<h1><?php echo $h1; ?></h1>
			</div>
		<?php } ?>

		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>

		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="row">
				<div class="mm_top_products_left col-sm-6">
					<div class="btn-group hidden-xs hidden">
						<button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
						<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
					</div>
				</div>

				<div class="mm_top_products_right col-sm-6" id="mspf_sort_limit_panel">
					<div class="mm_sort_group">
						<label class="control-label" for="input-sort"><?php echo $this->language->get('text_sort'); ?></label>
						<select id="input-sort" class="form-control">
							<?php foreach ($sorts as $sorts) { ?>
								<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
									<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
								<?php } else { ?>
									<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class="mm_sort_group">
						<label class="control-label" for="input-limit"><?php echo $this->language->get('text_limit'); ?></label>
						<select id="input-limit" class="form-control">
							<?php foreach ($limits as $limits) { ?>
								<?php if ($limits['value'] == $limit) { ?>
									<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
								<?php } else { ?>
									<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="cl"></div>
			</div>

			<div class="row" id="mspf_products_container">
				<?php if (!empty($seller['products'])) { ?>
					<?php foreach ($seller['products'] as $product) { ?>
						<div class="product-layout product-list col-xs-12">
							<div class="product-thumb">
								<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
								<div class="caption">
									<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
									<p><?php echo $product['description']; ?></p>
									<?php if ($product['rating']) { ?>
										<div class="rating">
											<?php for ($i = 1; $i <= 5; $i++) { ?>
												<?php if ($product['rating'] < $i) { ?>
													<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
												<?php } else { ?>
													<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<?php } ?>
											<?php } ?>
										</div>
									<?php } ?>
									<?php if ($product['price']) { ?>
										<p class="price">
											<?php if (!$product['special']) { ?>
												<?php echo $product['price']; ?>
											<?php } else { ?>
												<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
											<?php } ?>
											<?php if ($product['tax']) { ?>
												<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
											<?php } ?>
										</p>
									<?php } ?>
								</div>
								<div class="button-group">
									<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
									<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
									<button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } else { ?>
					<p><?php echo $ms_catalog_seller_products_empty; ?></p>
				<?php } ?>
			</div>
			<div class="row">
				<div class="col-sm-6 text-left" id="mspf_pagination"><?php echo $pagination; ?></div>
				<div class="col-sm-6 text-right" id="mspf_products_count"><?php echo $results; ?></div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
<?php echo $footer; ?>