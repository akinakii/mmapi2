<?php foreach ($msf_attributes as $msf_attribute) { ?>
	<div class="form-group">
		<input type="hidden" name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][required]" value="<?php echo $msf_attribute['required']; ?>" />

		<label class="mm_label col-sm-2 <?php if ($msf_attribute['required']) { ?>mm_req<?php } ?>"><?php echo $msf_attribute['name']; ?></label>

		<div class="col-sm-10">
			<?php if ('select' === $msf_attribute['type']) { ?>
				<select name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][id_values][]" class="form-control">
					<?php foreach ($msf_attribute['values'] as $value) { ?>
						<option value="<?php echo $value['id']; ?>" <?php if (!empty($product_msf_attributes[$msf_attribute['id']]['id_values']) && in_array($value['id'], array_keys($product_msf_attributes[$msf_attribute['id']]['id_values']))) { ?>selected="selected"<?php } ?>><?php echo $value['name']; ?></option>
					<?php } ?>
				</select>
			<?php } elseif ('checkbox' === $msf_attribute['type']) { ?>
				<div class="well well-sm" style="height: 150px; overflow: auto;">
					<?php foreach ($msf_attribute['values'] as $value) { ?>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][id_values][]" value="<?php echo $value['id']; ?>" <?php if (!empty($product_msf_attributes[$msf_attribute['id']]['id_values']) && in_array($value['id'], array_keys($product_msf_attributes[$msf_attribute['id']]['id_values']))) { ?>checked="checked"<?php } ?> />
								<?php echo $value['name'] ?: ''; ?>
							</label>
						</div>
					<?php } ?>
				</div>
			<?php } elseif ('text' === $msf_attribute['type']) { ?>
				<?php foreach ($languages as $language) { ?>
					<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
					<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
						<div class="lang-img-icon-input-text"><img src="<?php echo $img; ?>"></div>
					</div>
					<input type="text" name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][text_values][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($product_msf_attributes[$msf_attribute['id']]['text_values'][$language['language_id']]) ? $product_msf_attributes[$msf_attribute['id']]['text_values'][$language['language_id']] : ''; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
				<?php } ?>
			<?php } ?>

			<?php foreach ($languages as $language) { ?>
				<?php if(!empty($msf_attribute['description'][$language['language_id']]['note'])) { ?>
					<p class="ms-note lang-select-field" data-lang="<?php echo $language['code']; ?>"><?php echo $msf_attribute['description'][$language['language_id']]['note']; ?></p>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
<?php } ?>