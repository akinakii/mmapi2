<ul class="nav nav-tabs">
    <li class="active">
        <a data-toggle="tab" href="#seller_tab<?php echo $order_id ?>"><?php echo $ms_order_details ?></a>
    </li>
    <?php if (MsLoader::getInstance()->MsHelper->isInstalled() && isset($seller_histories) && $seller_histories) { ?>
        <?php foreach ($seller_histories as $history) { ?>
            <li>
                <a href="#seller_tab<?php echo $history['seller_id']; ?>" data-toggle="tab">
                    <?php echo $history['participant'], '(#', $order_id, '-', $history['suborder_id'], ')'; ?>
                </a>
            </li>
        <?php } ?>
    <?php } ?>
</ul>

<div class="tab-content">
    <div class="active tab-pane" id="seller_tab<?php echo $order_id ?>">
        <h3><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $ms_order_column_order_id, $order_id; ?></h3>
        <table class="table">
            <tbody>
                <tr>
                    <td class="text-left" style="width: 50%;">
                        <?php if ($invoice_no) { ?>
		                    <?php echo $text_invoice_no; ?><b><?php echo $invoice_no; ?></b><br />
                        <?php } ?>
                        <?php echo $ms_order_placed; ?>&nbsp;<b><?php echo $date_added; ?></b><br/>
	                    <?php if ($payment_method) { ?>
		                    <?php echo $ms_payment_method, ':'; ?>&nbsp;<b><?php echo $payment_method; ?></b><br />
	                    <?php } ?>
                        <?php if (MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
                            <?php echo $this->language->get('ms_payment_status'), ':'; ?>
                            <b><?php echo $this->MsLoader->MsHelper->getStatusName(array('order_status_id' => $order_status_id)); ?></b><br>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>

	    <?php if ($shipping_address && $shipping_address == $payment_address) { ?>
            <h3><i class="fa fa-map-marker"></i>&nbsp;<?php echo $this->language->get('ms_account_orders_addresses'); ?></h3>
	    <?php } else { ?>
            <h3><i class="fa fa-map-marker"></i>&nbsp;<?php echo $text_payment_address ?></h3>
	    <?php } ?>
        <table class="table">
            <tbody>
            <tr>
                <td class="text-left"><?php echo $payment_address; ?></td>
            </tr>
            </tbody>
        </table>

	    <?php if ($shipping_address && $shipping_address != $payment_address) { ?>
            <h3><i class="fa fa-map-marker"></i>&nbsp;<?php echo $text_shipping_address ?></h3>
            <table class="table">
                <tbody>
                <tr>
                    <td class="text-left"><?php echo $shipping_address; ?></td>
                </tr>
                </tbody>
            </table>
	    <?php } ?>

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <td></td>
                        <td class="text-left"><b><?php echo $ms_account_products_product; ?></b></td>
                        <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
                            <?php if ($suborders_flag) { ?>
                                <td><b><?php echo MsLoader::getInstance()->getRegistry()->get('language')->get('ms_order_sold_by') ?></b></td>
                            <?php } ?>
                            <td><b><?php echo MsLoader::getInstance()->getRegistry()->get('language')->get('ms_status'); ?></b></td>
                        <?php } ?>
                        <td class="text-right"><b><?php echo $ms_account_product_unit_price; ?></b></td>
                        <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
                            <?php if ($mm_shipping_flag) { ?>
                                <td class="text-right">
                                    <b><?php echo MsLoader::getInstance()->getRegistry()->get('language')->get('mm_account_order_shipping_cost'); ?></b>
                                </td>
                            <?php } ?>
                        <?php } ?>
                        <td class="text-right"><b><?php echo $column_total; ?></b></td>
                    </tr>
                </thead>
                <tbody>
			    <?php foreach ($products as $product) { ?>
                    <tr>
                        <td>
                            <img src="<?php echo $product['image'] ?>" alt="<?php addslashes($product['name']) ?>">
                        </td>
                        <td class="text-left">
                            <a href="<?php echo $this->url->link('product/product', 'product_id=' . $product['product_id']);?>">
	                            <?php echo $product['name'], $product['quantity'] > 1 ? " x {$product['quantity']}" : ''; ?>
                            </a>
						    <?php if (!empty($product['msf_variation'])) { ?>
							    <?php foreach ($product['msf_variation'] as $msf_variation) { ?>
                                    <br />
                                    <small><?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
							    <?php } ?>
						    <?php } ?>
						    <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
						    <?php } ?>
                        </td>
					    <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
	                        <?php if ($suborders_flag) { ?>
                                <?php
							    MsLoader::getInstance()->getRegistry()->get('load')->language('multiseller/multiseller');
							    $seller = MsLoader::getInstance()->MsSeller->getSeller(MsLoader::getInstance()->MsProduct->getSellerId($product['product_id']));
                                ?>
                                <td>
                                    <?php  if ($seller) { ?>
                                        <a href="<?php echo $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id']) ?>"><?php echo $seller['ms.nickname'] ?></a>
                                    <?php } ?>
                                </td>
                            <?php } ?>
                            <td><?php echo $product['suborder_status_text']; ?></td>
					    <?php } ?>
                        <td class="text-right"><?php echo $product['price']; ?></td>
					    <?php if (MsLoader::getInstance()->MsHelper->isInstalled() && $mm_shipping_flag > 0) { ?>
                            <td class="text-right"><?php echo $product['shipping_cost_formatted']; ?></td>
					    <?php } ?>
                        <td class="text-right"><?php echo $product['total']; ?></td>
                    </tr>
			    <?php } ?>
			    <?php foreach ($vouchers as $voucher) { ?>
                    <tr>
                        <td class="text-left"><?php echo $voucher['description']; ?></td>
                        <td class="text-left"></td>
                        <td class="text-right">1</td>
                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                    </tr>
			    <?php } ?>
                </tbody>
                <tfoot>
			    <?php foreach ($totals as $total) { ?>
                    <tr>
					    <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
                            <td colspan="<?php echo $mm_shipping_flag ? '5' : '4'; ?>"></td>
					    <?php } else { ?>
                            <td colspan="3"></td>
					    <?php } ?>
                        <td class="text-right"><?php echo $total['title']; ?></td>
                        <td class="text-right"><?php echo $total['text']; ?></td>
                    </tr>
			    <?php } ?>
                </tfoot>
            </table>
	        <?php if ($comment) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <td class="text-left"><?php echo $text_comment; ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-left"><?php echo $comment; ?></td>
                    </tr>
                    </tbody>
                </table>
	        <?php } ?>
	        <?php if ($histories) { ?>
                <h3><i class="fa fa-credit-card"></i>&nbsp;
			        <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
				        <?php echo $this->language->get('ms_payment_history'); ?>
			        <?php } else { ?>
				        <?php echo $text_history; ?>
			        <?php } ?></h3>
                <table class="table">
                    <thead>
                    <tr>
                        <td class="text-left"><b><?php echo $column_date_added; ?></b></td>
                        <td class="text-left"><b><?php echo $column_comment; ?></b></td>
                        <td class="text-left"><b><?php echo $column_status; ?></b></td>
                    </tr>
                    </thead>
                    <tbody>
			        <?php if ($histories) { ?>
				        <?php foreach ($histories as $history) { ?>
                            <tr>
                                <td class="text-left"><?php echo $history['date_added']; ?></td>
                                <td class="text-left"><?php echo $history['comment']; ?></td>
                                <td class="text-left"><?php echo $history['status']; ?></td>
                            </tr>
				        <?php } ?>
			        <?php } else { ?>
                        <tr>
                            <td colspan="3" class="text-center"><?php echo $text_no_results; ?></td>
                        </tr>
			        <?php } ?>
                    </tbody>
                </table>
	        <?php } ?>
        </div>
    </div>
    <?php if (MsLoader::getInstance()->MsHelper->isInstalled() && isset($seller_histories) && $seller_histories) { ?>
        <?php foreach ($seller_histories as $history) { ?>
            <?php if(!empty($history['entries']) || $this->config->get('mmess_conf_enable')) { ?>
                <div class="tab-pane" id="seller_tab<?php echo $history['seller_id']; ?>">
                    <h3><i class="fa fa-shopping-cart"></i>&nbsp;<?php echo $ms_order_column_order_id, $order_id, '-', $history['suborder_id']; ?></h3>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="text-left" style="width: 50%;">
				                <?php echo $this->language->get('ms_order_products_by'); ?>&nbsp;
                                <a href="<?php echo $this->url->link('seller/catalog-seller/products', 'seller_id=' . $history['seller_id']); ?>">
                                    <?php echo $history['participant']; ?>
                                </a><br/>
				                <?php echo $this->language->get('ms_order_status'), ':'; ?>&nbsp;<b><?php echo $this->MsLoader->MsSuborderStatus->getSubStatusName(array('order_status_id' => $history['suborder_status'])); ?></b><br/>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table">
                        <thead>
                        <tr>
                            <td></td>
                            <td class="text-left"><b><?php echo $ms_account_products_product; ?></b></td>
                            <td class="text-right"><b><?php echo $ms_account_product_unit_price; ?></b></td>
			                <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
				                <?php if ($mm_shipping_flag) { ?>
                                    <td class="text-right"><b><?php echo MsLoader::getInstance()->getRegistry()->get('language')->get('mm_account_order_shipping_cost'); ?></b></td>
				                <?php } ?>
			                <?php } ?>
                            <td class="text-right"><b><?php echo $column_total; ?></b></td>
                        </tr>
                        </thead>
                        <tbody>
		                <?php foreach ($products as $product) { ?>
			                <?php if ($product['seller_id'] != $history['seller_id']) continue; ?>
                            <tr>
                                <td>
                                    <img src="<?php echo $product['image'] ?>" alt="<?php addslashes($product['name']) ?>">
                                </td>
                                <td class="text-left">
                                    <a href="<?php echo $this->url->link('product/product', 'product_id=' . $product['product_id']); ?>">
	                                    <?php echo $product['name'], $product['quantity'] > 1 ? " x {$product['quantity']}" : ''; ?>
                                    </a>
					                <?php if (!empty($product['msf_variation'])) { ?>
						                <?php foreach ($product['msf_variation'] as $msf_variation) { ?>
                                            <br />
                                            <small><?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
						                <?php } ?>
					                <?php } ?>
					                <?php foreach ($product['option'] as $option) { ?>
                                        <br />
                                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
					                <?php } ?>
                                </td>
                                <td class="text-right"><?php echo $product['price']; ?></td>
				                <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
					                <?php if ($mm_shipping_flag) { ?>
                                        <td class="text-right"><?php echo $product['shipping_cost_formatted']; ?></td>
					                <?php } ?>
				                <?php } ?>
                                <td class="text-right"><?php echo $product['total']; ?></td>
                            </tr>
		                <?php } ?>
                        </tbody>
                        <tfoot>
		                <?php foreach ($history['totals'] as $total) { ?>
                            <tr>
				                <?php if(MsLoader::getInstance()->MsHelper->isInstalled()) { ?>
                                    <td colspan="<?php echo $mm_shipping_flag ? '3' : '2'; ?>"></td>
				                <?php } else { ?>
                                    <td colspan="3"></td>
				                <?php } ?>
                                <td class="text-right"><?php echo $total['title']; ?></td>
                                <td class="text-right"><?php echo $total['text']; ?></td>
                            </tr>
		                <?php } ?>
                        </tfoot>
                    </table>

                    <?php if(!empty($history['entries'])) { ?>
                        <h3><i class="fa fa-truck"></i>&nbsp;<?php echo $this->language->get('ms_account_order_history'); ?></h3>
                        <table class="table list">
                            <thead>
                                <tr>
                                    <td class="text-left col-sm-3"><b><?php echo $this->language->get('ms_date_modified'); ?></b></td>
                                    <td class="text-left col-sm-6"><b><?php echo $column_comment; ?></b></td>
                                    <td class="text-left col-sm-3"><b><?php echo $column_status; ?></b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($history['entries'] as $he) { ?>
                                    <tr>
                                        <td class="text-left col-sm-3"><?php echo $he['date_added']; ?></td>
                                        <td class="text-left col-sm-6"><?php echo nl2br($he['comment']); ?></td>
                                        <td class="text-left col-sm-3"><?php echo $he['status']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>

                    <?php if ($this->config->get('mmess_conf_enable') && $this->MsLoader->MsSeller->isCustomerSeller($history['seller_id'])) { ?>
                        <a href="javascript:;" onclick="$('#ms-account-conversation-<?php echo $history['seller_id'] ?>').toggle()"><?php echo !empty($history['order_messages']) ? $history['conversation_title'] : sprintf($ms_account_conversations_start_with_seller, $history['seller_nickname']); ?></a>

                        <div class="ms-account-conversation" id="ms-account-conversation-<?php echo $history['seller_id'] ?>" style="display: none">
                            <div class="ms-messages">
                                <?php foreach ($history['order_messages'] as $message) { ?>
                                    <div class="row ms-message <?php echo $message['sender_type_id'] == MsConversation::SENDER_TYPE_ADMIN ? 'admin' : ($message['sender_type_id'] == MsConversation::SENDER_TYPE_SELLER ? 'seller' : ''); ?>">
                                        <div class="col-sm-12 ms-message-body">
                                            <div class="title">
                                                <?php echo ucwords($message['sender']); ?>
                                                <span class="date"><?php echo $message['date_created']; ?></span>
                                            </div>

                                            <div class="body">
                                                <?php echo nl2br($message['message']); ?>
                                            </div>

                                            <?php if(!empty($message['attachments'])) { ?>
                                                <div class="attachments">
                                                    <?php foreach($message['attachments'] as $attachment) { ?>
                                                        <a href="<?php echo $this->url->link('account/msconversation/downloadAttachment', 'code=' . $attachment['code'], true); ?>"><i class="fa fa-file-o" aria-hidden="true"></i> <?php echo $attachment['name']; ?></a>
                                                        <br/>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="row ms-message-form">
                                <form id="ms-message-form<?php echo $history['suborder_id']; ?>" class="ms-form form-horizontal">
                                    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
                                    <input type="hidden" name="suborder_id" value="<?php echo $history['suborder_id']; ?>" />
                                    <input type="hidden" name="seller_id" value="<?php echo $history['seller_id']; ?>" />

                                    <div class="col-sm-9 col-md-9 col-lg-10">
                                        <textarea class="form-control ms-message-text" rows="5" cols="50" name="ms-message-text" placeholder="<?php echo $ms_account_conversations_textarea_placeholder; ?>"></textarea>
                                        <div class="list">
                                            <ul class="attachments"></ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                        <div class="buttons text-center">
                                            <button type="button" class="btn btn-default ms-message-upload" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <button data-suborder_id="<?php echo $history['suborder_id']; ?>" type="button" class="btn btn-primary ms-order-message"><?php echo $ms_post_message; ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>