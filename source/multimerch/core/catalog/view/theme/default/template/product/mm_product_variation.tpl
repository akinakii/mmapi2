<div class="row">
	<div class="col-sm-12">
		<?php foreach ($available['all'] as $msf_variation_id => $data) { ?>
			<div class="form-group required">
				<label class="control-label" for="input-msf-variation-<?php echo $msf_variation_id; ?>"><?php echo $data['variation']['name']; ?></label>
				<select name="msf_variation[<?php echo $msf_variation_id; ?>]" data-msf_variation_id="<?php echo $msf_variation_id; ?>" id="input-msf-variation-<?php echo $msf_variation_id; ?>" class="form-control">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($data['values'] as $value) { ?>
						<option value="<?php echo $value['id']; ?>"
							<?php echo !empty($available['filtered'][$msf_variation_id]['values']['disabled_ids']) && in_array($value['id'], $available['filtered'][$msf_variation_id]['values']['disabled_ids']) ? 'style="color: rgba(0,0,0,0.5);"' : ''; ?>
							<?php echo !empty($available['filtered'][$msf_variation_id]['values']['hidden_ids']) && in_array($value['id'], $available['filtered'][$msf_variation_id]['values']['hidden_ids']) ? 'style="display: none;"' : ''; ?>
							<?php echo !empty($selected[$msf_variation_id]['id']) && (int)$value['id'] === (int)$selected[$msf_variation_id]['id'] ? 'selected' : ''; ?>
						><?php echo $value['name']; ?></option>
					<?php } ?>
				</select>
			</div>
		<?php } ?>
	</div>
</div>