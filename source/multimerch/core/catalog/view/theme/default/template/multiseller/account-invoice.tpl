<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<?php if (isset($success) && $success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
	<?php } ?>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
			<div class="mm_dashboard">
				<h1>
					<i class="fa fa-usd"></i><?php echo $ms_invoice_heading; ?>
					<div class="pull-right">
						<button style="display: none;" id="ms-pay" type="submit" form="ms-invoices" data-toggle="tooltip" title="<?php echo $ms_button_pay; ?>" class="btn btn-primary"><i class="fa fa-money" style="font-size: inherit; padding: 0; overflow: initial"></i></button>
					</div>
				</h1>

				<div class="error"></div>

				<!-- PAYMENT REQUESTS -->
				<div class="table-responsive">
					<form class="form-inline" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"  id="ms-invoices">
						<table class="mm_dashboard_table table table-borderless table-hover" style="text-align: center" id="list-invoices">
							<thead>
								<tr>
									<td width="25" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', $(this).prop('checked'))"></td>
									<td class="mm_size_tiny"><?php echo $ms_invoice_column_invoice_id; ?></td>
									<td class="medium"><?php echo $ms_invoice_column_type; ?></td>
									<td class="large"><?php echo $ms_invoice_column_title; ?></td>
									<td class="small"><?php echo $ms_invoice_column_status; ?></td>
									<td class="small"><?php echo $ms_invoice_column_payment_info; ?></td>
									<td class="small"><?php echo $ms_invoice_column_date_generated; ?></td>
									<td class="small"><?php echo $ms_invoice_column_total; ?></td>
								</tr>

								<tr class="filter">
									<td></td>
									<td><input type="text" /></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td><input type="text" class="input-date-datepicker"/></td>
									<td></td>
								</tr>
							</thead>

							<tbody></tbody>
						</table>
					</form>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>

	<script>
		$(function() {
			$('#list-invoices').dataTable( {
				"sAjaxSource": $('base').attr('href') + "index.php?route=seller/account-invoice/getTableData",
				"aoColumns": [
					{ "mData": "checkbox", "bSortable": false },
					{ "mData": "invoice_id" },
					{ "mData": "type" },
					{ "mData": "title", "bSortable": false },
					{ "mData": "status" },
					{ "mData": "payment", "bSortable": false },
					{ "mData": "date_generated" },
					{ "mData": "total" }
				],
				"aaSorting":  [[6,'desc']]
			});

			$(document).on('click', '#list-invoices input:checkbox', function() {
				var selected_invoices = $('#list-invoices').children('tbody').find('input:checkbox:checked');
				if(selected_invoices.length > 0) {
					$('#ms-pay').show();
				} else {
					$('#ms-pay').hide();
				}
			});

			$(document).on('click', '#ms-pay', function(e) {
				e.preventDefault();

				if ($("#ms-invoices tbody input:checkbox:checked").length == 0) {
					var html = '';
					html += '<div class="cl"></div>';
					html += '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ';
					html += '<?php echo htmlspecialchars($ms_invoice_error_not_selected, ENT_QUOTES, "UTF-8"); ?>';
					html += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					html += '</div>';

					$(".error").html(html);
				} else {
					$(".error").html('');
					$("#ms-invoices").submit();
				}
			});
		});
	</script>
</div>
<?php echo $footer; ?>