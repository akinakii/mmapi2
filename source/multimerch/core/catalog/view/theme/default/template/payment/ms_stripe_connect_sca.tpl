<div class="text-center" id="stripe-checkout-holder">
	<button id="stripe-checkout-btn" class="stripe-button"><?php echo $text_checkout_confirm; ?></button>
</div>

<script>
	var stripe = Stripe('<?php echo $stripe_pk; ?>');

	$('#stripe-checkout-btn').click(function () {
		stripe.redirectToCheckout({
			sessionId: '<?php echo $stripe_checkout_session_id; ?>'
		}).then(function (result) {
			$('#stripe-checkout-btn').before('<div class="alert alert-danger">' + result.error.message + '</div>');
			// If `redirectToCheckout` fails due to a browser or network
			// error, display the localized error message to your customer
			// using `result.error.message`.
		});
	})
</script>

<style>
	#stripe-checkout-btn {
		overflow: hidden;
		display: inline-block;
		visibility: visible !important;
		background-image: -webkit-linear-gradient(#28a0e5,#015e94);
		background-image: -moz-linear-gradient(#28a0e5,#015e94);
		background-image: -ms-linear-gradient(#28a0e5,#015e94);
		background-image: -o-linear-gradient(#28a0e5,#015e94);
		background-image: -webkit-linear-gradient(#28a0e5,#015e94);
		background-image: -moz-linear-gradient(#28a0e5,#015e94);
		background-image: -ms-linear-gradient(#28a0e5,#015e94);
		background-image: -o-linear-gradient(#28a0e5,#015e94);
		background-image: linear-gradient(#28a0e5,#015e94);
		-webkit-font-smoothing: antialiased;
		border: 0;
		text-decoration: none;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		-ms-border-radius: 5px;
		-o-border-radius: 5px;
		border-radius: 5px;
		-webkit-box-shadow: 0 1px 0 rgba(0,0,0,0.2);
		-moz-box-shadow: 0 1px 0 rgba(0,0,0,0.2);
		-ms-box-shadow: 0 1px 0 rgba(0,0,0,0.2);
		-o-box-shadow: 0 1px 0 rgba(0,0,0,0.2);
		box-shadow: 0 1px 0 rgba(0,0,0,0.2);
		-webkit-touch-callout: none;
		-webkit-tap-highlight-color: transparent;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		-o-user-select: none;
		user-select: none;
		cursor: pointer;
		color: white;
		padding: 15px 100px;
		font-size: 16px;
	}
</style>