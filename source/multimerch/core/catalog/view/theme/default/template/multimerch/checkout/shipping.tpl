<div id="ms-shipping-checkout">
	<div class="shipping-address">
		<strong><?php echo $mm_checkout_shipping_delivery_details_title; ?>:</strong>
		<?php echo $shipping['address']['inline']; ?>
		<span><a href="#collapse-shipping-address" class="bottom-dotted-link" data-toggle="collapse" data-parent="#accordion"><?php echo $mm_checkout_shipping_delivery_details_change; ?></a></span>
	</div>

	<div class="alert alert-danger ms-no-background <?php echo empty($shipping['errors']) ? 'hidden' : ''; ?>" id="shipping-error-holder">
		<?php if (!empty($shipping['errors'])) { ?>
			<p class="ms-error"><?php echo $ms_shipping_checkout_error_general; ?></p>
		<?php } ?>
	</div>
	
	<div class="alert alert-info ms-no-background <?php echo !empty($shipping['errors']) ? 'hidden' : ''; ?>" id="shipping-info-holder">
		<?php echo $shipping['contains_digital'] ? $ms_shipping_checkout_info_title_with_digital : $ms_shipping_checkout_info_title; ?>
	</div>

	

	<div class="carts">
		<?php foreach ($shipping['carts'] as $seller_id => $products) { ?>
			<div class="<?php echo !empty($products['digital']) && empty($products['individual']) && empty($products['combined']['products']) && empty($products['unavailable']) ? 'hidden' : ''; ?>">
				<table class="table">
					<thead>
						<tr>
							<td colspan="3">
								<?php if ($seller_id) { ?>
									<h4><?php echo sprintf($ms_shipping_checkout_title_seller_table, $this->MsLoader->MsSeller->getSellerNickname($seller_id)); ?>:</h4>
								<?php } else { ?>
									<h4><?php echo $ms_shipping_checkout_title_marketplace_table; ?>:</h4>
								<?php } ?>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($products['individual'] as $product) { ?>
							<tr class="individual <?php echo !empty($product['options']) || !empty($product['msf_variation']) ? 'with-options' : ''; ?>">
								<td class="col-sm-1 image"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
								<td class="col-sm-4 product-info">
									<div>
										<strong><?php echo $product['name']; ?></strong>
									</div>

									<?php if (!empty($product['msf_variation'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['msf_variation'] as $msf_variation) { ?>
													<li><?php echo $msf_variation['name'] . ': ' . $msf_variation['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } elseif (!empty($product['options'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['options'] as $option) { ?>
													<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } ?>

									<div>
										<span><?php echo $mm_checkout_shipping_products_price; ?></span>
										<?php echo $product['price_formatted']; ?>
									</div>

									<div>
										<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
										<span><?php echo $product['quantity']; ?></span>
									</div>
								</td>
								<td class="col-sm-7 shipping-methods">
									<strong><?php echo $ms_shipping_checkout_title_select_option; ?>:</strong>

									<?php foreach($product['shipping_methods'] as $rule_id => $shipping_method) { ?>
										<div class="radio">
											<label>
												<input type="radio" name="shipping[<?php echo $seller_id; ?>][individual][<?php echo $product['cart_id']; ?>]" value="<?php echo $rule_id; ?>" data-cost="<?php echo $shipping_method['cost']; ?>" <?php echo $shipping_method === reset($product['shipping_methods']) ? 'checked="checked"' : ''; ?> />
												<?php echo $shipping_method['name'] ?: ''; ?> <?php echo $shipping_method['delivery_time'] ? "({$shipping_method['delivery_time']})" : ""; ?><?php echo $shipping_method['name'] || $shipping_method['delivery_time'] ? ':' : ''; ?>
												<span class="inline cost"><?php echo $shipping_method['cost_formatted']; ?></span>
											</label>
										</div>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>

						<?php foreach ($products['combined']['products'] as $product) { ?>
							<tr class="combined <?php echo !empty($product['options']) || !empty($product['msf_variation']) ? 'with-options' : ''; ?>">
								<td class="col-sm-1 image"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
								<td class="col-sm-4 product-info">
									<div>
										<strong><?php echo $product['name']; ?></strong>
									</div>

									<?php if (!empty($product['msf_variation'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['msf_variation'] as $msf_variation) { ?>
												<li><?php echo $msf_variation['name'] . ': ' . $msf_variation['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } elseif (!empty($product['options'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['options'] as $option) { ?>
													<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } ?>

									<div>
										<span><?php echo $mm_checkout_shipping_products_price; ?></span>
										<?php echo $product['price_formatted']; ?>
									</div>

									<div>
										<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
										<span><?php echo $product['quantity']; ?></span>
									</div>

									<?php if (!empty($products['combined']['errors'])) { ?>
										<div>
											<a href="#" class="ms-remove-link" data-cart-id="<?php echo $product['cart_id']; ?>"><?php echo $ms_shipping_checkout_remove_product; ?></a>
										</div>
									<?php } ?>
								</td>

								<?php if ($product === reset($products['combined']['products'])) { ?>
									<td class="col-sm-7 shipping-methods" rowspan="<?php echo count($products['combined']['products']); ?>">

										<?php if (!empty($products['combined']['shipping_methods'])) { ?>
											<strong><?php echo $ms_shipping_checkout_title_select_option; ?>:</strong>

											<?php foreach($products['combined']['shipping_methods'] as $rule_id => $shipping_method) { ?>
												<div class="radio">
													<label>
														<input type="radio" name="shipping[<?php echo $seller_id; ?>][combined][<?php echo (int)$this->config->get('msconf_shipping_type'); ?>]" value="<?php echo $rule_id; ?>" data-cost="<?php echo $shipping_method['cost']; ?>" <?php echo $shipping_method === reset($products['combined']['shipping_methods']) ? 'checked="checked"' : ''; ?> />
														<?php echo $shipping_method['name'] ?: ''; ?> <?php echo $shipping_method['delivery_time'] ? "({$shipping_method['delivery_time']})" : ""; ?><?php echo $shipping_method['name'] || $shipping_method['delivery_time'] ? ':' : ''; ?>
														<span class="inline cost"><?php echo $shipping_method['cost_formatted']; ?></span>
													</label>
												</div>
											<?php } ?>
										<?php } elseif (!empty($products['combined']['errors'])) { ?>
											<?php foreach ($products['combined']['errors'] as $error) { ?>
												<p class="ms-error"><?php echo $error; ?></p>
											<?php } ?>
										<?php } ?>
									</td>
								<?php } ?>
							</tr>
						<?php } ?>

						<?php foreach ($products['unavailable'] as $product) { ?>
							<tr class="unavailable <?php echo !empty($product['options']) || !empty($product['msf_variation']) ? 'with-options' : ''; ?>">
								<td class="col-sm-1 image"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
								<td class="col-sm-4 product-info">
									<div>
										<strong><?php echo $product['name']; ?></strong>
									</div>

									<?php if (!empty($product['msf_variation'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['msf_variation'] as $msf_variation) { ?>
												<li><?php echo $msf_variation['name'] . ': ' . $msf_variation['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } elseif (!empty($product['options'])) { ?>
										<div>
											<ul class="options">
												<?php foreach($product['options'] as $option) { ?>
													<li><?php echo $option['name'] . ': ' . $option['value']; ?></li>
												<?php } ?>
											</ul>
										</div>
									<?php } ?>

									<div>
										<span><?php echo $mm_checkout_shipping_products_price; ?></span>
										<?php echo $product['price_formatted']; ?>
									</div>

									<div>
										<span><?php echo $mm_checkout_shipping_products_quantity; ?></span>
										<span><?php echo $product['quantity']; ?></span>
									</div>

									<div>
										<a href="#" class="ms-remove-link" data-cart-id="<?php echo $product['cart_id']; ?>"><?php echo $ms_shipping_checkout_remove_product; ?></a>
									</div>
								</td>
								<td class="col-sm-7 shipping-methods">
									<p class="ms-error"><?php echo $ms_shipping_checkout_error_individual_general; ?></p>
								</td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
						<?php if ($products === end($shipping['carts'])) { ?>
							<tr class="total">
								<td></td>
								<td></td>
								<td>
									<strong class="shipping-total-holder">
										<?php echo $ms_shipping_checkout_title_shipping_total; ?>:
										<?php echo $this->currency->getSymbolLeft($this->config->get('config_currency')); ?><span class="shipping-total"></span><?php echo $this->currency->getSymbolRight($this->config->get('config_currency')); ?>
									</strong>

									<?php if (empty($shipping['errors'])) { ?>
										<button class="btn btn-primary" id="ms-shipping-submit" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_continue; ?></button>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tfoot>
				</table>
			</div>
		<?php } ?>
	</div>
</div>

<script>
	$(function () {
		updateTotalShippingCost();
		$(document).on('click', '.carts input[type="radio"]', updateTotalShippingCost);

		function updateTotalShippingCost() {
			var shipping_total = 0;

			var selected_options = $('.carts').find('input[type="radio"]:checked');
			$(selected_options).each(function () {
				shipping_total += parseFloat($(this).attr('data-cost'));
			});

			$('.shipping-total').html(shipping_total.toFixed(2));
		}

		// Update markup for mobile devices
		if ($(window).width() <= 425) {
			// Move td with shipping methods
			$.map($('.carts .table tbody'), function (table) {
				// Get shipping methods td
				var shipping_methods_td = $(table).find('tr.combined:first > .shipping-methods').html();

				// Create new row after combined products and put methods in it
				$(table).find('tr.combined:last').after('<tr class="combined"><td style="text-align: left;">' + shipping_methods_td + '</td></tr>');

				// Remove methods from first row
				$(table).find('tr.combined:first > .shipping-methods').remove();
			});
		}

		$(document).on('click', '.ms-remove-link', function (e) {
			e.preventDefault();

			$.post('index.php?route=multimerch/checkout/shipping/jxRemoveProduct', { cart_id: $(this).attr('data-cart-id') }, function (json) {
				if (json.error) {
					alert(json.error);
				} else if (json.success) {
					$('#collapse-shipping-method .panel-body').load('index.php?route=multimerch/checkout/shipping');
				}
			})
		});

		$('#ms-shipping-submit').click(function (e) {
			e.preventDefault();

			var $button = $(this);

			$.ajax({
				url: 'index.php?route=multimerch/checkout/shipping/jxSave',
				type: 'POST',
				data: $('#ms-shipping-checkout .carts input[type="radio"]:checked'),
				dataType: 'json',
				beforeSend: function () {
					$button.button('loading');
					$('#shipping-error-holder').empty().addClass('hidden');
				},
				success: function (json) {
					$button.button('reset');

					if (json.errors) {
						$.each(json.errors, function (key, error) {
							$('#shipping-error-holder').append('<p class="ms-error">' + error + '</p>').removeClass('hidden');
						});

						$('html,body').animate({
							scrollTop: $('#collapse-shipping-method').offset().top
						}, 'slow');
					} else if (json.success) {
						$.ajax({
							url: 'index.php?route=checkout/payment_method',
							dataType: 'html',
							complete: function() {
								$('#button-shipping-method').button('reset');
							},
							success: function(html) {
								$('#collapse-payment-method .panel-body').html(html);
								$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');
								$('a[href=\'#collapse-payment-method\']').trigger('click');
								$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								console.error(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								alert("Error: " + xhr.responseText);
							}
						});
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	});
</script>
