<div class="col-sm-12 ">
	<div class="row">
		<div class="col-sm-12 step_header">
			<div class="col-md-4"></div>
			<div class="col-md-4 text-center step_header_text"><?php echo $ms_feed_step_mapping_title; ?></div>
			<div class="col-md-4 text-right step_header_steps"><?php echo $ms_feed_step_mapping_seq_number;?></div>
		</div>
		<div class="col-md-12 table-responsive">
			<form class="form-horizontal" id="import_fields">
                <table class="table">
                    <thead>
                    <tr>
                        <th><?php echo $ms_feed_column_csv_caption; ?></th>
                        <th><?php echo $ms_feed_column_product_property; ?></th>
                        <th><?php echo $ms_feed_column_preview_information; ?></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach ($field_captions as $caption => $caption_id) { ?>
                        <tr>
                            <td><?php echo $caption; ?></td>
                            <td>
								<?php if ($mapping) { ?>
                                    <select class="form-control property_selector">
                                        <option value="">-</option>
                                        <?php if ($oc_fields) { ?>
                                            <optgroup label="<?php echo $this->language->get('ms_seller_fields') ?>">
                                            <?php foreach ($oc_fields as $type => $field_data) { ?>
                                                <option <?php if (isset($mapping[$caption_id]) && $mapping[$caption_id] == $type) { ?>selected <?php } ?> value="<?php echo $type; ?>"><?php echo $field_data['field_name']; ?></option>
                                            <?php } ?>
                                            </optgroup>
                                        <?php } ?>
                                        <?php if ($attributes) { ?>
                                            <optgroup label="<?php echo $this->language->get('ms_seller_attributes') ?>">
		                                        <?php foreach ($attributes as $attribute) { ?>
                                                    <option <?php if (isset($mapping[$caption_id]) && $mapping[$caption_id] == $attribute['id']) { ?>selected <?php } ?> value="<?php echo $attribute['id'] ?>"><?php echo $attribute['name'] ?></option>
		                                        <?php } ?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
								<?php } else { ?>
                                    <select class="form-control property_selector">
                                        <option value="">-</option>
	                                    <?php if ($oc_fields) { ?>
                                            <optgroup label="<?php echo $this->language->get('ms_seller_fields') ?>">
                                                <?php foreach ($oc_fields as $type => $field_data) { ?>
                                                    <option <?php if ($caption == $field_data['csv_name']) { ?>selected <?php } ?> value="<?php echo $type; ?>"><?php echo $field_data['field_name']; ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        <?php } ?>
                                        <?php if ($attributes) { ?>
                                            <optgroup label="<?php echo $this->language->get('ms_seller_attributes') ?>">
                                                <?php foreach ($attributes as $attribute) { ?>
                                                    <option value="<?php echo $attribute['id'] ?>"><?php echo $attribute['name'] ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
								<?php } ?>
                            </td>
                            <td class="preview_information" style="width: 50%;">
								<?php if (isset($mapping[$caption_id]) && isset($samples[$mapping[$caption_id]])) { ?>
									<?php echo $samples[$mapping[$caption_id]]; ?>
								<?php } elseif (isset($samples[$caption_id])) { ?>
									<?php echo $samples[$caption_id]; ?>
								<?php } ?>
                            </td>
                        </tr>
					<?php } ?>
                    </tbody>
                </table>
			</form>
		</div>
		<div class="col-sm-12 step_footer">
			<div class="pull-right">
				<button id="proceed_to_step_confirm" class="btn btn-primary"><?php echo $ms_feed_to_step_confirm; ?></button>
			</div>
			<button class="btn btn-default prev_step" data-next_id="1"><?php echo $ms_button_back; ?></button>
		</div>
	</div>
</div>