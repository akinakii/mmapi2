<?php foreach ($menus as $menu) { ?>
	<?php if (empty($menu['items'])) continue; ?>

	<div class="list-group">
		<?php if (!empty($menu['title'])) { ?>
			<span class="list-group-item"><b><?php echo $menu['title']; ?></b></span>
		<?php } ?>

		<?php foreach ($menu['items'] as $code => $item) { ?>
			<?php if (!empty($item['items'])) { ?>
				<a href="#<?php echo $code; ?>" class="list-group-item" data-toggle="collapse"><?php echo $item['name']; ?> <i class="fa fa-caret-down"></i></a>
				<div class="collapse list-group-submenu" id="<?php echo $code; ?>">
					<?php foreach ($item['items'] as $child_item) { ?>
						<a class="list-group-item" href="<?php echo $child_item['link']; ?>" data-parent="#<?php echo $code; ?>"><?php echo $child_item['name']; ?></a>
					<?php } ?>
				</div>
			<?php } elseif (!empty($item)) { ?>
				<a class="list-group-item" href="<?php echo $item['link']; ?>"><?php echo $item['name']; ?></a>
			<?php } ?>
		<?php } ?>
	</div>
<?php } ?>

<script>
	$(document).ready(function() {
		var items = $('#column-left .list-group a');
		for(var i = 0; i < items.length; i++) {
			var url = $(items[i]).attr('href');
			if(url == window.location.href) {
				$(items[i]).addClass('active');
			}
		}
	});
</script>