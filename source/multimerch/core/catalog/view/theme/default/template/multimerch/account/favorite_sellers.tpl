<?php echo $header; ?>
<div class="container ms-followed-sellers">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<h2><?php echo $heading_title; ?></h2>

			<?php if (!empty($sellers)) { ?>
				<div id="ms-sellers-panel-grid">
					<div id="mm_store_info" class="row ms-sellers-panel">
						<?php foreach ($sellers as $seller) { ?>
							<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 15px;">
								<div class="avatar">
									<a href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" title="<?php echo $seller['nickname']; ?>" alt="<?php echo $seller['nickname']; ?>" /></a>
								</div>

								<div class="name">
									<div>
										<a href="<?php echo $seller['href']; ?>"><?php echo $seller['nickname']; ?></a>
									</div>
								</div>

								<?php if ($this->config->get('msconf_reviews_enable')) { ?>
									<div class="rating">
										<span data-toggle="tooltip" title="<?php echo $seller['reviews']['tooltip']; ?>">
											<div class="ms-ratings main">
												<div class="ms-empty-stars"></div>
												<div class="ms-full-stars" style="width: <?php echo $seller['reviews']['rating'] * 20; ?>%"></div>
											</div>
										</span>
										(<?php echo $seller['reviews']['total']; ?>)
									</div>
								<?php } ?>

								<div class="text-center">
									<a href="javascript:;" class="ms-seller-unfollow bottom-dotted-link" data-seller_id="<?php echo $seller['seller_id']; ?>"><?php echo $ms_favorite_seller_unfollow_button; ?></a>
									<p><?php echo $seller['date_followed']; ?></p>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php } else { ?>
				<p><?php echo $ms_favorite_seller_no_results; ?></p>
			<?php } ?>

			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<script>
	$(function() {
		$(document).on('click', '.ms-seller-unfollow', function () {
			var seller_id = $(this).data('seller_id');

			$.get("index.php?route=account/msfavoriteseller/follow&seller_id=" + seller_id, function (json) {
				if (json.success) {
					window.location.reload();
				} else if (json.error) {
					alert(json.error);
				}
			}, 'json')
		});
	});
</script>

<?php echo $footer; ?>