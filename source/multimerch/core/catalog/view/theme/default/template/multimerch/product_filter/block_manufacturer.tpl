<div class="multimerch_productfilter_block" data-type="manufacturer">
	<h4><?php echo $title; ?></h4>
	<div class="rows">
		<?php foreach ($elements as $v) { ?>
			<label class="checkbox-inline">
				<input
					type="checkbox"
					name="<?php echo $v['name']; ?>"
					value="<?php echo $v['value']; ?>"
					<?php echo $v['checked'] ? 'checked="checked"' : ''; ?>
					<?php echo !$v['enabled'] ? 'disabled="disabled"' : ''; ?>
				/>
				<span><?php echo $v['label']; ?></span>
			</label>
		<?php } ?>
	</div>
</div>