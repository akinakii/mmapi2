<?php echo $header; ?>
<div class="container ms-account-order-info">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>

	<?php if (isset($success) && ($success)) { ?>
	<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo $success; ?></div>
	<?php } ?>

	<?php if (isset($statustext) && ($statustext)) { ?>
	<div class="alert alert-<?php echo $statusclass; ?>"><?php echo $statustext; ?></div>
	<?php } ?>
	
	<?php if (isset($error_warning) && $error_warning) { ?>
	<div class="alert alert-danger warning main"><i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo $error_warning; ?></div>
	<?php } ?>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8 col-md-6'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<!-- order information -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
                        <i class="fa fa-book"></i>&nbsp;
                        <b><?php echo $ms_account_order_column_order_id . $suborder_id; ?></b>
                        <a href="<?php echo $link_print; ?>" class="btn btn-sm pull-right"><i class="fa fa-print fa-fw"></i>&nbsp;<?php echo $ms_print_order_packing_slip; ?></a>
                    </h3>
				</div>
				<table class="table table-responsive">
					<tbody>
                        <tr>
                            <td>
                                <p><?php echo $text_date_added; ?><b>&nbsp;<?php echo $date_added; ?></b></p>
	                            <?php if ($payment_method) { ?>
                                    <p><?php echo $text_payment_method; ?><b>&nbsp;<?php echo $payment_method; ?></b></p>
                                    <p><?php echo $ms_account_order_payment_status; ?>:<b>&nbsp;<?php echo $this->MsLoader->MsHelper->getStatusName(array('order_status_id' => $order_status_id)); ?></p>
	                            <?php } ?>
                            </td>
                        </tr>
					</tbody>
				</table>

                <div class="hidden-md hidden-lg sm-12">
                    <hr>
	                <?php echo $customer_block ?>
                </div>

                <!-- products -->
                <hr>
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i>&nbsp;<b><?php echo $ms_account_products; ?></b></h3>
                </div>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <td class="text-left" colspan="2"><b><?php echo $column_name; ?></b></td>
                            <td class="text-right"><b><?php echo $column_price; ?></b></td>
                            <?php if ($mm_shipping_flag) { ?>
                                <td class="text-right"><b><?php echo $mm_account_order_shipping_cost; ?></b></td>
                            <?php } ?>
                            <td class="text-right"><b><?php echo $column_total; ?></b></td>
                        </tr>
                    </thead>
                    <tbody>
					<?php foreach ($products as $product) { ?>
                        <tr>
                            <td class="text-center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
                            <td class="text-left"><?php echo ($product['quantity'] > 1 ? "{$product['quantity']} x " : ""); ?><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a> <?php echo $product['shipping_method']; ?>
								<?php foreach ($product['option'] as $option) { ?>
                                    <br />
									<?php if ($option['type'] != 'file') { ?>
                                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
									<?php } else { ?>
                                        &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
									<?php } ?>
								<?php } ?></td>
                            <td class="text-right"><?php echo $product['price']; ?></td>
							<?php if ($mm_shipping_flag) { ?>
                                <td class="text-right"><?php echo $product['shipping_cost']; ?></td>
							<?php } ?>
                            <td class="text-right"><?php echo $product['total']; ?></td>
                        </tr>
					<?php } ?>
                    </tbody>
                    <tfoot style="text-align: center;">
					<?php foreach ($totals as $total) { ?>
                        <tr>
                            <td class="text-left" colspan="<?php echo $mm_shipping_flag ? '3' : '2'; ?>"></td>
                            <td class="text-right"><?php echo $total['title']; ?></td>
                            <td class="text-right"><?php echo $total['text']; ?></td>
                        </tr>
					<?php } ?>
                    </tfoot>
                </table>

                <!-- transactions -->
                <hr>
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money"></i>&nbsp;<b><?php echo $ms_account_orders_transactions; ?></b></h3>
                </div>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <td class="text-left"><b><?php echo $ms_date_created; ?></b></td>
                            <td class="text-center"><b><?php echo $ms_account_transactions_description; ?></b></td>
                            <td class="text-center"><b><?php echo $ms_account_transactions_amount; ?></b></td>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($suborder_transactions) { ?>
						<?php foreach ($suborder_transactions as $transaction) { ?>
                            <tr>
                                <td class="text-left"><?php echo date($this->language->get('date_format_short'), strtotime($transaction['mb.date_created'])); ?></td>
                                <td class="text-center"><?php echo (utf8_strlen($transaction['mb.description']) > 80 ? mb_substr($transaction['mb.description'], 0, 80) . '...' : $transaction['mb.description']); ?></td>
                                <td class="text-center"><?php echo $this->currency->format($transaction['amount'], $this->config->get('config_currency')); ?></td>
                            </tr>
						<?php } ?>
					<?php } else { ?>
                        <td colspan="3" class="text-center" style="padding: 25px;"><?php echo $ms_account_orders_notransactions; ?></td>
					<?php } ?>
                    </tbody>
                </table>

                <!-- history -->
                <hr>
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-history"></i>&nbsp;<b><?php echo $ms_account_orders_history; ?></b></h3>
                </div>

                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <td class="text-left"><b><?php echo $ms_date_added; ?></b></td>
                        <td class="text-center"><b><?php echo $ms_account_orders_comment; ?></b></td>
                        <td class="text-center"><b><?php echo $ms_status; ?></b></td>
                    </tr>
                    </thead>

                    <tbody>
					<?php if ($order_history) { ?>
						<?php foreach ($order_history as $history) { ?>
                            <tr>
                                <td class="text-left"><?php echo date($this->language->get('date_format_short'), strtotime($history['date_added'])); ?></td>
                                <td class="text-center"><?php echo nl2br($history['comment']); ?></td>
                                <td class="text-center"><?php echo $this->MsLoader->MsSuborderStatus->getSubStatusName(array('order_status_id' => $history['order_status_id'])); ?></td>
                            </tr>
						<?php } ?>
					<?php } else { ?>
                        <td colspan="3" class="text-center" style="padding: 25px;"><?php echo $ms_account_orders_nohistory; ?></td>
					<?php } ?>
                    </tbody>
                    <tfoot>
                        <tr align="left">
                            <td colspan="3" class="no-border"><a id="button-change-history" href="javascript:;"><?php echo $ms_account_orders_change_status; ?></a></td>
                        </tr>
                        <tr id="history-content" style="display: none">
                            <td colspan="3" class="no-border">
                                <input type="hidden" name="suborder_id" id="suborder_id" value="<?php echo $suborder_id; ?>" /><br>
                                <textarea class="form-control" name="order_comment" id="order_comment" placeholder="<?php echo $ms_account_orders_add_comment; ?>" rows="3"></textarea><br>
                                <select name="order_status" id="order_status" class="form-control">
                                    <option value="0" disabled="disabled" selected="selected"><?php echo $ms_account_orders_status_select_default; ?></option>
                                    <?php foreach ($order_statuses as $status) { ?>
                                        <option value="<?php echo $status['ms_suborder_status_id']; ?>"><?php echo $status['name']; ?></option>
                                    <?php } ?>
                                </select><br>
                                <button style="margin-top: 5px;" type="button" id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary form-control" disabled="disabled"><?php echo $ms_account_orders_change_status; ?></button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
			</div>

            <?php if ($customer) { ?>
			<div id="seller_history"></div>
			<script>
				$('#seller_history').load('index.php?route=multimerch/account_order/sellerOrderConversation&order_id=<?php echo $order_id; ?>&seller_id=<?php echo $this->customer->getId(); ?>');
			</script>
            <?php } ?>
			<div class="buttons">
				<div class="pull-left"><a href="<?php echo $link_back; ?>" class="btn btn-default"><span><?php echo $button_back; ?></span></a></div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
        <div class="col-md-3 hidden-sm hidden-xs">
            <div class="panel panel-default">
                <?php echo $customer_block ?>
            </div>
        </div>
		<?php echo $column_right; ?>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#order_status').change(function() {
			$('#button-history').attr('disabled', $(this).val() !== 0 ? false : true);
		});

		$("#button-history").click(function() {
			var $btn = $(this).button('loading');

			$.ajax({
				type: "POST",
				dataType: "json",
				url: $('base').attr('href') + 'index.php?route=seller/account-order/jxAddHistory',
				data: $("#order_comment,#order_status,#suborder_id").serialize(),
				success: function(jsonData) {
					window.location.reload();
				},
				error: function() {
					console.error('Error occurred during status change!');
				}
			});

			$btn.button('reset');
		});

		$("#button-change-history").click(function() {
		    $("#history-content").toggle();
        });
	});
</script>
<?php echo $footer; ?>
