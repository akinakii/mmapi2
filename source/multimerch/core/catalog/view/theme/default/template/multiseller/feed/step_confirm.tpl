<div class="col-md-12">
	<div class="row">
		<div class="col-sm-12 step_header">
			<div class="col-md-4"></div>
			<div class="col-md-4 text-center step_header_text"><?php echo $ms_feed_step_confirm_title; ?></div>
			<div class="col-md-4 text-right step_header_steps"><?php echo $ms_feed_step_confirm_seq_number;?></div>
		</div>

		<div class="col-sm-12">
			<form method="post" id="ms-import-form" action="<?php echo $import; ?>" class="ms-form form-horizontal">
				<input type="hidden" name="key_field" value="model" />

				<div class="table-responsive">
					<table class="table">
						<?php foreach ($samples as $key => $value) { ?>
							<?php if (!$key || !isset($oc_fields[$key])) continue; ?>
							<?php $value = strip_tags($value); ?>
							<tr>
								<td class="text-right" style="width: 25%;"><?php echo $oc_fields[$key]['field_name']; ?></td>
								<td><?php echo utf8_strlen($value) > 100 ? mb_substr($value, 0, 100) . '...' : $value; ?></td>
							</tr>
						<?php } ?>
						<?php if ($attributes_included){ ?>
							<?php foreach ($attributes as $attribute){ ?>
								<?php if (isset($samples[$attribute['id']])) { ?>
									<tr>
										<td class="text-right"><?php echo $attribute['name']; ?></td>
										<td><?php echo utf8_strlen($samples[$attribute['id']]) > 100 ? mb_substr($samples[$attribute['id']], 0, 100) . '...' : $samples[$attribute['id']]; ?></td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</table>
				</div>

				<?php if ($this->config->get('msconf_scheduled_import_enabled') && !empty($this->session->data['import_data']['url_path'])) { ?>
					<div class="import-schedule">
						<div class="row">
							<div class="col-sm-12">
								<p style="float: left; margin-right: 10px; margin-top: 10px;"><?php echo $ms_feed_import_schedule_title; ?></p>
								<div class="radio">
									<label class="radio-inline">
										<input type="radio" name="is_scheduled" value="0" checked="checked" />
										<?php echo $ms_feed_import_schedule_no; ?>
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_scheduled" value="1" />
										<?php echo $ms_feed_import_schedule_yes; ?>
									</label>
								</div>
							</div>
						</div>
						<div class="row schedule_settings" style="margin-top:5px; display: none;">
							<div class="col-sm-12">
								<p style="float: left; margin-right: 10px; margin-top: 7px;"><?php echo $ms_feed_import_schedule_template; ?></p>
								<select name="schedule_cycle" class="form-control" style="width: 120px;">
									<option value="5_minutes"><?php echo $ms_feed_import_schedule_5_minutes; ?></option>
									<option value="30_minutes"><?php echo $ms_feed_import_schedule_30_minutes; ?></option>
									<option value="1_hour"><?php echo $ms_feed_import_schedule_1_hour; ?></option>
									<option value="4_hours"><?php echo $ms_feed_import_schedule_4_hours; ?></option>
									<option value="12_hours"><?php echo $ms_feed_import_schedule_12_hours; ?></option>
									<option value="24_hours"><?php echo $ms_feed_import_schedule_24_hours; ?></option>
								</select>
							</div>
						</div>
					</div>
				<?php } ?>
			</form>
		</div>

		<div class="col-sm-12 step_footer">
			<div class="pull-right">
				<button onclick="startImport();" class="btn btn-primary"><?php echo $ms_feed_start_new_import; ?></button>
			</div>

			<button class="btn btn-default prev_step" data-next_id="2"><?php echo $ms_button_back; ?></button>
		</div>
	</div>
</div>