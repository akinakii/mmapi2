<?php foreach ($elements as $attribute_id => $attribute) { ?>
	<div class="multimerch_productfilter_block" id="<?php echo $attribute['id']; ?>" data-type="oc_attribute" data-id="<?php echo $attribute['data-id']; ?>">
		<h4><?php echo $attribute['title']; ?></h4>
		<?php foreach ($attribute['values'] as $v) { ?>
			<label class="checkbox-inline">
				<input
					type="checkbox"
					name="<?php echo $v['name']; ?>"
					value="<?php echo $v['value']; ?>"
					<?php echo $v['checked'] ? 'checked="checked"' : ''; ?>
					<?php echo !$v['enabled'] ? 'disabled="disabled"' : ''; ?>
				/>
				<span><?php echo $v['label']; ?></span>
			</label>
		<?php } ?>
	</div>
<?php } ?>
