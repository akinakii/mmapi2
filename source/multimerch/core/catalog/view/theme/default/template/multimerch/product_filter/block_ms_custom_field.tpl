<?php foreach ($elements as $custom_field_id => $custom_field) { ?>
	<div class="multimerch_productfilter_block" id="<?php echo $custom_field['id']; ?>" data-type="ms_custom_field" data-id="<?php echo $custom_field['data-id']; ?>">
		<h4><?php echo $custom_field['title']; ?></h4>
		<?php foreach ($custom_field['values'] as $v) { ?>
			<label class="checkbox-inline">
				<input
					type="checkbox"
					name="<?php echo $v['name']; ?>"
					value="<?php echo $v['value']; ?>"
					<?php echo $v['checked'] ? 'checked="checked"' : ''; ?>
					<?php echo !$v['enabled'] ? 'disabled="disabled"' : ''; ?>
				/>
				<span><?php echo $v['label']; ?></span>
			</label>
		<?php } ?>
	</div>
<?php } ?>
