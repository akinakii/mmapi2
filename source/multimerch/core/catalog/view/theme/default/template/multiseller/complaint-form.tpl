<?php echo $header; ?>
<div class="container">
<?php if (!$complaint_sent){ ?>
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
<?php } ?>
<?php if (!empty($error)) { ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } ?>
<div class="row">
    <div class="col-xs-12">
        <div id="content">
            <h1><?php echo $ms_complaint_form_heading; ?></h1>

            <?php if ($product){ ?>

            <!-- mm complaint product info block start -->
            <div class="col-xs-12 col-md-8 mm_box mm_description">
                <div class="row">
                    <div class="col-xs-3"><a href="<?php echo $product['product_href']; ?>"><img class='img-responsive' src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>"/></a></div>
                    <div class="col-xs-9"><h4><a href="<?php echo $product['product_href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <h5><?php echo $ms_by;?> <a href="<?php echo $product['seller_href']; ?>"><?php echo $product['seller_nickname']; ?></a></h5></div>
                </div>
            </div>
            <!-- mm complaint product info block end -->

            <?php } else { ?>
                <!-- mm complaint seller profile avatar block start -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-5 col-lg-4">
                        <div class="mm_box mm_description clearfix">
                            <div class="info-box">
                                <a class="avatar-box thumbnail" href="<?php echo $seller['href']; ?>"><img src="<?php echo $seller['thumb']; ?>" /></a>
                                <div>
                                    <ul class="list-unstyled">
                                        <li><h3 class="sellersname"><a href="<?php echo $seller['href']; ?>"><?php echo $seller['ms.nickname']; ?></a></h3></li>
                                        <li><?php echo $seller['settings']['slr_company'] ;?></li>
                                        <li><a target="_blank" href="<?php echo $seller['settings']['slr_website'] ;?>"><?php echo $seller['settings']['slr_website'] ;?></a></li>
                                        <li><?php echo trim((!empty($seller['settings']['slr_city']) ? $seller['settings']['slr_city'] : '') . ', ' . (!empty($seller['settings']['slr_country']) ? $seller['settings']['slr_country'] : ''), ',') ;?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- mm complaint seller profile avatar block end -->
            <?php } ?>
            <div class="clearfix"></div>
            <form method="POST">
                <div class="form-group required">
                    <label class="control-label" for="input-comment"><?php echo $ms_complaint_form_title; ?></label>
                    <textarea name="comment" id="input-comment" rows="5" class="form-control"><?php echo $comment; ?></textarea>
                </div>
                <div class="buttons">
                    <div class="pull-left">
                        <button class="btn btn-primary"><?php echo $ms_complaint_button_submit; ?> </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
</div>
<?php echo $footer; ?>