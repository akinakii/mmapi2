<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<?php if (!empty($success)) { ?>
		<div class="alert alert-success"><?php echo $success; ?></div>
	<?php } ?>
	
	<?php if (!empty($error_warning)) { ?>
		<div class="alert alert-danger"><?php echo $error_warning; ?></div>
	<?php } ?>
	
	<?php if (isset($holiday_mode_message) && $holiday_mode_message) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $holiday_mode_message; ?></div>
	<?php } ?>
	
	<?php if (!empty($ms_shipping_rules_empty_message)) { ?>
		<div class="alert alert-warning"><i class="fa fa-warning"></i> <?php echo $ms_shipping_rules_empty_message; ?></div>
	<?php } ?>

	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>

		<div id="content" class="<?php echo $class; ?> ms-account-dashboard"><?php echo $content_top; ?>
			<div class="mm_dashboard ms-seller-settings">
				<h1><i class="fa fa-tachometer"></i><?php echo $ms_account_settings ;?></h1>

				<ul class="nav nav-tabs topbar">
					<li class="active"><a href="#tab-account" data-toggle="tab"><?php echo $ms_seller_account; ?></a></li>
					<li><a href="#tab-address" data-toggle="tab"><?php echo $ms_seller_address; ?></a></li>

					<?php if (!empty($stripe_subscription)) { ?>
						<li><a href="#tab-stripe-subscription" data-toggle="tab"><?php echo $ms_stripe_subscription; ?></a></li>
					<?php } ?>

					<?php if(!empty($payment_gateways)) { ?>
						<li><a href="#tab-payment-gateways" data-toggle="tab"><?php echo $ms_account_setting_payments_tab; ?></a></li>
					<?php } ?>

					<?php if (!in_array((int)$this->config->get('msconf_shipping_type'), [\MultiMerch\Core\Shipping\Shipping::TYPE_DISABLED, \MultiMerch\Core\Shipping\Shipping::TYPE_OC, \MultiMerch\Core\Shipping\Shipping::TYPE_PER_PRODUCT])) { ?>
						<li><a href="#tab-seller-shipping" data-toggle="tab"><?php echo $ms_shipping_title; ?></a></li>
					<?php } ?>

					<?php if ($this->config->get('msconf_holiday_mode_allow')) { ?>
						<li><a href="#tab-holiday-mode" data-toggle="tab"><?php echo $ms_account_setting_holiday_mode_tab; ?></a></li>
					<?php } ?>
					
					<?php if (1 == $this->config->get('ms_linnworks_enabled')) { ?>
						<li><a href="#tab-linnworks" data-toggle="tab"><?php echo $lw_integration_tab; ?></a></li>
					<?php }?>
				</ul>

				<div class="tab-content">
					<div id="tab-account" class="tab-pane active">
						<form id="ms-seller-account" class="ms-form form-horizontal">
							<fieldset>
								<legend style="margin-bottom: 0;"><?php echo $ms_seller_information; ?></legend>

								<div class="lang-chooser">
									<?php $i = 0; foreach ($languages as $language) { ?>
										<img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo $i == 0 ? 'active' : ''; ?>" data-lang="<?php echo $language['code'] ?>">
									<?php $i++; } ?>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_first_name; ?></label>
									<div class="col-sm-10">
										<input type="text" name="account[firstname]" placeholder="<?php echo $ms_seller_first_name; ?>" class="form-control" value="<?php echo !empty($account['firstname']) ? $account['firstname'] : ''; ?>" />
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_last_name; ?></label>
									<div class="col-sm-10">
										<input type="text" name="account[lastname]" placeholder="<?php echo $ms_seller_last_name; ?>" class="form-control" value="<?php echo !empty($account['lastname']) ? $account['lastname'] : ''; ?>" />
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_email; ?></label>
									<div class="col-sm-10">
										<input type="email" name="account[email]" placeholder="<?php echo $ms_seller_email; ?>" class="form-control" value="<?php echo !empty($account['email']) ? $account['email'] : ''; ?>" />
									</div>
								</div>

								<?php if ($this->config->get('msconf_msf_seller_property_enabled') && !empty($msf_seller_properties['default'])) { ?>
									<?php foreach ($msf_seller_properties['default'] as $msf_seller_property) { ?>
										<div class="form-group <?php if ($msf_seller_property['required']) { ?>required<?php } ?>">
											<label class="col-sm-2 control-label"><?php echo $msf_seller_property['name']; ?></label>
											<div class="col-sm-10">
												<?php if ('select' === $msf_seller_property['type']) { ?>
													<select name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" class="form-control">
														<?php foreach ($msf_seller_property['values'] as $value) { ?>
															<option value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>selected="selected"<?php } ?>><?php echo $value['name']; ?></option>
														<?php } ?>
													</select>
												<?php } elseif ('checkbox' === $msf_seller_property['type']) { ?>
													<div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 0;">
														<?php foreach ($msf_seller_property['values'] as $value) { ?>
															<div class="checkbox">
																<label>
																	<input type="checkbox" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>checked="checked"<?php } ?> />
																	<?php echo $value['name'] ?: ''; ?>
																</label>
															</div>
														<?php } ?>
													</div>
												<?php } elseif ('text' === $msf_seller_property['type']) { ?>
													<?php foreach ($languages as $language) { ?>
														<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
														<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
															<div class="lang-img-icon-input-text"><img src="<?php echo $img; ?>"></div>
														</div>
														<input type="text" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][text_values][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']]) ? $msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']] : ''; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
													<?php } ?>
												<?php } ?>

												<?php if (!empty($msf_seller_property['description'][$this->config->get('config_language_id')]['note'])) { ?>
													<div class="ms-note">
														<?php echo $msf_seller_property['description'][$this->config->get('config_language_id')]['note']; ?>
													</div>
												<?php } ?>

												<?php if (!empty($msf_seller_property['description'][$this->config->get('config_language_id')]['note'])) { ?>
													<div class="ms-note">
														<?php echo $msf_seller_property['description'][$this->config->get('config_language_id')]['note']; ?>
													</div>
												<?php } elseif (!empty($msf_seller_property['note'])) { ?>
													<div class="ms-note">
														<?php echo $msf_seller_property['note']; ?>
													</div>
												<?php } ?>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</fieldset>
						</form>
					</div>

					<div id="tab-address" class="tab-pane">
						<form id="ms-seller-address" class="ms-form form-horizontal">
							<input type="hidden" name="address[address_id]" value="<?php echo isset($address['address_id']) ? $address['address_id'] : ''; ?>" />

							<fieldset>
								<legend><?php echo $ms_seller_address; ?></legend>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_full_name; ?></label>

									<div class="mm_form col-sm-10">
										<input type="text" class="form-control" name="address[fullname]"
											   value="<?php echo isset($address['fullname']) ? $address['fullname'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_full_name; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_address1; ?></label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="address[address_1]"
											   value="<?php echo isset($address['address_1']) ? $address['address_1'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_address1_placeholder ;?>">
                                        <input type="hidden" name="settings[slr_address_line1_old]" value="<?php echo (isset($settings['slr_city'])) ? $settings['slr_city'] : '' ; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_address2; ?></label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="address[address_2]"
											   value="<?php echo isset($address['address_2']) ? $address['address_2'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_address2_placeholder ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_city; ?></label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="address[city]"
											   value="<?php echo isset($address['city']) ? $address['city'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_city; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_state; ?></label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="address[state]"
											   value="<?php echo isset($address['state']) ? $address['state'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_state ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_zip; ?></label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="address[zip]"
											   value="<?php echo isset($address['zip']) ? $address['zip'] : ''; ?>"
											   placeholder="<?php echo $ms_seller_zip ;?>">
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_country; ?></label>

									<div class="col-sm-10">
										<select class="form-control" name="address[country_id]">
											<option value="0" disabled="disabled"><?php echo $ms_seller_country_select; ?></option>

											<?php foreach($countries as $country) { ?>
												<option value="<?php echo $country['country_id'] ;?>" <?php echo isset($address['country_id']) && (int)$address['country_id'] === (int)$country['country_id'] ? 'selected' : ''; ?>><?php echo $country['name']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</fieldset>
						</form>
					</div>

					<div id="tab-stripe-subscription" class="tab-pane">
						<?php if (!empty($stripe_subscription)) { ?>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right"><?php echo $ms_stripe_subscription_plan_selected; ?></label>
								<div class="col-sm-10">
									<?php echo !empty($stripe_subscription['name']) ? $stripe_subscription['name'] : '-'; ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label text-right"><?php echo $ms_stripe_subscription_period; ?></label>
								<div class="col-sm-10">
									<?php echo !empty($stripe_subscription['period']) ? $stripe_subscription['period'] : '-'; ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label text-right"><?php echo $ms_stripe_subscription_terms; ?></label>
								<div class="col-sm-10">
									<?php if (!empty($stripe_subscription['terms'])) { ?>
										<?php foreach ($stripe_subscription['terms'] as $term) { ?>
											<?php echo $term['amount_formatted'] . ' ' . $term['interval_formatted']; ?>
										<?php } ?>
									<?php } else { ?>
										-
									<?php } ?>
								</div>
							</div>
						<?php } ?>

						<?php if (!empty($stripe_customer_card)) { ?>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right"><?php echo $ms_stripe_subscription_your_card; ?></label>
								<div class="col-sm-10">
									<?php echo $stripe_customer_card; ?>
								</div>
							</div>
						<?php } ?>
					</div>

					<div id="tab-payment-gateways" class="tab-pane">
						<ul class="nav nav-tabs pg-topbar">
							<?php foreach($payment_gateways as $payment_gateway) { ?>
								<li <?php echo reset($payment_gateways) == $payment_gateway ? 'class="active"' : ''; ?>><a href="#tab-<?php echo $payment_gateway['code']; ?>" data-toggle="tab"><?php echo $payment_gateway['text_title']; ?></a></li>
							<?php } ?>
						</ul>
						<div class="pg-message" style="display: none;"></div>
						<div class="tab-content ms-pg-content">
							<?php foreach($payment_gateways as $payment_gateway) { ?>
								<div id="tab-<?php echo $payment_gateway['code']; ?>" class="tab-pane <?php echo reset($payment_gateways) == $payment_gateway ? 'active' : ''; ?>">
									<input type="hidden" class="ms_pg_code" value="<?php echo $payment_gateway['code']; ?>">
									<?php echo $payment_gateway['view']; ?>
								</div>
							<?php } ?>
						</div>
					</div>

					<div id="tab-seller-shipping" class="tab-pane">
						<fieldset>
							<legend><?php echo $ms_shipping_rules_manage_title; ?></legend>
							<?php echo $ms_shipping; ?>
						</fieldset>
					</div>

					<?php if($this->config->get('msconf_holiday_mode_allow')) { ?>
						<div id="tab-holiday-mode" class="tab-pane">
							<fieldset>
								<form id="ms-seller-holiday-mode" class="ms-form form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label"><?php echo $ms_account_setting_holiday_mode_tab; ?></label>
										<div class="mm_form col-sm-10">
											<div style="display:block; width: 100%;">
												<input type="radio" name="settings[slr_holiday_mode_enabled]" value="0" id="holiday-mode-off" <?php echo !$settings['slr_holiday_mode_enabled'] ? "checked": ''; ?> /> <label for="holiday-mode-off"><?php echo $ms_account_setting_holiday_mode_inactive; ?></label>
											</div>
											<div style="display:block; width: 100%;">
												<input type="radio" name="settings[slr_holiday_mode_enabled]" value="1" id="holiday-mode-on" <?php echo $settings['slr_holiday_mode_enabled'] ? "checked": ''; ?> /> <label for="holiday-mode-on"><?php echo $ms_account_setting_holiday_mode_active; ?></label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label"><?php echo $ms_account_setting_holiday_mode_message; ?></label>
										<div class="mm_form col-sm-10">
											<div class="lang-chooser">
												<?php $i = 0; foreach ($languages as $language) { ?>
												<img src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo $i == 0 ? 'active' : ''; ?>" data-lang="<?php echo $language['code'] ?>">
												<?php $i++; ?>
												<?php } ?>
											</div>
											<?php $i = 0; foreach ($languages as $language) { ?>
											<?php $img = "catalog/language/{$language['code']}/{$language['code']}.png"; ?>
											<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo $i == 0 ? 'true' : 'false'; ?>">
												<textarea rows="6" name="languages[<?php echo $language['language_id']; ?>][slr_holiday_mode_note]" class="form-control ckeditor mm_input_language mm_flag_<?php echo $language['code']; ?>"><?php echo isset($settings['slr_holiday_mode_note'][$language['language_id']] ) ? $settings['slr_holiday_mode_note'][$language['language_id']] : ''; ?></textarea>
											</div>
											<?php $i++; ?>
											<?php } ?>
											<p class="ms-note"><?php echo $ms_account_setting_holiday_mode_message_note; ?></p>
										</div>
									</div>
								</form>
							</fieldset>
						</div>
					<?php }  ?>
					
					<?php if (1 == $this->config->get('ms_linnworks_enabled')) { ?>
						<div id="tab-linnworks" class="tab-pane">
								<fieldset>
									<legend>Linnworks channel integration</legend>
									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-3 control-label"><?php echo $ms_status; ?></label>
											<div class="mm_form col-sm-9">
												<div style="display:block; width: 100%;">
													<?php if ($lw_token){ ?>
														<span class="alert alert-success"><?php echo $lw_status_active; ?></span>
													<?php } else { ?>
														<span class="alert alert-danger"><?php echo $lw_status_inactive; ?></span>
													<?php } ?>

												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label"><?php echo $lw_access_key; ?></label>
											<div class="mm_form col-sm-9">
												<div style="display:block; width: 100%;">
													<input type="text" class="form-control" readonly="true" value="<?php echo $lw_access_code; ?>" onfocus="$(this).select()"/>
												</div>
											</div>
										</div>
									</div>	
								</fieldset>
						</div>
					<?php } ?>
					
				</div>

				<div class="buttons">
					<div class="pull-right">
						<a class="btn btn-primary" id="ms-submit-button"><span><?php echo $ms_button_save; ?></span></a>
					</div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>

<?php $timestamp = time(); ?>
<script>
	var msGlobals = {
		config_language: '<?php echo $this->config->get("config_language") ;?>',
		timestamp: '<?php echo $timestamp; ?>',
		session_id: '<?php echo session_id(); ?>',
		uploadError: "<?php echo htmlspecialchars(sprintf($ms_error_file_upload_error, $ms_file_default_filename, $ms_file_unclassified_error), ENT_QUOTES, 'UTF-8'); ?>",
		formError: '<?php echo htmlspecialchars($ms_error_form_submit_error, ENT_QUOTES, "UTF-8"); ?>',
		field_from_enabled: "$this->config->get('msconf_shipping_field_from_enabled')",
		field_shipping_method_enabled: "$this->config->get('msconf_shipping_field_shipping_method_enabled')",
		field_delivery_time_enabled: "$this->config->get('msconf_shipping_field_delivery_time_enabled')",
	};
</script>
<?php echo $footer; ?>