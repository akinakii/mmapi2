<!-- addresses -->

<?php if (! empty($customer)): ?>
	<div class="panel-heading">
		<h3 class="panel-title">
			<i class="fa fa-user"></i>&nbsp;<b><?php echo $column_customer; ?></b>
		</h3>
	</div>
	<table class="table table-responsive">
		<tbody>
		<tr>
			<td class="left no-border">
				<p>
					<?php if (isset($customer['href'])) { ?>
						<a href="<?php echo $customer['href']; ?>" target="_blank"><?php echo $customer['firstname']; ?> <?php echo $customer['lastname']; ?></a>
					<?php } else { ?>
						<?php echo $customer['firstname']; ?> <?php echo $customer['lastname']; ?>
					<?php } ?>
				</p>
				<p><?php echo $customer['email']; ?></p>
				<p><?php echo $customer['telephone']; ?></p>
			</td>
		</tr>
		</tbody>
	</table>
	<hr>
<?php endif; ?>

<div class="panel-heading">
	<h3 class="panel-title">
		<i class="fa fa-map-marker"></i>&nbsp;
		<b>
			<?php echo $shipping_address === $payment_address ? $ms_seller_address : $text_payment_address; ?>
		</b>
	</h3>
</div>
<table class="table table-responsive">
	<tbody>
	<tr>
		<td class="left no-border">
			<?php echo $payment_address ?>
		</td>
	</tr>
	</tbody>
</table>
<?php if ($shipping_address && $shipping_address !== $payment_address) { ?>
	<hr>
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-map-marker"></i>&nbsp;<b><?php echo $text_shipping_address; ?></b></h3>
	</div>
	<table class="table table-responsive">
		<tbody>
		<tr>
			<td class="left no-border">
				<?php echo $shipping_address ?>
			</td>
		</tr>
		</tbody>
	</table>
<?php } ?>
