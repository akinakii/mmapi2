<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-body">
			<?php if ($is_authorized && !empty($wishlist)) { ?>
				<i class="fa fa-check success"></i>
				<div class="title"><?php echo sprintf($ms_wishlist_add_success, $product['name']); ?></div>
				<div class="buttons">
					<button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
						<?php echo $ms_wishlist_btn_continue; ?>
					</button>
					<a href="<?php echo $this->url->link('account/mswishlist', '', 'SSL'); ?>" class="btn btn-default"><?php echo $ms_wishlist_btn_to_wishlist; ?></a>
				</div>
			<?php } else { ?>
				<i class="fa fa-times error"></i>
				<div class="title"><?php echo $ms_wishlist_add_error_login; ?></div>
				<div class="buttons">
					<a href="<?php echo $this->url->link('account/login', '', 'SSL'); ?>" class="btn btn-primary"><?php echo $ms_wishlist_btn_login; ?></a>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						<?php echo $ms_wishlist_btn_continue; ?>
					</button>
				</div>
			<?php } ?>
		</div>
	</div>
</div>