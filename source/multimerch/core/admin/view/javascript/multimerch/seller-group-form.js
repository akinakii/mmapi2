$(function () {
	$("#ms-submit-button").click(function() {
		var id = $(this).attr('id');
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=multimerch/seller-group/jxSave&token=' + msGlobals.token,
			data: $('#form').serialize(),
			beforeSend: function() {
				$('div.text-danger').remove();
				$('.alert-danger').hide().find('i').text('');
				$('#error-holder').find('p').remove();
			},
			complete: function(jqXHR, textStatus) {
				$(this).show().prev('span.wait').remove();
			},
			success: function(jsonData) {
				if (!jQuery.isEmptyObject(jsonData.errors)) {
					var html_errors = '';
					for (error in jsonData.errors) {
						html_errors += jsonData.errors[error] + '<br/>';
						$('[name="'+error+'"]').parents('.col-sm-10, td').append("<div class='text-danger'>" + jsonData.errors[error] + "</div>");
						$('[name="'+error+'"]').closest('div').addClass('has-error');
					}

					$('#error-holder').find('i').after(" <p style='display: inline-table;'>"+html_errors+"</p>");
					$('#error-holder').show();

					window.scrollTo(0, 0);
				} else {
					window.location = 'index.php?route=multimerch/seller-group&token=' + msGlobals.token;
				}
			}
		});
	});

	function toggleStripeSubscriptionFields() {
		var enabled = $('input[name="seller_group[settings][slr_gr_stripe_subscription_enabled]"]:checked').val();
		if (enabled == 0) {
			$('#base-plan, #per-seat-plan').css('opacity', '0.5');
			$('#base-plan, #per-seat-plan').find('select').attr('disabled', true);
		} else {
			$('#base-plan, #per-seat-plan').css('opacity', '1');
			$('#base-plan, #per-seat-plan').find('select').attr('disabled', false);
		}
	}

	toggleStripeSubscriptionFields();
	$('input[name="seller_group[settings][slr_gr_stripe_subscription_enabled]"]').on('change', toggleStripeSubscriptionFields);

	$('#override-default-commissions').click(function () {
		$('#commissions-settings').toggle();

		if ($('#commissions-settings').is(':visible')) {
			$('#override-default-commissions').siblings('.alert').hide();
			$('#override-default-commissions').text(msGlobals.text.remove_override);
		} else {
			$('#override-default-commissions').siblings('.alert').show();
			$('#override-default-commissions').text(msGlobals.text.add_override);
		}
	});

	// Start MSF seller properties

	function jxGetSellerGroupMsfSellerProperties(action) {
		var action = action || 'initial';
		var seller_group_id = $('input[name="seller_group[seller_group_id]"]').val();

		$.ajax({
			url: 'index.php?route=multimerch/seller-group/jxGetSellerGroupMsfSellerProperties',
			type: 'GET',
			data: {token: msGlobals.token, seller_group_id: seller_group_id, action: action},
			dataType: 'json',
			success: function (json) {
				if (json.html) {
					$('#seller-group-msf-seller-properties').html(json.html);
				}

				$('input[id="input-msf-seller-property"]').autocomplete({
					'source': function(request, response) {
						var added_ids = [];
						$.map($('#msf-seller-properties .msf-seller-property-id'), function (input) {
							added_ids.push($(input).val());
						});

						$.ajax({
							url: 'index.php?route=multimerch/seller-group/jxAutocompleteMsfSellerProperties',
							type: 'GET',
							data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
							dataType: 'json',
							success: function(json) {
								response($.map(json, function(item) {
									return {
										label: item['label'],
										value: item['msf_seller_property_id']
									}
								}));
							}
						});
					},
					'select': function(item) {
						$('input[id="input-msf-seller-property"]').val('');

						var html = '';
						html += '<li class="list-group-item ui-state-default seller-property">';
						html += '	<input type="hidden" name="seller_group[msf_seller_properties][]" value="' + item['value'] + '" class="msf-seller-property-id" />';
						html += '	<strong class="value-name">' + item['label'] + '</strong>';
						html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
						html += '</li>';

						$('#msf-seller-properties').append(html);
					}
				});
			}
		});
	}

	$(document).on('click', '#seller-group-msf-seller-properties .override', function () {
		$('input[name="msf_seller_properties_overridden"]').val(1);
		jxGetSellerGroupMsfSellerProperties('override');
	});

	$(document).on('click', '#seller-group-msf-seller-properties .remove-override', function () {
		$('input[name="msf_seller_properties_overridden"]').val(0);
		jxGetSellerGroupMsfSellerProperties('remove_override');
	});

	jxGetSellerGroupMsfSellerProperties();

	$(document).on('click', '.ms-remove', function () {
		$(this).closest('li').remove();
	});

	// End MSF seller properties
});