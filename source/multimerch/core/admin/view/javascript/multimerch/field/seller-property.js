$(function () {
	$('#list-msf-seller-properties').dataTable({
		"sAjaxSource": "index.php?route=multimerch/field/seller-property/jxGetList&token=" + msGlobals.token,
		"aoColumns": [
			{ "mData": "name", "sClass": "text-left" },
			{ "mData": "type", "sClass": "text-left" },
			{ "mData": "values", "bSortable": false, "sClass": "text-left" },
			{ "mData": "seller_groups", "bSortable": false, "sClass": "text-left" },
			{ "mData": "status", "sClass": "text-left" },
			{ "mData": "actions", "bSortable": false, "sClass": "text-right"}
		],
		"initComplete": function(settings, json) {
			var api = this.api();
			var statusColumn = api.column('#status_column');

			$('#status_select').change(function() {
				statusColumn.search($(this).val()).draw();
			});
		}
	});

	$(document).on('click', '#list-msf-seller-properties input:checkbox', function() {
		var selected = $('#list-msf-seller-properties').children('tbody').find('input:checkbox:checked');
		if(selected.length > 0) {
			$('#ms-delete').show();
		} else {
			$('#ms-delete').hide();
		}
	});
})