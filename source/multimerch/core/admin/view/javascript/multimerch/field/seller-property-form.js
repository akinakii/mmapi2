$(function () {
	// Show only selected language's fields
	processLanguageChange(msGlobals.current_language)

	// Language select
	$(".select-input-lang").on("click", function() {
		var selected_lang = $(this).data('lang');
		processLanguageChange(selected_lang);
	});

	function processLanguageChange(language) {
		var lang_inputs = $('.lang-select-field');

		lang_inputs.each(function() {
			if ($(this).data('lang') == language) {
				$(this).closest('.lang-field-holder').show();
				$(this).siblings('.lang-img-icon-input').show();
			} else {
				$(this).closest('.lang-field-holder').hide();
				$(this).siblings('.lang-img-icon-input').hide();
			}
		});

		$('.lang-chooser img').each(function() {
			if ($(this).data('lang') == language) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	}

	$(document).on('click', '#ms-submit-button', function() {
		// Change values sort order
		$.map($('.value'), function (item, index) {
			var actual_index = index + 1;
			var inputs = $(item).find('input');
			$.map(inputs, function (input) {
				var new_name = $(input).attr('name').replace(/values\[\d+\]/gi, 'values[' + actual_index + ']');
				$(input).attr('name', new_name);
			});
		});

		// Disable values creator input
		$('.value-creator input').attr('disabled', true);
		$('.value-creator').hide();

		$.ajax({
			type: 'post',
			url: 'index.php?route=multimerch/field/seller-property/jxSave&token=' + msGlobals.token,
			data: $('#msf-seller-property-form').serialize(),
			dataType: 'json',
			success: function(json) {
				if(json.errors) {
					$('#error-holder').empty();
					$('#msf-seller-property-form').find('.text-danger').remove();
					$('#msf-seller-property-form').find('div.has-error').removeClass('has-error');

					for (error in json.errors) {
						if ($('[name^="' + error + '"]').length > 0) {
							$('[name^="' + error + '"]').closest('div').addClass('has-error');
							$('[name^="' + error + '"]').parents('div:first').append("<div class='text-danger' id='error_" + error + "'>" + json.errors[error] + "</div>");
						}

						$('#error-holder').append(json.errors[error] + '<BR>').show();
					}

					$('.value-creator input').attr('disabled', false);
					$('.value-creator').show();

					window.scrollTo(0,0);
				} else {
					window.location = json.redirect.replace('&amp;', '&');
				}
			}
		});
	});

	$('select[name="type"]').on('change', function() {
		if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image') {
			$('#values-block').show();
		} else {
			$('#values-block').hide();
		}
	});

	$('select[name="type"]').trigger('change');

	// MSF seller property values (sortable list)
	$("#msf-seller-property-values").sortable({
		placeholder: "ui-sortable-placeholder",
		items: "> li:not(:last)"
	});

	$(document).on('click', '.ms-remove-value', function () {
		$(this).closest('li').remove();
	})

	$(document).on('click', '.value-title', function () {
		$(this).siblings('.value-info').toggleClass('hidden');
		$(this).find('.toggle-value-info i').toggleClass('fa-sort-down').toggleClass('fa-sort-up');
	});

	$('.create-value').click(function (e) {
		e.preventDefault();
		var $creator = $(this).closest('.value-creator');
		var languages = $(".select-input-lang");
		var default_language = $(".select-input-lang.default").data('lang');
		var active_language = $(".select-input-lang.active").data('lang');
		var default_name = $creator.find('input:text[data-lang="' + default_language + '"]').val();

		if (default_name) {
			$.map(languages, function (item) {
				var lang = $(item).data('lang');
				var name = $creator.find('input:text[data-lang="' + lang + '"]').val();
				$creator.find('input:text[data-lang="' + lang + '"]').attr('value', name);
			});

			var $creator_content = $creator.find('.value-creator-content').html();

			var html = '';
			html += '<li class="list-group-item ui-state-default value ui-sortable-handle">';
			html += '	<div class="value-title">';

			$.map(languages, function (item) {
				var lang = $(item).data('lang');
				var name = $creator.find('input:text[data-lang="' + lang + '"]').val();

				html += '	<div class="lang-field-holder"' + (lang != active_language ? ' style="display: none;"' : '') + '>';
				html += '		<strong class="value-name lang-select-field" data-lang="' + lang + '">' + (name ? name : ('<i>' + msGlobals.texts.ms_field_seller_property_no_name + '</i>')) + '</strong>';
				html += '	</div>';
			});

			html += '		<span class="toggle-value-info"><i class="fa fa-sort-down"></i></span>';
			html += '	</div>'
			html += '	<div class="value-info hidden">';
			html += 		$creator_content;
			html += '		<div class="row actions">';
			html += '			<div class="col-sm-12 text-right">';
			html += '				<a class="icon-remove ms-remove-value bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
			html += '			</div>';
			html += '		</div>';
			html += '	</div>';
			html += '</li>';

			$('#msf-seller-property-values li:last').before(html);
		}

		$creator.find('input:text').val('');
	});
})