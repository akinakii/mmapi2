$(function () {
	$(document).on('click', '#ms-submit-button', function() {
		// Change sort order
		$.map($('ul.ui-sortable'), function (ul) {
			$.map($(ul).find('li.ui-state-default'), function (item, index) {
				var actual_index = index + 1;
				var inputs = $(item).find('input');
				$.map(inputs, function (input) {
					var new_name = $(input).attr('name').replace(/\[\d+\]/gi, '[' + actual_index + ']');
					$(input).attr('name', new_name);
				});
			})
		});

		$.ajax({
			type: 'post',
			url: 'index.php?route=multimerch/field/default-configuration/jxSave&token=' + msGlobals.token,
			data: $('#fields-form').serialize(),
			dataType: 'json',
			success: function(json) {
				if (json.redirect) {
					window.location = json.redirect.replace('&amp;', '&');
				}
			}
		});
	});

	// Sortable lists
	$("#msf-attributes").sortable({
		placeholder: "ui-sortable-placeholder"
	});
	$("#msf-variations").sortable({
		placeholder: "ui-sortable-placeholder"
	});
	$("#msf-seller-properties").sortable({
		placeholder: "ui-sortable-placeholder"
	});
	$("#mspf-blocks").sortable({
		placeholder: "ui-sortable-placeholder"
	});

	$(document).on('click', '.ms-remove', function () {
		$(this).closest('li').remove();
	});

	$('input[id="input-msf-attribute"]').autocomplete({
		'source': function(request, response) {
			var added_ids = [];
			$.map($('#msf-attributes .msf-attribute-id'), function (input) {
				added_ids.push($(input).val());
			});

			$.ajax({
				url: 'index.php?route=multimerch/field/attribute/jxAutocompleteMsfAttributes',
				type: 'GET',
				data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['label'],
							value: item['msf_attribute_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[id="input-msf-attribute"]').val('');

			var html = '';
			html += '<li class="list-group-item ui-state-default attribute">';
			html += '	<input type="hidden" name="msf_attributes[0][id]" value="' + item['value'] + '" class="msf-attribute-id" />';
			html += '	<strong class="value-name">' + item['label'] + '</strong>';
			html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
			html += '</li>';

			$('#msf-attributes').append(html);
		}
	});

	$('input[id="input-msf-variation"]').autocomplete({
		'source': function(request, response) {
			var added_ids = [];
			$.map($('#msf-variations .msf-variation-id'), function (input) {
				added_ids.push($(input).val());
			});

			$.ajax({
				url: 'index.php?route=multimerch/field/variation/jxAutocompleteMsfVariations',
				type: 'GET',
				data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['label'],
							value: item['msf_variation_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[id="input-msf-variation"]').val('');

			var html = '';
			html += '<li class="list-group-item ui-state-default variation">';
			html += '	<input type="hidden" name="msf_variations[0][id]" value="' + item['value'] + '" class="msf-variation-id" />';
			html += '	<strong class="value-name">' + item['label'] + '</strong>';
			html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
			html += '</li>';

			$('#msf-variations').append(html);
		}
	});

	$('input[id="input-msf-seller-property"]').autocomplete({
		'source': function(request, response) {
			var added_ids = [];
			$.map($('#msf-seller-properties .msf-seller-property-id'), function (input) {
				added_ids.push($(input).val());
			});

			$.ajax({
				url: 'index.php?route=multimerch/field/seller-property/jxAutocompleteMsfSellerProperties',
				type: 'GET',
				data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['label'],
							value: item['msf_seller_property_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[id="input-msf-seller-property"]').val('');

			var html = '';
			html += '<li class="list-group-item ui-state-default seller-property">';
			html += '	<input type="hidden" name="msf_seller_properties[0][id]" value="' + item['value'] + '" class="msf-seller-property-id" />';
			html += '	<strong class="value-name">' + item['label'] + '</strong>';
			html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
			html += '</li>';

			$('#msf-seller-properties').append(html);
		}
	});

	$('input[id="input-mspf-block"]').autocomplete({
		'source': function(request, response) {
			var added_ids_by_type = [];
			$.map($('#mspf-blocks .mspf-block-item'), function (input) {
				added_ids_by_type.push($(input).val());
			});

			$.ajax({
				url: 'index.php?route=module/multimerch_productfilter/jxAutocompleteMspfBlocks',
				type: 'GET',
				data: {
					token: msGlobals.token,
					filter_name: encodeURIComponent(request),
					added_ids_by_type: added_ids_by_type.join(',')
				},
				dataType: 'json',
				success: function (json) {
					response($.map(json, function (item) {
						return {
							category: item.category,
							label: item.label,
							value: item.block_type + '-' + item.id
						}
					}));
				}
			});
		},
		'select': function (item) {
			$('input[id="input-mspf-block"]').val('');

			var html = '';
			html += '<li class="list-group-item ui-state-default mspf-block">';
			html += '	<input type="hidden" name="mspf_blocks[0]" value="' + item.value + '" class="mspf-block-item" />';
			html += '	<strong class="value-name">' + item.category + ': ' + item.label + '</strong>';
			html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
			html += '</li>';

			$('#mspf-blocks').append(html);
		}
	});
})