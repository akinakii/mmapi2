$(function () {

    $('#list-complaints').dataTable({
        "sAjaxSource": "index.php?route=multimerch/complaint/getTableData&token=" + msGlobals.token,
        "aoColumns": [
            {"mData": "product_name", "sClass": "text-left" },
            {"mData": "seller_name", "sClass": "text-left" },
            {"mData": "comment", "bSortable": false, "sClass": "text-left" },
            {"mData": "user_name", "sClass": "text-left" },
            {"mData": "date_added", "sClass": "text-right" },
            {"mData": "actions", "bSortable": false, "sClass": "text-right" }
        ],
        "aaSorting": [[4, 'desc']],
    });
    $(document).on('click', '.ms-delete-complaint', function (e) {

        var complaint_id = $(this).data('id');
        if (confirm('Delete this complaint?')) {
            $.ajax({
                url: 'index.php?route=multimerch/complaint/jxDeleteComplaint&complaint_id=' + complaint_id + '&token=' + msGlobals.token,
                dataType: 'json',
                success: function (json) {
                    if (json.errors) {
                        $('#error-holder').html(json.errors).show();
                    } else {
                        window.location = json.redirect.replace('&amp;', '&');
                    }

                }
            });
        }
    });
});