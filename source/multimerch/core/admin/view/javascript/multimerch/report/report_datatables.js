function initDatatable(id, picker) {
    if ($.fn.DataTable.isDataTable(id)) {
        $(id).dataTable().api().destroy();
    }

	var url = $('base').attr('href');
	var aoColumns = [];
	var aaSorting = [];

	switch(id) {
		case "#list-reports-sales":
			url += "index.php?route=multimerch/report/sales/getTableData&token=" + msGlobals.token;
			aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "order_id", "sClass": "text-left" },
                { "mData": "product", "sClass": "text-left" },
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "tax", "sClass": "text-right" },
                { "mData": "shipping", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
			];
			aaSorting = [[0, 'desc']];
			break;

        case "#list-reports-sales-day":
            url += "index.php?route=multimerch/report/sales-day/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "total_sales", "sClass": "text-right" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "tax", "sClass": "text-right" },
                { "mData": "shipping", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

        case "#list-reports-sales-month":
            url += "index.php?route=multimerch/report/sales-month/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "total_sales", "sClass": "text-right" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "tax", "sClass": "text-right" },
                { "mData": "shipping", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

        case "#list-reports-sales-product":
            url += "index.php?route=multimerch/report/sales-product/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "product", "sClass": "text-left" },
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "total_sales", "sClass": "text-right" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "tax", "sClass": "text-right" },
                { "mData": "shipping", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
            ];
            aaSorting = [[8, 'desc']];
            break;

        case "#list-reports-sales-seller":
            url += "index.php?route=multimerch/report/sales-seller/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "total_sales", "sClass": "text-right" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
            ];
            aaSorting = [[5, 'desc']];
            break;

        case "#list-reports-sales-customer":
            url += "index.php?route=multimerch/report/sales-customer/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "customer", "sClass": "text-left" },
                { "mData": "email", "sClass": "text-left" },
                { "mData": "total_orders", "sClass": "text-right" },
                { "mData": "gross", "sClass": "text-right" },
                { "mData": "net_marketplace", "sClass": "text-right" },
                { "mData": "net_seller", "sClass": "text-right" },
                { "mData": "total", "sClass": "text-right" }
            ];
            aaSorting = [[6, 'desc']];
            break;

        case "#list-reports-finances-transaction":
            url += "index.php?route=multimerch/report/finances-transaction/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "transaction_id", "sClass": "text-left" },
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "description", bSortable: false, "sClass": "text-left" },
                { "mData": "gross", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

        case "#list-reports-finances-seller":
            url += "index.php?route=multimerch/report/finances-seller/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "balance_in", "sClass": "text-right" },
                { "mData": "balance_out", "sClass": "text-right" },
                { "mData": "marketplace_earnings", "sClass": "text-right" },
                { "mData": "seller_earnings", "sClass": "text-right" },
                { "mData": "payments_received", "sClass": "text-right" },
                { "mData": "payouts_paid", "sClass": "text-right" },
                { "mData": "current_balance", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

        case "#list-reports-finances-payment":
            url += "index.php?route=multimerch/report/finances-payment/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "payment_id", "sClass": "text-left" },
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "method", "sClass": "text-left" },
                { "mData": "description", bSortable: false, "sClass": "text-left" },
                { "mData": "gross", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

        case "#list-reports-finances-payout":
            url += "index.php?route=multimerch/report/finances-payout/getTableData&token=" + msGlobals.token;
            aoColumns = [
                { "mData": "date", "sClass": "text-right" },
                { "mData": "invoice_id", "sClass": "text-left" },
                { "mData": "seller", "sClass": "text-left" },
                { "mData": "method", "sClass": "text-left" },
                { "mData": "description", bSortable: false, "sClass": "text-left" },
                { "mData": "gross", "sClass": "text-right" }
            ];
            aaSorting = [[0, 'desc']];
            break;

		default:
			console.error('Specified id does not exist in DOM!');
			break;
	}

	if(picker) {
		url += "&date_start=" + picker.startDate.format('MMMM D, YYYY') + "&date_end=" + picker.endDate.format('MMMM D, YYYY');
	}

	$(id).dataTable({
		"sAjaxSource": url,
		"aoColumns": aoColumns,
		"aaSorting": aaSorting
	});
}