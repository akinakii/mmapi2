$(function() {
	$.fn.dataTableExt.sErrMode = 'throw';

	if (typeof msGlobals.config_language != 'undefined') {
		$.extend($.fn.dataTable.defaults, {
			"oLanguage": {
				"sUrl": msGlobals.config_language
			}
		});
	}

	$.extend($.fn.dataTable.defaults, {
		"bProcessing": true,
		"bSortCellsTop": true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [],
		"bAutoWidth": false,
		"bLengthChange": false,
		"iDisplayLength": parseInt(msGlobals.config_limit_admin),
		"fnDrawCallback": function() {
			var paginator = $("#"+$(this).attr('id')  + '_paginate');
			if (paginator.find('span a').length > 1) {
				// $(this).find('tr.filter').show();
				paginator.show();
			} else {
				// $(this).find('tr.filter').hide();
				paginator.hide();
			}
		}
	});
	
	
	$("body").delegate(".dataTable .filter input[type='text']", "keyup",  function() {
		$(this).parents(".dataTable").dataTable().fnFilter(this.value, $(this).parents(".dataTable").find("thead tr.filter td").index($(this).parent("td")));
	});

	$(document).ready(function() {
		$('.input-date-datepicker').datetimepicker({
			format: 'YYYY-MM-DD',
			pickTime: false,
			useCurrent: false,
			focusOnShow: false,
			showClear: true
		})
		.on('dp.change', function() {
			$(this).trigger('keyup');
		});
		//.attr('readonly', 'readonly');
	});

	// Image Manager
	$(document).on('click', 'a[data-toggle="mm-image"]', function(e) {
		var $element = $(this);
		var $popover = $element.data('bs.popover'); // element has bs popover?

		e.preventDefault();

		// destroy all image popovers
		$('a[data-toggle="image"]').popover('destroy');

		// remove flickering (do not re-add popover when clicking for removal)
		if ($popover) {
			return;
		}

		$element.popover({
			html: true,
			placement: 'right',
			trigger: 'manual',
			content: function() {
				return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
			}
		});

		$element.popover('show');

		$('#button-image').on('click', function() {
			var $button = $(this);
			var $icon   = $button.find('> i');

			$('#modal-image').remove();

			$.ajax({
				url: 'index.php?route=common/filemanager&token=' + getURLVar('token') + '&target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
				dataType: 'html',
				beforeSend: function() {
					$button.prop('disabled', true);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-circle-o-notch fa-spin');
					}
				},
				complete: function() {
					$button.prop('disabled', false);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-pencil');
					}
				},
				success: function(html) {
					$('body').append('<div id="modal-image" class="modal">' + html + '</div>');

					$('#modal-image').modal('show');
				}
			});

			$element.popover('destroy');
		});

		$('#button-clear').on('click', function() {
			$element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

			$element.parent().find('input').val('');

			$element.popover('destroy');
		});
	});

	$(document).on('click', '.ms-delete', function() {
		var $this = $(this);

		// Get request referrer
		var referrer = $this.attr('data-referrer');

		// Delete link
		var url = 'index.php?token=' + msGlobals.token + '&route=';

		// Multiple items selected for deletion
		var selected = [];
		$.map($('input[name="selected[]"]:checked'), function(item) {
			selected.push(parseInt($(item).val()));
		});

		switch (referrer) {
			case 'attribute':
				url += 'multimerch/attribute/jxDeleteAttribute';
				break;

			case 'attribute_group':
				url += 'multimerch/attribute/jxDeleteAttributeGroup';
				break;

			case 'badge':
				url += 'multimerch/badge/delete';
				break;

			case 'category':
				url += 'multimerch/category/jxDeleteCategory';
				break;

			case 'coupon':
				url += 'multimerch/coupon/jxDeleteCoupon';
				break;

			case 'occategory':
				url += 'multimerch/category/jxDeleteOcCategory';
				break;

			case 'conversation':
				url += 'multimerch/conversation/delete';
				break;

			case 'coupon':
				url += 'multimerch/coupon/jxDeleteCoupon';
				break;

			case 'custom_field':
				url += 'multimerch/custom-field/jxDeleteCF';
				break;

			case 'custom_field_group':
				url += 'multimerch/custom-field/jxDeleteCFG';
				break;

			case 'invoice':
				url += 'multimerch/invoice/jxDelete';
				break;

			case 'msf_attribute':
				url += 'multimerch/field/attribute/jxDelete';
				break;

			case 'msf_variation':
				url += 'multimerch/field/variation/jxDelete';
				break;

			case 'msf_seller_property':
				url += 'multimerch/field/seller-property/jxDelete';
				break;

			case 'option':
				url += 'multimerch/option/jxDeleteOption';
				break;

			case 'product':
				url += 'multimerch/product/delete';
				break;

			case 'question':
				url += 'multimerch/question/delete';
				break;

			case 'review':
				url += 'multimerch/review/delete';
				break;

			case 'seller':
				url += 'multimerch/seller/delete';
				break;

			case 'seller_group':
				url += 'multimerch/seller-group/delete';
				break;

			case 'shipping_method':
				url += 'multimerch/shipping-method/delete';
				break;

			case 'social_channel':
				url += 'multimerch/social_link/delete';
				break;

			default:
				alert('Error deleting element(s)!');
				return;
		}

		if(typeof($this.data('id')) != 'undefined') {
			selected = [];
			selected.push($this.data('id'));
		}

		$.ajax({
			type: "post",
			dataType: "json",
			url: 'index.php?route=multimerch/base/jxConfirmDelete&token=' + msGlobals.token + '&referrer=' + encodeURIComponent(referrer),
			data: {selected: selected},
			success: function(confirm_msg) {
				if(confirm(confirm_msg)) {
					$.ajax({
						type: "post",
						dataType: "json",
						url: url,
						data: {selected: selected},
						success: function(json) {
							if(json.redirect) {
								window.location = json.redirect.replace('&amp;', '&');
								window.location.reload();
							} else if (json.errors) {
								var errors_html = '';
								$.map(json.errors, function(item) {
									errors_html += item + '<br/>';
								});

								$('.alert-danger').html(errors_html).show();
							}
						}
					});
				}
			}
		});
	});
});