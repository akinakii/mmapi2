$(function() {
	$('#list-invoices').dataTable( {
		"sAjaxSource": "index.php?route=multimerch/invoice/getTableData&token=" + msGlobals.token,
		"aoColumns": [
			{ "mData": "invoice_id", "sClass": "text-left" },
			{ "mData": "type", "sClass": "text-left" },
			{ "mData": "recipient", "bSortable": false, "sClass": "text-left" },
			{ "mData": "total", "sClass": "text-right" },
			{ "mData": "title", "bSortable": false, "sClass": "text-left" },
			{ "mData": "date_generated", "sClass": "text-right" },
			{ "mData": "status", "sClass": "text-left" },
			{ "mData": "payment_id", "sClass": "text-left" }
		],
		"aaSorting":  [[5,'desc']]
	});
});