$(function(){
    // Show only selected language's fields
    var lang_inputs = $('.lang-select-field');
    var current_language = msGlobals.current_language;
    for(var i = 0; i < lang_inputs.length; i++) {
        if($(lang_inputs[i]).data('lang') != current_language) {
            $(lang_inputs[i]).hide();
            $(lang_inputs[i]).siblings('.lang-img-icon-input').hide();
        } else {
            $(lang_inputs[i]).show();
            $(lang_inputs[i]).siblings('.lang-img-icon-input').show();
        }
    }

    // Language select
    $(".select-input-lang").on("click", function() {
        var selectedLang = $(this).data('lang');

        $('.lang-select-field').each(function() {
            if ($(this).data('lang') == selectedLang) {
                $(this).show();
                $(this).siblings('.lang-img-icon-input').show();
            } else {
                $(this).hide();
                $(this).siblings('.lang-img-icon-input').hide();
            }
        });

        $('.lang-chooser img').each(function() {
            if ($(this).data('lang') == selectedLang) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    });

    // Category parent
    $('input[name="path"]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=multimerch/category/jxAutocompleteOcCategories&token=' + msGlobals.token + '&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    json.unshift({
                        category_id: 0,
                        name: msGlobals.text_none
                    });

                    response($.map(json, function(item) {
                        if(item['category_id'] != $('input[name="category_id"]').val()) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name="path"]').val(item['label']);
            $('input[name="parent_id"]').val(item['value']);
        }
    });

    // Category filters
    $('input[name="filter"]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=multimerch/category/jxAutocompleteFilters&token=' + msGlobals.token + '&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['filter_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name="filter"]').val('');
            $('#category-filter' + item['value']).remove();

            var filter_row_html = '';
            filter_row_html += '<div id="category-filter' + item['value'] + '">';
            filter_row_html += '	<i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" />';
            filter_row_html += '</div>';

            $('#category-filter').append(filter_row_html);
        }
    });

    // Remove category filter
    $('#category-filter').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

    $('#ms-submit-button').click(function() {
        var button = $(this);

        $.ajax({
            type: 'post',
            url: 'index.php?route=multimerch/category/jxSaveOcCategory&token=' + msGlobals.token,
            data: $('#ms-category-form').serialize(),
            dataType: 'json',
            success: function(json) {
                if(json.errors) {
                    $('#error-holder').empty();
                    $('#ms-category-form').find('.text-danger').remove();
                    $('#ms-category-form').find('div.has-error').removeClass('has-error');

                    for (error in json.errors) {
                        if ($('[name^="' + error + '"]').length > 0) {
                            $('[name^="' + error + '"]').closest('div').addClass('has-error');
                            $('[name^="' + error + '"]').parents('div:first').append("<div class='text-danger' id='error_" + error + "'>" + json.errors[error] + "</div>");
                        }

                        $('#error-holder').append(json.errors[error] + '<BR>').show();
                    }

                    window.scrollTo(0,0);
                } else {
                    window.location = json.redirect.replace('&amp;', '&');
                }
            }
        });
    })

    // Commissions
    $(document).on('click', '.override-fees', function (e) {
        e.preventDefault();

        var override = parseInt($(this).closest('div').attr('data-override'));

        if (override == 1) {
            $('#commissions-rates').addClass('hidden');
            $('#commissions-rates input, #commissions-rates select').attr('disabled', true);
            $('.override-note[data-override="0"]').removeClass('hidden');
            $('.override-note[data-override="1"]').addClass('hidden');
        } else {
            $('#commissions-rates').removeClass('hidden');
            $('#commissions-rates input, #commissions-rates select').attr('disabled', false);
            $('.override-note[data-override="0"]').addClass('hidden');
            $('.override-note[data-override="1"]').removeClass('hidden');
        }
    });

    function getOcCategoryCommission() {
        // If it is a new category, check if the parent has been selected. Else, take existing category id.
        var category_id = $('input[name="category_id"]').val();
        var parent_id = $('input[name="parent_id"]').val();

        $.ajax({
            url: 'index.php?route=multimerch/category/jxGetOcCategoryCommission',
            type: 'GET',
            data: {token: msGlobals.token, category_id: category_id, parent_id: parent_id},
            dataType: 'json',
            success: function (json) {
                if (json.success) {
                    $('#oc-category-fees').html(json.html);
                } else {
                    console.error(json.errors);
                }

            }
        });
    }

    getOcCategoryCommission();
    $(document).on('click', '#input-parent ~ ul li', getOcCategoryCommission);
    // End Commissions

    // Start MM product attributes
    function jxGetMsfAttributes(action) {
        var action = action || 'initial';

        var category_id = $('input[name="category_id"]').val();
        var parent_id = $('input[name="parent_id"]').val();

        $.ajax({
            url: 'index.php?route=multimerch/category/jxGetOcCategoryMsfAttributes',
            type: 'GET',
            data: {token: msGlobals.token, category_id: category_id, parent_id: parent_id, action: action},
            dataType: 'json',
            success: function (json) {
                if (json.html) {
                    $('#oc-category-msf-attributes').html(json.html);
                }

                $('input[id="input-msf-attribute"]').autocomplete({
                    'source': function(request, response) {
                        var added_ids = [];
                        $.map($('#msf-attributes .msf-attribute-id'), function (input) {
                            added_ids.push($(input).val());
                        });

                        $.ajax({
                            url: 'index.php?route=multimerch/category/jxAutocompleteMsfAttributes',
                            type: 'GET',
                            data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
                            dataType: 'json',
                            success: function(json) {
                                response($.map(json, function(item) {
                                    return {
                                        label: item['label'],
                                        value: item['msf_attribute_id']
                                    }
                                }));
                            }
                        });
                    },
                    'select': function(item) {
                        $('input[id="input-msf-attribute"]').val('');

                        var html = '';
                        html += '<li class="list-group-item ui-state-default attribute">';
                        html += '	<input type="hidden" name="msf_attributes[]" value="' + item['value'] + '" class="msf-attribute-id" />';
                        html += '	<strong class="value-name">' + item['label'] + '</strong>';
                        html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
                        html += '</li>';

                        $('#msf-attributes').append(html);
                    }
                });
            }
        });
    }

    $(document).on('click', '#msf_attributes .override', function () {
        $('input[name="msf_attributes_overridden"]').val(1);
        jxGetMsfAttributes('override');
    });

    $(document).on('click', '#msf_attributes .remove-override', function () {
        $('input[name="msf_attributes_overridden"]').val(0);
        jxGetMsfAttributes('remove_override');
    });

    $(document).on('click', '#input-parent ~ ul li', function () {
        var is_overridden = parseInt($('input[name="msf_attributes_overridden"]').val());

        if (!is_overridden) {
            jxGetMsfAttributes('parent_changed');
        }
    });

    jxGetMsfAttributes();
    // End MM product attributes

    // Start MM product variations
    function jxGetMsfVariations(action) {
        var action = action || 'initial';

        var category_id = $('input[name="category_id"]').val();
        var parent_id = $('input[name="parent_id"]').val();

        $.ajax({
            url: 'index.php?route=multimerch/category/jxGetOcCategoryMsfVariations',
            type: 'GET',
            data: {token: msGlobals.token, category_id: category_id, parent_id: parent_id, action: action},
            dataType: 'json',
            success: function (json) {
                if (json.html) {
                    $('#oc-category-msf-variations').html(json.html);
                }

                $('input[id="input-msf-variation"]').autocomplete({
                    'source': function(request, response) {
                        var added_ids = [];
                        $.map($('#msf-variations .msf-variation-id'), function (input) {
                            added_ids.push($(input).val());
                        });

                        $.ajax({
                            url: 'index.php?route=multimerch/category/jxAutocompleteMsfVariations',
                            type: 'GET',
                            data: {token: msGlobals.token, filter_name: encodeURIComponent(request), added_ids: added_ids.join(',')},
                            dataType: 'json',
                            success: function(json) {
                                response($.map(json, function(item) {
                                    return {
                                        label: item['label'],
                                        value: item['msf_variation_id']
                                    }
                                }));
                            }
                        });
                    },
                    'select': function(item) {
                        $('input[id="input-msf-variation"]').val('');

                        var html = '';
                        html += '<li class="list-group-item ui-state-default variation">';
                        html += '	<input type="hidden" name="msf_variations[]" value="' + item['value'] + '" class="msf-variation-id" />';
                        html += '	<strong class="value-name">' + item['label'] + '</strong>';
                        html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
                        html += '</li>';

                        $('#msf-variations').append(html);
                    }
                });
            }
        });
    }

    $(document).on('click', '#msf_variations .override', function () {
        $('input[name="msf_variations_overridden"]').val(1);
        jxGetMsfVariations('override');
    });

    $(document).on('click', '#msf_variations .remove-override', function () {
        $('input[name="msf_variations_overridden"]').val(0);
        jxGetMsfVariations('remove_override');
    });

    $(document).on('click', '#input-parent ~ ul li', function () {
        var is_overridden = parseInt($('input[name="msf_variations_overridden"]').val());

        if (!is_overridden) {
            jxGetMsfVariations('parent_changed');
        }
    });

    jxGetMsfVariations();
    // End MM product variations

    // Start MM product filter blocks
    function jxGetMspfBlocks(action) {
        var action = action || 'initial';

        var category_id = $('input[name="category_id"]').val();
        var parent_id = $('input[name="parent_id"]').val();

        $.ajax({
            url: 'index.php?route=multimerch/category/jxGetOcCategoryMspfBlocks',
            type: 'GET',
            data: {token: msGlobals.token, category_id: category_id, parent_id: parent_id, action: action},
            dataType: 'json',
            success: function (json) {
                if (json.html) {
                    $('#oc-category-mspf-blocks').html(json.html);
                }

                $('input[id="input-mspf-block"]').autocomplete({
                    'source': function(request, response) {
                        var added_ids_by_type = [];
                        $.map($('#mspf-blocks .mspf-block-item'), function (input) {
                            added_ids_by_type.push($(input).val());
                        });

                        $.ajax({
                            url: 'index.php?route=module/multimerch_productfilter/jxAutocompleteMspfBlocks',
                            type: 'GET',
                            data: {
                                token: msGlobals.token,
                                filter_name: encodeURIComponent(request),
                                added_ids_by_type: added_ids_by_type.join(',')
                            },
                            dataType: 'json',
                            success: function (json) {
                                response($.map(json, function (item) {
                                    return {
                                        category: item.category,
                                        label: item.label,
                                        value: item.block_type + '-' + item.id
                                    }
                                }));
                            }
                        });
                    },
                    'select': function(item) {
                        $('input[id="input-mspf-block"]').val('');

                        var html = '';
                        html += '<li class="list-group-item ui-state-default mspf-block">';
                        html += '	<input type="hidden" name="mspf_blocks[]" value="' + item.value + '" class="mspf-block-item" />';
                        html += '	<strong class="value-name">' + item.category + ': ' + item.label + '</strong>';
                        html += '	<a class="icon-remove ms-remove bottom-dotted-link" title="' + msGlobals.texts.ms_remove + '">' + msGlobals.texts.ms_remove + '</a>';
                        html += '</li>';

                        $('#mspf-blocks').append(html);
                    }
                });
            }
        });
    }

    $(document).on('click', '#mspf_blocks .override', function () {
        $('input[name="mspf_blocks_overridden"]').val(1);
        jxGetMspfBlocks('override');
    });

    $(document).on('click', '#mspf_blocks .remove-override', function () {
        $('input[name="mspf_blocks_overridden"]').val(0);
        jxGetMspfBlocks('remove_override');
    });

    $(document).on('click', '#input-parent ~ ul li', function () {
        var is_overridden = parseInt($('input[name="mspf_blocks_overridden"]').val());

        if (!is_overridden) {
            jxGetMspfBlocks('parent_changed');
        }
    });

    jxGetMspfBlocks();
    // End MM product filter blocks
});