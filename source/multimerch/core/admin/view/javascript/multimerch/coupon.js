$(function() {

	$('#list-coupons').dataTable({
		"sAjaxSource": "index.php?route=multimerch/coupon/getCouponTableData&token=" + msGlobals.token,
		"aoColumns": [
			{ "mData": "checkbox", "bSortable": false },
			{ "mData": "date_created", "sClass": "text-right" },
			{ "mData": "name", "sClass": "text-left" },
			{ "mData": "code", "sClass": "text-left" },
			{ "mData": "seller", "sClass": "text-left" },
			{ "mData": "value", "sClass": "text-right" },
			{ "mData": "total_uses", "sClass": "text-center" },
			{ "mData": "date_start", "sClass": "text-right" },
			{ "mData": "date_end", "sClass": "text-right" },
			{ "mData": "status", "sClass": "text-left" },
			{ "mData": "actions", "bSortable": false, "sClass": "text-right" }
		]
	});

	$(document).on('click', '#list-coupons input:checkbox', function() {
		var selected_coupons = $('#list-coupons').children('tbody').find('input:checkbox:checked');
		if(selected_coupons.length > 0) {
			$('#ms-coupon-delete').show();
		} else {
			$('#ms-coupon-delete').hide();
		}
	});
});