<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $this->url->link('multimerch/field/attribute/create', 'token=' . $token, true); ?>" id="ms-create" data-toggle="tooltip" title="<?php echo $ms_field_attribute_create; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<?php if (!empty($msf_attributes)) { ?>
					<a href="<?php echo $this->url->link('multimerch/field/default-configuration', 'token=' . $token, true); ?>" data-toggle="tooltip" title="<?php echo $ms_field_default_configuration_attribute; ?>" class="btn btn-success"><i class="fa fa-cog"></i></a>
				<?php } ?>
				<a style="display: none;" id="ms-delete" data-toggle="tooltip" title="<?php echo $ms_delete; ?>" class="btn btn-danger ms-delete" data-referrer="msf_attribute"><i class="fa fa-trash-o"></i></a>
			</div>

			<h1><?php echo $ms_field_attribute_heading; ?></h1>

			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="alert alert-danger" style="display: <?php echo !empty($error_warning) ? 'block' : 'none'; ?>;"><i class="fa fa-exclamation-circle"></i><?php if (!empty($error_warning)) { echo $error_warning; } ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<?php if (!empty($success)) { ?>
			<div class="alert alert-success" style="display: <?php echo !empty($success) ? 'block' : 'none'; ?>;"><i class="fa fa-check-circle"></i> <?php if(!empty($success)) { echo $success; } ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>

		<div class="panel panel-default">
			<div class="panel-body tab-content">
				<div class="tab-pane active" id="tab-msf-attribute">
					<div class="table-responsive">
						<form class="form-inline" action="" method="post" enctype="multipart/form-data" id="form-msf-attributes">
							<table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-msf-attributes">
								<thead>
									<tr>
										<td class="text-left large"><?php echo $ms_field_attribute_name; ?></td>
										<td class="text-left small"><?php echo $ms_field_attribute_type; ?></td>
										<td class="text-left large"><?php echo $ms_field_attribute_values; ?></td>
										<td class="text-left large"><?php echo $ms_field_attribute_categories; ?></td>
										<td class="text-left small" id="status_column"><?php echo $ms_status; ?></td>
										<td class="text-right small"><?php echo $ms_action; ?></td>
									</tr>
									<tr class="filter">
										<td><input type="text"/></td>
										<td></td>
										<td></td>
										<td></td>
										<td class="text-left">
											<select id="status_select">
												<option></option>
												<?php foreach ($statuses as $status) { ?>
													<option value="<?php echo $status; ?>"><?php echo $this->language->get('ms_field_attribute_status_' . $status); ?></option>
												<?php } ?>
											</select>
										</td>
										<td></td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var msGlobals = {
		token: '<?php echo $token; ?>',
		ms_field_attribute_create: '<?php echo $ms_field_attribute_create; ?>',
		ms_field_attribute_confirm_delete: '<?php echo $ms_field_attribute_confirm_delete; ?>',
		ms_field_attribute_error_not_selected: '<?php echo $ms_field_attribute_error_not_selected; ?>',
	};
</script>
<?php echo $footer; ?>