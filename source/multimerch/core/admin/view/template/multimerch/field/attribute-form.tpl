<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="ms-field-attribute-form">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button id="ms-submit-button" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $this->url->link('multimerch/field/attribute', 'token=' . $this->session->data['token']); ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>

			<h1><?php echo $heading; ?></h1>

			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div style="display: none" class="alert alert-danger" id="error-holder"><i class="fa fa-exclamation-circle"></i>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading; ?></h3>

				<div class="lang-chooser pull-right">
					<?php foreach ($languages as $language) { ?>
						<img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_admin_language')) ? 'active default' : ''; ?>" data-lang="<?php echo $language['code']; ?>" data-lang_id="<?php echo $language['language_id']; ?>">
					<?php } ?>
				</div>
			</div>

			<div class="panel-body">
				<form id="msf-attribute-form" class="form-horizontal">
					<input type="hidden" name="msf_attribute_id" value="<?php echo $msf_attribute['id']; ?>" />

					<fieldset class="ms-fieldset">
						<legend><?php echo $ms_field_attribute_general; ?></legend>

						<div class="form-group required">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_name; ?></label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
									<div class="lang-field-holder">
										<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
										<input type="text" name="description[<?php echo $language['language_id']; ?>][name]" class="lang-select-field form-control" data-lang="<?php echo $language['code']; ?>" value="<?php echo $msf_attribute['description'][$language['language_id']]['name']; ?>" />
									</div>
								<?php } ?>
								<div class="comment"><?php echo $ms_field_attribute_name_note; ?></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_note; ?></label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
									<div class="lang-field-holder">
										<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
										<input type="text" name="description[<?php echo $language['language_id']; ?>][note]" class="lang-select-field form-control" data-lang="<?php echo $language['code']; ?>" value="<?php echo $msf_attribute['description'][$language['language_id']]['note']; ?>" />
									</div>
								<?php } ?>
								<div class="comment"><?php echo $ms_field_attribute_note_note; ?></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_type; ?></label>
							<div class="col-sm-10">
								<select class="form-control" name="type">
									<?php foreach ($types as $type) { ?>
									<option value="<?php echo $type; ?>" <?php echo (string)$type === (string)$msf_attribute['type'] ? 'selected="selected"' : ''; ?>><?php echo $this->language->get('ms_type_' . $type); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-group" id="values-block">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_values; ?></label>
							<div class="col-sm-10">
								<ul id="msf-attribute-values" class="list-group">
									<?php if (!empty($msf_attribute['values'])) { ?>
										<?php foreach ($msf_attribute['values'] as $key => $value) { ?>
											<li class="list-group-item ui-state-default value">
												<div class="value-title">
													<?php foreach ($languages as $language) { ?>
														<div class="lang-field-holder">
															<strong class="value-name lang-select-field" data-lang="<?php echo $language['code']; ?>"><?php echo !empty($value['description'][$language['language_id']]['name']) ? $value['description'][$language['language_id']]['name'] : ('<i>' . $ms_field_attribute_no_name . '</i>'); ?></strong>
														</div>
													<?php } ?>

													<span class="toggle-value-info"><i class="fa fa-sort-down"></i></span>
												</div>

												<div class="value-info hidden">
													<input type="hidden" name="values[<?php echo $key + 1; ?>][id]" value="<?php echo $value['id']; ?>" />

													<div class="row">
														<div class="col-sm-12">
															<?php echo $ms_field_attribute_name; ?>
														</div>
														<div class="col-sm-12">
															<?php foreach ($languages as $language) { ?>
																<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
																<div class="lang-field-holder">
																	<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
																	<input type="text" name="values[<?php echo $key + 1; ?>][description][<?php echo $language['language_id']; ?>][name]" class="lang-select-field form-control" data-lang="<?php echo $language['code']; ?>" value="<?php echo isset($value['description'][$language['language_id']]['name']) ? $value['description'][$language['language_id']]['name'] : ''; ?>" />
																</div>
															<?php } ?>
														</div>
													</div>

													<div class="row actions">
														<div class="col-sm-12 text-right">
															<a class="icon-remove ms-remove-value bottom-dotted-link" title="<?php echo $ms_delete; ?>"><?php echo $ms_remove; ?></a>
														</div>
													</div>
												</div>
											</li>
										<?php } ?>
									<?php } ?>

									<li class="list-group-item value-creator">
										<strong><?php echo $ms_field_attribute_btn_create_value_long; ?></strong>

										<div class="value-creator-content">
											<div class="row">
												<div class="col-sm-12">
													<?php echo $ms_field_attribute_name; ?>
												</div>
												<div class="col-sm-12">
													<?php foreach ($languages as $language) { ?>
													<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
													<div class="lang-field-holder">
														<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
														<input type="text" name="values[0][description][<?php echo $language['language_id']; ?>][name]" class="lang-select-field form-control" data-lang="<?php echo $language['code']; ?>" />
													</div>
													<?php } ?>
												</div>
											</div>
										</div>

										<div class="row actions">
											<div class="col-sm-12 text-left">
												<button class="btn btn-default create-value"><?php echo $ms_field_attribute_btn_create_value; ?></button>
											</div>
										</div>
									</li>
								</ul>
								<div class="comment"><?php echo $ms_field_attribute_values_note; ?></div>
							</div>
						</div>

						<div class="form-group" id="required">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_required; ?></label>
							<div class="col-sm-10">
								<input type="hidden" name="required" value="0" />
								<input type="checkbox" name="required" value="1" <?php if ($msf_attribute['required']) { ?>checked="checked"<?php } ?> />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_label; ?></label>
							<div class="col-sm-10">
								<?php foreach ($languages as $language) { ?>
									<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
									<div class="lang-field-holder">
										<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
										<input type="text" name="description[<?php echo $language['language_id']; ?>][label]" class="lang-select-field form-control" data-lang="<?php echo $language['code']; ?>" value="<?php echo $msf_attribute['description'][$language['language_id']]['label']; ?>" />
									</div>
								<?php } ?>
								<div class="comment"><?php echo $ms_field_attribute_label_note; ?></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $ms_field_attribute_status; ?></label>
							<div class="col-sm-10">
								<select name="status" class="form-control">
									<?php foreach ($statuses as $status) { ?>
										<option value="<?php echo $status; ?>" <?php echo $msf_attribute && (int)$status === (int)$msf_attribute['status'] ? 'selected="selected"' : ''; ?>><?php echo $this->language->get('ms_field_attribute_status_' . $status); ?></option>
									<?php } ?>
								</select>
								<div class="comment"><?php echo $ms_field_attribute_status_note; ?></div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<script>
		var msGlobals = {
			token : "<?php echo $this->session->data['token']; ?>",
			current_language: "<?php echo $this->config->get('config_admin_language') ;?>",
			texts: {
				ms_field_attribute_no_name: "<?php echo $ms_field_attribute_no_name; ?>",
				ms_remove: "<?php echo $ms_remove; ?>"
			}
		};
	</script>
</div>
<?php echo $footer; ?>