<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="ms-field-default-configuration">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button id="ms-submit-button" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			</div>

			<h1><?php echo $heading; ?></h1>

			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div style="display: none" class="alert alert-danger" id="error-holder"><i class="fa fa-exclamation-circle"></i>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<form id="fields-form" class="form-horizontal">
					<?php if ($this->config->get('msconf_msf_attribute_enabled')) { ?>
						<fieldset class="ms-fieldset">
							<legend><?php echo $ms_field_default_configuration_msf_attribute_title; ?></legend>

							<input type="text" value="" placeholder="<?php echo $ms_field_default_configuration_msf_attribute_subtitle; ?>" id="input-msf-attribute" class="form-control" />

							<div class="row" style="margin-top: 10px;">
								<div class="col-sm-12">
									<ul id="msf-attributes" class="list-group">
										<?php if (!empty($msf_attributes)) { ?>
											<?php foreach ($msf_attributes as $key => $msf_attribute) { ?>
												<li class="list-group-item ui-state-default attribute">
													<input type="hidden" name="msf_attributes[<?php echo $key + 1; ?>][id]" value="<?php echo $msf_attribute['id']; ?>" class="msf-attribute-id" />
													<strong class="value-name"><?php echo $msf_attribute['full_name']; ?></strong>
													<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
												</li>
											<?php } ?>
										<?php } ?>
									</ul>
								</div>
							</div>
						</fieldset>
					<?php } ?>

					<?php if ($this->config->get('msconf_msf_variation_enabled')) { ?>
						<fieldset class="ms-fieldset">
							<legend><?php echo $ms_field_default_configuration_msf_variation_heading; ?></legend>

							<input type="text" value="" placeholder="<?php echo $ms_field_default_configuration_msf_variation_note; ?>" id="input-msf-variation" class="form-control" />

							<div class="row" style="margin-top: 10px;">
								<div class="col-sm-12">
									<ul id="msf-variations" class="list-group">
										<?php if (!empty($msf_variations)) { ?>
											<?php foreach ($msf_variations as $key => $msf_variation) { ?>
												<li class="list-group-item ui-state-default variation">
													<input type="hidden" name="msf_variations[<?php echo $key + 1; ?>][id]" value="<?php echo $msf_variation['id']; ?>" class="msf-variation-id" />
													<strong class="value-name"><?php echo $msf_variation['full_name']; ?></strong>
													<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
												</li>
											<?php } ?>
										<?php } ?>
									</ul>
								</div>
							</div>
						</fieldset>
					<?php } ?>

					<?php if ($this->config->get('msconf_msf_seller_property_enabled')) { ?>
						<fieldset class="ms-fieldset">
							<legend><?php echo $ms_field_default_configuration_msf_seller_property_title; ?></legend>

							<input type="text" value="" placeholder="<?php echo $ms_field_default_configuration_msf_seller_property_subtitle; ?>" id="input-msf-seller-property" class="form-control" />

							<div class="row" style="margin-top: 10px;">
								<div class="col-sm-12">
									<ul id="msf-seller-properties" class="list-group">
										<?php if (!empty($msf_seller_properties)) { ?>
											<?php foreach ($msf_seller_properties as $key => $msf_seller_property) { ?>
												<li class="list-group-item ui-state-default seller-property">
													<input type="hidden" name="msf_seller_properties[<?php echo $key + 1; ?>][id]" value="<?php echo $msf_seller_property['id']; ?>" class="msf-seller-property-id" />
													<strong class="value-name"><?php echo $msf_seller_property['full_name']; ?></strong>
													<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
												</li>
											<?php } ?>
										<?php } ?>
									</ul>
								</div>
							</div>
						</fieldset>
					<?php } ?>

					<?php if (!empty($mspf_blocks_enabled)) { ?>
						<fieldset class="ms-fieldset">
							<legend><?php echo $ms_field_default_configuration_mspf_block_title; ?></legend>

							<input type="text" value="" placeholder="<?php echo $ms_field_default_configuration_mspf_block_subtitle; ?>" id="input-mspf-block" class="form-control" />

							<div class="row" style="margin-top: 10px;">
								<div class="col-sm-12">
									<ul id="mspf-blocks" class="list-group">
										<?php if (!empty($mspf_blocks)) { ?>
											<?php foreach ($mspf_blocks as $key => $mspf_block) { ?>
												<li class="list-group-item ui-state-default mspf-block">
													<input type="hidden" name="mspf_blocks[<?php echo $key + 1; ?>]" value="<?php echo $mspf_block['value']; ?>" class="mspf-block-item" />
													<strong class="value-name"><?php echo $mspf_block['category'] . ': ' . $mspf_block['label']; ?></strong>
													<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
												</li>
											<?php } ?>
										<?php } ?>
									</ul>
								</div>
							</div>
						</fieldset>
					<?php } ?>
				</form>
			</div>
		</div>
	</div>

	<script>
		var msGlobals = {
			token : "<?php echo $this->session->data['token']; ?>",
			current_language: "<?php echo $this->config->get('config_admin_language') ;?>",
			texts: {
				ms_remove: "<?php echo $ms_remove; ?>"
			}
		};
	</script>
</div>
<?php echo $footer; ?>