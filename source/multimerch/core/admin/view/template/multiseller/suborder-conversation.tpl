<?php if ($this->config->get('mmess_conf_enable') && !empty($conversation)) { ?>
	<hr>

	<h4><i class="fa fa-comments"></i> <?php echo $ms_account_conversations_with_seller; ?></h4>

	<div>
		<div class="ms-messages">
			<?php foreach ($messages as $message) { ?>
				<div class="row ms-message <?php echo $message['sender_type_id'] == MsConversation::SENDER_TYPE_ADMIN ? 'admin' : ($message['sender_type_id'] == MsConversation::SENDER_TYPE_SELLER ? 'seller' : ''); ?>">
					<div class="col-sm-12 ms-message-body">
						<div class="title">
							<?php echo ucwords($message['sender']); ?>
							<span class="date"><?php echo $message['date_created']; ?></span>
						</div>

						<div class="body">
							<?php echo nl2br($message['message']); ?>
						</div>

						<?php if(!empty($message['attachments'])) { ?>
							<div class="attachments">
								<?php foreach($message['attachments'] as $attachment) { ?>
									<a href="<?php echo $this->url->link('multimerch/conversation/downloadAttachment', 'token=' . $this->session->data['token'] . '&code=' . $attachment['code'], true); ?>"><i class="fa fa-file-o" aria-hidden="true"></i> <?php echo $attachment['name']; ?></a>
									<br/>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>

		<div class="row ms-message-form">
			<form id="ms-message-form<?php echo $suborder_id; ?>" class="ms-form form-horizontal">
				<input type="hidden" name="conversation_id" value="<?php echo $conversation['conversation_id']; ?>" />

				<div class="col-sm-9">
					<textarea class="form-control ms-message-text" rows="5" cols="50" name="ms-message-text" placeholder="<?php echo $ms_account_conversations_textarea_placeholder; ?>"></textarea>
					<div class="list">
						<ul class="attachments"></ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="buttons text-center">
						<button type="button" class="btn btn-default ms-message-upload" data-suborder_id="<?php echo $suborder_id; ?>" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
						<button data-suborder_id="<?php echo $suborder_id; ?>" type="button" class="btn btn-primary ms-order-message" data-suborder_id="<?php echo $suborder_id; ?>"><?php echo $ms_post_message; ?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php } ?>