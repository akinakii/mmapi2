<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>

		<div class="error-holder"></div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading; ?></h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<form class="form-inline" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
						<table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-invoices">
							<thead>
								<tr>
									<td class="text-left small"><?php echo $ms_invoice_column_invoice_id; ?></td>
									<td class="text-left small"><?php echo $ms_invoice_column_type; ?></td>
									<td class="text-left large"><?php echo $ms_invoice_column_recipient; ?></td>
									<td class="text-right medium"><?php echo $ms_invoice_column_total; ?></td>
									<td class="text-left"><?php echo $ms_invoice_column_title; ?></td>
									<td class="text-right medium"><?php echo $ms_invoice_column_date_generated; ?></td>
									<td class="text-right small"><?php echo $ms_invoice_column_status; ?></td>
									<td class="text-left medium"><?php echo $ms_invoice_column_payment_id; ?></td>
								</tr>
								<tr class="filter">
									<td><input type="text"/></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td><input type="text" class="input-date-datepicker"/></td>
									<td></td>
									<td><input type="text"/></td>
								</tr>
							</thead>

							<tbody></tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var msGlobals = {
		token: '<?php echo $token; ?>'
	};
</script>
<?php echo $footer; ?>