<div class="msf-seller-properties-holder">
	<?php foreach ($msf_seller_properties['default'] as $msf_seller_property) { ?>
		<div class="form-group <?php if ($msf_seller_property['required']) { ?>required<?php } ?> <?php if (isset($errors[$msf_seller_property['id']])) { ?>has-error<?php } ?>">
			<label class="col-sm-2 control-label"><?php echo $msf_seller_property['name']; ?></label>
			<div class="col-sm-10">
				<?php if ('select' === $msf_seller_property['type']) { ?>
					<select name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" class="form-control">
						<?php foreach ($msf_seller_property['values'] as $value) { ?>
							<option value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>selected="selected"<?php } ?>><?php echo $value['name']; ?></option>
						<?php } ?>
					</select>
				<?php } elseif ('checkbox' === $msf_seller_property['type']) { ?>
					<div class="well well-sm" style="height: 150px; overflow: auto;">
						<?php foreach ($msf_seller_property['values'] as $value) { ?>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][id_values][]" value="<?php echo $value['id']; ?>" <?php if (!empty($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']) && in_array($value['id'], array_keys($msf_seller_properties['existing'][$msf_seller_property['id']]['id_values']))) { ?>checked="checked"<?php } ?> />
									<?php echo $value['name'] ?: ''; ?>
								</label>
							</div>
						<?php } ?>
					</div>
				<?php } elseif ('text' === $msf_seller_property['type']) { ?>
					<?php foreach ($languages as $language) { ?>
						<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
						<div class="lang-text-input">
							<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
							<input type="text" name="msf_seller_properties[<?php echo $msf_seller_property['id']; ?>][text_values][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']]) ? $msf_seller_properties['existing'][$msf_seller_property['id']]['text_values'][$language['language_id']] : ''; ?>" class="form-control" />
						</div>
					<?php } ?>
				<?php } ?>

				<?php if (!empty($msf_seller_property['description'][$this->config->get('config_language_id')]['note'])) { ?>
					<div class="comment" style="font-style: italic;">
						<?php echo $msf_seller_property['description'][$this->config->get('config_language_id')]['note']; ?>
					</div>
				<?php } elseif (!empty($msf_seller_property['note'])) { ?>
					<div class="comment" style="font-style: italic;">
						<?php echo $msf_seller_property['note']; ?>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>