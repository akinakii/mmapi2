<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<td class="text-left"><?php echo $ms_report_column_date; ?></td>
				<td class="text-left"><?php echo $column_comment; ?></td>
				<td class="text-left"><?php echo $column_status; ?></td>
			</tr>
		</thead>
		<tbody>
			<?php if ($histories) { ?>
				<?php foreach ($histories as $history) { ?>
					<tr>
						<td class="text-left"><?php echo $history['date_added']; ?></td>
						<td class="text-left" style="word-break: break-all;"><?php echo nl2br($history['comment']); ?></td>
						<td class="text-left"><?php echo $history['status']; ?></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>