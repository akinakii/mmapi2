<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $ms_notification_heading; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="list table table-bordered table-hover" style="text-align: center" id="list-notifications">
						<thead>
							<tr>
								<td class="tiny"></td>
								<td><?php echo $ms_notification_message; ?></td>
								<td class="large"><?php echo $ms_date_created; ?></td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#list-notifications').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/notification/getTableData&token=<?php echo $token; ?>",
			"aoColumns": [
				{ "mData": "avatar", "bSortable": false },
				{ "mData": "message", "bSortable": false, "sClass": "text-left" },
				{ "mData": "date_created" }
			],
			"aaSorting":  [[2,'desc']],
			"fnDrawCallback": function ( oSettings ) {
				$(oSettings.nTHead).hide();
			}
		});
	});
</script>
<?php echo $footer; ?>