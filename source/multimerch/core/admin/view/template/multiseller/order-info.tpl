<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="ms-dashboard ms-order-info">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $this->language->get('ms_order_column_order_id') . $order_id; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div style="display: none" class="alert alert-danger" id="error-holder"><i class="fa fa-exclamation-circle"></i>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="row">
					<div class="col-md-12">
						<div class="ms-dashboard-widget">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#combined_info" data-toggle="tab"><?php echo $ms_order_column_order_id . $order_id; ?></a></li>
								<?php foreach ($suborders as $suborder) { ?>
									<li><a href="#suborder_<?php echo $suborder['suborder_id']; ?>" data-toggle="tab"><?php echo $suborder['seller'] . ' (#' . $order_id . '-' . $suborder['suborder_id'] . ')'; ?></a></li>
								<?php } ?>
							</ul>

							<div class="tab-content">
								<div class="tab-pane active" id="combined_info">
									<h4><i class="fa fa-shopping-cart"></i> <?php echo $ms_order_column_order_id . $order_id; ?>
										<a href="<?php echo $link_print_invoice; ?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-print fa-fw"></i> <?php echo $ms_print_order_invoice; ?></a>
									</h4>
									<p><?php echo sprintf($ms_order_order_date, $order['date_added']); ?></p>
									<p><?php echo sprintf($ms_order_payment_method, $payment['method']); ?></p>
									<p><?php echo sprintf($ms_order_payment_status, $payment['status']); ?></p>

									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<td class="text-center"></td>
													<td class="text-left"><?php echo $column_product; ?></td>
													<td class="text-left"><?php echo $ms_order_sold_by; ?></td>
													<td class="text-right"><?php echo $column_price; ?></td>
													<?php if ($mm_shipping_flag) { ?>
													<td class="text-right"><?php echo $ms_sale_order_shipping_cost; ?></td>
													<?php } ?>
													<td class="text-right"><?php echo $column_total; ?></td>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($products as $product) { ?>
													<tr>
														<td class="text-center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
														<td class="text-left"><?php echo $product['quantity'] > 1 ? $product['quantity'] . ' x ' : ''; ?><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
															<?php foreach ($product['msf_variation'] as $msf_variation) { ?>
															<br />
															&nbsp;<small> - <?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
															<?php } ?>

															<?php foreach ($product['option'] as $option) { ?>
															<br />
															&nbsp;<small> - <?php echo $option['name']; ?>:
																<?php if (!empty($option['href'])) { ?>
																<a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a>
																<?php } else { ?>
																<?php echo $option['value']; ?>
																<?php } ?>
															</small>
															<?php } ?>
														</td>
														<td class="text-left">
															<?php if (!empty($product['seller_id'])) { ?>
															<a href="<?php echo $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $product['seller_id'], 'SSL');?>"><?php echo $product['seller_nickname']; ?></a>
															<?php } else { ?>
															<a href="<?php echo $this->url->link('multimerch/dashboard', 'token=' . $this->session->data['token'], 'SSL');?>"><?php echo $this->language->get('ms_store') ; ?></a>
															<?php } ?>
														</td>
														<td class="text-right"><?php echo $product['price_formatted']; ?></td>
														<?php if ($mm_shipping_flag) { ?>
															<td class="text-right"><?php echo $product['shipping_formatted']; ?></td>
														<?php } ?>
														<td class="text-right"><?php echo $product['total_formatted']; ?></td>
													</tr>
												<?php } ?>

												<?php foreach ($vouchers as $voucher) { ?>
													<tr>
														<td></td>
														<td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
														<td class="text-left">-</td>
														<td class="text-right"><?php echo $voucher['amount']; ?></td>
														<?php if ($mm_shipping_flag) { ?>
															<td class="text-right">-</td>
														<?php } ?>
														<td class="text-right"><?php echo $voucher['amount']; ?></td>
													</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<?php foreach ($order['totals'] as $total) { ?>
													<tr>
														<td colspan="<?php echo $mm_shipping_flag ? '5' : '4'; ?>" class="text-right"><?php echo $total['title']; ?>:</td>
														<td class="text-right"><?php echo $total['text']; ?></td>
													</tr>
												<?php } ?>
											</tfoot>
										</table>
									</div>

									<?php if ($order['comment']) { ?>
										<hr>

										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
												<tr>
													<td><?php echo $text_comment; ?></td>
												</tr>
												</thead>
												<tbody>
												<tr>
													<td><?php echo nl2br($order['comment']); ?></td>
												</tr>
												</tbody>
											</table>
										</div>
									<?php } ?>

									<hr>

									<div class="row" id="order-payment-history">
										<div class="col-md-12">
											<h4><i class="fa fa-credit-card"> </i> <?php echo $ms_order_payment_history; ?></h4>

											<div id="order-history"></div>

											<a href="javascript:;" class="bottom-dotted-link" id="add-order-history"><?php echo $ms_order_change_payment_status; ?></a>

											<div id="order-history-form" style="display: none;">
												<form class="form-horizontal">
													<div class="form-group">
														<div class="col-md-12">
															<select name="order_status_id" id="input-order-status" class="form-control">
																<option disabled="disabled" selected="selected"><?php echo $ms_order_change_payment_status_placeholder; ?></option>
																<?php foreach ($order_statuses as $order_status) { ?>
																	<option value="<?php echo $order_status['order_status_id']; ?>" <?php echo (int)$order_status['order_status_id'] === (int)$order['order_status_id'] ? 'style="display: none;"' : ''; ?>><?php echo $order_status['name']; ?></option>
																<?php } ?>
															</select>
														</div>
													</div>

													<input type="hidden" name="override" id="input-order-override" value="0" />

													<div class="form-group">
														<div class="col-md-12">
															<textarea name="comment" rows="4" id="input-order-comment" class="form-control" placeholder="<?php echo $ms_order_comment; ?>"></textarea>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-12">
															<select name="notify" id="input-order-notify" class="form-control">
																<option value="0"><?php echo $ms_order_notify_customer_no; ?></option>
																<option value="1"><?php echo $ms_order_notify_customer_yes; ?></option>
															</select>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-12">
															<button id="button-order-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" style="width: 100%;" disabled="disabled"><?php echo $ms_order_change_payment_status; ?></button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>

								<?php foreach ($suborders as $suborder) { ?>
									<div class="tab-pane" id="suborder_<?php echo $suborder['suborder_id']; ?>">
										<h4><i class="fa fa-shopping-cart"></i> <?php echo $ms_order_column_order_id . $order_id . '-' . $suborder['suborder_id']; ?> 
										<?php if ($mm_shipping_flag) { ?>
											<a href="<?php echo $suborder['link_print_packing_slip']; ?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-print fa-fw"></i> <?php echo $ms_print_order_packing_slip; ?></a>
										<?php } ?>
										</h4>
										<p><?php echo sprintf($ms_order_products_by, '<a href="' . $this->url->link('multimerch/seller/update', 'token=' . $token . '&seller_id=' . $suborder['seller_id'], 'SSL') . '">' . $suborder['seller'] . '</a>'); ?></p>
										<p><?php echo sprintf($ms_order_order_status, $suborder['status']); ?></p>

										<div class="table-responsive">
											<table class="table">
												<thead>
													<tr>
														<td class="text-center"></td>
														<td class="text-left"><?php echo $column_product; ?></td>
														<td class="text-right"><?php echo $column_price; ?></td>
														<?php if ($mm_shipping_flag) { ?>
														<td class="text-right"><?php echo $ms_sale_order_shipping_cost; ?></td>
														<?php } ?>
														<td class="text-right"><?php echo $column_total; ?></td>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($suborder['products'] as $product) { ?>
														<tr>
															<td class="text-center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></td>
															<td class="text-left"><?php echo $product['quantity'] > 1 ? $product['quantity'] . ' x ' : ''; ?><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
																<?php foreach ($product['msf_variation'] as $msf_variation) { ?>
																<br />
																&nbsp;<small> - <?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
																<?php } ?>

																<?php foreach ($product['option'] as $option) { ?>
																<br />
																&nbsp;<small> - <?php echo $option['name']; ?>:
																	<?php if (!empty($option['href'])) { ?>
																	<a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a>
																	<?php } else { ?>
																	<?php echo $option['value']; ?>
																	<?php } ?>
																</small>
																<?php } ?>
															</td>
															<td class="text-right"><?php echo $product['price_formatted']; ?></td>
															<?php if ($mm_shipping_flag) { ?>
															<td class="text-right"><?php echo $product['shipping_formatted']; ?></td>
															<?php } ?>
															<td class="text-right"><?php echo $product['total_formatted']; ?></td>
														</tr>
													<?php } ?>
												</tbody>
												<tfoot>
													<?php foreach ($suborder['totals'] as $total) { ?>
														<tr>
															<td colspan="<?php echo $mm_shipping_flag ? '4' : '3'; ?>" class="text-right"><?php echo $total['title']; ?>:</td>
															<td class="text-right"><?php echo $total['text']; ?></td>
														</tr>
													<?php } ?>
												</tfoot>
											</table>
										</div>

										<hr>

										<h4><i class="fa fa-truck"></i> <?php echo $this->language->get('ms_order_history'); ?></h4>
										<?php if (!empty($suborder['status_history'])) { ?>
											<div class="table-responsive">
												<table class="table text-center list">
													<thead>
														<tr>
															<td class="text-left"><?php echo $this->language->get('ms_report_column_date'); ?></td>
															<td class="text-left"><?php echo $this->language->get('column_comment'); ?></td>
															<td class="text-left"><?php echo $this->language->get('column_status'); ?></td>
														</tr>
													</thead>
													<tbody>
													<?php foreach ($suborder['status_history'] as $history) { ?>
														<tr>
															<td class="text-left col-sm-3"><?php echo $history['date_added']; ?></td>
															<td class="text-left col-sm-6"><?php echo nl2br($history['comment']); ?></td>
															<td class="text-left col-sm-3"><?php echo $history['status']; ?></td>
														</tr>
													<?php } ?>
													</tbody>
												</table>
											</div>
										<?php } ?>

										<a href="javascript:;" id="add-suborder-history-<?php echo $suborder['suborder_id']; ?>" class="bottom-dotted-link"><?php echo $ms_order_change_order_status; ?></a>

										<div id="suborder-history-form-<?php echo $suborder['suborder_id']; ?>" style="display: none;">
											<form class="form-horizontal">
												<div class="form-group">
													<div class="col-md-12">
														<select name="suborder_status_id" id="input-suborder-status-<?php echo $suborder['suborder_id']; ?>" class="form-control">
															<option disabled="disabled" selected="selected"><?php echo $ms_order_change_order_status_placeholder; ?></option>
															<?php foreach ($suborder_statuses as $suborder_status) { ?>
																<option value="<?php echo $suborder_status['ms_suborder_status_id']; ?>" <?php echo (int)$suborder_status['ms_suborder_status_id'] === (int)$suborder['suborder_status_id'] ? 'style="display: none;"' : ''; ?>><?php echo $suborder_status['name']; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<div class="col-md-12">
														<textarea name="comment" rows="4" class="form-control" placeholder="<?php echo $ms_order_comment; ?>"></textarea>
													</div>
												</div>

												<div class="form-group">
													<div class="col-md-12">
														<select name="notify" class="form-control">
															<option value="0"><?php echo $ms_order_notify_customer_no; ?></option>
															<option value="1"><?php echo $ms_order_notify_customer_yes; ?></option>
														</select>
													</div>
												</div>

												<div class="form-group">
													<div class="col-md-12">
														<button style="width: 100%;" id="button-suborder-history-<?php echo $suborder['suborder_id']; ?>" data-order_id="<?php echo $suborder['order_id']; ?>" data-seller_id="<?php echo $suborder['seller_id']; ?>" data-suborder_id="<?php echo $suborder['suborder_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" disabled="disabled"><?php echo $ms_order_change_order_status; ?></button>
													</div>
												</div>
											</form>
										</div>

										<hr>

										<h4><i class="fa fa-usd"></i> <?php echo $this->language->get('ms_order_transactions'); ?></h4>
										<div class="table-responsive">
											<table class="table text-center list">
												<thead>
													<tr>
														<td class="text-left"><?php echo $this->language->get('ms_order_date_created'); ?></td>
														<td class="text-left"><?php echo $this->language->get('ms_order_transactions_description'); ?></td>
														<td class="text-left"><?php echo $this->language->get('ms_order_transactions_amount'); ?></td>
													</tr>
												</thead>

												<tbody>
													<?php if ($suborder['transactions']) { ?>
														<?php foreach ($suborder['transactions'] as $transaction) { ?>
															<tr>
																<td class="text-left col-sm-3"><?php echo date($this->language->get('date_format_short'), strtotime($transaction['mb.date_created'])); ?></td>
																<td class="text-left col-sm-6"><?php echo (utf8_strlen($transaction['mb.description']) > 80 ? mb_substr($transaction['mb.description'], 0, 80) . '...' : $transaction['mb.description']); ?></td>
																<td class="text-left col-sm-3"><?php echo $this->currency->format($transaction['amount'], $this->config->get('config_currency')); ?></td>
															</tr>
														<?php } ?>
													<?php } else { ?>
														<td colspan="3" class="text-center" style="padding: 25px;"><?php echo $this->language->get('ms_order_notransactions'); ?></td>
													<?php } ?>
												</tbody>
											</table>
										</div>

										<?php if ($this->config->get('mmess_conf_enable')) { ?>
											<div id="suborder-conversation-<?php echo $suborder['suborder_id']; ?>"></div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-12">
				<div class="ms-dashboard-widget">
					<div class="customer-info">
						<h4><i class="fa fa-user"> </i> <?php echo $ms_report_column_customer; ?></h4>
						<p>
							<?php if ($customer['href']) { ?>
								<a href="<?php echo $customer['href']; ?>" target="_blank"><?php echo $customer['firstname']; ?> <?php echo $customer['lastname']; ?></a>
							<?php } else { ?>
								<?php echo $customer['firstname']; ?> <?php echo $customer['lastname']; ?>
							<?php } ?>
						</p>
						<p><?php echo $customer['email']; ?></p>
						<p><?php echo $customer['telephone']; ?></p>
					</div>

					<hr>

					<div class="payment-address">
						<h4><i class="fa fa-map-marker"> </i> <?php echo $shipping['address'] === $payment['address'] ? $ms_order_address : $ms_order_payment_address; ?></h4>
						<?php echo $payment['address']; ?>
					</div>

					<?php if ($shipping['address'] && $shipping['address'] !== $payment['address']) { ?>
						<hr>

						<div class="shipping-address">
							<h4><i class="fa fa-map-marker"> </i> <?php echo $ms_order_shipping_address; ?></h4>
							<?php echo $shipping['address']; ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var url = document.location.toString();
		if (url.match('#')) {
			$('ul.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			window.scrollTo(0, 0);
		}

		$('ul.nav-tabs a').on('shown.bs.tab', function (e) {
			// Open certain tab if it is passed in url
			window.location.hash = e.target.hash;
			window.scrollTo(0, 0);
		});

		var token = '';

		// Login to the API
		$.ajax({
			url: '<?php echo $catalog; ?>index.php?route=api/login',
			type: 'post',
			dataType: 'json',
			data: 'key=<?php echo $api_key; ?>',
			crossDomain: true,
			success: function(json) {
				$('.alert:not(#error-holder)').remove();

				if (json['error']) {
					if (json['error']['key']) {
						$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					if (json['error']['ip']) {
						$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
					}
				}

				if (json['token']) {
					token = json['token'];
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});

		$(document).delegate('#button-ip-add', 'click', function() {
			$.ajax({
				url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
				type: 'post',
				data: 'ip=<?php echo $api_ip; ?>',
				dataType: 'json',
				beforeSend: function() {
					$('#button-ip-add').button('loading');
				},
				complete: function() {
					$('#button-ip-add').button('reset');
				},
				success: function(json) {
					$('.alert').remove();

					if (json['error']) {
						$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					if (json['success']) {
						$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		// PAYMENT STATUS HISTORY

		$('#order-history').load('index.php?route=multimerch/order/paymentHistory&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

		$('#order-history').delegate('.pagination a', 'click', function(e) {
			e.preventDefault();
			$('#order-history').load(this.href);
		});

		$(document).on('click', '#add-order-history', function () {
			$('#order-history-form').toggle();
		});

		$(document).on('change', '#input-order-status', function () {
			$('#button-order-history').attr('disabled', false);
		});

		$('#button-order-history').on('click', function(e) {
			e.preventDefault();

			var order_status_id = $('#input-order-status').val();

			if (!order_status_id) {
				$('html,body').animate({
					scrollTop: $('#order-payment-history').offset().top
				}, 'slow');

				$('#order-payment-history .alert').remove();
				$('#order-history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $ms_order_change_payment_status_error; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				return;
			}

			var data = {
				order_status_id: encodeURIComponent(order_status_id),
				notify: encodeURIComponent($('#input-order-notify').val()),
				override: encodeURIComponent($('#input-order-override').val()),
				comment: encodeURIComponent($('#input-order-comment').val())
			};

			$.ajax({
				url: '<?php echo $catalog; ?>index.php?route=api/order/history&token=' + token + '&store_id=<?php echo $store_id; ?>&order_id=<?php echo $order_id; ?>',
				type: 'post',
				dataType: 'json',
				data: data,
				beforeSend: function() {
					$('#button-order-history').button('loading');
				},
				complete: function() {
					$('#button-order-history').button('reset');
				},
				success: function(json) {
					$('.alert:not(#error-holder)').remove();

					if (json['error']) {
						$('#order-history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					if (json['success']) {
						$('#order-history').load('index.php?route=multimerch/order/paymentHistory&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

						$('#order-history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

						// Reset values
						$('#input-order-status').val($("#input-order-status option:first").val());
						$('#input-order-comment').val('');

						// Hide option from selected and show all others
						$.map($('select[name="order_status_id"] > option'), function (option) {
							if ($(option).val() == order_status_id) {
								$(option).hide();
							} else {
								$(option).show();
							}
						});
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});

		function changeStatus(){
			var status_id = $('select[name="order_status_id"]').val();

			$('#openbay-info').remove();

			$.ajax({
				url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
				dataType: 'html',
				success: function(html) {
					$('#order-history').after(html);
				}
			});
		}

		changeStatus();

		$('#input-order-status').change(function(){
			changeStatus();
		});

		// END PAYMENT STATUS HISTORY

		// SUBORDER STATUS HISTORY

		$(document).on('click', '[id^="add-suborder-history"]', function () {
			var suborder_id = $(this).attr('id').split('-').pop();
			$('#suborder-history-form-' + suborder_id).toggle();
		});

		$(document).on('change', '[id^="input-suborder-status"]', function () {
			var suborder_id = $(this).attr('id').split('-').pop();
			$('#button-suborder-history-' + suborder_id).attr('disabled', false);
		});

		$(document).on('click', '[id^="button-suborder-history"]', function (e) {
			e.preventDefault();

			var $form = $(this).closest('form');
			var suborder_status_id = $form.find('[name="suborder_status_id"]').val();

			if (!suborder_status_id) {
				$('html,body').animate({ scrollTop: 0 }, 'slow');
				$('#error-holder').html("<?php echo $ms_order_change_order_status_error; ?>").show();
				return;
			}

			var data = {
				order_id: $(this).data('order_id'),
				seller_id: $(this).data('seller_id'),
				suborder_id: $(this).data('suborder_id'),
				suborder_status_id: suborder_status_id,
				notify: $form.find('[name="notify"]').val(),
				comment: $form.find('[name="comment"]').val()
			};

			$.ajax({
				url: 'index.php?route=multimerch/order/jxChangeSuborderStatus&token=<?php echo $token; ?>',
				type: 'POST',
				data: data,
				dataType: 'json',
				beforeSend: function () {
					$('#error-holder').html('').hide();
				},
				success: function (json) {
					if (json.error) {
						$('#error-holder').html(json.error).show();
						$("html, body").animate({ scrollTop: 0 }, "slow");
					} else if (json.success) {
						window.location.reload();
					}
				}
			});
		});

		// END SUBORDER STATUS HISTORY

		// ORDER CONVERSATION

		function loadConversations() {
			$.each($('[id^=suborder-conversation]'), function (key, item) {
				var id_parts = $(item).attr('id').split('-');
				var suborder_id = id_parts.pop();

				$(item).load("index.php?route=multimerch/order/suborderConversation&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&suborder_id=" + suborder_id);
			});
		}

		if ($('[id^=suborder-conversation]').length) {
			loadConversations();

			$(document).on('click', '.ms-order-message', function() {
				var button = $(this);

				$.ajax({
					type: "POST",
					dataType: "json",
					url: 'index.php?route=multimerch/conversation/jxSendMessage&token=<?php echo $token; ?>',
					data: $(this).closest('form').serialize(),
					beforeSend: function () {
						$(button).button('loading');
					},
					success: function(json) {
						$(button).button('reset');

						if (json.errors) {
							console.error(json.errors);
						} else {
							loadConversations();
						}
					}
				});
			});

			$(document).on('click', '.ms-message-upload', function() {
				var $this = $(this);
				var suborder_id = $this.data('suborder_id');

				$('#form-upload-' + suborder_id).remove();

				$('body').prepend('<form enctype="multipart/form-data" id="form-upload-' + suborder_id + '" style="display: none;"><input type="file" name="file" /></form>');

				$('#form-upload-' + suborder_id + ' input[name="file"]').trigger('click');

				if (typeof timer != 'undefined') {
					clearInterval(timer);
				}

				timer = setInterval(function () {
					if ($('#form-upload-' + suborder_id + ' input[name="file"]').val() != '') {
						clearInterval(timer);

						$.ajax({
							url: 'index.php?route=multimerch/conversation/jxUploadAttachment&token=<?php echo $token; ?>',
							type: 'post',
							dataType: 'json',
							data: new FormData($('#form-upload-' + suborder_id)[0]),
							cache: false,
							contentType: false,
							processData: false,
							beforeSend: function () {
								$this.button('loading');
							},
							complete: function () {
								$this.button('reset');
							},
							success: function (json) {
								if (json['error']) {
									alert(json['error']);
								}

								if (json['success']) {
									alert(json['success']);

									var html = '<li>';
									html += '<input type="hidden" name="attachments[]" value="' + json['code'] + '" />';
									html += json['filename'];
									html += '<span class="ms-remove"><i class="fa fa-times"></i></span>';
									html += '</li>';
									$('#suborder-conversation-' + suborder_id + ' ul.attachments').append(html);
								}
							},
							error: function (xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					}
				}, 500);
			});

			$(document).on('click', '.ms-remove', function() {
				$(this).closest('li').remove();
			});
		}

		// END ORDER CONVERSATION
	});
</script>

<?php echo $footer; ?>