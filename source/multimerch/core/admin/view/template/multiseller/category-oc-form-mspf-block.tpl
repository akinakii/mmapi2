<div class="col-sm-12" style="margin-bottom: 15px;">
	<div class="override-block <?php echo !$is_overridden ? '' : 'hidden'; ?>">
		<p><?php echo $messages['override']; ?></p>
		<a href="javascript:;" class="override bottom-dotted-link"><?php echo $ms_product_filter_block_btn_override; ?></a>
	</div>
	<div class="remove-override-block <?php echo $is_overridden ? '' : 'hidden'; ?>">
		<p><?php echo $messages['remove_override']; ?></p>
		<a href="javascript:;" class="remove-override bottom-dotted-link"><?php echo $ms_product_filter_block_btn_remove_override; ?></a>
	</div>
</div>

<div class="col-sm-12 <?php echo !$is_overridden ? 'ms-disabled' : ''; ?>">
	<input type="text" value="" placeholder="<?php echo $ms_product_filter_block_select_placeholder; ?>" id="input-mspf-block" class="form-control <?php if (!$is_overridden) { ?>hidden<?php } ?>" <?php if (!$is_overridden) { ?>disabled<?php } ?> />

	<ul id="mspf-blocks" class="list-group" style="margin-top: 10px;">
		<?php if (!empty($mspf_blocks)) { ?>
			<?php foreach ($mspf_blocks as $key => $mspf_block) { ?>
				<li class="list-group-item ui-state-default mspf-block">
					<input type="hidden" name="mspf_blocks[]" value="<?php echo $mspf_block['value']; ?>" class="mspf-block-item" />
					<strong class="value-name"><?php echo $mspf_block['category'] . ': ' . $mspf_block['label']; ?></strong>
					<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
				</li>
			<?php } ?>
		<?php } ?>
	</ul>
</div>

<script>
	$(function () {
		// Sortable list
		$("#mspf-blocks").sortable({
			placeholder: "ui-sortable-placeholder"
		});

		$(document).on('click', '.ms-remove', function () {
			$(this).closest('li').remove();
		});
	})
</script>