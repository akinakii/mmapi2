<div class="col-sm-12" style="margin-bottom: 15px;">
	<div class="override-block <?php echo !$is_overridden ? '' : 'hidden'; ?>">
		<p><?php echo $messages['override']; ?></p>
		<a href="javascript:;" class="override bottom-dotted-link"><?php echo $ms_field_variation_btn_override; ?></a>
	</div>
	<div class="remove-override-block <?php echo $is_overridden ? '' : 'hidden'; ?>">
		<p><?php echo $messages['remove_override']; ?></p>
		<a href="javascript:;" class="remove-override bottom-dotted-link"><?php echo $ms_field_variation_btn_remove_override; ?></a>
	</div>
</div>

<div class="col-sm-12 <?php echo !$is_overridden ? 'ms-disabled' : ''; ?>">
	<input type="text" value="" placeholder="<?php echo $ms_field_variation_select_placeholder; ?>" id="input-msf-variation" class="form-control <?php if (!$is_overridden) { ?>hidden<?php } ?>" <?php if (!$is_overridden) { ?>disabled<?php } ?> />

	<ul id="msf-variations" class="list-group" style="margin-top: 10px;">
		<?php if (!empty($msf_variations)) { ?>
			<?php foreach ($msf_variations as $key => $msf_variation) { ?>
				<li class="list-group-item ui-state-default variation">
					<input type="hidden" name="msf_variations[]" value="<?php echo $msf_variation['id']; ?>" class="msf-variation-id" />
					<strong class="value-name"><?php echo $msf_variation['full_name']; ?></strong>
					<a class="icon-remove ms-remove bottom-dotted-link" title="<?php echo $ms_remove; ?>" style="float: right;"><?php echo $ms_remove; ?></a>
				</li>
			<?php } ?>
		<?php } ?>
	</ul>
</div>

<script>
	$(function () {
		// Sortable list
		$("#msf-variations").sortable({
			placeholder: "ui-sortable-placeholder"
		});

		$(document).on('click', '.ms-remove', function () {
			$(this).closest('li').remove();
		});
	})
</script>