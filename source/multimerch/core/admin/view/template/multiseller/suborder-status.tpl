<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $ms_suborder_status_heading; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if (isset($success) && $success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $ms_suborder_status_heading; ?></h3>
            </div>
            <div class="panel-body tab-content">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-oc-order-statuses" data-toggle="tab"><?php echo $ms_suborder_status_tab_oc_status; ?></a></li>
                    <li><a href="#tab-ms-order-statuses" data-toggle="tab"><?php echo $ms_suborder_status_tab_ms_status; ?></a></li>
                </ul>
                <div class="tab-pane active" id="tab-oc-order-statuses">
                    <div class="ms-tab-pane-actions">
                        <div class="pull-right">
                            <a href="<?php echo $add_oc; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-oc-suborders-statuses">
                            <thead>
                            <tr>
                                <td class="small"><?php echo $ms_suborder_status_name; ?></td>
                                <td class="small"><?php echo $ms_suborder_status_action; ?></td>
                            </tr>
                            <tr class="filter">
                                <td><input type="text"/></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab-ms-order-statuses">
                    <div class="ms-tab-pane-actions">
                        <div class="pull-right">
                            <a href="<?php echo $add_ms; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-ms-suborders-statuses">
                            <thead>
                            <tr>
                                <td class="small"><?php echo $ms_suborder_status_name; ?></td>
                                <td class="small"><?php echo $ms_suborder_status_action; ?></td>
                            </tr>
                            <tr class="filter">
                                <td><input type="text"/></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list-oc-suborders-statuses').dataTable( {
            "sAjaxSource": "index.php?route=multimerch/suborder-status/getOcStatusTableData&token=<?php echo $token; ?>",
            "aoColumns": [
                { "mData": "name" },
                { "mData": "actions", "bSortable": false, "sClass": "text-right" }
            ],
            "initComplete": function(settings, json) {
                var api = this.api();
                var statusColumn = api.column('#status_column');

                $('#status_select').change( function() {
                    statusColumn.search( $(this).val() ).draw();
                });
            }

        });
		$('#list-ms-suborders-statuses').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/suborder-status/getMsStatusTableData&token=<?php echo $token; ?>",
			"aoColumns": [
				{ "mData": "name" },
				{ "mData": "actions", "bSortable": false, "sClass": "text-right" }
			],
			"initComplete": function(settings, json) {
				var api = this.api();
				var statusColumn = api.column('#status_column');

				$('#status_select').change( function() {
					statusColumn.search( $(this).val() ).draw();
				});
			}

		});

		$(document).on('click', '.delete-ms-status', function (e) {
			e.preventDefault();
			if (confirm("<?php echo $ms_delete_areyousure; ?>")) {
				window.location = $(this).attr('href');
			}
		});
    });
</script>
<?php echo $footer; ?>