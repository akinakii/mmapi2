<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button id="ms-submit-button" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $this->url->link('multimerch/seller-group', 'token=' . $this->session->data['token']); ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div style="display: none" class="alert alert-danger" id="error-holder"><i class="fa fa-exclamation-circle"></i>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading; ?></h3>
      </div>
      <div class="panel-body">
        <form method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
        <input type="hidden" name="seller_group[seller_group_id]" value="<?php echo $seller_group['seller_group_id']; ?>" />
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>

			<?php if ($this->config->get('msconf_sale_fee_seller_enabled') || $this->config->get('msconf_listing_fee_enabled') || $this->config->get('msconf_signup_fee_enabled')) { ?>
				<li><a href="#tab-commission" data-toggle="tab"><?php echo $ms_commissions_fees; ?></a></li>
			<?php } ?>

            <?php if ($this->config->get('msconf_badge_enabled')) { ?>
                <li><a href="#tab-badge" data-toggle="tab"><?php echo $ms_catalog_badges_breadcrumbs; ?></a></li>
            <?php } ?>

			<?php if ($this->config->get('ms_stripe_connect_enable_subscription')) { ?>
				<li><a href="#tab-stripe-subscription" data-toggle="tab"><?php echo $ms_seller_group_stripe_subscription_heading; ?></a></li>
			<?php } ?>
        </ul>

        <div class="tab-content">
        <div class="tab-pane active" id="tab-general">

            <div class="form-group required">
                <label class="col-sm-2 control-label"><?php echo $ms_name; ?></label>
                <div class="col-sm-10">
                  <?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                    <input type="text" name="seller_group[description][<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($seller_group['description'][$language['language_id']]['name']) ? $seller_group['description'][$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $ms_name; ?>" class="form-control" />
                  </div>
                  <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $ms_description; ?></label>
                <div class="col-sm-10">
                  <?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                    <textarea name="seller_group[description][<?php echo $language['language_id']; ?>][description]" cols="40" rows="5" placeholder="<?php echo $ms_description; ?>" class="form-control"><?php echo isset($seller_group['description'][$language['language_id']]['description']) ? $seller_group['description'][$language['language_id']]['description'] : ''; ?></textarea>
                  </div>
                  <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $ms_seller_group_product_number_limit; ?></label>
                <div class="col-sm-10 control-inline">
                    <input class="form-control" type="text" name="seller_group[settings][slr_gr_product_number_limit]" value="<?php echo isset($settings['slr_gr_product_number_limit']) ? $settings['slr_gr_product_number_limit'] : ''; ?>" size="5"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $ms_config_product_validation; ?>
                    <span data-toggle="tooltip" title="<?php echo $ms_config_product_validation_note; ?>"></span>
                </label>
                <div class="col-sm-10">
                    <select class="form-control" name="seller_group[settings][slr_gr_product_validation]">
                        <option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_NONE; ?>" <?php if($settings['slr_gr_product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_NONE) { ?> selected="selected" <?php } ?> ><?php echo $ms_config_product_validation_none; ?></option>
                        <option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_APPROVAL; ?>" <?php if($settings['slr_gr_product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_APPROVAL) { ?> selected="selected" <?php } ?> ><?php echo $ms_config_product_validation_approval; ?></option>
                    </select>
                </div>
            </div>

			<?php if ($this->config->get('msconf_msf_seller_property_enabled') && !empty($msf_seller_properties)) { ?>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $ms_field_seller_property_heading; ?></label>
					<div class="col-sm-10" style="margin-top: 9px;">
						<input type="hidden" name="msf_seller_properties_overridden" value="<?php echo !empty($seller_group['msf_seller_properties']) ? 1 : 0; ?>" />

						<div id="seller-group-msf-seller-properties"></div>
					</div>
				</div>
			<?php } ?>
        </div>

		<div class="tab-pane" id="tab-commission">
			<input type="hidden" name="seller_group[commission_id]" value="<?php echo $seller_group['commission_id']; ?>" />

			<div id="commissions-settings">
				<?php if ($this->config->get('msconf_sale_fee_seller_enabled')) { ?>
					<fieldset>
						<legend><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_SALE); ?></legend>
						<div class="col-md-12 control-inline">
							<?php echo $this->currency->getSymbolLeft(); ?>
							<input type="text" class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_SALE; ?>][flat]" value="<?php echo isset($seller_group['commission_rates'][MsCommission::RATE_SALE]['flat']) ? $this->currency->format($seller_group['commission_rates'][MsCommission::RATE_SALE]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3"/>
							<?php echo $this->currency->getSymbolRight(); ?>
							+<input type="text" class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_SALE; ?>][percent]" value="<?php echo isset($seller_group['commission_rates'][MsCommission::RATE_SALE]['percent']) ? $seller_group['commission_rates'][MsCommission::RATE_SALE]['percent'] : ''; ?>" size="3"/>%
						</div>
					</fieldset>
				<?php } ?>

				<?php if ($this->config->get('msconf_listing_fee_enabled')) { ?>
					<fieldset>
						<legend><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_LISTING); ?></legend>
						<div class="col-md-12 control-inline">
							<?php echo $this->currency->getSymbolLeft(); ?>
							<input type="text" class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][flat]" value="<?php echo isset($seller_group['commission_rates'][MsCommission::RATE_LISTING]['flat']) ? $this->currency->format($seller_group['commission_rates'][MsCommission::RATE_LISTING]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3"/>
							<?php echo $this->currency->getSymbolRight(); ?>
							+<input type="text" class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][percent]" value="<?php echo isset($seller_group['commission_rates'][MsCommission::RATE_LISTING]['percent']) ? $seller_group['commission_rates'][MsCommission::RATE_LISTING]['percent'] : ''; ?>" size="3"/>%
							<select class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][payment_method]">
								<optgroup label="<?php echo $ms_payment_method; ?>">
									<option value="<?php echo MsPgPayment::METHOD_BALANCE; ?>" <?php if(isset($seller_group['commission_rates'][MsCommission::RATE_LISTING]) && $seller_group['commission_rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_BALANCE) { ?> selected="selected" <?php } ?>><?php echo $ms_payment_method_balance; ?></option>
									<option value="<?php echo MsPgPayment::METHOD_PG; ?>" <?php if(isset($seller_group['commission_rates'][MsCommission::RATE_LISTING]) && $seller_group['commission_rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_PG) { ?> selected="selected" <?php } ?>><?php echo $ms_pg_fee_payment_method_name; ?></option>
								</optgroup>
							</select>
						</div>
					</fieldset>
				<?php } ?>

				<?php if ($this->config->get('msconf_signup_fee_enabled')) { ?>
					<fieldset>
						<legend><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_SIGNUP); ?></legend>
						<div class="col-md-12 control-inline">
							<?php echo $this->currency->getSymbolLeft(); ?>
							<input type="text" class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_SIGNUP; ?>][flat]" value="<?php echo isset($seller_group['commission_rates'][MsCommission::RATE_SIGNUP]['flat']) ? $this->currency->format($seller_group['commission_rates'][MsCommission::RATE_SIGNUP]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3"/>
							<?php echo $this->currency->getSymbolRight(); ?>
							<select class="form-control" name="seller_group[commission_rates][<?php echo MsCommission::RATE_SIGNUP; ?>][payment_method]">
								<optgroup label="<?php echo $ms_payment_method; ?>">
									<option value="<?php echo MsPgPayment::METHOD_BALANCE; ?>" <?php if(isset($seller_group['commission_rates'][MsCommission::RATE_SIGNUP]) && $seller_group['commission_rates'][MsCommission::RATE_SIGNUP]['payment_method'] == MsPgPayment::METHOD_BALANCE) { ?> selected="selected" <?php } ?>><?php echo $ms_payment_method_balance; ?></option>
									<option value="<?php echo MsPgPayment::METHOD_PG; ?>" <?php if(isset($seller_group['commission_rates'][MsCommission::RATE_SIGNUP]) && $seller_group['commission_rates'][MsCommission::RATE_SIGNUP]['payment_method'] == MsPgPayment::METHOD_PG) { ?> selected="selected" <?php } ?>><?php echo $ms_pg_fee_payment_method_name; ?></option>
								</optgroup>
							</select>
						</div>
					</fieldset>
				<?php } ?>
			</div>
		</div>
		<!--  end commission tab -->

		<!-- begin badge tab -->
		<div class="tab-pane" id="tab-badge">
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $ms_catalog_badges_heading; ?></label>
				<div class="col-sm-10">
					<div class="well well-sm">
						<?php foreach ($badges as $badge) { ?>
						<div>
							<input type="checkbox" name="seller_group[badges][]" value="<?php echo $badge['badge_id']; ?>" <?php if (isset($seller_group['badges']) && in_array($badge['badge_id'], $seller_group['badges'])) { ?>checked="checked"<?php } ?> />
							<?php echo $badge['name']; ?> <img src="<?php echo $badge['image']; ?>"/>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- end badge tab -->

		<!-- begin Stripe subscriptions tab -->
		<?php if ($this->config->get('ms_stripe_connect_enable_subscription')) { ?>
			<div class="tab-pane" id="tab-stripe-subscription">
				<div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $ms_seller_group_stripe_subscription_info; ?></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">
						<?php echo $ms_seller_group_stripe_subscription_enabled; ?>
					</label>
					<div class="col-sm-10">
						<input type="radio" name="seller_group[settings][slr_gr_stripe_subscription_enabled]" value="1" <?php if (1 === (int)$settings['slr_gr_stripe_subscription_enabled']) { ?>checked="checked"<?php } ?> />
						<?php echo $text_yes; ?>
						<input type="radio" name="seller_group[settings][slr_gr_stripe_subscription_enabled]" value="0" <?php if (0 === (int)$settings['slr_gr_stripe_subscription_enabled']) { ?>checked="checked"<?php } ?> />
						<?php echo $text_no; ?>
					</div>
				</div>

				<div class="form-group" id="base-plan">
					<label class="col-sm-2 control-label"><?php echo $ms_seller_group_stripe_subscription_plan_base; ?></label>
					<div class="col-sm-10">
						<select class="form-control" name="seller_group[settings][slr_gr_stripe_plan_base_id]">
							<option value=""><?php echo $ms_default_select_value; ?></option>
							<?php if (!empty($stripe_plans['base'])) { ?>
								<?php foreach ($stripe_plans['base'] as $stripe_plan) { ?>
									<?php if (!empty($stripe_plan)) { ?>
										<option value="<?php echo $stripe_plan->id; ?>" <?php echo !empty($settings['slr_gr_stripe_plan_base_id']) && $stripe_plan->id === $settings['slr_gr_stripe_plan_base_id'] ? 'selected="selected"' : ''; ?>><?php echo $stripe_plan->nickname . ' (' . $stripe_plan->condition_base . ')'; ?></option>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="form-group" id="per-seat-plan">
					<label class="col-sm-2 control-label"><?php echo $ms_seller_group_stripe_subscription_plan_per_seat; ?></label>
					<div class="col-sm-10">
						<select class="form-control" name="seller_group[settings][slr_gr_stripe_plan_per_seat_id]">
							<option value=""><?php echo $ms_default_select_value; ?></option>
							<?php if (!empty($stripe_plans['per_seat'])) { ?>
								<?php foreach ($stripe_plans['per_seat'] as $stripe_plan) { ?>
									<?php if (!empty($stripe_plan)) { ?>
										<option value="<?php echo $stripe_plan->id; ?>" <?php echo !empty($settings['slr_gr_stripe_plan_per_seat_id']) && $stripe_plan->id === $settings['slr_gr_stripe_plan_per_seat_id'] ? 'selected="selected"' : ''; ?>><?php echo $stripe_plan->nickname . ' (' . $stripe_plan->condition_per_seat . ')'; ?></option>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
		<?php } ?>
		<!-- end Stripe subscriptions tab -->

        </div>
        </form>
      </div>
	</div>
  </div>
</div>

<script>
	var msGlobals = {
		token: '<?php echo $token; ?>',
		texts: {
			ms_remove: "<?php echo $ms_remove; ?>"
		}
	};
</script>
<?php echo $footer; ?>