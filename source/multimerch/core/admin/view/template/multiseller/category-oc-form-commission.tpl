<div class="col-sm-12" style="margin-bottom: 15px;">
	<div class="override-note <?php echo $override['off'] && !$form ? '' : 'hidden'; ?>" data-override="0">
		<p id="override-off-note"><?php echo $override['off']; ?></p>
		<a href="javascript:;" class="override-fees bottom-dotted-link"><?php echo $ms_oc_category_commission_override_off_btn; ?></a>
	</div>
	<div class="override-note <?php echo $override['on'] && $form ? '' : 'hidden'; ?>" data-override="1">
		<p><?php echo $override['on']; ?></p>
		<a href="javascript:;" class="override-fees bottom-dotted-link"><?php echo $ms_oc_category_commission_override_on_btn; ?></a>
	</div>
</div>

<div id="commissions-rates" class="<?php echo $form ? '' : 'hidden'; ?>">
	<?php if ($this->config->get('msconf_sale_fee_catalog_enabled')) { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_SALE); ?></label>
			<div class="col-sm-10 control-inline">
				<?php echo $this->currency->getSymbolLeft(); ?>
				<input type="text" class="form-control" name="category_ms_fee[commission_rates][<?php echo MsCommission::RATE_SALE; ?>][flat]" value="<?php echo isset($form['rates'][MsCommission::RATE_SALE]['flat']) ? $this->currency->format($form['rates'][MsCommission::RATE_SALE]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3" <?php echo $form ? '' : 'disabled'; ?> />
				<?php echo $this->currency->getSymbolRight(); ?>
				+<input type="text" class="form-control" name="category_ms_fee[commission_rates][<?php echo MsCommission::RATE_SALE; ?>][percent]" value="<?php echo isset($form['rates'][MsCommission::RATE_SALE]['percent']) ? $form['rates'][MsCommission::RATE_SALE]['percent'] : ''; ?>" size="3" <?php echo $form ? '' : 'disabled'; ?> />%
			</div>
		</div>
	<?php } ?>

	<?php if ($this->config->get('msconf_listing_fee_enabled')) { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_LISTING); ?></label>
			<div class="col-sm-10 control-inline">
				<?php echo $this->currency->getSymbolLeft(); ?>
				<input type="text" class="form-control" name="category_ms_fee[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][flat]" value="<?php echo isset($form['rates'][MsCommission::RATE_LISTING]['flat']) ? $this->currency->format($form['rates'][MsCommission::RATE_LISTING]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3" <?php echo $form ? '' : 'disabled'; ?> />
				<?php echo $this->currency->getSymbolRight(); ?>
				+<input type="text" class="form-control" name="category_ms_fee[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][percent]" value="<?php echo isset($form['rates'][MsCommission::RATE_LISTING]['percent']) ? $form['rates'][MsCommission::RATE_LISTING]['percent'] : ''; ?>" size="3" <?php echo $form ? '' : 'disabled'; ?> />%

				<select class="form-control" name="category_ms_fee[commission_rates][<?php echo MsCommission::RATE_LISTING; ?>][payment_method]">
					<optgroup label="<?php echo $this->language->get('ms_payment_method'); ?>">
						<option value="<?php echo MsPgPayment::METHOD_BALANCE; ?>" <?php if(isset($form['rates'][MsCommission::RATE_LISTING]) && $form['rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_BALANCE) { ?> selected="selected" <?php } ?>><?php echo $this->language->get('ms_payment_method_balance'); ?></option>
						<option value="<?php echo MsPgPayment::METHOD_PG; ?>" <?php if(isset($form['rates'][MsCommission::RATE_LISTING]) && $form['rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_PG) { ?> selected="selected" <?php } ?>><?php echo $this->language->get('ms_pg_fee_payment_method_name'); ?></option>
					</optgroup>
				</select>
			</div>
		</div>
	<?php } ?>
</div>