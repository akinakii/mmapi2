<div class="msf-attributes-holder">
	<?php foreach ($msf_attributes as $msf_attribute) { ?>
		<div class="form-group <?php if ($msf_attribute['required']) { ?>required<?php } ?> <?php if (isset($errors[$msf_attribute['id']])) { ?>has-error<?php } ?>">
			<label class="col-sm-2 control-label"><?php echo $msf_attribute['name']; ?></label>
			<div class="col-sm-10">
				<?php if ('select' === $msf_attribute['type']) { ?>
					<select name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][id_values][]" class="form-control">
						<?php foreach ($msf_attribute['values'] as $value) { ?>
							<option value="<?php echo $value['id']; ?>" <?php if (!empty($product_msf_attributes[$msf_attribute['id']]['id_values']) && in_array($value['id'], array_keys($product_msf_attributes[$msf_attribute['id']]['id_values']))) { ?>selected="selected"<?php } ?>><?php echo $value['name']; ?></option>
						<?php } ?>
					</select>
				<?php } elseif ('checkbox' === $msf_attribute['type']) { ?>
					<div class="well well-sm" style="height: 150px; overflow: auto;">
						<?php foreach ($msf_attribute['values'] as $value) { ?>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][id_values][]" value="<?php echo $value['id']; ?>" <?php if (!empty($product_msf_attributes[$msf_attribute['id']]['id_values']) && in_array($value['id'], array_keys($product_msf_attributes[$msf_attribute['id']]['id_values']))) { ?>checked="checked"<?php } ?> />
									<?php echo $value['name'] ?: ''; ?>
								</label>
							</div>
						<?php } ?>
					</div>
				<?php } elseif ('text' === $msf_attribute['type']) { ?>
					<?php foreach ($languages as $language) { ?>
						<?php $img = "language/{$language['code']}/{$language['code']}.png"; ?>
						<div class="lang-text-input">
							<div class="lang-img-icon-input"><img src="<?php echo $img?>"></div>
							<input type="text" name="product_msf_attribute[<?php echo $msf_attribute['id']; ?>][text_values][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($product_msf_attributes[$msf_attribute['id']]['text_values'][$language['language_id']]) ? $product_msf_attributes[$msf_attribute['id']]['text_values'][$language['language_id']] : ''; ?>" class="form-control" />
						</div>
					<?php } ?>
				<?php } ?>

				<?php if (isset($errors[$msf_attribute['id']])) { ?>
					<div class="text-danger"><?php echo $errors[$msf_attribute['id']]; ?></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>