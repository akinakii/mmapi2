<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button id="ms-submit-button" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $this->url->link('multimerch/seller', 'token=' . $token); ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $ms_catalog_sellers_heading; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div style="display: none" class="alert alert-danger" id="error-holder"><i class="fa fa-exclamation-circle"></i>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo isset($seller['seller_id']) ? $ms_catalog_sellerinfo_heading : $ms_catalog_sellers_newseller; ?></h3>
			</div>
			<div class="panel-body">
				<form id="ms-sellerinfo" class="form-horizontal">
					<input type="hidden" id="seller_id" name="seller[seller_id]" value="<?php echo $seller['seller_id']; ?>" />

					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
						<li><a href="#tab-ms-address" data-toggle="tab"><?php echo $ms_seller_address; ?></a></li>

						<?php if ($this->config->get('msconf_sale_fee_seller_enabled') || $this->config->get('msconf_listing_fee_enabled')) { ?>
							<li><a href="#tab-commission" data-toggle="tab"><?php echo $ms_commissions_fees; ?></a></li>
						<?php } ?>

						<?php if($this->config->get('msconf_badge_enabled')) { ?>
							<li><a href="#tab-badge" data-toggle="tab"><?php echo $ms_catalog_badges_breadcrumbs; ?></a></li>
						<?php } ?>

						<?php if (!empty($seller['seller_id'])) { ?>
							<li><a href="#tab-product" data-toggle="tab"><?php echo $ms_menu_products; ?></a></li>
							<li><a href="#tab-transaction" data-toggle="tab"><?php echo $ms_menu_transactions; ?></a></li>
							<li><a href="#tab-invoices" data-toggle="tab"><?php echo $ms_menu_invoice; ?></a></li>
							<li><a href="#tab-payment" data-toggle="tab"><?php echo $ms_menu_payment; ?></a></li>
						<?php } ?>

						<?php if(isset($payment_gateways) && is_array($payment_gateways) && !empty($payment_gateways)) { ?>
							<li><a href="#tab-payments" data-toggle="tab"><?php echo $ms_menu_payment_gateway_settings; ?></a></li>
						<?php } ?>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active" id="tab-general">
							<fieldset>
								<legend><?php echo $ms_catalog_sellerinfo_customer_data; ?></legend>
								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer; ?></label>

									<div class="col-sm-10">
										<?php if (!$seller['seller_id']) { ?>
											<select class="form-control" name="customer[customer_id]">
												<optgroup label="<?php echo $ms_catalog_sellerinfo_customer_new; ?>">
													<option value="0"><?php echo $ms_catalog_sellerinfo_customer_create_new; ?></option>
												</optgroup>
												<?php if (isset($customers)) { ?>
													<optgroup label="<?php echo $ms_catalog_sellerinfo_customer_existing; ?>">
														<?php foreach ($customers as $c) { ?>
															<option value="<?php echo $c['c.customer_id']; ?>"><?php echo $c['c.name']; ?></option>
														<?php } ?>
													</optgroup>
												<?php } ?>
											</select>
										<?php } else { ?>
											<a href="<?php echo $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $seller['seller_id'], 'SSL'); ?>"><?php echo $seller['name']; ?></a>
										<?php } ?>
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer_firstname; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="customer[firstname]" value="" />
									</div>
								</div>

								<div class="form-group required">
								<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer_lastname; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="customer[lastname]" value="" />
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer_email; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="customer[email]" value="" />
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer_password; ?></label>
									<div class="col-sm-10">
										<input type="password" class="form-control" name="customer[password]" value="" />
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_customer_password_confirm; ?></label>
									<div class="col-sm-10">
										<input type="password" class="form-control" name="customer[password_confirm]" value="" />
									</div>
								</div>
							</fieldset>

							<fieldset>
								<legend><?php echo $ms_catalog_sellerinfo_seller_data; ?></legend>

								<input type="hidden" name="seller[avatar]" value="<?php echo $seller['ms.avatar']; ?>" />
								<input type="hidden" name="seller[banner]" value="<?php echo $seller['banner']; ?>" />

								<div class="form-group required">
									<label class="col-sm-2 control-label required"><?php echo $ms_catalog_sellerinfo_nickname; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[nickname]" value="<?php echo $seller['ms.nickname']; ?>" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_keyword; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[keyword]" value="<?php echo $seller['keyword']; ?>" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_sellergroup; ?></label>
									<div class="col-sm-10">
										<select class="form-control" name="seller[seller_group]" id="seller-group-select">
											<?php foreach ($seller_groups as $group) { ?>
												<option value="<?php echo $group['seller_group_id']; ?>" <?php if ($seller['ms.seller_group'] == $group['seller_group_id']) { ?>selected="selected"<?php } ?>><?php echo $group['name']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">
										<span data-toggle="tooltip" title="<?php echo $ms_catalog_sellerinfo_product_validation_note; ?>"><?php echo $ms_catalog_sellerinfo_product_validation; ?></span>
									</label>
									<div class="col-sm-10">
										<?php if (isset($settings['slr_product_validation'])) { ?>
											<select class="form-control" name="seller_setting[slr_product_validation]">
												<option value="0" <?php if($settings['slr_product_validation'] == 0) { ?> selected="selected" <?php } ?>><?php echo $ms_config_product_validation_from_group_settings; ?></option>
												<option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_NONE; ?>" <?php if($settings['slr_product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_NONE) { ?> selected="selected" <?php } ?>><?php echo $ms_config_product_validation_none; ?></option>
												<option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_APPROVAL; ?>" <?php if($settings['slr_product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_APPROVAL) { ?> selected="selected" <?php } ?>><?php echo $ms_config_product_validation_approval; ?></option>
											</select>
										<?php } else { ?>
											<select class="form-control" name="seller_setting[slr_product_validation]">
												<option value="0" selected="selected"><?php echo $ms_config_product_validation_from_group_settings; ?></option>
												<option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_NONE; ?>"><?php echo $ms_config_product_validation_none; ?></option>
												<option value="<?php echo MsProduct::MS_PRODUCT_VALIDATION_APPROVAL; ?>"><?php echo $ms_config_product_validation_approval; ?></option>
											</select>
										<?php } ?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_slogan; ?></label>
									<div class="col-sm-10">
										<ul class="nav nav-tabs" id="language-slogan">
											<?php foreach ($languages as $language) { ?>
												<li>
													<a href="#language-slogan-<?php echo $language['language_id']; ?>" data-toggle="tab">
														<img class="lang_image" src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?>
													</a>
												</li>
											<?php } ?>
										</ul>
										<div class="tab-content">
											<?php foreach ($languages as $language) { ?>
												<div class="tab-pane" id="language-slogan-<?php echo $language['language_id']; ?>">
													<textarea name="seller[languages][<?php echo $language['language_id']; ?>][slogan]"  class="form-control"> <?php echo htmlspecialchars_decode($seller['descriptions'][$language['language_id']]['slogan']); ?></textarea>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_catalog_sellerinfo_description; ?></label>
									<div class="col-sm-10">
										<ul class="nav nav-tabs" id="language-description">
											<?php foreach ($languages as $language) { ?>
												<li>
													<a href="#language-description-<?php echo $language['language_id']; ?>" data-toggle="tab">
														<img class="lang_image" src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?>
													</a>
												</li>
											<?php } ?>
										</ul>
										<div class="tab-content">
											<?php foreach ($languages as $language) { ?>
												<div class="tab-pane" id="language-description-<?php echo $language['language_id']; ?>">
													<textarea name="seller[languages][<?php echo $language['language_id']; ?>][description]" id="seller_textarea<?php echo $language['language_id']; ?>" class="form-control summernote"> <?php echo htmlspecialchars_decode($seller['descriptions'][$language['language_id']]['description']); ?> </textarea>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>

								<?php $msSeller = new ReflectionClass('MsSeller'); ?>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_status; ?></label>
									<div class="col-sm-10">
										<select class="form-control" name="seller[status]">
											<?php foreach ($msSeller->getConstants() as $cname => $cval) { ?>
												<?php if (strpos($cname, 'STATUS_') !== FALSE) { ?>
													<option value="<?php echo $cval; ?>" <?php if ($seller['ms.seller_status'] == $cval) { ?>selected="selected"<?php } ?>><?php echo $this->language->get('ms_seller_status_' . $cval); ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">
										<span data-toggle="tooltip" title="<?php echo $ms_catalog_sellerinfo_notify_note; ?>"><?php echo $ms_catalog_sellerinfo_notify; ?></span>
									</label>
									<div class="col-sm-10">
										<input type="checkbox" style="margin-top: 10px" name="seller[notify]" value="1" checked="checked" /><br>
										<textarea class="form-control" name="seller[message]" placeholder="<?php echo $ms_catalog_sellerinfo_message_note; ?>"></textarea>
									</div>
								</div>
							</fieldset>

							<fieldset>
								<legend><?php echo $ms_menu_field_seller_properties_short; ?></legend>

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_website; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller_setting[slr_website]" value="<?php echo (isset($settings['slr_website'])) ? $settings['slr_website'] : '' ; ?>" placeholder="<?php echo $ms_seller_website ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_company; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller_setting[slr_company]" value="<?php echo (isset($settings['slr_company'])) ? $settings['slr_company'] : '' ; ?>" placeholder="<?php echo $ms_seller_company ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_phone; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller_setting[slr_phone]" value="<?php echo (isset($settings['slr_phone'])) ? $settings['slr_phone'] : '' ; ?>" placeholder="<?php echo $ms_seller_phone ;?>">
									</div>
								</div>

								<?php if ($this->config->get('msconf_msf_seller_property_enabled')) { ?>
									<div id="tab-msf-seller-property"></div>
								<?php } ?>
							</fieldset>
						</div>

						<div class="tab-pane" id="tab-commission">
							<input type="hidden" name="seller[commission_id]" value="<?php echo $seller['commission_id']; ?>" />

							<div class="override-note <?php echo empty($seller['commission_id']) ? '' : 'hidden'; ?>" data-override="0">
								<p id="override-off-note"><?php echo sprintf($ms_seller_commission_override_off_note, $this->MsLoader->MsSellerGroup->getSellerGroupName($seller['ms.seller_group'] ?: $this->config->get('msconf_default_seller_group_id')), !empty($seller['actual_fees']) ? $seller['actual_fees'] : ''); ?></p>
								<a href="javascript:;" class="override-fees bottom-dotted-link"><?php echo $ms_seller_commission_override_off_btn; ?></a>
							</div>
							<div class="override-note <?php echo empty($seller['commission_id']) ? 'hidden' : ''; ?>" data-override="1">
								<p><?php echo sprintf($ms_seller_commission_override_on_note, $this->url->link('multimerch/seller-group/update', 'token=' . $this->session->data['token'] . '&seller_group_id=' . ($seller['ms.seller_group'] ?: $this->config->get('msconf_default_seller_group_id')), 'SSL')); ?></p>
								<a href="javascript:;" class="override-fees bottom-dotted-link"><?php echo $ms_seller_commission_override_on_btn; ?></a>
							</div>

							<div id="commissions-rates" class="<?php echo empty($seller['commission_id']) ? 'hidden' : ''; ?>">
								<?php if ($this->config->get('msconf_sale_fee_seller_enabled')) { ?>
									<fieldset>
										<legend><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_SALE); ?></legend>
										<div class="col-md-12 control-inline">
											<?php echo $this->currency->getSymbolLeft(); ?>
											<input type="text" class="form-control" name="seller[commission][<?php echo MsCommission::RATE_SALE; ?>][flat]" value="<?php echo isset($seller['commission_rates'][MsCommission::RATE_SALE]['flat']) ? $this->currency->format($seller['commission_rates'][MsCommission::RATE_SALE]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3" <?php echo empty($seller['commission_id']) ? 'disabled' : ''; ?> />
											<?php echo $this->currency->getSymbolRight(); ?>
											+<input type="text" class="form-control" name="seller[commission][<?php echo MsCommission::RATE_SALE; ?>][percent]" value="<?php echo isset($seller['commission_rates'][MsCommission::RATE_SALE]['percent']) ? $seller['commission_rates'][MsCommission::RATE_SALE]['percent'] : ''; ?>" size="3" <?php echo empty($seller['commission_id']) ? 'disabled' : ''; ?> />%
										</div>
									</fieldset>
								<?php } ?>

								<?php if ($this->config->get('msconf_listing_fee_enabled')) { ?>
									<fieldset>
										<legend><?php echo $this->language->get('ms_commission_' . MsCommission::RATE_LISTING); ?></legend>
										<div class="col-md-12 control-inline">
											<?php echo $this->currency->getSymbolLeft(); ?>
											<input type="text" class="form-control" name="seller[commission][<?php echo MsCommission::RATE_LISTING; ?>][flat]" value="<?php echo isset($seller['commission_rates'][MsCommission::RATE_LISTING]['flat']) ? $this->currency->format($seller['commission_rates'][MsCommission::RATE_LISTING]['flat'], $this->config->get('config_currency'), '', FALSE) : '' ?>" size="3" <?php echo empty($seller['commission_id']) ? 'disabled' : ''; ?> />
											<?php echo $this->currency->getSymbolRight(); ?>
											+<input type="text" class="form-control" name="seller[commission][<?php echo MsCommission::RATE_LISTING; ?>][percent]" value="<?php echo isset($seller['commission_rates'][MsCommission::RATE_LISTING]['percent']) ? $seller['commission_rates'][MsCommission::RATE_LISTING]['percent'] : ''; ?>" size="3" <?php echo empty($seller['commission_id']) ? 'disabled' : ''; ?> />%
											<select class="form-control" name="seller[commission][<?php echo MsCommission::RATE_LISTING; ?>][payment_method]">
												<optgroup label="<?php echo $ms_payment_method; ?>">
													<option value="<?php echo MsPgPayment::METHOD_BALANCE; ?>" <?php if(isset($seller['commission_rates'][MsCommission::RATE_LISTING]) && $seller['commission_rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_BALANCE) { ?> selected="selected" <?php } ?>><?php echo $ms_payment_method_balance; ?></option>
													<option value="<?php echo MsPgPayment::METHOD_PG; ?>" <?php if(isset($seller['commission_rates'][MsCommission::RATE_LISTING]) && $seller['commission_rates'][MsCommission::RATE_LISTING]['payment_method'] == MsPgPayment::METHOD_PG) { ?> selected="selected" <?php } ?>><?php echo $ms_pg_fee_payment_method_name; ?></option>
												</optgroup>
											</select>
										</div>
									</fieldset>
								<?php } ?>
							</div>
						</div>

						<?php if (!empty($seller['seller_id'])) { ?>
							<div class="tab-pane" id="tab-product">
								<div class="table-responsive">
									<table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-products">
										<thead>
											<tr>
												<td width="1" style="text-align: center;"><input type="checkbox" onclick="$(document).find('#list-products input[name*=\'selected\']').prop('checked', $(this).prop('checked'));" /></td>
												<td class="tiny"></td>
												<td class="large"><?php echo $ms_product; ?></td>
												<td class="medium"><?php echo $ms_price ;?></td>
												<td class="medium"><?php echo $ms_quantity ;?></td>
												<td class="medium" id="status_column"><?php echo $ms_status; ?></td>
												<td class="medium"><?php echo $ms_date_modified; ?></td>
												<td class="medium"><?php echo $ms_date_created; ?></td>
												<td class="large"><?php echo $ms_action; ?></td>
											</tr>
											<tr class="filter">
												<td></td>
												<td></td>
												<td><input type="text"/></td>
												<td></td>
												<td></td>
												<td>
													<select id="status_select">
														<option></option>
														<?php $msProduct = new ReflectionClass('MsProduct'); ?>
														<?php foreach ($msProduct->getConstants() as $cname => $cval) { ?>
															<?php if (strpos($cname, 'STATUS_') !== FALSE) { ?>
																<option value="<?php echo $cval; ?>"><?php echo $this->language->get('ms_product_status_' . $cval); ?></option>
															<?php } ?>
														<?php } ?>
													</select>
												</td>
												<td><input type="text" class="input-date-datepicker"/></td>
												<td><input type="text" class="input-date-datepicker"/></td>
												<td></td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>

							<div class="tab-pane" id="tab-transaction">
								<div class="table-responsive">
									<table class="list table table-bordered table-hover" style="text-align: center" id="list-transactions">
										<thead>
											<tr>
												<td class="tiny"><?php echo $ms_id; ?></td>
												<td class="small"><?php echo $ms_net_amount; ?></a></td>
												<td><?php echo $ms_description; ?></a></td>
												<td class="medium"><?php echo $ms_date; ?></a></td>
											</tr>
											<tr class="filter">
												<td><input type="text"/></td>
												<td><input type="text"/></td>
												<td><input type="text"/></td>
												<td><input type="text" class="input-date-datepicker"/></td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>

							<div class="tab-pane" id="tab-invoices">
								<div class="table-responsive">
									<table class="list mmTable table table-bordered table-hover" style="text-align: center" id="list-invoices">
										<thead>
											<tr>
												<td class="small"><?php echo $ms_invoice_column_invoice_id; ?></td>
												<td class="medium"><?php echo $ms_invoice_column_type; ?></td>
												<td class="small"><?php echo $ms_invoice_column_total; ?></td>
												<td><?php echo $ms_invoice_column_title; ?></td>
												<td class="medium"><?php echo $ms_invoice_column_date_generated; ?></td>
												<td class="medium"><?php echo $ms_invoice_column_status; ?></td>
												<td class="medium"><?php echo $ms_invoice_column_payment_id; ?></td>
											</tr>
											<tr class="filter">
												<td><input type="text"/></td>
												<td></td>
												<td></td>
												<td></td>
												<td><input type="text" class="input-date-datepicker"/></td>
												<td></td>
												<td><input type="text"/></td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>

							<div class="tab-pane" id="tab-payment">
								<div class="table-responsive">
									<table class="table table-bordered table-hover" style="text-align: center" id="list-payments">
										<thead>
											<tr>
												<td class="tiny"><?php echo $ms_id; ?></td>
												<td class="medium"><?php echo $ms_type; ?></td>
												<td class="medium"><?php echo $ms_method; ?></td>
												<td class="large"><?php echo $ms_description; ?></td>
												<td class="small"><?php echo $ms_amount; ?></td>
												<td class="small"><?php echo $ms_status; ?></td>
												<td class="medium"><?php echo $ms_date_created; ?></td>
											</tr>
											<tr class="filter">
												<td></td>
												<td></td>
												<td><input type="text"/></td>
												<td></td>
												<td><input type="text"/></td>
												<td></td>
												<td><input type="text" class="input-date-datepicker"/></td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						<?php } ?>

						<div class="tab-pane" id="tab-badge">
							<div class="form-group">
								<label class="col-sm-2 control-label"><?php echo $ms_catalog_badges_heading; ?></label>
								<div class="col-sm-10">
									<div class="well well-sm">
										<?php foreach ($badges as $badge) { ?>
											<div>
												<input type="checkbox" name="seller[badges][]" value="<?php echo $badge['badge_id']; ?>" <?php if (isset($seller['badges']) && in_array($badge['badge_id'], $seller['badges'])) { ?>checked="checked"<?php } ?> />
												<?php echo $badge['name']; ?> <img src="<?php echo $badge['image']; ?>"/>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab-ms-address">
							<fieldset>
								<input type="hidden" name="seller[address][address_id]" value="<?php echo isset($address['address_id']) ? $address['address_id'] : ''; ?>" />

								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_full_name; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][fullname]" value="<?php echo (isset($address['fullname'])) ? $address['fullname'] : '' ; ?>" placeholder="<?php echo $ms_seller_full_name; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_address1; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][address_1]" value="<?php echo (isset($address['address_1'])) ? $address['address_1'] : '' ; ?>" placeholder="<?php echo $ms_seller_address1_placeholder ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_address2; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][address_2]" value="<?php echo (isset($address['address_2'])) ? $address['address_2'] : '' ; ?>" placeholder="<?php echo $ms_seller_address2_placeholder ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_city; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][city]" value="<?php echo (isset($address['city'])) ? $address['city'] : '' ; ?>" placeholder="<?php echo $ms_seller_city; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_state; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][state]" value="<?php echo (isset($address['state'])) ? $address['state'] : '' ; ?>" placeholder="<?php echo $ms_seller_state ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_zip; ?></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="seller[address][zip]" value="<?php echo (isset($address['zip'])) ? $address['zip'] : '' ; ?>" placeholder="<?php echo $ms_seller_zip ;?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"><?php echo $ms_seller_country; ?></label>
									<div class="col-sm-10">
										<select class="form-control" name="seller[address][country_id]">
											<?php foreach($countries as $country) { ?>
												<option value="<?php echo $country['country_id'] ;?>" <?php echo isset($address['country_id']) && (int)$address['country_id'] === (int)$country['country_id'] ? 'selected' : ''; ?>><?php echo $country['name'] ;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</fieldset>
						</div>

						<div class="tab-pane" id="tab-store-settings">
							<fieldset>
								<legend><?php echo $ms_catalog_sellerinfo_information; ?></legend>

							</fieldset>
						</div>

						<?php if(isset($payment_gateways) && is_array($payment_gateways) && !empty($payment_gateways)) { ?>
							<div class="tab-pane" id="tab-payments">
								<ul class="nav nav-tabs pg-topbar">
									<?php foreach($payment_gateways as $payment_gateway) { ?>
										<li <?php echo reset($payment_gateways) == $payment_gateway ? 'class="active"' : ''; ?>><a href="#tab-<?php echo $payment_gateway['code']; ?>" data-toggle="tab"><?php echo $payment_gateway['text_title']; ?></a></li>
									<?php } ?>
								</ul>
								<div class="tab-content ms-pg-content">
									<?php foreach($payment_gateways as $payment_gateway) { ?>
										<div id="tab-<?php echo $payment_gateway['code']; ?>" class="tab-pane <?php echo reset($payment_gateways) == $payment_gateway ? 'active' : ''; ?>">
											<input type="hidden" class="ms_pg_code" value="<?php echo $payment_gateway['code']; ?>">
											<?php echo $payment_gateway['view']; ?>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#language-slogan a:first').tab('show');
		$('#language-description a:first').tab('show');

		$('input[name^="customer"]').parents('div.form-group').hide();
		$('select[name="customer[customer_id]"]').bind('change', function() {
			if (this.value == '0') {
				$('input[name^="customer"]').parents('div.form-group').show();
				$('[name="seller[notify]"], [name="seller[message]"]').parents('div.form-group').hide();
			} else {
				$('input[name^="customer"]').parents('div.form-group').hide();
				$('[name="seller[notify]"], [name="seller[message]"]').parents('div.form-group').show();
			}
		}).change();

		$("#ms-submit-button").click(function() {
			var button = $(this);
			var id = $(this).attr('id');
			$.ajax({
				type: "POST",
				dataType: "json",
				url: 'index.php?route=multimerch/seller/jxsavesellerinfo&token=<?php echo $token; ?>',
				data: $('#ms-sellerinfo').serialize(),
				beforeSend: function() {
					$('div.text-danger').remove();
					$('div.has-error').removeClass('has-error');
					$('.alert-danger').hide().find('i').text('');
				},
				complete: function(jqXHR, textStatus) {
					button.show().prev('span.wait').remove();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('.alert-danger').show().find('i').text(textStatus);
                    $('#error-holder').html("<?php echo $this->language->get('ms_error_form_submit_error'); ?>").show();
                },
				success: function(jsonData) {
					if (!jQuery.isEmptyObject(jsonData.errors)) {
						var errors_text = '';
						for (error in jsonData.errors) {
							errors_text += jsonData.errors[error] + '<br/>';
							$('[name="'+error+'"]').closest('div').addClass('has-error');
							$('[name="'+error+'"]').after("<div class='text-danger'>" + jsonData.errors[error] + "</div>");
						}

						$('#error-holder').html(errors_text).show();
						window.scrollTo(0,0);
					} else {
						window.location = 'index.php?route=multimerch/seller&token=<?php echo $token; ?>';
					}
				}
			});
		});

		$("select[name='seller[country]']").bind('change', function() {
			$.ajax({
				url: 'index.php?route=customer/customer/country&token=<?php echo $token; ?>&country_id=' + this.value,
				dataType: 'json',
				beforeSend: function() {
					$("select[name='seller[country]']").after('<i class="fa fa-circle-o-notch fa-spin"></i>');
				},
				complete: function() {
					$('.fa-spin').remove();
				},
				success: function(json) {
					html = '<option value=""><?php echo $ms_catalog_sellerinfo_zone_select; ?></option>';

					if (json['zone']) {
						for (i = 0; i < json['zone'].length; i++) {
							html += '<option value="' + json['zone'][i]['zone_id'] + '"';

							if (json['zone'][i]['zone_id'] == '<?php echo !empty($settings['slr_country']) ? $settings["slr_country"] : ""; ?>') {
								html += ' selected="selected"';
							}

							html += '>' + json['zone'][i]['name'] + '</option>';
						}
					} else {
						html += '<option value="0" selected="selected"><?php echo $ms_catalog_sellerinfo_zone_not_selected; ?></option>';
					}

					$("select[name='seller[zone]']").html(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}).trigger('change');

		$('#list-products').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/seller/getProductTableData&token=<?php echo $token; ?>&seller_id=<?php echo $seller['seller_id']; ?>",
			"aoColumns": [
				{ "mData": "checkbox", "bSortable": false },
				{ "mData": "image", "bSortable": false },
				{ "mData": "name" },
				{ "mData": "price"},
				{ "mData": "quantity"},
				{ "mData": "status" },
				{ "mData": "date_modified" },
				{ "mData": "date_added", "visible": false },
				{ "mData": "actions", "bSortable": false, "sClass": "text-right" }
			],
			"initComplete": function(settings, json) {
				var api = this.api();
				var statusColumn = api.column('#status_column');

				$('#status_select').change( function() {
					statusColumn.search( $(this).val() ).draw();
				});
			}
		});

		$('#list-transactions').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/seller/getTransactionTableData&token=<?php echo $token; ?>&seller_id=<?php echo $seller['seller_id']; ?>",
			"aoColumns": [
				{ "mData": "id" },
				{ "mData": "amount" },
				{ "mData": "description" },
				{ "mData": "date_created" }
			],
			"aaSorting":  [[3,'desc']]
		});

		$('#list-invoices').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/seller/getInvoiceTableData&token=<?php echo $token; ?>&seller_id=<?php echo $seller['seller_id']; ?>",
			"aoColumns": [
				{ "mData": "invoice_id" },
				{ "mData": "type" },
				{ "mData": "total" },
				{ "mData": "title", "bSortable": false },
				{ "mData": "date_generated" },
				{ "mData": "status" },
				{ "mData": "payment_id" }
			],
			"aaSorting":  [[4,'desc']]
		});

		$('#list-payments').dataTable( {
			"sAjaxSource": "index.php?route=multimerch/seller/getPaymentTableData&token=<?php echo $token; ?>&seller_id=<?php echo $seller['seller_id']; ?>",
			"aoColumns": [
				{ "mData": "payment_id" },
				{ "mData": "payment_type" },
				{ "mData": "payment_code" },
				{ "mData": "description" },
				{ "mData": "amount" },
				{ "mData": "payment_status" },
				{ "mData": "date_created" },
			],
			"aaSorting":  [[6,'desc']]
		});

		$(document).delegate('#tab-product #list-products span.ms-button-delete', 'click', function() {
			var product_id = $(this).data('product_id');
			if(confirm('Are you sure?')) {
				$.ajax({
					dataType: "json",
					data: 'product_id='+product_id,
					url: 'index.php?route=multimerch/seller/jxDeleteProduct&token=<?php echo $token; ?>',
					complete: function (jsonData) {
						$('#list-products').DataTable().ajax.reload();
					}
				});
			}
		});

		$(document).on('click', '.ms-confirm-manually', function(e) {
			e.preventDefault();
			var button = $(this);
			var payment_id = button.closest('tr').find('input[name="payment_id"]').val();

			if(payment_id.length) {
				$.ajax({
					url: 'index.php?route=multimerch/payment/jxConfirmManually&token=<?php echo $token; ?>',
					type: 'post',
					data: {payment_id: payment_id},
					dataType: 'json',
					beforeSend: function () {
						button.button('loading');
					},
					success: function (json) {
						if(json.success) {
							button.button('reset');
							button.parent('td').html(json.success);
						}
					}
				});
			}
		});

		// Commissions
		$(document).on('click', '.override-fees', function (e) {
			e.preventDefault();

			var override = parseInt($(this).closest('div').attr('data-override'));

			if (override == 1) {
				$('#commissions-rates').addClass('hidden');
				$('#commissions-rates input, #commissions-rates select').attr('disabled', true);
				$('.override-note[data-override="0"]').removeClass('hidden');
				$('.override-note[data-override="1"]').addClass('hidden');
			} else {
				$('#commissions-rates').removeClass('hidden');
				$('#commissions-rates input, #commissions-rates select').attr('disabled', false);
				$('.override-note[data-override="0"]').addClass('hidden');
				$('.override-note[data-override="1"]').removeClass('hidden');
			}
		});

		$(document).on('change', '#seller-group-select', function () {
			var seller_group_id = parseInt($(this).val());

			$.ajax({
				url: 'index.php?route=multimerch/seller/jxGetSellerGroupCommission',
				method: 'GET',
				data: {token: "<?php echo $token; ?>", seller_group_id: seller_group_id},
				dataType: 'json',
				success: function (json) {
					if (json.success) {
						$('#override-off-note').html(json.text);
					} else {
						console.error(json.errors);
					}
				}
			});
		});

		$('#seller-group-select').trigger('change');
		// End Commissions

		// Start MSF seller properties
		getMsfSellerProperties();
		$(document).on('change', '#seller-group-select', getMsfSellerProperties);

		function getMsfSellerProperties() {
			var seller_id = $('#seller_id').val();
			var seller_group_id = $('#seller-group-select').val();

			$.ajax({
				url: 'index.php?route=multimerch/seller/jxGetSellerMsfSellerProperties',
				method: 'GET',
				data: {token: "<?php echo $token; ?>", seller_id: seller_id, seller_group_id: seller_group_id},
				dataType: 'json',
				success: function (json) {
					if (json.html) {
						$('#tab-msf-seller-property').html(json.html);
					}
				}
			});
		}
		// End MSF seller properties
	});
</script>

<?php echo $footer; ?>