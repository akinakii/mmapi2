<div class="msf-variations-holder">
	<?php foreach ($msf_variations as $key => $msf_variation) { ?>
		<div class="form-group required <?php if (isset($errors[$msf_variation['id']])) { ?>has-error<?php } ?>">
			<input type="hidden" name="msf_variation_required[]" value="<?php echo $msf_variation['id']; ?>" />

			<label class="col-sm-2 control-label"><?php echo $msf_variation['name']; ?></label>
			<div class="col-sm-10">
				<div class="well well-sm" style="height: 150px; overflow: auto; margin-bottom: 5px;">
					<?php foreach ($msf_variation['values'] as $value) { ?>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="msf_variation_selected[<?php echo $msf_variation['id']; ?>][]" value="<?php echo $value['id']; ?>" class="msf-variation-value" data-index="<?php echo $key; ?>" <?php if (!empty($msf_variation['selected_values']) && in_array($value['id'], $msf_variation['selected_values'])) { ?>checked="checked"<?php } ?> />
								<?php echo $value['name'] ?: ''; ?>
							</label>
						</div>
					<?php } ?>
				</div>

				<?php if (!empty($msf_variation['description'][$this->config->get('config_language_id')]['note'])) { ?>
					<div class="comment" style="font-style: italic;">
						<?php echo $msf_variation['description'][$this->config->get('config_language_id')]['note']; ?>
					</div>
				<?php } ?>

				<?php if (isset($errors[$msf_variation['id']])) { ?>
					<div class="text-danger"><?php echo $errors[$msf_variation['id']]; ?></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<div id="msf-variations-matrix"></div>
</div>