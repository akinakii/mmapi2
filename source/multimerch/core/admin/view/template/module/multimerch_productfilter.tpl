<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="button" id="saveSettings" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="alert alert-danger" id="error-holder" style="display: none;"><i class="fa fa-exclamation-circle"></i></div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $text_default_config; ?></label>
						<div class="col-sm-10">
							<a href="<?php echo $this->url->link('multimerch/field/default-configuration', 'token=' . $this->session->data['token'], 'SSL'); ?>" class="btn btn-primary"><?php echo $text_default_config_link; ?></a>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $text_status; ?></label>
						<div class="col-sm-10">
							<select name="status" id="input-status" class="form-control">
								<option value="1" <?php echo 1 === (int)$status ? 'selected="selected"' : ''; ?>><?php echo $text_enabled; ?></option>
								<option value="0" <?php echo 0 === (int)$status ? 'selected="selected"' : ''; ?>><?php echo $text_disabled; ?></option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		$('#saveSettings').on('click', function (e) {
			e.preventDefault();

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: "<?php echo str_replace('&amp;', '&', $action); ?>",
				data: $('#form').serialize(),
				success: function (json) {
					console.log(json);
					if (json.hasOwnProperty('errors') && json.errors.length) {
						let html_errors = '';
						for (error in json.errors) {
							html_errors += json.errors[error] + '<br/>';
						}

						$('#error-holder').show().find('i').after(" <p style='display: inline-table;'>" + html_errors + "</p>");

						window.scrollTo(0, 0);
					} else if(json.hasOwnProperty('redirect') && json.redirect) {
						window.location.href = json.redirect.replace(/&amp;/g, '&');
					}
				}
			});

		});
	});
</script>

<?php echo $footer; ?>