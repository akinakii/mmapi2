<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="multimerch-tiles">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button id="form-submit" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="alert alert-danger" id="error-holder" style="display: none;"></div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>

				<div class="lang-chooser pull-right">
					<?php foreach ($languages as $language) { ?>
					<img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" width="19" class="select-input-lang <?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_admin_language')) ? 'active' : ''; ?>" data-lang="<?php echo $language['code']; ?>">
					<?php } ?>
				</div>
			</div>

			<div class="panel-body">
				<form method="POST" enctype="multipart/form-data" id="form-multimerch-tiles" class="form-horizontal">
					<div class="form-group" style="display: none;">
						<label class="col-sm-2 control-label"><?php echo $text_module_name; ?></label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" value="<?php echo $module['name']; ?>" />
						</div>
					</div>

					<div class="form-group required">
						<label class="col-sm-2 control-label"><?php echo $text_title; ?></label>
						<div class="col-sm-10">
							<?php foreach ($languages as $language) { ?>
								<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
									<div class="lang-img-icon-input"><img src="<?php echo $language['image']; ?>"></div>
								</div>
								<input type="text" name="i18n[<?php echo $language['language_id']; ?>][title]" value="<?php echo $module['i18n'][$language['language_id']]['title']; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
							<?php } ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_subtitle; ?></label>
						<div class="col-sm-10">
							<?php foreach ($languages as $language) { ?>
								<div class="lang-textarea lang-select-field" data-lang="<?php echo $language['code'] ?>" data-lang-default="<?php echo (int)$language['language_id'] == (int)$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language')) ? 'true' : 'false'; ?>">
									<div class="lang-img-icon-input"><img src="<?php echo $language['image']; ?>"></div>
								</div>
								<input type="text" name="i18n[<?php echo $language['language_id']; ?>][subtitle]" value="<?php echo $module['i18n'][$language['language_id']]['subtitle']; ?>" class="lang-select-field lang-img-icon-text-input form-control mm_input_language mm_flag mm_flag_<?php echo $language['code']; ?>" data-lang="<?php echo $language['code']; ?>" />
							<?php } ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_link; ?></label>
						<div class="col-sm-10">
							<input type="text" name="link" class="form-control" value="<?php echo $module['link']; ?>" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_appearance; ?></label>
						<div class="col-sm-10">
							<select name="type" class="form-control">
								<?php foreach ($types as $type) { ?>
									<option value="<?php echo $type; ?>" <?php if ((string)$type === (string)$module['type']) { ?>selected="selected"<?php } ?>><?php echo ${"module_type_$type"}; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_item_type; ?></label>
						<div class="col-sm-10">
							<select name="item_type" class="form-control">
								<?php foreach ($item_types as $o_type) { ?>
									<option value="<?php echo $o_type; ?>" <?php if ((string)$o_type === (string)$module['item_type']) { ?>selected="selected"<?php } ?>><?php echo ${"module_item_type_$o_type"}; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<input type="hidden" name="num_cols" value="<?php echo $module['num_cols']; ?>" />
					<input type="hidden" name="num_rows" value="<?php echo $module['num_rows']; ?>" />
					<input type="hidden" name="selection_type" value="<?php echo array_shift($selection_types); ?>" />
					<input type="hidden" name="status" value="<?php echo $module['status']; ?>" />

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_items; ?></span></label>
						<div class="col-sm-10">
							<input type="text" name="item_name" value="" placeholder="<?php echo $text_items_placeholder; ?>" class="form-control" />
							<div id="selection" class="well well-sm" style="height: 150px; overflow: auto;">
								<?php if(!empty($module['items'])) { ?>
									<?php foreach ($module['items'] as $item) { ?>
										<div id="item<?php echo $item['id']; ?>">
											<i class="fa fa-minus-circle"></i> <?php echo $item['name']; ?>
											<input type="hidden" name="item_ids[]" value="<?php echo $item['id']; ?>" />
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_status; ?></label>
						<div class="col-sm-10">
							<select name="status" class="form-control">
								<option value="1" <?php if (1 === (string)$module['status']) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
								<option value="0" <?php if (0 === (string)$module['status']) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	var msGlobals = {
		token : "<?php echo $this->session->data['token']; ?>",
		module_id: "<?php echo isset($this->request->get['module_id']) ? $this->request->get['module_id'] : ''; ?>",
		config_enable_rte: "<?php echo $this->config->get('msconf_enable_rte'); ?>",
		current_language: "<?php echo $this->config->get('config_admin_language') ;?>",
		text_none: "<?php echo $text_none; ?>"
	};

	$(function () {
		// Show only selected language's fields
		var lang_inputs = $('.lang-select-field');
		var current_language = msGlobals.current_language;
		for(var i = 0; i < lang_inputs.length; i++) {
			if($(lang_inputs[i]).data('lang') != current_language) {
				$(lang_inputs[i]).hide();
				$(lang_inputs[i]).siblings('.lang-img-icon-input').hide();
			} else {
				$(lang_inputs[i]).show();
				$(lang_inputs[i]).siblings('.lang-img-icon-input').show();
			}
		}

		// Language select
		$(".select-input-lang").on("click", function() {
			var selectedLang = $(this).data('lang');

			$('.lang-select-field').each(function() {
				if ($(this).data('lang') == selectedLang) {
					$(this).show();
					$(this).siblings('.lang-img-icon-input').show();
				} else {
					$(this).hide();
					$(this).siblings('.lang-img-icon-input').hide();
				}
			});

			$('.lang-chooser img').each(function() {
				if ($(this).data('lang') == selectedLang) {
					$(this).addClass('active');
				} else {
					$(this).removeClass('active');
				}
			});
		});

		// Category filters
		$('[name=item_type]').on('change', function () {
			initAutocomplete(false);
		});

		initAutocomplete(true);

		function initAutocomplete(is_init) {
			if (!is_init) {
				$('#selection > div').remove();
			}

			var item_type = $('[name=item_type]').val();
			var url = '';

			switch(item_type) {
				case 'seller':
					// Sellers
					url = 'index.php?route=module/multimerch_tiles/jxAutocompleteSellers&token=' + msGlobals.token
					break;

				case 'oc_category':
					// OC categories
					url = 'index.php?route=module/multimerch_tiles/jxAutocompleteOcCategories&token=' + msGlobals.token
					break;

				case 'product':
				default:
					// Products
					url = 'index.php?route=module/multimerch_tiles/jxAutocompleteProducts&token=' + msGlobals.token
					break;
			}

			// Remove previously created uls
			$('input[name="item_name"]').siblings('ul').remove();

			$('input[name="item_name"]').autocomplete({
				'source': function(request, response) {
					$.ajax({
						url: url + '&name=' +  encodeURIComponent(request),
						dataType: 'json',
						success: function(json) {
							response($.map(json, function(item) {
								return {
									label: item['name'],
									value: item['id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					$('input[name="item_name"]').val('');
					$('#item' + item['value']).remove();

					var filter_row_html = '';
					filter_row_html += '<div id="item' + item['value'] + '">';
					filter_row_html += '	<i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="item_ids[]" value="' + item['value'] + '" />';
					filter_row_html += '</div>';

					$('#selection').append(filter_row_html);
				}
			});
		}

		$('#selection').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});

		$('#form-submit').on('click', function (e) {
			e.preventDefault();

			var data = $('#form-multimerch-tiles').serializeArray();
			data.push({name: 'module_id', value: msGlobals.module_id});

			$.ajax({
				url: 'index.php?route=module/multimerch_tiles/jxSave&token=' + msGlobals.token,
				type: 'POST',
				data: data,
				dataType: 'json',
				success: function (json) {
					if (json.success && json.redirect) {
						window.location.href = json.redirect.replace(/&amp;/g, '&');
					} else if (json.errors) {
						$('#error-holder').empty();
						$('#form-multimerch-tiles').find('.text-danger').remove();
						$('#form-multimerch-tiles').find('div.has-error').removeClass('has-error');

						for (error in json.errors) {
							if ($('[name^="' + error + '"]').length > 0 && $('[name^="' + error + '"]').is(':visible')) {
								$('[name^="' + error + '"]').closest('div').addClass('has-error');
								$('[name^="' + error + '"]').parents('div:first').append('<div class="text-danger" id="error_' + error + '">' + json.errors[error] + '</div>');
							}

							$('#error-holder').append(json.errors[error] + '<BR>').show();
						}

						window.scrollTo(0,0);
					}
				}
			});
		});
	});
</script>

<?php echo $footer; ?>