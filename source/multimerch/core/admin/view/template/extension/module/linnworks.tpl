<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="multimerch-tiles">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form='linnworks-settings' data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="alert alert-danger" id="error-holder" style="display: none;"></div>
		<form method="POST" form="form-affiliate" enctype="multipart/form-data" id='linnworks-settings' class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $text_status; ?></label>
				<div class="col-sm-10">
					<select name="settings[ms_linnworks_enabled]" class="form-control">
						<option value="1" <?php if (1 == (string)$settings['ms_linnworks_enabled']) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
						<option value="0" <?php if (0 == (string)$settings['ms_linnworks_enabled']) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
					</select>
				</div>
			</div>

			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $channel_title; ?></label>
				<div class="col-sm-10">
					<input type="text" name="settings[ms_linnworks_channel_title]" value="<?php echo $settings['ms_linnworks_channel_title']; ?>" id="input-title" class="form-control" />
					<?php if (!empty($error['ms_linnworks_channel_title'])) { ?>
					<div class="text-danger"><?php echo $error['ms_linnworks_channel_title'] ?></div>
					<?php } ?>
				</div>
			</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $channel_name; ?></label>
				<div class="col-sm-10">
					<input type="text" name="settings[ms_linnworks_channel_name]" value="<?php echo $settings['ms_linnworks_channel_name']; ?>" id="input-name" class="form-control" />
					<?php if (!empty($error['ms_linnworks_channel_name'])) { ?>
					<div class="text-danger"><?php echo $error['ms_linnworks_channel_name'] ?></div>
					<?php } ?>
				</div>
			</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label"><?php echo $channel_logo; ?></label>
				<div class="col-sm-10">
					<input type="text" name="settings[ms_linnworks_channel_logo_url]" value="<?php echo $settings['ms_linnworks_channel_logo_url']; ?>" id="input-logo" class="form-control" />
					<?php if (!empty($error['ms_linnworks_channel_logo_url'])) { ?>
					<div class="text-danger"><?php echo $error['ms_linnworks_channel_logo_url'] ?></div>
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $app_manifest; ?></label>
				<div class="col-sm-10">
					<div class="alert alert-info">Use this manifest for your Application in Linnworks Developer:
						<ol>
							<li>Create new Application</li>
							<li>Switch to text editor in "App modules" tab</li>
							<li>Use Application Manifest from field below</li>
						</ol></div>
					<textarea readonly="true" class="form-control" rows='20' onfocus="$(this).select()"><?php echo $manifest_content; ?></textarea>
				</div>
			</div>


		</form>
	</div>
</div>




<?php echo $footer; ?>