<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="button" id="saveSettings" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="alert alert-info"><i class="fa fa-exclamation-circle"></i>
			<?php echo $text_api_keys_info; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="alert alert-danger hidden"><i class="fa fa-exclamation-circle"></i>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>

		<div class="alert alert-success <?php if (empty($success)) { ?>hidden<?php } ?>"><i class="fa fa-check-circle"></i>
			<?php if (!empty($success)) { echo $success; } ?>
			<button type="button" class="close" data-dismiss="alert" class="pull-right">&times;</button>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">
				<form id="settings" class="form-horizontal">
					<fieldset id="general">
						<legend><?php echo $text_general; ?></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_test_mode_note; ?>"><?php echo $text_test_mode; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="radio" name="test_mode" value="1" <?php if($test_mode == 1) { ?> checked="checked" <?php } ?> />
								<?php echo $text_yes; ?>
								<input type="radio" name="test_mode" value="0" <?php if($test_mode == 0) { ?> checked="checked" <?php } ?> />
								<?php echo $text_no; ?>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_sca_note; ?>"><?php echo $text_sca; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="radio" name="sca_required" value="1" <?php if($sca_required == 1) { ?> checked="checked" <?php } ?> />
								<?php echo $text_yes; ?>
								<input type="radio" name="sca_required" value="0" <?php if($sca_required == 0) { ?> checked="checked" <?php } ?> />
								<?php echo $text_no; ?>
							</div>
						</div>

						<input type="hidden" name="debug" value="1" />

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $text_status; ?></label>
							<div class="col-sm-10">
								<select class="form-control" name="status">
									<option value="1" <?php if ($status == 1) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
									<option value="0" <?php if ($status == 0) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
								</select>
							</div>
						</div>
					</fieldset>

					<fieldset id="payment_status">
						<legend><?php echo $text_order_status; ?></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_event_charge_status_succeeded_note; ?>"><?php echo $text_event_charge_status_succeeded; ?></span>
							</label>
							<div class="col-sm-10">
								<?php if (!empty($order_statuses)) { ?>
									<select class="form-control" name="event_to_order_status[charge.succeeded]">
										<?php foreach ($order_statuses as $order_status) { ?>
											<option value="<?php echo $order_status['order_status_id']; ?>" <?php if (!empty($event_to_order_status['charge.succeeded']) && (int)$event_to_order_status['charge.succeeded'] === (int)$order_status['order_status_id']) { ?>selected="selected"<?php } ?>><?php echo $order_status['name']; ?></option>
										<?php } ?>
									</select>
								<?php } else { ?>
									<?php echo $error_no_order_statuses_available; ?>
								<?php } ?>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_event_charge_status_failed_note; ?>"><?php echo $text_event_charge_status_failed; ?></span>
							</label>
							<div class="col-sm-10">
								<?php if (!empty($order_statuses)) { ?>
									<select class="form-control" name="event_to_order_status[charge.failed]">
										<?php foreach ($order_statuses as $order_status) { ?>
											<option value="<?php echo $order_status['order_status_id']; ?>" <?php if (!empty($event_to_order_status['charge.failed']) && (int)$event_to_order_status['charge.failed'] === (int)$order_status['order_status_id']) { ?>selected="selected"<?php } ?>><?php echo $order_status['name']; ?></option>
										<?php } ?>
									</select>
								<?php } else { ?>
									<?php echo $error_no_order_statuses_available; ?>
								<?php } ?>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_event_charge_status_refunded_note; ?>"><?php echo $text_event_charge_status_refunded; ?></span>
							</label>
							<div class="col-sm-10">
								<?php if (!empty($order_statuses)) { ?>
									<select class="form-control" name="event_to_order_status[charge.refunded]">
										<?php foreach ($order_statuses as $order_status) { ?>
											<option value="<?php echo $order_status['order_status_id']; ?>" <?php if (!empty($event_to_order_status['charge.refunded']) && (int)$event_to_order_status['charge.refunded'] === (int)$order_status['order_status_id']) { ?>selected="selected"<?php } ?>><?php echo $order_status['name']; ?></option>
										<?php } ?>
									</select>
								<?php } else { ?>
									<?php echo $error_no_order_statuses_available; ?>
								<?php } ?>
							</div>
						</div>
					</fieldset>

					<fieldset id="api">
						<legend><?php echo $text_api_settings; ?></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_account_currency_note; ?>"><?php echo $text_account_currency; ?></span>
							</label>
							<div class="col-sm-10">
								<?php if (!empty($currencies)) { ?>
									<select class="form-control" name="account_currency">
										<?php foreach ($currencies as $currency) { ?>
											<option value="<?php echo $currency['code']; ?>" <?php if ((string)$account_currency === (string)$currency['code']) { ?>selected="selected"<?php } ?>><?php echo $currency['title']; ?></option>
										<?php } ?>
									</select>
								<?php } else { ?>
									<?php echo $error_no_currencies_available; ?>
								<?php } ?>
							</div>
						</div>

						<div class="form-group required">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_public_key_note; ?>"><?php echo $text_public_key; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][public_key]" value="<?php echo $environments[$live_environment_id]['public_key']; ?>" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][public_key]" value="<?php echo $environments[$test_environment_id]['public_key']; ?>" />
							</div>
						</div>

						<div class="form-group required">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_secret_key_note; ?>"><?php echo $text_secret_key; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][secret_key]" value="<?php echo $environments[$live_environment_id]['secret_key']; ?>" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][secret_key]" value="<?php echo $environments[$test_environment_id]['secret_key']; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_account_webhooks_note; ?>"><?php echo $text_account_webhooks; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][account_webhooks]" value="<?php echo $environments[$live_environment_id]['account_webhooks']; ?>" disabled="disabled" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][account_webhooks]" value="<?php echo $environments[$test_environment_id]['account_webhooks']; ?>" disabled="disabled" />
							</div>
						</div>
					</fieldset>

					<fieldset id="connect">
						<legend><?php echo $text_connect_settings; ?></legend>

						<div class="form-group required">
							<label class="col-sm-2 control-label"><?php echo $text_client_id; ?></label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][client_id]" value="<?php echo $environments[$live_environment_id]['client_id']; ?>" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][client_id]" value="<?php echo $environments[$test_environment_id]['client_id']; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_connect_webhooks_note; ?>"><?php echo $text_connect_webhooks; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][connect_webhooks]" value="<?php echo $environments[$live_environment_id]['connect_webhooks']; ?>" disabled="disabled" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][connect_webhooks]" value="<?php echo $environments[$test_environment_id]['connect_webhooks']; ?>" disabled="disabled" />
							</div>
						</div>

						<div class="form-group" id="charging-approach">
							<label class="col-sm-2 control-label"><?php echo $text_charging_approach; ?></label>
							<div class="col-sm-10">
								<input type="radio" name="charging_approach" value="direct" <?php if($charging_approach == 'direct') { ?> checked="checked" <?php } ?> />
								<?php echo $text_charging_approach_direct; ?>
								<input type="radio" name="charging_approach" value="destination" <?php if($charging_approach == 'destination') { ?> checked="checked" <?php } ?> />
								<?php echo $text_charging_approach_destination; ?>
								<input type="radio" name="charging_approach" value="separate" <?php if($charging_approach == 'separate') { ?> checked="checked" <?php } ?> />
								<?php echo $text_charging_approach_separate; ?>
								<div class="comment" style="font-style: italic; font-size: 11px; color: #b2b2b2; margin-top: 10px;"><?php echo $text_charging_approach_note; ?></div>
							</div>
						</div>
					</fieldset>

					<fieldset id="api">
						<legend><?php echo $text_subscription_settings; ?></legend>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $text_enable_subscription; ?></label>
							<div class="col-sm-10">
								<input type="radio" name="enable_subscription" value="1" <?php if($enable_subscription == 1) { ?> checked="checked" <?php } ?> />
								<?php echo $text_yes; ?>
								<input type="radio" name="enable_subscription" value="0" <?php if($enable_subscription == 0) { ?> checked="checked" <?php } ?> />
								<?php echo $text_no; ?>
							</div>
						</div>

						<div class="form-group <?php if($enable_subscription == 1) { ?> required <?php } ?>">
							<label class="col-sm-2 control-label">
								<span data-toggle="tooltip" title="<?php echo $text_stripe_product_id_note; ?>"><?php echo $text_stripe_product_id; ?></span>
							</label>
							<div class="col-sm-10">
								<input type="text" class="form-control <?php if($test_mode == 1) { ?>hidden<?php } ?>" name="environments[<?php echo $live_environment_id; ?>][product_id]" value="<?php echo $environments[$live_environment_id]['product_id']; ?>" />
								<input type="text" class="form-control <?php if($test_mode == 0) { ?>hidden<?php } ?>" name="environments[<?php echo $test_environment_id; ?>][product_id]" value="<?php echo $environments[$test_environment_id]['product_id']; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $text_retrieve_plans; ?></label>
							<div class="col-sm-10">
								<input type="button" class="btn btn-success" id="retrieve-plans" value="<?php echo $text_retrieve; ?>" />
								<div class="alert hidden" id="retrieve-plans-info" style="margin-top: 15px;"></div>

								<ul class="list-group <?php if (empty($plans_info)) { ?>hidden<?php } ?>" id="retrieve-plans-list" style="margin-top: 15px;">
									<?php if (!empty($plans_info)) { ?>
										<?php foreach ($plans_info as $plan) { ?>
											<li class="list-group-item">
												<strong><?php echo $plan['nickname']; ?></strong><?php echo !empty($plan['condition_base']) ? (' - ' . $plan['condition_base']) : ''; ?><?php echo !empty($plan['condition_per_seat']) ? (' + ' . $plan['condition_per_seat']) : ''; ?>
											</li>
										<?php } ?>
									<?php } ?>
								</ul>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	var msGlobals = {
		token: "<?php echo $token; ?>",
		ms_stripe_connect_environment_live: "<?php echo $live_environment_id; ?>",
		ms_stripe_connect_environment_test: "<?php echo $test_environment_id; ?>",
		error_product_id: "<?php echo $error_product_id; ?>"
	};

	$(function() {
		$("#saveSettings").click(function() {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: 'index.php?route=payment/ms_stripe_connect/saveSettings&token=' + msGlobals.token,
				data: $('#settings').serialize(),
				beforeSend: function () {
					$.map($('.alert-danger').find('p'), function (p) {
						$(p).remove();
					});
				},
				success: function (jsonData) {
					if (jsonData.errors && jsonData.errors.length > 0) {
						var errors_html = '';
						for (error in jsonData.errors) {
							if (!jsonData.errors.hasOwnProperty(error)) {
								continue;
							}
							errors_html += jsonData.errors[error] + '<br>';
						}
						$('.alert-danger').removeClass('hidden').find('i').after(" <p style='display: inline-table;'>"+errors_html+"</p>");
					} else {
						window.location.reload();
					}
				}
			});
		});

		$(document).on('click', 'input[name="test_mode"]', function () {
			var live_settings = $(document).find('input[name^="environments[' + msGlobals.ms_stripe_connect_environment_live + ']"]');
			var test_settings = $(document).find('input[name^="environments[' + msGlobals.ms_stripe_connect_environment_test + ']"]');

			$.map($(this).val() == 0 ? test_settings : live_settings, function (setting) {
				$(setting).addClass('hidden');
			});

			$.map($(this).val() == 0 ? live_settings : test_settings, function (setting) {
				$(setting).removeClass('hidden');
			});
		});

		$(document).on('click', 'input[name="enable_subscription"]', function () {
			if ($(this).val() == 1) {
				$(this).closest('fieldset').find('div.form-group:nth-of-type(2)').addClass('required');
			} else {
				$(this).closest('fieldset').find('div.form-group:nth-of-type(2)').removeClass('required');
			}
		});

		$('#retrieve-plans').on('click', function (e) {
			e.preventDefault();

			var selected_environment_id = $('input[name="test_mode"]:checked').val();

			var $button = $(this);

			// Delete all remaining errors
			$.map($('.alert-danger').find('p'), function (p) {
				$(p).remove();
			});

			// Delete all remaining plans in a list and hide it
			$('#retrieve-plans-list').empty().addClass('hidden');

			var product_id = $('input[name="environments[' + selected_environment_id + '][product_id]"]').val();

			if (!product_id) {
				$('#retrieve-plans-info').removeClass('hidden').addClass('alert-danger').html(" <p style='display: inline-table;'>" + msGlobals.error_product_id + "</p>");
				return;
			}

			// Send request to retrieve all plans from Stripe
			$.ajax({
				type: "GET",
				dataType: "json",
				url: 'index.php?route=payment/ms_stripe_connect/jxRetrievePlans&token=' + msGlobals.token + '&product_id=' + product_id,
				beforeSend: function () {
					$.map($('#retrieve-plans-info').find('p'), function (p) {
						$(p).remove();
					});
					$('#retrieve-plans-info').addClass('hidden').removeClass('alert-danger alert-success');

					$button.button('loading').attr('disabled', true);
				},
				success: function (jsonData) {
					$button.button('reset').attr('disabled', false);

					if (jsonData.errors && jsonData.errors.length > 0) {
						for (error in jsonData.errors) {
							if (!jsonData.errors.hasOwnProperty(error)) {
								continue;
							}
							$('#retrieve-plans-info').removeClass('hidden').addClass('alert-danger').html(" <p style='display: inline-table;'>" + jsonData.errors[error] + "</p>");
						}
					} else if (jsonData.success) {
						$('#retrieve-plans-info').removeClass('hidden').addClass('alert-success').html(" <p style='display: inline-table;'>" + jsonData.errors[error] + "</p>");

						if (jsonData.plans_info) {
							$('#retrieve-plans-list').empty();

							for (plan in jsonData.plans_info) {
								var li = "<li class='list-group-item'>";
								li += "<strong>" + jsonData.plans_info[plan].nickname + "</strong>";
								li += jsonData.plans_info[plan].condition_base ? (" - " + jsonData.plans_info[plan].condition_base) : "";
								li += jsonData.plans_info[plan].condition_per_seat ? (" + " + jsonData.plans_info[plan].condition_per_seat) : "";
								li += "</li>";
								$('#retrieve-plans-list').append(li);
							}

							$('#retrieve-plans-list').removeClass('hidden');
						}
					}
				}
			});
		});

		$(document).on('change', '[name="sca_required"]', function () {
			console.log($(this).val())
			if ($(this).val() == 1) {
				$('#charging-approach').hide();
			} else {
				$('#charging-approach').show();
			}
		});
		$('[name="sca_required"]:checked').trigger('change');
	});
</script>

<?php echo $footer; ?>