<?php
$_['heading_title'] = 'MultiMerch Linnworks integration';
$_['text_modules'] = 'Modules';
$_['text_status'] = 'Enable integration for sellers';
$_['channel_name'] = 'Integration channel name';
$_['channel_title'] = 'Integration channel title';
$_['channel_logo'] = 'Logo URL (57x57 max.)';
$_['app_manifest'] = 'Integration Application manifest';
$_['error_channel_name'] = 'Channel name length must be between 5 and 20 characters';
$_['error_channel_title'] = 'Channel title length must be between 5 and 50 characters';
$_['error_channel_logo'] = 'Specify logo url!';
$_['text_success'] = 'Settings saved!';
