<?php
$_['heading_title'] = "Stripe Connect for MultiMerch";
$_['text_ms_stripe_connect'] = "<a href='//stripe.com/connect'  target='_blank'><img src='view/image/payment/stripe_connect.png' alt='Stripe Connect' title='Stripe Connect' style='border: 1px solid #EEEEEE;' /></a>";

$_['text_payment'] = "Payment";
$_['text_api_keys_info'] = "To fill out API settings visit <a href='//dashboard.stripe.com/account/apikeys' target='_blank'>API keys</a> and <a href='//dashboard.stripe.com/account/applications/settings' target='_blank'>Connect Settings</a> sections of Stripe Dashboard";

$_['text_general'] = "General";
$_['text_test_mode'] = "Test Mode";
$_['text_test_mode_note'] = "In test mode, payments are not processed by card networks or payment providers, and only test payment information can be used";
$_['text_debug'] = "Debug Mode";
$_['text_debug_note'] = "Enable detailed information logging";
$_['text_sca'] = "Require SCA";
$_['text_sca_note'] = "As of September 2019, a regulation called Strong Customer Authentication (SCA) requires businesses in Europe to request additional customer authentication for online payments.";

$_['text_status'] = "Status";
$_['text_account_currency'] = "Account currency";
$_['text_account_currency_note'] = "Currency associated with this Stripe account";

$_['text_order_status'] = "Order statuses";
$_['text_event_charge_status_succeeded'] = "Charge succeeded";
$_['text_event_charge_status_succeeded_note'] = "Orders status after the charge is successfully processed by Stripe";
$_['text_event_charge_status_failed'] = "Charge failed";
$_['text_event_charge_status_failed_note'] = "Order status after the charge is failed to be processed by Stripe";
$_['text_event_charge_status_refunded'] = "Charge refunded";
$_['text_event_charge_status_refunded_note'] = "Order status after the charge is refunded";

$_['text_api_settings'] = "API settings";
$_['text_public_key'] = "Publishable key";
$_['text_public_key_note'] = "Publishable API keys are meant solely to identify your account with Stripe";
$_['text_secret_key'] = "Secret key";
$_['text_secret_key_note'] = "Secret API key can perform any API request to Stripe without restriction";
$_['text_account_webhooks'] = "Account webhooks";
$_['text_account_webhooks_note'] = "Endpoints receiving events from your account";

$_['text_connect_settings'] = "Stripe Connect settings";
$_['text_client_id'] = "Client ID";
$_['text_connect_webhooks'] = "Connect webhooks";
$_['text_connect_webhooks_note'] = "Endpoints receiving events from Connect applications";
$_['text_signing_secret'] = "Signing secret";
$_['text_charging_approach'] = "Charging approach";
$_['text_charging_approach_note'] = "<strong>Direct charges</strong> allows to make charges directly on the connected account and take fees in the process.<br/><strong>Destination charges</strong> allows to make charges through the platform account on behalf of connected accounts and take fees in the process.<br/><strong>Seperate charges and transfers</strong> allows to make charges on your platform account on behalf of connected accounts, perform transfers separately, and retain funds in the process.<br/>Creating separate charges and transfers is only supported when both the platform and the connected account are in the same region: both in Europe or both in the U.S.";
$_['text_charging_approach_direct'] = "Direct charges";
$_['text_charging_approach_destination'] = "Destination charges";
$_['text_charging_approach_separate'] = "Seperate charges and transfers";

$_['text_subscription_settings'] = "Subscriptions";
$_['text_enable_subscription'] = "Enable subscriptions";
$_['text_stripe_product_id'] = "Stripe product ID";
$_['text_stripe_product_id_note'] = "Create a product in your Stripe dashboard (Billing > Products section) and copy its id here";
$_['text_retrieve_plans'] = "Retrieve plans from Stripe";
$_['text_retrieve'] = "Retrieve";
$_['text_plan_condition_base'] = "%s %s";
$_['text_plan_condition_per_seat'] = "%s per product %s";
$_['text_plan_interval_day'] = "Daily";
$_['text_plan_interval_month'] = "Monthly";
$_['text_plan_interval_year'] = "Yearly";

$_['error_account_currency'] = "You must select default account currency";
$_['error_public_key'] = "You must fill the 'Publishable key' field";
$_['error_secret_key'] = "You must fill the 'Secret key' field";
$_['error_client_id'] = "You must fill the 'Client ID' field";
$_['error_signing_secret'] = "You must fill the 'Signing secret' field";
$_['error_product_id'] = "You must specify valid 'Stripe product ID'!";
$_['error_no_currencies_available'] = "No available currencies!";
$_['error_no_order_statuses_available'] = "No available order statuses!";
$_['error_charging_approach'] = "You must select a charging approach";

$_['success_modified'] = 'Success: You have modified [MultiMerch] Stripe Connect module!';
$_['success_plans_retrieved'] = 'Success: Plans were successfully retrieved from Stripe!';
