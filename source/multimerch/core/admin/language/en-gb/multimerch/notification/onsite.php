<?php

$_['msn_onsite_admin_updated_suborder_status'] = "Order <a href='%s' class='order'>#%s</a> status has been changed to <strong>%s</strong>.";

$_['msn_onsite_customer_created_account'] = "New customer has signed up: <a href='%s' class='customer'>%s</a>.";
$_['msn_onsite_customer_created_message'] = "Customer <a href='%s' class='customer'>%s</a> has replied to the conversation <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_customer_created_order'] = "Customer <a href='%s' class='customer'>%s</a> has placed a new order: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_guest_created_order'] = "Guest has placed a new order: <a href='%s' class='order'>#%s</a>.";

$_['msn_onsite_seller_created_account'] = "New seller has signed up: <a href='%s' class='seller'>%s</a>.";
$_['msn_onsite_seller_updated_invoice_status'] = "Seller <a href='%s' class='seller'>%s</a> has paid the <a href='%s' class='catalog'><strong>invoice</strong></a> <strong>#%s</strong>.";
$_['msn_onsite_seller_created_message'] = "Seller <a href='%s' class='seller'>%s</a> has replied to the conversation <a href='%s'><strong>%s</strong></a>.";
$_['msn_onsite_seller_created_product'] = "Seller <a href='%s' class='seller'>%s</a> has published a new product <a href='%s' class='product'>%s</a>.";
