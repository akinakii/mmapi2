<?php

$_['msn_mail_subject_admin_updated_suborder_status'] = "Your order #%s has been updated by %s";
$_['msn_mail_admin_updated_suborder_status'] = <<<EOT
Order at %s has been updated by %s:

Order#: %s

Products:
%s

Status: %s

Comment:
%s
EOT;

$_['msn_mail_subject_admin_created_message'] = "New message received";
$_['msn_mail_admin_created_message'] = <<<EOT
You have received a new message from %s!

%s

%s

You can reply in the messaging area in your account.
EOT;

$_['msn_mail_subject_admin_created_payout'] = "New payout from %s";
$_['msn_mail_admin_created_payout'] = <<<EOT
You have just received a new payout from %s.
EOT;

$_['msn_mail_subject_admin_updated_account_status'] = "Seller account modified";
$_['msn_mail_admin_updated_account_status'] = <<<EOT
Your seller account at %s has been modified by the administrator.

Account status: %s
EOT;
