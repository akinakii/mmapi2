<?php

$_['heading_title'] = 'MultiMerch Tiles';
$_['text_module'] = 'Modules';

$_['module_type_grid'] = "Grid";
$_['module_type_slider'] = "Slider";

$_['module_item_type_seller'] = "Sellers";
$_['module_item_type_product'] = "Products";
$_['module_item_type_oc_category'] = "Marketplace categories";

$_['text_module_name'] = "Module name";
$_['text_title'] = "Title";
$_['text_subtitle'] = "Subtitle";
$_['text_link'] = "Link";
$_['text_appearance'] = "Appearance";
$_['text_item_type'] = "Type";
$_['text_items'] = "Items";
$_['text_items_placeholder'] = "Select items to show";
$_['text_status'] = "Status";

$_['error_title'] = "You must specify a title (%s)";
$_['error_items'] = "You must select items to show!";