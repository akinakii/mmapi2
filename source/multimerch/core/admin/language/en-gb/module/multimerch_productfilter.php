<?php

$_['heading_title'] = "MultiMerch Product Filter Module";
$_['text_module'] = "Modules";

$_['text_default_config'] = "Default config";
$_['text_default_config_link'] = "Enable / Disable blocks in filter";
$_['text_status'] = "Status";

$_['error_status'] = "You must set module status!";
