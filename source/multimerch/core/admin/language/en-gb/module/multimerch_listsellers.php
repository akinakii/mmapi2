<?php

$_['heading_title']    = '[MultiMerch] List Sellers Module Addon (Deprecated)';
$_['text_module']         = 'Modules';

$_['ms_config_listsellers'] = 'List sellers';

$_['ms_orderby'] = 'Order By';
$_['ms_date_created'] = 'Date Created';

$_['text_nickname'] = 'Nickname';
$_['text_earnings'] = 'Earnings';

$_['text_ascending'] = 'Ascending ';
$_['text_descending'] = 'Descending';

$_['error_permission']  = "Warning: You do not have permission to modify 'List sellers' module!";
$_['error_name'] = "Name must be between 3 and 64 characters!";
$_['error_limit'] = "Limit must be an integer!";
$_['error_width'] = "Width must be an integer!";
$_['error_height'] = "Height must be an integer!";
