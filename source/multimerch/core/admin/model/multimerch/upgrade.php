<?php

use \MultiMerch\Core\Shipping\Shipping;

class ModelMultimerchUpgrade extends Model {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->model('localisation/language');
		$this->load->language('multiseller/multiseller');
	}

	public function getDbVersion() {
		$res = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "ms_db_schema'");
		if (!$res->num_rows) return '0.0.0.0';
		
		$res = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_db_schema` ORDER BY schema_change_id DESC LIMIT 1");

		if ($res->num_rows)
			return $res->row['major'] . '.' . $res->row['minor'] . '.' . $res->row['build'] . '.' . $res->row['revision'];
		else
			return '0.0.0.0';
	}
	
	public function isDbLatest() {
		return version_compare($this->MsLoader->dbVer, $this->getDbVersion()) > 0 ? false : true;
	}

	public function isFilesLatest() {
		return version_compare($this->MsLoader->dbVer, $this->getDbVersion()) < 0 ? false : true;
	}

	private function _createSchemaEntry($version) {
		$schema = explode(".", $version);
		$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_db_schema` (major, minor, build, revision, date_applied) VALUES({$schema[0]},{$schema[1]},{$schema[2]},{$schema[3]}, NOW())");
	}
	
	public function upgradeDb() {
		/**	@var	array	$upgrade_info	Array of messages after certain MultiMerch update.
		 *
		 * Its structure:
		 * array(
		 * 	'app_version' => array('message 1', 'message 2', ...)
		 * )
		 * */
		$upgrade_info = array();

		$version = $this->getDbVersion();

		if (version_compare($version, '1.0.0.0') < 0) {
			$this->db->query("
			CREATE TABLE `" . DB_PREFIX . "ms_db_schema` (
				`schema_change_id` int(11) NOT NULL AUTO_INCREMENT,
				`major` TINYINT NOT NULL,
				`minor` TINYINT NOT NULL,
				`build` TINYINT NOT NULL,
				`revision` SMALLINT NOT NULL,
				`date_applied` DATETIME NOT NULL,
			PRIMARY KEY (`schema_change_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_suborder` (
			`suborder_id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`seller_id` int(11) NOT NULL,
			`order_status_id` int(11) NOT NULL,
			PRIMARY KEY (`suborder_id`)
			) DEFAULT CHARSET=utf8");

			$this->_createSchemaEntry('1.0.0.0');
		}

		if (version_compare($version, '1.0.1.0') < 0) {
			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_seller` ADD (
				`banner` VARCHAR(255) DEFAULT NULL)");

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multiseller/addon');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multiseller/addon');

			$this->_createSchemaEntry('1.0.1.0');
		}

		if (version_compare($version, '1.0.2.0') < 0) {
			$this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_suborder_history` (
				`suborder_history_id` int(5) NOT NULL AUTO_INCREMENT,
				`suborder_id` int(5) NOT NULL,
				`order_status_id` int(5) NOT NULL,
				`comment` text NOT NULL DEFAULT '',
				`date_added` datetime NOT NULL,
				PRIMARY KEY (`suborder_history_id`)
				) DEFAULT CHARSET=utf8");

			$this->_createSchemaEntry('1.0.2.0');
		}

		if (version_compare($version, '1.0.2.1') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multiseller/debug');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multiseller/debug');

			$this->_createSchemaEntry('1.0.2.1');
		}

		if (version_compare($version, '1.0.2.2') < 0) {
			$this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_setting` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`seller_id` int(11) unsigned DEFAULT NULL,
				`seller_group_id` int(11) unsigned DEFAULT NULL,
				`name` varchar(50) DEFAULT NULL,
				`value` varchar(250) DEFAULT NULL,
				`is_encoded` smallint(1) unsigned DEFAULT NULL,
				PRIMARY KEY (`id`)
				) DEFAULT CHARSET=utf8;");

			$this->_createSchemaEntry('1.0.2.2');
		}

		if (version_compare($version, '1.0.3.1') < 0) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_order_product_data` ADD `order_product_id` int(11) DEFAULT NULL AFTER `product_id`");

			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_balance` ADD `order_product_id` int(11) DEFAULT NULL AFTER `product_id`");

			$this->_createSchemaEntry('1.0.3.1');
		}

		if (version_compare($version, '1.0.3.2') < 0) {
			$this->db->query("
				CREATE UNIQUE INDEX slr_id_name
				ON " . DB_PREFIX . "ms_setting (seller_id, name)");


			//replace data from ms_seller to ms_setting
			$getDataQuery = "SELECT seller_id, company, website FROM " . DB_PREFIX . "ms_seller WHERE 1 GROUP BY seller_id";
			$seller_data = $this->db->query($getDataQuery)->rows;
			foreach ($seller_data as $row) {
				$company = $row['company'];
				$website = $row['website'];
				$seller_id = $row['seller_id'];
//                $seller_group = $this->MsLoader->MsSellerGroup->getSellerGroupBySellerId($seller_id);

				$insertDataQuery =
					"INSERT INTO " . DB_PREFIX . "ms_setting
					SET seller_id = " . (int)$seller_id . ", name = 'slr_company', value = '" . $this->db->escape($company) . "'
					ON DUPLICATE KEY UPDATE
					value = '" . $this->db->escape($company) . "'";
				$this->db->query($insertDataQuery);

				$insertDataQuery =
					"INSERT INTO " . DB_PREFIX . "ms_setting
					SET seller_id = " . (int)$seller_id . ", name = 'slr_website', value = '" . $this->db->escape($website) . "'
					ON DUPLICATE KEY UPDATE
					value = '" . $this->db->escape($website) . "'";
				$this->db->query($insertDataQuery);
			}

			$this->_createSchemaEntry('1.0.3.2');
		}

		if (version_compare($version, '1.0.4.0') < 0) {
			/*ADD `total` decimal(15,4) NOT NULL AFTER `invoice_no`,*/
			$suborderSql = "ALTER TABLE " . DB_PREFIX . "ms_suborder
				ADD `invoice_no` int(11) NOT NULL DEFAULT '0' AFTER `seller_id`,
				ADD `invoice_prefix` varchar(26) NOT NULL DEFAULT '' AFTER `invoice_no`,
				ADD `date_added` datetime NOT NULL AFTER `order_status_id`,
				ADD `date_modified` datetime NOT NULL AFTER `date_added`";
			$this->db->query($suborderSql);

			$orderProductSql = "ALTER TABLE " . DB_PREFIX . "ms_order_product_data ADD `suborder_id` int(11) NOT NULL DEFAULT '0'";
			$this->db->query($orderProductSql);

			$sqlOrders = "SELECT * FROM " . DB_PREFIX . "order WHERE 1";
			$ordersData = $this->db->query($sqlOrders);
			foreach ($ordersData->rows as $row) {
				$order_id = $row['order_id'];
				$dateAdded = $row['date_added'];
				$dateModified = $row['date_modified'];
				$customerSql = "SELECT seller_id FROM " . DB_PREFIX . "ms_suborder WHERE order_id = " . (int)$order_id . " GROUP BY seller_id";
				$seller_ids = $this->db->query($customerSql)->rows;

				foreach ($seller_ids as $seller_id) {
					//$total = $this->MsLoader->MsOrderData->getOrderTotal($order_id, $seller_id);
					/*total = '" . $total . "'*/
					$sqlSubOrders = "UPDATE " . DB_PREFIX . "ms_suborder
					SET date_added = '" . $dateAdded . "',
					date_modified = '" . $dateModified . "'
					WHERE order_id = " . (int)$order_id . " AND seller_id = " . (int)$seller_id['seller_id'];
					$this->db->query($sqlSubOrders);
				}
			}

			$sqlSubOrders = "SELECT suborder_id, order_id, seller_id FROM " . DB_PREFIX . "ms_suborder WHERE 1";
			$subOrders = $this->db->query($sqlSubOrders)->rows;
			foreach ($subOrders as $subOrder) {
				$sqlOrderProduct = "UPDATE " . DB_PREFIX . "ms_order_product_data
					SET suborder_id = " . (int)$subOrder['suborder_id'] . " WHERE
					seller_id = " . (int)$subOrder['seller_id'] . " AND order_id = " . (int)$subOrder['order_id'];
				$this->db->query($sqlOrderProduct);
			}

			$this->_createSchemaEntry('1.0.4.0');
		}

		if (version_compare($version, '1.0.4.1') < 0) {
			$layout_id = $this->db->query("SELECT layout_id FROM " . DB_PREFIX . "layout WHERE `name` = 'Account'")->row['layout_id'];
			$sql = "INSERT INTO " . DB_PREFIX . "layout_module (`layout_id`, `code`, `position`, `sort_order`) VALUES($layout_id, 'account', 'column_left', 1);";
			$this->db->query($sql);

			$account = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code`='account' AND `key`='account_status'")->row;
			if (empty($account)) {
				$sql = "INSERT INTO " . DB_PREFIX . "setting SET `store_id` = 0, `code` = 'account', `key` = 'account_status', `value` = 1, `serialized` = 0";
			} else {
				$sql = "UPDATE " . DB_PREFIX . "setting SET `store_id` = 0, `code` = 'account', `key` = 'account_status', `value` = 1, `serialized` = 0 WHERE `setting_id` = " . (int)$account['setting_id'];
			}

			$this->db->query($sql);

			$this->_createSchemaEntry('1.0.4.1');
		}

		if (version_compare($version, '2.0.0.0') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_badge` (
			`badge_id` int(11) NOT NULL AUTO_INCREMENT,
			`image` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`badge_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_badge_description` (
			`badge_id` int(11) NOT NULL,
			`name` varchar(32) NOT NULL DEFAULT '',
			`description` text NOT NULL,
			`language_id` int(11) NOT NULL,
			PRIMARY KEY (`badge_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_badge_seller_group` (
			`badge_seller_group_id` INT(11) NOT NULL AUTO_INCREMENT,
			`badge_id` INT(11) NOT NULL,
			`seller_id` int(11) DEFAULT NULL,
			`seller_group_id` int(11) DEFAULT NULL,
			PRIMARY KEY (`badge_seller_group_id`)) default CHARSET=utf8");


			/* social links */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_channel` (
			`channel_id` int(11) NOT NULL AUTO_INCREMENT,
			`image` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`channel_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_channel_description` (
			`channel_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` VARCHAR(32) NOT NULL DEFAULT '',
			`description` TEXT NOT NULL DEFAULT '',
			PRIMARY KEY (`channel_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_channel` (
			`seller_id` int(11) NOT NULL,
			`channel_id` int(11) NOT NULL,
			`channel_value` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`seller_id`, `channel_id`)) default CHARSET=utf8");

			/* messaging */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_conversation` (
			`conversation_id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) DEFAULT NULL,
			`order_id` int(11) DEFAULT NULL,
			`title` varchar(256) NOT NULL DEFAULT '',
			`date_created` DATETIME NOT NULL,
			PRIMARY KEY (`conversation_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_message` (
			`message_id` int(11) NOT NULL AUTO_INCREMENT,
			`conversation_id` int(11) NOT NULL,
			`from` int(11) DEFAULT NULL,
			`to` int(11) DEFAULT NULL,
			`message` text NOT NULL DEFAULT '',
			`read` tinyint(1) NOT NULL DEFAULT 0,
			`date_created` DATETIME NOT NULL,
			PRIMARY KEY (`message_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.0.0.0');
		}

		if (version_compare($version, '2.0.0.1') < 0) {
			//Questions
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."ms_question` (
			`question_id` int(11) NOT NULL AUTO_INCREMENT,
			`author_id` int(11) NOT	NULL,
			`product_id` int(11) NOT NULL,
			`text` text NOT NULL DEFAULT '',
			`date_created` DATETIME NOT NULL,
			PRIMARY KEY (`question_id`)) DEFAULT CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_answer` (
			`answer_id` int(11) NOT NULL AUTO_INCREMENT,
			`question_id` int(11) NOT NULL,
			`author_id` int(11) NOT NULL,
			`date_created` DATETIME NOT NULL,
			`rating` int(11) DEFAULT NULL,
			`text` text NOT NULL DEFAULT '',
			PRIMARY KEY (`answer_id`)) DEFAULT CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_user_vote` (
			`answer_id` int(11) NOT NULL,
			`user_id` int(11) NOT NULL,
			`type` tinyint(1) NOT NULL) DEFAULT CHARSET=utf8");

			/*Index part for questions */
			$this->db->query("
				CREATE UNIQUE INDEX user
				ON " . DB_PREFIX ."ms_user_vote (user_id, answer_id)");

			$this->db->query("
				CREATE INDEX question_id
				ON " . DB_PREFIX ."ms_answer (question_id)");

			$this->db->query("
				CREATE INDEX answer_id
				ON " . DB_PREFIX ."ms_answer (answer_id)");

			$this->db->query("
				CREATE INDEX product_id
				ON " . DB_PREFIX ."ms_question (product_id)");

			$this->_createSchemaEntry('2.0.0.1');
		}

		if(version_compare($version, '2.0.0.2') < 0) {
			// Ratings/reviews
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_review` (
			`review_id` int(11) NOT NULL AUTO_INCREMENT,
			`author_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`order_product_id` int(11) NOT NULL,
			`order_id` int(11) DEFAULT NULL,
			`rating` int(1) NOT NULL,
			`title` varchar(128) NOT NULL DEFAULT '',
			`comment` text NOT NULL DEFAULT '',
			`description_accurate` int(1) NOT NULL,
			`helpful` int(11) DEFAULT NULL,
			`unhelpful` int(11) DEFAULT NULL,
			`date_created` DATETIME NOT NULL,
			`date_updated` DATETIME DEFAULT NULL,
			`status` tinyint DEFAULT 0,
			PRIMARY KEY (`review_id`)) default CHARSET=utf8");

			$this->db->query("CREATE UNIQUE INDEX idx_ms_review_order_product ON `" . DB_PREFIX ."ms_review` (order_id, product_id, order_product_id)");

			// drop indexes
			$this->db->query("DROP INDEX `user` ON `" . DB_PREFIX ."ms_user_vote`");
			$this->db->query("DROP INDEX `question_id` ON `" . DB_PREFIX ."ms_answer`");
			$this->db->query("DROP INDEX `answer_id` ON `" . DB_PREFIX ."ms_answer`");
			$this->db->query("DROP INDEX `product_id` ON `" . DB_PREFIX ."ms_question`");

			$this->_createSchemaEntry('2.0.0.2');
		}

		if(version_compare($version, '2.0.0.3') < 0) {
			// Shipping methods
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_shipping_method` (
			`shipping_method_id` int(11) NOT NULL AUTO_INCREMENT,
			`logo` TEXT DEFAULT '',
			`status` tinyint(1) NOT NULL DEFAULT 0,
			PRIMARY KEY (`shipping_method_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_shipping_method_description` (
			`shipping_method_description_id` int(11) NOT NULL AUTO_INCREMENT,
			`shipping_method_id` int(11) NOT NULL,
			`name` VARCHAR(32) NOT NULL DEFAULT '',
			`description` TEXT DEFAULT '',
			`language_id` int(11) DEFAULT NULL,
			PRIMARY KEY (`shipping_method_description_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_shipping_delivery_time` (
			`delivery_time_id` int(11) NOT NULL AUTO_INCREMENT,
			PRIMARY KEY (`delivery_time_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_shipping_delivery_time_description` (
			`delivery_time_desc_id` int(11) NOT NULL AUTO_INCREMENT,
			`delivery_time_id` int(11) NOT NULL,
			`name` TEXT DEFAULT '',
			`language_id` int(11) NOT NULL DEFAULT 1,
			PRIMARY KEY (`delivery_time_desc_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_product_shipping` (
			`product_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`from_country` int(11) NOT NULL DEFAULT 0,
			`free_shipping` int(11) NOT NULL DEFAULT 0,
			`processing_time` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`product_shipping_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_product_shipping_location` (
			`product_shipping_location_id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`to_country` int(11) NOT NULL,
			`shipping_method_id` int(11) NOT NULL DEFAULT 0,
			`delivery_time_id` int(11) NOT NULL DEFAULT 1,
			`cost` DECIMAL(15,4) NOT NULL,
			`additional_cost` DECIMAL(15,4) NOT NULL,
			PRIMARY KEY (`product_shipping_location_id`)) default CHARSET=utf8");

			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_order_product_data` 
			ADD `shipping_location_id` int(11) DEFAULT NULL,
			ADD `shipping_cost` DECIMAL(15,4) DEFAULT NULL");

			// MM Order total module install
			$this->db->query("INSERT INTO " . DB_PREFIX . "extension SET `type` = 'total', `code` = 'mm_shipping_total'");

			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('mm_shipping_total', array(
				'mm_shipping_total_status' => 1,
				'mm_shipping_total_sort_order' => 1
			));

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/shipping-method');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/shipping-method');

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'total/mm_shipping_total');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'total/mm_shipping_total');

			$this->_createSchemaEntry('2.0.0.3');
		}

		if(version_compare($version, '2.0.0.4') < 0) {
			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_product_shipping_location`
			CHANGE `to_country` `to_geo_zone_id` int(11) NOT NULL;");

			$this->_createSchemaEntry('2.0.0.4');
		}

		if(version_compare($version, '2.0.0.5') < 0) {
			// Create new table for order product shipping data
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_order_product_shipping_data` (
			`order_product_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`order_product_id` int(11) NOT NULL,
			`shipping_location_id` int(11) DEFAULT NULL,
			`shipping_cost` DECIMAL(15,4) DEFAULT NULL,
			PRIMARY KEY (`order_product_shipping_id`)) default CHARSET=utf8");

			// Get shipping data from oc_ms_order_product_data
			$sql = "SELECT
						order_id,
						product_id,
						order_product_id,
						shipping_location_id,
						shipping_cost
					FROM " . DB_PREFIX . "ms_order_product_data";
			$old_shipping_data = $this->db->query($sql);

			foreach ($old_shipping_data->rows as $row) {
				$this->db->query("
				INSERT INTO " . DB_PREFIX . "ms_order_product_shipping_data
				SET
					order_id = " . (int)$row['order_id'] . ",
					product_id = " . (int)$row['product_id'] . ",
					order_product_id = " . (int)$row['order_product_id'] . ",
					shipping_location_id = " . (is_null($row['shipping_location_id']) ? "NULL" : (int)$row['shipping_location_id']) . ",
					shipping_cost = " . (is_null($row['shipping_cost']) ? "NULL" : (float)$row['shipping_cost']));
			}

			// Drop shipping columns from ms_order_product_data
			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_order_product_data
			DROP COLUMN shipping_location_id,
			DROP COLUMN shipping_cost;");

			$this->_createSchemaEntry('2.0.0.5');
		}

		if(version_compare($version, '2.0.1.0') < 0) {
			// Copy everything from oc_ms_payment to oc_ms_pg_payment and oc_ms_pg_request. And then delete oc_ms_payment
			/* Payment gateways, requests and payments */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_pg_payment` (
			`payment_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`payment_type` int(11) NOT NULL,
			`payment_code` VARCHAR(128) NOT NULL,
			`payment_status` int(11) NOT NULL,
			`amount` DECIMAL(15,4) NOT NULL,
			`currency_id` int(11) NOT NULL,
			`currency_code` VARCHAR(3) NOT NULL,
			`sender_data` TEXT NOT NULL,
			`receiver_data` TEXT NOT NULL,
			`description` TEXT NOT NULL,
			`date_created` DATETIME NOT NULL,
			PRIMARY KEY (`payment_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_pg_request` (
			`request_id` int(11) NOT NULL AUTO_INCREMENT,
			`payment_id` VARCHAR(128) DEFAULT NULL,
			`seller_id` int(11) NOT NULL,
			`product_id` int(11) DEFAULT NULL,
			`order_id` int(11) DEFAULT NULL,
			`request_type` int(11) NOT NULL,
			`request_status` int(11) NOT NULL,
			`description` TEXT NOT NULL,
			`amount` DECIMAL(15,4) NOT NULL,
			`currency_id` int(11) NOT NULL,
			`currency_code` VARCHAR(3) NOT NULL,
			`date_created` DATETIME NOT NULL,
			`date_modified` DATETIME DEFAULT NULL,
			PRIMARY KEY (`request_id`)) default CHARSET=utf8");

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payment-gateway');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payment-gateway');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payment-request');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payment-request');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payment');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payment');

			/**
			 * Backward compatibility for payments
			*/
			$old_payments = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_payment`");

			// Old payment types => new payment types
			$payment_types = array(
				'1' => MsPgPayment::TYPE_PAID_REQUESTS, // Signup fee => Paid requests
				'2' => MsPgPayment::TYPE_PAID_REQUESTS, // Product listing fee => Paid requests
				'3' => MsPgPayment::TYPE_PAID_REQUESTS, // Payout => Paid requests
				'4' => MsPgPayment::TYPE_PAID_REQUESTS, // Payout request => Paid requests
				'5' => MsPgPayment::TYPE_PAID_REQUESTS, // Recurring => Paid requests
				'6' => MsPgPayment::TYPE_SALE  			// Sales => Sale
			);

			// Payment method => new payment code
			$payment_codes = array(
				'1' => NULL, // Balance
				'2' => 'ms_pg_paypal', // PayPal
				'3' => 'ms_pp_adaptive' // PayPal Adaptive payments
			);

			foreach ($old_payments->rows as $old_payment_data) {
				// If record is balance related, skip it
				if(is_null($payment_codes[$old_payment_data['payment_method']])) continue;

				// If payment type is Payout, set seller_id = 0 that means payment was created by admin.
				// Else seller_id means id of a target participant of payment
				$payment_seller_id = ($old_payment_data['payment_type'] == 3 ? 0 :(int)$old_payment_data['seller_id']);

				// Payment method => description
				$receiver_data = array(
					'2' => array($old_payment_data['seller_id'] => array('pp_address' => $old_payment_data['payment_data'])),
					'3' => array('pp_address' => $old_payment_data['payment_data'])
				);

				// Create new record in oc_ms_pg_payment
				$this->db->query("
				INSERT INTO " . DB_PREFIX . "ms_pg_payment
				SET
					seller_id = '" . (int)$payment_seller_id . "',
					payment_type = " . (int)$payment_types[$old_payment_data['payment_type']] . ",
					payment_code = '" . $this->db->escape($payment_codes[$old_payment_data['payment_method']]) . "',
					payment_status = " . (int)$old_payment_data['payment_status'] . ",
					amount = " . (float)$old_payment_data['amount'] . ",
					currency_id = " . (int)$old_payment_data['currency_id'] . ",
					currency_code = '" . $this->db->escape($old_payment_data['currency_code']) . "',
					sender_data = '" . json_encode(array()) . "',
					receiver_data = '" . json_encode($receiver_data[$old_payment_data['payment_method']]) . "',
					description = '" . json_encode(array()) . "',
					date_created = '" . $this->db->escape($old_payment_data['date_created']) . "'
				");

				$payment_id = $this->db->getLastId();

				// Create new record in oc_ms_pg_request
				$this->db->query("
				INSERT INTO " . DB_PREFIX . "ms_pg_request
				SET
					payment_id = '" . (int)$payment_id . "',
					seller_id = " . (int)$old_payment_data['seller_id'] . ",
					product_id = " . (isset($old_payment_data['product_id']) ? (int)$old_payment_data['product_id'] : 'NULL') . ",
					order_id = " . (isset($old_payment_data['order_id']) ? (int)$old_payment_data['order_id'] : 'NULL') . ",
					request_type = " . (int)$old_payment_data['payment_type'] . ",
					request_status = " . (int)$old_payment_data['payment_status'] . ",
					description = '" . $this->db->escape($old_payment_data['description']) . "',
					amount = " . (float)$old_payment_data['amount'] . ",
					currency_id = " . (int)$old_payment_data['currency_id'] . ",
					currency_code = '" . $this->db->escape($old_payment_data['currency_code']) . "',
					date_created = '" . $this->db->escape($old_payment_data['date_created']) . "',
					date_modified = " . (is_null($old_payment_data['date_paid']) ? 'NULL' : ("'" . $this->db->escape($old_payment_data['date_paid']) . "'")) . "
				");

				$request_id = $this->db->getLastId();

				// Payment method => description
				$description = array(
					$request_id => $old_payment_data['description']
				);

				// Update Payment record
				$this->db->query("UPDATE " . DB_PREFIX . "ms_pg_payment SET description = '" . json_encode($description) . "'	WHERE payment_id = '" . $payment_id . "'");
			}

			// Drop oc_ms_payment
			$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "ms_payment");

			/**
			 * Update paypal information for each seller
			 */
			$sellers = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_seller`");
			foreach ($sellers as $seller_data) {
				if(isset($seller_data['paypal']) && $seller_data['paypal']) {
					$pp_setting_name = 'slr_pg_paypal_pp_address';

					$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_setting`
					SET
						seller_id = " . $seller_data['seller_id'] . ",
						seller_group_id = " . $seller_data['seller_group'] . ",
						name = '" . $pp_setting_name . "', 
						value = '" . $this->db->escape($seller_data['paypal']) . "',
						is_encoded = NULL
					ON DUPLICATE KEY UPDATE
						value = '" . $this->db->escape($seller_data['paypal']) . "'
					");
				}
			}

			// Drop column `paypal` from oc_ms_seller
			$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_seller DROP COLUMN paypal;");

			$this->_createSchemaEntry('2.0.1.0');
		}

		if(version_compare($version, '2.1.0.0') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_shipping` (
			`seller_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`from_geo_zone_id` int(11) NOT NULL,
			`processing_time` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`seller_shipping_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_shipping_location` (
			`seller_shipping_location_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_shipping_id` int(11) NOT NULL,
			`shipping_method_id` int(11) NOT NULL,
			`delivery_time_id` int(11) NOT NULL,
			`to_geo_zone_id` int(11) NOT NULL,
			`weight_from` DECIMAL(15,4) NOT NULL,
			`weight_to` DECIMAL(15,4) NOT NULL,
			`weight_class_id` int(11) NOT NULL,
			`cost` DECIMAL(15,4) NOT NULL,
			PRIMARY KEY (`seller_shipping_location_id`)) default CHARSET=utf8");

			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_order_product_shipping_data`
			CHANGE `shipping_location_id` `fixed_shipping_method_id` int(11) NOT NULL");

			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_order_product_shipping_data`
			ADD `combined_shipping_method_id` int(11) DEFAULT NULL AFTER `fixed_shipping_method_id`");

			$this->_createSchemaEntry('2.1.0.0');
		}

		if(version_compare($version, '2.1.0.1') < 0) {
			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_product_shipping`
			ADD `override` int(11) DEFAULT 0");

			$this->_createSchemaEntry('2.1.0.1');
		}

		if(version_compare($version, '2.2.0.0') < 0) {
			// Category and product based commissions
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category_commission` (
			`category_commission_id` int(11) NOT NULL AUTO_INCREMENT,
			`category_id` int(11) NOT NULL,
			`commission_id` int(11) DEFAULT NULL,
			PRIMARY KEY (`category_commission_id`)) default CHARSET=utf8");

			$this->db->query("CREATE UNIQUE INDEX cat_id ON " . DB_PREFIX ."ms_category_commission (category_id)");

			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_product`
			ADD `commission_id` int(11) DEFAULT NULL");

			// Multi-language seller description
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_description` (
			`seller_description_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`description` text DEFAULT '',
			PRIMARY KEY (`seller_description_id`)) default CHARSET=utf8");

			// Add seller descriptions from ms_seller to ms_seller_description
			$getDataQuery = "SELECT seller_id, description FROM " . DB_PREFIX . "ms_seller";
			$seller_data = $this->db->query($getDataQuery)->rows;

			$this->load->model('localisation/language');
			$languages = $this->model_localisation_language->getLanguages();

			foreach ($seller_data as $row) {
				foreach ($languages as $code => $language) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "ms_seller_description
					SET seller_id = " . (int)$row['seller_id'] . ",
						language_id = " . (int)$language['language_id'] . ",
						description = '" . $this->db->escape($row['description']) . "'");
				}
			}

			// Refactor seller's and seller's group settings structure
			$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_setting DROP COLUMN seller_group_id;");
			$this->db->query("RENAME TABLE `" . DB_PREFIX . "ms_setting` TO `" . DB_PREFIX . "ms_seller_setting`");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_group_setting` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_group_id` int(11) unsigned DEFAULT NULL,
			`name` varchar(50) DEFAULT NULL,
			`value` varchar(250) DEFAULT NULL,
			`is_encoded` smallint(1) unsigned DEFAULT NULL,
			PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8;");

			$this->db->query("CREATE UNIQUE INDEX slr_gr_id_name ON " . DB_PREFIX ."ms_seller_group_setting (seller_group_id, name)");

			// Set default fee priority
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_fee_priority',
				'value' => 2
			));

			$this->_createSchemaEntry('2.2.0.0');
		}

		if(version_compare($version, '2.2.0.1') < 0) {
			// cleanup duplicate descriptions
			$getDataQuery = "SELECT * FROM " . DB_PREFIX . "ms_seller_description ORDER BY seller_description_id DESC";
			$description_data = $this->db->query($getDataQuery)->rows;

			$seller_descriptions = array();
			$removeids = array();

			foreach ($description_data as $d) {
				$seller_id = $d['seller_id'];
				$language_id = $d['language_id'];
				$description = $d['description'];
				if (!isset($seller_descriptions[$seller_id][$language_id])) {
					$seller_descriptions[$seller_id][$language_id] = $description;
				} else {
					$removeids[] = $d['seller_description_id'];
				}
			}

			foreach ($removeids as $id) {
				$sql = "DELETE FROM " . DB_PREFIX . "ms_seller_description WHERE seller_description_id = ".(int)$id;
				$this->db->query($sql);
			}

			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_seller_description` ADD UNIQUE `seller_language_id`(`seller_id`, `language_id`)");
			$this->_createSchemaEntry('2.2.0.1');
		}

		if(version_compare($version, '2.2.0.2') < 0) {
			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_seller_shipping`
			CHANGE `from_geo_zone_id` `from_country_id` int(11) NOT NULL;");

			$this->_createSchemaEntry('2.2.0.2');
		}

		if(version_compare($version, '2.2.0.3') < 0) {
			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_seller_shipping_location`
			CHANGE `cost` `cost_fixed` DECIMAL(15,4) NOT NULL;");

			$this->db->query("
			ALTER TABLE `" . DB_PREFIX . "ms_seller_shipping_location`
			ADD (`cost_pwu` DECIMAL(15,4) NOT NULL DEFAULT 0);");

			$this->_createSchemaEntry('2.2.0.3');
		}

		if(version_compare($version, '2.2.0.4') < 0) {
			if($this->config->get('config_weight_class_id')) {
				$weights_to_convert = $this->db->query("SELECT seller_shipping_location_id, weight_from, weight_to, weight_class_id FROM `" . DB_PREFIX . "ms_seller_shipping_location`;");

				if($weights_to_convert->num_rows) {
					foreach ($weights_to_convert->rows as $row) {
						$weight_from_converted = $this->weight->convert($row['weight_from'], $row['weight_class_id'], $this->config->get('config_weight_class_id'));
						$weight_to_converted = $this->weight->convert($row['weight_to'], $row['weight_class_id'], $this->config->get('config_weight_class_id'));

						$this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_shipping_location`
							SET weight_from = " . (float)$weight_from_converted . ",
								weight_to = " . (float)$weight_to_converted . ",
								weight_class_id = " . (int)$this->config->get('config_weight_class_id') ."
							WHERE seller_shipping_location_id = " . (int)$row['seller_shipping_location_id']);
					}
				}
			}

			$this->_createSchemaEntry('2.2.0.4');
		}

		if(version_compare($version, '2.3.0.0') < 0) {
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_attribute`");
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_attribute_description`");
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_attribute_value`");
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_attribute_value_description`");
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_attribute_attribute`");
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_product_attribute`");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_attribute` (
			`attribute_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT 0,
			`attribute_status` int(11) NOT NULL,
			PRIMARY KEY (`attribute_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_attribute_group` (
			`attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT 0,
			`attribute_group_status` int(11) NOT NULL,
			PRIMARY KEY (`attribute_group_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.3.0.0');
		}

		if(version_compare($version, '2.3.0.1') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_option` (
			`option_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT 0,
			`option_status` int(11) NOT NULL,
			PRIMARY KEY (`option_id`)) default CHARSET=utf8");

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/option');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/option');

			$this->_createSchemaEntry('2.3.0.1');
		}

		if(version_compare($version, '2.3.0.2') < 0) {
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allow_seller_attributes',
				'value' => 0
			));

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allow_seller_options',
				'value' => 0
			));

			$this->_createSchemaEntry('2.3.0.2');
		}

		if(version_compare($version, '2.3.0.3') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_customer_ppakey` (
			`customer_id` int(11) NOT NULL,
            `preapprovalkey` varchar(255) NOT NULL,
            `active` smallint(1) NOT NULL DEFAULT '0',
             PRIMARY KEY (`customer_id`)
			) DEFAULT CHARSET=utf8;");

			$this->_createSchemaEntry('2.3.0.3');
		}

		if(version_compare($version, '2.3.0.4') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ms_conversation_to_order (
  			`order_id` int(11) NOT NULL,
  			`suborder_id` int(11) NOT NULL,
  			`conversation_id` int(11) NOT NULL,
 			PRIMARY KEY (`order_id`,`suborder_id`,`conversation_id`))
			default CHARSET=utf8");

			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_conversation DROP COLUMN order_id;");

			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_conversation
			ADD conversation_from INT(11) DEFAULT NULL");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ms_conversation_participants (
  			`conversation_id` int(11) NOT NULL,
  			`customer_id` int(11) NOT NULL DEFAULT '0',
  			`user_id` int(11) NOT NULL DEFAULT '0',
 			PRIMARY KEY (`conversation_id`,`customer_id`,`user_id`))
			default CHARSET=utf8");

			$conversation_data = $this->db->query("
			SELECT conv.*,
			(SELECT msm.from  FROM `" . DB_PREFIX . "ms_message` as msm WHERE msm.conversation_id = conv.conversation_id ORDER BY message_id ASC LIMIT 1) as data_conversation_from
			FROM " . DB_PREFIX . "ms_conversation conv
			");

			if ($conversation_data->num_rows){
				foreach ($conversation_data->rows as $data){
					$this->db->query("UPDATE " . DB_PREFIX . "ms_conversation SET
					conversation_from = '" . (int)$data["data_conversation_from"] . "'
					WHERE conversation_id = '" .  (int)$data["conversation_id"] . "'");
				}
			}

			$this->_createSchemaEntry('2.3.0.4');
		}

		if(version_compare($version, '2.3.0.5') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/conversation');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/conversation');

			$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ms_conversation_to_product (
  			`product_id` int(11) NOT NULL,
  			`conversation_id` int(11) NOT NULL,
 			PRIMARY KEY (`product_id`,`conversation_id`))
			default CHARSET=utf8");

			$conversation_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_conversation conv WHERE product_id IS NOT NULL");
			if ($conversation_data->num_rows){
				foreach ($conversation_data->rows as $data){
					$this->db->query("INSERT INTO " . DB_PREFIX . "ms_conversation_to_product SET
					product_id = '" . (int)$data["product_id"] . "',
					conversation_id = '" . (int)$data["conversation_id"] . "'
					");
				}
			}

			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_conversation DROP COLUMN product_id;");

			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_message DROP COLUMN `to`;");

			$this->db->query("
			ALTER TABLE " . DB_PREFIX . "ms_message
			ADD from_admin INT(1) DEFAULT 0");

			$this->_createSchemaEntry('2.3.0.5');
		}

		if(version_compare($version, '2.3.0.6') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ms_message_upload (
  			`message_id` int(11) NOT NULL,
  			`upload_id` int(11) NOT NULL,
			PRIMARY KEY (`message_id`, `upload_id`))
			default CHARSET=utf8");

			$this->_createSchemaEntry('2.3.0.6');
		}

		if(version_compare($version, '2.4.0.0') < 0) {
			/* Seller categories */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category` (
			`category_id` int(11) NOT NULL AUTO_INCREMENT,
			`parent_id` int(11) NOT NULL DEFAULT 0,
			`seller_id` int(11) NOT NULL DEFAULT 0,
			`image` VARCHAR(255) DEFAULT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			`category_status` int(11) NOT NULL,
			PRIMARY KEY (`category_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category_description` (
			`category_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` VARCHAR(255) NOT NULL DEFAULT '',
			`description` TEXT NOT NULL DEFAULT '',
			`meta_title` VARCHAR(255) NOT NULL DEFAULT '',
			`meta_description` VARCHAR(255) NOT NULL DEFAULT '',
			`meta_keyword` VARCHAR(255) NOT NULL DEFAULT '',
			PRIMARY KEY (`category_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category_filter` (
			`category_id` int(11) NOT NULL,
			`oc_filter_id` int(11) NOT NULL,
			PRIMARY KEY (`category_id`, `oc_filter_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category_to_store` (
			`category_id` int(11) NOT NULL,
			`store_id` int(11) NOT NULL,
			PRIMARY KEY (`category_id`, `store_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_category_path` (
			`category_id` int(11) NOT NULL,
			`path_id` int(11) NOT NULL,
			`level` int(11) NOT NULL,
			PRIMARY KEY (`category_id`, `path_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_product_to_category` (
			`product_id` int(11) NOT NULL,
			`ms_category_id` int(11) NOT NULL,
			PRIMARY KEY (`product_id`, `ms_category_id`)) default CHARSET=utf8");

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/category');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/category');

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allow_seller_categories',
				'value' => 0
			));

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_product_category_type',
				'value' => 1
			));

			$this->_createSchemaEntry('2.4.0.0');
		}

		if(version_compare($version, '2.4.0.1') < 0) {
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_msg_allowed_file_types',
				'value' => 'png,jpg,jpeg,zip,rar,pdf'
			));

			$this->_createSchemaEntry('2.4.0.1');
		}

		if(version_compare($version, '2.4.0.2') < 0) {
			// Create additional tables for reviews
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_review_attachment` (
			`review_attachment_id` int(11) NOT NULL AUTO_INCREMENT,
			`review_id` int(11) NOT NULL,
			`attachment` text NOT NULL,
			PRIMARY KEY (`review_attachment_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_review_comment` (
			`comment_id` int(11) NOT NULL AUTO_INCREMENT,
			`review_id` int(11) NOT NULL,
			`author_id` int(11) NOT NULL,
			`text` text NOT NULL,
			`rating` int(11) NOT NULL,
			`date_created` DATETIME NOT NULL,
			`date_updated` DATETIME DEFAULT NULL,
			PRIMARY KEY (`comment_id`)) default CHARSET=utf8");

			$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_review DROP COLUMN `description_accurate`;");

			$this->_createSchemaEntry('2.4.0.2');
		}

		if(version_compare($version, '2.4.0.3') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/review');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/review');

			$this->_createSchemaEntry('2.4.0.3');
		}

		if(version_compare($version, '2.4.0.4') < 0) {
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_logging_level',
				'value' => \MultiMerch\Logger\Logger::LEVEL_ERROR
			));

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_logging_filename',
				'value' => 'ms_logging.log'
			));

			$this->_createSchemaEntry('2.4.0.4');
		}

		if(version_compare($version, '2.4.0.5') < 0) {
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_logging_filename',
				'value' => 'ms_logging.' . uniqid() . '.log'
			));

			$this->_createSchemaEntry('2.4.0.5');
		}

		if(version_compare($version, '2.4.0.6') < 0) {
			$allowed_option_types = array(
				'choose' => array('select', 'radio', 'checkbox'),
				'input' => array('text', 'textarea'),
				'file' => array('file'),
				'date' => array('date', 'time', 'datetime')
			);

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allowed_seller_option_types',
				'value' => $allowed_option_types
			));

			$this->_createSchemaEntry('2.4.0.6');
		}

		if(version_compare($version, '2.4.0.7') < 0) {
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allow_questions',
				'value' => 0
			));

			$this->db->query("CREATE UNIQUE INDEX idx_shipping_delivery_time_description_to_language ON `" . DB_PREFIX ."ms_shipping_delivery_time_description` (delivery_time_desc_id, language_id)");

			$this->_createSchemaEntry('2.4.0.7');

		}

		if(version_compare($version, '2.4.1.0') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/event');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/event');

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_event` (
			`event_id` int(11) NOT NULL AUTO_INCREMENT,
			`admin_id` int(11) unsigned DEFAULT NULL,
			`customer_id` int(11) unsigned DEFAULT NULL,
			`seller_id` int(11) unsigned DEFAULT NULL,
			`event_type` tinyint NOT NULL,
			`data` text NOT NULL,
			`body` text DEFAULT NULL,
			`date_created` DATETIME NOT NULL,
			PRIMARY KEY (`event_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.4.1.0');
		}

		if(version_compare($version, '2.5.0.0') < 0) {
            $this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_import_config` (
              `config_id` int(11) NOT NULL AUTO_INCREMENT,
              `customer_id` int(11) NOT NULL,
              `config_name` varchar(255) DEFAULT NULL,
              `import_type` varchar(100) DEFAULT NULL,
              `attachment_code` varchar(255) DEFAULT NULL,
              `mapping` text,
              `start_row` int(11) DEFAULT NULL,
              `finish_row` int(11) DEFAULT NULL,
              `update_key_id` int(11) DEFAULT NULL,
              `file_encoding` int(11) DEFAULT NULL,
              `date_added` datetime NOT NULL,
              `date_modified` datetime NOT NULL,
              PRIMARY KEY (`config_id`)
			) default CHARSET=utf8");

            //test config for import
            //$this->db->query("INSERT IGNORE INTO `" . DB_PREFIX . "ms_import_config` VALUES (27, 0, 'TestImport1', 'product', '1a203d50ff8790d7254e854259b64046a834290f', 'a:13:{i:0;s:1:\"2\";i:1;s:2:\"15\";i:2;s:1:\"3\";i:4;s:3:\"101\";i:5;s:3:\"102\";i:6;s:1:\"6\";i:8;s:3:\"103\";i:9;s:3:\"104\";i:12;s:3:\"105\";i:17;s:2:\"11\";i:18;s:2:\"12\";i:19;s:1:\"4\";i:22;s:2:\"14\";}', 2, 2, 2, 1, '2017-04-24 12:46:53', '2017-04-25 17:37:45')");
            //$this->db->query("INSERT IGNORE INTO `" . DB_PREFIX . "upload` VALUES (130, 'BookAndAuthor21.04.2017.csv', 'BookAndAuthor21.04.2017.csv.tQgtdNnwZAP9E2JTVot3gIREmxvBCHgc', '1a203d50ff8790d7254e854259b64046a834290f', '2017-04-21 16:02:25')");

            $this->_createSchemaEntry('2.5.0.0');
        }

		if(version_compare($version, '2.5.0.1') < 0) {

			if (!$this->columnExists(DB_PREFIX .'ms_import_config', 'cell_container')) {
				$this->db->query("
				ALTER TABLE " . DB_PREFIX . "ms_import_config
				ADD cell_container varchar(10) DEFAULT NULL");
			}

			if (!$this->columnExists(DB_PREFIX .'ms_import_config', 'cell_separator')) {
				$this->db->query("
				ALTER TABLE " . DB_PREFIX . "ms_import_config
				ADD cell_separator varchar(10) DEFAULT NULL");
			}

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_msg_allowed_file_types',
				'value' => 'png,jpg,jpeg,zip,rar,pdf,csv'
			));
			$this->_createSchemaEntry('2.5.0.1');
		}

		if(version_compare($version, '2.5.0.2') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/import');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/import');

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_import_history` (
              `import_id` int(11) NOT NULL AUTO_INCREMENT,
			  `seller_id` int(11) NOT NULL,
			  `name` varchar(255) NOT NULL,
			  `filename` varchar(255) NOT NULL,
			  `date_added` datetime NOT NULL,
			  `type` varchar(100) NOT NULL,
			  `processed` int(11) NOT NULL,
			  `added` int(11) NOT NULL,
			  `updated` int(11) NOT NULL,
			  `errors` int(11) NOT NULL,
			  `product_ids` text DEFAULT NULL,
			  PRIMARY KEY (`import_id`)
			) default CHARSET=utf8");
			$this->_createSchemaEntry('2.5.0.2');
		}

		if(version_compare($version, '2.5.0.3') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/question');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/question');

			$this->_createSchemaEntry('2.5.0.3');
		}

		if(version_compare($version, '2.6.0.0') < 0) {
			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/report');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/report');

			$reports = glob(DIR_APPLICATION . 'controller/multimerch/report/*.php');
			foreach ($reports as $report) {
				$route = basename($report, '.php');
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/report/' . $route);
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/report/' . $route);
			}

			// Seller layouts
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout SET name = '[MultiMerch] Sellers Reports'");
			$layout_id = $this->db->getLastId();
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module (`layout_id`, `code`, `position`, `sort_order`) VALUES($layout_id, 'account', 'column_right', 1);");
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET layout_id = '" . (int)$layout_id . "', route = 'seller/report/%'");

			$this->_createSchemaEntry('2.6.0.0');
		}

		if(version_compare($version, '2.6.0.1') < 0) {
			// Delete all empty entries from `ms_seller_channel`
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_channel` WHERE `channel_value` IS NULL OR `channel_value` = ''");

			$this->_createSchemaEntry('2.6.0.1');
		}

		if(version_compare($version, '2.7.0.0') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_group` (
			`custom_field_group_id` int(11) NOT NULL AUTO_INCREMENT,
			`admin_id` int(11) NOT NULL DEFAULT 0,
			`status` tinyint NOT NULL DEFAULT 0,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`custom_field_group_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_group_description` (
			`custom_field_group_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` text NOT NULL,
			PRIMARY KEY (`custom_field_group_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_group_to_location` (
			`custom_field_group_id` int(11) NOT NULL,
			`location_id` int(11) NOT NULL DEFAULT 1,
			PRIMARY KEY (`custom_field_group_id`, `location_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field` (
			`custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
			`custom_field_group_id` int(11) NOT NULL,
			`admin_id` int(11) NOT NULL DEFAULT 0,
			`type` varchar(100) NOT NULL DEFAULT '',
			`required` tinyint NOT NULL DEFAULT 0,
			`status` tinyint NOT NULL DEFAULT 0,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`custom_field_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_description` (
			`custom_field_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` text NOT NULL,
			PRIMARY KEY (`custom_field_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_value` (
			`custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT,
			`custom_field_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`custom_field_value_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_value_description` (
			`custom_field_value_id` int(11) NOT NULL,
			`custom_field_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` text NOT NULL,
			PRIMARY KEY (`custom_field_value_id`, `language_id`)) default CHARSET=utf8");

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/custom-field');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/custom-field');

			$this->_createSchemaEntry('2.7.0.0');
		}

		if(version_compare($version, '2.7.0.1') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_custom_field` (
			`product_id` int(11) NOT NULL,
			`custom_field_id` int(11) NOT NULL,
			`value` text NOT NULL DEFAULT '',
			PRIMARY KEY (`product_id`, `custom_field_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.7.0.1');
		}

		if(version_compare($version, '2.7.0.2') < 0) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_custom_field ADD `validation` text NOT NULL DEFAULT '' AFTER `required`");

			$this->_createSchemaEntry('2.7.0.2');
		}

		if(version_compare($version, '2.7.0.3') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_group_note` (
			`custom_field_group_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`note` text NOT NULL DEFAULT '',
			PRIMARY KEY (`custom_field_group_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_custom_field_note` (
			`custom_field_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`note` text NOT NULL DEFAULT '',
			PRIMARY KEY (`custom_field_id`, `language_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.7.0.3');
		}

		/****************************************** RELEASE 8.11 ******************************************************/

		if(version_compare($version, '2.8.0.0') < 0) {
			$this->load->model('user/user_group');

			/**********************************************************************************************************/
			/*                               		  	   Licensing											  	  */
			/**********************************************************************************************************/

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_license_key',
				'value' => ''
			));

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_license_activated',
				'value' => 0
			));


			/**********************************************************************************************************/
			/*                               		Order system improvements									 	  */
			/**********************************************************************************************************/

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/order');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/order');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/suborder-status');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/suborder-status');

			/**
			 * Create new `ms_suborder_status` table.
			 */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_suborder_status` (
			`ms_suborder_status_id` int(11) NOT NULL AUTO_INCREMENT,
			PRIMARY KEY (`ms_suborder_status_id`)) default CHARSET=utf8");

			/**
			 * Create new `ms_suborder_status_description` table.
			 */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_suborder_status_description` (
			`ms_suborder_status_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`name` varchar(256) NOT NULL DEFAULT '',
			PRIMARY KEY (`ms_suborder_status_id`, `language_id`)) default CHARSET=utf8");

			/**
			 * Create suborder statuses.
			 *
			 * In case of update, we only copy OpenCart order statuses.
			 */
			$oc_order_statuses = $this->db->query("SELECT order_status_id, language_id, `name` FROM `" . DB_PREFIX . "order_status`");

			foreach ($oc_order_statuses->rows as $row) {
				$this->db->query("INSERT IGNORE INTO `" . DB_PREFIX . "ms_suborder_status` SET ms_suborder_status_id = " . (int)$row['order_status_id']);

				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_suborder_status_description`
					SET ms_suborder_status_id = " . (int)$row['order_status_id'] . ",
						language_id = " . (int)$row['language_id'] . ",
						`name` = '" . $this->db->escape($row['name']) . "'
				");
			}

			/**
			 * Create new default suborder status setting `msconf_suborder_default_status`.
			 *
			 * Default status is taken from OpenCart's setting `config_order_status_id`. It can be changed by admin in
			 * Multimerch > System > Settings > Orders tab.
			 */
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_suborder_default_status',
				'value' => $this->config->get('config_order_status_id')
			));

			/**
			 * Update old default suborder statuses in `ms_suborder` and `ms_suborder_history` tables.
			 *
			 * Old default suborder status = 0, we now update it to the value of OpenCart's setting `config_order_status_id`.
			 */
			$this->db->query("
				UPDATE `" . DB_PREFIX . "ms_suborder`
				SET `order_status_id` = '" . $this->config->get('config_order_status_id') . "'
				WHERE `order_status_id` = 0
			");

			$this->db->query("
				UPDATE `" . DB_PREFIX . "ms_suborder_history`
				SET `order_status_id` = '" . $this->config->get('config_order_status_id') . "'
				WHERE `order_status_id` = 0
			");

			/**
			 * Create order/suborder statuses to states relation.
			 *
			 * In case of update, order and suborder states and their statuses are the same.
			 * Firstly, we check OpenCart order_status_ids are not duplicated in different states.
			 */
			$state_pending = (array)$this->config->get('config_order_status_id');
			$state_completed = array_diff($this->config->get('config_complete_status') ?: array(), $state_pending);
			$state_processing = array_diff($this->config->get('config_processing_status') ?: array(), array_merge($state_pending, $state_completed));

			/**
			 * Create `msconf_order_state` setting that represents OpenCart order_states - order_status_ids linkings.
			 *
			 * Its structure:
			 * 		array(
			 * 			'order_state_id' => array('order_status_id', ...),
			 * 			...
			 * 		)
			 */
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_order_state',
				'value' => array(
					MsOrderData::STATE_PENDING => (array)$state_pending,
					MsSuborder::STATE_PROCESSING => (array)$state_processing,
					MsSuborder::STATE_COMPLETED => (array)$state_completed,
				)
			));

			/**
			 * Create `msconf_suborder_state` setting that represents MultiMerch suborder_states - suborder_status_ids linkings.
			 *
			 * Its structure:
			 * 		array(
			 * 			'suborder_state_id' => array('suborder_status_id', ...),
			 * 			...
			 * 		)
			 */
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_suborder_state',
				'value' => array(
					MsSuborder::STATE_PENDING => (array)$state_pending,
					MsSuborder::STATE_PROCESSING => (array)$state_processing,
					MsSuborder::STATE_COMPLETED => (array)$state_completed
				)
			));

			/**
			 * Re-create settings `msconf_credit_order_statuses` and `msconf_debit_order_statuses` responsible for
			 * credit-debit suborder statuses.
			 *
			 * Settings' structure:
			 * 		array(
			 * 			'oc' => array('oc_status_id', ...),
			 * 			'ms' => array('ms_suborder_status_id', ...)
			 * 		)
			 */
			$old_credit_order_statuses = (array)$this->config->get('msconf_credit_order_statuses');
			$old_debit_order_statuses = (array)$this->config->get('msconf_debit_order_statuses');

			// Re-create msconf_credit_order_statuses to include ms suborder statuses
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_credit_order_statuses',
				'value' => array(
					'oc' => $old_credit_order_statuses,
					'ms' => array()
				)
			));

			// Re-create msconf_debit_order_statuses to include ms suborder statuses
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_debit_order_statuses',
				'value' => array(
					'oc' => $old_debit_order_statuses,
					'ms' => array()
				)
			));


			/**********************************************************************************************************/
			/*                               	 		Payout system										  		  */
			/**********************************************************************************************************/

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payout');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payout');

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_payout` (
			`payout_id` int(11) NOT NULL AUTO_INCREMENT,
			`name` text NOT NULL DEFAULT '',
			`date_created` datetime NOT NULL,
			`date_payout_period` datetime NOT NULL,
			PRIMARY KEY (`payout_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_payout_to_invoice` (
			`payout_id` int(11) NOT NULL,
			`invoice_id` int(11) NOT NULL,
			PRIMARY KEY (`payout_id`, `invoice_id`)) default CHARSET=utf8");

			/**
			 * Get all old invoices and assign them to one Payout instance.
			 */
			$old_invoices_data = $this->db->query("
				SELECT
					request_id,
					date_created
				FROM `" . DB_PREFIX . "ms_pg_request`
				WHERE request_type = 3
				ORDER BY date_created DESC
			");

			if($old_invoices_data->num_rows) {
				$last_payout_date = isset($old_invoices_data->rows[0]['date_created']) ? $old_invoices_data->rows[0]['date_created'] : FALSE;

				if($last_payout_date) {
					/**
					 * Create Payout instance.
					 */
					$payout_id = 1;
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_payout`
						SET `payout_id` = '" . (int)$payout_id . "',
							`name` = '" . $this->language->get('ms_payout_payout') . " " . $this->language->get('ms_id') . "1 (" . $this->db->escape(date($this->language->get('date_format_short'), strtotime($last_payout_date))) . ")',
							`date_created` = '" . $this->db->escape($last_payout_date) . "',
							`date_payout_period` = '" . $this->db->escape($last_payout_date) . "'
					");

					foreach ($old_invoices_data->rows as $row) {
						$this->db->query("
							INSERT INTO `" . DB_PREFIX . "ms_payout_to_invoice`
							SET `payout_id` = '" . (int)$payout_id . "',
								`invoice_id` = '" . (int)$row['request_id'] . "'
						");
					}
				}
			}


			/**********************************************************************************************************/
			/*                               	Items deletion improvements									  		  */
			/**********************************************************************************************************/

			/**
			 * Recreate default seller group if it has been deleted.
			 * This group has special meaning and thus can't be deleted anymore.
			 */
			$this->db->query("
				INSERT INTO `" . DB_PREFIX . "ms_seller_group`
				SET seller_group_id = '1',
					commission_id = '1',
					product_period = '0',
					product_quantity = '0'
				ON DUPLICATE KEY UPDATE seller_group_id = seller_group_id
			");

			$languages = $this->model_localisation_language->getLanguages();

			foreach ($languages as $code => $language) {
				$exists = $this->db->query("SELECT 1 FROM `" . DB_PREFIX . "ms_seller_group_description` WHERE seller_group_id = '1' AND language_id = '" . (int)$language['language_id'] . "'");

				if (!$exists->num_rows) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_seller_group_description`
						SET seller_group_id = '1',
							name = 'Default',
							description = 'Default seller group',
							language_id = '" . (int)$language['language_id'] . "'
						ON DUPLICATE KEY UPDATE seller_group_id = seller_group_id
					");
				}
			}


			/**********************************************************************************************************/
			/*                             	Additional category levels for import							  		  */
			/**********************************************************************************************************/

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_import_category_type',
				'value' => 0
			));


			/**********************************************************************************************************/
			/*                             		   Reviews system refactoring							  		      */
			/**********************************************************************************************************/

			if (!$this->columnExists(DB_PREFIX . 'ms_review', 'seller_id')) {
				$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_review ADD seller_id int(11) DEFAULT NULL");
			}

			$this->db->query("
				UPDATE `" . DB_PREFIX . "ms_review` msr
				SET seller_id = (SELECT seller_id FROM " . DB_PREFIX . "ms_product WHERE product_status = 1 AND product_approved = 1 AND product_id = msr.product_id)
			");


			/**********************************************************************************************************/
			/*                             		  What's new in MultiMerch 8.11						  		      	  */
			/**********************************************************************************************************/

			$upgrade_info['8.11'][] = "You can now activate your license key and receive information about MultiMerch updates in <a target='_blank' href='" . $this->url->link('module/multimerch', 'token=' . $this->session->data['token'] . '#tab-updates') . "'>MultiMerch > System > Settings > Updates</a>.";
			$upgrade_info['8.11'][] = "You can now specify a different set of order statuses for sellers in <a target='_blank' href='" . $this->url->link('multimerch/suborder-status', 'token=' . $this->session->data['token']) . "'>MultiMerch > Marketplace > Seller order statuses</a> and keep them separate from OpenCart order statuses. For backward compatibility with existing orders, we've copied your existing OpenCart statuses to new seller statuses.";
			$upgrade_info['8.11'][] = "You can now assign different statuses to different logical order states in <a target='_blank' href='" . $this->url->link('module/multimerch', 'token=' . $this->session->data['token'] . '#tab-order') . "'>MultiMerch > System > Settings > Orders</a>. MultiMerch will start using these settings in one of the upcoming updates.";
			$upgrade_info['8.11'][] = "You can now configure MultiMerch to generate seller balance transactions based not only on OpenCart (customer) order statuses, but also on seller order statuses in <a target='_blank' href='" . $this->url->link('module/multimerch', 'token=' . $this->session->data['token'] . '#tab-order') . "'>MultiMerch > System > Settings > Orders</a>.";
			$upgrade_info['8.11'][] = "MultiMerch will now group seller payouts into a new \"payout\" entity for each payout you initiate. For backward compatiblity with past payouts, we've automatically placed all past payout invoices into a default Payout #1 group.";

			$this->_createSchemaEntry('2.8.0.0');
		}

		/****************************************** RELEASE 8.12 ******************************************************/

		if(version_compare($version, '2.9.0.0') < 0) {
			$this->load->model('user/user_group');

			/**********************************************************************************************************/
			/*                             		  	   Discount coupons											  	  */
			/**********************************************************************************************************/

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/coupon');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/coupon');

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon` (
			`coupon_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT NULL,
			`name` varchar(255) NOT NULL DEFAULT '',
			`description` text NOT NULL DEFAULT '',
			`code` varchar(255) NOT NULL DEFAULT '',
			`type` tinyint NOT NULL DEFAULT 1,
			`value` decimal(15,4) NOT NULL DEFAULT 0,
			`date_start` datetime DEFAULT NULL,
			`date_end` datetime DEFAULT NULL,
			`max_uses` int(11) DEFAULT NULL,
			`max_uses_customer` int(11) DEFAULT NULL,
			`total_uses` int(11) NOT NULL DEFAULT 0,
			`min_order_total` decimal(15,4) NOT NULL DEFAULT 0,
			`login_required` tinyint NOT NULL DEFAULT 1,
			`status` tinyint NOT NULL DEFAULT 1,
			`date_created` datetime NOT NULL,
			PRIMARY KEY (`coupon_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon_history` (
			`coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
			`coupon_id` int(11) NOT NULL,
			`order_id` int(11) NOT NULL,
			`suborder_id` int(11) NOT NULL,
			`customer_id` int(11) NOT NULL,
			`amount` decimal(15,4) NOT NULL,
			`date_created` datetime NOT NULL,
			PRIMARY KEY (`coupon_history_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon_customer` (
			`coupon_customer_id` int(11) NOT NULL AUTO_INCREMENT,
			`coupon_id` int(11) NOT NULL,
			`customer_id` int(11) NOT NULL,
			`exclude` tinyint DEFAULT 0,
			PRIMARY KEY (`coupon_customer_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon_product` (
			`coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
			`coupon_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`exclude` tinyint DEFAULT 0,
			PRIMARY KEY (`coupon_product_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon_oc_category` (
			`coupon_oc_category_id` int(11) NOT NULL AUTO_INCREMENT,
			`coupon_id` int(11) NOT NULL,
			`oc_category_id` int(11) NOT NULL,
			`exclude` tinyint DEFAULT 0,
			PRIMARY KEY (`coupon_oc_category_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_coupon_ms_category` (
			`coupon_ms_category_id` int(11) NOT NULL AUTO_INCREMENT,
			`coupon_id` int(11) NOT NULL,
			`ms_category_id` int(11) NOT NULL,
			`exclude` tinyint DEFAULT 0,
			PRIMARY KEY (`coupon_ms_category_id`)) default CHARSET=utf8");

			/**
			 * Create MultiMerch's coupon code in default OpenCart's totals system
			 */
			$this->db->query("INSERT INTO `" . DB_PREFIX . "extension` SET `type` = 'total', `code` = 'ms_coupon'");

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_allow_seller_coupons',
				'value' => 1
			));

			/**
			 * Convert MultiMerch's approved categories status into active
			 */
			$this->db->query("UPDATE `" . DB_PREFIX . "ms_category` SET category_status = 1 WHERE category_status = 3");

			$upgrade_info['8.12'][] = "Sellers can now create and manage their own discount campaigns through MultiMerch Coupons";

			$this->_createSchemaEntry('2.9.0.0');
		}

		if(version_compare($version, '2.9.0.1') < 0)  {
			$this->load->model('setting/setting');
			$this->load->model('user/user_group');

			$this->model_setting_setting->editSetting('ms_coupon', array(
				'ms_coupon_status' => 1,
				'ms_coupon_sort_order' => 4
			));

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'total/ms_coupon');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'total/ms_coupon');

			$this->_createSchemaEntry('2.9.0.1');
		}

		/****************************************** RELEASE 8.13 ******************************************************/

		if(version_compare($version, '2.10.0.0') < 0) {
			$sellers = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_seller`");
			foreach ($sellers->rows as $seller){
				if (isset($seller['product_validation'])){
					$this->db->query("
						INSERT IGNORE INTO `" . DB_PREFIX . "ms_seller_setting`
						SET `seller_id` = '" . (int)$seller['seller_id'] . "',
							`name` = 'slr_product_validation',
							`value` = '" . (int)$seller['product_validation'] . "'
					");
				}
			}

			if ($this->columnExists(DB_PREFIX . 'ms_seller', 'product_validation')) {
				$this->db->query("ALTER TABLE " . DB_PREFIX . "ms_seller DROP COLUMN product_validation;");
			}

			$this->MsLoader->MsHelper->deleteOCSetting('msconf', 'msconf_product_category_type');

			$upgrade_msg = "Starting with version 8.13, MultiMerch does not require the following files or folders anymore – please remove them from your system manually:";
			$upgrade_msg .= "<ul>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/module/autogenerated/</li>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/module/cli/</li>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/module/extended/</li>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/module/config/autoload_classmap.php</li>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/module/init_multimerch.php</li>";
			$upgrade_msg .= "<li>catalog/controller/seller/account-return.php</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/multiseller/account-return-form.tpl</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/multiseller/account-return-info.tpl</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/multiseller/account-return.tpl</li>";
			$upgrade_msg .= "</ul>";

			$upgrade_info['8.13'][] = $upgrade_msg;

			$this->_createSchemaEntry('2.10.0.0');
		}

		/****************************************** RELEASE 8.14 ******************************************************/

		if(version_compare($version, '2.11.0.0') < 0) {
			// Create Google API key setting
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_google_api_key',
				'value' => ''
			));

			$this->load->model('user/user_group');

			// Add permissions to MultiMerch reports for all admin user groups
			$user_groups = $this->model_user_user_group->getUserGroups();
			foreach ($user_groups as $user_group) {
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'access', 'multimerch/report');
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'modify', 'multimerch/report');
			}

			$this->_createSchemaEntry('2.11.0.0');
		}

		if(version_compare($version, '2.11.1.0') < 0) {
			$deleted_orders = $this->db->query("SELECT DISTINCT mss.order_id FROM `" . DB_PREFIX . "ms_suborder` mss LEFT JOIN (SELECT order_id, order_status_id FROM `" . DB_PREFIX . "order`) o ON (mss.order_id = o.order_id) WHERE o.order_status_id IS NULL");

			foreach ($deleted_orders->rows as $row) {
				$order_id = $row['order_id'];

				// Delete MultiMerch conversations and mesages related to deleted order
				$ms_order_conversations = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_conversation_to_order` WHERE order_id = '" . (int)$order_id . "'");
				foreach ($ms_order_conversations->rows as $row) {
					$this->MsLoader->MsConversation->deleteConversation($row['conversation_id']);
				}

				// Delete MultiMerch order comments
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_order_comment` WHERE order_id = '" . (int)$order_id . "'");

				// Delete MultiMerch order product data
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_order_product_data` WHERE order_id = '" . (int)$order_id . "'");

				// Delete MultiMerch order product shipping data
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_order_product_shipping_data` WHERE order_id = '" . (int)$order_id . "'");

				// Delete MultiMerch invoices for order
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_pg_request` WHERE order_id = '" . (int)$order_id . "' AND request_status = 1");

				// Delete MultiMerch suborders
				$ms_order_suborders = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_suborder` WHERE order_id = '" . (int)$order_id . "'");
				foreach ($ms_order_suborders->rows as $row) {
					$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_suborder` WHERE suborder_id = '" . (int)$row['suborder_id'] . "'");
					$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_suborder_history` WHERE suborder_id = '" . (int)$row['suborder_id'] . "'");
				}
			}

			$this->_createSchemaEntry('2.11.1.0');
		}

		if(version_compare($version, '2.11.1.1') < 0) {
			// Create account pagination limit
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_account_pagination_limit',
				'value' => 10
			));
			$this->_createSchemaEntry('2.11.1.1');
		}

		if(version_compare($version, '2.11.1.2') < 0) {
			// Create account pagination limit
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_rte_whitelist',
				'value' => "p,strong,em,u,s,a,div,span,h1,h2,h3,h4,h5,h6,ul,ol,li,img,pre,address,iframe"
			));
			$this->_createSchemaEntry('2.11.1.2');
		}

		if(version_compare($version, '2.11.2.0') < 0) {
			$this->load->model('user/user_group');

			// Add permissions to MultiMerch reports for all admin user groups
			$user_groups = $this->model_user_user_group->getUserGroups();
			foreach ($user_groups as $user_group) {
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'access', 'payment/ms_stripe_connect');
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'modify', 'payment/ms_stripe_connect');
			}

			$this->_createSchemaEntry('2.11.2.0');
		}

		/****************************************** RELEASE 8.16 ******************************************************/

		if(version_compare($version, '2.12.0.0') < 0) {
			$information_description = [];

			$languages = $this->model_localisation_language->getLanguages();

			foreach ($languages as $code => $language) {
				$information_description[$language['language_id']] = [
					'title' => "New seller landing template (don't use this on live)",
					'description' => $this->load->view('multimerch/new_seller_landing', ['server_url' => $this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG]),
					'meta_title' => 'New seller landing template',
					'meta_description' => '',
					'meta_keyword' => ''
				];
			}

			if (!empty($information_description)) {
				$this->load->model('catalog/information');
				$seller_landing_page_id_default = $this->model_catalog_information->addInformation([
					'information_description' => $information_description,
					'information_store' => [$this->config->get('config_store_id')],
					'sort_order' => 1,
					'status' => 0
				]);

				$this->MsLoader->MsHelper->createOCSetting(array(
					'code' => 'msconf',
					'key' => 'msconf_seller_landing_page_id_default',
					'value' => $seller_landing_page_id_default
				));
			}

			$this->_createSchemaEntry('2.12.0.0');
		}

		if(version_compare($version, '2.12.0.1') < 0) {
			$information_description = [];

			$languages = $this->model_localisation_language->getLanguages();

			foreach ($languages as $code => $language) {
				$information_description[$language['language_id']] = [
					'title' => "New seller landing template (don't use this on live)",
					'description' => $this->load->view('multimerch/new_seller_landing', ['server_url' => $this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG]),
					'meta_title' => 'New seller landing template',
					'meta_description' => '',
					'meta_keyword' => ''
				];
			}

			if (!empty($information_description)) {
				$this->load->model('catalog/information');
				$this->model_catalog_information->editInformation($this->config->get('msconf_seller_landing_page_id_default'), [
					'information_description' => $information_description,
					'information_store' => [$this->config->get('config_store_id')],
					'sort_order' => 1,
					'status' => 0
				]);
			}

			$this->_createSchemaEntry('2.12.0.1');
		}

		if(version_compare($version, '2.12.1.0') < 0) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_coupon` MODIFY `login_required` tinyint NOT NULL DEFAULT 0;");
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_coupon_history` MODIFY `customer_id` int(11) DEFAULT NULL;");

			$this->_createSchemaEntry('2.12.1.0');
		}

		/******************************************* RELEASE 8.17 ******************************************************/

		if(version_compare($version, '2.13.0.0') < 0) {
			/**********************************************************************************************************/
			/*									One page seller registration										  */
			/**********************************************************************************************************/

			/**
			 * Get default country id.
			 * If the default country for the whole store has been set, its id is used. The fallback value is 0.
			 */
			$default_country_id = $this->config->get('config_country_id') ? (int)$this->config->get('config_country_id') : 0;

			/**
			 * Create table for MultiMerch addresses book.
			 */
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_seller_address` (
			`address_id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`fullname` varchar(255) NOT NULL DEFAULT '',
			`address_1` text NOT NULL DEFAULT '',
			`address_2` text NOT NULL DEFAULT '',
			`city` varchar(255) NOT NULL DEFAULT '',
			`state` varchar(255) NOT NULL DEFAULT '',
			`zip` varchar(16) NOT NULL DEFAULT '',
			`country_id` int(11) NOT NULL DEFAULT " . (int)$default_country_id . ",
			PRIMARY KEY (`address_id`)) default CHARSET=utf8");

			/**
			 * Truncate table, in case repeated update is being performed because something went wrong during previous
			 * update attempts.
			 */
			$this->db->query("TRUNCATE `" . DB_PREFIX . "ms_seller_address`");

			/**
			 * Move seller's address settings from `ms_seller_setting` table to `ms_seller_address` settings.
			 */
			$sellers = $this->MsLoader->MsSeller->getSellers();
			foreach ($sellers as $seller) {
				$slr_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $seller['seller_id']));

				$this->db->query("
					INSERT IGNORE INTO `" . DB_PREFIX . "ms_seller_address`
					SET `seller_id` = '" . (int)$seller['seller_id'] . "',
						`fullname` = '" . (isset($slr_settings['slr_full_name']) ? $this->db->escape($slr_settings['slr_full_name']) : '') . "',
						`address_1` = '" . (isset($slr_settings['slr_address_line1']) ? $this->db->escape($slr_settings['slr_address_line1']) : '') . "',
						`address_2` = '" . (isset($slr_settings['slr_address_line2']) ? $this->db->escape($slr_settings['slr_address_line2']) : '') . "',
						`city` = '" . (isset($slr_settings['slr_city']) ? $this->db->escape($slr_settings['slr_city']) : '') . "',
						`state` = '" . (isset($slr_settings['slr_state']) ? $this->db->escape($slr_settings['slr_state']) : '') . "',
						`zip` = '" . (isset($slr_settings['slr_zip']) ? $this->db->escape($slr_settings['slr_zip']) : '') . "',
						`country_id` = '" . (isset($slr_settings['slr_country']) ? (int)$slr_settings['slr_country'] : $default_country_id) . "'
				");

				$ms_address_id = $this->db->getLastId();

				$this->MsLoader->MsSetting->createSellerSetting(array(
					'seller_id' => $seller['seller_id'],
					'settings' => array(
						'slr_ms_address' => $ms_address_id
					)
				));

				// @todo 9.0: delete old settings for `ms_seller_setting` table
				// @todo 9.0: in `ms_seller_setting` different address settings (e.g., slr_shipping_address, slr_billing_address etc.)
			}

			/**
			 * Add `slogan` column to `ms_seller_description_table`
			 */
			if (!$this->columnExists(DB_PREFIX . 'ms_seller_description', 'slogan')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_seller_description` ADD `slogan` text NOT NULL DEFAULT '' AFTER `language_id`");
			}

			/**
			 * Disable deprecated setting `msconf_change_group`
			 */
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_change_group',
				'value' => 0
			));

			/**
			 * Create admin setting for managing which fields are allowed at seller registration, profile and settings.
			 */
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_seller_included_fields',
				'value' => array(
					'slogan' => array('enabled' => 1, 'enabled_reg' => 1),
					'description' => array('enabled' => 1, 'enabled_reg' => 1),
					'website' => array('enabled' => 1, 'enabled_reg' => 1),
					'company' => array('enabled' => 1, 'enabled_reg' => 1),
					'phone' => array('enabled' => 1, 'enabled_reg' => 1),
					'logo' => array('enabled' => 1, 'enabled_reg' => 1),
					'banner' => array('enabled' => 1, 'enabled_reg' => 1),
					'fullname' => array('enabled' => 1, 'enabled_reg' => 1),
					'address_1' => array('enabled' => 1, 'enabled_reg' => 1),
					'address_2' => array('enabled' => 1, 'enabled_reg' => 1),
					'city' => array('enabled' => 1, 'enabled_reg' => 1),
					'state' => array('enabled' => 1, 'enabled_reg' => 1),
					'zip' => array('enabled' => 1, 'enabled_reg' => 1)
				)
			));

			$upgrade_info['8.17'][] = "Added a one page seller registration form.";
			$upgrade_info['8.17'][] = "Added seller coupon support for guests.";
			$upgrade_info['8.17'][] = "Improved seller account navigation.";
			$upgrade_info['8.17'][] = "Fixed various minor issues.";

			$this->_createSchemaEntry('2.13.0.0');
		}

		/******************************************* RELEASE 8.18 ******************************************************/

		if(version_compare($version, '2.14.0.0') < 0) {
			/**
			 * Refactor `ms_product_custom_field` table
			 */
			// Get old values
			$ms_product_custom_fields = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_product_custom_field`");

			$new_ms_product_custom_fields = [];

			foreach ($ms_product_custom_fields->rows as $row) {
				$values = (array)json_decode($row['value']);
				reset($values);
				$type = key($values);

				foreach ($values[$type] as $value) {
					switch ($type) {
						case 'select':
						case 'radio':
						case 'checkbox':
							$new_ms_product_custom_fields[] = [
								'product_id' => $row['product_id'],
								'custom_field_id' => $row['custom_field_id'],
								'custom_field_value_id' => $value,
								'value' => ''
							];
						break;

						default:
							$new_ms_product_custom_fields[] = [
								'product_id' => $row['product_id'],
								'custom_field_id' => $row['custom_field_id'],
								'custom_field_value_id' => '',
								'value' => $value
							];
						break;
					}
				}
			}

			$this->db->query("DROP TABLE `" . DB_PREFIX . "ms_product_custom_field`");

			$this->db->query("
				CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_custom_field` (
				`ms_product_custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
				`product_id` int(11) NOT NULL,
				`custom_field_id` int(11) NOT NULL,
				`custom_field_value_id` int(11) DEFAULT NULL,
				`value` text DEFAULT NULL,
				PRIMARY KEY (`ms_product_custom_field_id`)) default CHARSET=utf8
			");

			foreach ($new_ms_product_custom_fields as $ms_product_custom_field) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_product_custom_field`
					SET `product_id` = " . (int)$ms_product_custom_field['product_id'] . ",
						`custom_field_id` = " . (int)$ms_product_custom_field['custom_field_id']
					. ($ms_product_custom_field['custom_field_value_id'] ? ", `custom_field_value_id` = '" . (int)$ms_product_custom_field['custom_field_value_id'] . "'" : "")
					. ($ms_product_custom_field['value'] ? ", `value` = '" . (string)$ms_product_custom_field['value'] . "'" : "")
				);
			}

			$this->_createSchemaEntry('2.14.0.0');
		}

		if(version_compare($version, '2.14.0.1') < 0) {
			$this->db->query("CREATE INDEX id_values ON `" . DB_PREFIX . "ms_product_custom_field` (product_id, custom_field_id, custom_field_value_id)");
			$this->db->query("CREATE INDEX text_values ON `" . DB_PREFIX . "ms_product_custom_field` (product_id, custom_field_id, `value`(100))");

			$this->_createSchemaEntry('2.14.0.1');
		}

		if(version_compare($version, '2.14.1.0') < 0) {
			$upgrade_info['8.18.1'][] = "Seller store layout improvements";
			$upgrade_info['8.18.1'][] = "Various bugfixes";

			$upgrade_msg = "Starting with version 8.18.1, MultiMerch does not require the following files or folders anymore – please remove them from your system manually:";
			$upgrade_msg .= "<ul>";
			$upgrade_msg .= "<li>vqmod/xml/multimerch_core_seourl.xml</li>";
			$upgrade_msg .= "<li>vqmod/xml/multimerch_oc21x_controllerfix.xml</li>";
			$upgrade_msg .= "<li>vqmod/xml/multimerch_pre_oc23_fixes.xml</li>";
			$upgrade_msg .= "</ul>";

			$upgrade_info['8.18.1'][] = $upgrade_msg;

			$this->_createSchemaEntry('2.14.1.0');
		}

		/******************************************* RELEASE 8.20 ******************************************************/


		if(version_compare($version, '2.20.0.0') < 0) {
			// Fix field type to accept long values
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_seller_setting` MODIFY `value` TEXT;");

			/**
			 * Complaints system.
			 */

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_complaints_enable',
				'value' => 0
			));

			$this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_complaint` (
				`complaint_id` int(11) NOT NULL AUTO_INCREMENT,
				`customer_id` int(11) NOT NULL,
				`seller_id` int(11) NOT NULL,
				`product_id` int(11) NOT NULL,
				`comment` text NOT NULL DEFAULT '',
				`date_added` DATETIME NOT NULL,
				 PRIMARY KEY (`complaint_id`)) DEFAULT CHARSET=utf8
             ");

			$this->load->model('user/user_group');
			$user_groups = $this->model_user_user_group->getUserGroups();

			foreach ($user_groups as $user_group) {
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'access', 'multimerch/complaint');
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'modify', 'multimerch/complaint');
			}

			/**
			 * Notifications system.
			 */

			$this->db->query("
				CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_notification` (
				`notification_id` int(11) NOT NULL AUTO_INCREMENT,
				`producer_type` varchar(10) DEFAULT NULL,
				`producer_id` int(11) NOT NULL DEFAULT 0,
				`consumer_type` varchar(10) DEFAULT NULL,
				`consumer_id` int(11) NOT NULL DEFAULT 0,
				`read` tinyint DEFAULT 0,
				`date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`date_read` datetime DEFAULT NULL,
				PRIMARY KEY (`notification_id`)) default CHARSET=utf8
			");

			$this->db->query("
				CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_notification_channel` (
				`notification_channel_id` int(11) NOT NULL AUTO_INCREMENT,
				`notification_id` int(11) NOT NULL,
				`channel` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`notification_channel_id`)) default CHARSET=utf8
			");

			$this->db->query("
				CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_notification_object` (
				`notification_object_id` int(11) NOT NULL AUTO_INCREMENT,
				`notification_id` int(11) NOT NULL,
				`object_type` varchar(255) DEFAULT NULL,
				`object_subtype` varchar(255) DEFAULT NULL,
				`object_id` int(11) DEFAULT 0,
				`object_action` varchar(255) DEFAULT NULL,
				`metadata` text DEFAULT NULL,
				PRIMARY KEY (`notification_object_id`)) default CHARSET=utf8
			");

			foreach ($user_groups as $user_group) {
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'access', 'multimerch/notification');
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'modify', 'multimerch/notification');
			}

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_events',
				'value' => [
					// Admin events
					'admin.created.message'					=> ['mail', 'onsite'],
					'admin.created.payout'					=> ['mail', 'onsite'],
					'admin.updated.account.status'			=> ['mail', 'onsite'],
					'admin.updated.order.status'			=> ['mail', 'onsite'],

					// Customer events
					'customer.created.account'				=> ['mail', 'onsite'],
					'customer.created.order'				=> ['mail', 'onsite'],
					'customer.created.ms_review'			=> ['mail', 'onsite'],
					'customer.created.message'				=> ['mail', 'onsite'],

					// Seller events
					'seller.created.account'				=> ['mail', 'onsite'],
					'seller.created.message'				=> ['mail', 'onsite'],
					'seller.created.product'				=> ['mail', 'onsite'],
					'seller.updated.invoice.status'			=> ['mail', 'onsite'],

					// System events
					'system.created.invoice'				=> ['mail', 'onsite']
				]
			));

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_count_unack_per_user',
				'value' => ['admin.0' => 0]
			));

			/**
			 * Stripe destination charges
			 */

			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'ms_stripe_connect',
				'key' => 'ms_stripe_connect_charging_approach',
				'value' => 'direct'
			));

			/**
			 * Create customer settings table.
			 */

			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_customer_setting` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`customer_id` int(11) unsigned DEFAULT NULL,
			`name` varchar(255) DEFAULT NULL,
			`value` text DEFAULT NULL,
			`is_encoded` smallint(1) unsigned DEFAULT NULL,
			PRIMARY KEY (`id`)) DEFAULT CHARSET=utf8;");

			$this->db->query("CREATE UNIQUE INDEX customer_id_name ON `" . DB_PREFIX ."ms_customer_setting` (customer_id, `name`)");

			/**
			 * Detect whether the seller is a customer
			 */

			// Get all sellers
			$sellers = $this->MsLoader->MsSeller->getSellers();

			// Check whether seller has at least one order
			foreach ($sellers as $seller) {
				$result = $this->db->query("SELECT 1 FROM `" . DB_PREFIX . "order` WHERE `customer_id` = " . (int)$seller['seller_id']);

				$this->MsLoader->MsSetting->createSellerSetting([
					'seller_id' => $seller['seller_id'],
					'settings' => [
						'slr_is_customer' => $result->num_rows ? 1 : 0
					]
				]);
			}

			$upgrade_info['8.20'][] = "Multichannel notifications.";
			$upgrade_info['8.20'][] = "Holiday mode for sellers.";
			$upgrade_info['8.20'][] = "Customer complaint system.";
			$upgrade_info['8.20'][] = "Direct/destination charging approaches for Stripe payments.";
			$upgrade_info['8.20'][] = "Various bugfixes.";

			$upgrade_msg = "Starting with version 8.20, MultiMerch does not require the following folder anymore – please remove it from your system manually:";
			$upgrade_msg .= "<ul>";
			$upgrade_msg .= "<li>system/vendor/multimerchlib/core/</li>";
			$upgrade_msg .= "</ul>";

			$upgrade_info['8.20'][] = $upgrade_msg;

			$this->_createSchemaEntry('2.20.0.0');
		}

		if(version_compare($version, '2.21.0.0') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice` (
			`invoice_id` int(11) NOT NULL AUTO_INCREMENT,
			`invoice_no` varchar(255) DEFAULT '',
			`payment_id` int(11) DEFAULT NULL,
			`type` varchar(255) DEFAULT '',
			`object_type` varchar(255) DEFAULT NULL,
			`object_subtype` varchar(255) DEFAULT NULL,
			`object_id` int(11) DEFAULT NULL,
			`sender_type` varchar(10) DEFAULT '',
			`sender_id` int(11) DEFAULT NULL,
			`recipient_type` varchar(10) DEFAULT '',
			`recipient_id` int(11) DEFAULT NULL,
			`status` tinyint DEFAULT " . \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID . ",
			`total` decimal(15,4) NOT NULL DEFAULT 0,
			`currency_id` int(11) NOT NULL DEFAULT " . $this->currency->getId($this->config->get('config_currency')) . ",
			`currency_code` varchar(3) NOT NULL DEFAULT '" . $this->config->get('config_currency') . "',
			`date_due` datetime DEFAULT NULL,
			`date_generated` datetime DEFAULT CURRENT_TIMESTAMP,
			`date_updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`metadata` text NOT NULL DEFAULT '',
			PRIMARY KEY (`invoice_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_total` (
			`invoice_total_id` int(11) NOT NULL AUTO_INCREMENT,
			`invoice_id` int(11) NOT NULL,
			`type` varchar(255) NOT NULL DEFAULT '',
			`value` decimal(15,4) NOT NULL DEFAULT 0,
			PRIMARY KEY (`invoice_total_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_total_description` (
			`invoice_total_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`title` text NOT NULL DEFAULT '',
			PRIMARY KEY (`invoice_total_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_item` (
			`invoice_item_id` int(11) NOT NULL AUTO_INCREMENT,
			`invoice_id` int(11) DEFAULT NULL,
			`quantity` int(11) NOT NULL DEFAULT 1,
			`price` decimal(15,4) NOT NULL DEFAULT 0,
			`metadata` text NOT NULL DEFAULT '',
			PRIMARY KEY (`invoice_item_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_item_description` (
			`invoice_item_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`title` text NOT NULL DEFAULT '',
			PRIMARY KEY (`invoice_item_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_item_total` (
			`invoice_item_total_id` int(11) NOT NULL AUTO_INCREMENT,
			`invoice_item_id` int(11) NOT NULL,
			`type` varchar(255) NOT NULL DEFAULT '',
			`value` decimal(15,4) NOT NULL DEFAULT 0,
			PRIMARY KEY (`invoice_item_total_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_invoice_item_total_description` (
			`invoice_item_total_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL,
			`title` text NOT NULL DEFAULT '',
			PRIMARY KEY (`invoice_item_total_id`, `language_id`)) default CHARSET=utf8");

			// Add permissions for invoice route
			$this->load->model('user/user_group');
			$user_groups = $this->model_user_user_group->getUserGroups();

			foreach ($user_groups as $user_group) {
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'access', 'multimerch/invoice');
				$this->model_user_user_group->addPermission($user_group['user_group_id'], 'modify', 'multimerch/invoice');
			}

			// Alter `ms_pg_payment` table
			if (!$this->columnExists(DB_PREFIX . 'ms_pg_payment', 'customer_id')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_pg_payment` ADD `customer_id` int(11) DEFAULT NULL AFTER `seller_id`");
			}

			$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_pg_payment` CHANGE `seller_id` `seller_id` int(11) NULL DEFAULT NULL;");

			// Firstly, create a backup table
			$this->createTableBackup(DB_PREFIX . 'ms_pg_request');

			// Transfer existing invoices from `ms_pg_request` table to `ms_invoice` table
			if ($this->tableExists(DB_PREFIX . 'ms_pg_request')) {
				$ms_pg_requests = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_pg_request`");

				$invoice_manager = new \MultiMerch\Core\Invoice\Manager($this->MsLoader->getRegistry());

				foreach ($ms_pg_requests->rows as $request) {
					$seller_id = $request['seller_id'];
					$product_id = $request['product_id'];
					$order_id = $request['order_id'];
					$total = $request['amount'];

					$invoice_metadata = [
						'invoice_id' => $request['request_id'],
						'payment_id' => $request['payment_id'],
						'status' => (2 === (int)$request['request_status']) ? \MultiMerch\Core\Invoice\Invoice::STATUS_PAID : \MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID,
						'currency_id' => $request['currency_id'],
						'currency_code' => $request['currency_code'],
						'date_generated' => $request['date_created'],
						'date_updated' => $request['date_modified']
					];

					if (1 === (int)$request['request_type']) {
						$invoice_manager->createSellerSignupInvoice($seller_id, $total, false, $invoice_metadata);
					} elseif (2 === (int)$request['request_type']) {
						$invoice_manager->createProductListingInvoice($product_id, $seller_id, $total, false, $invoice_metadata);
					} elseif (3 === (int)$request['request_type']) {
						$invoice_manager->createSellerPayoutInvoice($seller_id, $total, false, $invoice_metadata);
					} elseif (6 === (int)$request['request_type'] && $request['payment_id']) {
						// Check if payment exists
						$payment = $this->MsLoader->MsPgPayment->getPayments(['payment_id' => $request['payment_id'], 'single' => true]);

						if ($payment) {
							// Check if order exists
							$this->load->model('sale/order');
							$order_info = $this->model_sale_order->getOrder($order_id);
							if (!$order_info)
								continue;

							switch ($payment['payment_code']) {
								case 'ms_pp_adaptive':
									// Create invoices from each seller in order to customer for their products
									$invoice_manager->createSaleInvoiceFromPlatformToCustomer($order_id, $order_info['customer_id'], false, $invoice_metadata);
									break;

								case 'ms_stripe_connect':
								default:
									// Create invoices from each seller in order to customer for their products
									$invoice_manager->createStripeSaleInvoiceFromSellerToCustomerForProducts($order_id, $order_info['customer_id'], $request['seller_id'], false, $invoice_metadata);
									break;
							}
						}
					}
				}

				// Delete old `ms_pg_request` table
				$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_pg_request`");
			}

			$upgrade_info['8.21'][] = "Added a new core invoicing architecture.";
			$upgrade_info['8.21'][] = "Added initial support for Journal3.";
			$upgrade_info['8.21'][] = "Improved search indexing for product tabs.";
			$upgrade_info['8.21'][] = "Improved the appearance of seller stores, profiles and filters.";
			$upgrade_info['8.21'][] = "Fixed image removal on mobile devices.";
			$upgrade_info['8.21'][] = "Fixed attribute sorting in product filters.";
			$upgrade_info['8.21'][] = "Fixed PHP 7.x deprecated functions.";
			$upgrade_info['8.21'][] = "Fixed global scope of notifications.";
			$upgrade_info['8.21'][] = "Fixed minor registration issues.";
			$upgrade_info['8.21'][] = "Fixed conversation participant types.";
			$upgrade_info['8.21'][] = "Fixed various undefined variable issues.";
			$upgrade_info['8.21'][] = "Fixed platform email headers.";
			$upgrade_info['8.21'][] = "Fixed Basel integration issues.";
			$upgrade_info['8.21'][] = "Fixed various minor issues.";

			$upgrade_msg = "Starting with version 8.21, MultiMerch does not require the following files or folders anymore – please remove them from your system manually:";
			$upgrade_msg .= "<ul>";
			$upgrade_msg .= "<li>admin/controller/multimerch/payment-request.php</li>";
			$upgrade_msg .= "<li>admin/view/javascript/multimerch/payment.js</li>";
			$upgrade_msg .= "<li>admin/view/javascript/multimerch/payment-request.js</li>";
			$upgrade_msg .= "<li>admin/view/template/multiseller/payment-request.tpl</li>";
			$upgrade_msg .= "<li>catalog/controller/seller/account-payment-request.php</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/multiseller/account-payment-request.tpl</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/product/mm_review_comment.tpl</li>";
			$upgrade_msg .= "<li>system/library/mspgrequest.php</li>";
			$upgrade_msg .= "</ul>";

			$upgrade_info['8.21'][] = $upgrade_msg;

			$this->_createSchemaEntry('2.21.0.0');
		}

		if (version_compare($version, '2.22.0.0') < 0) {
			// Primary OpenCart and MultiMerch categories for products
			if (!$this->columnExists(DB_PREFIX . 'ms_product', 'primary_oc_category')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_product` ADD `primary_oc_category` int(11) DEFAULT NULL;");
			}

			if (!$this->columnExists(DB_PREFIX . 'ms_product', 'primary_ms_category')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_product` ADD `primary_ms_category` int(11) DEFAULT NULL;");
			}

			// Create new shipping settings

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_from_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_processing_time_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_shipping_method_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_delivery_time_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_cost_ai_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_field_cost_pwu_enabled',
				'value' => 1
			]);

			// Update existing shipping settings
			$new_ms_shipping_type = $per_product_override_allow = 0;

			switch ((int)$this->config->get('msconf_shipping_type')) {
				case 1:
					$new_ms_shipping_type = 1; // OC shipping
					break;

				case 2:
					$ms_vendor_shipping_type = $this->config->get('msconf_vendor_shipping_type');
					if (1 === (int)$ms_vendor_shipping_type) {
						$new_ms_shipping_type = 2; // weight-based
					} elseif (2 === (int)$ms_vendor_shipping_type) {
						$new_ms_shipping_type = 5; // per-product
						$per_product_override_allow = 1;
					} elseif (3 === (int)$ms_vendor_shipping_type) {
						$new_ms_shipping_type = 2; // old both = weight-based + option to override by per-product rules
						$per_product_override_allow = 1;
					} else {
						$new_ms_shipping_type = 0; // disabled
					}
					break;

				case 0:
				default:
					$new_ms_shipping_type = 0; // disabled
					break;
			}

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_type',
				'value' => $new_ms_shipping_type
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_per_product_override_allow',
				'value' => $per_product_override_allow
			]);

			// Create settings for Worldwide destination option id
			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_shipping_worldwide_destination_id',
				'value' => 0
			]);

			// Delete leftover `msconf_vendor_shipping_type` setting
			$this->MsLoader->MsHelper->deleteOCSetting('msconf', 'msconf_vendor_shipping_type');

			// Create new tables for shipping system

			// Table for cart weight-based rules
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_cart_weight` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`shipping_method_id` int(11) DEFAULT NULL,
			`delivery_time_id` int(11) DEFAULT NULL,
			`weight_from` decimal(15,4) NOT NULL DEFAULT 0,
			`weight_to` decimal(15,4) NOT NULL DEFAULT 99999,
			`weight_class_id` int(11) NOT NULL,
			`cost` decimal(15,4) NOT NULL DEFAULT 0,
			`cost_per_weight_unit` DECIMAL(15,4) NOT NULL DEFAULT 0,
			`currency_id` int(11) NOT NULL,
			`currency_code` varchar(3) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_cart_weight_destination` (
			`rule_id` int(11) NOT NULL,
			`geo_zone_id` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`rule_id`, `geo_zone_id`)) default CHARSET=utf8");

			// Table for cart total-based rules
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_cart_total` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`shipping_method_id` int(11) DEFAULT NULL,
			`delivery_time_id` int(11) DEFAULT NULL,
			`total_from` decimal(15,4) NOT NULL DEFAULT 0,
			`total_to` decimal(15,4) NOT NULL DEFAULT 99999,
			`cost` decimal(15,4) NOT NULL DEFAULT 0,
			`currency_id` int(11) NOT NULL,
			`currency_code` varchar(3) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_cart_total_destination` (
			`rule_id` int(11) NOT NULL,
			`geo_zone_id` int(11) NOT NULL,
			PRIMARY KEY (`rule_id`, `geo_zone_id`)) default CHARSET=utf8");

			// Table for flat rules
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_flat` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`shipping_method_id` int(11) DEFAULT NULL,
			`delivery_time_id` int(11) DEFAULT NULL,
			`cost` decimal(15,4) NOT NULL DEFAULT 0,
			`currency_id` int(11) NOT NULL,
			`currency_code` varchar(3) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_flat_destination` (
			`rule_id` int(11) NOT NULL,
			`geo_zone_id` int(11) NOT NULL,
			PRIMARY KEY (`rule_id`, `geo_zone_id`)) default CHARSET=utf8");

			// Table for per-product rules
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_per_product` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`shipping_method_id` int(11) DEFAULT NULL,
			`delivery_time_id` int(11) DEFAULT NULL,
			`cost` decimal(15,4) NOT NULL DEFAULT 0,
			`cost_per_additional_item` decimal(15,4) NOT NULL DEFAULT 0,
			`currency_id` int(11) NOT NULL,
			`currency_code` varchar(3) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_shipping_rule_per_product_destination` (
			`rule_id` int(11) NOT NULL,
			`geo_zone_id` int(11) NOT NULL,
			PRIMARY KEY (`rule_id`, `geo_zone_id`)) default CHARSET=utf8");

			// Table for suborder shipping data
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_suborder_shipping_data` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`suborder_id` int(11) NOT NULL,
			`shipping_method_name` varchar(255) DEFAULT NULL,
			`cost` decimal(15,4) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			// Update existing shipping tables

			// Update `ms_order_product_shipping_data` table
			$this->createTableBackup(DB_PREFIX . "ms_order_product_shipping_data");

			if ($this->tableExists(DB_PREFIX . "ms_order_product_shipping_data")) {
				// Create field for shipping method name in `ms_order_product_shipping_data` table
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_order_product_shipping_data` ADD `shipping_method_name` varchar(255) DEFAULT NULL AFTER `order_product_id`");

				// Get shipping method name by `fixed_shipping_method_id` and `combined_shipping_method_id`
				$order_product_shipping_data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_order_product_shipping_data`");

				$shipping_methods_names = [];
				$suborder_shipping = [];

				foreach ($order_product_shipping_data->rows as $data) {
					if (!is_null($data['fixed_shipping_method_id'])) {
						// Per-product rule was applied
						$shipping_method_id = $data['fixed_shipping_method_id'];
						$name = null;

						if (isset($shipping_methods_names[$shipping_method_id])) {
							$name = $shipping_methods_names[$shipping_method_id];
						} else {
							$shipping_method_name = $this->db->query("
								SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description`
								WHERE `shipping_method_id` = " . (int)$shipping_method_id . "
								AND `language_id` = " . (int)$this->config->get('config_language_id')
							);

							if (isset($shipping_method_name->row['name'])) {
								$name = $shipping_methods_names[$shipping_method_id] = $shipping_method_name->row['name'];
							}
						}

						$this->db->query("
							UPDATE `" . DB_PREFIX . "ms_order_product_shipping_data`
							SET `shipping_method_name` = " . (!is_null($name) ? "'{$this->db->escape($name)}'" : 'NULL') . "
							WHERE `order_product_shipping_id` = " . (int)$data['order_product_shipping_id']
						);
					} elseif (!is_null($data['combined_shipping_method_id'])) {
						// Weight based shipping rule was applied
						$seller = $this->db->query("
							SELECT `seller_id`
							FROM `" . DB_PREFIX . "ms_order_product_data`
							WHERE `order_id` = " . (int)$data['order_id'] . "
								AND `product_id` = " . (int)$data['product_id'] . "
								AND `order_product_id` = " . (int)$data['order_product_id'] . "
						");

						if (isset($seller->row['seller_id'])) {
							$suborder_shipping["{$data['order_id']}_{$seller->row['seller_id']}_{$data['combined_shipping_method_id']}"][] = $data['shipping_cost'];
						}

						// Delete record from `ms_order_product_shipping_data`
						$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_order_product_shipping_data` WHERE `order_product_shipping_id` = " . (int)$data['order_product_shipping_id']);
					} else {
						// Redundant shipping record for a product
						// Additional check, whether the cost is 0
						if (0.0000 === (float)$data['shipping_cost']) {
							$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_order_product_shipping_data` WHERE `order_product_shipping_id` = " . (int)$data['order_product_shipping_id']);
						}
					}
				}

				// Transfer data to `ms_suborder_shipping_data` table
				foreach ($suborder_shipping as $shipping_data => $shipping_costs) {
					$parts = explode('_', $shipping_data);
					$order_id = $parts[0];
					$seller_id = $parts[1];
					$shipping_method_id = $parts[2];

					$suborder = $this->MsLoader->MsSuborder->getSuborders([
						'order_id' => $order_id,
						'seller_id' => $seller_id,
						'include_abandoned' => true,
						'single' => true
					]);

					if (isset($suborder['suborder_id'])) {
						$suborder_id = $suborder['suborder_id'];

						$name = null;

						if (isset($shipping_methods_names[$shipping_method_id])) {
							$name = $shipping_methods_names[$shipping_method_id];
						} else {
							$shipping_method_name = $this->db->query("
								SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description`
								WHERE `shipping_method_id` = " . (int)$shipping_method_id . "
								AND `language_id` = " . (int)$this->config->get('config_language_id')
							);

							if (isset($shipping_method_name->row['name'])) {
								$name = $shipping_methods_names[$shipping_method_id] = $shipping_method_name->row['name'];
							}
						}

						$total_shipping_cost = 0;
						foreach ($shipping_costs as $shipping_cost) {
							$total_shipping_cost += $shipping_cost;
						}

						$this->MsLoader->MsShipping->addSuborderShippingData([
							'suborder_id' => $suborder_id,
							'shipping_method_name' => $name,
							'cost' => round($total_shipping_cost, 2)
						]);
					}
				}

				// Delete leftover fields
				$this->db->query("
					ALTER TABLE `" . DB_PREFIX . "ms_order_product_shipping_data`
					DROP COLUMN fixed_shipping_method_id,
					DROP COLUMN combined_shipping_method_id;
				");
			}

			// Transfer data from old tables to the new ones

			// Combined shipping rules -> cart weight-based rules
			$this->createTableBackup(DB_PREFIX . "ms_seller_shipping");
			$this->createTableBackup(DB_PREFIX . "ms_seller_shipping_location");

			if ($this->tableExists(DB_PREFIX . "ms_seller_shipping") && $this->tableExists(DB_PREFIX . "ms_seller_shipping_location")) {
				$seller_shipping_data = $this->db->query("
					SELECT
						slrs.seller_id,
						slrsl.shipping_method_id,
						slrsl.delivery_time_id,
						slrsl.to_geo_zone_id,
						slrsl.weight_from,
						slrsl.weight_to,
						slrsl.weight_class_id,
						slrsl.cost_fixed,
						slrsl.cost_pwu
					FROM `" . DB_PREFIX . "ms_seller_shipping` slrs
					LEFT JOIN `" . DB_PREFIX . "ms_seller_shipping_location` slrsl
						ON (slrsl.seller_shipping_id = slrs.seller_shipping_id)
				");

				foreach ($seller_shipping_data->rows as $rule) {
					$data = [
						'seller_id' => $rule['seller_id'],
						'shipping_method_id' => $rule['shipping_method_id'],
						'delivery_time_id' => $rule['delivery_time_id'],
						'weight_from' => $rule['weight_from'],
						'weight_to' => $rule['weight_to'],
						'weight_class_id' => $rule['weight_class_id'],
						'cost' => $rule['cost_fixed'],
						'cost_per_weight_unit' => $rule['cost_pwu'],
						'destinations' => [$rule['to_geo_zone_id']],
					];

					$this->MsLoader->MsShipping->createCartWeightBasedRule($data);
				}

				$seller_shipping_data_2 = $this->db->query("
					SELECT
						`seller_id`,
						GROUP_CONCAT(`from_country_id` separator ',') as `country_ids`,
						GROUP_CONCAT(`processing_time` separator ',') as `processing_times`
					FROM `" . DB_PREFIX . "ms_seller_shipping`
					GROUP BY `seller_id`
				");

				foreach ($seller_shipping_data_2->rows as $data) {
					$country_ids = explode(',', $data['country_ids']);
					$from_country_id = isset($country_ids[0]) ? $country_ids[0] : null;

					$processing_times = explode(',', $data['processing_times']);
					$processing_time = isset($processing_times[0]) ? $processing_times[0] : null;

					$this->MsLoader->MsSetting->createSellerSetting([
						'seller_id' => $data['seller_id'],
						'settings' => [
							'slr_shipping_from_country_id' => $from_country_id,
							'slr_shipping_processing_days' => $processing_time
						]
					]);
				}

				$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_seller_shipping`");
				$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_seller_shipping_location`");
			}

			// Per-product shipping rules
			$this->createTableBackup(DB_PREFIX . "ms_product_shipping");
			$this->createTableBackup(DB_PREFIX . "ms_product_shipping_location");

			if ($this->tableExists(DB_PREFIX . "ms_product_shipping") && $this->tableExists(DB_PREFIX . "ms_product_shipping_location")) {
				$per_product_shipping_data = $this->db->query("
					SELECT
						ps.product_id,
						ps.processing_time,
						psl.to_geo_zone_id,
						psl.shipping_method_id,
						psl.delivery_time_id,
						psl.cost,
						psl.additional_cost
					FROM `" . DB_PREFIX . "ms_product_shipping` ps
					LEFT JOIN `" . DB_PREFIX . "ms_product_shipping_location` psl
						ON (psl.product_id = ps.product_id)
				");

				foreach ($per_product_shipping_data->rows as $rule) {
					$data = [
						'seller_id' => $this->MsLoader->MsProduct->getSellerId($rule['product_id']),
						'product_id' => $rule['product_id'],
						'shipping_method_id' => $rule['shipping_method_id'],
						'delivery_time_id' => $rule['delivery_time_id'],
						'cost' => $rule['free_shipping'] ? 0 : $rule['cost'],
						'cost_per_additional_item' => $rule['free_shipping'] ? 0 : $rule['additional_cost'],
						'destinations' => [$rule['to_geo_zone_id']],
					];

					$this->MsLoader->MsShipping->createPerProductRule($data);
				}

				$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_product_shipping`");
				$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_product_shipping_location`");
			}

			// Simplified category listing
			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_enforce_childmost_categories',
				'value' => 0
			]);

			$upgrade_info['8.22'][] = "Added price-based vendor shipping mode";
			$upgrade_info['8.22'][] = "Added combined flat vendor shipping mode";
			$upgrade_info['8.22'][] = "Added per-product-only vendor shipping mode";
			$upgrade_info['8.22'][] = "Added a new category creation/management page";
			$upgrade_info['8.22'][] = "Added product filtering by manufacturer";
			$upgrade_info['8.22'][] = "Added product filters in vendor stores";
			$upgrade_info['8.22'][] = "Added simplified category listing mode";
			$upgrade_info['8.22'][] = "Improved shipping field management for admins";
			$upgrade_info['8.22'][] = "Improved shipping rule management for vendors";
			$upgrade_info['8.22'][] = "Improved shipping method selection at checkout";
			$upgrade_info['8.22'][] = "Improved primary category handling for SEO";
			$upgrade_info['8.22'][] = "Improved product publishing for digital marketplaces";
			$upgrade_info['8.22'][] = "Improved the appearance of product filters";
			$upgrade_info['8.22'][] = "Fixed language flags in text fields in single-language setups";
			$upgrade_info['8.22'][] = "Fixed sale invoice notifications to customers";
			$upgrade_info['8.22'][] = "Various bugfixes";

			$upgrade_msg = "Starting with version 8.22, MultiMerch does not require the following files anymore – please remove them from your system manually:";
			$upgrade_msg .= "<ul>";
			$upgrade_msg .= "<li>catalog/controller/multimerch/checkout_shipping_method.php</li>";
			$upgrade_msg .= "<li>catalog/view/theme/default/template/multimerch/checkout/shipping_method.php</li>";
			$upgrade_msg .= "</ul>";

			$upgrade_info['8.22'][] = $upgrade_msg;

			$this->_createSchemaEntry('2.22.0.0');
		}

		if (version_compare($version, '2.23.0.0') < 0) {

			// SUBORDER TOTALS

			// Create suborder totals table
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_suborder_total` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`suborder_id` int(11) NOT NULL,
			`code` varchar(32) NOT NULL DEFAULT '',
			`title` varchar(255) NOT NULL DEFAULT '',
			`value` decimal(15,4) NOT NULL DEFAULT 0,
			`sort_order` int(3) NOT NULL DEFAULT 1,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			// 1. Get all existing suborders
			$suborders = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_suborder`")->rows;

			$this->load->model('sale/order');
			foreach ($suborders as $suborder) {
				// 2. Calculate suborder totals
				$suborder_totals = [
					'sub_total' => [
						'title' => $this->language->get('ms_invoice_total_title_subtotal'),
						'value' => 0,
						'sort_order' => 1
					],
					'mm_shipping_total' => [
						'title' => $this->language->get('ms_report_column_shipping'),
						'value' => 0,
						'sort_order' => 2
					],
					'tax' => [
						'title' => $this->language->get('ms_report_column_tax'),
						'value' => 0,
						'sort_order' => 3
					],
					'ms_coupon' => [ // Only one coupon per seller could've been used up to MM 8.23
						'title' => '',
						'value' => 0,
						'sort_order' => 4
					],
					'total' => [
						'title' => $this->language->get('ms_report_column_total'),
						'value' => 0,
						'sort_order' => 5
					],
				];

				$sub_total = $mm_shipping_total = $tax = $ms_coupon = 0;

				$suborder_products = $this->MsLoader->MsOrderData->getOrderProducts(array(
					'order_id' => $suborder['order_id'],
					'seller_id' => $suborder['seller_id']
				));

				foreach ($suborder_products as $product) {
					// 2a. Subtotal
					$sub_total += $product['total'];

					// 2b. OpenCart taxes
					$tax += ($product['tax'] * $product['quantity']);

					// 2c. MultiMerch shipping - per-product
					$mm_shipping_total += $product['shipping_cost'];
				}

				// 2c. MultiMerch shipping - combined
				$suborder_shipping_data = $this->MsLoader->MsShipping->getSuborderShippingData($suborder['suborder_id']);
				if (!empty($suborder_shipping_data['cost'])) {
					$mm_shipping_total += $suborder_shipping_data['cost'];
				}

				$order_totals = $this->model_sale_order->getOrderTotals($suborder['order_id']);
				foreach ($order_totals as $order_total) {
					// 2d. MultiMerch coupon
					if ('ms_coupon' === (string)$order_total['code']) {
						// Check coupon is by seller
						$coupon_code = '';
						$seller_nickname = $this->MsLoader->MsSeller->getSellerNickname($suborder['seller_id']);

						$start = strpos($order_total['title'], '(') + 1;
						$end = strrpos($order_total['title'], ')');

						if ($start && $end) {
							$coupon_code = substr($order_total['title'], $start, $end - $start);
						}

						if ($coupon_code && false !== strpos($order_total['title'], $seller_nickname)) {
							$suborder_totals['ms_coupon']['title'] = $order_total['title'];
							$ms_coupon += $order_total['value'];
						}
					}
				}

				// Total
				$total = $sub_total + $mm_shipping_total + $tax + $ms_coupon;

				$suborder_totals['sub_total']['value'] = $sub_total;
				$suborder_totals['mm_shipping_total']['value'] = $mm_shipping_total;
				$suborder_totals['tax']['value'] = $tax;
				$suborder_totals['ms_coupon']['value'] = $ms_coupon;
				$suborder_totals['total']['value'] = $total;

				foreach ($suborder_totals as $code => $suborder_total) {
					$this->MsLoader->MsSuborder->createSuborderTotal(
						$suborder['suborder_id'],
						$code,
						$suborder_total['title'],
						$suborder_total['value'],
						$suborder_total['sort_order']
					);
				}
			}

			// COMMISSIONS

			// Create commission fields in `ms_suborder` table
			$this->createTableBackup(DB_PREFIX . 'ms_suborder');

			if (!$this->columnExists(DB_PREFIX . 'ms_suborder', 'store_commission_flat')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_suborder` ADD `store_commission_flat` decimal(15, 4) NOT NULL DEFAULT 0 AFTER `order_status_id`;");
			}

			if (!$this->columnExists(DB_PREFIX . 'ms_suborder', 'store_commission_pct')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_suborder` ADD `store_commission_pct` decimal(15, 4) NOT NULL DEFAULT 0 AFTER `store_commission_flat`;");
			}

			// Create new settings

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_signup_fee_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_listing_fee_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_sale_fee_seller_enabled',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_sale_fee_catalog_enabled', // catalog = category + product
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_sale_fee_calculation_mode',
				'value' => 2 // 1 - per suborder + per product quantity, 2 - per order-product, backwards compatibility
			]);

			$this->createTableBackup(DB_PREFIX . 'ms_commission_rate');

			// Copy commission rates for seller groups inheriting rates from default seller group
			$default_commission = $this->db->query("SELECT `commission_id` FROM `" . DB_PREFIX . "ms_seller_group` WHERE `seller_group_id` = 1")->row;
			if (!empty($default_commission['commission_id'])) {
				$default_commission_id = $default_commission['commission_id'];

				// Get default commission rates
				$default_commission_rates = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_commission_rate` WHERE `commission_id` = " . (int)$default_commission_id)->rows;

				// Get list of seller groups
				$seller_groups = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_seller_group`")->rows;

				foreach ($seller_groups as $seller_group) {
					// If commission_id of a seller group is null or 0, it is inheriting default rates
					if (is_null($seller_group['commission_id']) || 0 === (int)$seller_group['commission_id']) {
						// Create new commission record
						$this->db->query("INSERT INTO " . DB_PREFIX . "ms_commission () VALUES()");
						$commission_id = $this->db->getLastId();

						// Copy from default rates
						foreach ($default_commission_rates as $rate) {
							$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_commission_rate` (rate_type, commission_id, flat, percent, payment_method) VALUES(" . (int)$rate['rate_type'] . ", " . (int)$commission_id . ", " . (float)$rate['flat'] . ", " . ($rate['percent'] ?: 'NULL') . ", " . (int)$rate['payment_method'] . ")");
						}

						// update seller group record
						$this->db->query("UPDATE `" . DB_PREFIX . "ms_seller_group` SET `commission_id` = " . (int)$commission_id . " WHERE `seller_group_id` = " . (int)$seller_group['seller_group_id']);
					}
				}
			}

			// Suborder status change event

			$this->MsLoader->MsHelper->updateOCSettingJson(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_events',
				'value' => [
					'admin.updated.suborder.status' => ['mail', 'onsite']
				]
			));

			$upgrade_info['8.23'][] = "Added MultiMerch's own order management interfaces for admins.";
			$upgrade_info['8.23'][] = "Improved marketplace fee calculations (see blog post).";
			$upgrade_info['8.23'][] = "Improved order total handling for vendors";
			$upgrade_info['8.23'][] = "Fixed a number of minor shipping system issues";
			$upgrade_info['8.23'][] = "Fixed Open Graph description missing from product pages";
			$upgrade_info['8.23'][] = "Fixed Stripe subscription configuration issue";
			$upgrade_info['8.23'][] = "Improved Journal3 integration";
			$upgrade_info['8.23'][] = "Fixed a few minor issues";

			$this->_createSchemaEntry('2.23.0.0');
		}

		if (version_compare($version, '2.24.0.0') < 0) {

			// MULTIMERCH WISHLISTS

			// Create wishlists table
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_wishlist` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`customer_id` int(11) NOT NULL,
			`name` varchar(255) NOT NULL DEFAULT '" . MsWishlist::NAME_DEFAULT . "',
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			// Create wishlist product table
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_wishlist_product` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`customer_id` int(11) NOT NULL,
			`wishlist_id` int(11) unsigned NOT NULL,
			`product_id` int(11) unsigned NOT NULL,
			`date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			// Create new settings

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_wishlist_enabled',
				'value' => 0
			]);


			// FAVORITE SELLERS

			// Create favorite sellers table
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_favorite_seller` (
			`customer_id` int(11) NOT NULL,
			`seller_id` int(11) NOT NULL,
			`date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`customer_id`, `seller_id`)) default CHARSET=utf8");

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_allow_favorite_sellers',
				'value' => 0
			]);

			// Permissions for new routes
			$this->load->model('user/user_group');

			// MultiMerch information
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/information');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/information');

			// MultiMerch geo zones
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/geo_zone');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/geo_zone');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/country');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/country');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/zone');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/zone');

			// MultiMerch customers
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/customer');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/customer');

			// Guest created order event
			$this->MsLoader->MsHelper->updateOCSettingJson(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_events',
				'value' => [
					'guest.created.order' => ['mail', 'onsite']
				]
			));

			$upgrade_info['8.24'][] = "Added a new product wishlist system";
			$upgrade_info['8.24'][] = "Added a new favorite seller system";
			$upgrade_info['8.24'][] = "Added new admin interfaces for pages, customers and regions";
			$upgrade_info['8.24'][] = "Improved DB query performance throughout the marketplace";
			$upgrade_info['8.24'][] = "Fixed Stripe setting bug";
			$upgrade_info['8.24'][] = "Fixed various minor issues";

			$this->_createSchemaEntry('2.24.0.0');
		}
		
		if (version_compare($version, '2.25.0.0') < 0) {

			// MULTIMERCH PRODUCT ATTRIBUTES

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_attribute` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`type` varchar(10) NOT NULL DEFAULT 'select',
			`required` tinyint NOT NULL DEFAULT 0,
			`status` tinyint NOT NULL DEFAULT 0,
			`default` tinyint NOT NULL DEFAULT 0,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_attribute_description` (
			`msf_attribute_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			`label` text NOT NULL DEFAULT '',
			`note` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_attribute_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_attribute_value` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`msf_attribute_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_attribute_value_description` (
			`msf_attribute_value_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_attribute_value_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_oc_category_msf_attribute` (
			`oc_category_id` int(11) NOT NULL,
			`msf_attribute_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`oc_category_id`, `msf_attribute_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_msf_attribute_value_id` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`msf_attribute_id` int(11) NOT NULL,
			`msf_attribute_value_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_msf_attribute_value_text` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`msf_attribute_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`value` text NOT NULL DEFAULT '',
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			// Indices
			if (!$this->indexExists(DB_PREFIX . 'ms_product_msf_attribute_value_id', 'idx_id_values')) {
				$this->db->query("CREATE INDEX idx_id_values ON `" . DB_PREFIX . "ms_product_msf_attribute_value_id` (msf_attribute_id, msf_attribute_value_id)");
			}

			if (!$this->indexExists(DB_PREFIX . 'ms_product_msf_attribute_value_text', 'idx_text_values')) {
				$this->db->query("CREATE INDEX idx_text_values ON `" . DB_PREFIX . "ms_product_msf_attribute_value_text` (msf_attribute_id, `language_id`, `value`(10))");
			}

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_msf_attribute_enabled',
				'value' => 0
			]);

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/field');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/field');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/field/attribute');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/field/attribute');

			// Images in descriptions
			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_allow_description_images',
				'value' => 1
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_description_images_type',
				'value' => 'base64'
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_max_description_image_width',
				'value' => '750'
			]);

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_max_description_image_height',
				'value' => '450'
			]);

			$upgrade_info['8.25'][] = "New typed category-based product attributes";
			$upgrade_info['8.25'][] = "Marketplace categories are now mandatory";
			$upgrade_info['8.25'][] = "Fixed guest checkout showing \"customer deleted\"";
			$upgrade_info['8.25'][] = "Fixed order deletion invoice error";
			$upgrade_info['8.25'][] = "Fixed various minor issues";
			$upgrade_info['8.25'][] = "Fixed theme integration issues";

			$this->_createSchemaEntry('2.25.0.0');
		}

		if (version_compare($version, '2.26.0.0') < 0) {

			// MULTIMERCH PRODUCT VARIATIONS

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_variation` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`status` tinyint NOT NULL DEFAULT 0,
			`default` tinyint NOT NULL DEFAULT 0,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_variation_description` (
			`msf_variation_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			`label` text NOT NULL DEFAULT '',
			`note` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_variation_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_variation_value` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`msf_variation_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_variation_value_description` (
			`msf_variation_value_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_variation_value_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_oc_category_msf_variation` (
			`oc_category_id` int(11) NOT NULL,
			`msf_variation_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`oc_category_id`, `msf_variation_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_msf_variation` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT NULL,
			`product_id` int(11) NOT NULL,
			`sku` varchar(100) DEFAULT NULL,
			`quantity` int(11) DEFAULT NULL,
			`price` decimal(15,4) DEFAULT NULL,
			`image` varchar(255) DEFAULT NULL,
			`status` tinyint NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_product_msf_variation_matrix` (
			`ms_product_msf_variation_id` int(11) NOT NULL, /* row */
			`msf_variation_id` int(11) NOT NULL, /* column */
			`msf_variation_value_id` int(11) NOT NULL, /* value */
			PRIMARY KEY (`ms_product_msf_variation_id`, `msf_variation_id`, `msf_variation_value_id`)) default CHARSET=utf8");

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_msf_variation_enabled',
				'value' => 0
			]);

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/field/variation');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/field/variation');

			$this->_createSchemaEntry('2.26.0.0');
		}

		if (version_compare($version, '2.26.0.1') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_oc_cart_product_msf_variation` (
			`cart_id` int(11) NOT NULL,
			`msf_variation` text NOT NULL DEFAULT '',
			PRIMARY KEY (`cart_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.26.0.1');
		}

		if (version_compare($version, '2.26.0.2') < 0) {
			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_order_product_msf_variation` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`order_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`order_product_id` int(11) NOT NULL,
			`msf_variation_id` int(11) NOT NULL,
			`msf_variation_value_id` int(11) NOT NULL,
			`name` varchar(255) NOT NULL DEFAULT '',
			`value` text NOT NULL DEFAULT '',
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.26.0.2');
		}

		if (version_compare($version, '2.26.1.0') < 0) {

			// MULTIMERCH SELLER PROPERTIES

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_seller_property` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`type` varchar(10) NOT NULL DEFAULT 'select',
			`required` tinyint NOT NULL DEFAULT 0,
			`status` tinyint NOT NULL DEFAULT 0,
			`default` tinyint NOT NULL DEFAULT 0,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_seller_property_description` (
			`msf_seller_property_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			`label` text NOT NULL DEFAULT '',
			`note` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_seller_property_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_seller_property_value` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`msf_seller_property_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_seller_property_value_description` (
			`msf_seller_property_value_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`name` text NOT NULL DEFAULT '',
			PRIMARY KEY (`msf_seller_property_value_id`, `language_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_seller_group_msf_seller_property` (
			`seller_group_id` int(11) NOT NULL,
			`msf_seller_property_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`seller_group_id`, `msf_seller_property_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_seller_msf_seller_property_value_id` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`msf_seller_property_id` int(11) NOT NULL,
			`msf_seller_property_value_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_seller_msf_seller_property_value_text` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) NOT NULL,
			`msf_seller_property_id` int(11) NOT NULL,
			`language_id` int(11) NOT NULL DEFAULT 1,
			`value` text NOT NULL DEFAULT '',
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_msf_seller_property_enabled',
				'value' => 0
			]);

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/field/seller-property');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/field/seller-property');

			$this->_createSchemaEntry('2.26.1.0');
		}

		if (version_compare($version, '2.26.2.0') < 0) {

			// MULTIMERCH PRODUCT FILTER BLOCK CONFIG

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "msf_default_mspf_block` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`block_type` int(11) NOT NULL,
			`block_type_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`, `block_type`, `block_type_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_category_mspf_block` (
			`ms_category_id` int(11) NOT NULL,
			`block_type` int(11) NOT NULL,
			`block_type_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`ms_category_id`, `block_type`, `block_type_id`)) default CHARSET=utf8");

			$this->db->query("
			CREATE TABLE IF NOT EXISTS`" . DB_PREFIX . "ms_oc_category_mspf_block` (
			`oc_category_id` int(11) NOT NULL,
			`block_type` int(11) NOT NULL,
			`block_type_id` int(11) NOT NULL,
			`sort_order` int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (`oc_category_id`, `block_type`, `block_type_id`)) default CHARSET=utf8");

			$this->_createSchemaEntry('2.26.2.0');
		}

		if (version_compare($version, '2.26.3.0') < 0) {

			// Tabbed Payment methods page

			$this->load->model('user/user_group');

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payment-gateway-marketplace');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payment-gateway-marketplace');

			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/payment-gateway-customer');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/payment-gateway-customer');

			// Create "Enable MultiMerch custom fields" setting (deprecated)

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'msconf',
				'key' => 'msconf_ms_custom_field_enabled',
				'value' => 1
			]);

			$upgrade_info['8.26'][] = "New product variation system";
			$upgrade_info['8.26'][] = "New custom seller property system";
			$upgrade_info['8.26'][] = "Added support for inline image uploads";
			$upgrade_info['8.26'][] = "Added product filter block configuration";
			$upgrade_info['8.26'][] = "Improved payment extension interfaces";
			$upgrade_info['8.26'][] = "Fixed various minor issues";
			$upgrade_info['8.26'][] = "Fixed theme integration issues";

			$this->_createSchemaEntry('2.26.3.0');
		}

		if (version_compare($version, '2.27.0.0') < 0) {
			$upgrade_info['8.27'][] = "Introducing Multimerch Tiles";
			$upgrade_info['8.27'][] = "Seller stores now take preference over profiles";
			$upgrade_info['8.27'][] = "Fixed admin permissions for the new custom fields";
			$upgrade_info['8.27'][] = "Fixed inline product images with server uploads";
			$upgrade_info['8.27'][] = "Fixed shipping cost values in sales reports";
			$upgrade_info['8.27'][] = "Fixed admin seller account notification emails";
			$upgrade_info['8.27'][] = "Fixed an empty product variation block being shown in some cases";
			$upgrade_info['8.27'][] = "Fixed shipping checkout on mobile devices";
			$upgrade_info['8.27'][] = "Fixed various minor issues";
			$upgrade_info['8.27'][] = "Fixed theme integration issues";

			$this->_createSchemaEntry('2.27.0.0');
		}

		if (version_compare($version, '2.28.0.0') < 0) {
			if ($this->columnExists(DB_PREFIX . 'ms_import_config', 'cell_container')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `cell_container` `delimiter` varchar(10) NOT NULL DEFAULT ';'");
			}

			if ($this->columnExists(DB_PREFIX .'ms_import_config', 'cell_separator')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `cell_separator` `enclosure` varchar(10) NOT NULL DEFAULT '\"'");
			}

			if ($this->columnExists(DB_PREFIX .'ms_import_config', 'update_key_id')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `update_key_id` `key_field` varchar(255) NOT NULL DEFAULT 'model'");
			}

			// Allow import source file from url
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_feed_external_source_enabled',
				'value' => 1
			));

			$this->load->model('user/user_group');
			$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'multimerch/feed');
			$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'multimerch/feed');

			$upgrade_info['8.28'][] = "New import system";
			$upgrade_info['8.28'][] = "New PDF packing slips and invoices";
			$upgrade_info['8.28'][] = "Fixed theme integration issues";
			$upgrade_info['8.28'][] = "Various bugfixes";

			$this->_createSchemaEntry('2.28.0.0');
		}

		if (version_compare($version, '2.28.1.0') < 0) {
			// Allow import source file from url
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_feed_primary_product_field',
				'value' => 'model'
			));

			// Allow import source file from url
			$this->MsLoader->MsHelper->createOCSetting(array(
				'code' => 'msconf',
				'key' => 'msconf_import_category_type',
				'value' => 0
			));

			$upgrade_info['8.28.1'][] = "New order templates for buyers and sellers";
			$upgrade_info['8.28.1'][] = "Added support for UTF8 to mass imports";
			$upgrade_info['8.28.1'][] = "Added support for product attributes to mass imports";
			$upgrade_info['8.28.1'][] = "Improved handling of CSV column labels";
			$upgrade_info['8.28.1'][] = "Improved handling of product variation XML changes";
			$upgrade_info['8.28.1'][] = "Fixed product attribute display in category filters";
			$upgrade_info['8.28.1'][] = "Fixed imported product statuses";
			$upgrade_info['8.28.1'][] = "Various mass import system improvements";
			$upgrade_info['8.28.1'][] = "Various minor bugfixes";

			$this->_createSchemaEntry('2.28.1.0');
		}

		if (version_compare($version, '2.28.2.0') < 0) {
			// Create default setting for Stripe SCA (if Stripe payment gateway is enabled)
			if (1 === (int)$this->config->get('ms_stripe_connect_status')) {
				$this->MsLoader->MsHelper->createOCSetting(array(
					'code' => 'ms_stripe_connect',
					'key' => 'ms_stripe_connect_sca_required',
					'value' => 0
				));
			}

			// Suborder comments
			if (!$this->columnExists(DB_PREFIX . 'ms_suborder', 'customer_comment')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_suborder` ADD `customer_comment` text DEFAULT NULL;");
			}

			$upgrade_info['8.28.2'][] = "New Stripe Checkout with SCA support";
			$upgrade_info['8.28.2'][] = "New order comments per vendor";
			$upgrade_info['8.28.2'][] = "New seller terms settings";
			$upgrade_info['8.28.2'][] = "Improved support for mobile devices";
			$upgrade_info['8.28.2'][] = "Various import system improvements";
			$upgrade_info['8.28.2'][] = "Various bugfixes";

			$this->_createSchemaEntry('2.28.2.0');
		}

		if (version_compare($version, '2.29.0.0') < 0) {
			// Create table for import entities
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_import` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`seller_id` int(11) DEFAULT NULL,
			`type` varchar(100) NOT NULL DEFAULT 'product',
			`import_config_id` int(11) DEFAULT NULL,
			`name` varchar(255) NOT NULL DEFAULT '',
			`is_scheduled` tinyint(1) NOT NULL DEFAULT 0,
			`processed` int(11) NOT NULL DEFAULT 0,
			`added` int(11) NOT NULL DEFAULT 0,
			`updated` int(11) NOT NULL DEFAULT 0,
			`errors` int(11) NOT NULL DEFAULT 0,
			`product_ids` text DEFAULT NULL,
			`date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$imports = $this->MsLoader->ModelProductImport->getHistoryLegacy();

			$this->db->query("RENAME TABLE `" . DB_PREFIX . "ms_import_history` TO `" . DB_PREFIX . "ms_import_history_backup`");

			// Create table for import history
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_import_history` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`import_id` int(11) DEFAULT NULL,
			`status` tinyint NOT NULL DEFAULT " . MM_FEED_CONST_IMPORT_STATUS_SCHEDULED . ",
			`date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			foreach ($imports as $import) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_import`
					SET `seller_id` = {$import['seller_id']},
						`type` = 'product',
						`name` = '{$this->db->escape($import['name'])}',
						`processed` = {$import['processed']},
						`added` = {$import['added']},
						`updated` = {$import['updated']},
						`errors` = {$import['errors']},
						`product_ids` = '{$this->db->escape($import['product_ids'])}',
						`date_added` = '{$import['date_added']}'
				");

				$import_id = $this->db->getLastId();

				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_import_history`
					SET `import_id` = {$import_id},
						`status` = " . MM_FEED_CONST_IMPORT_STATUS_FINISHED . ",
						`date_added` = '{$import['date_added']}'
				");
			}

			// Update import configs table
			if ($this->columnExists(DB_PREFIX . 'ms_import_config', 'customer_id')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `customer_id` `seller_id` int(11) NOT NULL");
			}
			if ($this->columnExists(DB_PREFIX .'ms_import_config', 'attachment_code')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `attachment_code` `upload_path` varchar(2048) DEFAULT NULL");
			}
			if (!$this->columnExists(DB_PREFIX .'ms_import_config', 'url_path')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` ADD `url_path` varchar(2048) DEFAULT NULL AFTER `upload_path`");
			}
			if ($this->columnExists(DB_PREFIX .'ms_import_config', 'date_modified')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `date_modified` `date_modified` datetime DEFAULT NULL");
			}

			// Create table for cron jobs
			$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_cron` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`import_id` int(11) DEFAULT NULL,
			`cycle` varchar(12) NOT NULL,
			`status` tinyint(1) NOT NULL DEFAULT " . MM_FEED_CONST_IMPORT_STATUS_SCHEDULED . ",
			`is_running` tinyint(1) NOT NULL DEFAULT 0,
			`date_start` datetime DEFAULT NULL,
			`date_next` datetime DEFAULT NULL,
			PRIMARY KEY (`id`)) default CHARSET=utf8");

			$this->MsLoader->MsHelper->updateOCSettingJson(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_events',
				'value' => [
					'system.finished.import' => ['mail', 'onsite']
				]
			));

            $this->MsLoader->MsHelper->createOCSetting(array(
                'code' => 'msconf',
                'key' => 'msconf_scheduled_import_enabled',
                'value' => 0
            ));

			$upgrade_info['8.29'][] = "New Linnworks integration";
			$upgrade_info['8.29'][] = "New Cron import scheduling";
			$upgrade_info['8.29'][] = "Fixed UTF8 in packing slips";
			$upgrade_info['8.29'][] = "Theme integration fixes";
			$upgrade_info['8.29'][] = "Various bugfixes";

			$this->_createSchemaEntry('2.29.0.0');
		}

		if (version_compare($version, '2.29.1.0') < 0) {
			if ($this->columnExists(DB_PREFIX .'ms_import_config', 'upload_path')) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "ms_import_config` CHANGE `upload_path` `attachment_code` varchar(2048) DEFAULT NULL");
			}

			$this->_createSchemaEntry('2.29.1.0');
		}

		if (version_compare($version, '2.29.2.0') < 0) {
			$this->MsLoader->MsHelper->updateOCSettingJson(array(
				'code' => 'msconf',
				'key' => 'msconf_notification_events',
				'value' => [
					'system.finished.import' => ['onsite']
				]
			));

			$this->_createSchemaEntry('2.29.2.0');
		}

		return $upgrade_info;
	}

	/**
	 * Checks whether a column exists in a specified table in the database.
	 *
	 * @param	string	$table		Table where column is checked.
	 * @param	string	$column		Column being checked.
	 * @return	bool				Whether column exists.
	 */
	public function columnExists($table, $column)
	{
		$query = $this->db->query("SHOW COLUMNS FROM `{$table}` LIKE '{$this->db->escape($column)}'");

		return $query->num_rows ? true : false;
	}

	/**
	 * Checks whether a specified table exists in the database.
	 *
	 * @param	string	$table		Table being checked.
	 * @return	bool				Whether table exists.
	 */
	public function tableExists($table)
	{
		$query = $this->db->query("SHOW TABLES LIKE '".  $this->db->escape($this->db->escape($table)) ."'");

		return $query->num_rows ? true : false;
	}

	/**
	 * Checks whether a specified index exists in the specified table.
	 *
	 * @param	string	$table		Table being checked.
	 * @param	string	$name		Index being checked.
	 * @return	bool				Whether index exists.
	 */
	public function indexExists($table, $name)
	{
		$query = $this->db->query("SHOW INDEX FROM `" . $this->db->escape($table) . "` WHERE `Key_name` = '" . $this->db->escape($name) . "'");

		return $query->num_rows ? true : false;
	}

	/**
	 * Creates backup for a specified table in the database.
	 *
	 * Backup is a full duplicate of a table, but with different name.
	 *
	 * @param	string	$table	Table to backup.
	 * @return	bool			Whether backup is created.
	 */
	public function createTableBackup($table)
	{
		$table = $this->db->escape($table);
		$backup_table = "{$table}_backup";

		if (!$this->tableExists($table) || $this->tableExists($backup_table))
			return false;

		$this->db->query("CREATE TABLE `{$backup_table}` LIKE `{$table}`");
		$this->db->query("INSERT `{$backup_table}` SELECT * FROM `{$table}`");

		return true;
	}
}
