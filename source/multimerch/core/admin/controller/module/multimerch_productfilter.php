<?php

class ControllerModuleMultiMerchProductfilter extends ControllerMultimerchBase
{
	private $module_name = 'multimerch_productfilter';
	private $settings_code = 'multimerch_productfilter';

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'), $this->load->language("module/{$this->module_name}"));
		$this->data['token'] = $this->session->data['token'];

		$this->load->model('setting/setting');
		$this->load->model('extension/module');
	}

	public function index()
	{
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['module_name'] = $this->module_name;
		$this->data['status'] = $this->config->get("{$this->settings_code}_status");

		$this->data['action'] = $this->url->link("extension/module/{$this->module_name}/jxSaveSettings", 'token=' . $this->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->data['token'], 'SSL');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('text_module'),
				'href' => $this->url->link('extension/extension', '', 'SSL')
			],
			[
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link("extension/module/{$this->module_name}", '', 'SSL')
			]
		]);

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view("module/{$this->module_name}", $this->data));
	}

	public function jxSaveSettings()
	{
		$json = [];
		$data = $this->request->post;

		$json['errors'] = $this->validateFormData($data);

		if (empty($json['errors'])) {
			foreach ($data as $key => $value) {
				$data["{$this->settings_code}_{$key}"] = $value;
				unset($data[$key]);
			}

			$this->model_setting_setting->editSetting($this->settings_code, $data);

			// If module enabled, add it to the left column of Category layout
			/*if ($data['status']) {
				// insert into oc_layout_module
			}*/

			$this->session->data['success'] = $this->language->get('text_success');
			$json['redirect'] = $this->url->link('extension/extension', 'token=' . $this->data['token'] . '&type=module', 'SSL');
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($json));
	}

	private function validateFormData($data)
	{
		$errors = [];

		if (!$this->user->hasPermission('modify', "module/{$this->module_name}"))
			$errors[] = $this->language->get('error_permission');

		if (!isset($data['status']))
			$errors[] = $this->language->get('error_status');

		return $errors;
	}

	public function jxAutocompleteMspfBlocks()
	{
		$json = [];

		if (isset($this->request->get['filter_name'])) {
			$added_ids = [
				MsProductFilter::TYPE_OC_ATTRIBUTE => [],
				MsProductFilter::TYPE_OC_OPTION => [],
				MsProductFilter::TYPE_OC_MANUFACTURER => [],
				MsProductFilter::TYPE_MSF_ATTRIBUTE => [],
				MsProductFilter::TYPE_PRICE => [],
				MsProductFilter::TYPE_CATEGORY => []
			];

			$added_ids_by_type = !empty($this->request->get['added_ids_by_type']) ? explode(',', $this->request->get['added_ids_by_type']) : [];

			foreach ($added_ids_by_type as $added_id_by_type) {
				list($block_type, $id) = explode('-', $added_id_by_type);

				if (!isset($added_ids[$block_type])) {
					continue;
				}

				$added_ids[$block_type][] = $id;
			}

			if ($this->config->get('msconf_msf_attribute_enabled')) {
				// MSF attributes
				$msf_attributes = $this->MsLoader->MsfAttribute->getList(['not_in_ids' => $added_ids[MsProductFilter::TYPE_MSF_ATTRIBUTE]], [
					'order_by'  => 'label',
					'order_way' => 'ASC',
					'filters' => ['name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))],
					'offset' => 0,
					'limit' => 3
				]);

				foreach ($msf_attributes as $msf_attribute) {
					$json[] = [
						'id' => $msf_attribute['id'],
						'label' => $msf_attribute['full_name'],
						'category' => $this->language->get('ms_product_filter_block_title_attributes'),
						'block_type' => MsProductFilter::TYPE_MSF_ATTRIBUTE
					];
				}
			} elseif (empty($added_ids[MsProductFilter::TYPE_OC_ATTRIBUTE])) {
				// OC attributes
				$json[] = [
					'id' => 0,
					'label' => $this->language->get('ms_product_filter_block_title_attributes'),
					'category' => $this->language->get('ms_product_filter_block_title_general'),
					'block_type' => MsProductFilter::TYPE_OC_ATTRIBUTE
				];

				/*// OC attributes
				$oc_attributes = $this->MsLoader->MsAttribute->ocGetAttributes([
					'filter_name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8')),
					'not_in_ids' => $added_ids[MsProductFilter::TYPE_OC_ATTRIBUTE],
					'sort' => 'ad.name',
					'order' => 'ASC',
					'start' => 0,
					'limit' => 3
				]);

				foreach ($oc_attributes as $oc_attribute) {
					$json[] = [
						'id' => $oc_attribute['attribute_id'],
						'label' => $oc_attribute['name'],
						'category' => $this->language->get('ms_product_filter_block_title_attributes'),
						'block_type' => MsProductFilter::TYPE_OC_ATTRIBUTE
					];
				}*/
			}

			if ($this->config->get('msconf_msf_variation_enabled')) {
				// MSF variations
			} elseif (empty($added_ids[MsProductFilter::TYPE_OC_OPTION])) {
				// OC options
				$json[] = [
					'id' => 0,
					'label' => $this->language->get('ms_product_filter_block_title_oc_options'),
					'category' => $this->language->get('ms_product_filter_block_title_general'),
					'block_type' => MsProductFilter::TYPE_OC_OPTION
				];

				/*// OC options
				$oc_options = $this->MsLoader->MsOption->ocGetOptions([
					'filter_name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8')),
					'not_in_ids' => $added_ids[MsProductFilter::TYPE_OC_OPTION],
					'sort' => 'od.name',
					'order' => 'ASC',
					'start' => 0,
					'limit' => 3
				]);

				foreach ($oc_options as $oc_option) {
					$json[] = [
						'id' => $oc_option['option_id'],
						'label' => $oc_option['name'],
						'category' => $this->language->get('ms_product_filter_block_title_oc_options'),
						'block_type' => MsProductFilter::TYPE_OC_OPTION
					];
				}*/
			}

			// OC manufacturers
			if (empty($added_ids[MsProductFilter::TYPE_OC_MANUFACTURER])) {
				$json[] = [
					'id' => 0,
					'label' => $this->language->get('ms_product_filter_block_title_oc_manufacturers'),
					'category' => $this->language->get('ms_product_filter_block_title_general'),
					'block_type' => MsProductFilter::TYPE_OC_MANUFACTURER
				];
			}

			// Price
			if (empty($added_ids[MsProductFilter::TYPE_PRICE])) {
				$json[] = [
					'id' => 0,
					'label' => $this->language->get('ms_product_filter_block_title_price'),
					'category' => $this->language->get('ms_product_filter_block_title_general'),
					'block_type' => MsProductFilter::TYPE_PRICE
				];
			}

			// Price
			if (empty($added_ids[MsProductFilter::TYPE_CATEGORY])) {
				$json[] = [
					'id' => 0,
					'label' => $this->language->get('ms_product_filter_block_title_category'),
					'category' => $this->language->get('ms_product_filter_block_title_general'),
					'block_type' => MsProductFilter::TYPE_CATEGORY
				];
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
