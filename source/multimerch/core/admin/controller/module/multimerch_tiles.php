<?php

class ControllerModuleMultiMerchTiles extends ControllerMultimerchBase
{
	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->load->model('localisation/language');
		$this->load->model('extension/module');

		$this->data = array_merge($this->data, $this->load->language('module/multimerch_tiles'));
	}

	public function index()
	{
		$this->document->addStyle('view/stylesheet/multimerch/multiseller.css');
		$this->document->setTitle($this->language->get('heading_title'));

		// Languages
		$languages = $this->model_localisation_language->getLanguages();
		foreach ($languages as &$language) {
			$language['image'] = "language/{$language['code']}/{$language['code']}.png";
		}

		// Module information
		$module = isset($this->request->get['module_id']) ? $this->model_extension_module->getModule($this->request->get['module_id']) : [];

		if (!isset($module['name'])) {
			$module['name'] = '';
		}

		foreach ($languages as &$language) {
			if (!isset($module['i18n'][$language['language_id']]['title'])) {
				$module['i18n'][$language['language_id']]['title'] = '';
			}

			if (!isset($module['i18n'][$language['language_id']]['subtitle'])) {
				$module['i18n'][$language['language_id']]['subtitle'] = '';
			}
		}

		if (!isset($module['link'])) {
			$module['link'] = '';
		}

		if (!isset($module['type'])) {
			$module['type'] = 'grid';
		}

		if (!isset($module['num_rows'])) {
			$module['num_rows'] = 1;
		}

		if (!isset($module['item_type'])) {
			$module['item_type'] = 'product';
		}

		if (!isset($module['selection_type'])) {
			$module['selection_type'] = 'manual';
		}

		if (!isset($module['item_ids'])) {
			$module['item_ids'] = [];
		}

		if (!isset($module['status'])) {
			$module['status'] = 1;
		}

		$module['items'] = [];

		switch ($module['item_type']) {
			case 'seller':
				$module['num_cols'] = 5;

				foreach ($module['item_ids'] as $seller_id) {
					$item = $this->MsLoader->MsSeller->getSellers([
						'seller_id' => $seller_id,
						'seller_status' => [MsSeller::STATUS_ACTIVE],
						'single' => true
					]);

					if (!empty($item)) {
						$module['items'][] = [
							'id' => $item['seller_id'],
							'name' => $item['ms.nickname']
						];
					}
				}
				break;

			case 'oc_category':
				$module['num_cols'] = 6;

				foreach ($module['item_ids'] as $oc_category_id) {
					$item = $this->MsLoader->MsCategory->getOcCategories([
						'category_id' => $oc_category_id,
						'category_status' => MsCategory::STATUS_ACTIVE,
						'single' => true
					]);

					if (!empty($item)) {
						$module['items'][] = [
							'id' => $item['category_id'],
							'name' => $item['name']
						];
					}
				}
				break;

			case 'product':
			default:
				$module['num_cols'] = 4;

				foreach ($module['item_ids'] as $product_id) {
					$item = $this->MsLoader->MsProduct->getProducts([
						'product_id' => $product_id,
						'oc_status' => MsProduct::STATUS_ACTIVE,
						'single' => true
					]);

					if (!empty($item)) {
						$module['items'][] = [
							'id' => $item['product_id'],
							'name' => $item['pd.name']
						];
					}
				}
				break;
		}

		$this->data['languages'] = $languages;
		$this->data['types'] = ['grid', 'slider'];
		$this->data['item_types'] = ['seller', 'product', 'oc_category'];
		$this->data['selection_types'] = ['manual'];
		$this->data['module'] = $module;

		$this->data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', 'SSL');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_module'),
				'href' => $this->url->link('extension/extension', 'type=module', 'SSL'),
			),
			array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/multimerch_tiles', '', 'SSL'),
			)
		));

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/multimerch_tiles', $this->data));
	}

	public function jxSave()
	{
		$data = $this->request->post;

		$errors = $this->processPost($data);

		if (!empty($errors)) {
			$this->response->addHeader('Content-Type: application/json');
			return $this->response->setOutput(json_encode(['success' => false, 'errors' => $errors]));
		}

		$module_id = !empty($data['module_id']) ? $data['module_id'] : false;

		if (!$module_id) {
			$this->model_extension_module->addModule('multimerch_tiles', $data);
		} else {
			$this->model_extension_module->editModule($module_id, $data);
		}

		$this->session->data['success'] = $this->language->get('text_success');

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode(['success' => true, 'redirect' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', 'SSL')]));
	}

	public function jxAutocompleteSellers()
	{
		$items = [];
		$input = isset($this->request->get['name']) ? ('%' . (string)$this->request->get['name'] . '%') : '';

		$sellers = $this->MsLoader->MsSeller->getSellers([
			'seller_status' => [MsSeller::STATUS_ACTIVE],
			'nickname' => $input
		]);

		foreach ($sellers as $seller) {
			$items[] = [
				'id' => $seller['seller_id'],
				'name' => $seller['ms.nickname']
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($items));
	}

	public function jxAutocompleteProducts()
	{
		$items = [];
		$input = !empty($this->request->get['name']) ? ('%' . (string)$this->request->get['name'] . '%') : '';

		$products = $this->MsLoader->MsProduct->getProducts([
			'oc_status' => MsProduct::STATUS_ACTIVE,
			'name' => $input
		], [
			'limit' => 10,
			'offset' => 0
		]);

		foreach ($products as $product) {
			$items[] = [
				'id' => $product['product_id'],
				'name' => $product['pd.name']
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($items));
	}

	public function jxAutocompleteOcCategories()
	{
		$items = [];
		$input = isset($this->request->get['name']) ? ('%' . (string)$this->request->get['name'] . '%') : '';

		$oc_categories = $this->MsLoader->MsCategory->getOcCategories([
			'category_status' => MsCategory::STATUS_ACTIVE,
			'name' => $input
		]);

		foreach ($oc_categories as $oc_category) {
			$items[] = [
				'id' => $oc_category['category_id'],
				'name' => $oc_category['name']
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode($items));
	}

	private function processPost(&$data)
	{
		$errors = [];

		// Set title as module name
		$data['name'] = !empty($data['i18n'][$this->config->get('config_language_id')]['title']) ? $data['i18n'][$this->config->get('config_language_id')]['title'] : 'Tiles';

		$languages = $this->model_localisation_language->getLanguages();
		foreach ($languages as $language) {
			if (empty($data['i18n'][$language['language_id']]['title'])) {
				$errors["i18n[{$language['language_id']}][title]"] = sprintf($this->language->get('error_title'), $language['name']);
			}
		}

		if (!isset($data['link'])) {
			$data['link'] = '';
		}

		if (!isset($data['type'])) {
			$data['type'] = 'grid';
		}

		if (!isset($data['item_type'])) {
			$data['item_type'] = 'product';
		}

		if (!isset($data['num_rows'])) {
			$data['num_rows'] = 1;
		}

		switch ($data['item_type']) {
			case 'oc_category':
				$data['num_cols'] = 6;
				break;

			case 'seller':
				$data['num_cols'] = 5;
				break;

			case 'product':
			default:
				$data['num_cols'] = 4;
				break;
		}

		if (!isset($data['selection_type'])) {
			$data['selection_type'] = 'manual';
		}

		if (empty($data['item_ids'])) {
			$errors['item_name'] = $this->language->get('error_items');
		}

		if (!isset($data['status'])) {
			$data['status'] = 1;
		}

		return $errors;
	}
}
