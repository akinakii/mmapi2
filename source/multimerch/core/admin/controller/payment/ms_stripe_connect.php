<?php
class ControllerPaymentMsStripeConnect extends Controller
{
	use \MultiMerch\Module\Traits\Stripe;

	const ENVIRONMENT_LIVE = 0;
	const ENVIRONMENT_TEST = 1;

	protected $module_name = 'ms_stripe_connect';

	/**
	 * @var array Data of the module.
	 */
	protected $data = [];

	/**
	 * @var array Default module settings.
	 */
	protected $settings = [];

	/**
	 * ControllerPaymentMsStripeConnect constructor.
	 * @param $registry
	 */
	public function __construct($registry) {
		parent::__construct($registry);

		$this->load->model('setting/setting');
		$this->load->model('localisation/currency');
		$this->load->model('localisation/order_status');

		$this->data = array_merge($this->data, $this->load->language('payment/ms_stripe_connect'));
		$this->data['token'] = $this->session->data['token'];

		$this->data['live_environment_id'] = static::ENVIRONMENT_LIVE;
		$this->data['test_environment_id'] = static::ENVIRONMENT_TEST;

		$this->settings = [
			'test_mode' => 0,
			'debug' => 0,
			'status' => 0,

			// Strong customer authentication
			'sca_required' => false,

			// Use direct charges ('direct'), destination charges ('destination') or separate charges and transfers ('separate')
			'charging_approach' => 'direct',

			'enable_subscription' => 0,
			'current_subscription_period_billed_product_ids' => [],

			'account_currency' => 'USD',

			// Map Stripe events to OC order statuses
			'event_to_order_status' => [
				'charge.succeeded' => 5,
				'charge.failed' => 10,
				'charge.refunded' => 11
			],

			'environments' => [
				static::ENVIRONMENT_LIVE => [
					// API
					'public_key' => '',
					'secret_key' => '',

					// Account
					'account_webhooks' => ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . 'index.php?route=payment/ms_stripe_connect/account_webhooks',
					'account_signing_secret' => '',

					// Connect
					'client_id' => '',
					'connect_webhooks' => ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . 'index.php?route=payment/ms_stripe_connect/connect_webhooks',
					'connect_signing_secret' => '',

					// Subscriptions
					'product_id' => ''
				],
				static::ENVIRONMENT_TEST => [
					// API
					'public_key' => '',
					'secret_key' => '',

					// Account
					'account_webhooks' => ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . 'index.php?route=payment/ms_stripe_connect/account_webhooks',
					'account_signing_secret' => '',

					// Connect
					'client_id' => '',
					'connect_webhooks' => ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . 'index.php?route=payment/ms_stripe_connect/connect_webhooks',
					'connect_signing_secret' => '',

					// Subscriptions
					'product_id' => ''
				]
			]
		];

		list($stripe_connect_settings, $environment) = $this->initStripe();
		$this->settings = array_merge($this->settings, (array)$stripe_connect_settings);
	}

	/**
	 * Entry point.
	 */
	public function index() {
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		foreach ($this->settings as $name => $value) {
			$value_from_configs = $this->config->get($this->module_name.'_'.$name);
			$this->data[$name] = isset($value_from_configs) ? $value_from_configs : $value;
		}

		$this->data['currencies'] = $this->model_localisation_currency->getCurrencies();
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->data['token'] = $this->session->data['token'];
		$this->data['action'] = $this->url->link('module/ms_stripe_connect', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', 'SSL');
		$this->data['currency_code'] = $this->config->get('config_currency');

		try {
			$this->data['plans_info'] = $this->getPlansInfo($this->settings['environments'][$this->settings['test_mode']]['product_id']);
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$this->ms_logger->error('admin/payment/ms_stripe_connect/index - ' . $e->getMessage());
		}

		$this->data['breadcrumbs'] = [
			[
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
			],
			[
				'text'      => $this->language->get('text_payment'),
				'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', 'SSL'),
				'separator' => ' :: '
			],
			[
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('extension/payment/ms_stripe_connect', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			]
		];

		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');

		$this->response->setOutput($this->load->view('payment/ms_stripe_connect', $this->data));
	}

	/**
	 * Saves settings.
	 */
	public function saveSettings() {
		$json = [];
		$data = $this->request->post;

		$json['errors'] = $this->_validate($data);

		if (empty($json['errors'])) {
			// Forbid changing Account and Connect webhooks
			foreach ([static::ENVIRONMENT_LIVE, static::ENVIRONMENT_TEST] as $environment_id) {
				$data['environments'][$environment_id]['account_webhooks'] = $this->settings['environments'][$environment_id]['account_webhooks'];
				$data['environments'][$environment_id]['connect_webhooks'] = $this->settings['environments'][$environment_id]['connect_webhooks'];
			}

			foreach ($data as $key => $value) {
				$data[$this->module_name . '_' . $key] = $value;
				unset($data[$key]);
			}

			$this->_processSettings($data);

			$json['success'] = $this->language->get('success_modified');
			$this->session->data['success'] = $this->language->get('success_modified');
		}

		$this->response->setOutput(json_encode($json));
	}

	/**
	 * AJAX: Retrieves plans and their information from Stripe.
	 *
	 * @return mixed
	 */
	public function jxRetrievePlans()
	{
		$json = [];

		if (empty($this->request->get['product_id'])) {
			$json['errors'][] = $this->language->get('error_product_id');
			return $this->response->setOutput(json_encode($json));
		}

		$stripe_product_id = $this->request->get['product_id'];

		try {
			$plans_info = $this->getPlansInfo($stripe_product_id);

			$json['success'] = $this->language->get('success_plans_retrieved');
			$json['plans_info'] = $plans_info;
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$this->ms_logger->error('admin/payment/ms_stripe_connect/jxRetrievePlans - ' . $e->getMessage());
			$json['errors'][] = $e->getMessage();
		}

		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Gets all Stripe plans information.
	 *
	 * @param	int		$stripe_product_id
	 * @return	array
	 * @throws	\MultiMerch\Module\Errors\Generic
	 */
	protected function getPlansInfo($stripe_product_id)
	{
		$ms_stripe_connect_plan_ids = $plans_info = [];

		try {
			if (empty($stripe_product_id))
				throw new \MultiMerch\Module\Errors\Generic('Error: Unable to retrieve Stripe product!');

			// Retrieve all active stripe plans for specified product
			$stripe_plans = \Stripe\Plan::all([
				'product' => $stripe_product_id,
				'active' => true
			]);

			// Store all plan ids in Stripe Connect settings
			foreach ($stripe_plans->data as $stripe_plan) {
				$plan_type = ('metered' === $stripe_plan->usage_type) ? 'per_seat' : 'base';
				$ms_stripe_connect_plan_ids[$plan_type][] = $stripe_plan->id;

				$plans_info[] = [
					'nickname' => $stripe_plan->nickname,
					'condition_base' => ('base' === $plan_type ? sprintf($this->language->get('text_plan_condition_base'), $this->currency->format($stripe_plan->amount / 100, strtoupper($stripe_plan->currency)), $this->language->get('text_plan_interval_'.$stripe_plan->interval)) : null),
					'condition_per_seat' => ('per_seat' === $plan_type ? sprintf($this->language->get('text_plan_condition_per_seat'), $this->currency->format($stripe_plan->amount / 100, strtoupper($stripe_plan->currency)), $this->language->get('text_plan_interval_'.$stripe_plan->interval)) : null),
				];
			}

			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'ms_stripe_connect',
				'key' => 'ms_stripe_connect_plan_ids',
				'value' => $ms_stripe_connect_plan_ids
			]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			throw new \MultiMerch\Module\Errors\Generic($e->getMessage());
		} catch (\Stripe\Error\ApiConnection $e) {
			throw new \MultiMerch\Module\Errors\Generic($e->getMessage());
		}

		return $plans_info;
	}

	/**
	 * Processes passed settings.
	 *
	 * @param $data
	 */
	private function _processSettings($data) {
		$settings = $this->model_setting_setting->getSetting($this->module_name);

		foreach ($this->settings as $name => $value) {
			if (!array_key_exists($this->module_name.'_'.$name, $settings))
				$settings[$this->module_name.'_'.$name] = $value;
		}

		foreach ($settings as $s => $v) {
			$settings[$s] = $this->MsLoader->MsHelper->recursiveTrim(isset($data[$s]) ? $data[$s] : $v);
		}

		$this->model_setting_setting->editSetting($this->module_name, $settings);
	}

	/**
	 * Validates passed data.
	 *
	 * @param $data
	 * @return array
	 */
	private function _validate($data) {
		$errors = [];

		if (!$this->user->hasPermission('modify', 'payment/ms_stripe_connect'))
			$errors[] = $this->language->get('error_permission');

		$environment = !empty($data['test_mode']) ? static::ENVIRONMENT_TEST : static::ENVIRONMENT_LIVE;

		if (empty($data['account_currency']))
			$errors[] = $this->language->get('error_account_currency');


		if (empty($data['charging_approach']))
			$errors[] = $this->language->get('error_charging_approach');

		if (empty($data['environments'][$environment]['public_key']))
			$errors[] = $this->language->get('error_public_key');

		if (empty($data['environments'][$environment]['secret_key']))
			$errors[] = $this->language->get('error_secret_key');

		if (empty($data['environments'][$environment]['client_id']))
			$errors[] = $this->language->get('error_client_id');

		if (!empty($data['enable_subscription']) && (int)$data['enable_subscription'] === 1 && empty($data['environments'][$environment]['product_id']))
			$errors[] = $this->language->get('error_product_id');

		return $errors;
	}
}