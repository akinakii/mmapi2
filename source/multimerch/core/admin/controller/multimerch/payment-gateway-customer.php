<?php

class ControllerMultimerchPaymentGatewayCustomer extends ControllerMultimerchBase {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->model('extension/extension');
		$this->load->model('user/user_group');
		
		$this->data['url_pgw'] = $this->url->link('multimerch/payment-gateway', 'token=' . $this->session->data['token'], 'SSL'); //default tab - payment geteways
		$this->data['url_pgw_marketplace'] = $this->url->link('multimerch/payment-gateway-marketplace', 'token=' . $this->session->data['token'], 'SSL'); //Sellet gateways
		$this->data['url_pgw_customer'] = $this->url->link('multimerch/payment-gateway-customer', 'token=' . $this->session->data['token'], 'SSL');  //OC gateways
	}
	
	public function getList() {
		
		$this->load->model('extension/extension');

		$extensions = $this->model_extension_extension->getInstalled('payment');

		foreach ($extensions as $key => $value) {
			if (!is_file(DIR_APPLICATION . 'controller/extension/payment/' . $value . '.php') && !is_file(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
				$this->model_extension_extension->uninstall('payment', $value);

				unset($extensions[$key]);
			}
		}

		$files = glob(DIR_APPLICATION . 'controller/extension/payment/*.php', GLOB_BRACE);
		
		$total = 0;
		$columns = array();

		if ($files) {
			foreach ($files as $file) {
				$extension = basename($file, '.php');

				$this->load->language('extension/payment/' . $extension);

				$text_link = $this->language->get('text_' . $extension);

				if ($text_link != 'text_' . $extension) {
					$link = $this->language->get('text_' . $extension);
				} else {
					$link = '';
				}
				
				$actions = '';
				
				$ext_installed = in_array($extension, $extensions) ? 1 : 0;
				
				if($ext_installed) {
					$actions .= "<a class='btn btn-primary' href='" . $this->url->link('extension/payment/' . $extension, 'token=' . $this->session->data['token'], true) . "' title='" . $this->language->get('button_edit') . "'><i class='fa fa-pencil''></i></a>";
					$actions .= " <a class='btn btn-danger pg_uninstall' href='" . $this->url->link('multimerch/payment-gateway-customer/uninstall', 'token=' . $this->session->data['token'] . '&extension=' . $extension, true) . "' title='" . $this->language->get('ms_uninstall') . "'><i class='fa fa-minus-circle'></i></a>";
				} else {
					$actions .= "<a class='btn btn-success' href='" . $this->url->link('multimerch/payment-gateway-customer/install', 'token=' . $this->session->data['token'] . '&extension=' . $extension, true) . "'><i class='fa fa-plus''></i></a>";
				}
				

				$columns[] = array(
					'name'       => $this->language->get('heading_title'),
					'logo'		=> $link,
					'status'     => $this->config->get($extension . '_status') ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
					'actions' => $actions,
				);
			}
		}
		
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));

	}

	public function index() {
		if (isset($this->session->data['error_warning'])) {
			$this->data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['heading'] = $this->language->get('ms_pg_heading');
		$this->document->setTitle($this->language->get('ms_menu_payment_gateway_tab_customer'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_pg_heading'),
				'href' => $this->url->link('multimerch/payment-gateway', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('multiseller/payment-gateway-customer.tpl', $this->data));
	}

	public function install() {
		$this->load->language('extension/extension/payment');

		$this->load->model('extension/extension');

		if ($this->validate()) {
			$this->model_extension_extension->install('payment', $this->request->get['extension']);

			$this->load->model('user/user_group');

			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/payment/' . $this->request->get['extension']);
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/payment/' . $this->request->get['extension']);

			// Call install method if it exsits
			$this->load->controller('extension/payment/' . $this->request->get['extension'] . '/install');

			$this->session->data['success'] = sprintf($this->data['ms_pg_install'], $this->request->get['extension']);
		}

		$this->response->redirect($this->url->link('multimerch/payment-gateway-customer', 'token=' . $this->session->data['token'], true));
	}

	public function uninstall() {
		$this->load->language('extension/extension/payment');

		$this->load->model('extension/extension');

		if ($this->validate()) {
			$this->model_extension_extension->uninstall('payment', $this->request->get['extension']);

			// Call uninstall method if it exsits
			$this->load->controller('extension/payment/' . $this->request->get['extension'] . '/uninstall');

			$this->load->language('extension/payment/' . $this->request->get['extension']);

			$this->session->data['success'] = sprintf($this->data['ms_pg_uninstall'], $this->language->get('heading_title'));
		}

		$this->response->redirect($this->url->link('multimerch/payment-gateway-customer', 'token=' . $this->session->data['token'], true));
	}

	private function _validateAccess() {
		if (!$this->user->hasPermission('modify', 'multimerch/payment-gateway-customer')) {
			$this->session->data['error_warning'] = $this->data['ms_pg_modify_error'];
		}

		return isset($this->session->data['error_warning']) ? false : true;
	}
}
?>