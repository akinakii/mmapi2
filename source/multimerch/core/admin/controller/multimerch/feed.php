<?php

class ControllerMultimerchFeed extends ControllerMultimerchBase
{
	private $error = array();

	public function __construct($registry)
	{
		parent::__construct($registry);

		if (!$this->config->get('msconf_import_enable')) {
			$this->response->redirect($this->url->link('module/multimerch', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function getImportsTableData()
	{
		$colMap = [
			'seller' => 'mss.nickname',
			'date' => 'date_added'
		];
		$sorts = ['seller', 'date'];
		$filters = $sorts;
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->ModelProductImport->getImports(
			[
				'is_scheduled' => false
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];
		foreach ($results as $result) {
			$columns[] = array_merge(
				$result,
				[
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['id']}' />",
					'seller' => $result['nickname'] ?: $this->language->get('ms_seller_deleted'),
					'date' => $result['date_added'],
					'processed' => $result['processed'],
					'added' => $result['added'],
					'updated' => $result['updated'],
				]
			);
		}

		$this->response->setOutput(json_encode([
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		]));
	}

	public function getScheduledImportsTableData()
    {
        $colMap = [
            'seller' => 'mss.nickname',
			'date_last_run' => 'msc.date_start',
			'date_next_run' => 'msc.date_next',
        ];
        $sorts = ['seller', 'date_last_run', 'date_next_run'];
        $filters = $sorts;

        list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
        $filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

        $results = $this->MsLoader->ModelProductImport->getImports(
            [
            	'is_scheduled' => true
			],
            [
                'order_by'  => $sortCol,
                'order_way' => $sortDir,
                'filters' => $filterParams,
                'offset' => $this->request->get['iDisplayStart'],
                'limit' => $this->request->get['iDisplayLength']
            ]
        );

        $total = isset($results[0]) ? $results[0]['total_rows'] : 0;

        $columns = [];
        foreach ($results as $result) {
            // actions
            $actions = "";
            $actions .= "<a class='btn btn-danger ms-delete-import' href='" . $this->url->link('multimerch/feed/delete', 'token=' . $this->session->data['token'] . '&import_id=' . $result['id'], 'SSL') . "' title='".$this->language->get('button_delete')."'><i class='fa fa-trash-o'></i></a>";

            $columns[] = array_merge(
                $result,
                [
                    'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['id']}' />",
					'seller' => $result['nickname'] ?: $this->language->get('ms_seller_deleted'),
					'url_path' => $result['url_path'] ?: '-',
					'date_last_run' => $result['date_start'],
					'date_next_run' => $result['date_next'],
					'status' => $result['status'] ? ('<p style="color: green">' . $this->language->get('ms_enabled') . '</p>') : ('<p style="color: red">' . $this->language->get('ms_disabled') . '</p>'),
                    'actions' => $actions
                ]
            );
        }

        $this->response->setOutput(json_encode([
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => $total,
            'aaData' => $columns
        ]));
    }

	public function index()
	{
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_catalog_imports_breadcrumbs'),
				'href' => $this->url->link('multimerch/feed', '', 'SSL'),
			]
		]);

		$this->data['delete'] = $this->url->link('multimerch/feed/delete', 'token=' . $this->session->data['token'], 'SSL');
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['heading'] = $this->language->get('ms_catalog_imports_heading');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->document->setTitle($this->language->get('ms_catalog_imports_heading'));
		
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/import-list.tpl', $this->data));
	}
	
	// Bulk delete of import history
	public function delete()
	{
		if (isset($this->request->get['import_id'])) {
			$this->request->post['selected'] = [$this->request->get['import_id']];
		}

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $import_id) {
				$import = $this->MsLoader->ModelProductImport->getImports(['import_id' => $import_id]);
				$job = $this->MsLoader->ModelCron->get(['import_id' => $import_id]);

				$this->MsLoader->ModelProductImport->deleteImport($import_id);
				$this->MsLoader->ModelProductImport->deleteImportConfig($import['import_config_id']);
				$this->MsLoader->ModelProductImport->deleteHistory($import_id);
				$this->MsLoader->ModelCron->delete($job['id']);
			}

			$this->session->data['success'] = $this->language->get('ms_feed_import_list_success_deleted');
		}

		$this->response->redirect($this->url->link('multimerch/feed', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	// Validate delete of the import history
	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'multimerch/feed')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
      	
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
