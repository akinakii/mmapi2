<?php

class ControllerMultimerchNotification extends ControllerMultimerchBase
{
	public function getTableData()
	{
		$this->load->language('multimerch/notification/onsite');
		$this->load->model('tool/image');

		$colMap = [];

		$sorts = ['date_created'];
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsNotification->getNotifications(
			[
				'channel' => 'onsite',
				'consumer_type' => 'admin',
				'consumer_id' => 0
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];

		foreach ($results as $result) {
			$message_class = $this->MsLoader->MsNotification->getClassAlias('messages', 'onsite');

			switch ($result['producer_type']) {
				case 'admin':
					$image = $this->config->get('config_logo');
					break;

				case 'seller':
					$seller = $this->MsLoader->MsSeller->getSeller($result['producer_id']);
					$image = !empty($seller['ms.avatar']) ? $seller['ms.avatar'] : '';
					break;

				case 'customer':
				default:
					$image = '';
					break;
			}

			switch ($result['object_type']) {
				case 'message':
					$icon = '<i class="fa fa-envelope"></i>';
					break;

				case 'invoice':
				case 'payout':
					$icon = '<i class="fa fa-usd"></i>';
					break;

				case 'order':
					$icon = '<i class="fa fa-shopping-cart"></i>';
					break;

				case 'account':
					$icon = '<i class="fa fa-user"></i>';
					break;

				case 'product':
					$icon = '<i class="fa fa-briefcase"></i>';
					break;

				case 'review':
				default:
					$icon = '<i class="fa fa-star"></i>';
					break;
			}

			$columns[] = array_merge(
				$result,
				array(
					'avatar' => $icon,
					'message' => (new $message_class($this->registry, 'onsite',
						"{$result['producer_type']}.{$result['producer_id']}",
						"{$result['consumer_type']}.{$result['consumer_id']}",
						[
							'type' => $result['object_type'],
							'subtype' => $result['object_subtype'],
							'id' => $result['object_id'],
							'action' => $result['object_action'],
							'metadata' => $result['metadata']
						]
					))->getText(),
					'date_created' => date($this->language->get('datetime_format'), strtotime($result['date_created'])),
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function index() {
		// Reset new notifications counter
		$this->MsLoader->MsNotification->updateNewNotificationsCount('admin', 0, true);

		$this->data['token'] = $this->session->data['token'];
		$this->document->setTitle($this->language->get('ms_notification_heading'));
		$this->document->addStyle('view/stylesheet/multimerch/dashboard.css');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL')
			],
			[
				'text' => $this->language->get('ms_notification_breadcrumbs'),
				'href' => $this->url->link('multimerch/notification', '', 'SSL')
			]
		]);

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('multiseller/notification', $this->data));
	}

	public function jxGetNewNotifications()
	{
		$json = [];

		$json['count'] = $this->MsLoader->MsNotification->getNewNotificationsCount('admin', 0);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
