<?php
class ControllerMultimerchComplaint extends ControllerMultimerchBase {
	public function __construct($registry) {
		parent::__construct($registry);
		if (!$this->config->get('msconf_complaints_enable')) {
			return $this->response->redirect($this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function index() {
		$this->document->addScript('view/javascript/multimerch/complaint.js');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading'] = $this->language->get('ms_menu_complaints');
		$this->document->setTitle($this->language->get('ms_menu_complaints'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_menu_complaints'),
				'href' => $this->url->link('multimerch/complaint', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('multiseller/complaint', $this->data));
	}

	public function getTableData() {

		$sorts = array('date_added', 'product_name', 'seller_name', 'user_name');

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, FALSE);

		$results = $this->MsLoader->MsComplaint->getComplaints(
			array(
				'order_by' => $sortCol,
				'order_way' => $sortDir,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();

		foreach ($results as $result) {

			$actions = "";
			$actions .= "<a class='btn btn-danger ms-delete-complaint' title='" . $this->language->get('ms_delete') . "'  data-id='" . $result['complaint_id'] . "'><i class='fa fa-trash-o''></i></a>";

			$columns[] = array_merge(
				$result, array(
				'product_name' => '<a href="' . $this->url->link('catalog/product/edit', 'product_id=' . $result['product_id']) . '&token=' . $this->session->data['token'] . '" target="_blank">' . $result['product_name'] . '</a>',
				'seller_name' => '<a href="' . $this->url->link('multimerch/seller/update', 'seller_id=' . $result['seller_id']) . '&token=' . $this->session->data['token'] . '" target="_blank">' . $result['seller_name'] . '</a>',
				'comment' => $result['comment'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function jxDeleteComplaint() {

		$json = array();

		if (empty($this->request->get['complaint_id']))
				$json['errors'] = $this->language->get('ms_error_complaint_id');

		if (!empty($json['errors'])) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			return;
		}

		$this->MsLoader->MsComplaint->deleteComplaint($this->request->get['complaint_id']);
		$this->session->data['success'] = 'Success!';
		$json['redirect'] = $this->url->link('multimerch/complaint', 'token=' . $this->session->data['token'], true);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}