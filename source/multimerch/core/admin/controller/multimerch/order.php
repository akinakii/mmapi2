<?php

class ControllerMultimerchOrder extends ControllerMultimerchBase
{
	public function getOrderTableData() {
		$colMap = array(
			'customer' => "CONCAT(o.firstname, ' ', o.lastname)",
			'order_status' => "(SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "')",
			'total' => "o.total"
		);

		$sorts = array('order_id', 'order_status', 'customer', 'date_added', 'date_modified', 'total');
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsSuborder->getOrders(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]['total_rows']) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// actions
			$actions = "";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'], 'SSL') . "' title='".$this->language->get('button_view')."'><i class='fa fa-eye''></i></a>";

			$suborders = '<table class="table sub-table">';
			foreach ($result['suborders'] as $suborder){
				$suborders .= '<tr>';
				$suborders .= ($this->MsLoader->MsSeller->isCustomerSeller($suborder['seller_id']) ? ('	<td><a target="_blank" href="' . $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $suborder['seller_id'], 'SSL') .'">' . $suborder['nickname'] . '</a></td>') : ('	<td>' . $this->language->get('ms_seller_deleted') . '</td>'));
				$suborders .= '	<td>' . $suborder['status_name'] . '</td>';
				$suborders .= '</tr>';
			}
			$suborders .= '</table>';

			$columns[] = array_merge(
				$result,
				array(
					'customer' => '<a target="_blank" href="' . $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL') .'">' . $result['firstname'] . ' ' . $result['lastname'] . '</a>',
					'order_status' => $result['order_status'],
					'suborders' => $suborders,
					'total' =>  $this->currency->format($result['total']),
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function getSubOrderTableData() {
		$colMap = array(
			'order_id' => "o.order_id",
			'customer' => "CONCAT(o.firstname, ' ', o.lastname)",
			'seller' => "COALESCE((SELECT nickname FROM " . DB_PREFIX . "ms_seller s WHERE s.seller_id = mso.seller_id), '" . $this->language->get('ms_seller_deleted') . "')",
			'status' => "(SELECT name FROM " . DB_PREFIX . "ms_suborder_status_description msosd WHERE msosd.ms_suborder_status_id = mso.order_status_id AND msosd.language_id = '" . (int)$this->config->get('config_language_id') . "')"
		);

		$sorts = array(
			'order_id', 'seller', 'customer','status'
		);
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsSuborder->getSuborders(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]['total_rows']) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// actions
			$actions = "";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . '#suborder_' . $result['suborder_id'], 'SSL') . "' title='".$this->language->get('button_view')."'><i class='fa fa-eye''></i></a>";

			$suborder_totals = $this->MsLoader->MsSuborder->getSuborderTotals($result['order_id'], $result['seller_id']);

			$columns[] = array_merge(
				$result,
				array(
					'suborder_id' => $result['order_id'] . '-' .$result['suborder_id'],
					'customer' => '<a target="_blank" href="' . $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL') .'">' . $result['firstname'] . ' ' . $result['lastname'] . '</a>',
					'seller' => $this->MsLoader->MsSeller->isCustomerSeller($result['seller_id']) ? ('<a target="_blank" href="' . $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL') .'">' . $result['seller'] . '</a>') : $this->language->get('ms_seller_deleted'),
					'total' =>  !empty($suborder_totals['total']['text']) ? $suborder_totals['total']['text'] : $this->currency->format(0, $result['currency_code']),
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function index() {
		$this->validate(__FUNCTION__);

		$this->data['token'] = $this->session->data['token'];
		$this->document->setTitle($this->language->get('ms_order_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL')
			),
			array(
				'text' => $this->language->get('ms_order_breadcrumbs'),
				'href' => $this->url->link('multimerch/order', '', 'SSL')
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/order.tpl', $this->data));
	}
	
	public function printPackingSlip(){
		$order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : 0;
		$suborder_id = isset($this->request->get['suborder_id']) ? $this->request->get['suborder_id'] : 0;
		
		if (!$order_id || !$suborder_id){
			return new Action('error/not_found');
		}
		
		$this->MsLoader->MsPrinter->printOrderPackingSlip($order_id, $suborder_id);
	}
	
	public function printInvoice(){
		$order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : 0;
		
		if (!$order_id){
			return new Action('error/not_found');
		}
		
		$this->MsLoader->MsPrinter->printOrderInvoice($order_id);
	}

	public function info()
	{
		$this->document->addStyle('view/stylesheet/multimerch/multiseller.css');
		$this->document->addStyle('view/stylesheet/multimerch/dashboard.css');

		$this->load->model('sale/order');
		$this->load->model('tool/image');

		$order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : 0;

		$data = array_merge($this->load->language('multiseller/multiseller'), $this->load->language('sale/order'));

		$order_info = $this->model_sale_order->getOrder($order_id);

		if (!empty($order_info)) {
			$this->load->model('sale/order');
			$this->load->model('tool/upload');
			$this->load->model('localisation/order_status');

			$this->document->setTitle($data['heading_title']);

			$data['token'] = $this->session->data['token'];
			$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
			$data['order_id'] = $order_id;
			$data['order'] = $order_info;
			$data['store_id'] = $order_info['store_id'];

			// Order products
			$products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);

			// Whether per-product shipping is applied
			$mm_shipping_flag = false;

			// Products that are shipped via combined shipping method
			$cs_products = [];

			foreach ($products as &$product) {
				// Product MSF variation
				$msf_variation_data = [];

				$msf_variations = $this->MsLoader->MsOrderData->getOrderProductMsfVariations($order_id, $product['order_product_id']);
				foreach ($msf_variations as $msf_variation) {
					$msf_variation_data[] = [
						'name' => $msf_variation['name'],
						'value' => $msf_variation['value']
					];
				}

				// Product option
				$option_data = [];

				$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
				foreach ($options as $option) {
					$o = [
						'name'  => $option['name'],
						'value' => $option['value'],
						'type'  => $option['type']
					];

					if ('file' === (string)$option['type'] && $upload_info = $this->model_tool_upload->getUploadByCode($option['value'])) {
						$o['value'] = $upload_info['name'];
						$o['href'] = $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true);
					}

					$option_data[] = $o;
				}

				// Product image
				$thumb = $this->MsLoader->MsProduct->getProductThumbnail($product['product_id']);

				// Get product shipping cost
				$product_shipping_cost = (float)$product['shipping_cost'];

				if ($product_shipping_cost > 0) {
					$mm_shipping_flag = true;
				} else {
					if (!isset($cs_products[$product['seller_id']])) {
						$cs_products[$product['seller_id']] = 0;
					}
					$cs_products[$product['seller_id']] += 1;
				}

				$product['image'] = $this->model_tool_image->resize(!empty($thumb['image']) ? $thumb['image'] : 'no_image.png', 40, 40);
				$product['msf_variation'] = $msf_variation_data;
				$product['option'] = $option_data;

				$product['price'] += ($this->config->get('config_tax') ? $product['tax'] : 0);
				$product['price_formatted'] = $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']);

				$product['shipping_formatted'] = $this->currency->format($product_shipping_cost, $order_info['currency_code'], $order_info['currency_value']);

				$product['total'] += ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0) + $product_shipping_cost;
				$product['total_formatted'] = $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']);

				$product['href'] = $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], true);
				$product['seller_nickname'] = $this->MsLoader->MsSeller->getSellerNickname($product['seller_id']) ?: $this->language->get('ms_seller_deleted');
			}

			// Product is shipped using combined shipping method
			foreach ($products as &$product) {
				$seller_id = (int)$product['seller_id'];

				// Suborder shipping cost
				$suborder = $this->MsLoader->MsSuborder->getSuborders([
					'seller_id' => $seller_id,
					'order_id' => $order_id,
					'single' => true
				]);

				if (!empty($suborder)) {
					$suborder_shipping = $this->MsLoader->MsShipping->getSuborderShippingData($suborder['suborder_id']);

					if (!empty($suborder_shipping) && !$product['shipping_cost'] && isset($cs_products[$seller_id]) && $cs_products[$seller_id] > 0) {
						$mm_shipping_flag = true;

						$shipping_cost = $suborder_shipping['cost'] / $cs_products[$seller_id];

						$product['shipping_cost'] = $shipping_cost;
						$product['shipping_formatted'] = $this->currency->format($shipping_cost, $order_info['currency_code'], $order_info['currency_value']);
						$product['shipping_method'] = !empty($suborder_shipping['shipping_method_name']) ? " <i>(" . sprintf($this->language->get('ms_sale_order_shipping_via'), $suborder_shipping['shipping_method_name']) . ")</i>" : "";

						$product['total'] += $shipping_cost;
						$product['total_formatted'] = $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']);
					}
				}
			}

			$data['products'] = $products;
			$data['mm_shipping_flag'] = $mm_shipping_flag;

			// Group products by seller for future use
			$products_grouped_by_seller = [];
			foreach ($products as &$product) {
				if ($product['seller_id']) {
					$products_grouped_by_seller[(int)$product['seller_id']][] = $product;
				}
			}

			// Order totals
			$data['order']['totals'] = [];

			foreach ($this->model_sale_order->getOrderTotals($order_id) as $total) {
				$data['order']['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Customer info
			$this->load->model('customer/customer_group');
			$customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

			$data['customer'] = [
				'id' => $order_info['customer_id'],
				'firstname' => $order_info['firstname'],
				'lastname' => $order_info['lastname'],
				'email' => $order_info['email'],
				'telephone' => $order_info['telephone'],
				'customer_group' => $customer_group_info ? $customer_group_info['name'] : null,
				'href' => $order_info['customer_id'] ? $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], true) : null,
			];

			// Payment info
			$data['payment'] = [
				'method' => $order_info['payment_method'],
				'status' => $order_info['order_status'],
				'address' => $this->formatAddress('payment', $order_info)
			];

			// Shipping info
			$data['shipping'] = [
				'method' => $order_info['shipping_method'],
				'address' => $this->formatAddress('shipping', $order_info)
			];

			// Miscellaneous info: Vouchers
			$data['vouchers'] = [];

			$vouchers = $this->model_sale_order->getOrderVouchers($order_id);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					'href'        => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], true)
				);
			}

			// Suborders
			$suborders = $this->MsLoader->MsSuborder->getSuborders(['order_id' => $order_id]);

			foreach ($suborders as &$suborder) {
				$seller_id = $suborder['seller_id'];

				// Products
				$suborder['products'] = !empty($products_grouped_by_seller[$seller_id]) ? $products_grouped_by_seller[$seller_id] : [];

				// Statuses history
				$suborder['status_history'] = [];

				$suborder_status_history = $this->MsLoader->MsSuborder->getSuborderHistory(['suborder_id' => $suborder['suborder_id']]);
				foreach ($suborder_status_history as $h) {
					$suborder['status_history'][] = [
						'date_added' => date($this->language->get('date_format_short'), strtotime($h['date_added'])),
						'status'     => $this->MsLoader->MsSuborderStatus->getSubStatusName(['order_status_id' => $h['order_status_id']]),
						'comment'    => $h['comment']
					];
				}

				// Transactions
				$suborder['transactions'] = $this->MsLoader->MsBalance->getBalanceEntries(['seller_id' => $seller_id, 'order_id' => $order_id]);
				
				$suborder['link_print_packing_slip'] =  $this->url->link('multimerch/order/printPackingSlip', 'token=' . $this->session->data['token'] . '&order_id=' . $order_id .'&suborder_id=' . $suborder['suborder_id'], true);
			}

			$data['link_print_invoice'] =  $this->url->link('multimerch/order/printInvoice', 'token=' . $this->session->data['token'] . '&order_id=' . $order_id, true);
			
			$data['suborders'] = $suborders;

			// Suborder statuses
			$data['suborder_statuses'] = $this->MsLoader->MsSuborderStatus->getMsSuborderStatuses(['language_id' => $this->config->get('config_language_id')]);

			// Breadcrumbs
			$data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
				[
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('multimerch/order', 'token=' . $this->session->data['token'], true)
				]
			]);

			// The URL we send API requests to
			$data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

			// API login
			$this->load->model('user/api');

			$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

			if ($api_info) {
				$data['api_id'] = $api_info['api_id'];
				$data['api_key'] = $api_info['key'];
				$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
			} else {
				$data['api_id'] = '';
				$data['api_key'] = '';
				$data['api_ip'] = '';
			}

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('multiseller/order-info', $data));
		} else {
			return new Action('error/not_found');
		}
	}

	protected function formatAddress($type, $order_info)
	{
		return $this->MsLoader->MsHelper->formatAddressByOrderInfo($type, $order_info);
	}

	public function paymentHistory()
	{
		$data = array_merge($this->load->language('sale/order'), $this->load->language('multiseller/multiseller'));

		$data['histories'] = [];

		$this->load->model('sale/order');

		$results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], 0, 99);

		foreach ($results as $result) {
			$data['histories'][] = [
				'notify'     => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
				'status'     => $result['status'],
				'comment'    => nl2br($result['comment']),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			];
		}

		$this->response->setOutput($this->load->view('multiseller/order-history', $data));
	}

	public function jxChangeSuborderStatus()
	{
		$data = $this->request->post;

		if (empty($data['suborder_id']) || empty($data['suborder_status_id'])) {
			$this->ms_logger->error('Invalid data: ' . print_r($data, true));
			return $this->response->setOutput(json_encode(['error' => 'Something went wrong']));
		}

		$this->MsLoader->MsSuborder->updateSuborderStatus([
			'suborder_id' => $data['suborder_id'],
			'order_status_id' => $data['suborder_status_id']
		]);

		$this->MsLoader->MsSuborder->addSuborderHistory([
			'suborder_id' => $data['suborder_id'],
			'comment' => !empty($data['comment']) ? $data['comment'] : '',
			'order_status_id' => $data['suborder_status_id']
		]);

		if (!empty($data['order_id']) && !empty($data['seller_id'])) {
			$this->MsLoader->MsTransaction->processSuborderTransactions($data['order_id'], $data['seller_id'], $data['suborder_status_id']);

			if (!empty($data['notify'])) {
				$order = $this->MsLoader->MsOrderData->ocGetOrder($data['order_id']);

				$this->MsHooks->triggerAction('admin_updated_suborder_status', [
					'producer' => "admin.0",
					'consumers' => ["seller.{$data['seller_id']}", "customer.{$order['customer_id']}"],
					'object' => [
						'type' => 'suborder',
						'subtype' => 'status',
						'id' => $data['order_id'],
						'action' => 'updated',
						'metadata' => ['seller_id' => $data['seller_id'], 'suborder_id' => $data['suborder_id'], 'suborder_status_id' => $data['suborder_status_id'], 'comment' => !empty($data['comment']) ? $data['comment'] : '']
					]
				]);
			}
		}

		$this->response->setOutput(json_encode(['success' => true]));
	}

	public function suborderConversation()
	{
		$data = $this->load->language('multiseller/multiseller');

		$order_id = isset($this->request->get['order_id']) ? $this->request->get['order_id'] : false;
		$suborder_id = isset($this->request->get['suborder_id']) ? $this->request->get['suborder_id'] : false;

		if (!$order_id || !$suborder_id) {
			return false;
		}

		$data['order_id'] = $order_id;
		$data['suborder_id'] = $suborder_id;

		$conversation = $this->MsLoader->MsConversation->getConversations([
			'order_id' => $order_id,
			'suborder_id' => $suborder_id,
			'single' => true
		]);

		$data['conversation'] = $conversation;

		$data['messages'] = [];

		$messages = $conversation ? $this->MsLoader->MsMessage->getMessages([
			'conversation_id' => $conversation['conversation_id']
		], [
			'order_by'  => 'date_created',
			'order_way' => 'ASC',
		]) : [];

		foreach ($messages as $m) {
			$sender_type_id = $m['from_admin'] ? MsConversation::SENDER_TYPE_ADMIN : ($m['seller_sender'] ? MsConversation::SENDER_TYPE_SELLER : MsConversation::SENDER_TYPE_CUSTOMER);
			$sender = $m['from_admin'] ? $m['user_sender'] : ($m['seller_sender'] ? $m['seller_sender'] : $m['customer_sender']);

			$data['messages'][] = array_merge(
				$m,
				[
					'date_created' => date($this->language->get('datetime_format'), strtotime($m['date_created'])),
					'sender_type_id' => $sender_type_id,
					'sender' => $sender
				]
			);
		}

		$this->data['send_url'] = $this->url->link('multimerch/conversation/jxSendMessage', 'token=' . $this->session->data['token'], 'SSL');

		return $this->response->setOutput($this->load->view('multiseller/suborder-conversation', $data));
	}
}
