<?php
class ControllerMultimerchInformation extends ControllerMultimerchBase {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->data['error'] = false;
	}

	public function index() {

		$this->load->language('catalog/information');

		$this->document->setTitle($this->language->get('ms_information_heading'));

		$this->load->model('catalog/information');



		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/information');

		$this->document->setTitle($this->language->get('ms_information_heading'));

		$this->load->model('catalog/information');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_information->addInformation($this->request->post);

			$this->session->data['success'] = $this->language->get('ms_information_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/information');

		$this->document->setTitle($this->language->get('ms_information_heading'));

		$this->load->model('catalog/information');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_information->editInformation($this->request->get['information_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('ms_information_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/information');

		$this->document->setTitle($this->language->get('ms_information_heading'));

		$this->load->model('catalog/information');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $information_id) {
				$this->model_catalog_information->deleteInformation($information_id);
			}

			$this->session->data['success'] = $this->language->get('ms_information_success_deleted');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['add'] = $this->url->link('multimerch/information/add', 'token=' . $this->session->data['token'] . $url, true);
		$this->data['delete'] = $this->url->link('multimerch/information/delete', 'token=' . $this->session->data['token'] . $url, true);

		$this->data['informations'] = array();

		$filter_data = array(
			'sort' => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$information_total = $this->model_catalog_information->getTotalInformations();

		$data = $this->model_catalog_information->getInformations($filter_data);

		foreach ($data as $result) {
			$this->data['informations'][] = array(
				'information_id' => $result['information_id'],
				'title' => $result['title'],
				'sort_order' => $result['sort_order'],
				'edit' => $this->url->link('multimerch/information/edit', 'token=' . $this->session->data['token'] . '&information_id=' . $result['information_id'] . $url, true)
			);
		}
		if (isset($this->data['error']['warning'])) {
			$this->data['error_warning'] = $this->data['error']['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$this->data['selected'] = (array) $this->request->post['selected'];
		} else {
			$this->data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		//$this->data['sort_title'] = $this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, true);
		$this->data['sort_sort_order'] = $this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $information_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($information_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($information_total - $this->config->get('config_limit_admin'))) ? $information_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $information_total, ceil($information_total / $this->config->get('config_limit_admin')));

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_menu_information'),
				'href' => $this->url->link('multimerch/information', '', 'SSL'),
			)
		));

		$this->response->setOutput($this->load->view('multiseller/information', $this->data));
	}

	protected function getForm() {
		$this->data['text_form'] = !isset($this->request->get['information_id']) ? $this->language->get('ms_information_add') : $this->language->get('ms_information_edit');

		if (isset($this->data['error']['warning'])) {
			$this->data['error_warning'] = $this->data['error']['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->data['error']['title'])) {
			$this->data['error_title'] = $this->data['error']['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->data['error']['description'])) {

			$this->data['error_description'] = $this->data['error']['description'];
		} else {
			$this->data['error_description'] = array();
		}

		if (isset($this->data['error']['meta_title'])) {
			$this->data['error_meta_title'] = $this->data['error']['meta_title'];
		} else {
			$this->data['error_meta_title'] = array();
		}

		if (isset($this->data['error']['keyword'])) {
			$this->data['error_keyword'] = $this->data['error']['keyword'];
		} else {
			$this->data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_menu_information'),
				'href' => $this->url->link('multimerch/information', '', 'SSL'),
			)
		));

		if (!isset($this->request->get['information_id'])) {
			$this->data['action'] = $this->url->link('multimerch/information/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$this->data['action'] = $this->url->link('multimerch/information/edit', 'token=' . $this->session->data['token'] . '&information_id=' . $this->request->get['information_id'] . $url, true);
		}

		$this->data['cancel'] = $this->url->link('multimerch/information', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['information_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$information_info = $this->model_catalog_information->getInformation($this->request->get['information_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['information_description'])) {
			$this->data['information_description'] = $this->request->post['information_description'];
		} elseif (isset($this->request->get['information_id'])) {
			$this->data['information_description'] = $this->model_catalog_information->getInformationDescriptions($this->request->get['information_id']);
		} else {
			$this->data['information_description'] = array();
		}

		$this->load->model('setting/store');

		$this->data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['information_store'])) {
			$this->data['information_store'] = $this->request->post['information_store'];
		} elseif (isset($this->request->get['information_id'])) {
			$this->data['information_store'] = $this->model_catalog_information->getInformationStores($this->request->get['information_id']);
		} else {
			$this->data['information_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($information_info)) {
			$this->data['keyword'] = $information_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['bottom'])) {
			$this->data['bottom'] = $this->request->post['bottom'];
		} elseif (!empty($information_info)) {
			$this->data['bottom'] = $information_info['bottom'];
		} else {
			$this->data['bottom'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($information_info)) {
			$this->data['status'] = $information_info['status'];
		} else {
			$this->data['status'] = true;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($information_info)) {
			$this->data['sort_order'] = $information_info['sort_order'];
		} else {
			$this->data['sort_order'] = '';
		}

		if (isset($this->request->post['information_layout'])) {
			$this->data['information_layout'] = $this->request->post['information_layout'];
		} elseif (isset($this->request->get['information_id'])) {
			$this->data['information_layout'] = $this->model_catalog_information->getInformationLayouts($this->request->get['information_id']);
		} else {
			$this->data['information_layout'] = array();
		}

		$this->load->model('design/layout');

		//$data['layouts'] = $this->model_design_layout->getLayouts();

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('multiseller/information-form', $this->data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'multimerch/information')) {
			$this->data['error']['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['information_description'] as $language_id => $value) {
			if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
				$this->data['error']['title'][$language_id] = $this->language->get('ms_information_error_title');
			}

			if (utf8_strlen($value['description']) < 3) {
				$this->data['error']['description'][$language_id] = $this->language->get('ms_information_error_content');
			}

			if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
				$this->data['error']['meta_title'][$language_id] = $this->language->get('ms_information_error_meta_title');
			}
		}

		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['information_id']) && $url_alias_info['query'] != 'information_id=' . $this->request->get['information_id']) {
				$this->data['error']['keyword'] = sprintf($this->language->get('ms_oc_category_error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['information_id'])) {
				$this->data['error']['keyword'] = sprintf($this->language->get('ms_oc_category_error_keyword'));
			}
		}

		if ($this->data['error'] && !isset($this->data['error']['warning'])) {
			$this->data['error']['warning'] = $this->language->get('error_warning');
		}

		return !$this->data['error'];
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'multimerch/information')) {
			$this->data['error']['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->data['error']['warning'] = $this->language->get('ms_information_error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->data['error']['warning'] = $this->language->get('ms_information_error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->data['error']['warning'] = $this->language->get('ms_information_error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->data['error']['warning'] = $this->language->get('ms_information_error_return');
			}


			$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->data['error']['warning'] = sprintf($this->language->get('ms_information_error_store'), $store_total);
			}
		}

		return !$this->data['error'];
	}

}