<?php

class ControllerMultimerchInvoice extends ControllerMultimerchBase {
	public function getTableData() {
		$colMap = [];

		$sorts = array('invoice_id', 'type', 'total', 'title', 'status', 'date_generated', 'payment_id');
		$filters = array_diff($sorts, array('type', 'total', 'title', 'status'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$invoices = $this->MsLoader->MsInvoice->get(
			[
				'types' => ['signup', 'listing']
			],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = count($invoices);

		$this->load->model('customer/customer');

		$columns = [];
		foreach ($invoices as $invoice) {
			// Recipient
			$recipient = '-';
			if ('seller' === $invoice->getRecipientType()) {
				$seller_nickname = $this->MsLoader->MsSeller->getSellerNickname($invoice->getRecipientId());
				$recipient = "<a class='seller' href='{$this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $invoice->getRecipientId(), 'SSL')}'>{$seller_nickname}</a>";
			} elseif ('customer' === $invoice->getRecipientType()) {
				$this->load->model('customer/customer');
				$customer = $this->model_customer_customer->getCustomer($invoice->getRecipientId());
				$recipient = "<a class='customer' href='{$this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $invoice->getRecipientId(), 'SSL')}'>" . $customer['firstname'] . ' ' . $customer['lastname'] . "</a>";
			} elseif ('platform' === $invoice->getRecipientType()) {
				$recipient = $this->config->get('config_name');
			}

			$payment_info = '-';

			// If invoice is paid
			if ($invoice->getPaymentId()) {
				$payment = $this->MsLoader->MsPgPayment->getPayments(['payment_id' => $invoice->getPaymentId(), 'single' => true]);

				if (!empty($payment)) {
					$pg_name = str_replace('ms_pg_', '', $payment['payment_code']);
					$this->load->language('multimerch/payment/' . $pg_name);

					// Show payment details
					$html  = '<p>Method: ' . $this->language->get('text_method_name') . '</p>';
					$html .= '<p>Date paid: ' . date($this->language->get('date_format_short'), strtotime($payment['date_created'])) . '</p>';
					$html .= '<p>Status: ' . $this->language->get('ms_pg_payment_status_no_color_' . $payment['payment_status']) . '</p>';

					$payment_info = '<a class="paymentInfoControl" href="#" data-toggle="tooltip" data-html="true" title="' . $html . '" onclick="return false;">' . (int)$invoice->getPaymentId() . '</a>';
				}
			}

			$columns[] = [
				'invoice_id' => $invoice->getInvoiceId(),
				'type' => $this->language->get('ms_invoice_type_' . $invoice->getType()),
				'recipient' => $recipient,
				'total' => $this->currency->format(abs($invoice->getTotal()), $invoice->getCurrencyCode()),
				'title' => $invoice->getTitle(),
				'date_generated' => date($this->language->get('date_format_short'), strtotime($invoice->getDateGenerated())),
				'status' => $this->language->get('ms_invoice_status_' . $invoice->getStatus()),
				'payment_id' => $payment_info
			];
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function jxCreate() {
		$json = array();

		$data = $this->request->post;

		if(empty($data['sellers'])) {
			$json['errors'][] = $this->language->get('ms_invoice_error_not_selected');
		}

		if(empty($data['date_payout_period'])) {
			$json['errors'][] = $this->language->get('ms_invoice_error_date_period');
		}

		if(empty($json['errors'])) {
			$sellers = isset($this->request->post['sellers']) ? $this->request->post['sellers'] : array();
			$invoice_type = isset($this->request->post['type']) ? $this->request->post['type'] : 'payout';

			$created_invoices_ids = array();

			if($invoice_type) {
				switch($invoice_type) {
					case 'payout':
						foreach ($sellers as $seller_id => $amount) {
							$seller_name = $this->MsLoader->MsSeller->getSellerNickname($seller_id) ?: $this->MsLoader->MsSeller->getSellerFullName($seller_id);

							$invoice_manager = new \MultiMerch\Core\Invoice\Manager($this->MsLoader->getRegistry());
							$invoice_id = $invoice_manager->createSellerPayoutInvoice($seller_id, $amount);

							if($invoice_id) {
								array_push($created_invoices_ids, $invoice_id);
							} else {
								$json['errors'][] = sprintf($this->language->get('ms_invoice_error_not_created'), $seller_name);
							}
						}

						if(!empty($created_invoices_ids)) {
							$payout_id = $this->MsLoader->MsPayout->createPayout(array(
								'date_payout_period' => date("Y-m-d", strtotime($data['date_payout_period'])) . ' ' . date("H:i:s"),
								'invoice_ids' => $created_invoices_ids
							));

							$this->session->data['success'] = $json['success'] = sprintf($this->language->get('ms_payout_success_payout_created'), $payout_id);
						}

						break;

					default:
						$json['errors'][] = $this->language->get('ms_invoice_error_type');
						break;
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxDelete() {
		$json = array();

		if(!isset($this->request->post['selected'])) {
			$json['errors'][] = $this->language->get('ms_invoice_error_not_selected');
		}

		if(!isset($json['errors'])) {
			$invoice_ids = $this->request->post['selected'];

			foreach ($invoice_ids as $invoice_id) {
				$this->MsLoader->MsInvoice->delete($invoice_id);
			}

			$this->session->data['success'] = $this->language->get('ms_invoice_success_deleted');
			$json['redirect'] = $this->url->link('multimerch/invoice', 'token=' . $this->session->data['token'], true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function index() {
		$this->document->addScript('view/javascript/multimerch/invoice.js');

		if (isset($this->session->data['error_warning'])) {
			$this->data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('multimerch/payment/create', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];
		$this->data['heading'] = $this->language->get('ms_invoice_heading');
		$this->document->setTitle($this->language->get('ms_invoice_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_invoice_breadcrumbs'),
				'href' => $this->url->link('multimerch/invoice', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('multiseller/invoice', $this->data));
	}
}