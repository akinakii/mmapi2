<?php

class ControllerMultiMerchFieldAttribute extends ControllerMultimerchBase
{
	public function index()
	{
		$this->document->addScript('view/javascript/multimerch/field/attribute.js');

		// Warning or error message
		$this->data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : null;

		// Success message
		$this->data['success'] = null;
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$statuses = [];
		$msfAttribute = new ReflectionClass('MsfAttribute');
		foreach ($msfAttribute->getConstants() as $cname => $cval) {
			if (false !== strpos($cname, 'STATUS_')) {
				$statuses[] = $cval;
			}
		}

		$this->data['statuses'] = $statuses;

		$this->data['msf_attributes'] = $this->MsLoader->MsfAttribute->getList();

		$this->data['heading'] = $this->language->get('ms_field_attribute_heading');
		$this->document->setTitle($this->language->get('ms_field_attribute_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_field_attribute_breadcrumbs'),
				'href' => $this->url->link('multimerch/field/attribute', '', 'SSL'),
			]
		]);

		list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('multimerch/field/attribute');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxGetList()
	{
		$colMap = [];

		$sorts = ['name', 'type', 'status'];
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$statuses = [];
		$msfAttribute = new ReflectionClass('MsfAttribute');
		foreach ($msfAttribute->getConstants() as $cname => $cval) {
			if (false !== strpos($cname, 'STATUS_')) {
				$statuses[] = $cval;
			}
		}

		$results = $this->MsLoader->MsfAttribute->getList(['values' => true], [
			'order_by'  => $sortCol,
			'order_way' => $sortDir,
			'filters' => $filterParams,
			'offset' => $this->request->get['iDisplayStart'],
			'limit' => $this->request->get['iDisplayLength']
		]);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];
		foreach ($results as $result) {
			// status
			$status = "";
			if(isset($result['status'])) {
				$status .= "<p style='color: ";

				if ($result['status'] == MsfAttribute::STATUS_INACTIVE) $status .= "red";
				if ($result['status'] == MsfAttribute::STATUS_ACTIVE) $status .= "green";

				$status .= "'>" . $this->language->get('ms_field_attribute_status_' . $result['status']) . "</p>";
			}

			// values
			$values = [];
			foreach ($result['values'] as $value) {
				$values[] = $value['name'];
			}

			// categories
			$categories = [];
			foreach ($this->MsLoader->MsfAttribute->getOcCategoriesByMsfAttributeId($result['id']) as $category_id) {
				$categories[] = $this->MsLoader->MsCategory->getOcCategoryName($category_id, $this->config->get('config_language_id'));
			}

			// actions
			$actions = "";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/field/attribute/update', 'token=' . $this->session->data['token'] . '&msf_attribute_id=' . $result['id'], 'SSL') . "' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil''></i></a>";
			$actions .= "<button type='button' class='btn btn-danger ms-delete' title='".$this->language->get('button_delete')."' data-id='" . $result['id'] . "' data-referrer='msf_attribute'><i class='fa fa-trash-o''></i></button>";

			$columns[] = array_merge(
				$result,
				[
					'name' => $result['full_name'],
					'type' => ucfirst($result['type']),
					'values' => implode(', ', $values),
					'categories' => implode(', ', $categories),
					'status' => $status,
					'actions' => $actions
				]
			);
		}

		$this->response->setOutput(json_encode([
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		]));
	}

	public function create()
	{
		$this->initForm();

		$this->data['msf_attribute'] = false;
		$this->data['heading'] = $this->language->get('ms_field_attribute_heading_new');
		$this->document->setTitle($this->language->get('ms_field_attribute_heading_new'));

		list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('multimerch/field/attribute-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function update()
	{
		$this->initForm();

		$msf_attribute_id = isset($this->request->get['msf_attribute_id']) ? (int)$this->request->get['msf_attribute_id'] : 0;
		$msf_attribute = $this->MsLoader->MsfAttribute->get($msf_attribute_id);

		if (empty($msf_attribute)) {
			return $this->response->redirect($this->url->link('multimerch/field/attribute', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['msf_attribute'] = $msf_attribute;
		$this->data['heading'] = $this->language->get('ms_field_attribute_heading_update');
		$this->document->setTitle($this->language->get('ms_field_attribute_heading_update'));

		list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('multimerch/field/attribute-form');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxSave()
	{
		$data = $this->request->post;

		// Unset values if type is `text`
		if (isset($data['type']) && in_array($data['type'], ['text'])) {
			$data['values'] = [];
		}

		$errors = $this->validatePost($data);

		if (!empty($errors)) {
			$this->response->addHeader('Content-Type: application/json');
			return $this->response->setOutput(json_encode([
				'success' => false,
				'errors' => $errors
			]));
		}

		if (!empty($data['msf_attribute_id'])) {
			$this->MsLoader->MsfAttribute->update($data['msf_attribute_id'], $data);
			$this->session->data['success'] = $this->language->get('ms_field_attribute_success_updated');
		} else {
			$this->MsLoader->MsfAttribute->save($data);
			$this->session->data['success'] = $this->language->get('ms_field_attribute_success_created');
		}

		$json['redirect'] = $this->url->link('multimerch/field/attribute', 'token=' . $this->session->data['token'], true);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxDelete()
	{
		$json = array();

		if (!isset($this->request->get['msf_attribute_id']) && !isset($this->request->post['selected'])) {
			$json['error'] = $this->language->get('ms_field_attribute_error_not_selected');
		}

		if (!isset($json['error'])) {
			$msf_attribute_ids = isset($this->request->get['msf_attribute_id']) ?
				[$this->request->get['msf_attribute_id']] :
				(isset($this->request->post['selected']) ? $this->request->post['selected'] : []);

			foreach ($msf_attribute_ids as $msf_attribute_id) {
				$this->MsLoader->MsfAttribute->delete($msf_attribute_id);
			}

			$this->session->data['success'] = $this->language->get('ms_field_attribute_success_deleted');
			$json['redirect'] = $this->url->link('multimerch/field/attribute', 'token=' . $this->session->data['token'], true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxAutocompleteMsfAttributes() {
		$json = [];

		if (isset($this->request->get['filter_name'])) {
			$added_ids = !empty($this->request->get['added_ids']) ? explode(',', $this->request->get['added_ids']): [];

			$results = $this->MsLoader->MsfAttribute->getList(['not_in_ids' => $added_ids], [
				'order_by'  => 'label',
				'order_way' => 'ASC',
				'filters' => ['name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))],
				'offset' => 0,
				'limit' => 10
			]);

			foreach ($results as $result) {
				$json[] = [
					'msf_attribute_id'  => $result['id'],
					'label'        		=> strip_tags(html_entity_decode($result['full_name'], ENT_QUOTES, 'UTF-8'))
				];
			}
		}

		$sort_order = [];

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['label'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function initForm()
	{
		$this->document->addScript('view/javascript/multimerch/field/attribute-form.js');
		$this->document->addScript('view/javascript/multimerch/jquery/jquery-ui.min.js');

		// languages
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// stores
		$this->load->model('setting/store');
		$this->data['stores'] = $this->model_setting_store->getStores();

		// types
		$types = ['select', 'checkbox', 'text'];

		$this->data['types'] = $types;

		// statuses
		$this->data['statuses'] = [MsfAttribute::STATUS_ACTIVE, MsfAttribute::STATUS_INACTIVE];

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_field_attribute_breadcrumbs'),
				'href' => $this->url->link('multimerch/field/attribute', '', 'SSL'),
			]
		]);
	}

	private function validatePost(&$data)
	{
		$errors = [];

		$validator = $this->MsLoader->MsValidator;

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$defaultLanguageId = $this->config->get('config_language_id');

		// validate primary language
		foreach ($languages as $language) {
			$language_id = $language['language_id'];

			$primary = (int)$language_id === (int)$defaultLanguageId ? true : false;

			// validate name
			if (!$validator->validate([
				'name' => $this->language->get('ms_field_attribute_name'),
				'value' => $data['description'][$language_id]['name']
			], [
				!$primary ? [] : ['rule' => 'required'],
				['rule' => 'max_len,100']
			])) $errors["description[$language_id][name]"] = $validator->get_errors();

			// validate label
			if (!$validator->validate([
				'name' => $this->language->get('ms_field_attribute_label'),
				'value' => $data['description'][$language_id]['label']
			], [
				['rule' => 'max_len,100']
			])) $errors["description[$language_id][label]"] = $validator->get_errors();

			if (in_array($data['type'], ['select', 'radio', 'checkbox']) && empty($data['values'])) {
				$errors['values'] = $this->language->get('ms_field_attribute_error_values');
			}

			if (!empty($data['values'])) {
				foreach ($data['values'] as $key => $value) {
					if (!$validator->validate([
						'name' => $this->language->get('ms_field_attribute_name'),
						'value' => $value['description'][$language_id]['name']
					], [
						!$primary ? [] : ['rule' => 'required'],
						['rule' => 'max_len,100']
					])) $errors["values[$key][description][$language_id][name]"] = $validator->get_errors();
				}
			}

			if (!$primary) {
				if (empty($data['description'][$language_id]['name'])) {
					$data['description'][$language_id]['name'] = $data['description'][$defaultLanguageId]['name'];
				}

				if (isset($data['values'])) {
					foreach ($data['values'] as $key => $value) {
						if (empty($value['description'][$language_id]['name'])) {
							$data['values'][$key]['description'][$language_id]['name'] = $value['description'][$defaultLanguageId]['name'];
						}
					}
				}
			}
		}

		return $errors;
	}
}
