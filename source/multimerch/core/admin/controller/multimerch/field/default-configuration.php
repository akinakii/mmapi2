<?php

class ControllerMultiMerchFieldDefaultConfiguration extends ControllerMultimerchBase
{
	public function index()
	{
		$this->document->addScript('view/javascript/multimerch/field/default-configuration.js');
		$this->document->addScript('view/javascript/multimerch/jquery/jquery-ui.min.js');
		$this->document->addScript('view/javascript/common.js');

		// languages
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// MSF attributes
		if ($this->config->get('msconf_msf_attribute_enabled')) {
			$this->data['msf_attributes'] = $this->MsLoader->MsfAttribute->getList(['default' => true], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC'
			]);
		}

		// MSF variations
		if ($this->config->get('msconf_msf_variation_enabled')) {
			$this->data['msf_variations'] = $this->MsLoader->MsfVariation->getList(['default' => true], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC'
			]);
		}

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->data['msf_seller_properties'] = $this->MsLoader->MsfSellerProperty->getList(['default' => true], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC'
			]);
		}

		// MS product filter blocks
		$this->load->model('setting/setting');
		$mspf_settings = $this->model_setting_setting->getSetting('multimerch_productfilter');

		$this->data['mspf_blocks_enabled'] = !empty($mspf_settings['multimerch_productfilter_status']);

		if ($this->data['mspf_blocks_enabled']) {
			$this->data['mspf_blocks'] = $this->MsLoader->MsProductFilter->getDefaultMspfBlocks();
		}

		$this->data['heading'] = $this->language->get('ms_field_default_configuration_heading');
		$this->document->setTitle($this->language->get('ms_field_default_configuration_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_field_default_configuration_breadcrumbs'),
				'href' => $this->url->link('multimerch/field/default-configuration', '', 'SSL'),
			]
		]);

		list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('multimerch/field/default-configuration');
		$this->response->setOutput($this->load->view($template, array_merge($this->data, $children)));
	}

	public function jxSave()
	{
		$data = $this->request->post;

		// Firstly, reset all fields to be not default
		$this->resetMsfAttributes();
		$this->resetMsfVariations();
		$this->resetMsfSellerProperties();

		// Now set the default fields

		// MSF attributes
		if (!empty($data['msf_attributes'])) {
			foreach ($data['msf_attributes'] as $sort_order => $msf_attribute_data) {
				$this->MsLoader->MsfAttribute->update($msf_attribute_data['id'], [
					'default' => true,
					'sort_order' => $sort_order
				]);
			}

			$this->session->data['success'] = $this->language->get('ms_field_default_configuration_success_updated');
		}

		// MSF variations
		if (!empty($data['msf_variations'])) {
			foreach ($data['msf_variations'] as $sort_order => $msf_variation_data) {
				$this->MsLoader->MsfVariation->update($msf_variation_data['id'], [
					'default' => true,
					'sort_order' => $sort_order
				]);
			}

			$this->session->data['success'] = $this->language->get('ms_field_default_configuration_success_updated');
		}

		// MSF seller properties
		if (!empty($data['msf_seller_properties'])) {
			foreach ($data['msf_seller_properties'] as $sort_order => $msf_seller_property_data) {
				$this->MsLoader->MsfSellerProperty->update($msf_seller_property_data['id'], [
					'default' => true,
					'sort_order' => $sort_order
				]);
			}

			$this->session->data['success'] = $this->language->get('ms_field_default_configuration_success_updated');
		}

		// MS product filter
		if (!empty($data['mspf_blocks'])) {
			$this->MsLoader->MsProductFilter->saveDefaultBlocks($data['mspf_blocks']);
		}

		$json['redirect'] = $this->url->link('multimerch/field/default-configuration', 'token=' . $this->session->data['token'], true);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function resetMsfAttributes()
	{
		$all_msf_attributes = $this->MsLoader->MsfAttribute->getList();
		foreach ($all_msf_attributes as $msf_attribute) {
			$this->MsLoader->MsfAttribute->update($msf_attribute['id'], [
				'default' => false
			]);
		}
	}

	private function resetMsfVariations()
	{
		$all_msf_variations = $this->MsLoader->MsfVariation->getList();
		foreach ($all_msf_variations as $msf_variation) {
			$this->MsLoader->MsfVariation->update($msf_variation['id'], [
				'default' => false
			]);
		}
	}

	private function resetMsfSellerProperties()
	{
		$all_msf_seller_properties = $this->MsLoader->MsfSellerProperty->getList();
		foreach ($all_msf_seller_properties as $msf_seller_property) {
			$this->MsLoader->MsfSellerProperty->update($msf_seller_property['id'], [
				'default' => false
			]);
		}
	}
}
