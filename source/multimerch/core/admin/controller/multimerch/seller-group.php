<?php

class ControllerMultimerchSellerGroup extends ControllerMultimerchBase
{
	use \MultiMerch\Module\Traits\Stripe;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->load->model('tool/image');
		$this->load->model('localisation/language');
		$this->load->model('setting/setting');

		$this->data['token'] = $this->session->data['token'];
	}
	
	public function getTableData()
	{
		$colMap = [
			'id' => 'msg.seller_group_id'
		];

		$sorts = ['id', 'name', 'description'];
		$filters = $sorts;
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsSellerGroup->getSellerGroups(
			[],
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = [];
		foreach ($results as $result) {
			// actions
			$actions = "";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/seller-group/update', 'token=' . $this->session->data['token'] . '&seller_group_id=' . $result['seller_group_id'], 'SSL') . "' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil'></i></a>";

			// Forbid default seller group deletion
			if((int)$result['seller_group_id'] !== 1)
				$actions .= "<a class='btn btn-danger ms-delete' title='".$this->language->get('button_delete')."' data-id='" . $result['seller_group_id'] . "' data-referrer='seller_group'><i class='fa fa-trash-o''></i></a>";
			
			$rates = $this->MsLoader->MsSellerGroup->getFeeRates($result['seller_group_id']);
			$actual_fees = '';
			foreach ($rates as $rate) {
				$actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . ($rate['rate_type'] != MsCommission::RATE_SIGNUP ? $rate['percent'] . '%+' : '') . $this->currency->getSymbolLeft() .  $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', FALSE) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
			}
			
			$columns[] = array_merge(
				$result,
				[
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['seller_group_id']}' />",
					'id' => "<input type='hidden' value='{$result['seller_group_id']}' />" . $result['seller_group_id'],
					'name' => $result['name'],
					'description' => (utf8_strlen($result['description']) > 80 ? mb_substr($result['description'], 0, 80) . '...' : $result['description']),
					'rates' => $actual_fees,
					'actions' => $actions
				]
			);
		}

		$this->response->setOutput(json_encode([
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		]));
	}
	
	// List all the seller groups
	public function index()
	{
		$this->data['insert'] = $this->url->link('multimerch/seller-group/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('multimerch/seller-group/delete', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['error_warning'] = '';
		if (isset($this->session->data['error_warning'])) {
			$this->data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}

		$this->data['success'] = '';
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$this->data['heading'] = $this->language->get('ms_catalog_seller_groups_heading');
		$this->document->setTitle($this->language->get('ms_catalog_seller_groups_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_catalog_seller_groups_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller-group', '', 'SSL'),
			]
		]);
		
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/seller-group.tpl', $this->data));
	}
	
	// Insert a new seller group
	public function insert()
	{
		$this->data['heading'] = $this->language->get('ms_catalog_insert_seller_group_heading');
		$this->document->setTitle($this->language->get('ms_catalog_insert_seller_group_heading'));

		$this->getCommonFormData();

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->data['msf_seller_properties'] = $this->MsLoader->MsfSellerProperty->getList(['default' => true], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC',
			]);
		}

		$this->data['settings'] = $this->MsLoader->MsSetting->getSellerGroupDefaults();
		$this->data['seller_group'] = null;

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_catalog_seller_groups_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller-group', '', 'SSL'),
			]
		]);
		
		$this->response->setOutput($this->load->view('multiseller/seller-group-form.tpl', $this->data));
	}
	
	// Update a seller group
	public function update()
	{
		$seller_group_id = !empty($this->request->get['seller_group_id']) ? $this->request->get['seller_group_id'] : null;

		if (!$seller_group_id)
			return $this->response->redirect($this->url->link('multimerch/seller-group', 'token=' . $this->session->data['token'], 'SSL'));

		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroup($seller_group_id);

		if (empty($seller_group))
			return $this->response->redirect($this->url->link('multimerch/seller-group', 'token=' . $this->session->data['token'], 'SSL'));

		$this->data['heading'] = $this->language->get('ms_catalog_edit_seller_group_heading');
		$this->document->setTitle($this->language->get('ms_catalog_edit_seller_group_heading'));

		$this->getCommonFormData();

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->data['msf_seller_properties'] = $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($seller_group_id, true);
		}

		// @todo 9.0: include settings in seller_group array
		$seller_group_settings = $this->MsLoader->MsSetting->getSellerGroupSettings(['seller_group_id' => $seller_group_id]);
		$defaults = $this->MsLoader->MsSetting->getSellerGroupDefaults();
		$this->data['settings'] = array_merge($defaults, $seller_group_settings);

		$this->data['seller_group'] = [
			'seller_group_id' => $seller_group['seller_group_id'],
			'description' => $this->MsLoader->MsSellerGroup->getSellerGroupDescriptions($seller_group_id),
			'product_period' => $seller_group['product_period'],
			'product_quantity' => $seller_group['product_quantity'],
			'commission_id' => $seller_group['commission_id'],
			'commission_rates' => !is_null($seller_group['msg.commission_id']) ? $this->MsLoader->MsCommission->getCommissionRates($seller_group['msg.commission_id']) : null
		];

		$seller_group_badges = $this->MsLoader->MsBadge->getBadges(['seller_group_id' => $seller_group_id]);
		$this->data['seller_group']['badges'] = [];
		foreach($seller_group_badges as $b) {
			$this->data['seller_group']['badges'][] = $b['badge_id'];
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs([
			[
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			],
			[
				'text' => $this->language->get('ms_catalog_seller_groups_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller-group', '', 'SSL'),
			]
		]);

		$this->response->setOutput($this->load->view('multiseller/seller-group-form', $this->data));
	}

	protected function getCommonFormData()
	{
		$this->document->addScript('view/javascript/multimerch/seller-group-form.js');
		$this->document->addScript('view/javascript/multimerch/jquery/jquery-ui.min.js');
		$this->document->addScript('view/javascript/common.js');

		$this->data['cancel'] = $this->url->link('multimerch/seller-group', 'token=' . $this->session->data['token'], 'SSL'); //'multiseller/seller-group';
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// Badges
		$this->data['badges'] = $this->MsLoader->MsBadge->getBadges();
		foreach ($this->data['badges'] as &$badge) {
			$badge['image'] = $this->model_tool_image->resize($badge['image'], 30, 30);
		}

		if ($this->config->get('ms_stripe_connect_enable_subscription')) {
			try {
				$this->initStripe();

				$stripe_plans = [];

				if ($this->config->get('ms_stripe_connect_plan_ids')) {
					foreach ((array)$this->config->get('ms_stripe_connect_plan_ids') as $stripe_plan_type => $stripe_plan_ids) {
						foreach ($stripe_plan_ids as $stripe_plan_id) {
							$stripe_plan = $this->getStripePlan($stripe_plan_id);

							if (!empty($stripe_plan)) {
								if ('base' === $stripe_plan_type)
									$stripe_plan->condition_base = sprintf($this->language->get('ms_seller_group_stripe_subscription_plan_base_info'), $this->currency->format($stripe_plan->amount, strtoupper($stripe_plan->currency)), $this->language->get('ms_seller_group_stripe_subscription_plan_interval_'.$stripe_plan->interval));

								if ('per_seat' === $stripe_plan_type)
									$stripe_plan->condition_per_seat = sprintf($this->language->get('ms_seller_group_stripe_subscription_plan_per_seat_info'), $this->currency->format($stripe_plan->amount, strtoupper($stripe_plan->currency)), $this->language->get('ms_seller_group_stripe_subscription_plan_interval_'.$stripe_plan->interval));
							}

							$stripe_plans[$stripe_plan_type][] = $stripe_plan;
						}
					}
				}

				$this->data['stripe_plans'] = $stripe_plans;
			} catch (\Stripe\Error\InvalidRequest $e) {
				$this->ms_logger->error($e->getMessage());
			} catch (\MultiMerch\Module\Errors\Settings $e) {
				$this->ms_logger->error($e->getMessage());
			}
		}

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
	}
	
	// Bulk delete of seller groups
	public function delete()
	{
		$json = [];

		if(!isset($this->request->get['seller_group_id']) && !isset($this->request->post['selected'])) {
			$json['errors'][] = $this->language->get('ms_error_seller_group_deleting');
		}

		if(!isset($json['errors'])) {
			$seller_group_ids = isset($this->request->get['seller_group_id']) ?
				[$this->request->get['seller_group_id']] :
				(isset($this->request->post['selected']) ? $this->request->post['selected'] : []);

			foreach ($seller_group_ids as $seller_group_id) {
				$this->MsLoader->MsSellerGroup->deleteSellerGroup($seller_group_id);
			}

			$this->session->data['success'] =  $this->language->get('ms_success_seller_group_deleting');
			$json['redirect'] = $this->url->link('multimerch/seller-group', 'token=' . $this->session->data['token'], true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function jxSave()
	{
		$data = $this->request->post['seller_group'];
		$json = [];

		$data['product_period'] = !empty($data['product_period']) ? $data['product_period'] : 0;
		$data['product_quantity'] = !empty($data['product_quantity']) ? $data['product_quantity'] : 0;

		foreach ($data['description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 32)) {
				$json['errors']['seller_group[description]['.$language_id.'][name]'] = $this->language->get('ms_error_seller_group_name');
			}
		}

		// Commissions
		foreach ([MsCommission::RATE_SALE, MsCommission::RATE_LISTING, MsCommission::RATE_SIGNUP] as $type) {
			$flat = !empty($data['commission_rates'][$type]['flat']) ? (float)$data['commission_rates'][$type]['flat'] : 0;
			$percent = !empty($data['commission_rates'][$type]['percent']) ? (float)$data['commission_rates'][$type]['percent'] : 0;
			$payment_method = !empty($data['commission_rates'][$type]['payment_method']) ? (int)$data['commission_rates'][$type]['payment_method'] : ($type === MsCommission::RATE_SALE ? null : MsPgPayment::METHOD_BALANCE);

			$data['commission_rates'][$type] = [
				'flat' => $flat,
				'percent' => $percent,
				'payment_method' => $payment_method
			];
		}

		// Validate Stripe subscription settings
		if ($this->config->get('ms_stripe_connect_enable_subscription')) {
			if (empty($data['settings']['slr_gr_stripe_subscription_enabled'])) {
				// If Stripe subscription is disabled, nullify all related plans settings
				$data['settings']['slr_gr_stripe_plan_base_id'] = $data['settings']['slr_gr_stripe_plan_per_seat_id'] = null;
			} else {
				// If Stripe subscription is enabled, but no plans were selected, disable subscription
				if (empty($data['settings']['slr_gr_stripe_plan_base_id']) && empty($data['settings']['slr_gr_stripe_plan_per_seat_id']))
					$data['settings']['slr_gr_stripe_subscription_enabled'] = 0;
			}
		} else {
			$data['settings']['slr_gr_stripe_subscription_enabled'] = 0;
			$data['settings']['slr_gr_stripe_plan_base_id'] = $data['settings']['slr_gr_stripe_plan_per_seat_id'] = null;
		}

		$validator = $this->MsLoader->MsValidator;

		$is_valid = $validator->validate([
			'name' => $this->language->get('ms_seller_group_product_number_limit'),
			'value' => $data['settings']['slr_gr_product_number_limit']
		], [['rule' => 'numeric']]);

		if (!$is_valid)
			$json['errors']['seller_group[settings][slr_gr_product_number_limit]'] = $validator->get_errors();

		if (empty($json['errors'])) {
			if (empty($data['seller_group_id'])) {
				$seller_group_id = $this->MsLoader->MsSellerGroup->createSellerGroup($data);
				$this->MsLoader->MsSetting->createSellerGroupSetting(array_merge($data, ['seller_group_id' => $seller_group_id]));
				$this->session->data['success'] = $this->language->get('ms_success_seller_group_created');
			} else {
				$seller_group_id = $data['seller_group_id'];
				$this->MsLoader->MsSellerGroup->editSellerGroup($seller_group_id, $data);
				$this->MsLoader->MsSetting->createSellerGroupSetting(array_merge($data, ['seller_group_id' => $seller_group_id]));
				$this->session->data['success'] = $this->language->get('ms_success_seller_group_updated');
			}

			// Clear leftover settings @todo 9.0: consider deleting all empty settings from `ms_seller_group_setting` table
			foreach (['slr_gr_stripe_plan_base_id', 'slr_gr_stripe_plan_per_seat_id'] as $setting_name) {
				if (empty($data['settings'][$setting_name]))
					$this->MsLoader->MsSetting->deleteSellerGroupSetting([
						'seller_group_id' => $seller_group_id,
						'name' => $setting_name
					]);
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * Retrieves Stripe plan.
	 *
	 * @param	int					$stripe_plan_id		Stripe plan id.
	 * @return	null|\Stripe\Plan	$plan				Retrieved plan.
	 * @throws Exception
	 */
	protected function getStripePlan($stripe_plan_id)
	{
		$plan = null;

		try {
			$plan = \Stripe\Plan::retrieve($stripe_plan_id);
			$plan->amount /= 100;
		} catch (\Stripe\Error\Authentication $e) {
			$this->ms_logger->error($e->getMessage());
		} catch (\Stripe\Error\InvalidRequest $e) {
			$this->ms_logger->error($e->getMessage());
		} catch (\MultiMerch\Module\Errors\Settings $e) {
			$this->ms_logger->error($e->getMessage());
		}

		return $plan;
	}

	/**
	 * Creates Stripe plan.
	 *
	 * Throws 'error' if any required params were not passed in POST array.
	 * Throws 'error' if Stripe API request fails.
	 * Throws 'success' if Stripe plan is successfully created.
	 *
	 * @param	array		$data		Parameters for a plan creation.
	 * 									Required keys: `base`, `seller_group_id`, `seller_group_name`.
	 * 									Possible keys: `per_seat`.
	 * @return	array		$errors		Errors occurred during the process.
	 * @throws	Exception
	 */
	protected function createStripePlan($data)
	{
		$errors = [];

		try {
			list($stripe_connect_settings, $env) = $this->initStripe();

			$plan = \Stripe\Plan::create([
				'product' => (string)$stripe_connect_settings['environments'][$env]['product_id'],
				'nickname' => (string)$data['seller_group_name'] . ' (' . $this->language->get('ms_seller_group_subscription_plan_fee_'.$data['plan_type']) . ')',
				'interval' => (string)$data['interval'],
				'currency' => $stripe_connect_settings['account_currency'],
				'amount' => (float)$data['amount'] * 100,
				'usage_type' => ('per_seat' === (string)$data['plan_type']) ? 'metered' : 'licensed',
				'metadata' => [
					'plan_type' => (string)$data['plan_type'],
					'seller_group_id' => (int)$data['seller_group_id']
				]
			]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			$this->ms_logger->error($e->getMessage() . ' (Stripe code: ' . $e->getStripeCode() . ')');
			$errors[] = $e->getMessage();
		} catch (\MultiMerch\Module\Errors\Settings $e) {
			$this->ms_logger->error($e->getMessage());
			$errors[] = $e->getMessage();
		} catch (Exception $e) {
			$this->ms_logger->error($e->getMessage());
			throw $e;
		}

		return $errors;
	}

	public function jxAutocompleteMsfSellerProperties() {
		$json = [];

		if (isset($this->request->get['filter_name'])) {
			$added_ids = !empty($this->request->get['added_ids']) ? explode(',', $this->request->get['added_ids']): [];

			$results = $this->MsLoader->MsfSellerProperty->getList(['not_in_ids' => $added_ids], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC',
				'filters' => ['name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))],
				'offset' => 0,
				'limit' => 10
			]);

			foreach ($results as $result) {
				$json[] = [
					'msf_seller_property_id'  => $result['id'],
					'label'        		=> strip_tags(html_entity_decode($result['full_name'], ENT_QUOTES, 'UTF-8'))
				];
			}
		}

		$sort_order = [];

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['label'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetSellerGroupMsfSellerProperties()
	{
		$seller_group_id = !empty($this->request->get['seller_group_id']) ? $this->request->get['seller_group_id'] : null;
		$action = !empty($this->request->get['action']) ? $this->request->get['action'] : 'initial';

		$data = $this->load->language('multiseller/multiseller');

		// Initially set default MSF seller properties
		$data['msf_seller_properties'] = $this->MsLoader->MsfSellerProperty->getList(['default' => true], [
			'order_by'  => 'sort_order',
			'order_way' => 'ASC',
		]);

		// Whether attributes overridden
		$data['is_overridden'] = false;

		$data['messages'] = [
			'override' => $this->language->get('ms_field_seller_property_message_default'),
			'remove_override' => $this->language->get('ms_field_seller_property_message_override'),
		];

		switch ($action) {
			case 'override':
				if ($seller_group_id) {
					// Check if there are existing for current seller group
					$current_msf_seller_properties = $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($seller_group_id);

					if (!empty($current_msf_seller_properties)) {
						$data['msf_seller_properties'] = $current_msf_seller_properties;
					}
				}

				$data['is_overridden'] = true;
				break;

			case 'remove_override':
				$data['is_overridden'] = false;
				break;

			case 'initial':
			default:
				if ($seller_group_id) {
					// Check if there are existing for current seller group
					$current_msf_seller_properties = $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($seller_group_id);

					if (!empty($current_msf_seller_properties)) {
						$data['msf_seller_properties'] = $current_msf_seller_properties;
						$data['is_overridden'] = true;
					}
				}
				break;
		}

		return $this->response->setOutput(json_encode([
			'success' => true,
			'html' => $this->load->view('multiseller/seller-group-form-msf-seller-property', $data)
		]));
	}
}
