<?php

class ControllerMultimerchSeller extends ControllerMultimerchBase {
	public function __construct($registry) {
		parent::__construct($registry);

		$this->language->load('customer/customer');

		$this->load->model('customer/customer');
		$this->load->model('localisation/country');
		$this->load->model('tool/image');
		$this->load->model('setting/store');
		$this->load->model('localisation/language');

		// Validate seller exists
		if (!empty($this->request->get['seller_id']) && !$this->MsLoader->MsSeller->isCustomerSeller($this->request->get['seller_id']))
			return $this->response->redirect($this->url->link('multimerch/seller', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function getTableData() {
		$colMap = array(
			'seller' => 'ms.nickname',
			'email' => 'c.email',
			'balance' => '`current_balance`',
			'date_created' => '`ms.date_created`',
			'status' => 'ms.seller_status'
		);

		$sorts = array('seller', 'email', 'total_products', 'total_sales', 'balance', 'status', 'date_created');
		$filters = array_diff($sorts, array('total_sales', 'balance'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$results = $this->MsLoader->MsSeller->getSellers(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'total_products' => 1,
				'total_sales' => 1,
				'current_balance' => 1
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// actions
			$actions = "";

			// login as seller
			$actions .= "<a class='btn btn-info' target='_blank' href='" . ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . "index.php?route=seller/catalog-seller/profile&seller_id=" . $result['seller_id'] . "' data-toggle='tooltip' title='".$this->language->get('ms_catalog_sellers_view_profile')."'><i class='fa fa-search'></i></a> ";
			$actions .= "<div class='btn-group' data-toggle='tooltip' title='" . $this->language->get('button_login') . "'>";
			$actions .= "<button type='button' data-toggle='dropdown' class='btn btn-success dropdown-toggle'><i class='fa fa-lock'></i></button>";
			$actions .= "<ul class='dropdown-menu pull-right'>";
			$actions .= "<li><a href='" . $this->url->link('customer/customer/login', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['seller_id'] . '&store_id=0', 'SSL') . "' target='_blank'>" . $this->language->get('text_default') . "</a></li>";
			foreach ($this->model_setting_store->getStores() as $store) {
				$actions .= "<li><a href='" . $this->url->link('customer/customer/login', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['seller_id'] . '&store_id=' . $store['store_id'], 'SSL') . "' target='_blank'>" . $store['name'] . "</a></li>";
			}
			$actions .= "</ul>";
			$actions .= "</div> ";

			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL') . "' data-toggle='tooltip' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil'></i></a> ";
			$actions .= "<a class='btn btn-danger ms-delete' data-toggle='tooltip' title='".$this->language->get('button_delete')."' data-id='" . $result['seller_id'] . "' data-referrer='seller'><i class='fa fa-trash-o'></i></a> ";

			$image = $this->model_tool_image->resize($result['ms.avatar'] && is_file(DIR_IMAGE . $result['ms.avatar']) ? $result['ms.avatar'] : 'ms_no_image.jpg', '40', '40');

			// build table data
			$columns[] = array_merge(
				$result,
				array(
					'image' => '<img src="' . $image . '" class="ms-list-image-thumb" />',
					'seller' => $result['ms.nickname'],
					'email' => $result['c.email'],
					'balance' => $this->currency->format($this->MsLoader->MsBalance->getSellerBalance($result['seller_id']), $this->config->get('config_currency')),
					'status' => $this->language->get('ms_seller_status_' . $result['ms.seller_status']),
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['ms.date_created'])),
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
  			'iTotalDisplayRecords' => $total, //count($results),
			'aaData' => $columns
		)));
	}

	public function getProductTableData() {
		$colMap = array(
			'id' => 'product_id',
			'name' => 'pd.name',
			'status' => 'mp.product_status',
			'price' => 'CASE WHEN special THEN special ELSE p.price END',
			'quantity' => 'p.quantity',
			'seller' => 'ms.nickname',
			'date_added' => 'p.date_added',
			'date_modified' => 'p.date_modified'
		);

		$sorts = array('name', 'seller', 'price', 'quantity', 'date_added', 'date_modified', 'status');
		$filters = array_diff($sorts, array('price', 'quantity'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$fetch_conditions = array();
		if (isset($this->request->get['seller_id']) AND $this->request->get['seller_id']){
			$fetch_conditions['seller_id'] = $this->request->get['seller_id'];
		}

		$results = $this->MsLoader->MsProduct->getProducts(
			$fetch_conditions,
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'product_sales' => 1
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			$shop_url = ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . "index.php?route=product/product&product_id=" . $result['product_id'];
			// actions
			$actions = "";
			$actions .= "<a class='btn btn-info' target='_blank' class='ms-button' href='" . $shop_url . "' title='".$this->language->get('ms_view_in_store')."'><i class='fa fa-search'></i></a>";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'], 'SSL') . "' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil''></i></a>";
			$actions .= "<a class='btn btn-danger ms-delete' title='".$this->language->get('ms_delete')."' data-id='" . $result['product_id'] . "' data-referrer='product'><i class='fa fa-trash-o'></i></a>";

			$image = $this->model_tool_image->resize($result['p.image'] && is_file(DIR_IMAGE . $result['p.image']) ? $result['p.image'] : 'no_image.png', '40', '40');

			// price
			$product['p.price'] = $this->currency->format($result['p.price'], $this->config->get('config_currency'));
			if ($result['special']) {
				$special = $this->currency->format($result['special'], $this->config->get('config_currency'));
				$price = "<span style='text-decoration: line-through;'>{$product['p.price']}</span><br/>";
				$price .= "<span class='special-price' style='color: #b00;'>$special</span>";
			} else {
				$price = $product['p.price'];
			}

			$columns[] = array_merge(
				$result,
				array(
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['product_id']}' />",
					'image' => '<img src="' . $image . '" class="ms-list-image-thumb" />',
					'name' => $result['pd.name'],
					'price' => $price,
					'quantity' => $result['p.quantity'],
					'status' => $result['mp.product_status'] ? $this->language->get('ms_product_status_' . $result['mp.product_status']) : '',
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['p.date_added'])),
					'date_modified' => date($this->language->get('date_format_short'), strtotime($result['p.date_modified'])),
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function getTransactionTableData() {
		$colMap = array(
			'id' => 'balance_id',
			'seller' => '`nickname`',
			'description' => 'mb.description',
			'date_created' => 'mb.date_created'
		);

		$sorts = array('id', 'seller', 'amount', 'description', 'date_created');
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$fetch_conditions = array();
		if (isset($this->request->get['seller_id']) AND $this->request->get['seller_id']){
			$fetch_conditions['seller_id'] = $this->request->get['seller_id'];
		}

		$results = $this->MsLoader->MsBalance->getBalanceEntries(
			$fetch_conditions,
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			$columns[] = array_merge(
				$result,
				array(
					'id' => $result['balance_id'],
					'seller' => $result['nickname'],
					'amount' => $this->currency->format($result['amount'], $this->config->get('config_currency')),
					'description' => (utf8_strlen($result['mb.description']) > 80 ? mb_substr($result['mb.description'], 0, 80) . '...' : $result['mb.description']),
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['mb.date_created'])),
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function getInvoiceTableData() {
		$colMap = [];

		$sorts = ['invoice_id', 'type', 'total', 'status', 'date_generated', 'payment_id'];
		$filters = array_diff($sorts, ['type', 'total', 'status']);

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$fetch_conditions = ['type' => 'payout'];
		if (!empty($this->request->get['seller_id'])) {
			$fetch_conditions['sender_type'] = 'seller';
			$fetch_conditions['sender_id'] = (int)$this->request->get['seller_id'];
		}

		$invoices = $this->MsLoader->MsInvoice->get(
			$fetch_conditions,
			[
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			]
		);

		$total = count($invoices);

		$columns = [];
		foreach ($invoices as $invoice) {
			$payment_info = '-';

			// If invoice is paid
			if ($invoice->getPaymentId()) {
				$payment = $this->MsLoader->MsPgPayment->getPayments(['payment_id' => $invoice->getPaymentId(), 'single' => true]);

				$pg_name = str_replace('ms_pg_', '', $payment['payment_code']);
				$this->load->language('multimerch/payment/' . $pg_name);

				// Show payment details
				$html  = '<p>Method: ' . $this->language->get('text_method_name') . '</p>';
				$html .= '<p>Date paid: ' . date($this->language->get('date_format_short'), strtotime($payment['date_created'])) . '</p>';
				$html .= '<p>Status: ' . $this->language->get('ms_pg_payment_status_no_color_' . $payment['payment_status']) . '</p>';

				$payment_info  = '<a class="paymentInfoControl" href="#" data-toggle="tooltip" data-html="true" title="' . $html . '" onclick="return false;">' . (int)$invoice->getPaymentId() . '</a>';
			}

			$columns[] = [
				'invoice_id' => $invoice->getInvoiceId(),
				'type' => $this->language->get('ms_invoice_type_' . $invoice->getType()),
				'total' => $this->currency->format(abs($invoice->getTotal()), $invoice->getCurrencyCode()),
				'title' => $invoice->getTitle(),
				'date_generated' => date($this->language->get('date_format_short'), strtotime($invoice->getDateGenerated())),
				'status' => $this->language->get('ms_invoice_status_' . $invoice->getStatus()),
				'payment_id' => $payment_info
			];
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function getPaymentTableData() {
		$colMap = array(
			'payment_id' => 'payment_id',
			'payment_code' => 'payment_code',
			'seller' => 'nickname',
			'type' => 'payment_type',
			'description' => 'description',
			'payment_status' => 'payment_status',
			'date_created' => 'date_created'
		);

		$sorts = array('payment_id', 'payment_type', 'payment_code', 'seller', 'description', 'amount', 'payment_status', 'date_created');
		$filters = array_diff($sorts, array('payment_status'));

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$fetch_conditions = array();
		if (isset($this->request->get['seller_id']) AND $this->request->get['seller_id']){
			$fetch_conditions['seller_id'] = $this->request->get['seller_id'];
		}

		$results = $this->MsLoader->MsPgPayment->getPayments(
			$fetch_conditions,
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// payment method name
			$pg_name = str_replace(MsPgPayment::ADMIN_SETTING_PREFIX, '', $result['payment_code']);

			// description
			$description = '';
			$description .= '<ul style="list-style: none; padding-left: 0;">';
			foreach ($result['description'] as $invoice_id => $value) {
				$description .= '<li>' . $value . '</li>';
			}
			$description .= '</ul>';

			$invoices = $this->MsLoader->MsInvoice->get(['payment_id' => $result['payment_id']]);

			if($pg_name == 'ms_pp_adaptive') {
				$this->load->language('payment/ms_pp_adaptive');
				$payment_method = $this->language->get('ppa_adaptive');
			} else {
				$this->load->language('multimerch/payment/' . $pg_name);
				if($pg_name == 'paypal') {
					$payment_method = count($invoices) > 1 ? $this->language->get('text_mp_method_name') : $this->language->get('text_s_method_name');
				} else {
					$payment_method = $this->language->get('text_method_name');
				}
			}

			$payment_status = $this->language->get('ms_pg_payment_status_' . $result['payment_status']);
			if($result['payment_status'] == MsPgPayment::STATUS_INCOMPLETE) {
				$payment_status .= '<button type="button" data-toggle="tooltip" title="" class="ms-confirm-manually btn btn-primary" data-original-title="Apply"><i class="fa  fa-check"></i></button>';
			}

			$columns[] = array_merge(
				$result,
				array(
					'payment_id' => "<input type='hidden' name='payment_id' value='" . $result['payment_id']. "' />" . $result['payment_id'],
					'payment_type' => $this->language->get('ms_pg_payment_type_' . $result['payment_type']),
					'payment_code' => $payment_method,
					'seller' => "<a href='".$this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $result['seller_id'], 'SSL')."'>{$result['nickname']}</a>",
					'description' => $description,
					'amount' => $this->currency->format(abs($result['amount']), $result['currency_code']),
					'payment_status' => $payment_status,
					'date_created' => date($this->language->get('date_format_short'), strtotime($result['date_created']))
				)
			);
		}
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	/**
	 * Updates seller account and address information.
	 *
	 * Accepts data passed by the POST method.
	 *
	 * Returns 'errors' if validation of form(s) fields failed.
	 * Returns 'redirect' if seller account and address information was successfully updated.
	 *
	 * @return	string			JSON with possible messages: 'errors', 'redirect'.
	 */
	public function jxSaveSellerInfo() {
		$json = array();
		$data = $this->request->post;
		$data['seller_id'] = !empty($data['seller']['seller_id']) ? $data['seller']['seller_id'] : 0;

		$json['errors'] = $this->_validateFormFields($data);

		if (empty($json['errors'])) {
			// Prepare commissions
			if (!empty($data['seller']['commission'])) {
				foreach ($data['seller']['commission'] as $type => $rate) {
					if (empty($rate['flat']) && empty($rate['percent'])) {
						unset($data['seller']['commission'][$type]);
					}
				}
			}

			if (empty($data['seller']['seller_id'])) {
				$this->_createSeller($data);
			} else {
				$this->_editSeller($data);
			}

			if (isset($data['payment_gateways'])) {
				// add seller's payment gateways settings
				$pg_settings_data = array();
				foreach ($data['payment_gateways'] as $code => $setting) {
					$pg_settings_data['seller_id'] = $data['seller']['seller_id'];
					foreach ($setting as $name => $value) {
						$pg_settings_data['settings'][$code . '_' . $name] = $value;
					}
				}
				$this->MsLoader->MsSetting->createSellerSetting($pg_settings_data);
			}

			$this->session->data['success'] = 'Seller account data saved.';
		}

		$this->response->setOutput(json_encode($json));
	}

	public function jxSetPayoutAmount() {
		$json = array();
		$data = $this->request->post;

		if(!isset($data['seller_ids']) || empty($data['seller_ids'])) {
			$json['error'] = 'Something is empty!';
		} else {
			$sellers = array();
			foreach ($data['seller_ids'] as $seller) {
				$sellers[] = array(
					'info' => $this->MsLoader->MsSeller->getSeller($seller['id']),
					'available_amount' => $seller['available']
				);
			}

			$this->data['token'] = $this->session->data['token'];
			$this->data['sellers'] = $sellers;

			list($template, $children) = $this->MsLoader->MsHelper->admLoadTemplate('multiseller/seller-payout-amount-form');
			$json['html'] = $this->load->view($template, $this->data);
		}

		$this->response->setOutput(json_encode($json));
	}

	public function jxDeleteProduct() {
		$json = array();
		$product_ids = array();
		if(isset($this->request->post['selected_products'])){
			$product_ids = $this->request->post['selected_products'];
		} else if(isset($this->request->get['product_id'])) {
			$product_id =  $this->request->get['product_id'];
			$product_ids = array($product_id);
		}else{
			$json['errors'] = 'Something is empty!';
		}
		foreach($product_ids as $product_id) {
			$this->MsLoader->MsProduct->deleteProduct($product_id);
		}
		$this->response->setOutput(json_encode($json));
	}

	public function index() {
		$this->document->addScript('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
		$this->document->addStyle('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css');

		$this->validate(__FUNCTION__);

		// paypal listing payment confirmation
		if (isset($this->request->post['payment_status']) && strtolower($this->request->post['payment_status']) == 'completed') {
			$this->data['success'] = $this->language->get('ms_payment_completed');
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['heading'] = $this->language->get('ms_catalog_sellers_heading');
		$this->document->setTitle($this->language->get('ms_catalog_sellers_heading'));

		$this->data['link_create_seller'] = $this->url->link('multimerch/seller/create', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/seller.tpl', $this->data));
	}

	public function create() {
		$this->data['heading'] = $this->language->get('ms_catalog_sellerinfo_create_heading');
		$this->document->setTitle($this->language->get('ms_catalog_sellerinfo_create_heading'));

		$this->_initSellerForm();

		$this->data['seller'] = false;
		$this->data['address'] = false;
		$this->data['customers'] = $this->MsLoader->MsSeller->getCustomers();

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_newseller'),
				'href' => $this->url->link('multimerch/seller/create', 'SSL'),
			)
		));

		$this->response->setOutput($this->load->view('multiseller/seller-form.tpl', $this->data));
	}

	public function update() {
		if (empty($this->request->get['seller_id']))
			return $this->response->redirect($this->url->link('multimerch/seller', 'token=' . $this->session->data['token'], 'SSL'));

		$this->data['heading'] = $this->language->get('ms_catalog_sellerinfo_update_heading');
		$this->document->setTitle($this->language->get('ms_catalog_sellerinfo_update_heading'));

		$this->_initSellerForm();

		// MultiMerch address
		$ms_address_id = $this->MsLoader->MsSetting->getSellerSettings(array(
			'seller_id' => $this->request->get['seller_id'],
			'name' => 'slr_ms_address',
			'single' => 1
		));
		$this->data['address'] = !empty($ms_address_id) ? $this->MsLoader->MsSeller->getSellerMsAddress(array('address_id' => $ms_address_id, 'single' => 1)) : array();

		// Seller settings
		$seller_settings = $this->MsLoader->MsSetting->getSellerSettings(array('seller_id' => $this->request->get['seller_id']));
		$this->data['settings'] = array_merge(!empty($this->data['settings']) ? $this->data['settings'] : $this->MsLoader->MsSetting->getSellerDefaults(), $seller_settings);

		$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
		if (!empty($seller)) {
			$seller['commission_id'] = $seller['ms.commission_id'];
			$seller['commission_rates'] = !is_null($seller['commission_id']) ? $this->MsLoader->MsCommission->getCommissionRates($seller['commission_id']) : NULL;

			// Seller badges
			$seller_badges = $this->MsLoader->MsBadge->getBadges(array('seller_id' => $seller['seller_id']));
			$seller['badges'] = array();
			foreach ($seller_badges as $b) {
				$seller['badges'][] = $b['badge_id'];
			}

			foreach ($this->data['languages'] as $language) {
				if (empty($seller['descriptions'][$language['language_id']]['description']))
					$seller['descriptions'][$language['language_id']]['description'] = '';

				if (empty($seller['descriptions'][$language['language_id']]['slogan']))
					$seller['descriptions'][$language['language_id']]['slogan'] = '';
			}
		}

		$this->data['seller'] = $seller;

		if (!empty($seller['ms.avatar'])) {
			$this->data['seller']['avatar']['name'] = $seller['ms.avatar'];
			$this->data['seller']['avatar']['thumb'] = $this->model_tool_image->resize($seller['ms.avatar'], $this->config->get('msconf_preview_seller_avatar_image_width'), $this->config->get('msconf_preview_seller_avatar_image_height'));
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_catalog_sellers_breadcrumbs'),
				'href' => $this->url->link('multimerch/seller', '', 'SSL'),
			),
			array(
				'text' => $seller['ms.nickname'],
				'href' => $this->url->link('multimerch/seller/update', '&seller_id=' . $seller['seller_id'], 'SSL'),
			)
		));

		$this->response->setOutput($this->load->view('multiseller/seller-form.tpl', $this->data));
	}

	public function delete() {
		$json = array();

		if(!isset($this->request->get['seller_id']) && !isset($this->request->post['selected'])) {
			$json['errors'][] = $this->language->get('ms_seller_error_deleting');
		}

		if(!isset($json['errors'])) {
			$seller_ids = isset($this->request->get['seller_id']) ?
				array($this->request->get['seller_id']) :
				(isset($this->request->post['selected']) ? $this->request->post['selected'] : array());

			foreach ($seller_ids as $seller_id) {
				$this->MsLoader->MsSeller->deleteSeller($seller_id);

				$this->session->data['success'] =  $this->language->get('ms_seller_success_deleting');
				$json['redirect'] = $this->url->link('multimerch/seller', 'token=' . $this->session->data['token'], true);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function _initSellerForm()
	{
		$this->document->addScript('view/javascript/summernote/summernote.js');
		$this->document->addScript('view/javascript/summernote/opencart.js');
		$this->document->addStyle('view/javascript/summernote/summernote.css');

		// badges
		$badges = $this->MsLoader->MsBadge->getBadges();
		foreach($badges as &$badge) {
			$badge['image'] = $this->model_tool_image->resize($badge['image'], 30, 30);
		}

		$this->data['badges'] = $badges;
		$this->data['countries'] = $this->model_localisation_country->getCountries();
		$this->data['currency_code'] = $this->config->get('config_currency');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['payment_gateways'] = $this->_getPaymentGateways();
		$this->data['seller_groups'] = $this->MsLoader->MsSellerGroup->getSellerGroups([], ['order_by' => 'msg.seller_group_id', 'order_way' => 'ASC']);
		$this->data['settings'] = $this->MsLoader->MsSetting->getSellerDefaults();
		$this->data['token'] = $this->session->data['token'];

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
	}

	private function _getPaymentGateways() {
		$this->load->model('extension/extension');
		$this->load->model('setting/setting');

		$payment_gateways = array();
		$extensions = $this->model_extension_extension->getInstalled('ms_payment');

		foreach ($extensions as $extension) {
			$extension_name = str_replace('ms_pg_', '', $extension);

			$this->load->language('multimerch/payment/' . $extension_name);

			$settings = $this->model_setting_setting->getSetting($extension);

			foreach ($settings as $name => $value) {
				if((strpos($name, 'payout_enabled') && $value) || (strpos($name, 'fee_enabled') && $value)) {
					$payment_gateways[] = array(
						'code' => $extension,
						'text_title' => $this->language->get('heading_title'),
						'view' => $this->load->controller('multimerch/payment/' . $extension_name . '/jxGetPgSettingsForm')
					);
					break;
				}
			}
		}

		return $payment_gateways;
	}

	public function _createCustomer($data)
	{
		$customer_data = array(
			'firstname' => $data['customer']['firstname'],
			'lastname' => $data['customer']['lastname'],
			'email' => $data['customer']['email'],
			'password' => $data['customer']['password'],
			'telephone' => '',
			'fax' => '',
			'customer_group_id' => $this->config->get('config_customer_group_id'),
			'newsletter' => 1,
			'status' => 1,
			'approved' => 1,
			'safe' => 1
		);

		$customer_id = $this->model_customer_customer->addCustomer($customer_data);

		return $customer_id;
	}

	private function _createSeller($data)
	{
		$seller_data = array(
			'avatar_name' => isset($data['seller']['avatar']) ? $data['seller']['avatar'] : '',
			'banner_name' => isset($data['seller']['banner']) ? $data['seller']['banner'] : '',
			'seller_id' => empty($data['customer']['customer_id']) ? $this->_createCustomer($data) : $data['customer']['customer_id'],
			'status' => $data['seller']['status'],
			'approved' => 1,
			'seller_group' => $data['seller']['seller_group'],
			'nickname' => $data['seller']['nickname'],
			'languages' => isset($data['seller']['languages']) ? $data['seller']['languages'] : array(),
			'settings' => array(
				'slr_product_validation' => isset($data['seller_setting']['slr_product_validation']) ? $data['seller_setting']['slr_product_validation'] : '',
				'slr_website' => isset($data['seller_setting']['slr_website']) ? $data['seller_setting']['slr_website'] : '',
				'slr_company' => isset($data['seller_setting']['slr_company']) ? $data['seller_setting']['slr_company'] : '',
				'slr_phone' => isset($data['seller_setting']['slr_phone']) ? $data['seller_setting']['slr_phone'] : ''
			),
			'address' => isset($data['seller']['address']) ? $data['seller']['address'] : array(),
			'badges' => isset($data['seller']['badges']) ? $data['seller']['badges'] : array(),
			'keyword' => !empty($data['seller']['keyword']) ? $this->MsLoader->MsHelper->slugify($data['seller']['keyword']) : '',
			'commission_id' => !empty($data['seller']['commission_id']) ? $data['seller']['commission_id'] : null,
			'commission' => !empty($data['seller']['commission']) ? $data['seller']['commission'] : [],
			'msf_seller_properties' => !empty($data['msf_seller_properties']) ? $data['msf_seller_properties'] : []
		);

		$this->MsLoader->MsSeller->createSeller($seller_data);
	}

	public function _editSeller($data)
	{
		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$seller = $this->MsLoader->MsSeller->getSeller($data['seller_id']);

		$MailSellerAccountModified = $serviceLocator->get('MailSellerAccountModified', false)
			->setTo($seller['c.email'])
			->setData(array(
				'addressee' => $seller['ms.nickname'],
				'ms_seller_status' => $data['seller']['status'],
				'message' => (isset($data['seller']['message']) ? $data['seller']['message'] : ''),
			));

		$mails->add($MailSellerAccountModified);

		switch ($data['seller']['status']) {
			case MsSeller::STATUS_INACTIVE:
			case MsSeller::STATUS_DISABLED:
			case MsSeller::STATUS_DELETED:
			case MsSeller::STATUS_INCOMPLETE:
				$products = $this->MsLoader->MsProduct->getProducts(array(
					'seller_id' => $seller['seller_id']
				));

				foreach ($products as $p) {
					$this->MsLoader->MsProduct->changeStatus($p['product_id'], $data['seller']['status']);
				}

				$data['seller']['approved'] = 0;
				break;

			case MsSeller::STATUS_ACTIVE:
				if ($seller['ms.seller_status'] == MsSeller::STATUS_INACTIVE && $this->config->get('msconf_allow_inactive_seller_products')) {
					$products = $this->MsLoader->MsProduct->getProducts(array(
						'seller_id' => $seller['seller_id']
					));

					foreach ($products as $p) {
						$this->MsLoader->MsProduct->changeStatus($p['product_id'], $data['seller']['status']);
						if ($data['seller_setting']['slr_product_validation'] == MsProduct::MS_PRODUCT_VALIDATION_NONE) {
							$this->MsLoader->MsProduct->approve($p['product_id']);
						}
					}
				}

				$data['seller']['approved'] = 1;
				break;
		}

		$seller_data = array(
			'avatar_name' => isset($data['seller']['avatar']) ? $data['seller']['avatar'] : '',
			'banner_name' => isset($data['seller']['banner']) ? $data['seller']['banner'] : '',
			'seller_id' => $data['seller']['seller_id'],
			'status' => $data['seller']['status'],
			'approved' => isset($data['seller']['approved']) ? $data['seller']['approved'] : 1,
			'seller_group' => $data['seller']['seller_group'],
			'nickname' => $data['seller']['nickname'],
			'languages' => isset($data['seller']['languages']) ? $data['seller']['languages'] : array(),
			'settings' => array(
				'slr_product_validation' => isset($data['seller_setting']['slr_product_validation']) ? $data['seller_setting']['slr_product_validation'] : '',
				'slr_website' => isset($data['seller_setting']['slr_website']) ? $data['seller_setting']['slr_website'] : '',
				'slr_company' => isset($data['seller_setting']['slr_company']) ? $data['seller_setting']['slr_company'] : '',
				'slr_phone' => isset($data['seller_setting']['slr_phone']) ? $data['seller_setting']['slr_phone'] : ''
			),
			'address' => isset($data['seller']['address']) ? $data['seller']['address'] : array(),
			'badges' => isset($data['seller']['badges']) ? $data['seller']['badges'] : array(),
			'keyword' => !empty($data['seller']['keyword']) ? $this->MsLoader->MsHelper->slugify($data['seller']['keyword']) : '',
			'commission_id' => !empty($data['seller']['commission_id']) ? $data['seller']['commission_id'] : null,
			'commission' => !empty($data['seller']['commission']) ? $data['seller']['commission'] : [],
			'msf_seller_properties' => !empty($data['msf_seller_properties']) ? $data['msf_seller_properties'] : []
		);

		$this->MsLoader->MsSeller->editSeller($seller_data);
		$this->MsLoader->MsSeller->editSellerMsAddress($seller_data);

		// Trigger notification if seller status is changed
		if ((int)$seller['ms.seller_status'] !== (int)$seller_data['status']) {
			$this->MsHooks->triggerAction('admin_updated_seller_status', [
				'producer' => "admin.0",
				'consumers' => ["seller.{$data['seller_id']}"],
				'object' => [
					'type' => 'account',
					'subtype' => 'status',
					'id' => $data['seller_id'],
					'action' => 'updated',
					'metadata' => ['seller_status_id' => $data['seller']['status']]
				]
			]);
		}

		/*if (!empty($data['seller']['notify']))
			$mailTransport->sendMails($mails);*/
	}

	private function _validateFormFields(&$data)
	{
		$errors = array();
		$validator = $this->MsLoader->MsValidator;

		$defaultLanguageId = $this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'));
		$languages = $this->model_localisation_language->getLanguages();

		// Validate primary language
		foreach ($languages as $language) {
			$language_id = $language['language_id'];
			$primary = (int)$language_id !== (int)$defaultLanguageId ? false : true;

			$data['seller']['languages'][$language_id]['slogan'] = trim($data['seller']['languages'][$language_id]['slogan']);
			$data['seller']['languages'][$language_id]['description'] = trim($data['seller']['languages'][$language_id]['description']);

			if (!empty($data['seller']['languages'][$language_id]['slogan'])) {
				// Slogan - max_len 255
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_catalog_sellerinfo_slogan'),
					'value' => html_entity_decode($data['seller']['languages'][$language_id]['slogan'])
				),
					array(
						array('rule' => 'max_len,255')
					)
				)) $errors["seller[languages][$language_id][slogan]"] = $validator->get_errors();
			}

			if (!empty($data['seller']['languages'][$language_id]['description'])) {
				// Description - max_len 5000
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_catalog_sellerinfo_description'),
					'value' => html_entity_decode($data['seller']['languages'][$language_id]['description'])
				),
					array(
						array('rule' => 'max_len,5000')
					)
				)) $errors["seller[languages][$language_id][description]"] = $validator->get_errors();
			}

			// Copy fields values from main language
			if (!$primary) {
				if (empty($data['seller']['languages'][$language_id]['slogan']) && !empty(trim($data['seller']['languages'][$defaultLanguageId]['slogan'])))
					$data['seller']['languages'][$language_id]['slogan'] = trim($data['seller']['languages'][$defaultLanguageId]['slogan']);

				if (empty($data['seller']['languages'][$language_id]['description']) && !empty(trim($data['seller']['languages'][$defaultLanguageId]['description'])))
					$data['seller']['languages'][$language_id]['description'] = trim($data['seller']['languages'][$defaultLanguageId]['description']);
			}
		}

		// Nickname - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_catalog_sellerinfo_nickname'),
			'value' => html_entity_decode($data['seller']['nickname'])
		),
			array(
				array('rule' => 'required'),
				array('rule' => 'min_len,4'),
				array('rule' => 'max_len,128'),
				array('rule' => (int)$this->config->get('msconf_nickname_rules') === 1 ? 'latin' : ((int)$this->config->get('msconf_nickname_rules') === 2 ? 'utf8' : 'alpha_numeric')),
			)
		)) $errors["seller[nickname]"] = $validator->get_errors();

		// If new customer must be created firstly
		if (empty($data['seller']['seller_id']) && empty($data['customer']['customer_id'])) {
			// Firstname - required
			if (!$validator->validate(array(
				'name' => $this->language->get('ms_catalog_sellerinfo_customer_firstname'),
				'value' => html_entity_decode($data['customer']['firstname'])
			),
				array(
					array('rule' => 'required'),
					array('rule' => 'max_len,64')
				)
			)) $errors["customer[firstname]"] = $validator->get_errors();

			// Lastname - required
			if (!$validator->validate(array(
				'name' => $this->language->get('ms_catalog_sellerinfo_customer_lastname'),
				'value' => html_entity_decode($data['customer']['lastname'])
			),
				array(
					array('rule' => 'required'),
					array('rule' => 'max_len,64')
				)
			)) $errors["customer[lastname]"] = $validator->get_errors();

			// Email - required
			if (!$validator->validate(array(
				'name' => $this->language->get('ms_catalog_sellerinfo_customer_email'),
				'value' => html_entity_decode($data['customer']['email'])
			),
				array(
					array('rule' => 'required'),
					array('rule' => 'email'),
					array('rule' => 'max_len,96')
				)
			)) $errors["customer[email]"] = $validator->get_errors();

			if (empty($errors["customer[email]"]) && $this->model_customer_customer->getCustomerByEmail($data['customer']['email'])) {
				$errors["customer[email]"] = $this->language->get('ms_validate_email_exists');
			}

			// Password - required if already registered customer creates seller account
			if (isset($data['customer']['password']) && isset($data['customer']['password_confirm'])) {
				// Password
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_catalog_sellerinfo_customer_password'),
					'value' => html_entity_decode($data['customer']['password'])
				),
					array(
						array('rule' => 'required'),
						array('rule' => 'min_len,4'),
						array('rule' => 'max_len,20')
					)
				)) $errors["customer[password]"] = $validator->get_errors();

				// Password confirm
				if (!$validator->validate(array(
					'name' => $this->language->get('ms_catalog_sellerinfo_customer_password_confirm'),
					'value' => html_entity_decode($data['customer']['password_confirm'])
				),
					array(
						array('rule' => 'required')
					)
				)) $errors["customer[password_confirm]"] = $validator->get_errors();

				if ($data['customer']['password_confirm'] != $data['customer']['password']) {
					$errors["customer[password_confirm]"] = $this->language->get('ms_validate_password_confirm');
				}
			}

			if (empty($errors["seller[nickname]"]) && $this->MsLoader->MsSeller->nicknameTaken($data['seller']['nickname'])) {
				$errors["seller[nickname]"] = $this->language->get('ms_error_sellerinfo_nickname_taken');
			}
		}

		// Country - required
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_seller_country'),
			'value' => !empty($data['seller']['address']['country_id']) ? html_entity_decode($data['seller']['address']['country_id']) : null
		),
			array(
				array('rule' => 'required')
			)
		)) $errors["seller[address][country_id]"] = $validator->get_errors();

		// Company name
		if (!$validator->validate(array(
			'name' => $this->language->get('ms_seller_country'),
			'value' => $data['seller_setting']['slr_company']
		),
			array(
				array('rule' => 'max_len,50')
			)
		)) $errors["seller_setting[slr_company]"] = $validator->get_errors();

		// Validate MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			if (!empty($data['msf_seller_properties'])) {
				foreach ($data['msf_seller_properties'] as $msf_seller_property_id => $values) {
					$msf_seller_property = $this->MsLoader->MsfSellerProperty->get($msf_seller_property_id);

					if (!empty($msf_seller_property['required'])) {
						if ('text' === $msf_seller_property['type']) {
							foreach ($languages as $language) {
								if (empty($values['text_values'][$language['language_id']])) {
									$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$language['language_id']}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
									break;
								} elseif (utf8_strlen($values['text_values'][$language['language_id']]) > 255) {
									$errors["msf_seller_properties[$msf_seller_property_id][text_values][{$language['language_id']}]"] = sprintf($this->language->get('ms_field_seller_property_error_field_length'), $msf_seller_property['name']);
									break;
								}
							}
						} elseif (empty($values['id_values'])) {
							$errors["msf_seller_properties[$msf_seller_property_id][id_values][]"] = sprintf($this->language->get('ms_field_seller_property_error_field_empty'), $msf_seller_property['name']);
						}
					}
				}
			}
		}

		return $errors;
	}

	public function jxGetSellerGroupCommission()
	{
		$seller_group_id = !empty($this->request->get['seller_group_id']) ? $this->request->get['seller_group_id'] : null;

		if (!$seller_group_id) {
			return $this->response->setOutput(json_encode(['success' => false, 'errors' => 'Invalid seller group id']));
		}

		$actual_fees = '';

		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroup($seller_group_id);

		if (!empty($seller_group['msg.commission_id'])) {
			$commission_rates = $this->MsLoader->MsCommission->getCommissionRates($seller_group['msg.commission_id']);

			foreach ($commission_rates as $rate) {
				if ($rate['rate_type'] == MsCommission::RATE_SIGNUP)
					continue;

				$actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . $rate['percent'] . '%+' . $this->currency->getSymbolLeft() .  $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', FALSE) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
			}
		}

		return $this->response->setOutput(json_encode(['success' => true, 'text' => sprintf($this->language->get('ms_seller_commission_override_off_note'), $this->MsLoader->MsSellerGroup->getSellerGroupName($seller_group_id), $actual_fees)]));
	}

	public function jxGetSellerMsfSellerProperties()
	{
		$seller_id = !empty($this->request->get['seller_id']) ? $this->request->get['seller_id'] : null;
		$seller_group_id = !empty($this->request->get['seller_group_id']) ? $this->request->get['seller_group_id'] : null;

		if (!$seller_group_id) {
			return $this->response->setOutput(json_encode(['success' => false, 'errors' => 'Invalid seller group id']));
		}

		$data = $this->load->language('multiseller/multiseller');

		// languages
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['msf_seller_properties'] = [
			'default' => $this->MsLoader->MsfSellerProperty->getSellerGroupMsfSellerProperties($seller_group_id, true),
			'existing' => $this->MsLoader->MsfSellerProperty->getSellerMsfSellerProperties($seller_id)
		];

		return $this->response->setOutput(json_encode([
			'success' => true,
			'html' => $this->load->view('multiseller/seller-form-msf-seller-property', $data)
		]));
	}
}
