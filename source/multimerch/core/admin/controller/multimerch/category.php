<?php

class ControllerMultimerchCategory extends ControllerMultimerchBase {

	public function index() {
		$this->validate(__FUNCTION__);

		$this->document->addScript('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
		$this->document->addScript('view/javascript/multimerch/category.js');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading'] = $this->language->get('ms_seller_category_heading');
		$this->document->setTitle($this->language->get('ms_seller_category_heading'));

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_seller_category_breadcrumbs'),
				'href' => $this->url->link('multimerch/category', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('multiseller/category', $this->data));
	}


	/************************************************************/


	// MultiMerch Categories

	public function getMsCategoryTableData() {
		$colMap = array(
			'id' => 'msc.category_id',
			'name' => '`full_path`',
			'seller' => 'mss.nickname',
			'status' => 'msc.category_status'
		);

		$sorts = array('name', 'seller', 'status');
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$statuses = array();
		$msCategory = new ReflectionClass('MsCategory');
		foreach ($msCategory->getConstants() as $cname => $cval) {
			if (strpos($cname, 'STATUS_') !== FALSE) {
				$statuses[] = $cval;
			}
		}

		$results = $this->MsLoader->MsCategory->getCategories(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'`full_path`' => 1
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// status
			$status = "";
			if(isset($result['category_status'])) {
				$status .= "<p style='color: ";

				if($result['category_status'] == MsCategory::STATUS_ACTIVE) $status .= "green";
				if($result['category_status'] == MsCategory::STATUS_INACTIVE || $result['category_status'] == MsCategory::STATUS_DISABLED) $status .= "red";

				$status .= "'>" . $this->language->get('ms_seller_category_status_' . $result['category_status']) . "</p>";
			}

			// actions
			$actions = "";
			if($result['category_status'] == MsCategory::STATUS_DISABLED) {
				$actions .= "<button type='button' class='btn btn-success ms-cat-change-status ms-spinner' data-status='" . MsCategory::STATUS_ACTIVE . "' data-toggle='tooltip' title='' data-original-title='" . $this->language->get('ms_button_approve') . "'><i class='fa fa-check'></i></button>";
			}
			$actions .= "<a class='btn btn-info' target='_blank' href='" . ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . "index.php?route=seller/catalog-seller/products&seller_id=" . $result['seller_id'] . "&ms_category_id=" . $result['category_id'] . "' data-toggle='tooltip' title='".$this->language->get('ms_categories_view_mscategory_front_page')."'><i class='fa fa-search'></i></a> ";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'], 'SSL') . "' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil''></i></a>";
			$actions .= "<button type='button' class='btn btn-danger ms-delete' title='".$this->language->get('button_delete')."' data-id='" . $result['category_id'] . "' data-referrer='category'><i class='fa fa-trash-o'></i></button>";

			$columns[] = array_merge(
				$result,
				array(
					'checkbox' => "<input type='checkbox' name='selected[]' value='{$result['category_id']}' />",
					'name' => ($result['path'] ? $result['path'] . '&nbsp;&nbsp;&gt;&nbsp;&nbsp;' : '') . $result['name'],
					'seller' => $this->MsLoader->MsSeller->getSellerNickname($result['seller_id']),
					'status' => $status,
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function create() {
		if (!$this->config->get('msconf_allow_seller_categories')) {
			return $this->response->redirect($this->url->link('multimerch/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->document->setTitle($this->language->get('ms_seller_newcategory_heading'));
		$this->_initCategoryForm();

		$this->data['category'] = FALSE;
		$this->data['heading'] = $this->language->get('ms_seller_newcategory_heading');

		$this->response->setOutput($this->load->view('multiseller/category-form', $this->data));
	}

	public function update() {
		if (!$this->config->get('msconf_allow_seller_categories')) {
			return $this->response->redirect($this->url->link('multimerch/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$category_id = isset($this->request->get['category_id']) ? (int)$this->request->get['category_id'] : 0;

		if ($this->MsLoader->MsCategory->isMsCategory($category_id)) {
			$category = $this->MsLoader->MsCategory->getCategories(array('category_id' => $category_id, 'single' => 1));
		} else {
			return $this->response->redirect($this->url->link('multimerch/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->document->setTitle($this->language->get('ms_seller_editcategory_heading'));
		$this->_initCategoryForm();

		// name, description, metas
		foreach ($category['languages'] as $id => $l) {
			$category['cat_name'][$id] = $l['name'];
			$category['cat_description'][$id] = ($this->config->get('msconf_enable_rte') ? htmlspecialchars_decode($l['description']) : strip_tags(htmlspecialchars_decode($l['description'])));
			$category['cat_meta_keyword'][$id] = $l['meta_keyword'];
			$category['cat_meta_description'][$id] = $l['meta_description'];
			$category['cat_meta_title'][$id] = $l['meta_title'];
		}

		// image
		if($category['image']) $this->data['thumb'] = $this->model_tool_image->resize($category['image'], 100, 100);

		$this->data['category'] = $category;
		$this->data['heading'] = $this->language->get('ms_seller_editcategory_heading');

		$this->response->setOutput($this->load->view('multiseller/category-form', $this->data));
	}

	public function jxSaveCategory() {
		if (!$this->config->get('msconf_allow_seller_categories')) {
			return $this->response->redirect($this->url->link('multimerch/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$json = array();

		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();
		$validator = $this->MsLoader->MsValidator;

		$data = $this->request->post;

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$defaultLanguageId = $this->config->get('config_language_id');

		// validate primary language
		foreach ($languages as $language) {
			$language_id = $language['language_id'];
			$primary = true;
			if($language_id != $defaultLanguageId)
				$primary = false;

			// validate category name
			if(!$validator->validate(array(
				'name' => $this->language->get('ms_seller_category_name'),
				'value' => $data['category_description'][$language_id]['name']
			),
				array(
					!$primary ? array() : array('rule' => 'required'),
					array('rule' => 'max_len,100')
				)
			)) $json['errors']["category_description[$language_id][name]"] = $validator->get_errors();

			if(!$primary) {
				if (empty($data['category_description'][$language_id]['name'])) $data['category_description'][$language_id]['name'] = $data['category_description'][$defaultLanguageId]['name'];
			}
		}

		if (!empty($json['errors'])) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			return;
		}

		if (empty($data['category_id'])) {
			if(!isset($data['status'])) $data['status'] = MsCategory::STATUS_ACTIVE;

			// create new category
			$this->MsLoader->MsCategory->createCategory($data);

			// @todo mails

			$this->session->data['success'] = $this->language->get('ms_seller_category_created');
		} else {
			// update existing category
			$this->MsLoader->MsCategory->updateCategory($data['category_id'], $data);

			// @todo mails

			$this->session->data['success'] = $this->language->get('ms_seller_category_updated');
		}

		$json['redirect'] = $this->url->link('multimerch/category', 'token=' . $this->session->data['token'], true);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function _initCategoryForm() {
		$this->document->addScript('view/javascript/summernote/summernote.js');
		$this->document->addScript('view/javascript/summernote/opencart.js');
		$this->document->addStyle('view/javascript/summernote/summernote.css');

		$this->document->addScript('view/javascript/multimerch/category-form.js');

		// languages
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// sellers
		// @todo error message if no sellers
		$this->data['sellers'] = $this->MsLoader->MsSeller->getSellers(
			array(
				'seller_status' => array(MsSeller::STATUS_ACTIVE, MsSeller::STATUS_INACTIVE)
			),
			array(
				'order_by'  => 'ms.nickname',
				'order_way' => 'ASC'
			)
		);

		// stores
		$this->load->model('setting/store');
		$this->data['stores'] = $this->model_setting_store->getStores();

		// image
		$this->load->model('tool/image');
		$this->data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		$this->data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		// statuses
		$statuses = array();
		$msCategory = new ReflectionClass('MsCategory');
		foreach ($msCategory->getConstants() as $cname => $cval) {
			if (strpos($cname, 'STATUS_') !== FALSE) {
				$statuses[] = $cval;
			}
		}
		$this->data['statuses'] = $statuses;

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_seller_category_breadcrumbs'),
				'href' => $this->url->link('multimerch/category', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
	}

	public function jxChangeSeller() {
		$json = array();

		if(!isset($this->request->get['category_id']) && !isset($this->request->get['seller_id'])) {
			$json['error'] = $this->language->get('ms_seller_category_error_updating');
		}

		if(!isset($json['error'])) {
			$this->MsLoader->MsCategory->changeSeller($this->request->get['category_id'], $this->request->get['seller_id']);

			$this->session->data['success'] = $this->language->get('ms_seller_category_updated');
			$json['redirect'] = $this->url->link('multimerch/category', 'token=' . $this->session->data['token'], true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxChangeStatus() {
		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$json = array();

		if(!isset($this->request->get['category_id']) && !isset($this->request->post['selected_categories']) && !isset($this->request->get['category_status'])) {
			$json['error'] = $this->language->get('ms_seller_category_error_updating');
		}

		if(!isset($json['error'])) {
			$category_ids = isset($this->request->get['category_id']) ?
				array($this->request->get['category_id']) :
				(isset($this->request->post['selected_categories']) ? $this->request->post['selected_categories'] : array());

			foreach ($category_ids as $category_id) {
				$category_info = $this->MsLoader->MsCategory->getCategories(array('category_id' => $category_id, 'single' => 1));
				$seller = isset($category_info['seller_id']) ? $this->MsLoader->MsSeller->getSeller($category_info['seller_id']) : FALSE;

				$category_status = $this->request->get['category_status'];

				$status = "<p style='color: ";

				if($category_status == MsCategory::STATUS_ACTIVE) $status .= "green";
				if($category_status == MsCategory::STATUS_INACTIVE || $category_status == MsCategory::STATUS_DISABLED) $status .= "red";

				$status .= "'>" . $this->language->get('ms_seller_category_status_' . $category_status) . "</p>";

				$json['category_status'][$category_id] = $status;

				if ($seller) {
					$MailCategoryStatusChanged = $serviceLocator->get('MailCategoryStatusChanged', false)
						->setTo($seller['c.email'])
						->setData(array(
							'addressee' => $seller['ms.nickname'],
							'cat_name' => $category_info['name'],
							'cat_status' => $this->language->get('ms_seller_category_status_' . $this->request->get['category_status'])
						));
					$mails->add($MailCategoryStatusChanged);
				}

				$this->MsLoader->MsCategory->changeStatus($category_id, $this->request->get['category_status']);
			}

			if ($mails->count()) {
				$mailTransport->sendMails($mails);
			}

			$this->session->data['success'] = $this->language->get('ms_seller_category_updated');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxDeleteCategory() {
		$json = array();

		if(!isset($this->request->get['category_id']) && !isset($this->request->post['selected'])) {
			$json['error'] = $this->language->get('ms_seller_category_error_deleting');
		}

		if(!isset($json['error'])) {
			$category_ids = isset($this->request->get['category_id']) ?
				array($this->request->get['category_id']) :
				(isset($this->request->post['selected']) ? $this->request->post['selected'] : array());

			foreach ($category_ids as $category_id) {
				$this->MsLoader->MsCategory->deleteCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('ms_seller_category_deleted');
			$json['redirect'] = $this->url->link('multimerch/category', 'token=' . $this->session->data['token'] . '#tab-mscategories', true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxAutocompleteCategories() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/category');

			$data = array(
				'seller_ids' => $this->customer->getId(),
				'category_status' => MsCategory::STATUS_ACTIVE
			);

			$filter_data = array(
				'filters' => array(
					'mscd.name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))
				),
				'order_by' => 'mscd.name',
				'order_way' => 'ASC',
				'offset' => 0,
				'limit' => 5
			);

			$results = $this->MsLoader->MsCategory->getCategories($data, $filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode(($result['path'] ? $result['path'] . '&nbsp;&nbsp;&gt;&nbsp;&nbsp;' : '') . $result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxAutocompleteFilters() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/filter');

			$filter_data = array(
				'filter_name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8')),
				'start'       => 0,
				'limit'       => 5
			);

			$filters = $this->model_catalog_filter->getFilters($filter_data);

			foreach ($filters as $filter) {
				$json[] = array(
					'filter_id' => $filter['filter_id'],
					'name'      => strip_tags(html_entity_decode($filter['group'] . ' &gt; ' . $filter['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	// OpenCart Categories

	public function getOcCategoryTableData() {
		$colMap = array(
			'id' => 'c.category_id',
			'name' => '`full_path`',
			'status' => 'c.status'
		);

		$sorts = array('name', 'status');
		$filters = $sorts;

		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		if (isset($filterParams['c.status'])){
			$filterParams['c.status']-= 1;
		}

		$results = $this->MsLoader->MsCategory->getOcCategories(
			array(),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'filters' => $filterParams,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			),
			array(
				'`full_path`' => 1
			)
		);

		$total = isset($results[0]) ? $results[0]['total_rows'] : 0;

		$columns = array();
		foreach ($results as $result) {
			// status
			if($result['status']) {
				$status = "<p style='color: green'>" . $this->language->get('ms_enabled') . "</p>";
			}else{
				$status = "<p style='color: red'>" . $this->language->get('ms_disabled') . "</p>";
			}

			// actions
			$actions = "";
			$actions .= "<a class='btn btn-info' target='_blank' href='" . ($this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG) . "index.php?route=product/category&path=" . $result['full_path_ids'] . "' data-toggle='tooltip' title='".$this->language->get('ms_categories_view_occategory_front_page')."'><i class='fa fa-search'></i></a> ";
			$actions .= "<a class='btn btn-primary' href='" . $this->url->link('multimerch/category/updateOc', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'], 'SSL') . "' title='".$this->language->get('button_edit')."'><i class='fa fa-pencil''></i></a>";
			$actions .= "<button type='button' class='btn btn-danger ms-delete' title='".$this->language->get('button_delete')."' data-id='" . $result['category_id'] . "' data-referrer='occategory'><i class='fa fa-trash-o'></i></button>";

			$columns[] = array_merge(
				$result,
				array(
					'name' => ($result['path'] ? $result['path'] . '&nbsp;&nbsp;&gt;&nbsp;&nbsp;' : '') . $result['name'],
					'status' => $status,
					'actions' => $actions
				)
			);
		}

		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}

	public function createOc() {
		$this->document->setTitle($this->language->get('ms_oc_newcategory_heading'));
		$this->_initOcCategoryForm();

		$this->data['category'] = FALSE;
		$this->data['heading'] = $this->language->get('ms_oc_newcategory_heading');

		$this->response->setOutput($this->load->view('multiseller/category-oc-form', $this->data));
	}

	public function updateOc(){
		$this->load->model('catalog/category');
		$category_id = isset($this->request->get['category_id']) ? (int)$this->request->get['category_id'] : 0;

		if ($category = $this->MsLoader->MsCategory->getOcCategories(array('category_id'=>$category_id))) {
			$category = reset($category);

			$languages = $this->model_catalog_category->getCategoryDescriptions($category_id);
			foreach ($languages as $id => $l) {
				$category['cat_name'][$id] = $l['name'];
				$category['cat_description'][$id] = ($this->config->get('msconf_enable_rte') ? htmlspecialchars_decode($l['description']) : strip_tags(htmlspecialchars_decode($l['description'])));
				$category['cat_meta_keyword'][$id] = $l['meta_keyword'];
				$category['cat_meta_description'][$id] = $l['meta_description'];
				$category['cat_meta_title'][$id] = $l['meta_title'];
			}

		} else {
			return $this->response->redirect($this->url->link('multimerch/category', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->document->setTitle($this->language->get('ms_oc_editcategory_heading'));

		$this->_initOcCategoryForm();

		// Prepare category image
		$this->load->model('tool/image');
		if (!empty($category['image']) && is_file(DIR_IMAGE . $category['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($category['image'], 100, 100);
		}

		$this->data['category'] = $category;
		$this->data['heading'] = $this->language->get('ms_oc_editcategory_heading');
		$this->response->setOutput($this->load->view('multiseller/category-oc-form', $this->data));
	}

	public function jxSaveOcCategory(){
		$json = array();

		$this->load->model('localisation/language');
		$this->load->model('catalog/category');

		list($json['errors'], $data) = $this->_validateOcCategoryForm();

		if (!empty($json['errors'])) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			return;
		}

		// Commissions
		if (!empty($data['category_ms_fee']['commission_rates'])) {
			foreach ($data['category_ms_fee']['commission_rates'] as $type => $rate) {
				if (empty($rate['flat']) && empty($rate['percent'])) {
					unset($data['category_ms_fee']['commission_rates'][$type]);
				}
			}
		}

		if (empty($data['category_id'])) {
			$this->MsLoader->MsCategory->createOcCategory($data);
			$this->session->data['success'] = $this->language->get('ms_seller_category_created');
		} else {
			$this->MsLoader->MsCategory->updateOcCategory($data['category_id'], $data);
			$this->session->data['success'] = $this->language->get('ms_seller_category_updated');
		}

		$json['redirect'] = $this->url->link('multimerch/category', 'token=' . $this->session->data['token'], true);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function _initOcCategoryForm() {
		$this->document->addScript('view/javascript/summernote/summernote.js');
		$this->document->addScript('view/javascript/summernote/opencart.js');
		$this->document->addStyle('view/javascript/summernote/summernote.css');

		$this->document->addScript('view/javascript/multimerch/category-oc-form.js');
		$this->document->addScript('view/javascript/multimerch/jquery/jquery-ui.min.js');
		$this->document->addScript('view/javascript/common.js');

		// languages
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		// stores
		$this->load->model('setting/store');
		$this->data['stores'] = $this->model_setting_store->getStores();

		// image
		$this->load->model('tool/image');
		$this->data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		$this->data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		// Prodct filter blocks status
		$this->load->model('setting/setting');
		$mspf_settings = $this->model_setting_setting->getSetting('multimerch_productfilter');
		$this->data['mspf_blocks_enabled'] = !empty($mspf_settings['multimerch_productfilter_status']);

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('multimerch/dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_seller_category_breadcrumbs'),
				'href' => $this->url->link('multimerch/category', '', 'SSL'),
			)
		));

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');
	}

	private function _validateOcCategoryForm() {
		$data = $this->request->post;
		$errors = false;

		foreach ($data['category_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
				$errors["category_description[$language_id][name]"] = $this->language->get('ms_oc_category_error_name');
			}

		}

		if (isset($data['category_id']) && $data['parent_id']) {
			$results = $this->model_catalog_category->getCategoryPath($data['parent_id']);

			foreach ($results as $result) {
				if ($result['path_id'] == $data['category_id']) {
					$errors["parent_id"] = $this->language->get('ms_oc_category_error_parent');
					break;
				}
			}
		}

		if (utf8_strlen($data['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($data['keyword']);

			if ($url_alias_info && isset($data['category_id']) && $url_alias_info['query'] != 'category_id=' . $data['category_id']) {
				$errors['keyword'] = sprintf($this->language->get('ms_oc_category_error_keyword'));
			}

			if ($url_alias_info && !isset($data['category_id'])) {
				$errors['keyword'] = sprintf($this->language->get('ms_oc_category_error_keyword'));
			}
		}


		return [$errors, $data];
	}

	public function jxDeleteOcCategory() {
		$json = array();

		if(!isset($this->request->get['category_id']) && !isset($this->request->post['selected'])) {
			$json['error'] = $this->language->get('ms_seller_category_error_deleting');
		}

		if(!isset($json['error'])) {
			$this->load->model('catalog/category');
			$category_ids = isset($this->request->get['category_id']) ?
				array($this->request->get['category_id']) :
				(isset($this->request->post['selected']) ? $this->request->post['selected'] : array());

			foreach ($category_ids as $category_id) {
				$this->model_catalog_category->deleteCategory($category_id);
			}

			$this->session->data['success'] = $this->language->get('ms_seller_category_deleted');
			$json['redirect'] = $this->url->link('multimerch/category', 'token=' . $this->session->data['token'] . '#tab-occategories', true);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxAutocompleteOcCategories() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/category');

			$filter_data = array(
				'filter_name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8')),
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_category->getCategories($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetOcCategoryCommission()
	{
		$category_id = !empty($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
		$parent_id = !empty($this->request->get['parent_id']) ? $this->request->get['parent_id'] : null;

		$data = $this->load->language('multiseller/multiseller');

		if ($category_id) {
			// Edit category
			list($rates, $commissioned_category_id) = $this->MsLoader->MsCategory->getOcCategoryFeeRates($category_id, true);

			$actual_fees = '';
			foreach ($rates as $rate) {
				if ($rate['rate_type'] == MsCommission::RATE_SIGNUP) {
					continue;
				}

				$actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . $rate['percent'] . '%+' . $this->currency->getSymbolLeft() .  $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', false) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
			}

			if ($parent_id) {
				// If parent selected, so the fallback should be displayed
				list($p_rates, $p_commissioned_category_id) = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates([$parent_id], MsCommission::RATE_LISTING, 1, true);

				// If selected parent category (or one of its parents) has commission rates
				$p_actual_fees = '';
				foreach ($p_rates as $rate) {
					if ($rate['rate_type'] == MsCommission::RATE_SIGNUP) {
						continue;
					}

					$p_actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . $rate['percent'] . '%+' . $this->currency->getSymbolLeft() . $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', false) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
				}

				$data = array_merge($data, [
					'override' => [
						'off' => sprintf($this->language->get('ms_oc_category_commission_override_off_note'), $this->url->link('multimerch/category/updateOc', 'token=' . $this->session->data['token'] . '&category_id=' . $p_commissioned_category_id), $this->MsLoader->MsCategory->getOcCategoryName($p_commissioned_category_id, $this->config->get('config_language_id')), $p_actual_fees),
						'on' => sprintf($this->language->get('ms_oc_category_commission_override_on_note'), $this->url->link('multimerch/category/updateOc', 'token=' . $this->session->data['token'] . '&category_id=' . $p_commissioned_category_id), $this->MsLoader->MsCategory->getOcCategoryName($p_commissioned_category_id, $this->config->get('config_language_id')))
					],
					// If category has commissions and override applied, show also form
					'form' => !empty($rates) && (int)$category_id === (int)$commissioned_category_id ? ['rates' => $rates] : false
				]);

			} else {
				// Only rates, fallback should not be displayed
				$data = array_merge($data, [
					'override' => [
						'off' => false,
						'on' => false
					],
					'form' => ['rates' => $rates]
				]);
			}
		} elseif ($parent_id) {
			// New category with selected parent
			list($rates, $commissioned_category_id) = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates([$parent_id], MsCommission::RATE_LISTING, 1, true);

			$actual_fees = '';
			foreach ($rates as $rate) {
				if ($rate['rate_type'] == MsCommission::RATE_SIGNUP) {
					continue;
				}

				$actual_fees .= '<span class="fee-rate-' . $rate['rate_type'] . '"><b>' . $this->language->get('ms_commission_short_' . $rate['rate_type']) . ':</b>' . $rate['percent'] . '%+' . $this->currency->getSymbolLeft() .  $this->currency->format($rate['flat'], $this->config->get('config_currency'), '', false) . $this->currency->getSymbolRight() . '&nbsp;&nbsp;';
			}

			if ($commissioned_category_id) {
				// Parent category has commissions
				$data = array_merge($data, [
					'override' => [
						'off' => sprintf($this->language->get('ms_oc_category_commission_override_off_note'), $this->url->link('multimerch/category/updateOc', 'token=' . $this->session->data['token'] . '&category_id=' . $commissioned_category_id), $this->MsLoader->MsCategory->getOcCategoryName($commissioned_category_id, $this->config->get('config_language_id')), $actual_fees),
						'on' => sprintf($this->language->get('ms_oc_category_commission_override_on_note'), $this->url->link('multimerch/category/updateOc', 'token=' . $this->session->data['token'] . '&category_id=' . $commissioned_category_id), $this->MsLoader->MsCategory->getOcCategoryName($commissioned_category_id, $this->config->get('config_language_id')))
					],
					'form' => false
				]);
			} else {
				// Parents have no commissions
				$data = array_merge($data, [
					'override' => [
						'off' => false,
						'on' => false
					],
					'form' => ['rates' => false]
				]);
			}
		} else {
			// New category without selected parent
			$data = array_merge($data, [
				'override' => [
					'off' => false,
					'on' => false
				],
				'form' => ['rates' => false]
			]);
		}

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/category-oc-form-commission', $data)]));
	}

	public function jxAutocompleteMsfAttributes() {
		$json = [];

		if (isset($this->request->get['filter_name'])) {
			$added_ids = !empty($this->request->get['added_ids']) ? explode(',', $this->request->get['added_ids']): [];

			$results = $this->MsLoader->MsfAttribute->getList(['not_in_ids' => $added_ids], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC',
				'filters' => ['name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))],
				'offset' => 0,
				'limit' => 10
			]);

			foreach ($results as $result) {
				$json[] = [
					'msf_attribute_id'  => $result['id'],
					'label'        		=> strip_tags(html_entity_decode($result['full_name'], ENT_QUOTES, 'UTF-8'))
				];
			}
		}

		$sort_order = [];

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['label'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetOcCategoryMsfAttributes()
	{
		$category_id = !empty($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
		$parent_id = !empty($this->request->get['parent_id']) ? $this->request->get['parent_id'] : null;
		$action = !empty($this->request->get['action']) ? $this->request->get['action'] : 'initial';

		$data = $this->load->language('multiseller/multiseller');

		// Initially set default attributes
		$data['msf_attributes'] = $this->MsLoader->MsfAttribute->getList(['default' => true], [
			'order_by'  => 'sort_order',
			'order_way' => 'ASC',
		]);

		// Whether attributes overridden
		$data['is_overridden'] = false;

		$data['messages'] = [
			'override' => $this->language->get('ms_field_attribute_message_default'),
			'remove_override' => $this->language->get('ms_field_attribute_message_override'),
		];

		switch ($action) {
			case 'override':
				if ($category_id) {
					// Existing category

					list($current_msf_attributes_ids, $final_category_id) = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($category_id, true);

					if (!empty($current_msf_attributes_ids) && $final_category_id === $category_id) {
						// Existing category, that has attributes previously overridden

						$msf_attributes = [];
						foreach ($current_msf_attributes_ids as $msf_attribute_id) {
							$msf_attributes[] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
						}

						$data['msf_attributes'] = $msf_attributes;
					} elseif ($parent_id) {
						// Existing category, that has parent, and that has no attributes previously
						// So we take attributes from parent

						list($parent_msf_attributes_ids, $final_category_id) = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($parent_id, true);

						if (!empty($parent_msf_attributes_ids)) {
							$msf_attributes = [];
							foreach ($parent_msf_attributes_ids as $msf_attribute_id) {
								$msf_attributes[] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
							}

							$data['msf_attributes'] = $msf_attributes;

							$data['messages']['override'] = sprintf($this->language->get('ms_field_attribute_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				} elseif ($parent_id) {
					// New category, that has parent
					list($parent_msf_attributes_ids, $final_category_id) = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($parent_id, true);

					if (!empty($parent_msf_attributes_ids)) {
						$msf_attributes = [];
						foreach ($parent_msf_attributes_ids as $msf_attribute_id) {
							$msf_attributes[] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
						}

						$data['msf_attributes'] = $msf_attributes;

						$data['messages']['override'] = sprintf($this->language->get('ms_field_attribute_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}

				$data['is_overridden'] = true;
				break;

			case 'remove_override':
			case 'parent_changed':
				if ($parent_id) {
					// Check if parent has attributes

					list($parent_msf_attributes_ids, $final_category_id) = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($parent_id, true);

					if (!empty($parent_msf_attributes_ids)) {
						$msf_attributes = [];
						foreach ($parent_msf_attributes_ids as $msf_attribute_id) {
							$msf_attributes[] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
						}

						$data['msf_attributes'] = $msf_attributes;

						$data['messages']['override'] = sprintf($this->language->get('ms_field_attribute_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}
				break;

			case 'initial':
			default:
				if ($category_id) {
					// Existing category

					list($current_msf_attributes_ids, $final_category_id) = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($category_id, true);

					if (!empty($current_msf_attributes_ids)) {
						$msf_attributes = [];
						foreach ($current_msf_attributes_ids as $msf_attribute_id) {
							$msf_attributes[] = $this->MsLoader->MsfAttribute->get($msf_attribute_id);
						}

						$data['msf_attributes'] = $msf_attributes;

						if ($final_category_id === $category_id) {
							$data['is_overridden'] = true;
						} else {
							$data['is_overridden'] = false;
							$data['messages']['override'] = sprintf($this->language->get('ms_field_attribute_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				}
				break;
		}

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/category-oc-form-msf-attribute', $data)]));
	}

	public function jxAutocompleteMsfVariations() {
		$json = [];

		if (isset($this->request->get['filter_name'])) {
			$added_ids = !empty($this->request->get['added_ids']) ? explode(',', $this->request->get['added_ids']): [];

			$results = $this->MsLoader->MsfVariation->getList(['not_in_ids' => $added_ids], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC',
				'filters' => ['name' => urldecode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'))],
				'offset' => 0,
				'limit' => 10
			]);

			foreach ($results as $result) {
				$json[] = [
					'msf_variation_id'  => $result['id'],
					'label'        		=> strip_tags(html_entity_decode($result['full_name'], ENT_QUOTES, 'UTF-8'))
				];
			}
		}

		$sort_order = [];

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['label'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function jxGetOcCategoryMsfVariations()
	{
		$category_id = !empty($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
		$parent_id = !empty($this->request->get['parent_id']) ? $this->request->get['parent_id'] : null;
		$action = !empty($this->request->get['action']) ? $this->request->get['action'] : 'initial';

		$data = $this->load->language('multiseller/multiseller');

		// Initially set default variations
		$data['msf_variations'] = $this->MsLoader->MsfVariation->getList(['default' => true], [
			'order_by'  => 'sort_order',
			'order_way' => 'ASC',
		]);

		// Whether variations overridden
		$data['is_overridden'] = false;

		$data['messages'] = [
			'override' => $this->language->get('ms_field_variation_message_default'),
			'remove_override' => $this->language->get('ms_field_variation_message_override'),
		];

		switch ($action) {
			case 'override':
				if ($category_id) {
					// Existing category

					list($current_msf_variations_ids, $final_category_id) = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($category_id, true);

					if (!empty($current_msf_variations_ids) && $final_category_id === $category_id) {
						// Existing category, that has variations previously overridden

						$msf_variations = [];
						foreach ($current_msf_variations_ids as $msf_variation_id) {
							$msf_variations[] = $this->MsLoader->MsfVariation->get($msf_variation_id);
						}

						$data['msf_variations'] = $msf_variations;
					} elseif ($parent_id) {
						// Existing category, that has parent, and that has no variations previously
						// So we take variations from parent

						list($parent_msf_variations_ids, $final_category_id) = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($parent_id, true);

						if (!empty($parent_msf_variations_ids)) {
							$msf_variations = [];
							foreach ($parent_msf_variations_ids as $msf_variation_id) {
								$msf_variations[] = $this->MsLoader->MsfVariation->get($msf_variation_id);
							}

							$data['msf_variations'] = $msf_variations;

							$data['messages']['override'] = sprintf($this->language->get('ms_field_variation_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				} elseif ($parent_id) {
					// New category, that has parent
					list($parent_msf_variations_ids, $final_category_id) = $this->MsLoader->MsfVariation->getOcCategoryMsfvariations($parent_id, true);

					if (!empty($parent_msf_variations_ids)) {
						$msf_variations = [];
						foreach ($parent_msf_variations_ids as $msf_variation_id) {
							$msf_variations[] = $this->MsLoader->MsfVariation->get($msf_variation_id);
						}

						$data['msf_variations'] = $msf_variations;

						$data['messages']['override'] = sprintf($this->language->get('ms_field_variation_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}

				$data['is_overridden'] = true;
				break;

			case 'remove_override':
			case 'parent_changed':
				if ($parent_id) {
					// Check if parent has variations

					list($parent_msf_variations_ids, $final_category_id) = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($parent_id, true);

					if (!empty($parent_msf_variations_ids)) {
						$msf_variations = [];
						foreach ($parent_msf_variations_ids as $msf_variation_id) {
							$msf_variations[] = $this->MsLoader->MsfVariation->get($msf_variation_id);
						}

						$data['msf_variations'] = $msf_variations;

						$data['messages']['override'] = sprintf($this->language->get('ms_field_variation_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}
				break;

			case 'initial':
			default:
				if ($category_id) {
					// Existing category

					list($current_msf_variations_ids, $final_category_id) = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($category_id, true);

					if (!empty($current_msf_variations_ids)) {
						$msf_variations = [];
						foreach ($current_msf_variations_ids as $msf_variation_id) {
							$msf_variations[] = $this->MsLoader->MsfVariation->get($msf_variation_id);
						}

						$data['msf_variations'] = $msf_variations;

						if ($final_category_id === $category_id) {
							$data['is_overridden'] = true;
						} else {
							$data['is_overridden'] = false;
							$data['messages']['override'] = sprintf($this->language->get('ms_field_variation_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				}
				break;
		}

		return $this->response->setOutput(json_encode(['success' => true, 'html' => $this->load->view('multiseller/category-oc-form-msf-variation', $data)]));
	}

	public function jxGetOcCategoryMspfBlocks()
	{
		$category_id = !empty($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
		$parent_id = !empty($this->request->get['parent_id']) ? $this->request->get['parent_id'] : null;
		$action = !empty($this->request->get['action']) ? $this->request->get['action'] : 'initial';

		$data = $this->load->language('multiseller/multiseller');

		// Initially set default blocks
		$data['mspf_blocks'] = $this->MsLoader->MsProductFilter->getDefaultMspfBlocks();

		// Whether blocks overridden
		$data['is_overridden'] = false;

		$data['messages'] = [
			'override' => $this->language->get('ms_product_filter_block_message_default'),
			'remove_override' => $this->language->get('ms_product_filter_block_message_override'),
		];

		switch ($action) {
			case 'override':
				if ($category_id) {
					// Existing category

					list($current_mspf_blocks, $final_category_id) = $this->MsLoader->MsProductFilter->getOcCategoryMspfBlocks($category_id, true);

					if (!empty($current_mspf_blocks) && $final_category_id === $category_id) {
						// Existing category, that has blocks previously overridden

						$data['mspf_blocks'] = $current_mspf_blocks;
					} elseif ($parent_id) {
						// Existing category, that has parent, and that has no blocks previously
						// So we take blocks from parent

						list($parent_mspf_blocks, $final_category_id) = $this->MsLoader->MsProductFilter->getOcCategoryMspfBlocks($parent_id, true);

						if (!empty($parent_mspf_blocks)) {
							$data['mspf_blocks'] = $parent_mspf_blocks;

							$data['messages']['override'] = sprintf($this->language->get('ms_product_filter_block_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				} elseif ($parent_id) {
					// New category, that has parent
					list($parent_mspf_blocks, $final_category_id) = $this->MsLoader->MsProductFilter->getOcCategoryMspfBlocks($parent_id, true);

					if (!empty($parent_mspf_blocks)) {
						$data['mspf_blocks'] = $parent_mspf_blocks;

						$data['messages']['override'] = sprintf($this->language->get('ms_product_filter_block_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}

				$data['is_overridden'] = true;
				break;

			case 'remove_override':
			case 'parent_changed':
				if ($parent_id) {
					// Check if parent has blocks

					list($parent_mspf_blocks, $final_category_id) = $this->MsLoader->MsProductFilter->getOcCategoryMspfBlocks($parent_id, true);

					if (!empty($parent_mspf_blocks)) {
						$data['mspf_blocks'] = $parent_mspf_blocks;

						$data['messages']['override'] = sprintf($this->language->get('ms_product_filter_block_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
					}
				}
				break;

			case 'initial':
			default:
				if ($category_id) {
					// Existing category

					list($current_mspf_blocks, $final_category_id) = $this->MsLoader->MsProductFilter->getOcCategoryMspfBlocks($category_id, true);

					if (!empty($current_mspf_blocks)) {
						$data['mspf_blocks'] = $current_mspf_blocks;

						if ($final_category_id === $category_id) {
							$data['is_overridden'] = true;
						} else {
							$data['is_overridden'] = false;
							$data['messages']['override'] = sprintf($this->language->get('ms_product_filter_block_message_inherited'), $this->MsLoader->MsCategory->getOcCategoryName($final_category_id, $this->config->get('config_language_id')));
						}
					}
				}
				break;
		}

		return $this->response->setOutput(json_encode([
			'success' => true,
			'html' => $this->load->view('multiseller/category-oc-form-mspf-block', $data)
		]));
	}
}
