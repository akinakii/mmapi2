<?php
class ControllerExtensionModuleLinnworks extends Controller {
	private $data = [
		'error' => []
	];

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->model('setting/setting');
		$this->data['settings'] = $this->model_setting_setting->getSetting('ms_linnworks');
	}

	public function index() {

		$this->data = array_merge($this->data, $this->language->load('extension/module/linnworks'));

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ($this->validate()) {
				$this->saveSettings($this->request->post);
				$this->session->data['success'] = $this->language->get('text_success');
				$this->response->redirect($this->url->link('extension/module/linnworks', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->data['settings'] = array_merge($this->data['settings'], $this->request->post['settings']);
			}
		}

		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_modules'),
				'href' => $this->url->link('extension/extension', 'type=module', 'SSL'),
			),
			array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/linnworks', '', 'SSL'),
			)
		));

		$this->data['manifest_content'] = $this->getManifest();
		$this->data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', 'SSL');
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view("extension/module/linnworks", $this->data));
	}

	protected function validate() {

		if ((utf8_strlen($this->request->post['settings']['ms_linnworks_channel_title']) < 5) || (utf8_strlen($this->request->post['settings']['ms_linnworks_channel_title']) > 50)) {
			$this->data['error']['ms_linnworks_channel_title'] = $this->language->get('error_channel_title');
		}

		if ((utf8_strlen($this->request->post['settings']['ms_linnworks_channel_name']) < 5) || (utf8_strlen($this->request->post['settings']['ms_linnworks_channel_name']) > 20)) {
			$this->data['error']['ms_linnworks_channel_name'] = $this->language->get('error_channel_name');
		}

		if ((utf8_strlen($this->request->post['settings']['ms_linnworks_channel_logo_url']) < 5)) {
			$this->data['error']['ms_linnworks_channel_logo_url'] = $this->language->get('error_channel_logo');
		}

		return !$this->data['error'];
	}

	protected function saveSettings($post) {
		$this->model_setting_setting->editSetting('ms_linnworks', $post['settings']);
	}

	protected function getManifest() {
		$base_url = HTTP_SERVER;
		$base_url = str_replace('admin/', '', $base_url);
		$ext_url = $base_url . 'index.php?route=extension/module/linnworks/';

		return <<<EOD
{
    "LinnworksPermissions": [
        "GlobalPermissions.Settings.ChannelIntegration.ChannelDetailsNode"
    ],
    "Modules": [
        {
            "RequiredPermissionId": null,
            "moduleName": "Channel integration module",
            "formattedName": null,
            "icon": null,
            "path": "",
            "type": "ChannelIntegration",
            "isHidden": false,
            "isBeta": false,
            "maxOpened": 0,
            "group": [],
            "parameters": [
                {
                    "name": "ChannelName",
                    "value": "{$this->data['settings']['ms_linnworks_channel_name']}"
                },
                {
                    "name": "ChannelFriendlyName",
                    "value": "{$this->data['settings']['ms_linnworks_channel_title']}"
                },
                {
                    "name": "ChannelLogoURL",
                    "value": "{$this->data['settings']['ms_linnworks_channel_logo_url']}"
                },
                {
                    "name": "AddNewUserEndpoint",
                    "value": "{$ext_url}AddNewUserEndpoint"
                },
                {
                    "name": "UserConfigEndpoint",
                    "value": "{$ext_url}UserConfigEndpoint"
                },
                {
                    "name": "SaveConfigEndpoint",
                    "value": "{$ext_url}SaveConfigEndpoint"
                },
                {
                    "name": "ShippingTagsEndpoint",
                    "value": "{$ext_url}ShippingTagsEndpoint"
                },
                {
                    "name": "PaymentTagsEndpoint",
                    "value": "{$ext_url}PaymentTagsEndpoint"
                },
                {
                    "name": "ConfigDeletedEndpoint",
                    "value": "{$ext_url}ConfigDeletedEndpoint"
                },
                {
                    "name": "ConfigTestEndpoint",
                    "value": "{$ext_url}ConfigTestEndpoint"
                },
                {
                    "name": "OrdersEndpoint",
                    "value": "{$ext_url}OrdersEndpoint"
                },
                {
                    "name": "DespatchEndpoint",
                    "value": "{$ext_url}DespatchEndpoint"
                },
                {
                    "name": "ProductsEndpoint",
                    "value": "{$ext_url}ProductsEndpoint"
                },
                {
                    "name": "InventoryUpdateEndpoint",
                    "value": "{$ext_url}InventoryUpdateEndpoint"
                },
                {
                    "name": "PriceUpdateEndpoint",
                    "value": "{$ext_url}PriceUpdateEndpoint"
                },
                {
                    "name": "GetConfiguratorSettingsEndpoint",
                    "value": "{$ext_url}GetConfiguratorSettingsEndpoint"
                },
                {
                    "name": "GetCategoriesEndpoint",
                    "value": "{$ext_url}GetCategoriesEndpoint"
                },
                {
                    "name": "GetAttributesByCategoryEndpoint",
                    "value": "{$ext_url}GetAttributesByCategoryEndpoint"
                },
                {
                    "name": "GetVariationsByCategoryEndpoint",
                    "value": "{$ext_url}GetVariationsByCategoryEndpoint"
                },
                {
                    "name": "ListingUpdateEndpoint",
                    "value": "{$ext_url}ListingUpdateEndpoint"
                },
                {
                    "name": "ListingDeleteEndpoint",
                    "value": "{$ext_url}ListingDeleteEndpoint"
                },
                {
                    "name": "CheckFeedEndpoint",
                    "value": "{$ext_url}CheckFeedEndpoint"
                },
                {
                    "name": "IsListingSupported",
                    "value": "True"
                },
                {
                    "name": "InventoryUpdateBatchSize",
                    "value": "50"
                },
                {
                    "name": "DespatchUpdateBatchSize",
                    "value": "50"
                },
                {
                    "name": "PriceUpdateBatchSize",
                    "value": "50"
                },
                {
                    "name": "ListingUpdateBatchSize",
                    "value": "50"
                },
                {
                    "name": "AllowRefundOnCancel",
                    "value": "True"
                }
            ],
            "files": {
                "scripts": [],
                "style": null,
                "styles": []
            }
        }
    ]
}
EOD;
	}

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ms_seller_linnworks` (
					`seller_id` int(11) unsigned NOT NULL,
					`lw_uid` varchar(255) NOT NULL DEFAULT '',
					`lw_token` varchar(255) NOT NULL DEFAULT ''
				) ENGINE=MyISAM DEFAULT CHARSET=utf8
			");
		$this->db->query("CREATE UNIQUE INDEX lw_uid ON `" . DB_PREFIX . "ms_seller_linnworks` (lw_uid)");

		$site_title = $this->model_setting_setting->getSettingValue('config_meta_title');
		$site_title_slug = $this->MsLoader->MsHelper->slugify($site_title);

		$settings = array(
			'ms_linnworks_enabled' => 0,
			'ms_linnworks_channel_title' => $site_title . ' integration channel',
			'ms_linnworks_channel_name' => $site_title_slug . '_multimerch_channel',
			'ms_linnworks_channel_logo_url' => '',
		);

		$this->model_setting_setting->editSetting('ms_linnworks', $settings);
	}

	public function uninstall() {
		$this->model_setting_setting->deleteSetting('ms_linnworks');
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "ms_seller_linnworks`");
	}

}