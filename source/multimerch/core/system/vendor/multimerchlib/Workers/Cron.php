<?php

require_once(__DIR__ . '/../Core/Worker.php');

if (!MsLoader::getInstance()->getRegistry()->get('config')->get('msconf_scheduled_import_enabled')) {
    echo 'Scheduled imports are disabled' . PHP_EOL;
    return false;
}

$model_ms_cron = MsLoader::getInstance()->ModelCron;
$model_product_import = MsLoader::getInstance()->ModelProductImport;

// Get all seller scheduled jobs
$jobs = $model_ms_cron->getList();

foreach ($jobs as $job) {
	// Check whether cron should run this time
	$should_run = false;

	$import_id = $job['import_id'];
	$cycle = str_replace('_', ' ', $job['cycle']);

	if ($job['status'] && empty($job['date_start'])) {
		// Job has not yet started
		$should_run = true;
	} elseif ($job['status'] && strtotime($job['date_next']) < (time() + 10)) {
		// Check if import takes longer than specified cycle
		if ($job['is_running']) {
			// If it is time to run again, but previous import is still running
			// We update the `date_next` field
			$new_date_next = strtotime('+' . $cycle, strtotime($job['date_next']));
			$model_ms_cron->update($job['cron_id'], [
				'date_next' => date('Y-m-d H:i:s', $new_date_next)
			]);

			$should_run = false;
		} else {
			// If import is not running right now
			$should_run = true;
		}
	}

	if ($should_run) {
		$import_data = $model_product_import->getImports(['import_id' => $import_id]);

		if (!empty($import_data)) {
			$type = $import_data['type'];
			$seller_id = $import_data['seller_id'];
			$config = array_merge($model_product_import->getCommonImportSettings($seller_id), $import_data['config']);

			$model_product_import->createImportHistory([
				'import_id' => $import_id,
				'status' => MM_FEED_CONST_IMPORT_STATUS_SCHEDULED
			]);

			$mapper = new \MultiMerch\Core\Feed\Mapper(MsLoader::getInstance()->getRegistry(), $type);
			$importer = new \MultiMerch\Core\Feed\Importer(MsLoader::getInstance()->getRegistry(), $type, $config, $mapper->getFields($seller_id, true));

			if ($import_data['is_scheduled']) {
				// Set next import date
				$new_date_start = date('Y-m-d H:i:s');
				$new_date_next = strtotime('+' . $cycle, strtotime($new_date_start));
				$model_ms_cron->update($job['id'], [
					'date_start' => $new_date_start,
					'date_next' => date('Y-m-d H:i:s', $new_date_next),
					'is_running' => true,
				]);
			} else {
				// Import is single - delete cron job
				$model_ms_cron->delete($job['id']);
			}

			$importer->start($import_id);
		} else {
			// Remove cron as import does not exist anymore
			$model_ms_cron->delete($job['id']);
		}
	}
}
