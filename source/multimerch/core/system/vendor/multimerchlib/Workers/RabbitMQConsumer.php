<?php

require_once(__DIR__ . '/../Core/Worker.php');

spl_autoload_register(function ($class) {
	$class = str_replace('\\', '/', $class);

	$file = DIR_SYSTEM . 'library/multimerch/' . $class . '.php';
	
	if (file_exists($file))
		require_once($file);
});

$host = 'localhost'; // $config->get('ms_config_rabbitmq_host');
$port = 5672;
$user = 'vlad';
$password = '123123';

$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection($host, $port, $user, $password);

$channel = $connection->channel();

$channel->exchange_declare('multimerch', 'direct', false, false, false);

list($queue_name, , $consumer_count) = $channel->queue_declare("", false, false, true, false);

// Stop script if number of consumers is bigger than 5
/*if ($consumer_count > 5) {
	exit;
}*/

// Bind admin routing key
$channel->queue_bind($queue_name, 'multimerch', "admin.0");

// Bind seller routing keys
$sellers = $registry->get('db')->query("SELECT seller_id FROM `" . DB_PREFIX . "ms_seller` WHERE seller_status = '1'");
foreach ($sellers->rows as $row) {
	$channel->queue_bind($queue_name, 'multimerch', "seller.{$row['seller_id']}");
}

// Bind customer routing keys
$customers = $registry->get('db')->query("SELECT customer_id FROM `" . DB_PREFIX . "customer` WHERE status = '1'");
foreach ($customers->rows as $row) {
	$channel->queue_bind($queue_name, 'multimerch', "customer.{$row['customer_id']}");
}

echo " [*] Waiting for logs. To exit press CTRL+C\n";

$channel->basic_consume($queue_name, '', false, false, false, false, function ($msg) {
	try {
		echo ' [x] ', $msg->delivery_info['routing_key'], ': ', $msg->body, "\n";
	} catch (\Exception $e) {
		echo $e->getMessage();
	}
});

while (count($channel->callbacks)) {
	$channel->wait();
}

$channel->close();
$connection->close();