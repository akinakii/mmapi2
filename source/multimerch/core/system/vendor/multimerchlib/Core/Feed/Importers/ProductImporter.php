<?php

namespace MultiMerch\Core\Feed\Importers;

use MultiMerch\Core\Feed\Importer;
use MultiMerch\Core\Feed\Parsers\CsvParser;

class ProductImporter extends Importer
{
	public function import($import_id = null)
	{
		$result = [];

		$statistics = [
			'total_rows' => 0,
			'inserted_rows' => 0,
			'updated_rows' => 0,
			'duplicated_rows' => 0,
			'new_rows' => 0,
		];

		$updated_ids = [];
		$product_ids = [];

		if (!empty($this->import_data['url_path'])) {
			// Retrieve import file by url
			$file_by_url_errors = $this->registry->get('MsLoader')->MsHelper->isFileByUrlValid($this->import_data['url_path'], true);

			if (!empty($file_by_url_errors)) {
				$this->registry->get('ms_logger')->error('Import (id #' . $import_id . ') Error: unable to retrieve file by url: ' . print_r($file_by_url_errors, true));
			} else {
				$url = $this->import_data['url_path'];
				$url_parts = explode('/', $url);
				$file_mask = end($url_parts);
				$file_name = time() . '_' . md5(rand()) . '.' . $this->registry->get('MsLoader')->MsSeller->getSellerNickname($this->import_data['seller_id']) . '_' . $file_mask;

				$file_contents = @file_get_contents($url);

				if ($file_contents === false) {
					$this->registry->get('ms_logger')->error('Import (id #' . $import_id . ') Error: ' . $this->registry->get('language')->get('ms_feed_error_source_file_invalid'));
				} else {
					file_put_contents(DIR_UPLOAD . $file_name, $file_contents);
					$filename = DIR_UPLOAD . $file_name;
				}
			}
		} elseif (!empty($this->import_data['attachment_code'])) {
			$filename = DIR_UPLOAD . $this->helper->getFilename($this->import_data['attachment_code']);
		} else {
			$this->registry->get('ms_logger')->error('Import (id #' . $import_id . ') Error: no url or local file is set');
			return false;
		}

		$start_row = $this->import_data['start_row'] ?? 2;
		$finish_row = $this->import_data['finish_row'] ?: false;
		$key_field = $this->import_data['key_field'] ?? 'model';
		$captions = $this->import_data['mapping'];

		$parser = new CsvParser($filename, $this->import_data['file_encoding'], $this->import_data['enclosure'], $this->import_data['delimiter']);
		$data_rows = $parser->parseData($captions, $start_row, $finish_row);

		$this->MsLoader->ModelProductImport->createImportHistory([
			'import_id' => $import_id,
			'status' => MM_FEED_CONST_IMPORT_STATUS_STARTED
		]);

		foreach ($data_rows as $row) {
			// Check new products limit for seller
			if ($this->import_data['new_product_limit'] !== false && $statistics['inserted_rows'] >= $this->import_data['new_product_limit']) {
				break;
			}

			$statistics['total_rows']++;

			// Detect whether product should be inserted, updated or skipped
			// Based on key field

			if ('name' === $key_field && empty($row[$key_field])) {
				// Special case for multi-language name
				$new_key_field = $key_field . '_' . $this->registry->get('config')->get('config_language_id');

				if (!empty($row[$new_key_field])) {
					$maybe_update = true;
					$field_value = $row[$new_key_field];
				} else {
					$maybe_update = false;
					$field_value = '';
				}
			} elseif (!empty($row[$key_field])) {
				$maybe_update = true;
				$field_value = $row[$key_field];
			} else {
				$maybe_update = false;
				$field_value = '';
			}

			$product_id = $maybe_update ? $this->helper->getProductByKeyField($key_field, $field_value, $this->import_data) : null;

			if ($product_id) {
				if (!in_array($product_id, $updated_ids)) {
					// Product has not been updated earlier
					$product_id = $this->MsLoader->ModelProductImport->updateProduct($product_id, $row, $this->import_data);
					$updated_ids[] = $product_id;
					$statistics['updated_rows']++;
				} else {
					// Product has been updated earlier - mark as duplicate
					$statistics['duplicated_rows']++;
				}
			} else {
				// Create new product
				$product_id = $this->MsLoader->ModelProductImport->insertProduct($row, $this->import_data);
				$statistics['inserted_rows']++;
			}

			if ($product_id) {
				$product_ids[] = $product_id;
			}
		}

		if ($import_id) {
			$this->MsLoader->ModelProductImport->createImportHistory([
				'import_id' => $import_id,
				'status' => MM_FEED_CONST_IMPORT_STATUS_FINISHED
			]);

			$this->MsLoader->ModelProductImport->updateImport($import_id, [
				'processed' => $statistics['total_rows'],
				'added' => $statistics['inserted_rows'],
				'updated' => $statistics['updated_rows'],
				'product_ids' => $product_ids
			]);

			$job = $this->MsLoader->ModelCron->get(['import_id' => $import_id]);
			if (!empty($job['id'])) {
				$this->MsLoader->ModelCron->update($job['id'], [
					'is_running' => false
				]);
			}

			// Send notification to seller
			$this->MsHooks->triggerAction('system_finished_import', [
				'producer' => "system.0",
				'consumers' => ["seller.{$this->import_data['seller_id']}"],
				'object' => [
					'type' => 'import',
					'id' => $import_id,
					'action' => 'finished'
				]
			]);
		}

		return $result;
	}
}
