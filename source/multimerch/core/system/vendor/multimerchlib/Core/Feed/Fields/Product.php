<?php

$fields = [

	// Static
	'model' => [
		'csv_name' => 'Model',
		'field_name' => 'Model',
		'required' => true
	],
	'sku' => [
		'csv_name' => 'SKU',
		'field_name' => 'SKU',
		'required' => false
	],
	'price' => [
		'csv_name' => 'Price',
		'field_name' => 'Price',
		'required' => true
	],
	'quantity' => [
		'csv_name' => 'Quantity',
		'field_name' => 'Quantity',
		'required' => false
	],
	'manufacturer' => [
		'csv_name' => 'Manufacturer',
		'field_name' => 'Manufacturer',
		'required' => false
	],
	'image_url' => [
		'csv_name' => 'Primary image',
		'field_name' => 'Primary image',
		'required' => false
	],
	'images_url' => [
		'csv_name' => 'Additional images',
		'field_name' => 'Additional images',
		'required' => false
	],
	'categories_imploded' => [
		'csv_name' => 'Categories',
		'field_name' => 'All categories',
		'required' => false
	],
	/*'currency_code' => [
		'csv_name' => 'Currency',
		'field_name' => 'Currency ISO',
		'required' => false
	],*/
	'weight' => [
		'csv_name' => 'Weight',
		'field_name' => 'Weight',
		'required' => false
	],
	/*'weight_class' => [
		'csv_name' => 'Weight class',
		'field_name' => 'Weight class id',
		'required' => false
	],*/
	/*'tax' => [
		'csv_name' => 'Tax class',
		'field_name' => 'Tax class id',
		'required' => false
	],*/

	// Non-static
	'name' => [
		'csv_name' => 'Product name',
		'field_name' => 'Product name',
		'required' => true
	],
	'description' => [
		'csv_name' => 'Product description',
		'field_name' => 'Product description',
		'required' => true
	],
	/*'category_single' => [
		'csv_name' => 'Category %s',
		'field_name' => 'Category %s',
		'required' => false
	],*/
	/*'msf_attribute' => [
		'csv_name' => '_MSF_ATTRIBUTE_%s_',
		'field_name' => '%s',
		'required' => false
	],*/
	/*'msf_variation' => [
		'csv_name' => '_MSF_VARIATION_%s_',
		'field_name' => '%s',
		'required' => false
	],*/
];
