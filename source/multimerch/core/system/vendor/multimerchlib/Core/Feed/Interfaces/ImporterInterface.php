<?php

namespace MultiMerch\Core\Feed\Interfaces;

interface ImporterInterface
{
	public function start();
}