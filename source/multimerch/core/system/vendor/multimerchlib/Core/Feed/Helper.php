<?php

namespace MultiMerch\Core\Feed;

class Helper extends \Controller
{
	public function __construct($registry)
	{
		parent::__construct($registry);
	}

	public function prepareImageByImageUrl($product_id, $image_url, $images_path, $key=0)
	{
		$from_url = $image_url;

		$img_name = $product_id . ($key ? ('-' .$key) : '') . '-' . basename($from_url);

		$to_url = DIR_IMAGE . $images_path . $img_name;

		$allowed_file_types = $this->config->get('msconf_msg_allowed_file_types');
		$file_types = explode(',', $allowed_file_types);
		$file_types = array_map('strtolower', $file_types);
		$file_types = array_map('trim', $file_types);

		$ext = end(explode('.', $img_name));
		if (in_array(strtolower($ext), $file_types)) {
			if (!file_exists($to_url)) {
				copy($from_url, $to_url);
			}

			return $images_path . $img_name;
		}

		return '';
	}

	public function getStockStatus($stock_status_id)
	{
		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "stock_status`
			WHERE `stock_status_id` = '" . (int)$stock_status_id . "'
				AND `language_id` = '" . (int)$this->config->get('config_language_id') . "'
		";

		$query = $this->db->query($sql);

		return $query->row;
	}

	public function getLanguages()
	{
		$this->load->model('localisation/language');

		return $this->model_localisation_language->getLanguages();
	}

	public function getFilename($attachment_code)
	{
		$this->load->model('tool/upload');

		$upload_info = $this->model_tool_upload->getUploadByCode($attachment_code);

		return $upload_info['filename'] ?? false;
	}

	public function getProductByKeyField($key, $value, $import_config)
	{
		switch ($key) {
			case 'product_id':
				$where_condition = " WHERE p.product_id = " . (int)$value;
				break;
			case 'name':
				$where_condition = " WHERE pd.name = '" . $this->db->escape(htmlspecialchars($value)) . "'";
				break;
			case 'sku':
				$where_condition = " WHERE p.sku = '" . $this->db->escape($value). "'";
				break;
			case 'model':
				$where_condition = " WHERE p.model = '" . $this->db->escape($value). "'";
				break;
			default:
				$where_condition = '';
				break;
		}

		if (!$where_condition) {
			return null;
		}

		$where_condition .= "
			AND mp.seller_id = " . (int)$import_config['seller_id'] . "
			AND pd.language_id = " . (int)$this->config->get('config_language_id') . "
			AND p.status = 1
		";

		$sql = "
			SELECT
				p.product_id
			FROM `" . DB_PREFIX . "product_description` pd
			LEFT JOIN `" . DB_PREFIX . "product` p
				USING (product_id)
			LEFT JOIN `" . DB_PREFIX . "ms_product` mp
				USING(product_id)
			" . $where_condition
		;

		$result = $this->db->query($sql);

		return !empty($result->row['product_id']) ? $result->row['product_id'] : null;
	}
}
