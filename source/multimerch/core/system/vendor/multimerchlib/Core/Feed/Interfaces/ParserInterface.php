<?php

namespace MultiMerch\Core\Feed\Interfaces;

interface ParserInterface
{
	public function parseFieldCaptions();

	public function parseData($mapping, $start_row, $finish_row, $only_active_columns = true);
}