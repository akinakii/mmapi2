<?php

namespace MultiMerch\Core\Feed\Parsers;

use MultiMerch\Core\Feed\Helper;
use MultiMerch\Core\Feed\Interfaces\ParserInterface;

class CsvParser implements ParserInterface
{
	const ENCODING_WINDOWS_1251 = 1;
	const ENCODING_UTF_8 = 2;

	protected $filename;
	protected $file_encoding;
	protected $enclosure;
	protected $delimiter;

	public function __construct($filename, $file_encoding = null, $enclosure = '"', $delimiter = ';')
	{
		if ($file_encoding === null) {
			$file_encoding = $this->detectFileEncoding($filename);
		}

		$this->filename = $filename;
		$this->file_encoding = $file_encoding; // 1 - windows 1251, 2 - utf8
		$this->enclosure = $enclosure;
		$this->delimiter = $delimiter;
	}

	public function parseFieldCaptions()
	{
		$result = [];

		if (($handle = fopen($this->filename, 'r')) !== false) {
			if (($field_caption = fgetcsv($handle, 1000, $this->delimiter, $this->enclosure)) !== false) {
				for ($i = 0; $i < count($field_caption); $i++) {
					$field_caption[$i] = trim($field_caption[$i], " \t\n");

					if ($this->file_encoding == 1) {
						$field_caption[$i] = iconv('windows-1251', 'UTF-8//IGNORE', $field_caption[$i]);
					}
				}

				$result = array_flip($field_caption);
			}

			fclose($handle);
		}

		return $result;
	}

	public function parseData($mapping, $start_row, $finish_row, $only_active_columns = true)
	{
		$result = [];

		$current_row = 1;

		try {
			if (($handle = fopen($this->filename, 'r')) !== false) {
				while (($data = fgetcsv($handle, 10*1024, $this->delimiter, $this->enclosure)) !== false) {
					if ($current_row < $start_row) {
						$current_row++;
						continue;
					}

					if ($finish_row && $current_row > $finish_row) {
						break;
					}

					foreach ($data as $caption_id => $value) {
						if ($only_active_columns && $mapping && empty($mapping[$caption_id])) {
							continue;
						}

						if ($this->file_encoding == 1) {
							$value = iconv('windows-1251', 'UTF-8//IGNORE', $value);
						}

						// For existing mapping key will be text value (e.g. 'model', 'name_1' etc.)
						// For fresh mapping, key will be integer - taken from CSV caption
						$map_key = $mapping[$caption_id] ?? $caption_id;

						$result[$current_row][$map_key] = trim($value);
					}

					$current_row++;
				}

				fclose($handle);
			}
		} catch (\Exception $e) {
			$log = new \Log('mm-import.log');
			$log->write('Unable to read file: ' . $this->filename);

			return [];
		}

		return $result;
	}

	protected function detectFileEncoding($filename)
	{
		$line = file_get_contents($filename);

		switch (mb_detect_encoding($line, ['WINDOWS-1251', 'UTF-8'], true)) {
			case 'UTF-8':
				return self::ENCODING_UTF_8;
			case 'WINDOWS-1251':
			default:
				return self::ENCODING_WINDOWS_1251;
		}
	}
}
