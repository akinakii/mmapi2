<?php

namespace MultiMerch\Core\Feed;

use MultiMerch\Core\Feed\Interfaces\ImporterInterface;
use MultiMerch\Core\Feed\Importers\ProductImporter;

class Importer extends \Controller implements ImporterInterface
{
	protected $registry;
	protected $type;
	protected $import_data;
	protected $oc_fields;
	protected $helper;

	public function __construct($registry, $type, $import_data, $oc_fields)
	{
		parent::__construct($registry);

		$this->registry = $registry;
		$this->type = $type;
		$this->import_data = $import_data;
		$this->oc_fields = $oc_fields;
		$this->helper = new Helper($registry);
	}

	public function start($import_id = null)
	{
		switch ($this->type) {
			case 'product':
			default:
				$importer = new ProductImporter($this->registry, $this->type, $this->import_data, $this->oc_fields);
				break;
		}

		$importer->import($import_id);
	}
}
