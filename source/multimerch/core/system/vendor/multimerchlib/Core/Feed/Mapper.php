<?php

namespace MultiMerch\Core\Feed;

class Mapper
{
	protected $registry;
	protected $type;
	protected $availableFieldKeys;

	public function __construct($registry, $type)
	{
		$this->registry = $registry;
		$this->type = $type;
		$this->helper = new Helper($this->registry);
	}

	public function getFields($seller_id = false, $filterByConfig = false)
	{
		$fields = [];
		if (is_file(__DIR__ . "/Fields/" . ucfirst(strtolower($this->type)) . ".php")) {
			// Retrieve $fields array
			include_once (__DIR__ . "/Fields/" . ucfirst(strtolower($this->type)) . ".php");
		}

		// Prepare non-static fields
		$field_keys = array_keys($fields);

		$languages = $this->helper->getLanguages();

		$language_data = $this->registry->get('load')->language('multimerch/feed');

		// Product name
		if (in_array('name', $field_keys)) {
			$sample = $fields['name'];

			if (count($languages) === 1) {
				$language = reset($languages);
				$language_id = $language['language_id'];

				$fields['name_' . $language_id] = [
					'csv_name' => $sample['csv_name'],
					'field_name' => $language_data['ms_feed_caption_name_single_lang'],
					'required' => true
				];
			} else {
				foreach ($languages as $language) {
					$language_id = $language['language_id'];
					$language_code = $language['code'];

					$fields['name_' . $language_id] = [
						'csv_name' => $sample['csv_name'] . " ($language_code)",
						'field_name' => sprintf($language_data['ms_feed_caption_name_multi_lang'], $language_code),
						'required' => ($language_id === $this->registry->get('config')->get('config_language_id')) ? true : false
					];
				}
			}

			// Unset sample key
			unset($fields['name']);
		}

		// Product description
		if (in_array('description', $field_keys)) {
			$sample = $fields['description'];

			if (count($languages) === 1) {
				$language = reset($languages);
				$language_id = $language['language_id'];

				$fields['description_' . $language_id] = [
					'csv_name' => $sample['csv_name'],
					'field_name' => $language_data['ms_feed_caption_description_single_lang'],
					'required' => true
				];
			} else {
				foreach ($languages as $language) {
					$language_id = $language['language_id'];
					$language_code = $language['code'];

					$fields['description_' . $language_id] = [
						'csv_name' => $sample['csv_name'] . " ($language_code)",
						'field_name' => sprintf($language_data['ms_feed_caption_description_multi_lang'], $language_code),
						'required' => ($language_id === $this->registry->get('config')->get('config_language_id')) ? true : false
					];
				}
			}

			// Unset sample key
			unset($fields['description']);
		}

		// Check whether categories are configured to be in one or separate cells
		// @DEPRECATED
		if ($this->registry->get('config')->get('msconf_import_category_type')) {
			// Different levels in different cells

			if (in_array('categories_imploded', $field_keys)) {
				unset($fields['categories_imploded']);
			}

			$sample = $fields['category_single'];

			// Up to 6 category levels
			for ($i = 1; $i < 7; $i++) {
				$fields['category_single_' . $i] = [
					'csv_name' => sprintf($sample['csv_name'], $i),
					'field_name' => sprintf($language_data['ms_feed_caption_category_single'], $i),
					'required' => false,
				];
			}

			if (in_array('category_single', $field_keys)) {
				unset($fields['category_single']);
			}
		} else {
			// All levels in a single cell (separator - |)

			if (in_array('category_single', $field_keys)) {
				unset($fields['category_single']);
			}
		}

		// @todo 8.28
		if ($seller_id) {
			// Product MSF attributes

			// Product MSF variations
		}

		// Field names translation
		foreach ($fields as $type => &$field) {
			if (isset($language_data["ms_feed_caption_$type"])) {
				$field['field_name'] = $language_data["ms_feed_caption_$type"];
			}
		}

		if ($filterByConfig) {
			$fields = $this->filterAvailableFields($fields);
		}

		// Make primary product field required
		switch ($this->registry->get('config')->get('msconf_feed_primary_product_field')) {
			// Name always required

			case 'name':
				if (isset($fields['sku'])) $fields['sku']['required'] = false;
				$fields['model']['required'] = false;
				break;
			case 'sku':
				if (isset($fields['sku'])) $fields['sku']['required'] = true;
				$fields['model']['required'] = false;
				break;
			case 'model':
			default:
				if (isset($fields['sku'])) $fields['sku']['required'] = false;
				$fields['model']['required'] = true;
				break;
		}

		return $fields;
	}

	protected function filterAvailableFields($fields)
	{
		foreach ($fields as $key => $value) {
			if ($this->isAttributeToUnset($key, $value)) {
				unset($fields[$key]);
			}
		}

		return $fields;
	}

	protected function isAttributeToUnset($key, $value)
	{
		if ($value['required']) {
			return false;
		}

		if (in_array($key, $this->getAvailableFieldKeys())) {
			return false;
		}

		if (preg_match('/^image/', $key) && in_array('images', $this->getAvailableFieldKeys())) {
			return false;
		}

		if (preg_match('/^category/', $key) && in_array('marketplace_category', $this->getAvailableFieldKeys())) {
			return false;
		}

		if ($key == 'categories_imploded' && in_array('marketplace_category', $this->getAvailableFieldKeys())) {
			return false;
		}

		/**
		 * language-related fields is required
		 */
		if (preg_match('/^name|description/', $key)) {
			return false;
		}

		return true;
	}

	protected function getAvailableFieldKeys()
	{
		if ($this->availableFieldKeys === null) {
			$this->availableFieldKeys = (array)$this->registry->get('config')->get('msconf_product_included_fields');
		}

		return $this->availableFieldKeys;
	}
}
