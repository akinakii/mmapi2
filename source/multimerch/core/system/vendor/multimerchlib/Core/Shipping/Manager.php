<?php

namespace MultiMerch\Core\Shipping;

use MultiMerch\Core\Shipping\Factories\ShippingFactory;

class Manager extends \Controller
{
	/**
	 * @var ShippingFactory	Shipping factory.
	 */
	protected $shipping_factory;

	protected $is_admin = false;

	/**
	 * Manager constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->shipping_factory = new ShippingFactory($registry);

		if (defined('DIR_CATALOG'))
			$this->is_admin = true;

		$this->load->model('localisation/language');
	}

	public function getShippingFactory()
	{
		return $this->shipping_factory;
	}

	public function getCountries($data = [])
	{
		return $this->MsLoader->MsShipping->getCountries($data);
	}

	public function getGeoZones($data = [])
	{
		return $this->MsLoader->MsShipping->getGeoZones($data);
	}

	public function getShippingMethods($data = [])
	{
		return $this->MsLoader->MsShipping->getShippingMethods($data);
	}

	public function getDeliveryTimes($data = [])
	{
		return $this->MsLoader->MsShipping->getDeliveryTimes($data);
	}
}
