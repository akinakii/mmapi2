<?php

namespace MultiMerch\Core\Shipping\Types;

use MultiMerch\Core\Shipping\Shipping;

class CartWeightShipping extends Shipping
{
	protected $weightFrom = true;
	protected $weightTo = true;
	protected $costPerWeightUnit = false;

	public function __construct($registry)
	{
		parent::__construct($registry);

		if ($this->config->get('msconf_shipping_field_cost_pwu_enabled')) {
			$this->costPerWeightUnit = true;
		}
	}

	public function getRules($data = [])
	{
		return $this->MsLoader->MsShipping->getCartWeightBasedRules($data);
	}
}
