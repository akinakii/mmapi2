<?php

namespace MultiMerch\Core\Shipping\Factories;

use MultiMerch\Core\Shipping\Types\OcShipping as OcShipping;
use MultiMerch\Core\Shipping\Types\CartWeightShipping as CartWeightShipping;
use MultiMerch\Core\Shipping\Types\CartTotalShipping as CartTotalShipping;
use MultiMerch\Core\Shipping\Types\FlatShipping as FlatShipping;
use MultiMerch\Core\Shipping\Types\PerProductShipping as PerProductShipping;

/**
 * Class ShippingFactory
 * @package MultiMerch\Core\Shipping\Factories
 */
class ShippingFactory
{
	private $registry;

	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	public function create($type)
	{
		switch ((int)$type) {
			case 1:
				$shipping = new OcShipping($this->registry);
				break;

			case 2:
				$shipping = new CartWeightShipping($this->registry);
				break;

			case 3:
				$shipping = new CartTotalShipping($this->registry);
				break;

			case 4:
				$shipping = new FlatShipping($this->registry);
				break;

			case 5:
				$shipping = new PerProductShipping($this->registry);
				break;

			case 0:
			default:
				$shipping = null;
				break;
		}

		return $shipping;
	}
}
