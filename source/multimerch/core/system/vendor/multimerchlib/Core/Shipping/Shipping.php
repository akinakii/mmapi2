<?php

namespace MultiMerch\Core\Shipping;

/**
 * Class Manager
 * @package MultiMerch\Core\Shipping
 */
class Shipping
{
    const TYPE_DISABLED = 0;
    const TYPE_OC = 1;
    const TYPE_CART_WEIGHT = 2;
    const TYPE_CART_TOTAL = 3;
    const TYPE_FLAT = 4;
    const TYPE_PER_PRODUCT = 5;
	const TYPE_CUSTOM_POSTCODE = 6;

	/**
	 * @var	mixed	OpenCart registry.
	 */
	protected $registry;

	protected $fromCountry = false;
	protected $processingDays = false;
	protected $destinations = true;
	protected $shippingMethod = false;
	protected $deliveryTime = false;
	protected $cost = true;

	/**
	 * InvoiceFactory constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;

		if ($this->config->get('msconf_shipping_field_from_enabled')) {
			$this->fromCountry = true;
		}

		if ($this->config->get('msconf_shipping_field_processing_time_enabled')) {
			$this->processingDays = true;
		}

		if ($this->config->get('msconf_shipping_field_shipping_method_enabled')) {
			$this->shippingMethod = true;
		}

		if ($this->config->get('msconf_shipping_field_delivery_time_enabled')) {
			$this->deliveryTime = true;
		}
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->registry->get($name);
	}
}
