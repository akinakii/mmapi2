<?php

const PROJECT_ROOT_PATH = __DIR__ . '/../../../../';

// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file(PROJECT_ROOT_PATH . 'config.php')) {
	require_once(PROJECT_ROOT_PATH . 'config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	echo 'OC Config not found';
	exit;
}

// VirtualQMOD
require_once(PROJECT_ROOT_PATH . 'vqmod/vqmod.php');
VQMod::bootup();

// VQMODDED Startup
//require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

// Modification Override
function modification($filename) {
	if (defined('DIR_CATALOG')) {
		$file = DIR_MODIFICATION . 'admin/' .  substr($filename, strlen(DIR_APPLICATION));
	} elseif (defined('DIR_OPENCART')) {
		$file = DIR_MODIFICATION . 'install/' .  substr($filename, strlen(DIR_APPLICATION));
	} else {
		$file = DIR_MODIFICATION . 'catalog/' . substr($filename, strlen(DIR_APPLICATION));
	}

	if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
		$file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
	}

	if (is_file($file)) {
		return $file;
	}

	return $filename;
}

// Autoloader
if (is_file(DIR_SYSTEM . '../../vendor/autoload.php')) {
	require_once(DIR_SYSTEM . '../../vendor/autoload.php');
}

function library($class) {
	$file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

	if (is_file($file)) {
		include_once(modification($file));

		return true;
	} else {
		return false;
	}
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

// Engine
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/action.php'), DIR_SYSTEM . 'engine/action.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/controller.php'), DIR_SYSTEM . 'engine/controller.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/event.php'), DIR_SYSTEM . 'engine/event.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/front.php'), DIR_SYSTEM . 'engine/front.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/loader.php'), DIR_SYSTEM . 'engine/loader.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/model.php'), DIR_SYSTEM . 'engine/model.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/registry.php'), DIR_SYSTEM . 'engine/registry.php'));
require_once(\VQMod::modCheck(modification(DIR_SYSTEM . 'engine/proxy.php'), DIR_SYSTEM . 'engine/proxy.php'));

// Helper
require_once(\VQMod::modCheck(DIR_SYSTEM . 'helper/general.php'));
require_once(\VQMod::modCheck(DIR_SYSTEM . 'helper/utf8.php'));
require_once(\VQMod::modCheck(DIR_SYSTEM . 'helper/json.php'));

// MultiMerch constants
if (is_file(DIR_SYSTEM . 'vendor/multimerchlib/Core/Constants.php')) {
	require_once(DIR_SYSTEM . 'vendor/multimerchlib/Core/Constants.php');
}

// Registry
$registry = new Registry();

// Config
$config = new Config();
$config->load('default');
$config->load('catalog');
$registry->set('config', $config);

// Event
$event = new Event($registry);
$registry->set('event', $event);

// Event Register
if ($config->has('action_event')) {
	foreach ($config->get('action_event') as $key => $value) {
		$event->register($key, new Action($value));
	}
}

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Database
if ($config->get('db_autostart')) {
	$registry->set('db', new DB($config->get('db_type'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port')));
}

// Settings from DB
$query = $registry->get('db')->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' ORDER BY store_id ASC");

foreach ($query->rows as $result) {
	$config->set($result['key'], !$result['serialized'] ? $result['value'] : json_decode($result['value'], true));
}

// Cache
$registry->set('cache', new Cache($config->get('cache_type'), $config->get('cache_expire')));

// Language
$language = new Language($config->get('language_default'));
$language->load($config->get('language_default'));
$registry->set('language', $language);

// Get config_language_id
$registry->get('load')->model('localisation/language');
$languages = $registry->get('model_localisation_language')->getLanguages();
$config->set('config_language_id', $languages[$config->get('config_language')]['language_id']);

// Currency
$registry->set('currency', new Cart\Currency($registry));

// Tax
$registry->set('tax', new Cart\Tax($registry));

// MultiMerch hooks
$registry->set('MsHooks', new \MultiMerch\Core\Hook());
$registry->get('MsHooks')->setRegistry($registry);
$registry->get('MsHooks')->registerDefaultHooks();