<?php

namespace MultiMerch\Core\Invoice\TemplateInvoices;

use MultiMerch\Core\Invoice\Invoice;

class PayoutInvoice extends Invoice
{
	public function __construct($registry, $sender, $recipient)
	{
		parent::__construct($registry, 'payout', $sender, $recipient);
	}

	/**
	 * Overrides setInvoiceNo(). Adds prefix to generated invoice number.
	 *
	 * @param	int			$invoice_no
	 */
	public function setInvoiceNo($invoice_no)
	{
		$prefix = 'POI';

		$this->invoice_no = $prefix . $invoice_no;
	}

	/**
	 * Generates invoice title based on invoicing object.
	 *
	 * @return	string	Invoice title.
	 */
	public function getTitle()
	{
		$this->load->language('multiseller/multiseller');

		switch ($this->getObjectType()) {
			case 'seller':
			default:
				$seller_id = $this->getObjectId();
				$seller_name = $this->MsLoader->MsSeller->getSellerNickname($seller_id);
				$title = sprintf($this->language->get('ms_invoice_title_seller_payout'), $seller_name);
				break;
		}

		return $title;
	}
}
