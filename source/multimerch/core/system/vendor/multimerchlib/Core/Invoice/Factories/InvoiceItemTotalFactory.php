<?php

namespace MultiMerch\Core\Invoice\Factories;

use MultiMerch\Core\Invoice\InvoiceItemTotal;
use MultiMerch\Module\Errors\Generic;

/**
 * Class InvoiceItemTotalFactory
 * @package MultiMerch\Core\Invoice\Factories
 */
class InvoiceItemTotalFactory
{
	/**
	 * @var	mixed	OpenCart registry.
	 */
	private $registry;

	/**
	 * InvoiceItemTotalFactory constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * Creates InvoiceItemTotal instance.
	 *
	 * @param	string	$type						Invoice item total type.
	 * @param	array	$invoice_item_total_data	Invoice item total data.
	 * @return	InvoiceItemTotal|null				Returns null if total value is zero.
	 * @throws	Generic
	 */
	public function create($type, $invoice_item_total_data)
	{
		// Validate required parameters
		$this->validate($invoice_item_total_data);

		// If value is zero or below, do not create invoice total
		if ($invoice_item_total_data['value'] <= 0)
			return null;

		$invoice_item_total = new InvoiceItemTotal($type, $invoice_item_total_data['value'], $invoice_item_total_data['titles']);

		return $invoice_item_total;
	}

	/**
	 * Validates received parameters.
	 *
	 * @param	array	$invoice_item_total_data	Settings needed for invoice item total creation.
	 * @throws	Generic
	 */
	private function validate($invoice_item_total_data)
	{
		$required_parameters = ['value', 'titles'];

		foreach ($required_parameters as $p) {
			if (!isset($invoice_item_total_data[$p]))
				throw new Generic("InvoiceItemTotalFactory - Required parameter `{$p}` is missing!");
		}
	}
}
