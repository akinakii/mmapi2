<?php

namespace MultiMerch\Core\Invoice;

use MultiMerch\Core\Invoice\Factories\InvoiceItemTotalFactory;
use MultiMerch\Core\Invoice\Helper as Helper;

class InvoiceItem
{
	/**
	 * @var int	Related invoice id.
	 */
	protected $invoice_id = null;

	/**
	 * @var int	Quantity of the invoice item objects.
	 */
	protected $quantity = 1;

	/**
	 * @var float  Price for a single invoice item object.
	 */
	protected $price = 0;

	/**
	 * @var string	Additional data related to the invoice item.
	 */
	protected $metadata = [];

	/**
	 * @var array  Array of titles translated to different languages.
	 */
	protected $titles = [];

	/**
	 * @var InvoiceItemTotal[]	Array of invoice item totals.
	 */
	protected $totals = [];

	/**
	 * @var	float	Invoice item total.
	 */
	protected $total = 0;

	/**
	 * @var	mixed	OpenCart registry.
	 */
	protected $registry;

	/**
	 * @var	Helper	Invoice helper.
	 */
	protected $helper;

	/**
	 * @var	InvoiceItemTotalFactory	Invoice item totals factory.
	 */
	protected $invoice_item_total_factory;

	/**
	 * InvoiceItem constructor.
	 *
	 * @param	mixed	$registry
	 * @param	int		$quantity
	 * @param	float	$price
	 * @param	array	$titles
	 */
	public function __construct($registry, $quantity, $price, $titles)
	{
		$this->setRegistry($registry);
		$this->setHelper(new Helper($registry));
		$this->setInvoiceItemTotalFactory(new InvoiceItemTotalFactory($registry));

		$this->setQuantity($quantity);
		$this->setPrice($price);
		$this->setTitles($titles);
	}

	/**
	 * @return int
	 */
	public function getInvoiceId()
	{
		return $this->invoice_id;
	}

	/**
	 * @param int $invoice_id
	 */
	public function setInvoiceId($invoice_id)
	{
		$this->invoice_id = $invoice_id;
	}

	/**
	 * @param int|null $language_id
	 * @return string|array
	 */
	public function getTitles($language_id = null)
	{
		if ($language_id)
			return isset($this->titles[$language_id]) ? $this->titles[$language_id] : '';

		return $this->titles;
	}

	/**
	 * @param array $titles
	 */
	public function setTitles($titles)
	{
		$this->titles = $titles;
	}

	/**
	 * @return int
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return float
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	}

	/**
	 * @return string
	 */
	public function getMetadata()
	{
		return $this->metadata;
	}

	/**
	 * @param string $metadata
	 */
	public function setMetadata($metadata)
	{
		$this->metadata = $metadata;
	}

	/**
	 * @return InvoiceItemTotal[]
	 */
	public function getTotals()
	{
		return $this->totals;
	}

	/**
	 * @param	float	$subtotal
	 * @param	array	$totals_data
	 * @return	array
	 * @throws	\MultiMerch\Module\Errors\Generic
	 */
	public function createTotalsFromData($subtotal, $totals_data)
	{
		$result = [];
		$totals = [];
		$invoice_item_total = $subtotal;

		// Subtotal
		$totals['subtotal'] = [
			'titles' => $this->getHelper()->getDefaultTotalTitles('subtotal'),
			'value' => $subtotal
		];

		// Discount
		if (isset($totals_data['discount'])) {
			$totals['discount'] = [
				'titles' => $this->getHelper()->getDefaultTotalTitles('discount'),
				'value' => -$totals_data['discount']
			];

			$invoice_item_total -= $totals_data['discount'];
		}

		// Tax
		if (isset($totals_data['tax'])) {
			$totals['tax'] = [
				'titles' => $this->getHelper()->getDefaultTotalTitles('tax'),
				'value' => $totals_data['tax']
			];

			$invoice_item_total += $totals_data['tax'];
		}

		// OC Shipping
		if (isset($totals_data['shipping'])) {
			$totals['shipping'] = [
				'titles' => $this->getHelper()->getDefaultTotalTitles('shipping'),
				'value' => $totals_data['shipping']
			];

			$invoice_item_total += $totals_data['shipping'];
		}

		// MM Shipping
		if (isset($totals_data['mm_shipping_total'])) {
			$totals['mm_shipping_total'] = [
				'titles' => $this->getHelper()->getDefaultTotalTitles('ms_shipping'),
				'value' => $totals_data['mm_shipping_total']
			];

			$invoice_item_total += $totals_data['mm_shipping_total'];
		}

		// Custom totals
		if (isset($totals_data['custom'])) {
			foreach ($totals_data['custom'] as $key => $custom_total) {
				$totals["custom_{$key}"] = [
					'titles' => $this->getHelper()->complementTotalTitles($custom_total['titles']),
					'value' => $custom_total['value']
				];

				$invoice_item_total += $custom_total['value'];
			}
		}

		$totals['total'] = [
			'titles' => $this->getHelper()->getDefaultTotalTitles('total'),
			'value' => $invoice_item_total
		];

		foreach ($totals as $type => $total_data) {
			$result[] = $this->getInvoiceItemTotalFactory()->create($type, $total_data);
		}

		return $result;
	}

	/**
	 * @param $totals
	 */
	public function setTotals($totals)
	{
		foreach ($totals as $total) {
			if (!$total)
				continue;

			$this->totals[] = $total;

			if ('total' === $total->getType())
				$this->setTotal($total->getValue());
		}
	}

	/**
	 * @return float
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * @param float $total
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	}

	/**
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * @param mixed $registry
	 */
	public function setRegistry($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * @return mixed
	 */
	public function getHelper()
	{
		return $this->helper;
	}

	/**
	 * @param mixed $helper
	 */
	public function setHelper($helper)
	{
		$this->helper = $helper;
	}

	/**
	 * @return InvoiceItemTotalFactory
	 */
	public function getInvoiceItemTotalFactory()
	{
		return $this->invoice_item_total_factory;
	}

	/**
	 * @param InvoiceItemTotalFactory $invoice_item_total_factory
	 */
	public function setInvoiceItemTotalFactory($invoice_item_total_factory)
	{
		$this->invoice_item_total_factory = $invoice_item_total_factory;
	}
}
