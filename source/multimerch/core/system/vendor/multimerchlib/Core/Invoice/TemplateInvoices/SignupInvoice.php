<?php

namespace MultiMerch\Core\Invoice\TemplateInvoices;

use MultiMerch\Core\Invoice\Invoice;

class SignupInvoice extends Invoice
{
	public function __construct($registry, $sender, $recipient)
	{
		parent::__construct($registry, 'signup', $sender, $recipient);
	}

	/**
	 * Overrides setInvoiceNo(). Adds prefix to generated invoice number.
	 *
	 * @param	int			$invoice_no
	 */
	public function setInvoiceNo($invoice_no)
	{
		$prefix = 'SUI';

		$this->invoice_no = $prefix . $invoice_no;
	}

	/**
	 * Generates invoice title based on invoicing object.
	 *
	 * @return	string	Invoice title.
	 */
	public function getTitle()
	{
		$this->load->language('multiseller/multiseller');

		$is_admin = defined('DIR_CATALOG');

		switch ($this->getObjectType()) {
			case 'seller':
			default:
				$seller_id = $this->getObjectId();
				$seller_name = $this->MsLoader->MsSeller->getSellerNickname($seller_id);
				$title = $is_admin
					? sprintf(
						$this->language->get('ms_invoice_title_seller_signup'),
						$this->url->link('multimerch/seller/update', 'seller_id=' . $seller_id . (isset($this->session->data['token']) ? ('&token=' . $this->session->data['token']) : ''), 'SSL'),
						$seller_name,
						$this->config->get('config_name')
					)
					: sprintf(
						$this->language->get('ms_invoice_title_seller_signup'),
						$this->config->get('config_name')
					);
				break;
		}

		return $title;
	}
}
