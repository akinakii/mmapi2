<?php

namespace MultiMerch\Core\Invoice;

use MultiMerch\Core\Invoice\Factories\InvoiceFactory as InvoiceFactory;
use MultiMerch\Core\Invoice\Invoice as Invoice;
use MultiMerch\Module\Errors\Generic;

/**
 * Class Manager
 * @package MultiMerch\Core\Invoice
 */
class Manager extends \Controller
{
	/**
	 * @var	mixed	$registry	OpenCart registry.
	 */
	protected $registry;

	/**
	 * @var InvoiceFactory	Invoices factory.
	 */
	protected $invoice_factory;

	private $is_admin = false;

	/**
	 * Manager constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->registry = $registry;
		$this->invoice_factory = new InvoiceFactory($registry);

		if (defined('DIR_CATALOG'))
			$this->is_admin = true;

		$this->load->model('localisation/language');
	}

	/**
	 * Represents main flow of invoice creation.
	 *
	 * @param	string		$type			Invoice type: listing, signup, sale, payout, custom.
	 * @param	array		$invoice_data	Data for invoice cration.
	 * @return	Invoice|null
	 */
	public function process($type, $invoice_data = [])
	{
		$this->ms_logger->debug('InvoiceManager/process');

		$invoice = null;

		try {
			// Create invoice instance
			$invoice = $this->invoice_factory->create($type, $invoice_data);

			// Store invoice and its related items in the database
			$invoice_id = $this->MsLoader->MsInvoice->create($invoice);

			// Assign invoice id to the invoice instance
			$invoice->setInvoiceId($invoice_id);

			$this->ms_logger->debug('InvoiceManager/process - Invoice created');
		} catch (Generic $e) {
			$this->ms_logger->error('MultiMerch generic error in ' . static::class . ' - ' . $e->getMessage());
		} catch (\Exception $e) {
			$this->ms_logger->error('Generic error in ' . static::class . ' - ' . $e->getMessage());
		}

		return $invoice;
	}

	// Default invoices

	/**
	 * Creates product listing invoice.
	 *
	 * @param	int			$product_id		Product id of the product being invoiced.
	 * @param	int			$seller_id		Seller id assigned to the product.
	 * @param	float		$fee			Product listing fee.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createProductListingInvoice($product_id, $seller_id, $fee, $notify = true, $metadata = [])
	{
		$this->ms_logger->debug('InvoiceManager/createProductListingInvoice');

		// Invoice item 1
		$titles = [];
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_product_listing'), $this->MsLoader->MsProduct->getProductName($product_id, $language['language_id']));
		}

		$item_1 = [
			'titles' => $titles,
			'quantity' => 1,
			'price' => $fee
		];

		$invoice = $this->process('listing', array_merge($metadata, [
			'object' => [
				'type' => 'product',
				'id' => $product_id
			],
			'sender' => "platform.0",
			'recipient' => "seller.{$seller_id}",
			'metadata' => [
				'sender_address' => $this->MsLoader->MsHelper->getDefaultPlatformAddress(),
				'recipient_address' => $this->MsLoader->MsSeller->getSellerMsAddress(['address_id' => $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id, 'name' => 'slr_ms_address', 'single' => true])])
			],
			'custom_items_data' => [$item_1],
			'custom_totals_data' => []
		]));

		if (!$invoice) {
			$this->ms_logger->error('Error creating product listing fee invoice');
			return false;
		}

		if ($notify) {
			$this->MsHooks->triggerAction('system_created_invoice', [
				'producer' => "system.0",
				'consumers' => ["seller.{$seller_id}"],
				'object' => [
					'type' => 'invoice',
					'id' => $invoice->getInvoiceId(),
					'action' => 'created'
				]
			]);
		}

		return $invoice->getInvoiceId();
	}

	/**
	 * Creates seller signup invoice.
	 *
	 * @param	int			$seller_id		Seller id of the seller signing up.
	 * @param	float		$fee			Seller signup fee.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createSellerSignupInvoice($seller_id, $fee, $notify = true, $metadata = [])
	{
		$this->ms_logger->debug('InvoiceManager/createSellerSignupInvoice');

		// Invoice item 1
		$titles = [];
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_seller_signup'), $this->config->get('config_name'));
		}

		$item_1 = [
			'titles' => $titles,
			'quantity' => 1,
			'price' => $fee
		];

		$invoice = $this->process('signup', array_merge($metadata, [
			'object' => [
				'type' => 'seller',
				'id' => $seller_id
			],
			'sender' => "platform.0",
			'recipient' => "seller.{$seller_id}",
			'metadata' => [
				'sender_address' => $this->MsLoader->MsHelper->getDefaultPlatformAddress(),
				'recipient_address' => $this->MsLoader->MsSeller->getSellerMsAddress(['address_id' => $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id, 'name' => 'slr_ms_address', 'single' => true])])
			],
			'custom_items_data' => [$item_1],
			'custom_totals_data' => []
		]));

		if (!$invoice) {
			$this->ms_logger->error('Error creating seller signup fee invoice');
			return false;
		}

		if ($notify) {
			$this->MsHooks->triggerAction('system_created_invoice', [
				'producer' => "system.0",
				'consumers' => ["seller.{$seller_id}"],
				'object' => [
					'type' => 'invoice',
					'id' => $invoice->getInvoiceId(),
					'action' => 'created'
				]
			]);
		}

		return $invoice->getInvoiceId();
	}

	/**
	 * Creates sale invoice from platform to customer.
	 *
	 * Used in the default payment flow: all the funds are transferred from the customer to the marketplace.
	 *
	 * @param	int			$order_id		Order id.
	 * @param	int			$customer_id	Customer id.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createSaleInvoiceFromPlatformToCustomer($order_id, $customer_id, $notify = true, $metadata = [])
	{
		if ($this->is_admin) {
			$this->load->model('customer/customer');
			$this->load->model('sale/order');

			$customer = $this->model_customer_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_customer_customer->getAddress($customer['address_id']) : [];
			$order_totals = $this->model_sale_order->getOrderTotals($order_id);
		} else {
			$this->load->model('account/address');
			$this->load->model('account/customer');
			$this->load->model('account/order');

			$customer = $this->model_account_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_account_address->getAddress($customer['address_id']) : [];
			$order_totals = $this->model_account_order->getOrderTotals($order_id);
		}

		if (empty($customer)) {
			$this->ms_logger->debug('InvoiceManager/createSaleInvoiceFromPlatformToCustomer - Unable to retrieve customer!');
			return false;
		}

		$this->ms_logger->debug('InvoiceManager/createSaleInvoiceFromPlatformToCustomer');

		$order_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);

		$invoice_items = [];

		// Add invoice items - products
		foreach ($order_products as $order_product) {
			$titles = [];
			foreach ($this->model_localisation_language->getLanguages() as $language) {
				$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_sale_product'), $this->MsLoader->MsProduct->getProductName($order_product['product_id'], $language['language_id']));
			}

			$invoice_item_custom_totals = [];

			if (!empty($order_product['tax']))
				$invoice_item_custom_totals['tax'] = $order_product['tax'];

			if (!empty($order_product['shipping_cost']))
				$invoice_item_custom_totals['mm_shipping_total'] = $order_product['shipping_cost'];

			$invoice_item = [
				'titles' => $titles,
				'quantity' => $order_product['quantity'],
				'price' => $order_product['price'],
				'custom_totals' => $invoice_item_custom_totals
			];

			if ($invoice_item['price'] > 0 || !empty($invoice_item_custom_totals))
				$invoice_items[] = $invoice_item;
		}

		if (empty($invoice_items)) {
			$this->ms_logger->info('InvoiceManager/createSaleInvoiceFromPlatformToCustomer - Invoice has no invoice items, therefore it is not created');
			return false;
		}

		$invoice_custom_totals = [];

		foreach ($order_totals as $order_total) {
			if (isset($invoice_custom_totals[$order_total['code']]['value'])) {
				$invoice_custom_totals[$order_total['code']]['value'] += $order_total['value'];
			} else {
				$invoice_custom_totals[$order_total['code']] = [
					'titles' => [$this->config->get('config_language_id') => $order_total['title']],
					'value' => $order_total['value']
				];
			}
		}

		$invoice = $this->process('sale', array_merge($metadata, [
			'object' => [
				'type' => 'order',
				'id' => $order_id
			],
			'sender' => "platform.0",
			'recipient' => "customer.{$customer_id}",
			'status' => Invoice::STATUS_PAID,
			'metadata' => [
				'sender_address' => $this->MsLoader->MsHelper->getDefaultPlatformAddress(),
				'recipient_address' => $customer_address
			],
			'custom_items_data' => $invoice_items,
			'custom_totals_data' => $invoice_custom_totals
		]));

		if (!$invoice) {
			$this->ms_logger->error('InvoiceManager/createSaleInvoiceFromPlatformToCustomer - Error creating sale invoice');
			return false;
		}

		if ($notify) {
			$this->MsHooks->triggerAction('system_created_invoice', [
				'producer' => "system.0",
				'consumers' => ["customer.{$customer_id}"],
				'object' => [
					'type' => 'invoice',
					'id' => $invoice->getInvoiceId(),
					'action' => 'created'
				]
			]);
		}

		return $invoice->getInvoiceId();
	}

	/**
	 * Creates Stripe sale invoice from platform to customer for full order.
	 *
	 * All funds for commissions (store shipping, sale commissions etc.) are transferred from the customer to the platform.
	 *
	 * @param	int			$order_id		Order id.
	 * @param	int			$customer_id	Customer id.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createStripeSaleInvoiceFromPlatformToCustomerForCommissions($order_id, $customer_id, $notify = true, $metadata = [])
	{
		$this->ms_logger->debug('InvoiceManager/createStripeSaleInvoiceFromPlatformToCustomerForCommissions');

		if ($this->is_admin) {
			$this->load->model('customer/customer');
			$this->load->model('sale/order');

			$customer = $this->model_customer_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_customer_customer->getAddress($customer['address_id']) : [];
			$order_totals = $this->model_sale_order->getOrderTotals($order_id);
		} else {
			$this->load->model('account/address');
			$this->load->model('account/customer');
			$this->load->model('account/order');

			$customer = $this->model_account_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_account_address->getAddress($customer['address_id']) : [];
			$order_totals = $this->model_account_order->getOrderTotals($order_id);
		}

		$invoice_items = [];

		// Process store order totals (shipping, coupons etc.)
		foreach ($order_totals as $order_total) {
			// Skip tax and MM shipping totals, because they are related to sellers' products
			if (in_array($order_total['code'], ['sub_total', 'tax', 'mm_shipping_total', 'total']))
				continue;

			$titles = [];
			foreach ($this->model_localisation_language->getLanguages() as $language) {
				$titles[$language['language_id']] = $order_total['title'];
			}

			if ($order_total['value'] > 0) {
				$invoice_items[] = [
					'titles' => $titles,
					'quantity' => 1,
					'price' => $order_total['value']
				];
			}
		}

		// Process store commissions
		$order_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);

		foreach ($order_products as $order_product) {
			$titles = [];
			foreach ($this->model_localisation_language->getLanguages() as $language) {
				$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_sale_commission'), $this->MsLoader->MsProduct->getProductName($order_product['product_id'], $language['language_id']), $order_id);
			}

			$product_commission = $order_product['store_commission_flat'] + $order_product['store_commission_pct'];

			if ($product_commission > 0) {
				$invoice_items[] = [
					'titles' => $titles,
					'quantity' => 1,
					'price' => $product_commission
				];
			}
		}

		if (empty($invoice_items)) {
			$this->ms_logger->info('InvoiceManager/createStripeSaleInvoiceFromPlatformToCustomerForCommissions - Invoice contains no invoice items, therefore is not created');
			return false;
		}

		$invoice = $this->process('sale', array_merge($metadata, [
			'object' => [
				'type' => 'order',
				'subtype' => 'commission',
				'id' => $order_id
			],
			'sender' => "platform.0",
			'recipient' => "customer.{$customer_id}",
			'metadata' => [
				'sender_address' => $this->MsLoader->MsHelper->getDefaultPlatformAddress(),
				'recipient_address' => $customer_address
			],
			'custom_items_data' => $invoice_items,
			'custom_totals_data' => []
		]));

		if (!$invoice) {
			$this->ms_logger->error('Error creating sale invoice to seller');
			return false;
		}

		if ($notify) {
			$this->MsHooks->triggerAction('system_created_invoice', [
				'producer' => "system.0",
				'consumers' => ["customer.{$customer_id}"],
				'object' => [
					'type' => 'invoice',
					'id' => $invoice->getInvoiceId(),
					'action' => 'created'
				]
			]);
		}

		return $invoice->getInvoiceId();
	}

	/**
	 * Creates Stripe sale invoice from platform to customer for full order.
	 *
	 * All funds for products in order are transferred from the customer to the seller.
	 *
	 * @param	int			$order_id		Order id.
	 * @param	int			$customer_id	Customer id.
	 * @param	int			$seller_id		Seller id.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createStripeSaleInvoiceFromSellerToCustomerForProducts($order_id, $customer_id, $seller_id, $notify = true, $metadata = [])
	{
		$this->ms_logger->debug('InvoiceManager/createStripeSaleInvoiceFromSellerToCustomerForProducts');

		if ($this->is_admin) {
			$this->load->model('customer/customer');

			$customer = $this->model_customer_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_customer_customer->getAddress($customer['address_id']) : [];
		} else {
			$this->load->model('account/address');
			$this->load->model('account/customer');

			$customer = $this->model_account_customer->getCustomer($customer_id);
			$customer_address = !empty($customer['address_id']) ? $this->model_account_address->getAddress($customer['address_id']) : [];
		}

		$order_products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id, 'seller_id' => $seller_id]);

		$invoice_items = [];
		foreach ($order_products as $order_product) {
			$titles = [];
			foreach ($this->model_localisation_language->getLanguages() as $language) {
				$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_sale_product'), $this->MsLoader->MsProduct->getProductName($order_product['product_id'], $language['language_id']));
			}

			$invoice_item_custom_totals = [];

			if ($order_product['tax'])
				$invoice_item_custom_totals['tax'] = $order_product['tax'];

			if ($order_product['shipping_cost'])
				$invoice_item_custom_totals['mm_shipping_total'] = $order_product['shipping_cost'];

			// @todo 9.0 Invoicing: coupons total

			$invoice_item = [
				'titles' => $titles,
				'quantity' => $order_product['quantity'],
				'price' => (float)$order_product['total'] - (float)$order_product['store_commission_flat'] - (float)$order_product['store_commission_pct'],
				'custom_totals' => $invoice_item_custom_totals
			];

			if ($invoice_item['price'] > 0 || !empty($invoice_item_custom_totals))
				$invoice_items[] = $invoice_item;
		}

		if (empty($invoice_items)) {
			$this->ms_logger->info('InvoiceManager/createStripeSaleInvoiceFromSellerToCustomerForProducts - Invoice contains no invoice items, therefore is not created');
			return false;
		}

		/*$invoice_custom_totals = [];
		$suborder_totals = $this->MsLoader->MsSuborder->getSuborderTotals($order_id, $seller_id);

		foreach ($suborder_totals as $suborder_total) {
			$invoice_custom_totals[$suborder_total['code']] = [
				'titles' => [$this->config->get('config_language_id') => $suborder_total['title']],
				'value' => $suborder_total['value']
			];
		}*/

		$invoice = $this->process('sale', array_merge($metadata, [
			'object' => [
				'type' => 'order',
				'subtype' => 'products',
				'id' => $order_id
			],
			'sender' => "seller.{$seller_id}",
			'recipient' => "customer.{$customer_id}",
			'metadata' => [
				'sender_address' => $this->MsLoader->MsSeller->getSellerMsAddress(['address_id' => $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id, 'name' => 'slr_ms_address', 'single' => true])]),
				'recipient_address' => $customer_address
			],
			'custom_items_data' => $invoice_items,
			'custom_totals_data' => [] //$invoice_custom_totals
		]));

		if (!$invoice) {
			$this->ms_logger->error('InvoiceManager/createStripeSaleInvoiceFromSellerToCustomerForProducts - Error creating sale invoice from seller to customer for products');
			return false;
		}

		if ($notify) {
			$this->MsHooks->triggerAction('system_created_invoice', [
				'producer' => "system.0",
				'consumers' => ["customer.{$customer_id}"],
				'object' => [
					'type' => 'invoice',
					'id' => $invoice->getInvoiceId(),
					'action' => 'created'
				]
			]);
		}

		return $invoice->getInvoiceId();
	}

	/**
	 * Creates seller payout invoice.
	 *
	 * @param	int			$seller_id		Seller id to which payout is done.
	 * @param	float		$amount			Payout amount.
	 * @param	bool		$notify			Whether to create MultiMerch notification.
	 * @param	array		$metadata		Additional data for invoice creation.
	 * @return	bool|int					If invoice is created - invoice_id from database, else false.
	 */
	public function createSellerPayoutInvoice($seller_id, $amount, $notify = true, $metadata = [])
	{
		$this->ms_logger->debug('InvoiceManager/createSellerPayoutInvoice');

		// Invoice item 1
		$titles = [];
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$titles[$language['language_id']] = sprintf($this->language->get('ms_invoice_item_title_seller_payout'), $this->MsLoader->MsSeller->getSellerNickname($seller_id));
		}

		$item_1 = [
			'titles' => $titles,
			'quantity' => 1,
			'price' => $amount
		];

		$invoice = $this->process('payout', array_merge($metadata, [
			'object' => [
				'type' => 'seller',
				'id' => $seller_id
			],
			'sender' => "seller.{$seller_id}",
			'recipient' => "platform.0",
			'metadata' => [
				'sender_address' => $this->MsLoader->MsSeller->getSellerMsAddress(['address_id' => $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id, 'name' => 'slr_ms_address', 'single' => true])]),
				'recipient_address' => $this->MsLoader->MsHelper->getDefaultPlatformAddress()
			],
			'custom_items_data' => [$item_1],
			'custom_totals_data' => []
		]));

		if (!$invoice) {
			$this->ms_logger->error('Error creating seller payout invoice');
			return false;
		}

		return $invoice->getInvoiceId();
	}

	// @todo 9.0 Invoicing: implement custom invoices
	public function createCustomInvoice($data)
	{

	}
}
