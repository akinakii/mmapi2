<?php

namespace MultiMerch\Core\Invoice;

class InvoiceTotal
{
	/**
	 * @var	int	Related invoice id.
	 */
	protected $invoice_id;

	/**
	 * @var string	Invoice total type. Possible values: subtotal, tax, shipping, total.
	 */
	protected $type;

	/**
	 * @var array  Array of titles translated to different languages.
	 */
	protected $titles;

	/**
	 * @var float	Value of the invoice total.
	 */
	protected $value;

	/**
	 * InvoiceTotal constructor.
	 *
	 * @param	string	$type
	 * @param	float	$value
	 * @param	array	$titles
	 */
	public function __construct($type, $value, $titles)
	{
		$this->setType($type);
		$this->setValue($value);
		$this->setTitles($titles);
	}

	/**
	 * @return int
	 */
	public function getInvoiceId()
	{
		return $this->invoice_id;
	}

	/**
	 * @param int $invoice_id
	 */
	public function setInvoiceId($invoice_id)
	{
		$this->invoice_id = $invoice_id;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @param int|null $language_id
	 * @return string|array
	 */
	public function getTitles($language_id = null)
	{
		if ($language_id)
			return isset($this->titles[$language_id]) ? $this->titles[$language_id] : '';

		return $this->titles;
	}

	/**
	 * @param array $titles
	 */
	public function setTitles($titles)
	{
		$this->titles = $titles;
	}

	/**
	 * @return float
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param float $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}
}
