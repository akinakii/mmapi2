<?php

namespace MultiMerch\Core\Invoice;

/**
 * Class Helper
 * @package MultiMerch\Core\Invoice
 */
class Helper extends \Controller
{
	public function __construct($registry)
	{
		parent::__construct($registry);
	}

	/**
	 * Gets an array of default total titles by total type.
	 *
	 * @param $total_type
	 * @return array
	 */
	public function getDefaultTotalTitles($total_type)
	{
		$titles = [];

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			$titles[$language['language_id']] = $this->language->get("ms_invoice_total_title_{$total_type}");
		}

		return $titles;
	}

	/**
	 * Validates and complements the array of custom total titles.
	 *
	 * @param	array	$titles		Custom total titles in different languages.
	 * @return	array				Complemented array of custom total titles.
	 */
	public function complementTotalTitles($titles)
	{
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		$default_language_id = $this->config->get('config_language_id');

		// Copy title from the primary language
		foreach ($languages as $language) {
			if (!isset($titles[$language['language_id']]))
				$titles[$language['language_id']] = isset($titles[$default_language_id]) ? $titles[$default_language_id] : 'Custom total';
		}

		return $titles;
	}
}
