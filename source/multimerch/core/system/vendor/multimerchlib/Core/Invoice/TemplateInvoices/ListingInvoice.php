<?php

namespace MultiMerch\Core\Invoice\TemplateInvoices;

use MultiMerch\Core\Invoice\Invoice;

/**
 * Class ListingInvoice
 * @package MultiMerch\Core\Invoice\TemplateInvoices
 */
class ListingInvoice extends Invoice
{
	public function __construct($registry, $sender, $recipient)
	{
		parent::__construct($registry, 'listing', $sender, $recipient);
	}

	/**
	 * Overrides setInvoiceNo(). Adds prefix to generated invoice number.
	 *
	 * @param	int			$invoice_no
	 */
	public function setInvoiceNo($invoice_no)
	{
		$prefix = 'LI';

		$this->invoice_no = $prefix . $invoice_no;
	}

	/**
	 * Generates invoice title based on invoicing object.
	 *
	 * @return	string	Invoice title.
	 */
	public function getTitle()
	{
		$this->load->language('multiseller/multiseller');

		$is_admin = defined('DIR_CATALOG');

		switch ($this->getObjectType()) {
			case 'product':
			default:
				$product_id = $this->getObjectId();
				$product_name = $this->MsLoader->MsProduct->getProductName($product_id, $this->config->get('config_language_id'));
				$title = $is_admin
					? sprintf(
						$this->language->get('ms_invoice_title_product_listing'),
						$this->url->link('catalog/product/edit', 'product_id=' . $product_id . (isset($this->session->data['token']) ? ('&token=' . $this->session->data['token']) : ''), 'SSL'),
						$product_name
					)
					: sprintf(
						$this->language->get('ms_invoice_title_product_listing'),
						$this->url->link('seller/account-product/update', 'product_id=' . $product_id, 'SSL'),
						$product_name
					);
				break;
		}

		return $title;
	}
}
