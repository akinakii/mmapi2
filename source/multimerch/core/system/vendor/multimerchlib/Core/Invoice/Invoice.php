<?php

namespace MultiMerch\Core\Invoice;

use MultiMerch\Core\Invoice\Factories\InvoiceItemFactory;
use MultiMerch\Core\Invoice\Factories\InvoiceTotalFactory;
use MultiMerch\Core\Invoice\Helper as Helper;

class Invoice
{
	/**
	 * Possible invoice statuses.
	 */
	const STATUS_UNPAID = 1;
	const STATUS_PAID = 2;
	const STATUS_VOIDED = 3;

	/**
	 * @var	int	Invoice id from the database.
	 */
	protected $invoice_id = null;

	/**
	 * @var	int	Unique invoice number.
	 */
	protected $invoice_no = '';

	/**
	 * @var int Related payment id. Indicates whether invoice has been paid.
	 */
	protected $payment_id = null;

	/**
	 * @var	string	Invoice type. Possible values: listing, signup, sale, payout, custom.
	 */
	protected $type = '';

	/**
	 * @var string|null	Object of the invoice. Possible values: product, order, seller.
	 */
	protected $object_type = null;

	/**
	 * @var string|null	Subtype of invoice object of the invoice. Example: object type is `order`, subtype is `products` or `commission`.
	 */
	protected $object_subtype = null;

	/**
	 * @var int|null	Id of the invoice object.
	 */
	protected $object_id = null;

	/**
	 * @var string	Type of the invoice sender. Possible values: seller, customer, platform.
	 */
	protected $sender_type = '';

	/**
	 * @var int|null	Id of the invoice sender. Initially, if `system` is a sender, id is 0, and if `admin` - id is 1.
	 */
	protected $sender_id = null;

	/**
	 * @var string	Type of the invoice recipient. Possible values: admin, seller, customer.
	 */
	protected $recipient_type = '';

	/**
	 * @var int|null	Id of the invoice recipient. If `platform` is a recipient, id is 0.
	 */
	protected $recipient_id = null;

	/**
	 * @var int	Indicates whether the invoice is Unpaid, Paid, Past due.
	 */
	protected $status = self::STATUS_UNPAID;

	/**
	 * @var	float	Total of the invoice, calculated as a sum of invoice totals (@see \MultiMerch\Core\Invoice\InvoiceTotal).
	 */
	protected $total = 0;

	/**
	 * @var	int	Currency id in the OpenCart currencies system.
	 */
	protected $currency_id = 2;

	/**
	 * @var string	ISO 4217 currency code. Must contain exactly 3 letters.
	 */
	protected $currency_code = 'USD';

	/**
	 * @var string|null	Invoice due date. Must be in datetime format ('YYYY-MM-DD HH:MM:SS').
	 */
	protected $date_due = null;

	/**
	 * @var string	Invoice generation date. Must be in datetime format ('YYYY-MM-DD HH:MM:SS').
	 */
	protected $date_generated = null;

	/**
	 * @var string	Invoice update date. Must be in datetime format ('YYYY-MM-DD HH:MM:SS').
	 */
	protected $date_updated = null;

	// @todo 9.0 Invoicing: add soft deletes

	/**
	 * @var string	Additional data related to the invoice.
	 */
	protected $metadata = [];

	/**
	 * @var InvoiceItem[]	Array of the invoice items.
	 */
	protected $items = [];

	/**
	 * @var InvoiceTotal[]	Array of the default and custom invoice totals.
	 */
	protected $totals = [];

	/**
	 * @var	mixed	OpenCart registry.
	 */
	protected $registry;

	/**
	 * @var	Helper	Invoice helper.
	 */
	protected $helper;

	/**
	 * @var	InvoiceItemFactory	Invoice items factory.
	 */
	protected $invoice_item_factory;

	/**
	 * @var	InvoiceTotalFactory	Invoice totals factory.
	 */
	protected $invoice_total_factory;

	/**
	 * Invoice constructor.
	 *
	 * @param	mixed 		$registry
	 * @param	string 		$type
	 * @param	string 		$sender
	 * @param	string		$recipient
	 */
	public function __construct($registry, $type, $sender, $recipient)
	{
		$this->setRegistry($registry);
		$this->setHelper(new Helper($registry));
		$this->setInvoiceItemFactory(new InvoiceItemFactory($registry));
		$this->setInvoiceTotalFactory(new InvoiceTotalFactory($registry));

		$this->setType($type);

		list($sender_type, $sender_id) = explode('.', $sender);
		$this->setSenderType($sender_type);
		$this->setSenderId($sender_id);

		list($recipient_type, $recipient_id) = explode('.', $recipient);
		$this->setRecipientType($recipient_type);
		$this->setRecipientId($recipient_id);
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->getRegistry()->get($name);
	}

	/**
	 * @return int
	 */
	public function getInvoiceId()
	{
		return $this->invoice_id;
	}

	/**
	 * @param int $invoice_id
	 */
	public function setInvoiceId($invoice_id)
	{
		$this->invoice_id = $invoice_id;
	}

	/**
	 * @return int
	 */
	public function getInvoiceNo()
	{
		return $this->invoice_no;
	}

	/**
	 * @param int $invoice_no
	 */
	public function setInvoiceNo($invoice_no)
	{
		$this->invoice_no = $invoice_no;
	}

	/**
	 * @return int
	 */
	public function getPaymentId()
	{
		return $this->payment_id;
	}

	/**
	 * @param int $payment_id
	 */
	public function setPaymentId($payment_id)
	{
		$this->payment_id = $payment_id;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return null|string
	 */
	public function getObjectType()
	{
		return $this->object_type;
	}

	/**
	 * @param null|string $object_type
	 */
	public function setObjectType($object_type)
	{
		$this->object_type = $object_type;
	}

	/**
	 * @return null|string
	 */
	public function getObjectSubtype()
	{
		return $this->object_subtype;
	}

	/**
	 * @param null|string $object_subtype
	 */
	public function setObjectSubtype($object_subtype)
	{
		$this->object_subtype = $object_subtype;
	}

	/**
	 * @return int|null
	 */
	public function getObjectId()
	{
		return $this->object_id;
	}

	/**
	 * @param int|null $object_id
	 */
	public function setObjectId($object_id)
	{
		$this->object_id = $object_id;
	}

	/**
	 * @return string
	 */
	public function getSenderType()
	{
		return $this->sender_type;
	}

	/**
	 * @param string $sender_type
	 */
	public function setSenderType($sender_type)
	{
		$this->sender_type = $sender_type;
	}

	/**
	 * @return int
	 */
	public function getSenderId()
	{
		return $this->sender_id;
	}

	/**
	 * @param int $sender_id
	 */
	public function setSenderId($sender_id)
	{
		$this->sender_id = $sender_id;
	}

	/**
	 * @return string
	 */
	public function getRecipientType()
	{
		return $this->recipient_type;
	}

	/**
	 * @param string $recipient_type
	 */
	public function setRecipientType($recipient_type)
	{
		$this->recipient_type = $recipient_type;
	}

	/**
	 * @return int
	 */
	public function getRecipientId()
	{
		return $this->recipient_id;
	}

	/**
	 * @param int $recipient_id
	 */
	public function setRecipientId($recipient_id)
	{
		$this->recipient_id = $recipient_id;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return float
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * @param float $total
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	}

	/**
	 * @return int
	 */
	public function getCurrencyId()
	{
		return $this->currency_id;
	}

	/**
	 * @param int $currency_id
	 */
	public function setCurrencyId($currency_id)
	{
		$this->currency_id = $currency_id;
	}

	/**
	 * @return string
	 */
	public function getCurrencyCode()
	{
		return $this->currency_code;
	}

	/**
	 * @param string $currency_code
	 */
	public function setCurrencyCode($currency_code)
	{
		$this->currency_code = $currency_code;
	}

	/**
	 * @return string
	 */
	public function getDateDue()
	{
		return $this->date_due;
	}

	/**
	 * @param string $date_due
	 */
	public function setDateDue($date_due)
	{
		$this->date_due = $date_due;
	}

	/**
	 * @return string
	 */
	public function getDateGenerated()
	{
		return $this->date_generated;
	}

	/**
	 * @param string $date_generated
	 */
	public function setDateGenerated($date_generated)
	{
		$this->date_generated = $date_generated;
	}

	/**
	 * @return string
	 */
	public function getDateUpdated()
	{
		return $this->date_updated;
	}

	/**
	 * @param string $date_updated
	 */
	public function setDateUpdated($date_updated)
	{
		$this->date_updated = $date_updated;
	}

	/**
	 * @return string
	 */
	public function getMetadata()
	{
		return $this->metadata;
	}

	/**
	 * @param string $metadata
	 */
	public function setMetadata($metadata)
	{
		$this->metadata = $metadata;
	}

	/**
	 * @return InvoiceItem[]
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param $items_data
	 * @return array
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	public function createItemsFromData($items_data)
	{
		$items = [];

		foreach ($items_data as $item_data) {
			$items[] = $this->getInvoiceItemFactory()->create($item_data);
		}

		return $items;
	}

	/**
	 * @param $items
	 */
	public function setItems($items)
	{
		$this->items = $items;
	}

	/**
	 * @return InvoiceTotal[]
	 */
	public function getTotals()
	{
		return $this->totals;
	}

	/**
	 * Gets default totals data based on invoice items.
	 *
	 * @return	array
	 */
	public function getDefaultTotalsData()
	{
		$totals = [];

		foreach ($this->getItems() as $item) {
			foreach ($item->getTotals() as $total) {
				$type = $total->getType();
				$value = $total->getValue();

				if (!isset($totals[$type]) || false !== strpos($type, 'custom_')) {
					$totals[$type] = [
						'titles' => $this->getHelper()->getDefaultTotalTitles($type),
						'value' => $value
					];
				} else {
					$totals[$type]['value'] += $value;
				}
			}
		}

		return $totals;
	}

	/**
	 * @param $default_totals
	 * @param $custom_totals
	 * @return array
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	public function createTotalsFromData($default_totals, $custom_totals)
	{
		$totals = [];
		$totals_data = [];
		$invoice_total = 0;

		$this->prepareTotals($default_totals, $custom_totals);

		// @todo 9.0 Invoicing: think about different default totals of same type with different title (e.g. taxes)

		foreach ([$default_totals, $custom_totals] as $ts) {
			foreach ($ts as $type => $total) {
				if (!isset($totals_data[$type])) {
					$totals_data[$type] = [
						'titles' => $this->getHelper()->complementTotalTitles($total['titles']),
						'value' => $total['value']
					];
				}

				$invoice_total += $total['value'];
			}
		}

		$totals_data['total'] = [
			'titles' => $this->getHelper()->getDefaultTotalTitles('total'),
			'value' => $invoice_total
		];

		foreach ($totals_data as $type => $total_data) {
			$totals[] = $this->getInvoiceTotalFactory()->create($type, $total_data);
		}

		return $totals;
	}

	private function prepareTotals(&$default_totals, &$custom_totals)
	{
		// Unset custom `mm_shipping_total`, because it is automatically calculated based on seller products (from $default_totals)
		if (isset($custom_totals['mm_shipping_total'])) unset($custom_totals['mm_shipping_total']);

		// Unset `sub_total` from custom totals, because it is automatically calculated based on seller products (from $default_totals)
		if (isset($custom_totals['sub_total'])) unset($custom_totals['sub_total']);

		// Unset `total` from both custom and default totals, because it is calculated from scratch as a sum of all the totals
		if (isset($default_totals['total'])) unset($default_totals['total']);
		if (isset($custom_totals['total'])) unset($custom_totals['total']);
	}

	/**
	 * @param $totals
	 */
	public function setTotals($totals)
	{
		foreach ($totals as $total) {
			if (!$total)
				continue;

			$this->totals[] = $total;

			if ('total' === $total->getType())
				$this->setTotal($total->getValue());
		}
	}

	/**
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * @param mixed $registry
	 */
	public function setRegistry($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * @return Helper
	 */
	public function getHelper()
	{
		return $this->helper;
	}

	/**
	 * @param Helper $helper
	 */
	public function setHelper($helper)
	{
		$this->helper = $helper;
	}

	/**
	 * @return InvoiceItemFactory
	 */
	public function getInvoiceItemFactory()
	{
		return $this->invoice_item_factory;
	}

	/**
	 * @param InvoiceItemFactory $invoice_item_factory
	 */
	public function setInvoiceItemFactory($invoice_item_factory)
	{
		$this->invoice_item_factory = $invoice_item_factory;
	}

	/**
	 * @return InvoiceTotalFactory
	 */
	public function getInvoiceTotalFactory()
	{
		return $this->invoice_total_factory;
	}

	/**
	 * @param InvoiceTotalFactory $invoice_total_factory
	 */
	public function setInvoiceTotalFactory($invoice_total_factory)
	{
		$this->invoice_total_factory = $invoice_total_factory;
	}

	/**
	 * Generates invoice title based on invoicing object.
	 *
	 * Should be overridden in all inherit classes.
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return '-';
	}
}
