<?php

namespace MultiMerch\Core\Invoice\TemplateInvoices;

use MultiMerch\Core\Invoice\Invoice;

class SaleInvoice extends Invoice
{
	public function __construct($registry, $sender, $recipient)
	{
		parent::__construct($registry, 'sale', $sender, $recipient);
	}

	/**
	 * Overrides setInvoiceNo(). Adds prefix to generated invoice number.
	 *
	 * @param	int			$invoice_no
	 */
	public function setInvoiceNo($invoice_no)
	{
		$prefix = 'SI';

		$this->invoice_no = $prefix . $invoice_no;
	}

	/**
	 * Generates invoice title based on invoicing object.
	 *
	 * @return	string	Invoice title.
	 */
	public function getTitle()
	{
		$this->load->language('multiseller/multiseller');

		$is_admin = defined('DIR_CATALOG');

		switch ($this->getObjectType()) {
			case 'order':
			default:
				$order_id = $this->getObjectId();

				if ('commission' === $this->getObjectSubtype()) {
					$title = $is_admin
						? sprintf(
							$this->language->get('ms_invoice_title_sale_order_commission'),
							$this->url->link('sale/order/info', 'order_id=' . $order_id . (isset($this->session->data['token']) ? ('&token=' . $this->session->data['token']) : ''), 'SSL'),
							$order_id
						)
						: sprintf(
							$this->language->get('ms_invoice_title_sale_order_commission'),
							$this->url->link('seller/account-order/viewOrder', 'order_id=' . $order_id, 'SSL'),
							$order_id
						);
				} else {
					$title = $is_admin
						? sprintf(
							$this->language->get('ms_invoice_title_sale_order_products'),
							$this->url->link('sale/order/info', 'order_id=' . $order_id . (isset($this->session->data['token']) ? ('&token=' . $this->session->data['token']) : ''), 'SSL'),
							$order_id
						)
						: sprintf(
							$this->language->get('ms_invoice_title_sale_order_products'),
							$this->url->link('seller/account-order/viewOrder', 'order_id=' . $order_id, 'SSL'),
							$order_id
						);
				}
				break;
		}

		return $title;
	}
}
