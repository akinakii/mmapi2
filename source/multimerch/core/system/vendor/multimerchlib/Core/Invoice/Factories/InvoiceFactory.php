<?php

namespace MultiMerch\Core\Invoice\Factories;

use MultiMerch\Core\Invoice\Invoice as Invoice;
use MultiMerch\Core\Invoice\TemplateInvoices\CustomInvoice;
use MultiMerch\Core\Invoice\TemplateInvoices\ListingInvoice;
use MultiMerch\Core\Invoice\TemplateInvoices\PayoutInvoice;
use MultiMerch\Core\Invoice\TemplateInvoices\SaleInvoice;
use MultiMerch\Core\Invoice\TemplateInvoices\SignupInvoice;
use MultiMerch\Module\Errors\Generic;

/**
 * Class InvoiceFactory
 * @package MultiMerch\Core\Invoice\Factories
 */
class InvoiceFactory
{
	/**
	 * @var	mixed	OpenCart registry.
	 */
	private $registry;

	/**
	 * InvoiceFactory constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->registry->get($name);
	}

	/**
	 * Creates Invoice instance.
	 *
	 * @param	string	$type			Invoice type: listing, signup, sale, payout, custom.
	 * @param	array	$invoice_data	Settings needed for invoice creation.
	 * @return	Invoice					Invoice instance.
	 * @throws	Generic
	 */
	public function create($type, $invoice_data)
	{
		// Validate required parameters
		$this->validate($invoice_data);

		switch ($type) {
			case 'listing':
				$invoice = new ListingInvoice($this->registry, $invoice_data['sender'], $invoice_data['recipient']);
				break;

			case 'payout':
				$invoice = new PayoutInvoice($this->registry, $invoice_data['sender'], $invoice_data['recipient']);
				break;

			case 'sale':
				$invoice = new SaleInvoice($this->registry, $invoice_data['sender'], $invoice_data['recipient']);
				break;

			case 'signup':
				$invoice = new SignupInvoice($this->registry, $invoice_data['sender'], $invoice_data['recipient']);
				break;

			case 'custom':
			default:
				$invoice = new CustomInvoice($this->registry, $invoice_data['sender'], $invoice_data['recipient']);
				break;
		}

		if (isset($invoice_data['invoice_id']))
			$invoice->setInvoiceId($invoice_data['invoice_id']);

		// @todo 9.0 Invoicing
		/*if (isset($invoice_data['invoice_no']))
			$invoice->setInvoiceNo($invoice_data['invoice_no']);*/

		if (isset($invoice_data['payment_id']))
			$invoice->setPaymentId($invoice_data['payment_id']);

		if (isset($invoice_data['object'])) {
			if (isset($invoice_data['object']['type']))
				$invoice->setObjectType($invoice_data['object']['type']);

			if (isset($invoice_data['object']['subtype']))
				$invoice->setObjectSubtype($invoice_data['object']['subtype']);

			if (isset($invoice_data['object']['id']))
				$invoice->setObjectId($invoice_data['object']['id']);
		}

		if (isset($invoice_data['status']))
			$invoice->setStatus($invoice_data['status']);

		$invoice->setCurrencyId(isset($invoice_data['currency_id']) ? $invoice_data['currency_id'] : $this->currency->getId($this->config->get('config_currency')));
		$invoice->setCurrencyCode(isset($invoice_data['currency_code']) ? $invoice_data['currency_code'] : $this->config->get('config_currency'));

		if (isset($invoice_data['date_due']))
			$invoice->setDateDue($invoice_data['date_due']);

		if (isset($invoice_data['date_generated']))
			$invoice->setDateGenerated($invoice_data['date_generated']);

		if (isset($invoice_data['date_updated']))
			$invoice->setDateUpdated($invoice_data['date_updated']);

		if (isset($invoice_data['metadata']))
			$invoice->setMetadata($invoice_data['metadata']);

		// Source of items:
		// 1) Data from the database
		// 2) Data set by user
		$items = isset($invoice_data['items'])
			? $invoice_data['items']
			: $invoice->createItemsFromData(isset($invoice_data['custom_items_data']) ? $invoice_data['custom_items_data'] : []);

		$invoice->setItems($items);

		// Source of totals:
		// 1) Data from the database
		// 2) Totals based on invoice items and custom totals set by user
		$totals = isset($invoice_data['totals'])
			? $invoice_data['totals']
			: $invoice->createTotalsFromData($invoice->getDefaultTotalsData(), isset($invoice_data['custom_totals_data']) ? $invoice_data['custom_totals_data'] : []);

		$invoice->setTotals($totals);

		return $invoice;
	}

	/**
	 * Validates received parameters.
	 *
	 * @param	array	$invoice_data	Settings needed for invoice creation.
	 * @throws	Generic
	 */
	private function validate($invoice_data)
	{
		$required_parameters = ['sender', 'recipient'];

		foreach ($required_parameters as $p) {
			if (!isset($invoice_data[$p]))
				throw new Generic("InvoiceFactory - Required parameter `{$p}` is missing!");
		}
	}
}
