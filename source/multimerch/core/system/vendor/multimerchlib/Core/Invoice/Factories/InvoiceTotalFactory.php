<?php

namespace MultiMerch\Core\Invoice\Factories;

use MultiMerch\Core\Invoice\InvoiceTotal;
use MultiMerch\Module\Errors\Generic;

/**
 * Class InvoiceTotalFactory
 * @package MultiMerch\Core\Invoice\Factories
 */
class InvoiceTotalFactory
{
	/**
	 * @var	mixed	OpenCart registry.
	 */
	private $registry;

	/**
	 * InvoiceTotalFactory constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * Creates InvoiceTotal instance.
	 *
	 * @param	string	$type					Invoice total type.
	 * @param	array	$invoice_total_data		Invoice total data.
	 * @return	InvoiceTotal|null				Returns null if total value is zero.
	 * @throws	Generic
	 */
	public function create($type, $invoice_total_data)
	{
		// Validate required parameters
		$this->validate($invoice_total_data);

		// If value is zero or below, do not create invoice total
		if ((float)$invoice_total_data['value'] === 0)
			return null;

		$invoice_total = new InvoiceTotal($type, $invoice_total_data['value'], $invoice_total_data['titles']);

		return $invoice_total;
	}

	/**
	 * Validates received parameters.
	 *
	 * @param	array	$invoice_total_data	Settings needed for invoice total creation.
	 * @throws	Generic
	 */
	private function validate($invoice_total_data)
	{
		$required_parameters = ['value', 'titles'];

		foreach ($required_parameters as $p) {
			if (!isset($invoice_total_data[$p]))
				throw new Generic("InvoiceTotalFactory - Required parameter `{$p}` is missing!");
		}
	}
}
