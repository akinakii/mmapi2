<?php

namespace MultiMerch\Core\Invoice\Factories;

use MultiMerch\Core\Invoice\InvoiceItem;
use MultiMerch\Module\Errors\Generic;

/**
 * Class InvoiceItemFactory
 * @package MultiMerch\Core\Invoice\Factories
 */
class InvoiceItemFactory
{
	/**
	 * @var	mixed	OpenCart registry.
	 */
	private $registry;

	/**
	 * InvoiceItemFactory constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * Creates InvoiceItem instance.
	 *
	 * @param	array		$invoice_item_data	Settings needed for invoice creation.
	 * @return	InvoiceItem
	 * @throws	Generic
	 */
	public function create($invoice_item_data)
	{
		// Validate required parameters
		$this->validate($invoice_item_data);

		$quantity = $invoice_item_data['quantity'];
		$price = $invoice_item_data['price'];
		$subtotal = $quantity * $price;

		$invoice_item = new InvoiceItem($this->registry, $quantity, $price, $invoice_item_data['titles']);

		if (isset($invoice_data['metadata']))
			$invoice_item->setMetadata($invoice_item_data['metadata']);

		// Source of totals:
		// 1) Data from the database
		// 2) Totals based on invoice items and custom totals set by user
		$totals = isset($invoice_item_data['totals'])
			? $invoice_item_data['totals']
			: $invoice_item->createTotalsFromData($subtotal, isset($invoice_item_data['custom_totals']) ? $invoice_item_data['custom_totals'] : []);

		$invoice_item->setTotals($totals);

		return $invoice_item;
	}

	/**
	 * Validates received parameters.
	 *
	 * @param	array	$invoice_item_data	Settings needed for invoice item creation.
	 * @throws	Generic
	 */
	private function validate($invoice_item_data)
	{
		$required_parameters = ['quantity', 'price', 'titles'];

		foreach ($required_parameters as $p) {
			if (!isset($invoice_item_data[$p]))
				throw new Generic("InvoiceItemFactory - Required parameter `{$p}` is missing!");
		}
	}
}
