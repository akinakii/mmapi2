<?php

namespace MultiMerch\Core\Notification\Messages;

use MultiMerch\Core\Notification\Message;

class MailMessage extends Message
{
	private $subject;

	private $l_template_subject;

	public function __construct($registry, $channel = 'onsite', $producer = 'system.0', $consumer = 'system.0', array $object = [])
	{
		parent::__construct($registry, $channel, $producer, $consumer, $object);

		$this->load->language("multimerch/notification/{$this->channel}");

		$event = "{$this->getProducerType()}_{$this->object['action']}_{$this->object['type']}" . (!empty($this->object['subtype']) ? "_{$this->object['subtype']}" : "");

		// Mail subject template
		$this->l_template_subject = $this->language->get("msn_{$this->channel}_subject_{$event}");

		// Mail text template
		$this->l_template = $this->language->get("msn_{$this->channel}_{$event}");

		if (method_exists($this, $event)) {
			$this->$event();
		}
	}

	/**
	 * @return mixed
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param mixed $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	/**
	 * @return mixed
	 */
	public function getLTemplateSubject()
	{
		return $this->l_template_subject;
	}

	/**
	 * @param mixed $l_template_subject
	 */
	public function setLTemplateSubject($l_template_subject)
	{
		$this->l_template_subject = $l_template_subject;
	}


	// Admin events

	/**
	 * Generates message for `admin_created_message` event.
	 *
	 * From: admin
	 * To: customer, seller
	 */
	private function admin_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$sender_name,
			$conversation['title'],
			$message['message']
		));
	}

	/**
	 * Generates message for `admin_created_payout` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_created_payout()
	{
		$this->setSubject(sprintf(
			$this->l_template_subject,
			$this->config->get('config_name')
		));

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name')
		));
	}

	/**
	 * Generates message for `admin_updated_account_status` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_updated_account_status()
	{
		$seller_status_id = !empty($this->object['metadata']['seller_status_id']) ? $this->object['metadata']['seller_status_id'] : null;
		$seller_status_name = $seller_status_id ? $this->language->get('ms_seller_status_' . $seller_status_id) : '';

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name'),
			$seller_status_name
		));
	}

	/**
	 * Generates message for `admin_updated_order_status` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_updated_order_status()
	{
		$order = $this->MsLoader->MsOrderData->ocGetOrder($this->object['id']);

		$order_products = $this->MsLoader->MsOrderData->getOrderProducts(['order_id' => $this->object['id'], 'seller_id' => $this->getConsumerId()]);

		$parts = [];
		foreach ($order_products as $order_product) {
			$parts[] = "{$order_product['quantity']} x {$order_product['name']}";
		}

		$order_products_text = implode("\n", $parts);

		$this->setSubject(sprintf(
			$this->l_template_subject,
			$this->object['id'],
			$this->config->get('config_owner')
		));

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name'),
			$this->config->get('config_owner'),
			$this->object['id'],
			$order_products_text,
			$order['order_status'],
			$order['comment']
		));
	}

	/**
	 * Generates message for `admin_updated_order_status` event.
	 *
	 * From: admin
	 * To: seller, customer
	 */
	private function admin_updated_suborder_status()
	{
		switch ($this->getConsumerType()) {
			// @todo 9.0: different notification for customer
			case 'customer':
			case 'seller':
			default:
				$suborder_products = $this->MsLoader->MsOrderData->getOrderProducts([
					'order_id' => $this->object['id'],
					'seller_id' => $this->object['metadata']['seller_id']
				]);

				$suborder_products_text_parts = [];
				foreach ($suborder_products as $suborder_product) {
					$suborder_products_text_parts[] = "{$suborder_product['quantity']} x {$suborder_product['name']}";
				}

				$suborder_products_text = implode("\n", $suborder_products_text_parts);

				$suborder_status = '';
				if (!empty($this->object['metadata']['suborder_status_id'])) {
					$suborder_status = $this->MsLoader->MsSuborderStatus->getSubStatusName(['order_status_id' => $this->object['metadata']['suborder_status_id']]);
				}

				$this->setSubject(sprintf(
					$this->l_template_subject,
					$this->object['id'],
					$this->config->get('config_owner')
				));

				$this->setText(sprintf(
					$this->l_template,
					$this->config->get('config_name'),
					$this->config->get('config_owner'),
					$this->object['id'], // . ' (' . $this->MsLoader->MsSeller->getSellerNickname($this->object['metadata']['seller_id']) . ')',
					$suborder_products_text,
					$suborder_status,
					!empty($this->object['metadata']['comment']) ? $this->object['metadata']['comment'] : ''
				));

				break;
		}
	}

	// Customer events

	/**
	 * Generates message for `customer_created_account` event.
	 *
	 * From: customer
	 * To: admin
	 */
	private function customer_created_account()
	{
		$this->load->model('account/customer');
		$customer = $this->model_account_customer->getCustomer($this->object['id']);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name'),
			sprintf('%s %s', $customer['firstname'], $customer['lastname']),
			$customer['email']
		));
	}

	/**
	 * Generates message for `customer_created_message` event.
	 *
	 * From: customer
	 * To: seller + admin (if participant)
	 */
	private function customer_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$sender_name,
			$conversation['title'],
			$message['message']
		));
	}

	/**
	 * Generates message for `customer_created_order` event.
	 *
	 * From: customer
	 * To: seller, admin
	 */
	private function customer_created_order()
	{
		if ('admin' === $this->getConsumerType())
			return false;

		$order = $this->MsLoader->MsOrderData->ocGetOrder($this->object['id']);

		$this->load->model('account/customer');
		$customer = $this->model_account_customer->getCustomer($order['customer_id']);

		$order_products = $this->MsLoader->MsOrderData->getOrderProducts(['order_id' => $this->object['id'], 'seller_id' => $this->getConsumerId()]);

		$parts = [];
		foreach ($order_products as $order_product) {
			$parts[] = "{$order_product['quantity']} x {$order_product['name']}";
		}

		$order_products_text = implode("\n", $parts);

		$this->setSubject(sprintf(
			$this->l_template_subject,
			sprintf('%s %s', $customer['firstname'], $customer['lastname']),
			$this->config->get('config_name')
		));

		$this->setText(sprintf(
			$this->l_template,
			sprintf('%s %s', $customer['firstname'], $customer['lastname']),
			$this->config->get('config_name'),
			$this->object['id'],
			$order_products_text,
			$order['order_status'],
			$order['comment']
		));
	}
	
	/**
	 * Generates message for `guest_created_order` event.
	 *
	 * From: guest
	 * To: seller
	 */
	private function guest_created_order()
	{
		if ('admin' === $this->getConsumerType())
			return false;

		$order = $this->MsLoader->MsOrderData->ocGetOrder($this->object['id']);

		//$this->load->model('account/customer');
		//$customer = $this->model_account_customer->getCustomer($order['customer_id']);

		$order_products = $this->MsLoader->MsOrderData->getOrderProducts(['order_id' => $this->object['id'], 'seller_id' => $this->getConsumerId()]);

		$parts = [];
		foreach ($order_products as $order_product) {
			$parts[] = "{$order_product['quantity']} x {$order_product['name']}";
		}

		$order_products_text = implode("\n", $parts);

		$this->setSubject(sprintf(
			$this->l_template_subject,
			$this->config->get('config_name')
		));

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name'),
			$this->object['id'],
			$order_products_text,
			$order['order_status'],
			$order['comment']
		));
	}

	/**
	 * Generates message for `customer_created_ms_review` event.
	 *
	 * From: customer
	 * To: seller
	 */
	private function customer_created_ms_review()
	{
		$ms_review = $this->MsLoader->MsReview->getReviews(['review_id' => $this->object['id'], 'single' => true]);

		$this->setSubject($this->l_template_subject);
		$this->setText(sprintf(
			$this->l_template,
			$ms_review['product_name'],
			$this->url->link('product/product', 'product_id=' . $ms_review['product_id']),
			$ms_review['product_name']
		));
	}

	// Seller events

	/**
	 * Generates message for `seller_created_account` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_created_account()
	{
		$seller = $this->MsLoader->MsSeller->getSeller($this->object['id']);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$this->config->get('config_name'),
			$seller['name'],
			$seller['ms.nickname'],
			$seller['c.email']
		));
	}

	/**
	 * Generates message for `seller_created_message` event.
	 *
	 * From: seller
	 * To: customer + admin (if participant)
	 */
	private function seller_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$sender_name,
			$conversation['title'],
			$message['message']
		));
	}

	/**
	 * Generates message for `seller_created_product` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_created_product()
	{
		$product = $this->MsLoader->MsProduct->getProduct($this->object['id']);
		$seller = $this->MsLoader->MsSeller->getSeller($product['seller_id']);

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$product['languages'][$this->config->get('config_language_id')]['name'],
			$seller['ms.nickname']
		));
	}

	/**
	 * Generates message for `seller_updated_invoice_status` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_updated_invoice_status()
	{
		$invoice = $this->MsLoader->MsInvoice->get(['invoice_id' => $this->object['id'], 'single' => true]);

		$seller_nickname = '';
		if (!empty($invoice)) {
			$seller_id = ('seller' === $invoice->getRecipientType()) ? $invoice->getRecipientId() : null;
			$seller_nickname = $this->MsLoader->MsSeller->getSellerNickname($seller_id);
		}

		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$seller_nickname,
			$this->object['id']
		));
	}

	// System events

	/**
	 * Generates message for `system_created_invoice` event.
	 *
	 * From: system
	 * To: seller
	 */
	private function system_created_invoice()
	{
		$this->setSubject($this->l_template_subject);

		$this->setText(sprintf(
			$this->l_template,
			$this->object['id']
		));
	}

	/**
	 * Generates message for `system_finished_import` event.
	 *
	 * From: system
	 * To: seller
	 */
	private function system_finished_import()
	{
		$this->setSubject($this->l_template_subject);

		$this->setText($this->l_template);
	}
}
