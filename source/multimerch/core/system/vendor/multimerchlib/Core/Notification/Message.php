<?php

namespace MultiMerch\Core\Notification;

class Message
{
	protected $registry;
	protected $is_admin = false;
	protected $is_object_deleted = false;

	protected $channel;
	protected $producer_id;
	protected $producer_type;
	protected $consumer_id;
	protected $consumer_type;
	protected $object;

	protected $l_template;

	protected $text;

	public function __construct($registry, $channel = 'onsite', $producer = 'system.0', $consumer = 'system.0', array $object = [])
	{
		$this->registry = $registry;
		$this->channel = $channel;
		$this->object = $object;

		list($this->producer_type, $this->producer_id) = explode('.', $producer);
		list($this->consumer_type, $this->consumer_id) = explode('.', $consumer);

		// Detect whether it is admin-end, or front-end
		if (defined('DIR_CATALOG'))
			$this->is_admin = true;
	}

	public function __get($name)
	{
		return $this->getRegistry()->get($name);
	}

	/**
	 * @return mixed
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @param mixed $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}

	/**
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * @param mixed $registry
	 */
	public function setRegistry($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * @return string
	 */
	public function getChannel()
	{
		return $this->channel;
	}

	/**
	 * @param string $channel
	 */
	public function setChannel($channel)
	{
		$this->channel = $channel;
	}

	/**
	 * @return mixed
	 */
	public function getProducerId()
	{
		return $this->producer_id;
	}

	/**
	 * @param mixed $producer_id
	 */
	public function setProducerId($producer_id)
	{
		$this->producer_id = $producer_id;
	}

	/**
	 * @return mixed
	 */
	public function getProducerType()
	{
		return $this->producer_type;
	}

	/**
	 * @param mixed $producer_type
	 */
	public function setProducerType($producer_type)
	{
		$this->producer_type = $producer_type;
	}

	/**
	 * @return mixed
	 */
	public function getConsumerId()
	{
		return $this->consumer_id;
	}

	/**
	 * @param mixed $consumer_id
	 */
	public function setConsumerId($consumer_id)
	{
		$this->consumer_id = $consumer_id;
	}

	/**
	 * @return mixed
	 */
	public function getConsumerType()
	{
		return $this->consumer_type;
	}

	/**
	 * @param mixed $consumer_type
	 */
	public function setConsumerType($consumer_type)
	{
		$this->consumer_type = $consumer_type;
	}

	/**
	 * @return array
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @param array $object
	 */
	public function setObject($object)
	{
		$this->object = $object;
	}

	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->is_admin;
	}

	/**
	 * @param bool $is_admin
	 */
	public function setIsAdmin($is_admin)
	{
		$this->is_admin = $is_admin;
	}

	/**
	 * @return bool
	 */
	public function isObjectDeleted()
	{
		return $this->is_object_deleted;
	}

	/**
	 * @param bool $is_object_deleted
	 */
	public function setIsObjectDeleted($is_object_deleted)
	{
		$this->is_object_deleted = $is_object_deleted;
	}
}
