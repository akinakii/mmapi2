<?php

namespace MultiMerch\Core\Notification;

use http\Exception\InvalidArgumentException;
use MultiMerch\Core\Notification\Interfaces\NotificationInterface;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer
{
	protected $notification_manager;

	public function __construct(Manager $manager)
	{
		$this->notification_manager = $manager;
	}

	public function execute(AMQPMessage $message)
	{
		$notification = @unserialize($message->body);

		if (!$notification instanceof NotificationInterface)
			throw new InvalidArgumentException('The body of the AMQP must be a serialized instance of MultiMerch\Core\Notification\Interfaces\NotificationInterface');

		return $this->notification_manager->send($notification);
	}
}
