<?php

namespace MultiMerch\Core\Notification;

use MultiMerch\Core\Notification\Interfaces\NotificationInterface;

class Notification implements NotificationInterface
{
	/**
	 * @var mixed $registry OpenCart registry.
	 */
	protected $registry;

	/**
	 * @var string $message
	 */
	protected $message;

	/**
	 * @var array $parameters
	 */
	protected $parameters = [];

	/**
	 * Notification constructor.
	 *
	 * @param $message
	 * @param array $parameters
	 */
	public function __construct($registry, $message = '', array $parameters = [])
	{
		$this->registry = $registry;
		$this->message = $message;
		$this->parameters = $parameters;
	}

	/**
	 * Returns a message of this notification.
	 *
	 * @return mixed
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Sets the message of this notification.
	 *
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * Returns parameters of this notification.
	 *
	 * @return array
	 */
	public function getParameters()
	{
		return $this->parameters;
	}

	/**
	 * Returns a parameter.
	 *
	 * @param string $id
	 * @return mixed
	 */
	public function getParameter($id)
	{
		if (isset($this->parameters[$id]))
			return $this->parameters[$id];
	}

	/**
	 * Sets a parameter.
	 *
	 * @param string $id
	 * @param string $value
	 */
	public function setParameter($id, $value)
	{
		$this->parameters[$id] = $value;
	}
}
