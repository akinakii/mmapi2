<?php

namespace MultiMerch\Core\Notification\Notifications;

use MultiMerch\Core\Notification\Interfaces\DatabaseNotificationInterface;
use MultiMerch\Core\Notification\Notification;

class DatabaseNotification extends Notification implements DatabaseNotificationInterface
{
	/**
	 * @var	string	Type of a notification producer.
	 */
	protected $producer_type = null;

	/**
	 * @var	int		Id of a notification producer.
	 */
	protected $producer_id = 0;

	/**
	 * @var	string	Type of a notification consumer.
	 */
	protected $consumer_type = null;

	/**
	 * @var int		Id of a notification consumer.
	 */
	protected $consumer_id = 0;

	/**
	 * @var	bool	Flag to detect whether a notification has been read by a consumer.
	 */
	protected $read = false;

	/**
	 * @var	mixed	Object of a notification.
	 */
	protected $object = ['type' => null, 'subtype' => null, 'id' => 0, 'action' => null, 'metadata' => null];

	/**
	 * @var	array	Array of channel names (strings) through which a notification is transmitted.
	 */
	protected $channels = [];

	/**
	 * DatabaseNotification constructor.
	 *
	 * @param	mixed	$registry		OpenCart registry.
	 * @param	string	$message		Notification message.
	 * @param	array	$parameters		Additional parameters for notification creation.
	 */
	public function __construct($registry, $message, array $parameters = [])
	{
		parent::__construct($registry, $message, $parameters);

		list($this->producer_type, $this->producer_id) = explode('.', isset($parameters['producer']) ? $parameters['producer'] : 'system.0');
		list($this->consumer_type, $this->consumer_id) = explode('.', isset($parameters['consumer']) ? $parameters['consumer'] : 'system.0');

		if (!empty($parameters['object'])) {
			$this->object['type'] = isset($parameters['object']['type']) ? $parameters['object']['type'] : null;
			$this->object['subtype'] = isset($parameters['object']['subtype']) ? $parameters['object']['subtype'] : null;
			$this->object['id'] = isset($parameters['object']['id']) ? $parameters['object']['id'] : 0;
			$this->object['action'] = isset($parameters['object']['action']) ? $parameters['object']['action'] : null;
			$this->object['metadata'] = isset($parameters['object']['metadata']) ? $parameters['object']['metadata'] : null;
		}

		if (!empty($parameters['channels'])) {
			$this->read = in_array('mail', $parameters['channels']);
			$this->channels = $parameters['channels'];
		}
	}

	/**
	 * @return string
	 */
	public function getProducerType()
	{
		return $this->producer_type;
	}

	/**
	 * @param string $producer_type
	 */
	public function setProducerType($producer_type)
	{
		$this->producer_type = $producer_type;
	}

	/**
	 * @return int
	 */
	public function getProducerId()
	{
		return $this->producer_id;
	}

	/**
	 * @param int $producer_id
	 */
	public function setProducerId($producer_id)
	{
		$this->producer_id = $producer_id;
	}

	/**
	 * @return string
	 */
	public function getConsumerType()
	{
		return $this->consumer_type;
	}

	/**
	 * @param string $consumer_type
	 */
	public function setConsumerType($consumer_type)
	{
		$this->consumer_type = $consumer_type;
	}

	/**
	 * @return int
	 */
	public function getConsumerId()
	{
		return $this->consumer_id;
	}

	/**
	 * @param int $consumer_id
	 */
	public function setConsumerId($consumer_id)
	{
		$this->consumer_id = $consumer_id;
	}

	/**
	 * @return bool
	 */
	public function isRead()
	{
		return $this->read;
	}

	/**
	 * @param bool $read
	 */
	public function setRead($read)
	{
		$this->read = $read;
	}

	/**
	 * @return mixed
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @param mixed $object
	 */
	public function setObject($object)
	{
		$this->object = $object;
	}

	/**
	 * @return array
	 */
	public function getChannels()
	{
		return $this->channels;
	}

	/**
	 * @param array $channels
	 */
	public function setChannels($channels)
	{
		$this->channels = $channels;
	}
}
