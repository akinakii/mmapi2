<?php

namespace MultiMerch\Core\Notification\Channels;

use MultiMerch\Core\Notification\Interfaces\ChannelInterface;
use MultiMerch\Core\Notification\Interfaces\NotificationInterface;
use MultiMerch\Core\Notification\Notifications\DatabaseNotification;

class DatabaseChannel implements ChannelInterface
{
	/**
	 * @var	mixed	$registry	OpenCart registry.
	 */
	protected $registry;

	/**
	 * RabbitMQChannel constructor.
	 *
	 * @param	mixed	$registry
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;
	}

	/**
	 * Return OpenCart registry.
	 *
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * Detects whether notification can be transmitted through this channel.
	 *
	 * @param	NotificationInterface	$notification
	 * @return	bool
	 */
	public function shouldTransmit(NotificationInterface $notification)
	{
		return $notification instanceof DatabaseNotification;
	}

	/**
	 * Transmits notification through this channel.
	 *
	 * @param NotificationInterface $notification
	 */
	public function transmit(NotificationInterface $notification)
	{
		$this->registry->get('MsLoader')->MsNotification->createNotification([
			'producer_type' => $notification->getProducerType(),
			'producer_id' => $notification->getProducerId(),
			'consumer_type' => $notification->getConsumerType(),
			'consumer_id' => $notification->getConsumerId(),
			'read' => $notification->isRead(),
			'object' => $notification->getObject(),
			'channels' => $notification->getChannels()
		]);

		$this->registry->get('MsLoader')->MsNotification->updateNewNotificationsCount(
			$notification->getConsumerType(),
			$notification->getConsumerId()
		);
	}
}
