<?php

namespace MultiMerch\Core\Notification;

use MultiMerch\Core\Notification\Interfaces\ChannelInterface;
use MultiMerch\Core\Notification\Interfaces\NotificationInterface;

class Manager
{
	protected $registry;
	protected $channels;

	public function __construct($registry, array $channels = [])
	{
		$this->registry = $registry;
		$this->channels = $channels;
	}

	public function getRegistry()
	{
		return $this->registry;
	}

	public function setRegistry($registry)
	{
		$this->registry = $registry;
	}

	public function getChannels()
	{
		return $this->channels;
	}

	public function addChannel(ChannelInterface $channel)
	{
		if (!in_array($channel, $this->channels))
			$this->channels[] = $channel;
	}

	public function addChannels($channels)
	{
		foreach ($channels as $channel) {
			if ($channel instanceof ChannelInterface && !in_array($channel, $this->channels))
				$this->channels[] = $channel;
		}
	}

	public function send(NotificationInterface $notification, ChannelInterface $channel)
	{
		if ($channel->shouldTransmit($notification)) {
			$this->getRegistry()->get('ms_logger')->debug(sprintf("Transmitting notification through %s channel", get_class($channel)));

			if ($channel->transmit($notification))
				return true;
		}

		return true;
	}
}
