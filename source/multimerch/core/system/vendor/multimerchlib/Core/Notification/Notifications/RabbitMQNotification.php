<?php

namespace MultiMerch\Core\Notification\Notifications;

use MultiMerch\Core\Notification\Interfaces\RabbitMQNotificationInterface;
use MultiMerch\Core\Notification\Notification;

class RabbitMQNotification extends Notification implements RabbitMQNotificationInterface
{
	protected $routing_key;

	public function __construct($registry, $message, array $parameters = [])
	{
		parent::__construct($registry, $message, $parameters);

		$this->routing_key = isset($parameters['consumer']) ? $parameters['consumer'] : 'admin.0';
	}

	/**
	 * @return mixed|string
	 */
	public function getRoutingKey()
	{
		return $this->routing_key;
	}

	/**
	 * @param mixed|string $routing_key
	 */
	public function setRoutingKey($routing_key)
	{
		$this->routing_key = $routing_key;
	}
}
