<?php

namespace MultiMerch\Core\Notification\Notifications;

use MultiMerch\Core\Notification\Interfaces\MailNotificationInterface;
use MultiMerch\Core\Notification\Notification;

class MailNotification extends Notification implements MailNotificationInterface
{
	protected $to;
	protected $from;
	protected $sender;
	protected $subject;

	public function __construct($registry, $message, array $parameters = [])
	{
		parent::__construct($registry, $message, $parameters);

		if (isset($parameters['producer'])) {
			list($producer_type, $producer_id) = explode('.', $parameters['producer']);

			$producer = $this->getParticipant($producer_type, $producer_id);

			// Mails must always be sent from the Marketplace
			$this->from = $this->registry->get('config')->get('config_email');
			$this->sender = $this->registry->get('config')->get('config_name');
		}

		if (isset($parameters['consumer'])) {
			list($consumer_type, $consumer_id) = explode('.', $parameters['consumer']);

			$consumer = $this->getParticipant($consumer_type, $consumer_id);

			$this->to = $consumer['email'];
		}

		if (isset($parameters['subject'])) {
			$this->subject = $parameters['subject'];
		}
	}

	private function getParticipant($type, $id)
	{
		$participant = [
			'email' => '',
			'name' => ''
		];

		switch ($type) {
			case 'seller':
				$seller = $this->registry->get('MsLoader')->MsSeller->getSeller($id);

				$participant['email'] = $seller['c.email'];
				$participant['name'] = $seller['ms.nickname'];

				break;

			case 'customer':
				if (defined('DIR_CATALOG')) {
					$this->registry->get('load')->model('customer/customer');
					$customer = $this->registry->get('model_customer_customer')->getCustomer($id);
				} else {
					$this->registry->get('load')->model('account/customer');
					$customer = $this->registry->get('model_account_customer')->getCustomer($id);
				}

				$participant['email'] = $customer['email'];
				$participant['name'] = sprintf('%s %s', $customer['firstname'], $customer['lastname']);

				break;

			case 'admin':
			default:
				$participant['email'] = $this->registry->get('config')->get('config_email');
				$participant['name'] = $this->registry->get('config')->get('config_name');

				break;
		}

		return $participant;
	}

	/**
	 * @return mixed
	 */
	public function getTo()
	{
		return $this->to;
	}

	/**
	 * @param mixed $to
	 */
	public function setTo($to)
	{
		$this->to = $to;
	}

	/**
	 * @return mixed
	 */
	public function getFrom()
	{
		return $this->from;
	}

	/**
	 * @param mixed $from
	 */
	public function setFrom($from)
	{
		$this->from = $from;
	}

	/**
	 * @return mixed
	 */
	public function getSender()
	{
		return $this->sender;
	}

	/**
	 * @param mixed $sender
	 */
	public function setSender($sender)
	{
		$this->sender = $sender;
	}

	/**
	 * @return mixed
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param mixed $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
}
