<?php

namespace MultiMerch\Core\Notification\Channels;

use MultiMerch\Core\Notification\Interfaces\ChannelInterface;
use MultiMerch\Core\Notification\Interfaces\MailNotificationInterface;
use MultiMerch\Core\Notification\Interfaces\NotificationInterface;

class MailChannel implements ChannelInterface
{
	/**
	 * @var	mixed	$registry	OpenCart registry.
	 */
	protected $registry;

	/**
	 * @var	mixed	$config		OpenCart config array stored in $registry.
	 */
	private $config;

	/**
	 * @var mixed	$ms_logger	MultiMerch logger instance stored in $registry.
	 */
	private $ms_logger;

	/**
	 * MailChannel constructor.
	 *
	 * @param	mixed	$registry
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;

		$this->config = $this->registry->get('config');
		$this->ms_logger = $this->registry->get('ms_logger');
	}

	/**
	 * Return OpenCart registry.
	 *
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * Detects whether notification can be transmitted through this channel.
	 *
	 * @param	NotificationInterface	$notification
	 * @return	bool
	 */
	public function shouldTransmit(NotificationInterface $notification)
	{
		return $notification instanceof MailNotificationInterface;
	}

	/**
	 * Transmits notification through this channel.
	 *
	 * @param	NotificationInterface	$notification
	 * @throws	\Exception
	 */
	public function transmit(NotificationInterface $notification)
	{
		$mail = new \Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($notification->getTo());
		$mail->setFrom($notification->getFrom());
		$mail->setSender(html_entity_decode($notification->getSender(), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($notification->getSubject());
		$mail->setText($notification->getMessage());

//		$this->ms_logger->info(print_r($mail, true));

		$mail->send();
	}
}
