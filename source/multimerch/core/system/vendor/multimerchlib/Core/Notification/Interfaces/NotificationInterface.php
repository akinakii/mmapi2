<?php

namespace MultiMerch\Core\Notification\Interfaces;

interface NotificationInterface
{
	/**
	 * Returns a message of notification.
	 *
	 * @return string
	 */
	public function getMessage();

	/**
	 * Returns an array of parameters that might be used by channels to build the actual notification.
	 *
	 * @return array
	 */
	public function getParameters();
}
