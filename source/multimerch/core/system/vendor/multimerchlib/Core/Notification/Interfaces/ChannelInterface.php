<?php

namespace MultiMerch\Core\Notification\Interfaces;

interface ChannelInterface
{
	public function shouldTransmit(NotificationInterface $notification);

	public function transmit(NotificationInterface $notification);
}
