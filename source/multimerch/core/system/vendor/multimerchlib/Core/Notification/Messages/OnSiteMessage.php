<?php

namespace MultiMerch\Core\Notification\Messages;

use MultiMerch\Core\Notification\Message;

class OnSiteMessage extends Message
{
	public function __construct($registry, $channel = 'onsite', $producer = 'system.0', $consumer = 'system.0', array $object = [])
	{
		parent::__construct($registry, $channel, $producer, $consumer, $object);

		$this->load->language("multiseller/multiseller");
		$this->load->language("multimerch/notification/{$this->channel}");

		$event = "{$this->getProducerType()}_{$this->object['action']}_{$this->object['type']}" . (!empty($this->object['subtype']) ? "_{$this->object['subtype']}" : "");

		// On-site notification text template
		$this->l_template = $this->language->get("msn_{$this->channel}_{$event}");

		if (method_exists($this, $event))
			$this->$event();
	}

	// @todo 8.19: do not show notification with deleted object(s) - maybe return back false, so message won't be rendered

	// Admin events

	/**
	 * Generates message for `admin_created_message` event.
	 *
	 * From: admin
	 * To: customer, seller
	 */
	private function admin_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		$this->setText(sprintf(
			$this->l_template,
			$sender_name,
			$this->config->get('config_name'),
			$this->url->link('account/msmessage', 'conversation_id=' . $message['conversation_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
			$conversation['title']
		));
	}

	/**
	 * Generates message for `admin_created_payout` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_created_payout()
	{
		$payout_total = !empty($this->object['metadata']['payout_total']) ? $this->object['metadata']['payout_total'] : 0;

		$this->setText(sprintf(
			$this->l_template,
			$payout_total
		));
	}

	/**
	 * Generates message for `admin_updated_account_status` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_updated_account_status()
	{
		$seller_status_id = !empty($this->object['metadata']['seller_status_id']) ? $this->object['metadata']['seller_status_id'] : null;
		$seller_status_name = $seller_status_id ? $this->language->get('ms_seller_status_' . $seller_status_id) : '';

		$this->setText(sprintf(
			$this->l_template,
			$seller_status_name
		));
	}

	/**
	 * Generates message for `admin_updated_order_status` event.
	 *
	 * From: admin
	 * To: seller
	 */
	private function admin_updated_order_status()
	{
		if ($this->isAdmin()) {
			$this->load->model('sale/order');
			$order = $this->model_sale_order->getOrder($this->object['id']);
		} else {
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($this->object['id']);
		}

		$order_status_id = isset($this->object['metadata']['order_status_id']) ? (int)$this->object['metadata']['order_status_id'] : null;
		$order_status_name = $order_status_id ? $this->MsLoader->MsHelper->getStatusName(['order_status_id' => $order_status_id]) : '';

		$this->setText(sprintf(
			$this->l_template,
			!empty($order) ? $this->url->link('seller/account-order/viewOrder', 'order_id=' . $order['order_id'], 'SSL') : '#',
			$this->object['id'],
			$order_status_name
		));
	}

	/**
	 * Generates message for `admin_updated_order_status` event.
	 *
	 * From: admin
	 * To: seller, customer
	 */
	private function admin_updated_suborder_status()
	{
		if ($this->isAdmin()) {
			$this->load->model('sale/order');
			$order = $this->model_sale_order->getOrder($this->object['id']);
		} else {
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($this->object['id']);
		}

		$suborder_status_id = isset($this->object['metadata']['suborder_status_id']) ? (int)$this->object['metadata']['suborder_status_id'] : null;
		$suborder_status_name = $suborder_status_id ? $this->MsLoader->MsSuborderStatus->getSubStatusName(['order_status_id' => $suborder_status_id]) : '';

		$this->setText(sprintf(
			$this->l_template,
			!empty($order) ? $this->url->link('seller/account-order/viewOrder', 'order_id=' . $order['order_id'], 'SSL') : '#',
			$this->object['id'],
			$suborder_status_name
		));
	}

	// Customer events

	/**
	 * Generates message for `customer_created_account` event.
	 *
	 * From: customer
	 * To: admin
	 */
	private function customer_created_account()
	{
		if ($this->isAdmin()) {
			$this->load->model('customer/customer');
			$customer = $this->model_customer_customer->getCustomer($this->object['id']);
		} else {
			$this->load->model('account/customer');
			$customer = $this->model_account_customer->getCustomer($this->object['id']);
		}

		$this->setText(sprintf(
			$this->l_template,
			!empty($customer) ? $this->url->link('customer/customer/edit', 'customer_id=' . $this->object['id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
			!empty($customer) ? sprintf('%s %s', $customer['firstname'], $customer['lastname']) : $this->language->get('ms_placeholder_deleted')
		));
	}

	/**
	 * Generates message for `customer_created_message` event.
	 *
	 * From: customer
	 * To: seller + admin (if participant)
	 */
	private function customer_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		if ($this->isAdmin()) {
			$this->setText(sprintf(
				$this->l_template,
				$this->url->link('customer/customer/edit', 'customer_id=' . $message['from'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
				$sender_name,
				$this->url->link('multimerch/conversation/view', 'conversation_id=' . $message['conversation_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
				$conversation['title']
			));
		} else {
			$this->setText(sprintf(
				$this->l_template,
				$sender_name,
				$this->url->link('account/msmessage', 'conversation_id=' . $message['conversation_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
				$conversation['title']
			));
		}
	}

	/**
	 * Generates message for `customer_created_order` event.
	 *
	 * From: customer
	 * To: seller, admin
	 */
	private function customer_created_order()
	{
		if ($this->isAdmin()) {
			$this->load->model('sale/order');
			$order = $this->model_sale_order->getOrder($this->object['id']);

			$this->load->model('customer/customer');
			$customer = $this->model_customer_customer->getCustomer($order['customer_id']);
		} else {
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($this->object['id']);

			$this->load->model('account/customer');
			$customer = $this->model_account_customer->getCustomer($order['customer_id']);
		}

		if ($this->isAdmin()) {
			$this->setText(sprintf(
				$this->l_template,
				!empty($customer) ? $this->url->link('customer/customer/edit', 'customer_id=' . $this->object['id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
				!empty($customer) ? sprintf('%s %s', $customer['firstname'], $customer['lastname']) : $this->language->get('ms_placeholder_deleted'),
				!empty($order) ? $this->url->link('sale/order/info', 'order_id=' . $order['order_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
				!empty($order) ? $this->object['id'] : $this->language->get('ms_placeholder_deleted')
			));
		} else {
			$this->setText(sprintf(
				$this->l_template,
				!empty($order) ? $this->url->link('seller/account-order/viewOrder', 'order_id=' . $order['order_id'], 'SSL') : '#',
				!empty($order) ? $this->object['id'] : $this->language->get('ms_placeholder_deleted')
			));
		}
	}

	private function guest_created_order()
	{
		if ($this->isAdmin()) {
			$this->load->model('sale/order');
			$order = $this->model_sale_order->getOrder($this->object['id']);

			$this->setText(sprintf(
				$this->l_template,
				!empty($order) ? $this->url->link('sale/order/info', 'order_id=' . $order['order_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
				!empty($order) ? $this->object['id'] : $this->language->get('ms_placeholder_deleted')
			));
		} else {
			$this->load->model('checkout/order');
			$order = $this->model_checkout_order->getOrder($this->object['id']);

			$this->setText(sprintf(
				$this->l_template,
				!empty($order) ? $this->url->link('seller/account-order/viewOrder', 'order_id=' . $order['order_id'], 'SSL') : '#',
				!empty($order) ? $this->object['id'] : $this->language->get('ms_placeholder_deleted')
			));
		}
	}

	/**
	 * Generates message for `customer_created_ms_review` event.
	 *
	 * From: customer
	 * To: seller
	 */
	private function customer_created_ms_review()
	{
		$ms_review = $this->MsLoader->MsReview->getReviews(['review_id' => $this->object['id'], 'single' => true]);

		$this->load->model('account/customer');
		$customer = $this->model_account_customer->getCustomer($ms_review['author_id']);

		$this->setText(sprintf(
			$this->l_template,
			$this->url->link('seller/account-review/update', 'review_id=' . $this->object['id'], 'SSL'),
			sprintf('%s %s', $customer['firstname'], $customer['lastname'])
		));
	}

	// Seller events

	/**
	 * Generates message for `seller_created_account` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_created_account()
	{
		$seller = $this->MsLoader->MsSeller->getSeller($this->object['id']);

		$this->setText(sprintf(
			$this->l_template,
			!empty($seller) ? $this->url->link('multimerch/seller/update', 'seller_id=' . $this->object['id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
			!empty($seller) ? $seller['ms.nickname'] : $this->language->get('ms_placeholder_deleted')
		));
	}

	/**
	 * Generates message for `seller_created_message` event.
	 *
	 * From: seller
	 * To: customer + admin (if participant)
	 */
	private function seller_created_message()
	{
		$message = $this->MsLoader->MsMessage->getMessages(['message_id' => $this->object['id'], 'single' => true]);
		$sender_name = $message['from_admin'] ? $message['user_sender'] : ($message['seller_sender'] ? $message['seller_sender'] : $message['customer_sender']);
		$conversation = $this->MsLoader->MsConversation->getConversations(['conversation_id' => $message['conversation_id'], 'single' => true]);

		$this->setText(sprintf(
			$this->l_template,
			$this->url->link('multimerch/seller/update', 'seller_id=' . $message['from'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
			$sender_name,
			$this->url->link('multimerch/conversation/view', 'conversation_id=' . $message['conversation_id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL'),
			$conversation['title']
		));
	}

	/**
	 * Generates message for `seller_created_product` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_created_product()
	{
		$product = $this->MsLoader->MsProduct->getProduct($this->object['id']);
		$seller_id = ('seller' === $this->producer_type) ? $this->producer_id : null;

		$seller_nickname = '';
		if ($seller_id)
			$seller_nickname = $this->MsLoader->MsSeller->getSellerNickname($seller_id);

		$this->setText(sprintf(
			$this->l_template,
			!empty($seller_nickname) ? $this->url->link('multimerch/seller/update', 'seller_id=' . $seller_id. (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
			!empty($seller_nickname) ? $seller_nickname : $this->language->get('ms_placeholder_deleted'),
			!empty($product) ? $this->url->link('catalog/product/edit', 'product_id=' . $this->object['id'] . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
			!empty($product) ? $product['languages'][$this->config->get('config_language_id')]['name'] : $this->language->get('ms_placeholder_deleted')
		));
	}

	/**
	 * Generates message for `seller_updated_invoice_status` event.
	 *
	 * From: seller
	 * To: admin
	 */
	private function seller_updated_invoice_status()
	{
		$invoice = $this->MsLoader->MsInvoice->get(['invoice_id' => $this->object['id'], 'single' => true]);

		$seller_id = null;
		$seller = null;
		if (!empty($invoice)) {
			$seller_id = ('seller' === $invoice->getRecipientType()) ? $invoice->getRecipientId() : null;
			$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		}

		$this->setText(sprintf(
			$this->l_template,
			!empty($seller) ? $this->url->link('multimerch/seller/update', 'seller_id=' . $seller_id . (isset($this->session->data['token']) ? '&token=' . $this->session->data['token'] : ''), 'SSL') : '#',
			!empty($seller) ? $seller['ms.nickname'] : $this->language->get('ms_placeholder_deleted'),
			$this->url->link('multimerch/invoice', (isset($this->session->data['token']) ? 'token=' . $this->session->data['token'] : ''), 'SSL'),
			$this->object['id']
		));
	}

	// System events

	/**
	 * Generates message for `system_created_invoice` event.
	 *
	 * From: system
	 * To: seller
	 */
	private function system_created_invoice()
	{
		$this->setText(sprintf(
			$this->l_template,
			$this->url->link('seller/account-invoice', '', 'SSL'),
			$this->object['id']
		));
	}

	/**
	 * Generates message for `system_finished_import` event.
	 *
	 * From: system
	 * To: seller
	 */
	private function system_finished_import()
	{
		$this->setText($this->l_template);
	}
}
