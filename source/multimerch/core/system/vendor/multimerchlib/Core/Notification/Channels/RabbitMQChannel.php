<?php

namespace MultiMerch\Core\Notification\Channels;

use MultiMerch\Core\Notification\Interfaces\ChannelInterface;
use MultiMerch\Core\Notification\Interfaces\RabbitMQNotificationInterface;
use MultiMerch\Core\Notification\Interfaces\NotificationInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQChannel implements ChannelInterface
{
	/**
	 * RabbitMQ connection credentials.
	 */
	private $host;
	private $port;
	private $user;
	private $password;

	/**
	 * @var	mixed	$registry	OpenCart registry.
	 */
	protected $registry;

	protected $connection;
	protected $channel;

	/**
	 * RabbitMQChannel constructor.
	 *
	 * @param	mixed	$registry
	 */
	public function __construct($registry)
	{
		$this->registry = $registry;

		$this->host = 'localhost';
		$this->port = 5672;
		$this->user = 'vlad';
		$this->password = '123123';
	}

	/**
	 * Return OpenCart registry.
	 *
	 * @return mixed
	 */
	public function getRegistry()
	{
		return $this->registry;
	}

	/**
	 * Detects whether notification can be transmitted through this channel.
	 *
	 * @param	NotificationInterface	$notification
	 * @return	bool
	 */
	public function shouldTransmit(NotificationInterface $notification)
	{
		return $notification instanceof RabbitMQNotificationInterface;
	}

	/**
	 * Transmits notification through this channel.
	 *
	 * @param NotificationInterface $notification
	 */
	public function transmit(NotificationInterface $notification)
	{
		// Initialize connection to a RabbitMQ server
		$this->connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password);

		// Open channel
		$this->channel = $this->connection->channel();

		$this->channel->exchange_declare('multimerch', 'direct', false, false, false);

		$message = new AMQPMessage($notification->getMessage(), ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

		$this->channel->basic_publish($message, 'multimerch', $notification->getRoutingKey());

		$this->channel->close();
		$this->connection->close();
	}
}
