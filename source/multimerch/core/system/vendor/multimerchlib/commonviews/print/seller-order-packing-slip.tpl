<html>
	<head>
		<style>
			body{
				font-size: 12px;
			}
			table{
				border: 1px solid #000;
				border-collapse: collapse;
				width: 100%;

			}
			table td, table th{
				padding: 5px 10px;
				text-align: left;
			}
		</style>
	</head>
	<body>
		<h3 style="text-align: left; margin: 0;"><?php echo $seller_name; ?></h3>
		<?php if ($seller_company){ ?>
		<h3 style="text-align: left; margin: 0; font-weight: normal"><?php echo $seller_company; ?></h3>
		<?php } ?>
		<?php if ($seller_address){ ?>
		<div>
			<?php echo $seller_address['address_1'] ? $seller_address['address_1'] . '<br />' : ''; ?>
			<?php echo $seller_address['address_2'] ? $seller_address['address_2'] . '<br />' : ''; ?>
			<?php echo $seller_address['city'] ? $seller_address['city'] . '' : ''; ?>
			<?php echo $seller_address['zip'] ? $seller_address['zip'] . '<br />' : ''; ?>
			<?php echo $seller_address['state'] ? $seller_address['state'] . '<br />' : ''; ?>
			<?php echo $seller_address['country_name'] ? $seller_address['country_name'] . '<br />' : ''; ?>
			<?php echo $seller_phone ? $seller_phone . '<br />' : ''; ?>
		</div>
		<?php } ?>

		<h1 style="">Packing slip</h1>
		<div>Order #<?php echo $order_id; ?></div>
		<div>Order date: <?php echo $order_info['date_added']; ?></div>
		<br />
		<div>
			<strong>Ship to:</strong><br />
			<?php echo $order_info['shipping_firstname'] ? $order_info['shipping_firstname'] . ' ' : ''; ?>
			<?php echo $order_info['shipping_lastname'] ? $order_info['shipping_lastname'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_company'] ? $order_info['shipping_company'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_address_1'] ? $order_info['shipping_address_1'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_address_2'] ? $order_info['shipping_address_2'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_city'] ? $order_info['shipping_city'] . ' ' : ''; ?>
			<?php echo $order_info['shipping_postcode'] ? $order_info['shipping_postcode'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_zone'] ? $order_info['shipping_zone'] . '<br />' : ''; ?>
			<?php echo $order_info['shipping_country'] ? $order_info['shipping_country'] . '<br />' : ''; ?>
			<?php echo $order_info['telephone'] ? $order_info['telephone'] . '<br />' : ''; ?>
		</div>
		<br/>
		<table border='1'>
			<tr>
				<th>Product</th>
				<!--<th>Description</th>-->
				<th style='width: 15%;'>Quantity</th>
			</tr>
			<?php foreach($products as $product){ ?>
			<tr>
				<td>
					<?php echo $product['name']; ?>
					<?php foreach ($product['option'] as $option) { ?>
					<br />
					<?php if ($option['type'] != 'file') { ?>
					&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
					<?php } else { ?>
					&nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
					<?php } ?>
					<?php } ?>
					<?php if (!empty($product['msf_variation'])) { ?>
					<?php foreach ($product['msf_variation'] as $msf_variation) { ?>
					<br />
					<small><?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
					<?php } ?>
					<?php } ?>
				</td>
				<td ><?php echo $product['quantity']; ?></td>
			</tr>
			<?php } ?>
		</table>
		<h5 style='font-style: italic'>Thank you for your business!</h5>
	</body>
</html>