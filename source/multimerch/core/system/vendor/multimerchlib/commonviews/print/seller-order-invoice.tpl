<html>
	<head>
		<style>
			body{
				font-size: 12px;
			}
			table{
				border: 0px solid #000;
				border-collapse: collapse;
				width: 100%;

			}
			table td, table th{
				padding: 5px 10px;
				text-align: left;
				border: 1px solid #000;
			}
			table td.noborder{
				border:none;
			}
			table.noborder td, table.noborder th{
				border:none;
				padding: 0;
			}

			table td.text-right, table th.text-right{
				text-align: right;
			}
			table td.text-center, table th.text-center{
				text-align: center;
			}
		</style>
	</head>
	<body>
		<table style="width:100%; padding: 0" class="noborder">
			<tr>
				<th style="text-align: left; padding: 0; width:25%; vertical-align: top;"><h1>Invoice</h1></th>
				<td style="width: 50%"></td>
				<td style="text-align: right; width: 25%; text-align: left; vertical-align: top;">
					<strong><?php echo $marketplace_name; ?></strong><br />
					<?php echo $marketplace_address; ?><br />
					<?php echo $marketplace_phone; ?><br />
					<?php echo $marketplace_email; ?>
				</td>

			</tr>
		</table>
		<br />
		<table class="noborder">
			<tr>
				<td>
					<div>Order #<?php echo $order_id; ?></div>
					<div>Order date: <?php echo $order_info['date_added']; ?></div>
				</td>
			</tr>

		</table>
		<br />
		<table style="width: 100%;" class="noborder">
			<tr>
				<td style="padding: 0; width:25%; vertical-align: top;">
					<div>
						<strong>Bill to:</strong><br />
						<?php echo $order_info['payment_firstname'] ? $order_info['payment_firstname'] . ' ' : ''; ?>
						<?php echo $order_info['payment_lastname'] ? $order_info['payment_lastname'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_company'] ? $order_info['payment_company'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_address_1'] ? $order_info['payment_address_1'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_address_2'] ? $order_info['payment_address_2'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_city'] ? $order_info['payment_city'] . ' ' : ''; ?>
						<?php echo $order_info['payment_postcode'] ? $order_info['payment_postcode'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_zone'] ? $order_info['payment_zone'] . '<br />' : ''; ?>
						<?php echo $order_info['payment_country'] ? $order_info['payment_country'] . '<br />' : ''; ?>
						<?php echo $order_info['telephone'] ? $order_info['telephone'] . '<br />' : ''; ?>
					</div>
				</td>

				<td style="padding: 0; width:25%; vertical-align: top;">
					<?php if (!empty($totals['mm_shipping_total'])) { ?>
					<div>
						<strong>Ship to:</strong><br />
						<?php echo $order_info['shipping_firstname'] ? $order_info['shipping_firstname'] . ' ' : ''; ?>
						<?php echo $order_info['shipping_lastname'] ? $order_info['shipping_lastname'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_company'] ? $order_info['shipping_company'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_address_1'] ? $order_info['shipping_address_1'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_address_2'] ? $order_info['shipping_address_2'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_city'] ? $order_info['shipping_city'] . ' ' : ''; ?>
						<?php echo $order_info['shipping_postcode'] ? $order_info['shipping_postcode'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_zone'] ? $order_info['shipping_zone'] . '<br />' : ''; ?>
						<?php echo $order_info['shipping_country'] ? $order_info['shipping_country'] . '<br />' : ''; ?>
						<?php echo $order_info['telephone'] ? $order_info['telephone'] . '<br />' : ''; ?>
					</div>
					<?php } ?>
				</td>


			</tr>
		</table>
		<br/>
		<table>
			<tr>
				<th>Product</th>
				<!--<th>Description</th>-->
				<th style='width: 15%;' class="text-right">Unit price</th>
				<th style='width: 15%;' class="text-center">Quantity</th>
				<th style='width: 15%;' class="text-right">Total</th>
			</tr>
			<?php foreach($products as $product){ ?>
			<tr>
				<td>
					<?php echo $product['name']; ?>
					<?php foreach ($product['option'] as $option) { ?>
					<br />
					<?php if ($option['type'] != 'file') { ?>
					&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
					<?php } else { ?>
					&nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
					<?php } ?>
					<?php } ?>
					<?php if (!empty($product['msf_variation'])) { ?>
					<?php foreach ($product['msf_variation'] as $msf_variation) { ?>
					<br />
					<small><?php echo $msf_variation['name']; ?>: <?php echo $msf_variation['value']; ?></small>
					<?php } ?>
					<?php } ?>
				</td>
				<td class="text-right"><?php echo $product['price']; ?></td>
				<td class="text-center"><?php echo $product['quantity']; ?></td>
				<td class="text-right"><?php echo $product['total']; ?></td>

			</tr>
			<?php } ?>
			<?php if (!empty($totals['mm_shipping_total'])) { ?>
			<tr>
				<td colspan="3" class="text-right noborder">
					<?php echo $totals['mm_shipping_total']['title']; ?>
				</td>
				<td class="text-right">
					<?php echo $totals['mm_shipping_total']['value']; ?>
				</td>

			</tr>
			<?php } ?>
			<tr>
				<td colspan="3" class="text-right noborder">
					<?php echo $totals['sub_total']['title']; ?>
				</td>
				<td class="text-right">
					<?php echo $totals['sub_total']['value']; ?>
				</td>

			</tr>
			<tr>
				<td colspan="3" class="text-right noborder">
					<?php echo $totals['total']['title']; ?>
				</td>
				<td class="text-right">
					<?php echo $totals['total']['value']; ?>
				</td>

			</tr>
		</table>
		<h5 style='font-style: italic'>Thank you for your business!</h5>
	</body>
</html>