<?php

namespace MultiMerch\Mail\Message\Admin;

use MultiMerch\Mail\Message\Message;

class ComplaintReceived extends Message
{
    protected $template = 'admin/complaint_received.tpl';

    public function beforeSend()
    {
	$data = $this->getData();
        $this->setSubject('[' . $this->getSender() . '] ' . $data['title']);
    }
}