<?php

namespace MultiMerch\Models;

use MultiMerch\Core\OcModel;

class ModelCron extends OcModel
{
	public function getList($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msc.*,
				msi.seller_id
			FROM `" . DB_PREFIX . "ms_cron` msc
			LEFT JOIN `" . DB_PREFIX . "ms_import` msi
				ON (msi.id = msc.import_id)
			WHERE 1 = 1"

			. (isset($data['import_id']) ? " AND msc.`import_id` = " . (int)$data['import_id'] : "")

			. $filters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total'];
		}

		return $result->rows;
	}

	public function get($data = [])
	{
		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_cron`
			WHERE 1 = 1"

			. (isset($data['import_id']) ? " AND `import_id` = " . (int)$data['import_id'] : "")
			. (isset($data['status']) ? " AND `status` = " . (int)$data['status'] : "")
			. (isset($data['is_running']) ? " AND `is_running` = " . (int)$data['is_running'] : "")
		;

		$result = $this->db->query($sql);

		return isset($data['import_id']) ? $result->row : $result->rows;
	}

	public function create($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_cron`
			SET `import_id` = " . (int)$data['import_id'] . ",
				`cycle` = '" . $this->db->escape($data['cycle']) . "',
				`status` = " . MM_FEED_CONST_STATUS_ACTIVE
		;

		$this->db->query($sql);

		return $this->db->getLastId();
	}

	public function update($job_id, $data = [])
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_cron`
			SET `id` = `id`"

			. (isset($data['cycle']) ? ", `cycle` = '" . $this->db->escape($data['cycle']) . "'" : "")
			. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
			. (isset($data['is_running']) ? ", `is_running` = " . (int)$data['is_running'] : "")
			. (isset($data['date_start']) ? ", `date_start` = '" . $this->db->escape($data['date_start']) . "'" : "")
			. (isset($data['date_next']) ? ", `date_next` = '" . $this->db->escape($data['date_next']) . "'" : "")

			. " WHERE `id` = " . (int)$job_id;

		$this->db->query($sql);

		return $job_id;
	}

	public function delete($job_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_cron`
			WHERE `id` = " . (int)$job_id
		;

		$this->db->query($sql);

		return true;
	}
}
