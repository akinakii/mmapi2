<?php

namespace MultiMerch\Models;

use MultiMerch\Core\OcModel;

class ModelProductImport extends OcModel
{
	private $importedSellerProductStatus = null;
	private $category_attributes = [];
	private $attribute_values = [];

	//===============//
	//				 //
	// IMPORT ENTITY //
	//				 //
	//===============//

	public function getImports($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msi.*,
				msic.url_path,
				mss.nickname,
				msc.status,
				msc.cycle,
				msc.date_start,
				msc.date_next
			FROM `" . DB_PREFIX . "ms_import` msi
			LEFT JOIN (SELECT config_id, url_path FROM `" . DB_PREFIX . "ms_import_config`) msic
				ON (msic.config_id = msi.import_config_id)
			LEFT JOIN (SELECT seller_id, nickname FROM `" . DB_PREFIX . "ms_seller`) mss
				ON (mss.seller_id = msi.seller_id)
			LEFT JOIN `" . DB_PREFIX . "ms_cron` msc
				ON (msc.import_id = msi.id)
			WHERE 1 = 1"

			. (isset($data['import_id']) ? " AND msi.`id` = " . (int)$data['import_id'] : "")
			. (isset($data['seller_id']) ? " AND msi.`seller_id` = " . (int)$data['seller_id'] : "")
			. (isset($data['type']) ? " AND msi.`type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['is_scheduled']) ? " AND msi.`is_scheduled` = " . (int)$data['is_scheduled'] : "")

			. $filters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total'];
		}

		if ($result->num_rows) {
			if (isset($data['import_id'])) {
				$result->row['config'] = $this->getImportConfig($result->row['import_config_id']);
				$result->row['history'] = $this->getImportHistories(['import_id' => $data['import_id']], ['offset' => 0, 'limit' => 9]);
			} else {
				foreach ($result->rows as &$row) {
					$row['config'] = $this->getImportConfig($row['import_config_id']);
					$row['history'] = $this->getImportHistories(['import_id' => $row['id']], ['offset' => 0, 'limit' => 9]);
				}
			}
		}

		return isset($data['import_id']) ? $result->row : $result->rows;
	}

	public function createImport($data = [])
	{
		$sql = "
			INSERT INTO `" .  DB_PREFIX . "ms_import`
			SET `seller_id` = " . (int)$data['seller_id'] . ",
				`type` = '" . $this->db->escape($data['type']) . "',
				`import_config_id` = " . (int)$data['import_config_id'] . ",
				`name` = '" . $this->db->escape($data['name']) . "',
				`is_scheduled` = " . (int)$data['is_scheduled']
		;

		$this->db->query($sql);

		return $this->db->getLastId();
	}

	public function updateImport($import_id, $data = [])
	{
		$errors = isset($data['errors']) ? (int)$data['errors'] : 0;

		$sql = "
			UPDATE `" . DB_PREFIX . "ms_import`
			SET `id` = `id`"

			. (isset($data['is_scheduled']) ? ", `is_scheduled` = " . (int)$data['is_scheduled'] : "")
			. (isset($data['processed']) ? ", `processed` = " . (int)$data['processed'] : "")
			. (isset($data['added']) ? ", `added` = " . (int)$data['added'] : "")
			. (isset($data['updated']) ? ", `updated` = " . (int)$data['updated'] : "")
			. (isset($data['errors']) ? ", `errors` = " . (int)$errors : "")
			. (isset($data['product_ids']) ? ", `product_ids` = '" . $this->db->escape(json_encode($data['product_ids'])) . "'" : "")

			. " WHERE `id` = " . (int)$import_id
		;

		$this->db->query($sql);

		return $import_id;
	}

	public function deleteImport($import_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_import`
			WHERE `id` = " . (int)$import_id
		;

		$this->db->query($sql);

		return true;
	}

	//===============//
	//				 //
	// IMPORT CONFIG //
	//				 //
	//===============//

	public function getImportConfig($config_id)
	{
		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_import_config`
			WHERE `config_id` = " . (int)$config_id
		;

		$result = $this->db->query($sql);

		if (!empty($result->row['mapping'])) {
			$result->row['mapping'] = json_decode($result->row['mapping']);
		}

		return $result->row;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function createImportConfig($data)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_import_config`
			SET `seller_id` = '" . (int)$data['seller_id'] . "',
				`delimiter` = '" . $this->db->escape($data['delimiter']) . "',
				`enclosure` = '" . $this->db->escape($data['enclosure']) . "',
				`date_added` = NOW()"

			. (isset($data['config_name']) ? ", `config_name` = '" . $this->db->escape($data['config_name']) . "'" : "")
			. (isset($data['import_type']) ? ", `import_type` = '" . $this->db->escape($data['import_type']) . "'" : "")
			. (isset($data['attachment_code']) ? ", `attachment_code` = '" . $this->db->escape($data['attachment_code']) . "'" : "")
			. (isset($data['url_path']) ? ", `url_path` = '" . $this->db->escape($data['url_path']) . "'" : "")
			. (isset($data['mapping']) ? ", `mapping` = '" . $this->db->escape(json_encode($data['mapping'])) . "'" : "")
			. (isset($data['start_row']) ? ", `start_row` = " . (int)$data['start_row'] : "")
			. (isset($data['finish_row']) ? ", `finish_row` = " . (int)$data['finish_row'] : "")
			. (isset($data['key_field']) ? ", `key_field` = '" . $this->db->escape($data['key_field']) . "'" : "")
			. (isset($data['file_encoding']) ? ", `file_encoding` = '" . $this->db->escape($data['file_encoding']) . "'" : "")
		;

		$this->db->query($sql);

		return $this->db->getLastId();
	}

	/**
	 * @param $config_id
	 * @param $data
	 * @return mixed
	 */
	public function updateImportConfig($config_id, $data)
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_import_config`
			SET `config_name` = '" . $this->db->escape($data['config_name']) . "',
				`import_type` = '" . $this->db->escape($data['import_type']) . "',
				`attachment_code` = '" . $this->db->escape($data['attachment_code']) . "',
				`url_path` = '" . $this->db->escape($data['url_path']) . "',
				`mapping` = '" . $this->db->escape(htmlspecialchars_decode($data['mapping'])) . "',
				`start_row` = '" . (int)$data['start_row'] . "',
				`finish_row` = '" . (int)$data['finish_row'] . "',
				`delimiter` = '" . $this->db->escape($data['delimiter']) . "',
				`enclosure` = '" . $this->db->escape($data['enclosure']) . "',
				`key_field` = '" . $this->db->escape($data['key_field']) . "',
				`file_encoding` = '" . $this->db->escape($data['file_encoding']) . "',
				`date_modified` = NOW()
			WHERE `config_id` = '" . (int)$config_id . "'
		";

		$this->db->query($sql);

		return $config_id;
	}

	public function deleteImportConfig($config_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_import_config`
			WHERE `config_id` = " . (int)$config_id
		;

		$this->db->query($sql);

		return true;
	}

	//================//
	//				  //
	// IMPORT HISTORY //
	//				  //
	//================//

	/**
	 * @param array $data
	 * @param array $sort
	 * @return mixed
	 */
	public function getHistoryLegacy($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				`import_id`,
				`seller_id`,
				`name`,
				`date_added`,
				`type`,
				`processed`,
				`added`,
				`updated`,
				`errors`,
				`product_ids`,
				ms.`nickname`
			FROM `" . DB_PREFIX . "ms_import_history`
			LEFT JOIN `" . DB_PREFIX . "ms_seller` ms
				USING (`seller_id`)
			WHERE 1 = 1 "

			. (isset($data['import_id']) ? " AND `import_id` = " .  (int)$data['import_id'] : "")
			. (isset($data['name']) ? " AND `name` LIKE %  " .  $this->db->escape($data['name']) : "")
			. (isset($data['seller_id']) ? " AND ms.`seller_id` = " .  (int)$data['seller_id'] : "")
			. (isset($data['date_added']) ? " AND `date_added` = " .  $this->db->escape($data['date_added']) : "")
			. (isset($data['type']) ? " AND `type` = " .  $this->db->escape($data['type']) : "")

			. $filters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$res = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) {
			$res->rows[0]['total_rows'] = $total->row['total'];
		}

		return $res->num_rows == 1 && isset($data['single']) ? $res->row : $res->rows;
	}

	public function getImportHistories($data = [], $sort = [])
	{
		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_import_history`
			WHERE 1 = 1"

			. (isset($data['import_id']) ? " AND `import_id` = " . (int)$data['import_id'] : "")
			. (isset($data['status']) ? " AND `status` = " . (int)$data['status'] : "")

			. " ORDER BY date_added DESC"
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		return $result->rows;
	}

	public function createImportHistory($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_import_history`
			SET `import_id` = " . (int)$data['import_id'] . ",
				`status` = " . (int)$data['status']
		;

		$this->db->query($sql);

		return $this->db->getLastId();
	}

	public function deleteHistory($import_id)
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_import_history` WHERE `import_id` = '" . (int)$import_id . "'");
	}


	public function insertProduct($data = [], $config = [])
	{
		// Prepare required values
		$this->prepareRequiredData($data, $config);

		$sql = "
			INSERT IGNORE INTO `" . DB_PREFIX . "product`
			SET `date_added` = NOW(),
				`date_modified` = NOW(),
				`date_available` = NOW(),
				`quantity` = '" . (int)$data['quantity'] . "',
				`status` = '" . (int)$data['status'] . "',
				`stock_status_id` = '" . (int)$data['stock_status_id'] . "'"

			. (isset($data['product_id']) ? ", `product_id` = " . (int)$data['product_id'] : "")
			. (isset($data['model']) ? ", `model` = '" . $this->db->escape($data['model']) . "'" : "")
			. (isset($data['sku']) ? ", `sku` = '" . $this->db->escape($data['sku']) . "'" : "")
			. (isset($data['price']) ? ", `price` = " . (float)$data['price'] : "")
			. (isset($data['weight']) ? ", `weight` = " . (float)$data['weight'] : "")
			. (isset($data['weight_class_id']) ? ", `weight_class_id` = " . (int)$data['weight_class_id'] : "")
			. (isset($data['tax_class_id']) ? ", `tax_class_id` = " . (int)$data['tax_class_id'] : "")
			. (!empty($data['manufacturer_id']) ? ", `manufacturer_id` = " . (int)$data['manufacturer_id'] : "");

		$this->db->query($sql);

		$product_id = $this->db->getLastId();

		if (!$product_id) {
			return false;
		}

		$this->saveProductData($product_id, $data, $config);

		// Product SEO
		if (isset($data["name_{$this->config->get('config_language_id')}"])) {
			$keyword = $this->MsLoader->MsHelper->slugify($data['keyword'] ?? $data["name_{$this->config->get('config_language_id')}"]);
			$keyword = $this->MsLoader->MsHelper->handleSlugDuplicate($keyword);

			$this->db->query("
				INSERT INTO `" . DB_PREFIX . "url_alias`
				SET `query` = 'product_id=" . (int)$product_id . "',
					`keyword` = '" . $this->db->escape($keyword) . "'
			");

			$this->cache->delete('multimerch_seo_url');
		}

		// Seller product status
		$this->db->query("
			INSERT INTO `" . DB_PREFIX . "ms_product`
			SET `product_id` = " . (int)$product_id . ",
				`seller_id` = " . (int)$config['seller_id'] . ",
				`product_status` = " . $this->getSellerImportedProductStatus($config['seller_id']) . ",
				`product_approved` = " . (int)$config['product_approved']
			. ((isset($config['list_until']) && $config['list_until'] != NULL ) ? ", list_until = '" . $this->db->escape($config['list_until']) . "'" : "")
		);

		return $product_id;
	}

	public function updateProduct($product_id, $data = [], $config = [])
	{
		// Check product belongs to seller
		$log = new \Log("ms_import.log");
		$product_exists = $this->db->query("SELECT 1 FROM " . DB_PREFIX . "ms_product WHERE product_id = " . (int)$product_id . " AND seller_id = " . (int)$config['seller_id']);
		if (!$product_exists->num_rows) {
			$log->write('Seller ID error when updating product');
			return $product_id;
		}

		// Prepare required values
		$this->prepareRequiredData($data, $config);

		$sql = "
			UPDATE `" . DB_PREFIX . "product`
			SET `date_modified` = NOW(),
				`quantity` = " . (int)$data['quantity'] . ",
				`status` = " . (int)$data['status'] . ",
				`stock_status_id` = " . (int)$data['stock_status_id']

			. (isset($data['sku']) ? ", `sku` = '" . $this->db->escape($data['sku']) . "'" : "")
			. (isset($data['price']) ? ", `price` = " . (float)$data['price'] : "")
			. (isset($data['weight']) ? ", `weight` = " . (float)$data['weight'] : "")
			. (isset($data['weight_class_id']) ? ", `weight_class_id` = " . (int)$data['weight_class_id'] : "")
			. (isset($data['tax_class_id']) ? ", `tax_class_id` = " . (int)$data['tax_class_id'] : "")
			. (!empty($data['manufacturer_id']) ? ", `manufacturer_id` = " . (int)$data['manufacturer_id'] : "")

			. " WHERE product_id = " . (int)$product_id;

		$this->db->query($sql);

		// Delete old images if new are coming
		if (isset($data['image_url']) || isset($data['images_url'])) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = " . (int)$product_id);
		}

		// Delete old descriptions if new are coming
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		foreach ($languages as $language) {
			if (!empty($data["description_{$language['language_id']}"])) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' AND language_id = " . (int)$language['language_id']);
			}
		}

		// Delete old stores if new are coming
		if (isset($data['stores'])) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		}

		// Delete old oc_attributes if new are coming
		if (isset($data['attributes'])) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		}

		//$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_to_category` WHERE product_id = '" . (int)$product_id . "'");

		// Check if categories are in one cell
		$must_delete_old_cats = !empty($data['categories_imploded']);

		// Check if categories are in different cells
		if (!$must_delete_old_cats) {
			for ($i = 1; $i <= $config['max_product_categories']; $i++) {
				if (!empty($data["category_single_{$i}"])) {
					$must_delete_old_cats = true;
					break;
				}
			}
		}

		if ($must_delete_old_cats) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "product_to_category` WHERE product_id = '" . (int)$product_id . "'");
		}

		$this->saveProductData($product_id, $data, $config);

		$this->db->query("
			UPDATE " . DB_PREFIX . "ms_product
			SET product_status = " . (int)$this->getSellerImportedProductStatus($config['seller_id']) . "
			WHERE product_id = '" . (int)$product_id . "'
		");

		return $product_id;
	}

	private function prepareRequiredData(&$data, $config)
	{
		// Set default values for required fields (if they are empty)
		if (!isset($data['quantity'])) {
			$data['quantity'] = $config['default_quantity'];
		}

		if (!isset($data['stock_status_id'])) {
			$data['stock_status_id'] = $config['stock_status_id'];
		}

		// Default product status. Based on seller and seller's group 'Product validation' setting.
		if (!isset($data['status'])) {
			$data['status'] = $this->MsLoader->MsSetting->calculateSellerSettingValue($config['seller_id'], 'slr_product_validation', 'slr_gr_product_validation');
		}

		if (!empty($data['price'])) {
			/*$this->load->model('localisation/currency');
			if (!empty($data['currency']) && $this->model_localisation_currency->getCurrencyByCode($data['currency'])) {
				$data['price'] = $this->currency->convert($data['price'], $data['currency'], $this->config->get('config_currency'));
			}*/

			$data['price'] = $this->currency->format($data['price'], $this->config->get('config_currency'), '', false);
		}

		if (!empty($data['manufacturer'])) {
			$data['manufacturer_id'] = $this->MsLoader->MsHelper->getManufacturerIdByName($data['manufacturer']);
		}

		if (!empty($data['weight'])) {
			$data['weight'] = str_replace(',', '.', $data['weight']);
			$data['weight_class_id'] = $data['weight_class_id'] ?? $this->config->get('config_weight_class_id');
		}

		// Create dir for product images
		$data['images_path'] = $config['images_path'] ?? ('import/' . $config['seller_id'] . '/');
		if (!file_exists(DIR_IMAGE . $data['images_path'])) {
			mkdir(DIR_IMAGE . $data['images_path'], 0755, true);
		}
	}

	private function saveProductData($product_id, $data, $config)
	{
		// Main product image
		if (isset($data['image_url']) && @fopen($data['image_url'],'r')) {
			$main_image = $this->MsLoader->MsHelper->prepareImageByImageUrl($product_id, $data['image_url'], $data['images_path']);
			$this->db->query("
				UPDATE " . DB_PREFIX . "product
				SET `image` = '" . $this->db->escape($main_image) . "'
				WHERE product_id = '" . (int)$product_id . "'
			");
		}

		// Additional product images
		$additional_images = [];
		if (isset($data['images_url'])) {
			$images = explode(',', $data['images_url']);
			foreach ($images as $key => $a_image) {
				$a_image = trim($a_image);
				if (@fopen($a_image,'r')) {
					$additional_images[] = $this->MsLoader->MsHelper->prepareImageByImageUrl($product_id, $a_image, $data['images_path'], $key+1);
				}
			}

			$additional_images = array_diff($additional_images, ['']);
		}

		foreach ($additional_images as $image) {
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "product_image
				SET `product_id` = '" . (int)$product_id . "',
					`image` = '" . $this->db->escape($image) . "'
			");
		}

		// Product categories
		$categories = [];
		if (!empty($data['categories_imploded'])) {
			// if categories are in 1 cell with separator
			$categories = explode($config['delimiter_category'], $data['categories_imploded']);
			$categories = array_diff($categories, ['', null, false]);
		} else {
			// if categories are in different cells
			for ($i = 1; $i <= $config['max_product_categories']; $i++) {
				if (!empty($data["category_single_{$i}"])) {
					$categories[] = $data["category_single_{$i}"];
				}
			}
		}

		if (!empty($categories)) {
			// OpenCart categories
			$category_id = $this->getOcCategoryIdByNames($categories);

			if ($category_id) {
				if ($this->config->get('msconf_enforce_childmost_categories') && !empty($this->MsLoader->MsCategory->getChildCategoriesByOcCategoryId($category_id))) {
					// Enforce childmost category setting is selected.
					// Category is no the childmost, therefore we can't bind it to product.
				} else {
					$sql = "
						INSERT IGNORE INTO `" . DB_PREFIX . "product_to_category`
						SET `product_id` = " . (int)$product_id . ",
							`category_id` = " . (int)$category_id
					;
					$this->db->query($sql);
				}
			}

			// MultiMerch categories
			/*$category_ids = $this->getMsCategoryIdsByNames($categories, $config);
			foreach ($category_ids as $category_id) {
				$sql = "
					INSERT IGNORE INTO `" . DB_PREFIX . "ms_product_to_category`
					SET `product_id` = " . (int)$product_id . ",
						`ms_category_id` = " . (int)$category_id
				;
				$this->db->query($sql);
			}*/
		}

		// Product name and description
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		foreach ($languages as $language) {
			if (!isset($data['meta_title']) && !empty($data["name_{$language['language_id']}"])) {
				$data['meta_title'] = $data["name_{$language['language_id']}"];
			}

			if (!isset($data['meta_description']) && !empty($data["description_{$language['language_id']}"])) {
				$data['meta_description'] = $this->MsLoader->MsHelper->generateMetaDescription($data["description_{$language['language_id']}"]);
			}

			$this->db->query("
				INSERT INTO `" . DB_PREFIX . "product_description`
				SET `product_id` = '" . (int)$product_id . "',
					`language_id` = '" . (int)$language['language_id'] . "'"
				. (isset($data["name_{$language['language_id']}"]) ? ", name = '" . $this->db->escape(htmlspecialchars($data["name_{$language['language_id']}"], ENT_QUOTES, "UTF-8")) . "'" : "")
				. (isset($data["description_{$language['language_id']}"]) ? ", description = '" . $this->db->escape(htmlspecialchars($data["description_{$language['language_id']}"], ENT_QUOTES, "UTF-8")) . "'" : "")
				. (isset($data['meta_title']) ? ", meta_title = '" . $this->db->escape($data['meta_title']) . "'" : "")
				. (isset($data['meta_description']) ? ", meta_description = '" . $this->db->escape($data['meta_description']) . "'" : "")
				. (isset($data['meta_keyword']) ? ", meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "'" : "")
			);
		}

		// Product to store relationship. By default, only main store is taken
		$store_ids = $data['stores'] ?? [$this->config->get('config_store_id')];
		if (!empty($store_ids)) {
			foreach ($store_ids as $store_id) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` SET `product_id` = " . (int)$product_id . ", `store_id` = " . (int)$store_id);
			}
		}

		// Product OC attributes
		if (isset($data['attributes'])) {
			foreach ($data['attributes'] as $attribute_id => $attribute_value) {
				if ($attribute_value) {
					foreach ($languages as $language) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language['language_id'] . "', text = '" . $this->db->escape($attribute_value) . "'");
					}
				}
			}
		}

		// Product ms attributes
		$this->saveAttributes($product_id, $data);
	}

	private function getOcCategoryIdByNames($category_names)
	{
		$categories_id = [];
		foreach ($category_names as $level => $name) {
			$name = trim(htmlspecialchars($name), " \n\t");

			$result = $this->db->query("
				SELECT
					cd.`category_id`
				FROM `" . DB_PREFIX . "category_description` cd
				LEFT JOIN (SELECT category_id, path_id, `level` FROM `" . DB_PREFIX . "category_path`) cp
					ON (cp.path_id = cd.category_id)
				WHERE LOWER(cd.`name`) = LOWER('" . $this->db->escape($name) . "')
					AND cp.`level` = " . (int)$level . "
				GROUP BY cd.`category_id`
			");

			// Get all possible ids for current name
			$categories_id[$level] = [];
			foreach ($result->rows as $row) {
				$categories_id[$level][] = $row['category_id'];
			}
		}

		$last_category_ids = end($categories_id);

		if (count($last_category_ids) === 1) {
			return end($last_category_ids);
		}

		$possible_paths = [];
		foreach ($last_category_ids as $id) {
			$query = $this->db->query("
				SELECT
					GROUP_CONCAT(`path_id`) as `path`
				FROM `" . DB_PREFIX . "category_path`
				WHERE `category_id` = " . (int)$id . "
				GROUP BY `category_id`
			");

			if ($query->num_rows) {
				$possible_paths[] = explode(',', $query->row['path']);
			}
		}

		// Now create all possible paths from retrieved by names $categories_id
		$combinations = $this->MsLoader->MsHelper->getPossibleCombinations($categories_id);

		$correct_path = [];
		foreach ($combinations as $combination) {
			if (in_array($combination, $possible_paths)) {
				$correct_path = $combination;
				break;
			}
		}

		return !empty($correct_path) ? end($correct_path) : 0;
	}

	private function getMsCategoryIdsByNames($category_names, $config)
	{
		$categories_id = [];
		$parent_id = 0;

		foreach ($category_names as $name) {
			$name = trim($name, " \n\t");

			$result = $this->db->query("
				SELECT
					msc.category_id
				FROM `" . DB_PREFIX . "ms_category_description` mscd
				LEFT JOIN `" . DB_PREFIX . "ms_category` msc
					ON (msc.category_id = mscd.category_id)
				WHERE LOWER(mscd.name) = LOWER('" . $this->db->escape($name) . "')
					AND msc.parent_id = '" . $parent_id . "'
					AND msc.seller_id = '" . (int)$config['seller_id'] . "'
				LIMIT 1
			");

			if ($result->num_rows) {
				$category_id = $result->row['category_id'];
			} else {
				// Create new seller category
				$category_data = [
					'parent_id' => $parent_id,
					'seller_id' => $config['seller_id'],
					'sort_order' => 0,
					'status' => 1,
					'category_store' => [$this->config->get('config_store_id')],
					'keyword' => $this->MsLoader->MsHelper->slugify($name),
				];

				$this->load->model("localisation/language");
				$languages = $this->model_localisation_language->getLanguages();
				foreach ($languages as $language) {
					$category_data['category_description'][$language['language_id']] = [
						'name' => $name,
						'description' => '',
						'meta_title' => $name
					];
				}

				$category_id = $this->MsLoader->MsCategory->createCategory($category_data);
			}

			// Set current category as parent for further loops
			$parent_id = $category_id;

			$categories_id[] = $category_id;
		}

		return array_unique($categories_id);
	}

	private function getSellerImportedProductStatus($seller_id)
	{
		if ($this->importedSellerProductStatus === null) {
			$msStatus = $this->MsLoader->MsSetting->calculateSellerSettingValue($seller_id, 'slr_product_validation', 'slr_gr_product_validation');

			if ($msStatus == \MsProduct::MS_PRODUCT_VALIDATION_NONE) {
				$this->importedSellerProductStatus = \MsProduct::STATUS_ACTIVE;
			} else {
				$this->importedSellerProductStatus = \MsProduct::STATUS_INACTIVE;
			}
		}

		return $this->importedSellerProductStatus;
	}

	private function saveAttributes($product_id, $data)
	{
		$category_ids = $this->MsLoader->MsProduct->getProductOcCategories($product_id);

		if (! $category_ids) {
			return;
		}

		$attributes_with_value = [];
		$attributes = [];

		foreach (explode(',', $category_ids) as $category_id) {
			foreach ($this->getCategoryAttributeIds($category_id) as $attribute_id) {
				if (isset($attributes_with_value[$attribute_id])) {
					$attributes_with_value[$attribute_id] = array_merge($attributes_with_value[$attribute_id], $this->getAttributeValues($attribute_id));
				} else {
					$attributes_with_value[$attribute_id] = $this->getAttributeValues($attribute_id);
				}
			}
		}

		foreach ($attributes_with_value as $attribute_id => $attribute_values) {
			if (! in_array($attribute_id, array_keys($data))) {
				continue;
			}

			if ($value = array_search($data[$attribute_id], $attribute_values)) {
				$attributes[$attribute_id] = $value;
			}
		}

		if (empty($attributes)) {
			return;
		}

		$table_name = DB_PREFIX . 'ms_product_msf_attribute_value_id';

		foreach ($attributes as $attribute_id => $attribute_value) {
			$id = $this->db->query("SELECT id FROM `{$table_name}` WHERE `product_id` = {$product_id} AND `msf_attribute_id` = {$attribute_id}")->row['id'] ?? null;
			$this->db->query("INSERT INTO `{$table_name}`(id, product_id, msf_attribute_id, msf_attribute_value_id) 
				VALUES (" . (is_null($id) ? 'NULL' : $id) . ", {$product_id}, {$attribute_id}, {$attribute_value})
				ON DUPLICATE KEY UPDATE `msf_attribute_value_id` = {$attribute_value}");
		}
	}

	private function getCategoryAttributeIds($category_id)
	{
		if (! isset($this->category_attributes[$category_id])) {
			$sql = "SELECT msf_attribute_id FROM `" . DB_PREFIX . 'ms_oc_category_msf_attribute` WHERE `oc_category_id` = ' . $this->db->escape($category_id);
			$this->category_attributes[$category_id] = array_column($this->db->query($sql)->rows, 'msf_attribute_id');
		}

		return $this->category_attributes[$category_id];
	}

	private function getAttributeValues($attribute_id)
	{
		if (! isset($this->attribute_values[$attribute_id])) {
			$tableValueDescription = DB_PREFIX . 'msf_attribute_value_description';
			$tableValue = DB_PREFIX . 'msf_attribute_value';

			$sql = "SELECT `name`, `msf_attribute_value_id` AS `value_id` FROM `{$tableValueDescription}`
			RIGHT JOIN `{$tableValue}` ON `{$tableValue}`.id = `{$tableValueDescription}`.msf_attribute_value_id
			WHERE `{$tableValue}`.msf_attribute_id = " . $this->db->escape($attribute_id);

			foreach ($this->db->query($sql)->rows as $row) {
				$this->attribute_values[$attribute_id][$row['value_id']] = $row['name'];
			}
		}

		return $this->attribute_values[$attribute_id] ?? [];
	}

	public function getCommonImportSettings($seller_id)
	{
		$new_product_limit = false;
		$seller_group = $this->MsLoader->MsSellerGroup->getSellerGroupBySellerId($seller_id);
		if (!empty($seller_group['seller_product_limit'])) {
			$new_product_limit = $seller_group['seller_product_limit'] - $seller_group['seller_product_quantity'];
			if ($new_product_limit < 0) {
				$new_product_limit = 0;
			}
		}

		return [
			'new_product_limit' => $new_product_limit,
			'default_quantity' => 999,
			'default_product_status' => 0,
			'delimiter_category' => '>',
			'max_product_categories' => '6',
			'fill_category' => 1,
			'stock_status_id' => 7, // @todo 8.29
			'images_path' => 'import/' . $seller_id . '/',
			'product_approved' => 1
		];
	}
}