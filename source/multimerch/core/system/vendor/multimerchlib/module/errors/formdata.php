<?php

namespace MultiMerch\Module\Errors;

class FormData extends Base
{
	private $errors = [];

	public function __construct($message, $code = 0, $errors = [])
	{
		parent::__construct($message, $code);

		$this->errors = $errors;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}