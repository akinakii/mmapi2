<?php

namespace MultiMerch\Module\Traits;

trait Stripe
{
	public $stripe_settings;

	public function initStripe()
	{
		require_once (DIR_SYSTEM . 'library/multimerch/stripe/init.php');

		$this->load->model('setting/setting');
		$settings = $this->model_setting_setting->getSetting('ms_stripe_connect');
		foreach ($settings as $key => $value) {
			$new_key = str_replace("ms_stripe_connect_", "", $key);
			$settings[$new_key] = $value;
			unset($settings[$key]);
		}

		if (empty($settings['environments']))
			return [false, false];

		\Stripe\Stripe::setApiKey($settings['environments'][$settings['test_mode']]['secret_key']);

		if ($this->config->get('config_secure')) {
			$curl = new \Stripe\HttpClient\CurlClient([CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2]);
			\Stripe\ApiRequestor::setHttpClient($curl);
		} else {
			\Stripe\Stripe::setVerifySslCerts(false);
		}

		return [$settings, $settings['test_mode']];
	}
}