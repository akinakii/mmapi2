<?php
class MsMail extends Model {
	public function __construct($registry) {
		parent::__construct($registry);
		$this->errors = array();
	}

	private function _modelExists($model) {
		$file  = DIR_APPLICATION . 'model/' . $model . '.php';
		return file_exists($file);
	}

	private function _getOrderProducts($order_id) {
		$sql = "SELECT
					op.*,
					opd1.seller_id,
					opd1.seller_net_amt
				FROM " . DB_PREFIX . "order_product op
				LEFT JOIN (SELECT order_product_id, seller_id, seller_net_amt FROM `" . DB_PREFIX . "ms_order_product_data`) opd1
					ON (op.order_product_id = opd1.order_product_id)
				WHERE op.order_id = " . (int)$order_id;
		
		$res = $this->db->query($sql);

		return $res->rows;
	}

	public function sendOrderMails($order_id) {
		$serviceLocator = $this->MsLoader->load('\MultiMerch\Module\MultiMerch')->getServiceLocator();
		$mailTransport = $serviceLocator->get('MailTransport');
		$mails = new \MultiMerch\Mail\Message\MessageCollection();

		$order_products_by_seller = array();

		$order_products = $this->_getOrderProducts($order_id);

		foreach ($order_products as $order_product) {
			$seller_id = $order_product['seller_id'];

			if ($seller_id) {
				if (!isset($order_products_by_seller[$seller_id]))
					$order_products_by_seller[$seller_id] = array();

				$order_products_by_seller[$seller_id][] = $order_product;
			}
		}

		if ($this->_modelExists('checkout/order')) {
			// catalog
			$this->load->model('checkout/order');
			$this->load->model('account/order');
			$order_info = $this->model_checkout_order->getOrder($order_id);
		} else {
			// admin
			$this->load->model('sale/order');
			$order_info = $this->model_sale_order->getOrder($order_id);
		}

		foreach ($order_products_by_seller as $seller_id => $order_seller_products) {
			foreach ($order_seller_products as &$order_product) {
				if ($this->_modelExists('account/order')) {
					$options = $this->model_account_order->getOrderOptions($order_id, $order_product['order_product_id']);
				} else {
					$options = $this->model_sale_order->getOrderOptions($order_id, $order_product['order_product_id']);
				}

				$option_data = array();
				foreach ($options as $option) {
					if ($option['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
						$option['value'] = $upload_info ? $upload_info['name'] : '';
					}

					$option_data[] = $option;
				}

				$order_product['order_options'] = $option_data;
			}

			$suborder_totals = $this->MsLoader->MsSuborder->getSuborderTotals($order_id, $seller_id);

			$order_info['comment'] = $this->MsLoader->MsOrderData->getOrderComment(array(
				'order_id' => $order_id,
				'seller_id' => $seller_id
			));

			$MailProductPurchased = $serviceLocator->get('MailProductPurchased', false)
				->setTo($this->MsLoader->MsSeller->getSellerEmail($seller_id))
				->setData(array(
					'addressee' => $this->MsLoader->MsSeller->getSellerName($seller_id),
					'order_products' => $order_seller_products,
					'total' => !empty($suborder_totals['total']['text']) ? $suborder_totals['total']['text'] : $this->currency->format(0, $order_info['currency_code']),
					'order_info' => $order_info,
				));
			$mails->add($MailProductPurchased);
		}

		$mailTransport->sendMails($mails);
	}
}