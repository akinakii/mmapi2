<?php

use MultiMerch\Core\Notification\Manager;

class MsNotification extends Model
{
	protected $class_alias = [
		'channels' => [
			'onsite' => \MultiMerch\Core\Notification\Channels\DatabaseChannel::class,
			'mail' => \MultiMerch\Core\Notification\Channels\MailChannel::class
		],
		'messages' => [
			'onsite' => \MultiMerch\Core\Notification\Messages\OnSiteMessage::class,
			'mail' => \MultiMerch\Core\Notification\Messages\MailMessage::class
		],
		'notifications' => [
			'onsite' => \MultiMerch\Core\Notification\Notifications\DatabaseNotification::class,
			'mail' => \MultiMerch\Core\Notification\Notifications\MailNotification::class
		]
	];

	/**
	 * Overridden __call method.
	 * Used in order to minimize code duplication.
	 *
	 * @param $method
	 * @param $args
	 */
	public function __call($method, $args)
	{
		if (method_exists($this, $method)) {
			$this->processNotification($args[0], $args[1], $args[2]);
		}
	}

	/**
	 * @param string $type
	 * @param string $channel
	 * @return mixed|bool
	 */
	public function getClassAlias($type = '', $channel = '')
	{
		if (!$type || !$channel)
			return $this->class_alias;

		$class = isset($this->class_alias[$type][$channel]) ? $this->class_alias[$type][$channel] : null;

		return $class;
	}

	/**
	 * Processes notification.
	 *
	 * @param	string	$producer		Producer of notification. Producer is a string of structure `type`.`id`.
	 * @param	array	$consumers		Consumers of notification. Each consumer is a string of structure `type`.`id`.
	 * 									Wildcard usage allowed: 'seller.*', 'customer.*'.
	 * @param	array	$object			Object of notification.
	 */
	private function processNotification($producer, array $consumers, array $object)
	{
		// Initiate notification manager
		$notification_manager = new Manager($this->registry);

		// Get producer type and id
		list($producer_type, $producer_id) = explode('.', $producer);

		// Process wildcard if passed
		$consumers = $this->processWildcardConsumers($consumers);

		// Get notification channels for current event
		$channels = $this->getChannelsByEvent($producer_type, $object);

		foreach ($consumers as $consumer) {
			foreach ($channels as $channel) {
				$channel_class = isset($this->class_alias['channels'][$channel]) ? $this->class_alias['channels'][$channel] : false;
				$notification_class = isset($this->class_alias['notifications'][$channel]) ? $this->class_alias['notifications'][$channel] : false;
				$message_class = isset($this->class_alias['messages'][$channel]) ? $this->class_alias['messages'][$channel] : false;

				if (!$channel_class || !$notification_class || !$message_class)
					continue;

				// Message
				$notification_message = new $message_class($this->registry, $channel, $producer, $consumer, $object);

				// If no message should be send, skip to next consumer
				if (!$notification_message->getText())
					continue;

				// Additional notification parameters
				$notification_parameters = $this->getNotificationParametersByChannel($channel, $producer, $consumer, $object, [
					'notification_message' => $notification_message,
					'channels' => $channels
				]);

				// Send notification in channel
				$notification_manager->send(
					new $notification_class(
						$this->registry,
						$notification_message->getText(),
						$notification_parameters
					),
					new $channel_class($this->registry)
				);
			}
		}
	}

	private function processWildcardConsumers(array $consumers)
	{
		$processed_consumers = [];

		if (in_array('seller.*', $consumers)) {
			$seller_ids = $this->MsLoader->MsSeller->getSellers(['seller_status' => MsSeller::STATUS_ACTIVE]);
			foreach ($seller_ids as $seller_id) {
				$processed_consumers[] = "seller.{$seller_id}";
			}
		} elseif (in_array('customer.*', $consumers)) {
			$customer_ids = $this->MsLoader->MsSeller->getCustomers();
			foreach ($customer_ids as $customer_id) {
				$processed_consumers[] = "customer.{$customer_id}";
			}
		} else {
			$processed_consumers = $consumers;
		}

		return $processed_consumers;
	}

	private function getChannelsByConsumer($consumer)
	{
		list($consumer_type, $consumer_id) = explode('.', $consumer);

		switch ($consumer_type) {
			case 'seller':
				$channels = ['mail', 'onsite']; // $this->MsLoader->MsSetting->getSellerSettings(['name' => 'ms_notification_channels', 'seller_id' => $consumer_id])
				break;

			case 'admin':
			case 'customer':
			case 'guest':
			default:
				$channels = ['mail', 'onsite'];
				break;
		}

		return array_unique($channels);
	}

	private function getChannelsByEvent($producer_type, $object)
	{
		$event_name = $producer_type;

		foreach (['action', 'type', 'subtype'] as $key) {
			if (isset($object[$key]))
				$event_name .= ".{$object[$key]}";
		}

		$ms_notification_events = $this->config->get('msconf_notification_events');

		return !empty($ms_notification_events[$event_name]) ? $ms_notification_events[$event_name] : [];
	}

	private function getNotificationParametersByChannel($channel, $producer, $consumer, $object, array $metadata)
	{
		$parameters = ['producer' => $producer, 'consumer' => $consumer];

		switch ($channel) {
			case 'mail':
				$notification_message = isset($metadata['notification_message']) ? $metadata['notification_message'] : null;

				// For mail channel subject must be set
				if ($notification_message && property_exists($notification_message, 'subject'))
					$parameters['subject'] = $notification_message->getSubject();

				break;

			case 'onsite':
				$parameters['object'] = $object;
				$parameters['channels'] = isset($metadata['channels']) ? $metadata['channels'] : [];

				break;
		}

		return $parameters;
	}

	/********************************************* DATABASE METHODS ***************************************************/

	/**
	 * Gets notifications from database by passed parameters.
	 *
	 * @param	array	$data
	 * @param	array	$sort
	 * @param	array	$cols
	 * @return	mixed
	 */
	public function getNotifications($data = [], $sort = [], $cols = [])
	{
		$hFilters = $wFilters = '';
		if (isset($sort['filters'])) {
			foreach ($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msn.*,
				msno.*
			FROM `" . DB_PREFIX . "ms_notification` msn
			LEFT JOIN `" . DB_PREFIX . "ms_notification_channel` msnc
				ON (msnc.`notification_id` = msn.`notification_id`)
			LEFT JOIN `" . DB_PREFIX . "ms_notification_object` msno
				ON (msno.`notification_id` = msn.`notification_id`)
			WHERE 1 = 1"
			. (isset($data['producer_type']) ? " AND msn.`producer_type` = '" . $this->db->escape($data['producer_type']) . "'" : "")
			. (isset($data['producer_id']) ? " AND msn.`producer_id` = '" . (int)$data['producer_id'] . "'" : "")
			. (isset($data['consumer_type']) ? " AND msn.`consumer_type` = '" . $this->db->escape($data['consumer_type']) . "'" : "")
			. (isset($data['consumer_id']) ? " AND msn.`consumer_id` = '" . (int)$data['consumer_id'] . "'" : "")
			. (isset($data['read']) ? " AND msn.`read` = '" . (int)$data['read'] . "'" : "")
			. (isset($data['channel']) ? " AND msnc.`channel` = '" . $this->db->escape($data['channel']) . "'" : "")
			. (isset($data['object_type']) ? " AND msno.`object_type` = '" . $this->db->escape($data['object_type']) . "'" : "")
			. (isset($data['object_id']) ? " AND msno.`object_id` = '" . (int)$data['object_id'] . "'" : "")
			. (isset($data['object_action']) ? " AND msno.`object_action` = '" . $this->db->escape($data['object_action']) . "'" : "")

			. $wFilters

			. " GROUP BY msn.notification_id HAVING 1 = 1"

			. $hFilters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total_rows");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total_rows'];

			foreach ($result->rows as &$row) {
				if (!empty($row['metadata']))
					$row['metadata'] = (array)json_decode($row['metadata']);
			}
		}

		return $result->rows;
	}

	public function createNotification(array $data)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_notification`
			SET `producer_type` = " . (!empty($data['producer_type']) ? ("'" . $this->db->escape($data['producer_type']) . "'") : "NULL") . ",
				`producer_id` = " . (int)$data['producer_id'] . ",
				`consumer_type` = " . (!empty($data['consumer_type']) ? ("'" . $this->db->escape($data['consumer_type']) . "'") : "NULL") . ",
				`consumer_id` = " . (int)$data['consumer_id'] . ",
				`read` = " . (int)$data['read']
		;

		if (isset($data['date_created'])) {
			$sql .= ", `date_created` = '" . $this->db->escape($data['date_created']) . "'";
		} else {
			$sql .= ", `date_created` = NOW()";
		}

		if (isset($data['date_read'])) {
			$sql .= ", `date_read` = '" . $this->db->escape($data['date_read']) . "'";
		} elseif ($data['read']) {
			$sql .= ", `date_read` = NOW()";
		}

		$this->db->query($sql);

		$notification_id = $this->db->getLastId();

		// Channels through which notification is transmitted
		if (!empty($data['channels'])) {
			foreach ($data['channels'] as $channel) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_notification_channel`
					SET `notification_id` = " . (int)$notification_id . ",
						`channel` = '" . $this->db->escape($channel) . "'
				");
			}
		}

		// Notification object
		if (!empty($data['object'])) {
			$this->db->query("
				INSERT INTO `" . DB_PREFIX . "ms_notification_object`
				SET `notification_id` = " . (int)$notification_id . ",
					`object_type` = " . (!empty($data['object']['type']) ? ("'" . $this->db->escape($data['object']['type']) . "'") : "NULL") . ",
					`object_subtype` = " . (!empty($data['object']['subtype']) ? ("'" . $this->db->escape($data['object']['subtype']) . "'") : "NULL") . ",
					`object_id` = " . (int)$data['object']['id'] . ",
					`object_action` = " . (!empty($data['object']['action']) ? ("'" . $this->db->escape($data['object']['action']) . "'") : "NULL") . ",
					`metadata` = " . (!empty($data['object']['metadata']) ? ("'" . $this->db->escape(json_encode($data['object']['metadata'])) . "'") : "NULL")
			);
		}

		return $notification_id;
	}

	public function markNotificationAsRead($notification_id)
	{
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_notification`
			SET `read` = 1,
				`date_read` = NOW()
			WHERE `notification_id` = " . (int)$notification_id . "
		");

		return true;
	}

	/**
	 * Counts new unacknowledged notifications for a user.
	 *
	 * Counts are retrieved from a setting `msconf_notification_count_unack_per_user` with the structure:
	 * ['consumer_type.consumer_id' => (int)count, 'consumer_type.consumer_id' => (int)count ... ].
	 * Count for a user is automatically created and stored in array if it does not exist.
	 *
	 * @todo 9.0: create table `ms_customer_setting`, write everything in related tables (for sellers, customers and admin)
	 * @todo 9.0: delete setting `msconf_notification_count_unack_per_user`
	 *
	 * @param	string	$consumer_type	Consumer type: 'admin', 'seller' or 'customer'.
	 * @param	int		$consumer_id	Consumer id.
	 * @return	int						Count of a new unacknowledged notifications.
	 */
	public function getNewNotificationsCount($consumer_type, $consumer_id)
	{
		$this->load->model('setting/setting');
		$settings = $this->model_setting_setting->getSetting('msconf');

		$counts = isset($settings['msconf_notification_count_unack_per_user']) ? $settings['msconf_notification_count_unack_per_user'] : [];

		$user = "{$consumer_type}.{$consumer_id}";

		if (!isset($counts[$user])) {
			$counts[$user] = 0;

			$this->MsLoader->MsHelper->createOcSetting([
				'code' => 'msconf',
				'key' => 'msconf_notification_count_unack_per_user',
				'value' => $counts
			]);
		}

		return $counts[$user];
	}

	/**
	 * Updates notifications count for a user.
	 * By default, increments
	 *
	 * @param	string	$consumer_type	Consumer type: 'admin', 'seller' or 'customer'.
	 * @param	int		$consumer_id	Consumer id.
	 * @param	bool	$reset			Whether to reset notifications count.
	 */
	public function updateNewNotificationsCount($consumer_type, $consumer_id, $reset = false)
	{
		$this->load->model('setting/setting');
		$settings = $this->model_setting_setting->getSetting('msconf');

		$counts = isset($settings['msconf_notification_count_unack_per_user']) ? $settings['msconf_notification_count_unack_per_user'] : [];

		$user = "{$consumer_type}.{$consumer_id}";

		if (!isset($counts[$user])) {
			$counts[$user] = 1;
		} else {
			if ($reset) {
				$counts[$user] = 0;
			} else {
				$counts[$user] += 1;
			}
		}

		$this->MsLoader->MsHelper->createOcSetting([
			'code' => 'msconf',
			'key' => 'msconf_notification_count_unack_per_user',
			'value' => $counts
		]);
	}
}
