<?php

class MsHelper extends Model {
	public function getSortParams($sorts, $colMap, $defCol = false, $defWay = false) {
		if (isset($this->request->get['iSortCol_0'])) {
			if (isset($this->request->get['mDataProp_' . $this->request->get['iSortCol_0']])) {
				$sortCol = $this->request->get['mDataProp_' . $this->request->get['iSortCol_0']];
			} else {
				$sortCol = $defCol ? $defCol : $sorts[0];
			}
		} else {
			$sortCol = $defCol ? $defCol : $sorts[0];
		}
		
		if (!in_array($sortCol, $sorts)) {
			$sortCol = $defCol ? $defCol : $sorts[0];
		}
		
		$sortCol = isset($colMap[$sortCol]) ? $colMap[$sortCol] : $sortCol; 
		
		if (isset($this->request->get['sSortDir_0'])) {
			$sortDir = $this->request->get['sSortDir_0'] == 'desc' ? "DESC" : "ASC";
		} else {
			$sortDir = $defWay ? $defWay : "ASC";
		}
		
		return array($sortCol, $sortDir);
	}
	
	public function getFilterParams($filters, $colMap) {
		$filterParams = array();
		for ($col=0; $col < $this->request->get['iColumns']; $col++) {
			if (isset($this->request->get['sSearch_' .$col])) {
				$colName = $this->request->get['mDataProp_' . $col];
				$filterVal = $this->request->get['sSearch_' .$col];
				if (!empty($filterVal) && in_array($colName, $filters)) {
					$colName = isset($colMap[$colName]) ? $colMap[$colName] : $colName;
					$filterParams[$colName] = $this->request->get['sSearch_' .$col];
				}
			}
		}
		
		return $filterParams;
	}	
	
	public function setBreadcrumbs($data) {
		$breadcrumbs = array();
		
		$breadcrumbs[] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL'),
        	'separator' => false
      	);
		
		foreach ($data as $breadcrumb) {
	      	$breadcrumbs[] = array(
	        	'text'      => $breadcrumb['text'],
				'href'      => $breadcrumb['href'],
	        	'separator' => $this->language->get('text_separator')
	      	);
		}
		
		return $breadcrumbs;
	}
	
	public function admSetBreadcrumbs($data) {
		$breadcrumbs = array();
		
		$breadcrumbs[] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('multimerch/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
        	'separator' => false
      	);
		
		foreach ($data as $breadcrumb) {
	      	$breadcrumbs[] = array(
	        	'text'      => $breadcrumb['text'],
				'href'      => $breadcrumb['href'] . '&token=' . $this->session->data['token'],
	        	'separator' => $this->language->get('text_separator')
	      	);
		}
		
		return $breadcrumbs;
	}	

	public function loadTemplate($templateName, $children = FALSE) {
		// ugly
		if(strpos($templateName, '/') == false) {
			$templateName = 'multiseller/' . $templateName;
		}

		$template = "$templateName.tpl";

		if ($children === FALSE) {
			$children = array(
				'column_left' => $this->load->controller('common/column_left'),
				'column_right' => $this->load->controller('common/column_right'),
				'content_top' => $this->load->controller('common/content_top'),
				'content_bottom' => $this->load->controller('common/content_bottom'),
				'footer' => $this->load->controller('common/footer'),
				'header' => $this->load->controller('common/header')
			);
		}

		return array($template, $children);
	}
	
	public function loadCommonView($template, $data = array()){
		$file = DIR_SYSTEM . 'vendor/multimerchlib/commonviews/' . $template. '.tpl';

		if (is_file($file)) {
			extract($data);

			ob_start();

			require($file);

			return ob_get_clean();
		}

		trigger_error('Error: Could not load template ' . $file . '!');
		exit();
	}

	public function admLoadTemplate($templateName, $children = FALSE) {
		// ugly
		if(strpos($templateName, '/') == false) {
			$templateName = 'multiseller/' . $templateName;
		}

		$template = "$templateName.tpl";
		
		if ($children === FALSE) {
			$children = array(
				'column_left' => $this->load->controller('common/column_left'),
				'footer' => $this->load->controller('common/footer'),
				'header' => $this->load->controller('common/header')
			);
		}
	
		return array($template, $children);
	}
	
	public function addStyle($style, $rel = 'stylesheet', $media = 'screen') {
		if (file_exists("catalog/view/theme/" . MsLoader::getInstance()->load('\MultiMerch\Module\MultiMerch')->getViewTheme() . "/stylesheet/{$style}.css")) {
			$this->document->addStyle("catalog/view/theme/" . MsLoader::getInstance()->load('\MultiMerch\Module\MultiMerch')->getViewTheme() . "/stylesheet/{$style}.css", $rel, $media);
		} else {
			$this->document->addStyle("catalog/view/theme/default/stylesheet/{$style}.css", $rel, $media);
		}
	}
	
	public function getLanguageId($code) {
		$res = $this->db->query("SELECT language_id FROM `" . DB_PREFIX . "language` WHERE code = '" . $code . "'");
		
		return $res->row['language_id'];
	}

	public function getStockStatusName($stock_status_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		return $query->row;
	}

	// @todo Remove from MsHelper
    public function getStockStatuses($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sql .= " ORDER BY name";

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $stock_status_data = $this->cache->get('stock_status.' . (int)$this->config->get('config_language_id'));

            if (!$stock_status_data) {
                $query = $this->db->query("SELECT stock_status_id, name FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY name");

                $stock_status_data = $query->rows;

                $this->cache->set('stock_status.' . (int)$this->config->get('config_language_id'), $stock_status_data);
            }

            return $stock_status_data;
        }
    }

    // @todo Remove from MsHelperg
    public function getTaxClasses($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "tax_class";

            $sql .= " ORDER BY title";

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $tax_class_data = $this->cache->get('tax_class');

            if (!$tax_class_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class");

                $tax_class_data = $query->rows;

                $this->cache->set('tax_class', $tax_class_data);
            }

            return $tax_class_data;
        }
    }
	
	public function isUnsignedFloat($val) {
		$val = $this->uniformDecimalPoint($val);
		$val=str_replace(" ","",trim($val));
		//return eregi("^([0-9])+([\.|,]([0-9])*)?$", $val);
		return preg_match("/^([0-9])+([\.|,]([0-9])*)?$/", $val);
	}

	// Use only on save, not for displaying
	public function uniformDecimalPoint($number) {
		return (float)(str_replace(array($this->language->get('thousand_point'), $this->language->get('decimal_point')), array('', '.'), $number));
	}

	public function trueCurrencyFormat($number, $currency = null) {
		$this->load->model('localisation/currency');
		$currencies = $this->model_localisation_currency->getCurrencies();
		$decimal_place = !empty($currencies[$currency ?: $this->config->get('config_currency')]['decimal_place']) ? $currencies[$currency ?: $this->config->get('config_currency')]['decimal_place'] : 2;
		$decimal_point = $this->language->get('decimal_point');
		$thousand_point = $this->language->get('thousand_point');
		return number_format(round($number, (int)$decimal_place), (int)$decimal_place, $decimal_point, $thousand_point);
	}

	public function trueWeightFormat($number) {
		$decimal_place = 2;
		$decimal_point = $this->language->get('decimal_point');
		$thousand_point = $this->language->get('thousand_point');
		return number_format(round($number, (int)$decimal_place), (int)$decimal_place, $decimal_point, $thousand_point);
	}
	
	public function isInstalled() {
		$is_installed = $this->cache->get('multimerch_module_is_installed');

		if (!$is_installed) {
			$extension_data = array();

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = 'module' ORDER BY code");

			foreach ($query->rows as $result) {
				$extension_data[] = $result['code'];
			}

			$is_installed = array_search('multimerch', $extension_data) !== FALSE;

			$this->cache->set('multimerch_module_is_installed', $is_installed);
		}

		return $is_installed;
	}

	public function renderPmDialog(&$data) {
		if (isset($this->request->get['product_id'])) {
			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$data['product_id'] = (int)$this->request->get['product_id'];
		} else {
			$seller_id = $this->request->get['seller_id'];
			$data['product_id'] = 0;
		}


		$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		if (empty($seller)) return false;

		$data['seller_id'] = $seller_id;
		list($template, $children) = $this->MsLoader->MsHelper->loadTemplate('dialog-sellercontact', 1);
		return $this->load->view($template, $data);
	}

	public function getStatusName($data = array()) {
		$this->load->model('localisation/order_status');

		$order_statuses = $this->model_localisation_order_status->getOrderStatuses(array(
			'language_id' => (isset($data['language_id']) ? $data['language_id'] : $this->config->get('config_language_id'))
		));

		foreach ($order_statuses as $order_status) {
			if ($order_status['order_status_id'] == $data['order_status_id']) {
				return $order_status['name'];
			} else if ((int)$data['order_status_id'] == 0) {
				$this->load->language('multiseller/multiseller');
				return $this->language->get('ms_order_status_initial');
			}
		}

		return '';
	}

	public function formatAddressByOrderInfo($type, $order_info)
	{
		$format = "{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city} {postcode}\n{zone}\n{country}";

		$find = [
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		];

		switch ($type) {
			case 'shipping':
				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				}

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);
				break;

			case 'payment':
			default:
				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				}

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);
				break;
		}

		return str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
	}

	public function isValidUrl($url) {
		return preg_match('|[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,9}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?|i', $url);
		//return (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED));
	}

	public function addScheme($url) {
		$scheme = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
		return parse_url($url, PHP_URL_SCHEME) === null ? $scheme . $url : $url;
	}

	function addHttp($url) {
		if (!preg_match("@^http?://@i", $url) && !preg_match("@^https?://@i", $url)) {
			$url = "http://" . $url;
		}
		return $url;
	}

	public function generateMetaDescription($description) {
		//Truncates a string to 150
		$description = utf8_substr(strip_tags(html_entity_decode($description, ENT_QUOTES, 'UTF-8')), 0, 150);
		//Clear last any symbols
		$description = rtrim($description, "!,.-");

		return (string)$description;
	}

	public function generateAddressForGoogleGeoLocation($data) {
		$result = '';

		if (!isset($data['country']) || !trim($data['country'])){
			return $result;
		}

		if ((isset($data['address']) && trim($data['address'])) && (isset($data['city']) && trim($data['city']))){
			$result.= trim($data['address']) . ',' . trim($data['city']) . ',' . trim($data['country']);
		}elseif (isset($data['city']) && trim($data['city'])){
			$result.= trim($data['city']) . ',' . trim($data['country']);
		}else{
			$result.= trim($data['country']);
		}

		return $result;
	}

	public function slugify($str, $options = array()) {

		$defaults = array(
			'delimiter' => '-',
			'limit' => null,
			'lowercase' => true,
			'replacements' => array(),
			'transliterate' => true,
			'asciify' => true
		);

		// Make sure string is in UTF-8 and strip invalid UTF-8 characters
		$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

		// Merge options
		$options = array_merge($defaults, $options);

		$char_map = array(
			// Latin
			'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
			'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
			'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
			'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
			'ß' => 'ss',
			'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
			'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
			'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
			'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
			'ÿ' => 'y',
			// Latin symbols
			'©' => '(c)',
			// Greek
			'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
			'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
			'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
			'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
			'Ϋ' => 'Y',
			'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
			'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
			'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
			'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
			'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
			// Turkish
			'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
			'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
			// Russian
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
			'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
			'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
			'Я' => 'Ya',
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
			'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
			'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
			'я' => 'ya',
			// Ukrainian
			'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
			'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
			// Czech
			'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
			'Ž' => 'Z',
			'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
			'ž' => 'z',
			// Polish
			'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
			'Ż' => 'Z',
			'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
			'ż' => 'z',
			// Latvian
			'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
			'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
			'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
			'š' => 's', 'ū' => 'u', 'ž' => 'z',
			// German
			'Ä' => 'AE', 'Ö' => 'OE', 'Ü' => 'UE',
			'ä' => 'ae', 'ö' => 'oe', 'ü' => 'ue'
		);

		// Make custom replacements
		$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

		// Transliterate characters to ASCII
		if ($options['transliterate']) {
			$str = str_replace(array_keys($char_map), $char_map, $str);
		}

		// Replace non-alphanumeric characters with our delimiter
		$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

		// Strip non ASCII characters if any still exist
		if ($options['asciify']) {
			$str = preg_replace('/[^A-Za-z0-9_\-]/', '', $str);
		}

		// Truncate slug to max. characters
		$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

		// Remove duplicate delimiters
		$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

		// Remove delimiter from ends
		$str = trim($str, $options['delimiter']);

		return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
	}

	public function createOCSetting($data = array(), $store_id = 0) {
		$res = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($data['code']) . "' AND `key` = '" . $this->db->escape($data['key']) . "' AND `store_id` = '" . (int)$store_id . "'");

		if($res->num_rows && isset($res->row['setting_id'])) {
			$this->db->query("
				UPDATE " . DB_PREFIX . "setting
				SET `value` = '" . (!is_array($data['value']) ? $this->db->escape($data['value']) : $this->db->escape(json_encode($data['value']))) . "',
					`serialized` = '" . (!is_array($data['value']) ? 0 : 1) . "'
				WHERE `setting_id` = '" . (int)$res->row['setting_id'] . "'
			");
		} else {
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "setting
				SET `store_id` = '" . (int)$store_id . "',
					`code` = '" . $this->db->escape($data['code']) . "',
					`key` = '" . $this->db->escape($data['key']) . "',
					`value` = '" . (!is_array($data['value']) ? $this->db->escape($data['value']) : $this->db->escape(json_encode($data['value']))) . "',
					`serialized` = '" . (!is_array($data['value']) ? 0 : 1) . "'
			");
		}
	}

	public function updateOCSettingJson($data = [], $store_id = 0)
	{
		$res = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($data['code']) . "' AND `key` = '" . $this->db->escape($data['key']) . "' AND `store_id` = '" . (int)$store_id . "'");

		if (!empty($res->row['setting_id']) && !empty($res->row['serialized']) && !empty($data['value']) && is_array($data['value'])) {
			$json_value = json_decode($res->row['value'], true);
			$new_json_value = array_merge($json_value, $data['value']);

			$this->db->query("
				UPDATE " . DB_PREFIX . "setting
				SET `value` = '" . $this->db->escape(json_encode($new_json_value)) . "',
					`serialized` = '1'
				WHERE `setting_id` = '" . (int)$res->row['setting_id'] . "'
			");
		}
	}

	public function deleteOCSetting($code, $key = '', $store_id = 0) {
		$this->db->query("
			DELETE FROM " . DB_PREFIX . "setting
			WHERE `store_id` = '" . (int)$store_id . "'
				AND `code` = '" . $this->db->escape($code) . "'"

			. ($key != '' ? " AND `key` = '" . $this->db->escape($key) . "'" : "")
		);

	}

	public function getIntegrationPackVersion() {
		$version = false;
		$theme_name = false;
		$f_name = realpath(__DIR__ . '/../../vqmod/xml/multimerch_c_hooks.xml');

		if(is_file($f_name)) {
			$dom = new DOMDocument('1.0', 'UTF-8');
			$dom->load($f_name);

			$version_tag = $dom->getElementsByTagName('version')->item(0);
			if($version_tag && $version_tag->textContent) {
				$version = $version_tag->textContent;
			}

			$theme_name_tag = $dom->getElementsByTagName('themename')->item(0);
			if($theme_name_tag && $theme_name_tag->textContent) {
				$theme_name = $theme_name_tag->textContent;
			}
		}

		return $theme_name . ' Integration pack ' . $version;
	}

	public function adminUrlLink($route, $args = '', $secure = false) {
		$url = '';
		$admin_config_path = realpath(__DIR__ . '/../../admin/config.php');

		if($admin_config_path) {
			$admin_config_file = fopen($admin_config_path, "r") or die("Unable to open " . $admin_config_path);
			$raw_file_content = fread($admin_config_file, filesize($admin_config_path));

			if($raw_file_content) {
				$pattern = '~\'(HTTP_SERVER|HTTPS_SERVER)\', \'(.*)\'~i';
				preg_match_all($pattern, $raw_file_content, $config_urls);

				$url .= (isset($config_urls[2][1]) && $secure ? $config_urls[2][1] : $config_urls[2][0]) . 'index.php?route=' . $route;

				if ($args) {
					$url .= is_array($args) ? '&amp;' . http_build_query($args) : str_replace('&', '&amp;', '&' . ltrim($args, '&'));
				}
			}

			fclose($admin_config_file);
		}

		return $url;
	}

	public function addPpreapprovalkey($preapprovalKey, $customer_id) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "ms_customer_ppakey SET
					preapprovalkey = '" . $this->db->escape($preapprovalKey) . "',
					customer_id = '" . (int)$customer_id . "'
					ON DUPLICATE KEY UPDATE preapprovalkey = '" . $this->db->escape($preapprovalKey) . "'
					");
		return true;
	}

	public function getPpreapprovalkey($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_customer_ppakey WHERE customer_id=".(int)$customer_id);
		return $query->row;
	}

	public function activePpreapprovalkey($customer_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "ms_customer_ppakey SET active = '1' WHERE `customer_id` = '" . (int)$customer_id . "'");
	}

	/**
	 * Turn on / off vqmod modification file
	 *
	 * @param string $xml
	 * @param int $action. 1 - activate, 0 - disable
	 * @return bool
	 */
	public function setVqmod($xml, $action = 1) {
		$dir_vqmod = str_replace('system', 'vqmod/xml', DIR_SYSTEM);
		$on  = $dir_vqmod . $xml;
		$off = $dir_vqmod . $xml . '_';

		if($action) {
			if(file_exists($off) && is_writable($off)) {
				return rename($off, $on);
			} else {
				return 'File `' . $off . '` does not exist or is not writable!';
			}
		} else {
			if(file_exists($on) && is_writable($on)) {
				return rename($on, $off);
			} else {
				return 'File `' . $on . '` does not exist or is not writable!';
			}
		}
	}

	public function getOcDownload($download_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "download d LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE d.download_id = '" . (int)$download_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function addOcDownload($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "download SET filename = '" . $this->db->escape($data['filename']) . "', mask = '" . $this->db->escape($data['mask']) . "', date_added = NOW()");

		$download_id = $this->db->getLastId();

		foreach ($data['download_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "download_description SET download_id = '" . (int)$download_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		return $download_id;
	}

	public function deleteOcDownload($download_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "download WHERE download_id = '" . (int)$download_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "download_description WHERE download_id = '" . (int)$download_id . "'");
	}

	public function getNumberOfSignificantDigits($number) {
		return strlen(explode('.', $number)[0]);
	}

	/**
	 * Ceils number to the higher significance number.
	 *
	 * @see http://php.net/manual/ru/function.ceil.php#85430
	 *
	 * @param	float|int		$number
	 * @param	int				$significance
	 * @return	bool|float|int
	 */
	public function ceiling($number, $significance = 1) {
		return (is_numeric($number) && is_numeric($significance)) ? (ceil($number / $significance) * $significance) : false;
	}

	/**
	 * Checks if $x is power of $y.
	 *
	 * @param	int		$x		Number.
	 * @param	int		$y		Basis.
	 * @return	bool			True if $x is power of $y, false otherwise.
	 */
	public function isPowerOf($x, $y) {
		if ((int)$x === 0 || (int)$y === 0)
			return false;

		while ($x%$y == 0)
			$x /= $y;

		return $x == 1;
	}

	/**
	 * Checks whether slug already exists.
	 *
	 * If it exists, integer $suffix is added to the end of a slug.
	 *
	 * @param	string		$slug		Slug (seo keyword).
	 * @return	string					Either original, or modified slug.
	 */
	public function handleSlugDuplicate($slug) {
		$duplicate_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword LIKE '" . $this->db->escape($slug) . "'");

		if ($duplicate_query->num_rows) {
			$suffix = 1;
			do {
				$m_slug = $slug . '-' . $suffix;
				$duplicate_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword LIKE '" . $this->db->escape($m_slug) . "'");
				$suffix++;
			} while ($duplicate_query->num_rows);

			$slug = $m_slug;
		}

		return $slug;
	}

	public function getDefaultPlatformAddress()
	{
		return [
			'firstname' => $this->config->get('config_owner'),
			'lastname' => '',
			'company' => '',
			'address_1' => $this->config->get('config_address'),
			'address_2' => '',
			'postcode' => '',
			'city' => '',
			'country' => ''
		];
	}

	public function getCurrencyCodeById($currency_id)
	{
		$sql = "SELECT `code` FROM `" . DB_PREFIX . "currency` WHERE `currency_id` = " . (int)$currency_id;

		$result = $this->db->query($sql);

		return isset($result->row['code']) ? $result->row['code'] : null;
	}

	/**
	 * Moves primary OC category to the beginning of array
	 *
	 *
	 * @param	string		$categories		Initial categories array
	 * @param	int		$primary_category_id	Category id to move to the beginning
	 * @return	array					Modified categories 
	 */
	public function makePrimaryOcCategoryFirst($categories = array(), $primary_category_id = false)
	{
		foreach ($categories as $row_id => &$oc_categories) {
			if (!empty($oc_categories[0])) {
				$cat = end($oc_categories);
			} else{ // in case of adminpanel
				$cat = $oc_categories;
			}

			if ($cat['category_id'] == $primary_category_id && $row_id > 0){
				unset($categories[$row_id]);
				array_unshift($categories, $oc_categories);
				break;
			}
		}

		return $categories;
	}

	public function recursiveTrim($arr, $charlist = ' ')
	{
		if (is_string($arr)) {
			return trim($arr, $charlist);
		} elseif (is_array($arr)) {
			$result = [];

			foreach($arr as $key => $value){
				if (is_array($value)) {
					$result[$key] = $this->recursiveTrim($value, $charlist);
				} else {
					$result[$key] = trim($value, $charlist);
				}
			}

			return $result;
		} else {
			return $arr;
		}
	}

	public function getPossibleCombinations($options) {
		if (empty($options)) {
			return [];
		}

		// Reset array keys
		$options = array_values($options);

		$combinations = [[]];

		$count = count($options);

		for ($i = 0; $i < $count; $i++) {
			$tmp = [];

			foreach ($combinations as $v1) {
				foreach ($options[$i] as $v2)
					$tmp[] = array_merge($v1, [$v2]);

			}

			$combinations = $tmp;
		}

		return $combinations;
	}

	public function getManufacturerIdByName($manufacturer_name)
	{
		$query = $this->db->query("SELECT manufacturer_id FROM " . DB_PREFIX . "manufacturer WHERE name='" . $this->db->escape($manufacturer_name) . "'");

		if (isset($query->row['manufacturer_id'])) {
			$manufacturer_id = $query->row['manufacturer_id'];
		} else {
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "manufacturer
				SET `name` = '" . $this->db->escape(htmlspecialchars($manufacturer_name)) . "'
			");

			$manufacturer_id = $this->db->getLastId();

			$this->db->query("
				INSERT INTO " . DB_PREFIX . "manufacturer_to_store
				SET `manufacturer_id`  = '" . (int)$manufacturer_id . "'
			");
		}
		return $manufacturer_id;
	}

	public function prepareImageByImageUrl($product_id, $image_url, $images_path, $key = 0)
	{
		$from_url = $image_url;
		$img_name = $product_id . ($key ? ('-' . $key) : '') . '-' . basename($from_url);
		$to_url = DIR_IMAGE . $images_path . $img_name;

		$allowed_filetypes = $this->config->get('msconf_msg_allowed_file_types');
		$filetypes = explode(',', $allowed_filetypes);
		$filetypes = array_map('strtolower', $filetypes);
		$filetypes = array_map('trim', $filetypes);

		$ext = explode('.', $img_name);
		$ext = end($ext);

		$remote_url_headers = get_headers($from_url);

		if (in_array(strtolower($ext), $filetypes) && stripos($remote_url_headers[0],"200 OK") && !file_exists($to_url)) {
			copy($from_url, $to_url);
			return $images_path . $img_name;
		}

		return '';
	}

	public function isFileByUrlValid($url, $return_as_array = false)
	{
		$errors = [];

		// Validation
		if (empty($url)) {
			$errors[] = $this->language->get('ms_feed_error_source_url_not_specified');
		} else {
			if (!filter_var($url, FILTER_VALIDATE_URL)) {
				$errors[] = $this->language->get('ms_feed_error_source_url_invalid');
			} else {
				$url_parts = explode('/', $url);
				$file_name = end($url_parts);

				$ext = explode('.', $file_name);
				$ext = end($ext);

				if (strtolower($ext) !== 'csv') {
					$errors[] = sprintf($this->language->get('ms_error_file_extension'), $file_name, strtolower($ext), 'csv');
				}
			}
		}

		if ($return_as_array) {
			return $errors;
		}

		$this->response->addHeader('Content-Type: application/json');
		return $this->response->setOutput(json_encode(['errors' => $errors]));
	}
}
