<?php
class MsFile extends Model {
	private function _isNewUpload($fileName) {
		return file_exists(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $fileName) || file_exists(DIR_DOWNLOAD . $this->config->get('msconf_temp_download_path') . $fileName);
	}

	// ***FUNCTION***: checks whether file already exists and proposes a new name for a file
	function _checkExistingFiles($path, $filename) {
		$newFilename = $filename;
		$i = 1;

		while (file_exists($path . '/' . $newFilename)) {
			$newFilename = substr($filename, 0, strrpos($filename, '.')) . "-" . $i++ . substr($filename, strrpos($filename, '.'));
		}

		return $newFilename;
	}

	function _checkExistingFilesSizes($path, $filename, $md5) {
		$newFilename = $filename;
		$i = 1;

		while ( file_exists($path . '/' . $newFilename) && ($filesize != filesize($path . '/' . $newFilename)) ) {
			$newFilename = substr($filename, 0, strrpos($filename, '.')) . "-" . $i++ . substr($filename, strrpos($filename, '.'));
		}

		return $newFilename;
	}

	function _checkExistingFilesMd5($path, $filename, $md5) {
		$newFilename = $filename;
		$i = 1;

		while ( file_exists($path . '/' . $newFilename) && ($md5 !== md5_file($path . '/' . $newFilename)) ) {
			$newFilename = substr($filename, 0, strrpos($filename, '.')) . "-" . $i++ . substr($filename, strrpos($filename, '.'));
		}

		return $newFilename;
	}

	public function checkPostMax($post, $files) {
		$errors = array();

		if (empty($post) || empty($files)) {
			$POST_MAX_SIZE = ini_get('post_max_size');
			$mul = substr($POST_MAX_SIZE, -1);
			$mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
			if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > $mul * (int)$POST_MAX_SIZE && $POST_MAX_SIZE) {
				$errors[] = sprintf($this->language->get('ms_error_file_post_size'), $this->language->get('ms_file_default_filename'), $POST_MAX_SIZE);

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_post_size'), $this->language->get('ms_file_default_filename'), $POST_MAX_SIZE));
			} else {
				$errors[] = sprintf($this->language->get('ms_error_file_upload_error'), $this->language->get('ms_file_default_filename'), $this->language->get('ms_file_unclassified_error'));

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_upload_error'), $this->language->get('ms_file_default_filename'), $this->language->get('ms_file_unclassified_error')));
			}
		}

		return $errors;
	}

	public function checkFile($file, $allowed_filetypes) {
		$errors = array();

		$filetypes = explode(',', $allowed_filetypes);
		$filetypes = array_map('strtolower', $filetypes);
		$filetypes = array_map('trim', $filetypes);

		$ext = explode('.', $file['name']);
		$ext = end($ext);

		if (!in_array(strtolower($ext),$filetypes)) {
			$errors[] = sprintf($this->language->get('ms_error_file_extension'), $file['name'], strtolower($ext), implode(', ', $filetypes));

			$this->ms_logger->error(sprintf($this->language->get('ms_error_file_extension'), $file['name'], strtolower($ext), implode(', ', $filetypes)));
		}

		if ($file["error"] != UPLOAD_ERR_OK) {
			if ($file["error"] == UPLOAD_ERR_INI_SIZE || $file["error"] == UPLOAD_ERR_FORM_SIZE) {
				$UPLOAD_MAX_FILESIZE = ini_get('upload_max_filesize');

				$errors[] = sprintf($this->language->get('ms_error_file_upload_size'), $file['name'], $UPLOAD_MAX_FILESIZE);

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_upload_size'), $file['name'], $UPLOAD_MAX_FILESIZE));
			} else {
				$errors[] = sprintf($this->language->get('ms_error_file_upload_error'), $file['name'], 'UPLOAD_ERR #' . $file["error"]);

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_upload_error'), $file['name'], 'UPLOAD_ERR #' . $file["error"]));
			}
		} else {
			// todo filename size
			if (utf8_strlen($file['name']) > 150) {
				$errors[] = sprintf($this->language->get('ms_error_file_upload_error'), $file['name'], sprintf($this->language->get('ms_file_filename_error_greater'), 150));

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_upload_error'), $file['name'], sprintf($this->language->get('ms_file_filename_error_greater'), 150)));
			}
		}

		return $errors;
	}

	public function checkDownload($file) {
		return $this->checkFile($file, $this->config->get('msconf_allowed_download_types'));
	}

	public function checkImportFile($file) {
		return $this->checkFile($file, 'csv');
	}

	public function checkImage($file) {
		$errors = $this->checkFile($file, $this->config->get('msconf_allowed_image_types'));
		
		if (!$errors) {
			$size = getimagesize($file["tmp_name"]);
			
			list($width, $height, $type, $attr) = getimagesize($file["tmp_name"]);
			
			if (($this->config->get('msconf_min_uploaded_image_width') > 0 && $width < $this->config->get('msconf_min_uploaded_image_width')) || ($this->config->get('msconf_min_uploaded_image_height') > 0 && $height < $this->config->get('msconf_min_uploaded_image_height'))) {
				$errors[] = sprintf($this->language->get('ms_error_image_too_small'), $this->config->get('msconf_min_uploaded_image_width'), $this->config->get('msconf_min_uploaded_image_height'));
			} else if (($this->config->get('msconf_max_uploaded_image_width') > 0 && $width > $this->config->get('msconf_max_uploaded_image_width')) || ($this->config->get('msconf_max_uploaded_image_height') > 0 && $height > $this->config->get('msconf_max_uploaded_image_height'))) {
				$errors[] = sprintf($this->language->get('ms_error_image_too_big'), $this->config->get('msconf_max_uploaded_image_width'), $this->config->get('msconf_max_uploaded_image_height'));
			}
			//@TODO? Flash reports all files as octet-stream
			//if(!isset($size) || stripos($file['type'],'image/') === FALSE || stripos($size['mime'],'image/') === FALSE) {
			if(!isset($size)) {
				$errors[] = sprintf($this->language->get('ms_error_file_type'), $file['name']);

				$this->ms_logger->error(sprintf($this->language->get('ms_error_file_type'), $file['name']));
			}
		}

		return $errors;
	}
	
	public function moveDescriptionImages($text = array(), $moveFor = ''){
		if (empty($this->session->data['multiseller']['description_images'])){
			return FALSE;
		}
		$already_reuploaded = array(); //for same image in few langs descriptions
		
		foreach ($this->session->data['multiseller']['description_images'] as $i => $image_url){
			foreach ($text as &$one_lang_text){
				if (strpos($one_lang_text, $image_url) !== false ){
					
					if ( isset($already_reuploaded[$image_url]) ){
						$new_url = $already_reuploaded[$image_url];
					} else{
						$filename = basename($image_url);
						$new_url = $this->uploadDescriptionImage([
							'name' => $filename, 
							'tmp_name' => DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $filename], 
							$moveFor
						)[1];
						
						$already_reuploaded[$image_url] = $new_url;
					}
					
					$one_lang_text = str_replace($image_url, $new_url, $one_lang_text);
					unset($this->session->data['multiseller']['description_images'][$i]);
				}
			}
		}
		if (sizeof($this->session->data['multiseller']['description_images']) > 0){
			foreach ($this->session->data['multiseller']['description_images'] as $i => $image_url){
				$filename = basename($image_url);
				if (file_exists(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $filename)){
					unlink(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $filename);
				}
				unset($this->session->data['multiseller']['description_images'][$i]);
			}
		}
		
		return $text;
	}

	/**
	* Erase images which are not presented in description (product`s or seller`s)
	* @param string $text
	* description of product or seller to search in
	* @param string $checkFor
	* product_id or 'description'
	*/

	public function checkDescriptionImages($text = '', $checkFor = ''){
		
		$images_path = DIR_IMAGE . $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/". $checkFor . "/";

		if (file_exists($images_path)){
			
			$img_dir = scandir($images_path);
			
			foreach ($img_dir as $ik => $filename){
				if (!in_array($filename, ['.','..','index.html'])){
					if (strpos($text, $filename) === FALSE){
						unlink($images_path . $filename);
					}
				}
			}
		}
	}
	
	public function uploadDescriptionImage($file, $imageFor = false) {
		$ext = explode('.', $file["name"]);
		$ext = mb_strtolower(end($ext));
		$filename = md5(microtime() . $file["name"]) . '.' . $ext;
		
		// For new products or new sellers with no ID
		if (empty($imageFor)){
			$uploadingPath = DIR_IMAGE . $this->config->get('msconf_temp_image_path');
			$imageDir = $this->config->get('msconf_temp_image_path');
		} else{
			
			$uploadingPath = DIR_IMAGE . $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . $imageFor . "/";
			$imageDir = $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . $imageFor . "/";
			// Create seller`s directory
			if (!is_dir(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/")) {
				mkdir(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/", 0755);
				@touch(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . 'index.html');
			}
			// Create destination directory
			if (!is_dir(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . $imageFor . "/")) {
				mkdir(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . $imageFor . "/", 0755);
				@touch(DIR_IMAGE .  $this->config->get('msconf_product_image_path') . $this->customer->getId() . "/" . $imageFor . "/" . 'index.html');
			}
			
		}
		
		//Check sizes and resize before moving to final destination
		list($width, $height) = getimagesize($file["tmp_name"]);
		
		$max_width  = $this->config->get('msconf_max_description_image_width');
		$max_height = $this->config->get('msconf_max_description_image_height');

		if ($max_width && $max_height && ($width > $max_width || $height > $max_height) ){
			
			// Lets define proportional size for new image:
			if (($width - $max_width) > ($height - $max_height)) {
				$scale_w =  $max_width / $width;
				$scale_h = $scale_w;
			} else{
				$scale_w =  $max_height / $height;
				$scale_h = $scale_w;
			}
			
			$new_width = $width * $scale_w;
			$new_height = $height * $scale_h;
			
			$image = new Image($file["tmp_name"]);
			$image->resize($new_width, $new_height, '');
			$image->save($uploadingPath . $filename);
			
		} else{
			rename($file["tmp_name"], $uploadingPath . $filename);
		}
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$base = defined('HTTPS_CATALOG') ? HTTPS_CATALOG : HTTPS_SERVER;
		} else {
			$base = defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER;
		}

		$url = $base .'image/' . $imageDir . $filename;
		
		if (empty($imageFor)){
			$this->session->data['multiseller']['description_images'][] = $url;
		}
	
		return [$filename, $url];
	}

	public function uploadImage($file) {
		$filename =   time() . '_' . md5(rand()) . '.' . $file["name"];
		move_uploaded_file($file["tmp_name"], DIR_IMAGE . $this->config->get('msconf_temp_image_path') .  $filename);

		if (!in_array($filename, $this->session->data['multiseller']['files'])) {
			$this->session->data['multiseller']['files'][] = $filename;
		}
		return $filename;
	}

	public function uploadDownload($file) {
		$filename =   time() . '_' . md5(rand()) . '.' . $this->MsLoader->MsSeller->getNickname() . '_' . $file["name"];
		move_uploaded_file($file["tmp_name"], DIR_DOWNLOAD . $this->config->get('msconf_temp_download_path') .  $filename);

		if (!in_array($filename, $this->session->data['multiseller']['files']))
			$this->session->data['multiseller']['files'][] = $filename;

		return array(
			'fileName' => $filename,
			'fileMask' => $file['name']
		);
	}

	public function uploadImportFile($file) {
		$filename =   time() . '_' . md5(rand()) . '.' . $this->MsLoader->MsSeller->getNickname() . '_' . $file["name"];
		move_uploaded_file($file["tmp_name"], DIR_UPLOAD . $filename);

		return array(
			'fileName' => $filename,
			'fileMask' => $file['name']
		);
	}

	public function checkFileAgainstSession($fileName) {
		if (array_search($fileName, $this->session->data['multiseller']['files']) === FALSE) {
			return FALSE;
		}

		return TRUE;
	}

	public function moveDownload($fileName) {
		$newpath = $fileName;

		$key = array_search($fileName, $this->session->data['multiseller']['files']);

		//strip nonce and timestamp
		$original_file_name = substr($fileName, strpos($fileName, '.') + 1, utf8_strlen($fileName));

		if ($this->_isNewUpload($fileName)) {
			$newpath = $original_file_name . '.' . md5(rand());
			//var_dump($newpath);
			rename(DIR_DOWNLOAD . $this->config->get('msconf_temp_download_path') . $fileName, DIR_DOWNLOAD . $newpath);
		}

		unset ($this->session->data['multiseller']['files'][$key]);

		return $newpath;
	}

	public function moveImage($path) {
		$key = array_search($path, $this->session->data['multiseller']['files']);
		if ($key === FALSE) return;

		$dirname = dirname($path) . '/';
		$filename = basename($path);

		$imageDir = $this->config->get('msconf_product_image_path');

		// Check if folder exists and create if not
		if (!is_dir(DIR_IMAGE . $imageDir . $this->customer->getId() . "/")) {
			mkdir(DIR_IMAGE . $imageDir . $this->customer->getId() . "/", 0755);
			@touch(DIR_IMAGE . $imageDir . $this->customer->getId() . "/" . 'index.html');
		}

		if ($dirname == './') {
			// new upload
			$dirname = $this->config->get('msconf_temp_image_path');
			//strip nonce and timestamp
			$originalFilename = $filename;
			$filename = substr($filename, strpos($filename, '.') + 1, utf8_strlen($filename));
		}

		if (DIR_IMAGE . $imageDir . $this->customer->getId() . "/" . $filename != DIR_IMAGE . $path) {
			$newFilename = $this->_checkExistingFiles(DIR_IMAGE . $imageDir . $this->customer->getId(), $filename);
			$newPath = $imageDir . $this->customer->getId() . "/" . $newFilename;
			rename(DIR_IMAGE . $dirname . (isset($originalFilename) ? $originalFilename : $filename), DIR_IMAGE . $newPath);
		} else {
			$newPath = $imageDir . $this->customer->getId() . "/" . $filename;
		}

		unset ($this->session->data['multiseller']['files'][$key]);
		return $newPath;
	}

	public function deleteDownload($fileName) {
		if (empty($fileName))
			return false;

		$key = array_search($fileName, $this->session->data['multiseller']['files']);

		if (file_exists(DIR_DOWNLOAD . $fileName)) {
			unlink(DIR_DOWNLOAD. $fileName);
		}

		unset ($this->session->data['multiseller']['files'][$key]);
	}

	public function deleteImage($fileName) {
		if (empty($fileName))
			return false;

		$key = array_search($fileName, $this->session->data['multiseller']['files']);

		if (file_exists(DIR_IMAGE. $fileName)) {
			unlink(DIR_IMAGE. $fileName);
		}

		unset ($this->session->data['multiseller']['files'][$key]);
	}

	public function resizeImage($filename, $width, $height, $w = '') {
		// todo consider using default cache folder
		if (!file_exists(DIR_IMAGE . $filename) || !$filename || !filesize(DIR_IMAGE . $filename)) {
			return;
		}

		$size = getimagesize(DIR_IMAGE . $filename);
		if (!$size) return;
		
		$info = pathinfo($filename);
		$extension = $info['extension'];

		$temporary_filename = time() . '_' . md5(rand()) . '.' . $info["basename"];
		$image = new Image(DIR_IMAGE . $filename);
		$image->resize($width, $height, $w);
		$image->save(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $temporary_filename);

		$file = substr($info['basename'], 0, strrpos($info['basename'], '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		$new_image = $this->_checkExistingFilesMd5(DIR_IMAGE . $this->config->get('msconf_temp_image_path'), $file, md5_file(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $temporary_filename));

		if (copy(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $temporary_filename, DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $new_image)) {
			unlink(DIR_IMAGE . $this->config->get('msconf_temp_image_path') . $temporary_filename);
		}

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$base = defined('HTTPS_CATALOG') ? HTTPS_CATALOG : HTTPS_SERVER;
			return $base . 'image/' . $this->config->get('msconf_temp_image_path') . $new_image;
		} else {
			$base = defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER;
			return $base . 'image/' . $this->config->get('msconf_temp_image_path') . $new_image;
		}
	}
}
?>
