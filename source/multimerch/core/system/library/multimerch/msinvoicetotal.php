<?php

use MultiMerch\Core\Invoice\InvoiceTotal as InvoiceTotal;
use MultiMerch\Core\Invoice\Factories\InvoiceTotalFactory as InvoiceTotalFactory;

class MsInvoiceTotal extends Model
{
	protected $invoice_total_factory;

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->invoice_total_factory = new InvoiceTotalFactory($registry);
	}

	/**
	 * Retrieves totals of specified invoice from the database.
	 *
	 * @param	array	$data	Parameters.
	 * @return	array			Array of invoice totals.
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	public function get($data = [])
	{
		$invoice_totals = [];

		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_invoice_total`
			WHERE 1 = 1"
			. (isset($data['invoice_id']) ? " AND `invoice_id` = " . (int)$data['invoice_id'] : "")
			. (isset($data['type']) ? " AND `type` = '" . $this->db->escape($data['type']) . "'" : "")
		;

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$row['titles'] = $this->getTitles($row['invoice_total_id']);
				$invoice_totals[] = $this->invoice_total_factory->create($row['type'], $row);
			}
		}

		return $invoice_totals;
	}

	/**
	 * Stores invoice total instance in the database.
	 *
	 * @param	InvoiceTotal	$invoice_total	Invoice total instance.
	 * @return	int								Invoice total id.
	 */
	public function create(InvoiceTotal $invoice_total)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_invoice_total`
			SET `invoice_id` = " . (int)$invoice_total->getInvoiceId() . ",
				`type` = '" . $this->db->escape($invoice_total->getType()) . "',
				`value` = " . (float)$invoice_total->getValue() . "
		";

		$this->db->query($sql);

		$invoice_total_id = $this->db->getLastId();

		// Set titles
		$this->saveTitles($invoice_total_id, $invoice_total->getTitles());

		return $invoice_total_id;
	}

	/**
	 * Updates invoice total in the database.
	 *
	 * @param	int		$invoice_total_id	Invoice total id.
	 * @param	array	$data				Parameters.
	 */
	public function update($invoice_total_id, $data = [])
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_invoice_total`
			SET `invoice_total_id` = `invoice_total_id`"
			. (isset($data['invoice_id']) ? ", `invoice_id` = " . (int)$data['invoice_id'] : "")
			. (isset($data['type']) ? ", `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['value']) ? ", `value` = " . (float)$data['value'] : "")
			. " WHERE `invoice_total_id` = " . $invoice_total_id
		;

		$this->db->query($sql);

		// Set titles
		if (isset($data['titles']))
			$this->saveTitles($invoice_total_id, $data['titles']);
	}

	/**
	 * Deletes invoice total from the database.
	 *
	 * @param	int		$invoice_total_id	Invoice total id.
	 */
	public function delete($invoice_total_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_invoice_total`
			WHERE `invoice_total_id` = " . (int)$invoice_total_id
		;

		$this->db->query($sql);
	}

	/**
	 * Gets invoice total titles translated in different languages.
	 *
	 * @param	int		$invoice_total_id	Invoice total id.
	 * @return	array						Invoice total titles.
	 */
	private function getTitles($invoice_total_id)
	{
		$titles = [];

		$sql = "
			SELECT
				language_id,
				`title`
			FROM `" . DB_PREFIX . "ms_invoice_total_description`
			WHERE `invoice_total_id` = " . (int)$invoice_total_id . "
		";

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$titles[$row['language_id']] = $row['title'];
			}
		}

		return $titles;
	}

	/**
	 * Creates or updates titles of the invoice total.
	 *
	 * @param	int		$invoice_total_id	Invoice total id.
	 * @param	array	$titles				Array of titles. The structure is: [language_id => title, language_id => title ... ]
	 */
	private function saveTitles($invoice_total_id, $titles)
	{
		if (empty($titles))
			return;

		foreach ($titles as $language_id => $title) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_invoice_total_description`
				SET `invoice_total_id` = " . (int)$invoice_total_id . ",
					`language_id` = " . (int)$language_id . ",
					`title` = '" . $this->db->escape($title) . "'
				ON DUPLICATE KEY UPDATE
					`title` = '" . $this->db->escape($title) . "'
			";

			$this->db->query($sql);
		}
	}
}
