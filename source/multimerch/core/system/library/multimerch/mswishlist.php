<?php

class MsWishlist extends Model
{
	const NAME_DEFAULT = 'Wish List';

	private $customer_id;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->customer_id = $this->customer->getId();
	}

	public function getWishlists($data = [])
	{
		$sql = "
			SELECT
				`id`,
				`name`
			FROM `" . DB_PREFIX . "ms_wishlist`
			WHERE customer_id = " . (int)$this->customer_id
		;

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$row['products'] = $this->getWishlistProducts($row['id']);
		}

		return $result->rows;
	}

	public function getWishlist($data = [])
	{
		$sql = "
			SELECT
				`id`,
				`name`
			FROM `" . DB_PREFIX . "ms_wishlist`
			WHERE customer_id = " . (int)$this->customer_id

			. (isset($data['id']) ? " AND `id` = " . (int)$data['id'] : "")
			. (isset($data['name']) ? " AND `name` = '" . $this->db->escape($data['name']) . "'" : "")

			. " ORDER BY id DESC LIMIT 1"
		;

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			$result->row['products'] = $this->getWishlistProducts($result->row['id']);
		}

		return $result->row;
	}

	public function getWishlistProducts($wishlist_id)
	{
		$sql = "
			SELECT
				product_id,
				`date_added`
			FROM `" . DB_PREFIX . "ms_wishlist_product`
			WHERE `customer_id` = " . (int)$this->customer_id . "
				AND `wishlist_id` = " . (int)$wishlist_id
		;

		$result = $this->db->query($sql);

		return $result->rows;
	}

	public function createWishlist($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_wishlist`
			SET `customer_id` = " . (int)$this->customer_id

			. (isset($data['name']) ? ", `name` = '" . $this->db->escape($data['name']) . "'" : "")
		;

		$this->db->query($sql);

		$wishlist_id = $this->db->getLastId();

		if (!empty($data['products'])) {
			foreach ($data['products'] as $product_id) {
				$this->addProductToWishlist($wishlist_id, $product_id);
			}
		}

		return $wishlist_id;
	}

	public function addProductToWishlist($wishlist_id, $product_id)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_wishlist_product`
			SET `customer_id` = " . (int)$this->customer_id . ",
				`wishlist_id` = " . (int)$wishlist_id . ",
				`product_id` = " . (int)$product_id . "
		";

		$this->db->query($sql);

		return true;
	}

	public function removeProductFromWishlist($wishlist_id, $product_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_wishlist_product`
			WHERE `customer_id` = " . (int)$this->customer_id . "
				AND `wishlist_id` = " . (int)$wishlist_id . "
				AND `product_id` = " . (int)$product_id
		;

		$this->db->query($sql);

		return true;
	}

	public function deleteWishlist($wishlist_id)
	{
		$sql = "
			DELETE w, wp
			FROM `" . DB_PREFIX . "ms_wishlist` w
			LEFT JOIN `" . DB_PREFIX . "ms_wishlist_product` wp
				ON (wp.`wishlist_id` = w.`id`)
			WHERE w.`wishlist_id` = " . (int)$wishlist_id . "
				AND w.`customer_id` = " . (int)$this->customer_id
		;

		$this->db->query($sql);

		return true;
	}
}
