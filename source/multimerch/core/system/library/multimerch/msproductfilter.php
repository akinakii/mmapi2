<?php

class MsProductFilter extends Model
{
	const TYPE_OC_ATTRIBUTE = 1;
	const TYPE_OC_OPTION = 2;
	const TYPE_OC_MANUFACTURER = 3;
	const TYPE_MSF_ATTRIBUTE = 4;
	const TYPE_PRICE = 5;
	const TYPE_CATEGORY = 6;

	public function getProducts($data, $ids_only = false)
	{
		$products = [];

		$result = $this->db->query($this->getProductsQuery($data));

		$this->load->model('catalog/product');
		foreach ($result->rows as $row) {
			$products[$row['product_id']] = $ids_only
				? $row['product_id']
				: $this->model_catalog_product->getProduct($row['product_id']);
		}

		return $products;
	}

	public function getTotalProducts($data)
	{
		$this->db->query($this->getProductsQuery($data, true));

		$total_products = $this->db->query("SELECT FOUND_ROWS() as total")->row['total'];

		return $total_products;
	}



	public function getOcCategory($category_id)
	{
		$result = $this->db->query("
			SELECT DISTINCT
				c.category_id,
				c.parent_id,
				c.sort_order,
				cd.name
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN (SELECT category_id, `name` FROM `" . DB_PREFIX . "category_description` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "') cd
				ON (cd.category_id = c.category_id)
			WHERE c.category_id = '" . (int)$category_id . "'
			ORDER BY c.sort_order
		");

		return $result->row;
	}

	public function getOcChildCategories($category_id, $data = [])
	{
		$result = $this->db->query("
			SELECT DISTINCT
				cd.category_id,
				cd.name
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN `" . DB_PREFIX . "category_description` cd
				ON (cd.category_id = c.category_id)
			WHERE c.status = 1
				AND c.parent_id = '" . (int)$category_id . "'
				AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			GROUP BY cd.category_id
			ORDER BY c.sort_order, cd.name
		");

		return $result->rows;
	}

	public function getMsCategory($category_id, $seller_id)
	{
		if (0 === (int)$category_id) {
			$this->load->language('multiseller/multiseller');

			$result = (object) ['row' => [
				'category_id' => 0,
				'parent_id' => null,
				'sort_order' => 0,
				'name' => $this->language->get('ms_all_products')
			]];
		} else {
			$result = $this->db->query("
				SELECT DISTINCT
					mc.category_id,
					mc.parent_id,
					mc.sort_order,
					mcd.`name`
				FROM `" . DB_PREFIX . "ms_category` mc
				LEFT JOIN (SELECT category_id, `name` FROM `" . DB_PREFIX . "ms_category_description` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "') mcd
					ON (mcd.category_id = mc.category_id)
				WHERE mc.category_status = 1
					AND mc.category_id = '" . (int)$category_id . "'
					AND mc.seller_id = " . (int)$seller_id . "
				ORDER BY mc.sort_order, mcd.`name`
			");
		}

		return $result->row;
	}

	public function getMsChildCategories($category_id, $seller_id, $data = [])
	{
		$result = $this->db->query("
			SELECT DISTINCT
				mcd.category_id,
				mcd.`name`
			FROM `" . DB_PREFIX . "ms_category` mc
			LEFT JOIN `" . DB_PREFIX . "ms_category_description` mcd
				ON (mcd.category_id = mc.category_id)
			WHERE mc.category_status = 1
				AND mc.parent_id = '" . (int)$category_id . "'
				AND mc.seller_id = " . (int)$seller_id . "
				AND mcd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			GROUP BY mcd.category_id
			ORDER BY mc.sort_order, mcd.`name`
		");

		return $result->rows;
	}



	public function getOcOptions($product_ids, $oc_option_id = 0)
	{
		if (empty($product_ids))
			return [];

		$sql = "
			SELECT
				pov.option_id,
				od.`name`,
				pov.option_value_id,
				ov.sort_order as `option_value_sort_order`,
				IF (pov.option_value_id IS NOT NULL,
					(
						SELECT
							`name`
						FROM `" . DB_PREFIX . "option_value_description`
						WHERE option_value_id = pov.option_value_id
							AND language_id = '" . (int)$this->config->get('config_language_id') . "'
					),
					NULL
				) as `option_value_name`,
				po.`value`,
				o.type,
				o.sort_order
			FROM `" . DB_PREFIX . "product_option_value` pov
			LEFT JOIN `" . DB_PREFIX . "option` o
				ON (o.option_id = pov.option_id)
			LEFT JOIN `" . DB_PREFIX . "option_description` od
				ON (od.option_id = pov.option_id)
			LEFT JOIN (SELECT option_value_id, sort_order FROM `" . DB_PREFIX . "option_value`) ov
				ON (ov.option_value_id = pov.option_value_id)
			LEFT JOIN `" . DB_PREFIX . "product` p
				ON (p.product_id = pov.product_id)
			LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s
				ON (p2s.product_id = p.product_id)
			LEFT JOIN `" . DB_PREFIX . "product_option` po
				ON (po.product_option_id = pov.product_option_id)
			WHERE p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
				AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND pov.product_id IN (" . implode(',', $product_ids) . ")"

			. ($oc_option_id ? " AND pov.option_id = " . (int)$oc_option_id : "")

			. " ORDER BY o.sort_order, od.`name`, `option_value_sort_order`
		";

		$result = $this->db->query($sql);

		$oc_options = [];

		foreach ($result->rows as $row) {
			$option_id = (int)$row['option_id'];
			$option_value_id = (int)$row['option_value_id'];
			$value = htmlspecialchars($row['value'], ENT_COMPAT);
			$type = (string)$row['type'];

			if (in_array($type, ['file', 'date', 'time', 'datetime', 'textarea'])) {
				// Skip if custom field's type is inappropriate
				continue;
			}

			if (!isset($oc_options[$option_id])) {
				$oc_options[$option_id] = [
					'name' => $row['name'],
					'type' => $type,
					'id_values' => [], // [id => name, id => name, ... ]
					'text_values' => [] // [name, name, ... ]
				];
			}

			if (in_array($type, ['select', 'radio', 'checkbox']) && !isset($oc_options[$option_id]['id_values'][$option_value_id])) {
				$oc_options[$option_id]['id_values'][$option_value_id] = htmlspecialchars($row['option_value_name'], ENT_COMPAT);
			} elseif ($value && !in_array($value, $oc_options[$option_id]['text_values'])) {
				$oc_options[$option_id]['text_values'][] = $value;
			}
		}

		return $oc_options;
	}

	public function getOcAttributes($product_ids, $oc_attribute_id = 0)
	{
		if (empty($product_ids))
			return [];

		$sql = "
			SELECT
				pa.attribute_id,
				ad.`name`,
				pa.`text`,
				a.sort_order
			FROM `" . DB_PREFIX . "product_attribute` pa
			LEFT JOIN (SELECT attribute_id, sort_order FROM `" . DB_PREFIX . "attribute`) a
				ON (a.attribute_id = pa.attribute_id)
			LEFT JOIN (SELECT attribute_id, language_id, `name` FROM `" . DB_PREFIX . "attribute_description`) ad
				ON (ad.attribute_id = pa.attribute_id)
			LEFT JOIN (SELECT product_id, status, date_available FROM `" . DB_PREFIX . "product`) p
				ON (p.product_id = pa.product_id)
			LEFT JOIN (SELECT product_id, store_id FROM `" . DB_PREFIX . "product_to_store`) p2s
				ON (p2s.product_id = p.product_id)
			WHERE p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
				AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND pa.product_id IN (" . implode(',', $product_ids) . ")"

			. ($oc_attribute_id ? " AND pa.attribute_id = " . (int)$oc_attribute_id : "")

			. " ORDER BY a.sort_order, ad.`name`, pa.`text`
		";

		$result = $this->db->query($sql);

		$oc_attributes = [];

		foreach ($result->rows as $row) {
			$attribute_id = (int)$row['attribute_id'];
			$value = htmlspecialchars($row['text'], ENT_COMPAT);

			if (!isset($oc_attributes[$attribute_id])) {
				$oc_attributes[$attribute_id] = [
					'name' => $row['name'],
					'text_values' => [] // [name, name, ... ]
				];
			}

			if ($value && !in_array($value, $oc_attributes[$attribute_id]['text_values'])) {
				$oc_attributes[$attribute_id]['text_values'][] = $value;
			}
		}

		return $oc_attributes;
	}

	public function getManufacturers($product_ids)
	{
		if (empty($product_ids))
			return [];

		$sql = "
			SELECT DISTINCT
				p.manufacturer_id,
				m.`name`,
				m.sort_order
			FROM `" . DB_PREFIX . "product` p
			LEFT JOIN `" . DB_PREFIX . "manufacturer` m
				ON (m.manufacturer_id = p.manufacturer_id)
			LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s
				ON (p2s.product_id = p.product_id)
			WHERE p.product_id IN (" . implode(',', $product_ids) . ")
				AND p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
				AND p.manufacturer_id > 0
			ORDER BY m.sort_order, m.`name`
		";

		$result = $this->db->query($sql);

		$manufacturers = [];

		foreach ($result->rows as $row) {
			$manufacturer_id = (int)$row['manufacturer_id'];
			$value = htmlspecialchars($row['name'], ENT_COMPAT);

			if (!isset($manufacturers[$manufacturer_id])) {
				$manufacturers[$manufacturer_id] = $value;
			}
		}

		return $manufacturers;
	}

	public function getMsCustomFields($product_ids, $ms_custom_field_id = 0)
	{
		if (empty($product_ids) || !$this->config->get('msconf_ms_custom_field_enabled'))
			return [];

		$sql = "
			SELECT
				cf.custom_field_id,
				cfd.`name`,
				pcf.custom_field_value_id,
				cfv.sort_order as `custom_field_value_sort_order`,
				IF (pcf.custom_field_value_id IS NOT NULL,
					(
						SELECT
							`name`
						FROM `" . DB_PREFIX . "ms_custom_field_value_description`
						WHERE custom_field_value_id = pcf.custom_field_value_id
							AND language_id = '" . (int)$this->config->get('config_language_id') . "'
					),
					NULL
				) as `custom_field_value_name`,
				pcf.`value`,
				cf.type,
				cf.sort_order
			FROM `" . DB_PREFIX . "ms_product_custom_field` pcf
			LEFT JOIN `" . DB_PREFIX . "ms_custom_field` cf
				ON (cf.custom_field_id = pcf.custom_field_id)
			LEFT JOIN `" . DB_PREFIX . "ms_custom_field_description` cfd
				ON (cfd.custom_field_id = cf.custom_field_id)
			LEFT JOIN (SELECT custom_field_value_id, sort_order FROM `" . DB_PREFIX . "ms_custom_field_value`) cfv
				ON (cfv.custom_field_value_id = pcf.custom_field_value_id)
			LEFT JOIN `" . DB_PREFIX . "product` p
				ON (p.product_id = pcf.product_id)
			LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s
				ON (p2s.product_id = p.product_id)
			WHERE p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
				AND cfd.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND pcf.product_id IN (" . implode(',', $product_ids) . ")"

			. ($ms_custom_field_id ? " AND cf.custom_field_id = " . (int)$ms_custom_field_id : "")

			. " ORDER BY cf.sort_order, cfd.`name`, `custom_field_value_sort_order`
		";

		$result = $this->db->query($sql);

		$ms_custom_fields = [];

		foreach ($result->rows as $row) {
			$custom_field_id = (int)$row['custom_field_id'];
			$custom_field_value_id = (int)$row['custom_field_value_id'];
			$value = htmlspecialchars($row['value'], ENT_COMPAT);
			$type = (string)$row['type'];

			if (in_array($type, ['file', 'date', 'time', 'datetime', 'textarea'])) {
				// Skip if custom field's type is inappropriate
				continue;
			}

			if (!isset($ms_custom_fields[$custom_field_id])) {
				$ms_custom_fields[$custom_field_id] = [
					'name' => $row['name'],
					'type' => $type,
					'id_values' => [], // [id => name, id => name, ... ]
					'text_values' => [] // [name, name, ... ]
				];
			}

			if (in_array($type, ['select', 'radio', 'checkbox']) && !isset($ms_custom_fields[$custom_field_id]['id_values'][$custom_field_value_id])) {
				$ms_custom_fields[$custom_field_id]['id_values'][$custom_field_value_id] = htmlspecialchars($row['custom_field_value_name'], ENT_COMPAT);
			} elseif (!in_array($value, $ms_custom_fields[$custom_field_id]['text_values'])) {
				$ms_custom_fields[$custom_field_id]['text_values'][] = $value;
			}
		}

		return $ms_custom_fields;
	}

	public function getMsfAttributes($product_ids, $msf_attribute_id = 0)
	{
		if (empty($product_ids))
			return [];

		$sql = "
			SELECT
				mspavi.msf_attribute_id,
				msfad.`name` as `msf_attribute_name`,
				msfa.`type`,
				msfa.`sort_order` as `msf_attribute_sort_order`,
				mspavi.msf_attribute_value_id,
				msfavd.`name` as `msf_attribute_value_name`,
				msfav.`sort_order` as `msf_attribute_value_sort_order`
			FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` mspavi
			LEFT JOIN (SELECT `id`, `type`, `sort_order` FROM `" . DB_PREFIX . "msf_attribute`) msfa
				ON (msfa.`id` = mspavi.`msf_attribute_id`)
			LEFT JOIN (SELECT `msf_attribute_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") msfad
				ON (msfad.`msf_attribute_id` = mspavi.`msf_attribute_id`)
			LEFT JOIN (SELECT `id`, `sort_order` FROM `" . DB_PREFIX . "msf_attribute_value`) msfav
				ON (msfav.`id` = mspavi.`msf_attribute_value_id`)
			LEFT JOIN (SELECT `msf_attribute_value_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") msfavd
				ON (msfavd.`msf_attribute_value_id` = mspavi.`msf_attribute_value_id`)
			LEFT JOIN (SELECT product_id, status, date_available FROM `" . DB_PREFIX . "product`) p
				ON (p.product_id = mspavi.product_id)
			LEFT JOIN (SELECT product_id, store_id FROM `" . DB_PREFIX . "product_to_store`) p2s
				ON (p2s.product_id = p.product_id)
			WHERE p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
				AND mspavi.product_id IN (" . implode(',', $product_ids) . ")"

			. ($msf_attribute_id ? " AND mspavi.msf_attribute_id = " . (int)$msf_attribute_id : "")

			. " ORDER BY `msf_attribute_sort_order`, `msf_attribute_name`, `msf_attribute_value_sort_order`
		";

		$result = $this->db->query($sql);

		$msf_attributes = [];

		foreach ($result->rows as $row) {
			$msf_attribute_id = (int)$row['msf_attribute_id'];
			$msf_attribute_value_id = (int)$row['msf_attribute_value_id'];
			$value = htmlspecialchars($row['msf_attribute_value_name'], ENT_COMPAT);
			$type = (string)$row['type'];

			if (in_array($type, ['file', 'date', 'time', 'datetime', 'textarea', 'text'])) {
				// Skip if custom field's type is inappropriate
				continue;
			}

			if (!isset($msf_attributes[$msf_attribute_id])) {
				$msf_attributes[$msf_attribute_id] = [
					'name' => $row['msf_attribute_name'],
					'type' => $type,
					'id_values' => [], // [id => name, id => name, ... ]
					'text_values' => [] // [name, name, ... ]
				];
			}

			if (in_array($type, ['select', 'radio', 'checkbox']) && !isset($msf_attributes[$msf_attribute_id]['id_values'][$msf_attribute_value_id])) {
				$msf_attributes[$msf_attribute_id]['id_values'][$msf_attribute_value_id] = htmlspecialchars($row['msf_attribute_value_name'], ENT_COMPAT);
			}
		}

		return $msf_attributes;
	}

	public function getMsfAttributesWithTextValues($product_ids, $msf_attribute_id = 0)
	{
		if (empty($product_ids))
			return [];

		$sql = "
			(
				SELECT
					mspavi.msf_attribute_id,
					msfad.`name` as `msf_attribute_name`,
					msfa.`type`,
					msfa.`sort_order` as `msf_attribute_sort_order`,
					mspavi.msf_attribute_value_id,
					msfavd.`name` as `msf_attribute_value_name`,
					msfav.`sort_order` as `msf_attribute_value_sort_order`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` mspavi
				LEFT JOIN (SELECT `id`, `type`, `sort_order` FROM `" . DB_PREFIX . "msf_attribute`) msfa
					ON (msfa.`id` = mspavi.`msf_attribute_id`)
				LEFT JOIN (SELECT `msf_attribute_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") msfad
					ON (msfad.`msf_attribute_id` = mspavi.`msf_attribute_id`)
				LEFT JOIN (SELECT `id`, `sort_order` FROM `" . DB_PREFIX . "msf_attribute_value`) msfav
					ON (msfav.`id` = mspavi.`msf_attribute_value_id`)
				LEFT JOIN (SELECT `msf_attribute_value_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") msfavd
					ON (msfavd.`msf_attribute_value_id` = mspavi.`msf_attribute_value_id`)
				LEFT JOIN (SELECT product_id, status, date_available FROM `" . DB_PREFIX . "product`) p
					ON (p.product_id = mspavi.product_id)
				LEFT JOIN (SELECT product_id, store_id FROM `" . DB_PREFIX . "product_to_store`) p2s
					ON (p2s.product_id = p.product_id)
				WHERE p.status = '1'
					AND p.date_available <= NOW()
					AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
					AND mspavi.product_id IN (" . implode(',', $product_ids) . ")"

			. ($msf_attribute_id ? " AND mspavi.msf_attribute_id = " . (int)$msf_attribute_id : "")

			. ") UNION (
				SELECT
					mspavt.msf_attribute_id,
					msfad.`name` as `msf_attribute_name`,
					msfa.`type`,
					msfa.`sort_order` as `msf_attribute_sort_order`,
					NULL as `msf_attribute_value_id`,
					mspavt.`value` as `msf_attribute_value_name`,
					NULL as `msf_attribute_value_sort_order`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_text` mspavt
				LEFT JOIN (SELECT `id`, `type`, `sort_order` FROM `" . DB_PREFIX . "msf_attribute`) msfa
					ON (msfa.`id` = mspavt.`msf_attribute_id`)
				LEFT JOIN (SELECT `msf_attribute_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") msfad
					ON (msfad.`msf_attribute_id` = mspavt.`msf_attribute_id`)
				LEFT JOIN (SELECT product_id, status, date_available FROM `" . DB_PREFIX . "product`) p
					ON (p.product_id = mspavt.product_id)
				LEFT JOIN (SELECT product_id, store_id FROM `" . DB_PREFIX . "product_to_store`) p2s
					ON (p2s.product_id = p.product_id)
				WHERE p.status = '1'
					AND p.date_available <= NOW()
					AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
					AND mspavt.language_id = " . (int)$this->config->get('config_language_id') . "
					AND mspavt.product_id IN (" . implode(',', $product_ids) . ")"

			. ($msf_attribute_id ? " AND mspavt.msf_attribute_id = " . (int)$msf_attribute_id : "")

			. ")
			ORDER BY `msf_attribute_sort_order`, `msf_attribute_name`, `msf_attribute_value_sort_order`
		";

		$result = $this->db->query($sql);

		$msf_attributes = [];

		foreach ($result->rows as $row) {
			$msf_attribute_id = (int)$row['msf_attribute_id'];
			$msf_attribute_value_id = (int)$row['msf_attribute_value_id'];
			$value = htmlspecialchars($row['msf_attribute_value_name'], ENT_COMPAT);
			$type = (string)$row['type'];

			if (in_array($type, ['file', 'date', 'time', 'datetime', 'textarea'])) {
				// Skip if custom field's type is inappropriate
				continue;
			}

			if (!isset($msf_attributes[$msf_attribute_id])) {
				$msf_attributes[$msf_attribute_id] = [
					'name' => $row['msf_attribute_name'],
					'type' => $type,
					'id_values' => [], // [id => name, id => name, ... ]
					'text_values' => [] // [name, name, ... ]
				];
			}

			if (in_array($type, ['select', 'radio', 'checkbox']) && !isset($msf_attributes[$msf_attribute_id]['id_values'][$msf_attribute_value_id])) {
				$msf_attributes[$msf_attribute_id]['id_values'][$msf_attribute_value_id] = htmlspecialchars($row['msf_attribute_value_name'], ENT_COMPAT);
			} elseif (!in_array($value, $msf_attributes[$msf_attribute_id]['text_values'])) {
				$msf_attributes[$msf_attribute_id]['text_values'][] = $value;
			}
		}

		return $msf_attributes;
	}



	private function getProductsQuery($data, $count_total = false)
	{
		if ($count_total) {
			$sql = "
				SELECT
					SQL_CALC_FOUND_ROWS
					p.product_id
			";
		} else {
			$sql = "
				SELECT
					SQL_CALC_FOUND_ROWS
					p.product_id,
					(SELECT AVG(rating) AS total
						FROM `" . DB_PREFIX . "review` r1
						WHERE r1.product_id = p.product_id
							AND r1.status = '1'
						GROUP BY r1.product_id
					) AS rating,
					(SELECT price
						FROM `" . DB_PREFIX . "product_discount` pd2
						WHERE pd2.product_id = p.product_id
							AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
							AND pd2.quantity = '1'
							AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW()))
						ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1
					) AS discount,
					(SELECT price
						FROM `" . DB_PREFIX . "product_special` ps
						WHERE ps.product_id = p.product_id
							AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
							AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
						ORDER BY ps.priority ASC, ps.price ASC LIMIT 1
					) AS special
			";
		}

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} elseif (!empty($data['filter_ms_category_id'])) {
			if (!empty($data['filter_ms_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "ms_category_path mcp LEFT JOIN " . DB_PREFIX . "ms_product_to_category mp2c ON (mcp.category_id = mp2c.ms_category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "ms_product_to_category mp2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (mp2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (mp2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}

		if (isset($data['price']) && isset($data['price']['min']) && isset($data['price']['max'])) {
			// Taxes
			$customer_group_id = $this->config->get('config_customer_group_id');
			$shipping_country_id = isset($this->session->data['shipping_country_id']) ? $this->session->data['shipping_country_id'] : (($this->config->get('config_tax_default') == 'shipping') ? $this->config->get('config_country_id') : null);
			$shipping_zone_id = isset($this->session->data['shipping_zone_id']) ? $this->session->data['shipping_zone_id'] : (($this->config->get('config_tax_default') == 'shipping') ? $this->config->get('config_zone_id') : null);
			$payment_country_id = isset($this->session->data['payment_country_id']) ? $this->session->data['payment_country_id'] : (($this->config->get('config_tax_default') == 'payment') ? $this->config->get('config_country_id') : null);
			$payment_zone_id = isset($this->session->data['payment_zone_id']) ? $this->session->data['payment_zone_id'] : (($this->config->get('config_tax_default') == 'payment') ? $this->config->get('config_zone_id') : null);

			$sql .= " LEFT JOIN (SELECT SUM(t.rate) as `fixed_tax`, t.tax_class_id FROM (SELECT DISTINCT tr1.tax_class_id, rate FROM `" . DB_PREFIX . "tax_rule` as tr1 LEFT JOIN `" . DB_PREFIX . "tax_rate` as tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN `" . DB_PREFIX . "tax_rate_to_customer_group` as tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN `" . DB_PREFIX . "zone_to_geo_zone` as z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN `" . DB_PREFIX . "geo_zone` as gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr2.type = 'F' AND tr2cg.customer_group_id = '" . (int)$customer_group_id . "'";

			$conditions = [];

			if ($shipping_country_id || $shipping_zone_id)
				$conditions[] = "tr1.based = 'shipping' AND z2gz.country_id = '" . (int)$shipping_country_id . "' AND z2gz.zone_id IN ('0', '" . (int)$shipping_zone_id . "')";

			if ($payment_country_id || $payment_zone_id)
				$conditions[] = "tr1.based = 'payment' AND z2gz.country_id = '" . (int)$payment_country_id . "' AND z2gz.zone_id IN ('0', '" . (int)$payment_zone_id . "')";

			$conditions[] = "tr1.based = 'store' AND z2gz.country_id = '" . (int)$this->config->get('config_country_id') . "' AND z2gz.zone_id IN ('0', '" . (int)$this->config->get('config_zone_id') . "')";

			if (!empty($conditions))
				$sql .= ' AND ((' . implode(') OR (', $conditions) . '))';

			$sql .= ") as t GROUP BY t.tax_class_id) as tr1 ON (tr1.tax_class_id = p.tax_class_id) LEFT JOIN (SELECT tax_class_id, SUM(rate) as percent_tax FROM (SELECT DISTINCT tr1.tax_class_id, rate FROM `" . DB_PREFIX . "tax_rule` as tr1 LEFT JOIN `" . DB_PREFIX . "tax_rate` as tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN `" . DB_PREFIX . "tax_rate_to_customer_group` as tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN `" . DB_PREFIX . "zone_to_geo_zone` as z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN `" . DB_PREFIX . "geo_zone` as gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr2.type = 'P' AND tr2cg.customer_group_id = '" . (int)$customer_group_id . "'";

			if (!empty($conditions))
				$sql .= ' AND ((' . implode(') OR (', $conditions) . '))';

			$sql .= ") as t GROUP BY t.tax_class_id) as tr2 ON (tr2.tax_class_id = p.tax_class_id)";

			$sql .= "
				LEFT JOIN (SELECT price, product_id FROM " . DB_PREFIX . "product_discount) pdp
					ON (pdp.product_id = p.product_id)
				LEFT JOIN (SELECT price, product_id FROM " . DB_PREFIX . "product_special) psp
					ON (psp.product_id = p.product_id)
			";
		}

		$sql .= "
			LEFT JOIN `" . DB_PREFIX . "product_description` pd
				ON (p.product_id = pd.product_id)
			LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s
				ON (p.product_id = p2s.product_id)
		";

		if (!empty($data['filter_manufacturer'])) {
			$sql .= "
				LEFT JOIN (SELECT manufacturer_id, `name`, `sort_order` FROM `" . DB_PREFIX . "manufacturer`) m
					ON (m.manufacturer_id = p.manufacturer_id)
			";
		}

		if (!empty($data['filter_seller_id'])) {
			$sql .= "
				LEFT JOIN (SELECT product_id, seller_id, product_status FROM `" . DB_PREFIX . "ms_product`) mp
					ON (mp.product_id = p.product_id)
			";
		}

		if (!empty($data['filter_oc_attribute'])) {
			foreach ($data['filter_oc_attribute'] as $attribute_id => $values) {
				$sql .= "
					LEFT JOIN (SELECT * FROM `" . DB_PREFIX . "product_attribute` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "') pa".(int)$attribute_id."
						ON (pa".(int)$attribute_id.".product_id = p.product_id)
				";
			}
		}

		if (!empty($data['filter_ms_custom_field'])) {
			foreach ($data['filter_ms_custom_field'] as $custom_field_id => $values) {
				$sql .= "
					LEFT JOIN (SELECT product_id, custom_field_id, custom_field_value_id, `value` FROM `" . DB_PREFIX . "ms_product_custom_field`) mspcf".(int)$custom_field_id."
						ON (mspcf".(int)$custom_field_id.".product_id = p.product_id)
				";
			}
		}

		if (!empty($data['filter_msf_attribute'])) {
			foreach ($data['filter_msf_attribute'] as $msf_attribute_id => $values) {
				$sql .= "
					LEFT JOIN (SELECT product_id, msf_attribute_id, msf_attribute_value_id FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id`) mspavi".(int)$msf_attribute_id."
						ON (mspavi".(int)$msf_attribute_id.".product_id = p.product_id)
				";
			}
		}

		if (!empty($data['filter_oc_option'])) {
			foreach ($data['filter_oc_option'] as $option_id => $values) {
				$sql .= "
					LEFT JOIN `" . DB_PREFIX . "product_option_value` pov".(int)$option_id."
						ON (pov".(int)$option_id.".product_id = p.product_id)
					LEFT JOIN (SELECT product_option_id, `value` FROM `" . DB_PREFIX . "product_option`) po".(int)$option_id."
						ON (po".(int)$option_id.".product_option_id = pov".(int)$option_id.".product_option_id)
				";
			}
		}

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND p.status = '1'
				AND p.date_available <= NOW()
				AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
		";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		} elseif (!empty($data['filter_ms_category_id'])) {
			if (!empty($data['filter_ms_sub_category'])) {
				$sql .= " AND mcp.path_id = '" . (int)$data['filter_ms_category_id'] . "'";
			} else {
				$sql .= " AND mp2c.ms_category_id = '" . (int)$data['filter_ms_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";

			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}

			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}

			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			$sql .= ")";
		}

		if (!empty($data['filter_ms_custom_field'])) {
			$conditions = [];

			foreach ($data['filter_ms_custom_field'] as $custom_field_id => $values) {
				$sub_sql = " (mspcf".(int)$custom_field_id.".custom_field_id = '" . (int)$custom_field_id . "'";

				// @todo 8.18: use values delimiter
				$values_exploded = explode(',', $values);

				if (!empty($values_exploded)) {
					foreach ($values_exploded as &$value) {
						if (false !== strpos($value, '%d%')) {
							$value = str_replace('%d%', ',', $value);
						}

						$value = "'{$this->db->escape(html_entity_decode(urldecode($value)))}'";
					}

					$sub_sql .= " AND";

					if (in_array((string)$this->MsLoader->MsCustomField->getCustomFieldType($custom_field_id), ['select', 'radio', 'checkbox'])) {
						$sub_sql .= " mspcf".(int)$custom_field_id.".custom_field_value_id IN (" . implode(',', $values_exploded). ")";
					} else {
						$sub_sql .= " mspcf".(int)$custom_field_id.".`value` IN (" . implode(',', $values_exploded). ")";
					}
				}

				$sub_sql .= ")";

				$conditions[] = $sub_sql;
			}

			$sql .= " AND (" . implode(' AND ', $conditions) . ")";
		}

		if (!empty($data['filter_msf_attribute'])) {
			$conditions = [];

			foreach ($data['filter_msf_attribute'] as $msf_attribute_id => $values) {
				$sub_sql = " (mspavi".(int)$msf_attribute_id.".msf_attribute_id = '" . (int)$msf_attribute_id . "'";

				// @todo 8.18: use values delimiter
				$values_exploded = explode(',', $values);

				if (!empty($values_exploded) && in_array((string)$this->MsLoader->MsfAttribute->getType($msf_attribute_id), ['select', 'radio', 'checkbox'])) {
					foreach ($values_exploded as &$value) {
						if (false !== strpos($value, '%d%')) {
							$value = str_replace('%d%', ',', $value);
						}

						$value = "'{$this->db->escape(html_entity_decode(urldecode($value)))}'";
					}

					$sub_sql .= " AND mspavi".(int)$msf_attribute_id.".msf_attribute_value_id IN (" . implode(',', $values_exploded). ")";
				}

				$sub_sql .= ")";

				$conditions[] = $sub_sql;
			}

			$sql .= " AND (" . implode(' AND ', $conditions) . ")";
		}

		if (!empty($data['filter_oc_option'])) {
			$conditions = [];

			foreach ($data['filter_oc_option'] as $option_id => $values) {
				$sub_sql = " (pov".(int)$option_id.".option_id = '" . (int)$option_id . "'";

				// @todo 8.18: use values delimiter
				$values_exploded = explode(',', $values);

				if (!empty($values_exploded)) {
					foreach ($values_exploded as &$value) {
						if (false !== strpos($value, '%d%')) {
							$value = str_replace('%d%', ',', $value);
						}

						$value = "'{$this->db->escape(html_entity_decode(urldecode($value)))}'";
					}

					$sub_sql .= " AND";

					if (in_array((string)$this->MsLoader->MsOption->getOcOptionType($option_id), ['select', 'radio', 'checkbox'])) {
						$sub_sql .= " pov".(int)$option_id.".option_value_id IN (" . implode(',', $values_exploded). ")";
					} else {
						$sub_sql .= " po".(int)$option_id.".`value` IN (" . implode(',', $values_exploded). ")";
					}
				}

				$sub_sql .= ")";

				$conditions[] = $sub_sql;
			}

			$sql .= " AND (" . implode(' AND ', $conditions) . ")";
		}

		if (!empty($data['filter_oc_attribute'])) {
			$conditions = [];

			foreach ($data['filter_oc_attribute'] as $attribute_id => $values) {
				$sub_sql = " (pa".(int)$attribute_id.".attribute_id = '" . (int)$attribute_id . "'";

				// @todo 8.18: use values delimiter
				$values_exploded = explode(',', $values);

				if (!empty($values_exploded)) {
					foreach ($values_exploded as &$value) {
						if (false !== strpos($value, '%d%')) {
							$value = str_replace('%d%', ',', $value);
						}

						$value = "'{$this->db->escape(html_entity_decode(urldecode($value)))}'";
					}

					$sub_sql .= " AND pa".(int)$attribute_id.".`text` IN (" . implode(',', $values_exploded). ")";
				}

				$sub_sql .= ")";

				$conditions[] = $sub_sql;
			}

			$sql .= " AND (" . implode(' AND ', $conditions) . ")";
		}

		if (!empty($data['filter_manufacturer'])) {
			$conditions = [];

			$values_exploded = explode(',', $data['filter_manufacturer']);

			foreach ($values_exploded as $manufacturer_id) {
				$conditions[] = (int)$manufacturer_id;
			}

			$sql .= " AND p.manufacturer_id IN (" . implode(',', $conditions) . ")";
		}

		if (!empty($data['filter_seller_id'])) {
			$sql .= " AND mp.seller_id = " . (int)$data['filter_seller_id'];// . " AND mp.product_status = 1";
		}

		if (isset($data['price']['min'])) {
			$price_min = $this->currency->convert((float)$data['price']['min'], $this->session->data['currency'], $this->config->get('config_currency'));
			$sql .= " AND IF(psp.price IS NOT NULL, psp.price, IF(pdp.price IS NOT NULL, pdp.price, p.price)) * (1 + IFNULL(percent_tax, 0)/100) + IFNULL(fixed_tax, 0) >= '" . $this->db->escape($price_min) . "'";
		}

		if (!empty($data['price']['max'])) {
			$price_max = $this->currency->convert((float)$data['price']['max'], $this->session->data['currency'], $this->config->get('config_currency'));
			$sql .= " AND IF(psp.price IS NOT NULL, psp.price, IF(pdp.price IS NOT NULL, pdp.price, p.price)) * (1 + IFNULL(percent_tax, 0)/100) + IFNULL(fixed_tax, 0) <= '" . $this->db->escape($price_max) . "'";
		}

		$sql .= " GROUP BY p.product_id";

		if (!$count_total) {
			$sort_data = array(
				'pd.name',
				'p.model',
				'p.quantity',
				'p.price',
				'rating',
				'p.sort_order',
				'p.date_added'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
					$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
				} elseif ($data['sort'] == 'p.price') {
					$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
				} else {
					$sql .= " ORDER BY " . $data['sort'];
				}
			} else {
				$sql .= " ORDER BY p.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC, LCASE(pd.name) DESC";
			} else {
				$sql .= " ASC, LCASE(pd.name) ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				$data['start'] = isset($data['start']) && $data['start'] > 0 ? $data['start'] : 0;
				$data['limit'] = isset($data['limit']) && $data['limit'] > 0 ? $data['limit'] : 20;

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
		}

		return $sql;
	}



	public function getDefaultMspfBlocks()
	{
		$mspf_blocks = [];

		$sql = "
			SELECT
				`block_type`,
				`block_type_id`
			FROM `" . DB_PREFIX . "msf_default_mspf_block`
			ORDER BY `sort_order` ASC
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as $row) {
			switch ($row['block_type']) {
				case MsProductFilter::TYPE_OC_ATTRIBUTE:
					$category = $this->language->get('ms_product_filter_block_title_general');
					$label = $this->language->get('ms_product_filter_block_title_attributes');
					break;

				case MsProductFilter::TYPE_OC_OPTION:
					$category = $this->language->get('ms_product_filter_block_title_general');
					$label = $this->language->get('ms_product_filter_block_title_oc_options');
					break;

				case MsProductFilter::TYPE_OC_MANUFACTURER:
					$category = $this->language->get('ms_product_filter_block_title_general');
					$label = $this->language->get('ms_product_filter_block_title_oc_manufacturers');
					break;

				case MsProductFilter::TYPE_PRICE:
					$category = $this->language->get('ms_product_filter_block_title_general');
					$label = $this->language->get('ms_product_filter_block_title_price');
					break;

				case MsProductFilter::TYPE_CATEGORY:
					$category = $this->language->get('ms_product_filter_block_title_general');
					$label = $this->language->get('ms_product_filter_block_title_category');
					break;

				case MsProductFilter::TYPE_MSF_ATTRIBUTE:
				default:
					$category = $this->language->get('ms_product_filter_block_title_attributes');
					$msf_attribute = $this->MsLoader->MsfAttribute->get($row['block_type_id']);
					$label = $msf_attribute['full_name'];
					break;
			}

			$mspf_blocks[] = [
				'value' => $row['block_type'] . '-' . $row['block_type_id'],
				'category' => $category,
				'label' => $label
			];
		}

		return $mspf_blocks;
	}

	public function saveDefaultBlocks($data = [])
	{
		$this->db->query("TRUNCATE `" . DB_PREFIX . "msf_default_mspf_block`");

		foreach ($data as $sort_order => $id_by_type) {
			list($block_type, $block_type_id) = explode('-', $id_by_type);

			$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_default_mspf_block`
				SET `block_type` = " . (int)$block_type . ",
					`block_type_id` = " . (int)$block_type_id . ",
					`sort_order` = " . (int)$sort_order
			;

			$this->db->query($sql);
		}

		return true;
	}

	public function getOcCategoryMspfBlocks($oc_category_id, $retrieve_inherited_category = false)
	{
		$mspf_blocks = [];

		$sql = "
			SELECT
				c.`category_id`,
				c.`parent_id`,
				GROUP_CONCAT(CONCAT(cpfb.block_type, '-', cpfb.block_type_id) ORDER BY cpfb.sort_order SEPARATOR ',') as `mspf_block_values`
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN `" . DB_PREFIX . "ms_oc_category_mspf_block` cpfb
				ON (cpfb.oc_category_id = c.category_id)
			WHERE c.`category_id` = " . (int)$oc_category_id . "
			GROUP BY cpfb.oc_category_id
		";

		$oc_category = $this->db->query($sql)->row;

		if (!empty($oc_category['mspf_block_values'])) {
			foreach (explode(',', $oc_category['mspf_block_values']) as $mspf_block) {
				list($block_type, $block_type_id) = explode('-', $mspf_block);

				switch ($block_type) {
					case MsProductFilter::TYPE_OC_ATTRIBUTE:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_attributes');
						break;

					case MsProductFilter::TYPE_OC_OPTION:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_oc_options');
						break;

					case MsProductFilter::TYPE_OC_MANUFACTURER:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_oc_manufacturers');
						break;

					case MsProductFilter::TYPE_PRICE:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_price');
						break;

					case MsProductFilter::TYPE_CATEGORY:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_category');
						break;

					case MsProductFilter::TYPE_MSF_ATTRIBUTE:
					default:
						$category = $this->language->get('ms_product_filter_block_title_attributes');
						$msf_attribute = $this->MsLoader->MsfAttribute->get($block_type_id);
						$label = $msf_attribute['full_name'];
						break;
				}

				$mspf_blocks[] = [
					'value' => $block_type . '-' . $block_type_id,
					'category' => $category,
					'label' => $label
				];
			}

			$inherited_category_id = $oc_category['category_id'];
		} elseif (!empty($oc_category['parent_id'])) {
			$mspf_blocks = $this->getOcCategoryMspfBlocks($oc_category['parent_id']);
			$inherited_category_id = $oc_category['parent_id'];
		} else {
			$mspf_blocks = [];
			$inherited_category_id = 0;
		}

		return $retrieve_inherited_category ? [$mspf_blocks, $inherited_category_id] : $mspf_blocks;
	}

	public function saveOcCategoryMspfBlocks($oc_category_id, $data = [])
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_oc_category_mspf_block` WHERE `oc_category_id` = " . (int)$oc_category_id);

		foreach ($data as $sort_order => $id_by_type) {
			list($block_type, $block_type_id) = explode('-', $id_by_type);

			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_oc_category_mspf_block`
				SET `oc_category_id` = " . (int)$oc_category_id . ",
					`block_type` = " . (int)$block_type . ",
					`block_type_id` = " . (int)$block_type_id . ",
					`sort_order` = " . (int)$sort_order
			;

			$this->db->query($sql);
		}

		return true;
	}

	public function getMsCategoryMspfBlocks($ms_category_id, $retrieve_inherited_category = false)
	{
		$mspf_blocks = [];

		$sql = "
			SELECT
				c.`category_id`,
				c.`parent_id`,
				GROUP_CONCAT(CONCAT(cpfb.block_type, '-', cpfb.block_type_id) ORDER BY cpfb.sort_order SEPARATOR ',') as `mspf_block_values`
			FROM `" . DB_PREFIX . "ms_category` c
			LEFT JOIN `" . DB_PREFIX . "ms_category_mspf_block` cpfb
				ON (cpfb.ms_category_id = c.category_id)
			WHERE c.`category_id` = " . (int)$ms_category_id . "
			GROUP BY cpfb.ms_category_id
		";

		$ms_category = $this->db->query($sql)->row;

		if (!empty($ms_category['mspf_block_values'])) {
			foreach (explode(',', $ms_category['mspf_block_values']) as $mspf_block) {
				list($block_type, $block_type_id) = explode('-', $mspf_block);

				switch ($block_type) {
					case MsProductFilter::TYPE_OC_ATTRIBUTE:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_attributes');
						break;

					case MsProductFilter::TYPE_OC_OPTION:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_oc_options');
						break;

					case MsProductFilter::TYPE_OC_MANUFACTURER:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_oc_manufacturers');
						break;

					case MsProductFilter::TYPE_PRICE:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_price');
						break;

					case MsProductFilter::TYPE_CATEGORY:
						$category = $this->language->get('ms_product_filter_block_title_general');
						$label = $this->language->get('ms_product_filter_block_title_category');
						break;

					case MsProductFilter::TYPE_MSF_ATTRIBUTE:
					default:
						$category = $this->language->get('ms_product_filter_block_title_attributes');
						$msf_attribute = $this->MsLoader->MsfAttribute->get($block_type_id);
						$label = $msf_attribute['full_name'];
						break;
				}

				$mspf_blocks[] = [
					'value' => $block_type . '-' . $block_type_id,
					'category' => $category,
					'label' => $label
				];
			}

			$inherited_category_id = $ms_category['category_id'];
		} elseif (!empty($ms_category['parent_id'])) {
			$mspf_blocks = $this->getMsCategoryMspfBlocks($ms_category['parent_id']);
			$inherited_category_id = $ms_category['parent_id'];
		} else {
			$mspf_blocks = [];
			$inherited_category_id = 0;
		}

		return $retrieve_inherited_category ? [$mspf_blocks, $inherited_category_id] : $mspf_blocks;
	}

	public function saveMsCategoryMspfBlocks($ms_category_id, $data = [])
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_category_mspf_block` WHERE `ms_category_id` = " . (int)$ms_category_id);

		foreach ($data as $sort_order => $block_data) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_category_mspf_block`
				SET `ms_category_id` = " . (int)$ms_category_id . ",
					`block_type` = " . (int)$block_data['block_type'] . ",
					`block_type_id` = " . (int)$block_data['block_type_id'] . ",
					`sort_order` = " . (int)$sort_order
			;

			$this->db->query($sql);
		}

		return true;
	}
}
