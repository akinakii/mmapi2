<?php

class MsfAttribute extends Model
{
	const TYPE_SELECT = 'select';
	const TYPE_CHECKBOX = 'checkbox';
	const TYPE_TEXT = 'text';

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;

	private $types = [
		'plain' => [self::TYPE_TEXT],
		'values_required' => [self::TYPE_SELECT, self::TYPE_CHECKBOX]
	];

	public function getList($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msfa.*,
				msfad.`name`,
				msfad.`label`,
				IF(msfad.`label` <> '', CONCAT(msfad.`name`, ' / ', msfad.`label`), msfad.`name`) as `full_name`,
				msfad.`note`
			FROM `" . DB_PREFIX . "msf_attribute` msfa
			LEFT JOIN (SELECT msf_attribute_id, `name`, `label`, `note` FROM `" . DB_PREFIX . "msf_attribute_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfad
				ON (msfad.msf_attribute_id = msfa.id)
			WHERE 1 = 1"

			. (isset($data['default']) ? " AND `default` = 1" : "")
			. (!empty($data['not_in_ids']) ? " AND msfa.`id` NOT IN (" . implode(',', $data['not_in_ids']) . ")" : "")

			. $filters
			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total_rows");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total_rows'];

			if (isset($data['values'])) {
				foreach ($result->rows as &$row) {
					$row['values'] = $this->getValues($row['id']);
				}
			}
		}

		return $result->rows;
	}

	public function get($msf_attribute_id, $data = [])
	{
		$sql = "
			SELECT
				msfa.*,
				msfad.`name`,
				msfad.`label`,
				IF(msfad.`label` <> '', CONCAT(msfad.`name`, ' / ', msfad.`label`), msfad.`name`) as `full_name`
			FROM `" . DB_PREFIX . "msf_attribute` msfa
			LEFT JOIN (SELECT msf_attribute_id, `name`, `label` FROM `" . DB_PREFIX . "msf_attribute_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfad
				ON (msfad.msf_attribute_id = msfa.id)
			WHERE `id` = " . (int)$msf_attribute_id . "
			LIMIT 1
		";

		$result = $this->db->query($sql);

		$sql_2 = "
			SELECT
				language_id,
				`name`,
				`label`,
				`note`
			FROM `" . DB_PREFIX ."msf_attribute_description`
			WHERE msf_attribute_id = " . (int)$msf_attribute_id . "
			GROUP BY language_id
		";

		$result_2 = $this->db->query($sql_2);

		$descriptions = [];
		foreach ($result_2->rows as $row) {
			$descriptions[$row['language_id']] = [
				'name' => $row['name'],
				'label' => $row['label'],
				'note' => $row['note']
			];
		}

		$result->row['description'] = $descriptions;
		$result->row['values'] = $this->getValues($msf_attribute_id);

		return $result->row;
	}

	public function getName($msf_attribute_id, $language_id = null)
	{
		$language_id = $language_id ?: $this->config->get('config_language_id');

		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_attribute_description`
			WHERE msf_attribute_id = " . (int)$msf_attribute_id . "
				AND `language_id` = " . (int)$language_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function getType($msf_attribute_id)
	{
		$sql = "
			SELECT
				`type`
			FROM `" . DB_PREFIX . "msf_attribute`
			WHERE `id` = " . (int)$msf_attribute_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['type'] : '';
	}

	public function getValues($msf_attribute_id)
	{
		$sql = "
			SELECT
				msfav.`id`,
				msfavd.`name`,
				msfav.`sort_order`
			FROM `" . DB_PREFIX . "msf_attribute_value` msfav
			LEFT JOIN (SELECT msf_attribute_value_id, `name` FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfavd
				ON (msfavd.msf_attribute_value_id = msfav.id)
			WHERE msfav.`msf_attribute_id` = " . (int)$msf_attribute_id . "
			ORDER BY msfav.`sort_order`
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$sql_2 = "
				SELECT
					language_id,
					`name`
				FROM `" . DB_PREFIX ."msf_attribute_value_description`
				WHERE msf_attribute_value_id = " . (int)$row['id'] . "
				GROUP BY language_id
			";

			$result_2 = $this->db->query($sql_2);

			$descriptions = [];
			foreach ($result_2->rows as $r) {
				$descriptions[$r['language_id']] = ['name' => $r['name']];
			}

			$row['description'] = $descriptions;
		}

		return $result->rows;
	}

	public function getValueName($msf_attribute_value_id)
	{
		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_attribute_value_description`
			WHERE `msf_attribute_value_id` = " . (int)$msf_attribute_value_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function save($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "msf_attribute`
			SET `type` = '" . $this->db->escape($data['type']) . "'"
				. (isset($data['required']) ? ", `required` = " . (int)$data['required'] : "")
				. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
				. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
				. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
		;

		$this->db->query($sql);

		$msf_attribute_id = $this->db->getLastId();

		// Attribute description
		foreach ($data['description'] as $language_id => $v) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_attribute_description`
				SET `msf_attribute_id` = " . (int)$msf_attribute_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
				. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
				. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
			;

			$this->db->query($sql);
		}

		// Attribute values
		if (!empty($data['values'])) {
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_attribute_value`
					SET `msf_attribute_id` = " . (int)$msf_attribute_id . ",
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_attribute_value_id = $this->db->getLastId();

				// Attribute values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_attribute_value_description`
						SET `msf_attribute_value_id` = " . (int)$msf_attribute_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}
	}

	public function update($msf_attribute_id, $data = [])
	{
		// Needed to understand whether the type and/or values were changed
		$old_msf_attribute = $this->get($msf_attribute_id);
		$old_values_ids = array_column($old_msf_attribute['values'], 'id');

		// Update existing attribute
		$sql = "
			UPDATE `" . DB_PREFIX . "msf_attribute`
			SET `id` = `id`"

			. (isset($data['type']) ? ", `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['required']) ? ", `required` = " . (int)$data['required'] : "")
			. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
			. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
			. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
			
			. " WHERE `id` = " . (int)$msf_attribute_id;

		$this->db->query($sql);

		// Attribute description
		if (!empty($data['description'])) {
			// Cleanup attribute description
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_description` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id);

			foreach ($data['description'] as $language_id => $v) {
				$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_attribute_description`
				SET `msf_attribute_id` = " . (int)$msf_attribute_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
					. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
					. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
				;

				$this->db->query($sql);
			}
		}

		// Attribute values
		if (isset($data['type']) && !in_array($data['type'], $this->types['values_required'])) {
			// Cleanup if new attribute type does not require any values (`text` etc.)
			$this->cleanupAttributeValues($msf_attribute_id, $old_values_ids);
		} elseif (!empty($data['values'])) {
			// Remove relation to products for values that do not exist anymore
			$new_values_ids = array_column($data['values'], 'id');
			$this->cleanupAttributeValues($msf_attribute_id, array_diff($old_values_ids, $new_values_ids));

			// Save new values
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_attribute_value`
					SET `msf_attribute_id` = " . (int)$msf_attribute_id . ",
						`sort_order` = " . (int)$sort_order
						. (!empty($value['id']) ? ", `id` = " . $value['id'] : "") . "
					ON DUPLICATE KEY UPDATE
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_attribute_value_id = !empty($value['id']) ? $value['id'] : $this->db->getLastId();

				// Attribute values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_attribute_value_description`
						SET `msf_attribute_value_id` = " . (int)$msf_attribute_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
						ON DUPLICATE KEY UPDATE
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}

		return true;
	}

	public function delete($msf_attribute_id)
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute` WHERE `id` = " . (int)$msf_attribute_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_description` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id);

		$values = $this->db->query("SELECT `id` FROM `" . DB_PREFIX . "msf_attribute_value` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id)->rows;
		foreach ($values as $value) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_value` WHERE `id` = " . (int)$value['id']);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE `msf_attribute_value_id` = " . (int)$value['id']);
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_oc_category_msf_attribute` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_text` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id);
	}

	public function getProductMsfAttributes($product_id, $data = [])
	{
		$msf_attributes = [];

		$sql = "
			(
				SELECT
					pavi.product_id,
					pavi.msf_attribute_id,
					IFNULL(msca.sort_order, 0) as `sort_order`,
					GROUP_CONCAT(pavi.`msf_attribute_value_id` SEPARATOR '~') as `value_ids`,
					GROUP_CONCAT(avd.`name` SEPARATOR '~') as `value_names`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` pavi
				LEFT JOIN (SELECT `msf_attribute_value_id`, `name` FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE `language_id` = " . (isset($data['language_id']) ? (int)$data['language_id'] : (int)$this->config->get('config_language_id')) . ") avd
					ON (avd.`msf_attribute_value_id` = pavi.`msf_attribute_value_id`)
				LEFT JOIN (SELECT product_id, primary_oc_category FROM `" . DB_PREFIX . "ms_product`) msp
					ON (msp.product_id = pavi.product_id)
				LEFT JOIN `" . DB_PREFIX . "ms_oc_category_msf_attribute` msca
					ON (msca.`oc_category_id` = msp.`primary_oc_category` AND msca.msf_attribute_id = pavi.msf_attribute_id)
				WHERE pavi.`product_id` = " . (int)$product_id . "
				GROUP BY pavi.product_id, pavi.msf_attribute_id, msca.sort_order
			) UNION (
				SELECT
					pavi.product_id,
					pavi.msf_attribute_id,
					IFNULL(msca.sort_order, 0) as `sort_order`,
					'' as `value_ids`,
					GROUP_CONCAT(pavi.`language_id`, '†‡', pavi.`value` SEPARATOR '~') as `value_names`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_text` pavi
				LEFT JOIN (SELECT product_id, primary_oc_category FROM `" . DB_PREFIX . "ms_product`) msp
					ON (msp.product_id = pavi.product_id)
				LEFT JOIN `" . DB_PREFIX . "ms_oc_category_msf_attribute` msca
					ON (msca.`oc_category_id` = msp.`primary_oc_category` AND msca.msf_attribute_id = pavi.msf_attribute_id)
				WHERE pavi.`product_id` = " . (int)$product_id
					. (isset($data['language_id']) ? " AND pavi.`language_id` = " . (int)$data['language_id'] : "") . "
				GROUP BY pavi.product_id, pavi.msf_attribute_id, msca.sort_order
			)
			ORDER BY sort_order
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as $row) {
			$value_names = explode('~', $row['value_names']);

			if (!empty($row['value_ids'])) {
				// Id values
				$value_ids = explode('~', $row['value_ids']);

				$msf_attributes[$row['msf_attribute_id']] = [
					'id_values' => count($value_ids) === count($value_names) ? array_combine($value_ids, $value_names) : $value_names,
					'text_values' => []
				];
			} else {
				// Text values
				$text_values = [];

				foreach ($value_names as $value_name) {
					list($language_id, $name) = explode('†‡', $value_name);

					if ($name) {
						$text_values[$language_id] = $name;
					}
				}

				if (!empty($text_values)) {
					$msf_attributes[$row['msf_attribute_id']] = [
						'id_values' => [],
						'text_values' => $text_values
					];
				}
			}
		}

		return $msf_attributes;
	}

	public function getProductsByMsfAttributeId($msf_attribute_id)
	{
		$products_ids = [];

		$products = $this->db->query("
			(
				SELECT
					`product_id`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id`
				WHERE msf_attribute_id = '" . (int)$msf_attribute_id . "'
			) UNION (
				SELECT DISTINCT
					`product_id`
				FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_text`
				WHERE msf_attribute_id = '" . (int)$msf_attribute_id . "'
					AND `language_id` = " . (int)$this->config->get('config_language_id') . "
			)
		");

		foreach ($products->rows as $row) {
			$products_ids[] = $row['product_id'];
		}

		return array_unique($products_ids);
	}

	public function getOcCategoryMsfAttributes($oc_category_id, $retrieve_inherited_category = false)
	{
		$sql = "
			SELECT
				c.`category_id`,
				c.`parent_id`,
				GROUP_CONCAT(msca.msf_attribute_id ORDER BY msca.sort_order SEPARATOR ',') as `msf_attribute_ids`
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN `" . DB_PREFIX . "ms_oc_category_msf_attribute` msca
				ON (msca.oc_category_id = c.category_id)
			WHERE c.`category_id` = " . (int)$oc_category_id . "
			GROUP BY msca.oc_category_id
		";

		$oc_category = $this->db->query($sql)->row;

		if (!empty($oc_category['msf_attribute_ids'])) {
			$msf_attribute_ids = explode(',', $oc_category['msf_attribute_ids']);
			$inherited_category_id = $oc_category['category_id'];
		} elseif (!empty($oc_category['parent_id'])) {
			$msf_attribute_ids = $this->getOcCategoryMsfAttributes($oc_category['parent_id']);
			$inherited_category_id = $oc_category['parent_id'];
		} else {
			$msf_attribute_ids = [];
			$inherited_category_id = 0;
		}

		return $retrieve_inherited_category ? [$msf_attribute_ids, $inherited_category_id] : $msf_attribute_ids;
	}

	public function getOcCategoriesByMsfAttributeId($msf_attribute_id)
	{
		$oc_category_ids = [];

		$oc_categories = $this->db->query("
			SELECT DISTINCT
				`oc_category_id`
			FROM `" . DB_PREFIX . "ms_oc_category_msf_attribute`
			WHERE msf_attribute_id = '" . (int)$msf_attribute_id . "'
		");

		foreach ($oc_categories->rows as $row) {
			$oc_category_ids[] = $row['oc_category_id'];
		}

		return $oc_category_ids;
	}

	private function cleanupAttributeValues($msf_attribute_id, $msf_attribute_values_ids)
	{
		foreach ($msf_attribute_values_ids as $value_id) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_value` WHERE `id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_attribute_value_description` WHERE `msf_attribute_value_id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` WHERE `msf_attribute_id` = " . (int)$msf_attribute_id . " AND `msf_attribute_value_id` = " . (int)$value_id);
		}
	}
}
