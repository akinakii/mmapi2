<?php

class MsFavoriteSeller extends Model
{
	private $customer_id;

	public function __construct($registry)
	{
		parent::__construct($registry);

		$this->customer_id = $this->customer->getId();
	}

	public function getFollowedSellers($seller_id = null, $ids_only = false)
	{
		$sellers = [];

		$sql = "
			SELECT
				seller_id,
				date_added
			FROM `" . DB_PREFIX . "ms_favorite_seller`
			WHERE `customer_id` = " . (int)$this->customer_id
			. (!empty($seller_id) ? "AND `seller_id` = " . (int)$seller_id : "")
		;

		$result = $this->db->query($sql);

		if ($ids_only) {
			return array_column($result->rows, 'seller_id');
		}

		foreach ($result->rows as $row) {
			$seller = $this->MsLoader->MsSeller->getSeller($row['seller_id']);
			$seller['date_followed'] = $row['date_added'];
			$sellers[] = $seller;
		}

		return $sellers;
	}

	public function followSeller($seller_id)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_favorite_seller`
			SET `customer_id` = " . (int)$this->customer_id . ",
				`seller_id` = " . (int)$seller_id
		;

		$this->db->query($sql);

		return true;
	}

	public function unfollowSeller($seller_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_favorite_seller`
			WHERE `customer_id` = " . (int)$this->customer_id . "
				AND `seller_id` = " . (int)$seller_id
		;

		$this->db->query($sql);

		return true;
	}

	public function getTotalFollowers($seller_id)
	{
		$sql = "
			SELECT
				COUNT(1) as `total`
			FROM `" . DB_PREFIX . "ms_favorite_seller`
			WHERE `seller_id` = " . (int)$seller_id
		;

		$result = $this->db->query($sql);

		return !empty($result->row['total']) ? $result->row['total'] : 0;
	}

	public function isCustomerFollowing($seller_id)
	{
		$sql = "
			SELECT 1
			FROM `" . DB_PREFIX . "ms_favorite_seller`
			WHERE `customer_id` = " . (int)$this->customer_id . "
				AND `seller_id` = " . (int)$seller_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows;
	}
}
