<?php

use MultiMerch\Core\Invoice\InvoiceItemTotal as InvoiceItemTotal;
use MultiMerch\Core\Invoice\Factories\InvoiceItemTotalFactory as InvoiceItemTotalFactory;

class MsInvoiceItemTotal extends Model
{
	protected $invoice_item_total_factory;

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->invoice_item_total_factory = new InvoiceItemTotalFactory($registry);
	}

	/**
	 * Retrieves totals of specified invoice item from the database.
	 *
	 * @param	array	$data	Parameters.
	 * @return	array			Array of invoice item totals.
	 */
	public function get($data = [])
	{
		$invoice_item_totals = [];

		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_invoice_item_total`
			WHERE 1 = 1"
			. (isset($data['invoice_item_id']) ? " AND `invoice_item_id` = " . (int)$data['invoice_item_id'] : "")
			. (isset($data['type']) ? " AND `type` = '" . $this->db->escape($data['type']) . "'" : "")
		;

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$row['titles'] = $this->getTitles($row['invoice_item_total_id']);
				$invoice_totals[] = $this->invoice_item_total_factory->create($row['type'], $row);
			}
		}

		return $invoice_item_totals;
	}

	/**
	 * Stores invoice item instance in the database.
	 *
	 * @param	InvoiceItemTotal	$invoice_item_total		Invoice item total instance.
	 * @return	int											Invoice item total id.
	 */
	public function create(InvoiceItemTotal $invoice_item_total)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_invoice_item_total`
			SET `invoice_item_id` = " . (int)$invoice_item_total->getInvoiceItemId() . ",
				`type` = '" . $this->db->escape($invoice_item_total->getType()) . "',
				`value` = " . (float)$invoice_item_total->getValue() . "
		";

		$this->db->query($sql);

		$invoice_item_total_id = $this->db->getLastId();

		// Set titles
		$this->saveTitles($invoice_item_total_id, $invoice_item_total->getTitles());

		return $invoice_item_total_id;
	}

	/**
	 * Updates invoice item total in the database.
	 *
	 * @param	int		$invoice_total_item_id		Invoice total item id.
	 * @param	array	$data						Parameters.
	 */
	public function update($invoice_total_item_id, $data = [])
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_invoice_item_total`
			SET `invoice_total_item_id` = `invoice_total_item_id`"
			. (isset($data['invoice_item_id']) ? ", `invoice_item_id` = " . (int)$data['invoice_item_id'] : "")
			. (isset($data['type']) ? ", `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['value']) ? ", `value` = " . (float)$data['value'] : "")
			. " WHERE `invoice_total_item_id` = " . $invoice_total_item_id
		;

		$this->db->query($sql);

		// Set titles
		if (isset($data['titles']))
			$this->saveTitles($invoice_total_item_id, $data['titles']);
	}

	/**
	 * Deletes invoice item total from the database.
	 *
	 * @param	int		$invoice_item_total_id		Invoice item total id.
	 */
	public function delete($invoice_item_total_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_invoice_item_total`
			WHERE `invoice_item_total_id` = " . (int)$invoice_item_total_id
		;

		$this->db->query($sql);
	}

	/**
	 * Gets invoice item total titles translated in different languages.
	 *
	 * @param	int		$invoice_item_total_id	Invoice item total id.
	 * @return	array							Invoice item total titles.
	 */
	private function getTitles($invoice_item_total_id)
	{
		$titles = [];

		$sql = "
			SELECT
				language_id,
				`title`
			FROM `" . DB_PREFIX . "ms_invoice_item_total_description`
			WHERE `invoice_item_total_id` = " . (int)$invoice_item_total_id . "
		";

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$titles[$row['language_id']] = $row['title'];
			}
		}

		return $titles;
	}

	/**
	 * Creates or updates titles of the invoice item total.
	 *
	 * @param	int		$invoice_item_total_id	Invoice item total id.
	 * @param	array	$titles					Array of titles. The structure is: [language_id => title, language_id => title ... ]
	 */
	private function saveTitles($invoice_item_total_id, $titles)
	{
		if (empty($titles))
			return;

		foreach ($titles as $language_id => $title) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_invoice_item_total_description`
				SET `invoice_item_total_id` = " . (int)$invoice_item_total_id . ",
					`language_id` = " . (int)$language_id . ",
					`title` = '" . $this->db->escape($title) . "'
				ON DUPLICATE KEY UPDATE
					`title` = '" . $this->db->escape($title) . "'
			";

			$this->db->query($sql);
		}
	}
}
