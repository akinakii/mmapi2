<?php

class MsfSellerProperty extends Model
{
	const TYPE_SELECT = 'select';
	const TYPE_CHECKBOX = 'checkbox';
	const TYPE_TEXT = 'text';

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;

	private $types = [
		'plain' => [self::TYPE_TEXT],
		'values_required' => [self::TYPE_SELECT, self::TYPE_CHECKBOX]
	];

	public function getList($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msfsp.*,
				msfspd.`name`,
				msfspd.`label`,
				IF(msfspd.`label` <> '', CONCAT(msfspd.`name`, ' / ', msfspd.`label`), msfspd.`name`) as `full_name`,
				msfspd.`note`
			FROM `" . DB_PREFIX . "msf_seller_property` msfsp
			LEFT JOIN (SELECT msf_seller_property_id, `name`, `label`, `note` FROM `" . DB_PREFIX . "msf_seller_property_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfspd
				ON (msfspd.msf_seller_property_id = msfsp.id)
			WHERE 1 = 1"

			. (isset($data['default']) ? " AND `default` = 1" : "")
			. (!empty($data['not_in_ids']) ? " AND msfsp.`id` NOT IN (" . implode(',', $data['not_in_ids']) . ")" : "")

			. $filters
			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total_rows");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total_rows'];

			if (isset($data['values'])) {
				foreach ($result->rows as &$row) {
					$row['values'] = $this->getValues($row['id']);
				}
			}
		}

		return $result->rows;
	}

	public function get($msf_seller_property_id, $data = [])
	{
		$sql = "
			SELECT
				msfsp.*,
				msfspd.`name`,
				msfspd.`label`,
				IF(msfspd.`label` <> '', CONCAT(msfspd.`name`, ' / ', msfspd.`label`), msfspd.`name`) as `full_name`
			FROM `" . DB_PREFIX . "msf_seller_property` msfsp
			LEFT JOIN (SELECT msf_seller_property_id, `name`, `label` FROM `" . DB_PREFIX . "msf_seller_property_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfspd
				ON (msfspd.msf_seller_property_id = msfsp.id)
			WHERE `id` = " . (int)$msf_seller_property_id . "
			LIMIT 1
		";

		$result = $this->db->query($sql);

		$sql_2 = "
			SELECT
				language_id,
				`name`,
				`label`,
				`note`
			FROM `" . DB_PREFIX ."msf_seller_property_description`
			WHERE msf_seller_property_id = " . (int)$msf_seller_property_id . "
			GROUP BY language_id
		";

		$result_2 = $this->db->query($sql_2);

		$descriptions = [];
		foreach ($result_2->rows as $row) {
			$descriptions[$row['language_id']] = [
				'name' => $row['name'],
				'label' => $row['label'],
				'note' => $row['note']
			];
		}

		$result->row['description'] = $descriptions;
		$result->row['values'] = $this->getValues($msf_seller_property_id);

		return $result->row;
	}

	public function getName($msf_seller_property_id, $language_id)
	{
		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_seller_property_description`
			WHERE msf_seller_property_id = " . (int)$msf_seller_property_id . "
				AND `language_id` = " . (int)$language_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function getType($msf_seller_property_id)
	{
		$sql = "
			SELECT
				`type`
			FROM `" . DB_PREFIX . "msf_seller_property`
			WHERE `id` = " . (int)$msf_seller_property_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['type'] : '';
	}

	public function getValues($msf_seller_property_id)
	{
		$sql = "
			SELECT
				msfspv.`id`,
				msfspvd.`name`,
				msfspv.`sort_order`
			FROM `" . DB_PREFIX . "msf_seller_property_value` msfspv
			LEFT JOIN (SELECT msf_seller_property_value_id, `name` FROM `" . DB_PREFIX . "msf_seller_property_value_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfspvd
				ON (msfspvd.msf_seller_property_value_id = msfspv.id)
			WHERE msfspv.`msf_seller_property_id` = " . (int)$msf_seller_property_id . "
			ORDER BY msfspv.`sort_order`
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$sql_2 = "
				SELECT
					language_id,
					`name`
				FROM `" . DB_PREFIX ."msf_seller_property_value_description`
				WHERE msf_seller_property_value_id = " . (int)$row['id'] . "
				GROUP BY language_id
			";

			$result_2 = $this->db->query($sql_2);

			$descriptions = [];
			foreach ($result_2->rows as $r) {
				$descriptions[$r['language_id']] = ['name' => $r['name']];
			}

			$row['description'] = $descriptions;
		}

		return $result->rows;
	}

	public function getValueName($msf_seller_property_value_id)
	{
		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_seller_property_value_description`
			WHERE `msf_seller_property_value_id` = " . (int)$msf_seller_property_value_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function save($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "msf_seller_property`
			SET `type` = '" . $this->db->escape($data['type']) . "'"
				. (isset($data['required']) ? ", `required` = " . (int)$data['required'] : "")
				. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
				. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
				. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
		;

		$this->db->query($sql);

		$msf_seller_property_id = $this->db->getLastId();

		// Attribute description
		foreach ($data['description'] as $language_id => $v) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_seller_property_description`
				SET `msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
				. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
				. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
			;

			$this->db->query($sql);
		}

		// Attribute values
		if (!empty($data['values'])) {
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_seller_property_value`
					SET `msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_seller_property_value_id = $this->db->getLastId();

				// Attribute values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_seller_property_value_description`
						SET `msf_seller_property_value_id` = " . (int)$msf_seller_property_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}
	}

	public function update($msf_seller_property_id, $data = [])
	{
		// Needed to understand whether the type and/or values were changed
		$old_msf_seller_property = $this->get($msf_seller_property_id);
		$old_values_ids = array_column($old_msf_seller_property['values'], 'id');

		// Update existing attribute
		$sql = "
			UPDATE `" . DB_PREFIX . "msf_seller_property`
			SET `id` = `id`"

			. (isset($data['type']) ? ", `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['required']) ? ", `required` = " . (int)$data['required'] : "")
			. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
			. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
			. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
			
			. " WHERE `id` = " . (int)$msf_seller_property_id;

		$this->db->query($sql);

		// Attribute description
		if (!empty($data['description'])) {
			// Cleanup attribute description
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_description` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id);

			foreach ($data['description'] as $language_id => $v) {
				$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_seller_property_description`
				SET `msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
					. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
					. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
				;

				$this->db->query($sql);
			}
		}

		// Attribute values
		if (isset($data['type']) && !in_array($data['type'], $this->types['values_required'])) {
			// Cleanup if new attribute type does not require any values (`text` etc.)
			$this->cleanupAttributeValues($msf_seller_property_id, $old_values_ids);
		} elseif (!empty($data['values'])) {
			// Remove relation to sellers for values that do not exist anymore
			$new_values_ids = array_column($data['values'], 'id');
			$this->cleanupAttributeValues($msf_seller_property_id, array_diff($old_values_ids, $new_values_ids));

			// Save new values
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_seller_property_value`
					SET `msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
						`sort_order` = " . (int)$sort_order
						. (!empty($value['id']) ? ", `id` = " . $value['id'] : "") . "
					ON DUPLICATE KEY UPDATE
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_seller_property_value_id = !empty($value['id']) ? $value['id'] : $this->db->getLastId();

				// Attribute values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_seller_property_value_description`
						SET `msf_seller_property_value_id` = " . (int)$msf_seller_property_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
						ON DUPLICATE KEY UPDATE
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}

		return true;
	}

	public function delete($msf_seller_property_id)
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property` WHERE `id` = " . (int)$msf_seller_property_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_description` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id);

		$values = $this->db->query("SELECT `id` FROM `" . DB_PREFIX . "msf_seller_property_value` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id)->rows;
		foreach ($values as $value) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_value` WHERE `id` = " . (int)$value['id']);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_value_description` WHERE `msf_seller_property_value_id` = " . (int)$value['id']);
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_group_msf_seller_property` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_text` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id);
	}

	public function getSellerMsfSellerProperties($seller_id, $data = [])
	{
		if (!$seller_id) {
			return [];
		}

		$msf_seller_properties = [];

		$sql = "
			(
				SELECT
					spvi.seller_id,
					spvi.msf_seller_property_id,
					IFNULL(sgsp.sort_order, 0) as `sort_order`,
					GROUP_CONCAT(spvi.`msf_seller_property_value_id` SEPARATOR '~') as `value_ids`,
					GROUP_CONCAT(pvd.`name` SEPARATOR '~') as `value_names`
				FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id` spvi
				LEFT JOIN (SELECT `msf_seller_property_value_id`, `name` FROM `" . DB_PREFIX . "msf_seller_property_value_description` WHERE `language_id` = " . (isset($data['language_id']) ? (int)$data['language_id'] : (int)$this->config->get('config_language_id')) . ") pvd
					ON (pvd.`msf_seller_property_value_id` = spvi.`msf_seller_property_value_id`)
				LEFT JOIN (SELECT seller_id, seller_group FROM `" . DB_PREFIX . "ms_seller`) s
					ON (s.seller_id = spvi.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller_group_msf_seller_property` sgsp
					ON (sgsp.`seller_group_id` = s.`seller_group` AND sgsp.msf_seller_property_id = spvi.msf_seller_property_id)
				WHERE spvi.`seller_id` = " . (int)$seller_id . "
				GROUP BY spvi.seller_id, spvi.msf_seller_property_id, sgsp.sort_order
			) UNION (
				SELECT
					spvt.seller_id,
					spvt.msf_seller_property_id,
					IFNULL(sgsp.sort_order, 0) as `sort_order`,
					'' as `value_ids`,
					GROUP_CONCAT(spvt.`language_id`, '†‡', spvt.`value` SEPARATOR '~') as `value_names`
				FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_text` spvt
				LEFT JOIN (SELECT seller_id, seller_group FROM `" . DB_PREFIX . "ms_seller`) s
					ON (s.seller_id = spvt.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller_group_msf_seller_property` sgsp
					ON (sgsp.`seller_group_id` = s.`seller_group` AND sgsp.msf_seller_property_id = spvt.msf_seller_property_id)
				WHERE spvt.`seller_id` = " . (int)$seller_id
				. (isset($data['language_id']) ? " AND spvt.`language_id` = " . (int)$data['language_id'] : "") . "
				GROUP BY spvt.seller_id, spvt.msf_seller_property_id, sgsp.sort_order
			)
			ORDER BY sort_order
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as $row) {
			$value_names = explode('~', $row['value_names']);

			if (!empty($row['value_ids'])) {
				// Id values
				$value_ids = explode('~', $row['value_ids']);

				$msf_seller_properties[$row['msf_seller_property_id']] = [
					'id_values' => count($value_ids) === count($value_names) ? array_combine($value_ids, $value_names) : $value_names,
					'text_values' => []
				];
			} else {
				// Text values
				$text_values = [];

				foreach ($value_names as $value_name) {
					list($language_id, $name) = explode('†‡', $value_name);

					if ($name) {
						$text_values[$language_id] = $name;
					}
				}

				if (!empty($text_values)) {
					$msf_seller_properties[$row['msf_seller_property_id']] = [
						'id_values' => [],
						'text_values' => $text_values
					];
				}
			}
		}

		return $msf_seller_properties;
	}

	public function getSellersByMsfSellerPropertyId($msf_seller_property_id)
	{
		$sellers_ids = [];

		$sellers = $this->db->query("
			(
				SELECT
					`seller_id`
				FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id`
				WHERE msf_seller_property_id = '" . (int)$msf_seller_property_id . "'
			) UNION (
				SELECT DISTINCT
					`seller_id`
				FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_text`
				WHERE msf_seller_property_id = '" . (int)$msf_seller_property_id . "'
					AND `language_id` = " . (int)$this->config->get('config_language_id') . "
			)
		");

		foreach ($sellers->rows as $row) {
			$sellers_ids[] = $row['seller_id'];
		}

		return array_unique($sellers_ids);
	}

	public function getSellerGroupMsfSellerProperties($seller_group_id, $retrieve_default_if_empty = true)
	{
		$msf_seller_properties = [];

		// If seller group id is set, retrieve properties for it
		$sql = "
			SELECT
				`msf_seller_property_id`
			FROM `" . DB_PREFIX . "ms_seller_group_msf_seller_property` msgsp
			LEFT JOIN (SELECT `id`, `default` FROM `" . DB_PREFIX . "msf_seller_property`) msp
				ON (msp.`id` = msgsp.`msf_seller_property_id`)
			WHERE msgsp.`seller_group_id` = " . (int)$seller_group_id . "
			ORDER BY msgsp.sort_order
		";

		$result = $this->db->query($sql);

		if (!empty($result->rows)) {
			foreach ($result->rows as $row) {
				$msf_seller_properties[] = $this->get($row['msf_seller_property_id']);
			}
		} elseif ($retrieve_default_if_empty) {
			$msf_seller_properties = $this->getList(['default' => true, 'values' => true], [
				'order_by'  => 'sort_order',
				'order_way' => 'ASC',
			]);
		}

		return $msf_seller_properties;
	}



	public function getSellerGroupsByMsfSellerPropertyId($msf_seller_property_id)
	{
		$seller_group_ids = [];

		$seller_groups = $this->db->query("
			SELECT DISTINCT
				`seller_group_id`
			FROM `" . DB_PREFIX . "ms_seller_group_msf_seller_property`
			WHERE msf_seller_property_id = '" . (int)$msf_seller_property_id . "'
		");

		foreach ($seller_groups->rows as $row) {
			$seller_group_ids[] = $row['seller_group_id'];
		}

		return $seller_group_ids;
	}

	private function cleanupAttributeValues($msf_seller_property_id, $msf_seller_property_values_ids)
	{
		foreach ($msf_seller_property_values_ids as $value_id) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_value` WHERE `id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_seller_property_value_description` WHERE `msf_seller_property_value_id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id` WHERE `msf_seller_property_id` = " . (int)$msf_seller_property_id . " AND `msf_seller_property_value_id` = " . (int)$value_id);
		}
	}
}
