<?php

use MultiMerch\Core\Invoice\InvoiceItem as InvoiceItem;
use MultiMerch\Core\Invoice\Factories\InvoiceItemFactory as InvoiceItemFactory;

class MsInvoiceItem extends Model
{
	protected $invoice_item_factory;

	/**
	 * MsInvoice constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->invoice_item_factory = new InvoiceItemFactory($registry);
	}

	/**
	 * Retrieves invoice items of specified invoice from the database.
	 *
	 * @param	array	$data		Parameters.
	 * @param	array	$sorts		Sort parameters.
	 * @return	array				Invoice item or an array of invoice items.
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	public function get($data = [], $sorts = [])
	{
		$invoice_items = [];

		$filters = '';
		if(isset($sorts['filters'])) {
			foreach($sorts['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_invoice_item`
			WHERE 1 = 1"
			. (isset($data['invoice_item_id']) ? " AND `invoice_item_id` = " . (int)$data['invoice_item_id'] : "")
			. (isset($data['invoice_id']) ? " AND `invoice_id` = " . (int)$data['invoice_id'] : "")

			. $filters
			. (isset($sorts['order_by']) ? " ORDER BY {$sorts['order_by']} {$sorts['order_way']}" : '')
			. (isset($sorts['limit']) ? " LIMIT ".(int)$sorts['offset'].', '.(int)($sorts['limit']) : '')
		;

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$row['titles'] = $this->getTitles($row['invoice_item_id']);
				$row['totals'] = $this->MsLoader->MsInvoiceItemTotal->get(['invoice_item_id' => $row['invoice_item_id']]);

				$invoice_item = $this->invoice_item_factory->create($row);

				// Prepare metadata
				if ($metadata = $invoice_item->getMetadata())
					$invoice_item->setMetadata(json_decode($metadata, true));

				$invoice_items[] = $invoice_item;
			}
		}

		return $invoice_items;
	}

	/**
	 * Stores invoice item instance in the database.
	 *
	 * @param	InvoiceItem		$invoice_item	Invoice item instance.
	 * @return	int								Invoice item id.
	 */
	public function create(InvoiceItem $invoice_item)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_invoice_item`
			SET `invoice_id` = " . ((int)$invoice_item->getInvoiceId() ?: 'NULL') . ",
				`quantity` = " . (int)$invoice_item->getQuantity() . ",
				`price` = " . (float)$invoice_item->getPrice() . ",
				`metadata` = '" . $this->db->escape(json_encode($invoice_item->getMetadata())) . "'
		";

		$this->db->query($sql);

		$invoice_item_id = $this->db->getLastId();

		// Set titles
		$this->saveTitles($invoice_item_id, $invoice_item->getTitles());

		// Invoice item totals
		if ($invoice_item->getTotals()) {
			foreach ($invoice_item->getTotals() as $total) {
				$total->setInvoiceItemId($invoice_item_id);
				$this->MsLoader->MsInvoiceItemTotal->create($total);
			}
		}

		return $invoice_item_id;
	}

	/**
	 * Updates invoice item in the database.
	 *
	 * @param	int		$invoice_item_id	Invoice item id.
	 * @param	array	$data
	 */
	public function update($invoice_item_id, $data = [])
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_invoice_item`
			SET `invoice_item_id` = `invoice_item_id`"
			. (isset($data['invoice_id']) ? ", `invoice_id` = " . (int)$data['invoice_id'] : "")
			. (isset($data['quantity']) ? ", `quantity` = " . (int)$data['quantity'] : "")
			. (isset($data['price']) ? ", `price` = " . (float)$data['price'] : "")
			. (isset($data['metadata']) ? ", `metadata` = '" . $this->db->escape(json_encode($data['metadata'])) . "'" : "")
			. " WHERE `invoice_item_id` = " . $invoice_item_id
		;

		$this->db->query($sql);

		// Set titles
		if (isset($data['titles']))
			$this->saveTitles($invoice_item_id, $data['titles']);

		// @todo 9.0: Update totals
	}

	/**
	 * Deletes invoice item and its child invoice items from the database.
	 *
	 * @param	int		$invoice_item_id	Invoice item id.
	 */
	public function delete($invoice_item_id)
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_invoice_item`
			WHERE `invoice_item_id` = " . (int)$invoice_item_id
		;

		$this->db->query($sql);
	}

	/**
	 * Gets invoice item titles translated in different languages.
	 *
	 * @param	int		$invoice_item_id	Invoice item id.
	 * @return	array						Invoice item titles.
	 */
	private function getTitles($invoice_item_id)
	{
		$titles = [];

		$sql = "
			SELECT
				language_id,
				`title`
			FROM `" . DB_PREFIX . "ms_invoice_item_description`
			WHERE `invoice_item_id` = " . (int)$invoice_item_id . "
		";

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$titles[$row['language_id']] = $row['title'];
			}
		}

		return $titles;
	}

	/**
	 * Creates or updates titles of the invoice item.
	 *
	 * @param	int		$invoice_item_id	Invoice item id.
	 * @param	array	$titles				Array of titles. The structure is: [language_id => title, language_id => title ... ]
	 */
	private function saveTitles($invoice_item_id, $titles)
	{
		if (empty($titles))
			return;

		foreach ($titles as $language_id => $title) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_invoice_item_description`
				SET `invoice_item_id` = " . (int)$invoice_item_id . ",
					`language_id` = " . (int)$language_id . ",
					`title` = '" . $this->db->escape($title) . "'
				ON DUPLICATE KEY UPDATE
					`title` = '" . $this->db->escape($title) . "'
			";

			$this->db->query($sql);
		}
	}
}
