<?php

use MultiMerch\Core\Invoice\Invoice as Invoice;
use MultiMerch\Core\Invoice\Factories\InvoiceFactory as InvoiceFactory;

class MsInvoice extends Model
{
	protected $invoice_factory;

	/**
	 * MsInvoice constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->invoice_factory = new InvoiceFactory($registry);
	}

	/**
	 * Retrieves invoice from the database by specified parameters.
	 *
	 * @param	array			$data		Parameters.
	 * @param	array			$sorts		Sort parameters.
	 * @return	array|Invoice				Invoice or an array of invoices.
	 * @throws \MultiMerch\Module\Errors\Generic
	 */
	public function get($data = [], $sorts = [])
	{
		$invoices = [];

		$filters = '';
		if(isset($sorts['filters'])) {
			foreach($sorts['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$invoices_in_payout = [];
		if (isset($data['payout_id']))
			$invoices_in_payout = $this->MsLoader->MsPayout->getInvoiceIdByPayoutId($data['payout_id']);

		$invoice_types = [];
		if (isset($data['types'])) {
			$invoice_types = [];
			foreach ($data['types'] as $type) {
				$invoice_types[] = "'{$this->db->escape($type)}'";
			}
		}

		$sql = "
			SELECT
			SQL_CALC_FOUND_ROWS
				*
			FROM `" . DB_PREFIX . "ms_invoice`
			WHERE 1 = 1"
			. (isset($data['invoice_id']) ? " AND `invoice_id` = " . (int)$data['invoice_id'] : "")
			. (isset($data['invoice_no']) ? " AND `invoice_no` = '" . (int)$data['invoice_no'] . "'" : "")
			. (isset($data['payment_id']) ? " AND `payment_id` = " . (int)$data['payment_id'] : "")
			. (isset($data['type']) ? " AND `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['types']) ? " AND `type`IN (" . (!empty($invoice_types) ? implode(',', $invoice_types) : '') . ")" : "")
			. (isset($data['object_type']) ? " AND `object_type` = '" . $this->db->escape($data['object_type']) . "'" : "")
			. (isset($data['object_subtype']) ? " AND `object_subtype` = '" . $this->db->escape($data['object_subtype']) . "'" : "")
			. (isset($data['object_id']) ? " AND `object_id` = " . (int)$data['object_id'] : "")
			. (isset($data['sender_type']) ? " AND `sender_type` = '" . $this->db->escape($data['sender_type']) . "'" : "")
			. (isset($data['sender_id']) ? " AND `sender_id` = " . (int)$data['sender_id'] : "")
			. (isset($data['recipient_type']) ? " AND `recipient_type` = '" . $this->db->escape($data['recipient_type']) . "'" : "")
			. (isset($data['recipient_id']) ? " AND `recipient_id` = " . (int)$data['recipient_id'] : "")
			. (isset($data['status']) ? " AND `status` = " . (int)$data['status'] : "")
			. (isset($data['payout_id']) ? " AND `invoice_id` IN (" . (!empty($invoices_in_payout) ? implode(',', $invoices_in_payout) : 0) . ")" : "")

			. $filters
			. (isset($sorts['order_by']) ? " ORDER BY {$sorts['order_by']} {$sorts['order_way']}" : '')
			. (isset($sorts['limit']) ? " LIMIT ".(int)$sorts['offset'].', '.(int)($sorts['limit']) : '')
		;

		$result = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");

		if ($result->num_rows) {
			foreach ($result->rows as $row) {
				$this->prepareDataFromDb($row);
				$invoice = $this->invoice_factory->create($row['type'], $row);

				// Prepare metadata
				if ($metadata = $invoice->getMetadata())
					$invoice->setMetadata(json_decode($metadata, true));

				$invoices[] = $invoice;
			}
		}

		if (isset($invoices[0])) {
			$invoices[0]->total_rows = $total->row['total'];
		}

		return isset($data['single']) && !empty($invoices) ? $invoices[0] : $invoices;
	}

	/**
	 * Stores invoice instance in the database.
	 *
	 * @param	Invoice		$invoice		Invoice instance.
	 * @return	int			$invoice_id		Generated invoice id.
	 */
	public function create(Invoice $invoice)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_invoice`
			SET `invoice_no` = '" . $this->db->escape($invoice->getInvoiceNo()) . "',
				`payment_id` = " . ((int)$invoice->getPaymentId() ?: 'NULL') . ",
				`type` = '" . $this->db->escape($invoice->getType()) . "',
				`object_type` = " . ($invoice->getObjectType() ? "'{$this->db->escape($invoice->getObjectType())}'" : 'NULL') . ",
				`object_subtype` = " . ($invoice->getObjectSubtype() ? "'{$this->db->escape($invoice->getObjectSubtype())}'" : 'NULL') . ",
				`object_id` = " . (int)$invoice->getObjectId() . ",
				`sender_type` = '" . $this->db->escape($invoice->getSenderType()) . "',
				`sender_id` = " . (int)$invoice->getSenderId() . ",
				`recipient_type` = '" . $this->db->escape($invoice->getRecipientType()) . "',
				`recipient_id` = " . (int)$invoice->getRecipientId() . ",
				`status` = " . (int)$invoice->getStatus() . ",
				`total` = " . (float)$invoice->getTotal() . ",
				`currency_id` = " . (int)$invoice->getCurrencyId() . ",
				`currency_code` = '" . $this->db->escape($invoice->getCurrencyCode()) . "',
				`date_due` = " . $this->db->escape($invoice->getDateDue() ?: 'NULL') . ",
				`metadata` = '" . $this->db->escape(json_encode($invoice->getMetadata())) . "'"
				. ($invoice->getInvoiceId() ? ", `invoice_id` = " . (int)$invoice->getInvoiceId() : "")
				. ($invoice->getDateGenerated() ? ", `date_generated` = '" . $invoice->getDateGenerated() . "'" : "")
				. ($invoice->getDateUpdated() ? ", `date_updated` = '" . $invoice->getDateUpdated() . "'" : "")
		;

		$this->db->query($sql);

		$invoice_id = $invoice->getInvoiceId() ?: $this->db->getLastId();

		// Save invoice totals
		if ($invoice->getTotals()) {
			foreach ($invoice->getTotals() as $total) {
				$total->setInvoiceId($invoice_id);
				$this->MsLoader->MsInvoiceTotal->create($total);
			}
		}

		// Save invoice items
		if ($invoice->getItems()) {
			foreach ($invoice->getItems() as $item) {
				$item->setInvoiceId($invoice_id);
				$this->MsLoader->MsInvoiceItem->create($item);
			}
		}

		return $invoice_id;
	}

	/**
	 * Updates invoice in the database.
	 *
	 * @param	int		$invoice_id		Invoice id.
	 * @param	array	$data			Parameters for update.
	 */
	public function update($invoice_id, $data = [])
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_invoice`
			SET `invoice_id` = `invoice_id`"
			. (isset($data['invoice_no']) ? ", `invoice_no` = '" . $this->db->escape($data['invoice_no']) . "'" : "")
			. (isset($data['payment_id']) ? ", `payment_id` = " . (int)$data['payment_id'] : "")
			. (isset($data['type']) ? ", `type` = '" . $this->db->escape($data['type']) . "'" : "")
			. (isset($data['object_type']) ? ", `object_type` = '" . $this->db->escape($data['object_type']) . "'" : "")
			. (isset($data['object_subtype']) ? ", `object_subtype` = '" . $this->db->escape($data['object_subtype']) . "'" : "")
			. (isset($data['object_id']) ? ", `object_id` = " . (int)$data['object_id'] : "")
			. (isset($data['sender_type']) ? ", `sender_type` = '" . $this->db->escape($data['sender_type']) . "'" : "")
			. (isset($data['sender_id']) ? ", `sender_id` = " . (int)$data['sender_id'] : "")
			. (isset($data['recipient_type']) ? ", `recipient_type` = '" . $this->db->escape($data['recipient_type']) . "'" : "")
			. (isset($data['recipient_id']) ? ", `recipient_id` = " . (int)$data['recipient_id'] : "")
			. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
			. (isset($data['total']) ? ", `total` = " . (float)$data['total'] : "")
			. (isset($data['currency_id']) ? ", `currency_id` = " . (int)$data['currency_id'] : "")
			. (isset($data['currency_code']) ? ", `currency_code` = '" . $this->db->escape($data['currency_code']) . "'" : "")
			. (isset($data['date_due']) ? ", `currency_code` = '" . $this->db->escape(date('Y-m-d', $data['date_due'])) . "'" : "")
			. (isset($data['metadata']) ? ", `metadata` = '" . $this->db->escape(json_encode($data['metadata'])) . "'" : "")
			. " WHERE `invoice_id` = " . (int)$invoice_id
		;

		$this->db->query($sql);
	}

	/**
	 * Deletes invoice from the database.
	 *
	 * @param	int		$invoice_id		Invoice id.
	 * @param	array	$data			Parameters for delete.
	 */
	public function delete($invoice_id, $data = [])
	{
		$sql = "
			DELETE FROM `" . DB_PREFIX . "ms_invoice`
			WHERE `invoice_id` = " . (int)$invoice_id . "
		";

		$this->db->query($sql);
	}

	/**
	 * Prepares data from database to create Invoice instance.
	 *
	 * @param	array	$data	Data from the database.
	 */
	public function prepareDataFromDb(&$data)
	{
		$data['object'] = [
			'type' => $data['object_type'],
			'subtype' => $data['object_subtype'],
			'id' => $data['object_id']
		];

		$data['sender'] = "{$data['sender_type']}.{$data['sender_id']}";
		$data['recipient'] = "{$data['recipient_type']}.{$data['recipient_id']}";
		$data['items'] = $this->MsLoader->MsInvoiceItem->get(['invoice_id' => $data['invoice_id']]);
		$data['totals'] = $this->MsLoader->MsInvoiceTotal->get(['invoice_id' => $data['invoice_id']]);
	}
}
