<?php

class MsfVariation extends Model
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;

	public function getList($data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				msfv.*,
				msfvd.`name`,
				msfvd.`label`,
				IF(msfvd.`label` <> '', CONCAT(msfvd.`name`, ' / ', msfvd.`label`), msfvd.`name`) as `full_name`,
				msfvd.`note`
			FROM `" . DB_PREFIX . "msf_variation` msfv
			LEFT JOIN (SELECT msf_variation_id, `name`, `label`, `note` FROM `" . DB_PREFIX . "msf_variation_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfvd
				ON (msfvd.msf_variation_id = msfv.id)
			WHERE 1 = 1"

			. (isset($data['default']) ? " AND `default` = 1" : "")
			. (!empty($data['not_in_ids']) ? " AND msfv.`id` NOT IN (" . implode(',', $data['not_in_ids']) . ")" : "")

			. $filters
			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$result = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total_rows");
		if ($result->rows) {
			$result->rows[0]['total_rows'] = $total->row['total_rows'];

			if (isset($data['values'])) {
				foreach ($result->rows as &$row) {
					$row['values'] = $this->getValues($row['id']);
				}
			}
		}

		return $result->rows;
	}

	public function get($msf_variation_id, $data = [])
	{
		$sql = "
			SELECT
				msfv.*,
				msfvd.`name`,
				IF(msfvd.`label` <> '', msfvd.`label`, msfvd.`name`) as `label`,
				IF(msfvd.`label` <> '', CONCAT(msfvd.`name`, ' / ', msfvd.`label`), msfvd.`name`) as `full_name`
			FROM `" . DB_PREFIX . "msf_variation` msfv
			LEFT JOIN (SELECT msf_variation_id, `name`, `label` FROM `" . DB_PREFIX . "msf_variation_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfvd
				ON (msfvd.msf_variation_id = msfv.id)
			WHERE `id` = " . (int)$msf_variation_id . "
			LIMIT 1
		";

		$result = $this->db->query($sql);

		$sql_2 = "
			SELECT
				language_id,
				`name`,
				`label`,
				`note`
			FROM `" . DB_PREFIX ."msf_variation_description`
			WHERE msf_variation_id = " . (int)$msf_variation_id . "
			GROUP BY language_id
		";

		$result_2 = $this->db->query($sql_2);

		$descriptions = [];
		foreach ($result_2->rows as $row) {
			$descriptions[$row['language_id']] = [
				'name' => $row['name'],
				'label' => $row['label'],
				'note' => $row['note']
			];
		}

		$result->row['description'] = $descriptions;
		$result->row['values'] = $this->getValues($msf_variation_id);

		return $result->row;
	}

	public function getName($msf_variation_id, $language_id = null)
	{
		if (!$language_id) {
			$language_id = $this->config->get('config_language_id');
		}

		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_variation_description`
			WHERE msf_variation_id = " . (int)$msf_variation_id . "
				AND `language_id` = " . (int)$language_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function getValues($msf_variation_id)
	{
		$sql = "
			SELECT
				msfvv.`id`,
				msfvvd.`name`,
				msfvv.`sort_order`
			FROM `" . DB_PREFIX . "msf_variation_value` msfvv
			LEFT JOIN (SELECT msf_variation_value_id, `name` FROM `" . DB_PREFIX . "msf_variation_value_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfvvd
				ON (msfvvd.msf_variation_value_id = msfvv.id)
			WHERE msfvv.`msf_variation_id` = " . (int)$msf_variation_id . "
			ORDER BY msfvv.`sort_order`
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$sql_2 = "
				SELECT
					language_id,
					`name`
				FROM `" . DB_PREFIX ."msf_variation_value_description`
				WHERE msf_variation_value_id = " . (int)$row['id'] . "
				GROUP BY language_id
			";

			$result_2 = $this->db->query($sql_2);

			$descriptions = [];
			foreach ($result_2->rows as $r) {
				$descriptions[$r['language_id']] = ['name' => $r['name']];
			}

			$row['description'] = $descriptions;
		}

		return $result->rows;
	}

	public function getValue($msf_variation_value_id)
	{
		$sql = "
			SELECT
				msfvv.`id`,
				msfvvd.`name`,
				msfvv.`msf_variation_id`,
				msfvd.`name` as `msf_variation_name`
			FROM `" . DB_PREFIX . "msf_variation_value` msfvv
			LEFT JOIN (SELECT msf_variation_value_id, `name` FROM `" . DB_PREFIX . "msf_variation_value_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfvvd
				ON (msfvvd.msf_variation_value_id = msfvv.`id`)
			LEFT JOIN (SELECT msf_variation_id, `name`, `label` FROM `" . DB_PREFIX . "msf_variation_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") msfvd
				ON (msfvd.msf_variation_id = msfvv.`msf_variation_id`)
			WHERE msfvv.`id` = " . (int)$msf_variation_value_id
		;

		$result = $this->db->query($sql);

		return $result->row;
	}

	public function getValueName($msf_variation_value_id, $language_id = null)
	{
		if (!$language_id) {
			$language_id = $this->config->get('config_language_id');
		}

		$sql = "
			SELECT
				`name`
			FROM `" . DB_PREFIX . "msf_variation_value_description`
			WHERE msf_variation_value_id = " . (int)$msf_variation_value_id . "
				AND `language_id` = " . (int)$language_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function save($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "msf_variation`
			SET `status` = " . (int)$data['status']
				. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
				. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
		;

		$this->db->query($sql);

		$msf_variation_id = $this->db->getLastId();

		// Variation description
		foreach ($data['description'] as $language_id => $v) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_variation_description`
				SET `msf_variation_id` = " . (int)$msf_variation_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
				. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
				. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
			;

			$this->db->query($sql);
		}

		// Variation values
		if (!empty($data['values'])) {
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_variation_value`
					SET `msf_variation_id` = " . (int)$msf_variation_id . ",
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_variation_value_id = $this->db->getLastId();

				// Variation values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_variation_value_description`
						SET `msf_variation_value_id` = " . (int)$msf_variation_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}
	}

	public function update($msf_variation_id, $data = [])
	{
		// Needed to understand whether values were changed
		$old_msf_variation = $this->get($msf_variation_id);
		$old_values_ids = array_column($old_msf_variation['values'], 'id');

		// Update existing variation
		$sql = "
			UPDATE `" . DB_PREFIX . "msf_variation`
			SET `id` = `id`"

			. (isset($data['status']) ? ", `status` = " . (int)$data['status'] : "")
			. (isset($data['default']) ? ", `default` = " . (int)$data['default'] : "")
			. (isset($data['sort_order']) ? ", `sort_order` = " . (int)$data['sort_order'] : "")
			
			. " WHERE `id` = " . (int)$msf_variation_id;

		$this->db->query($sql);

		// Variation description
		if (!empty($data['description'])) {
			// Cleanup variation description
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_description` WHERE `msf_variation_id` = " . (int)$msf_variation_id);

			foreach ($data['description'] as $language_id => $v) {
				$sql = "
				INSERT INTO `" . DB_PREFIX . "msf_variation_description`
				SET `msf_variation_id` = " . (int)$msf_variation_id . ",
					`language_id` = " . (int)$language_id . ",
					`name` = '" . $this->db->escape($v['name']) . "'"
					. (isset($v['label']) ? ", `label` = '" . $this->db->escape($v['label']) . "'" : "")
					. (isset($v['note']) ? ", `note` = '" . $this->db->escape($v['note']) . "'" : "")
				;

				$this->db->query($sql);
			}
		}

		// Variation values
		if (!empty($data['values'])) {
			// Remove relation to products for values that do not exist anymore
			$new_values_ids = array_column($data['values'], 'id');
			$this->cleanupVariationValues($msf_variation_id, array_diff($old_values_ids, $new_values_ids));

			// Save new values
			foreach ($data['values'] as $sort_order => $value) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "msf_variation_value`
					SET `msf_variation_id` = " . (int)$msf_variation_id . ",
						`sort_order` = " . (int)$sort_order
						. (!empty($value['id']) ? ", `id` = " . $value['id'] : "") . "
					ON DUPLICATE KEY UPDATE
						`sort_order` = " . (int)$sort_order
				;

				$this->db->query($sql);

				$msf_variation_value_id = !empty($value['id']) ? $value['id'] : $this->db->getLastId();

				// Variation values description
				foreach ($value['description'] as $language_id => $v) {
					$sql = "
						INSERT INTO `" . DB_PREFIX . "msf_variation_value_description`
						SET `msf_variation_value_id` = " . (int)$msf_variation_value_id . ",
							`language_id` = " . (int)$language_id . ",
							`name` = '" . $this->db->escape($v['name']) . "'
						ON DUPLICATE KEY UPDATE
							`name` = '" . $this->db->escape($v['name']) . "'
					";

					$this->db->query($sql);
				}
			}
		}

		return true;
	}

	public function delete($msf_variation_id)
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation` WHERE `id` = " . (int)$msf_variation_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_description` WHERE `msf_variation_id` = " . (int)$msf_variation_id);

		$values = $this->db->query("SELECT `id` FROM `" . DB_PREFIX . "msf_variation_value` WHERE `msf_variation_id` = " . (int)$msf_variation_id)->rows;
		foreach ($values as $value) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_value` WHERE `id` = " . (int)$value['id']);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_value_description` WHERE `msf_variation_value_id` = " . (int)$value['id']);
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_oc_category_msf_variation` WHERE `msf_variation_id` = " . (int)$msf_variation_id);

		$product_variations = $this->db->query("SELECT DISTINCT `ms_product_msf_variation_id` FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` WHERE `msf_variation_id` = " . (int)$msf_variation_id)->rows;
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` WHERE `msf_variation_id` = " . (int)$msf_variation_id);

		foreach ($product_variations as $product_variation) {
			$product_has_other_variations = $this->db->query("SELECT `msf_variation_id` FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` WHERE `ms_product_msf_variation_id` = " . (int)$product_variation['ms_product_msf_variation_id'] . " LIMIT 1");

			if (empty($product_has_other_variations->row['msf_variation_id'])) {
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_variation` WHERE `id` = " . (int)$product_variation['ms_product_msf_variation_id']);
			}
		}
	}



	public function getProductMsfVariationsAll($product_id)
	{
		$sql = "
			SELECT
				pvm.`msf_variation_id`,
				GROUP_CONCAT(DISTINCT pvm.`msf_variation_value_id` SEPARATOR ',') as `msf_variation_value_ids`
			FROM `" . DB_PREFIX . "ms_product_msf_variation` pv
			LEFT JOIN `" . DB_PREFIX . "ms_product_msf_variation_matrix` pvm
				ON (pvm.`ms_product_msf_variation_id` = pv.`id`)
			WHERE pv.`product_id` = " . (int)$product_id . "
				AND pv.`status` = 1
			GROUP BY pvm.`msf_variation_id`
		";

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$row['values'] = [];
			foreach (explode(',', $row['msf_variation_value_ids']) as $msf_variation_value_id) {
				$row['values'][] = $this->getValue($msf_variation_value_id);
			}

			unset($row['msf_variation_value_ids']);
		}

		return $result->rows;
	}

	public function getProductMsfVariationsFiltered($product_id, $msf_variation_values)
	{
		$sql = "
			SELECT
				pv.*
			FROM `" . DB_PREFIX . "ms_product_msf_variation` pv
		";

		foreach ($msf_variation_values as $msf_variation_id => $value) {
			$sql .= "
				LEFT JOIN `" . DB_PREFIX . "ms_product_msf_variation_matrix` pvm{$msf_variation_id}
					ON (pvm{$msf_variation_id}.ms_product_msf_variation_id = pv.id)
			";
		}

		$sql .= " WHERE pv.product_id = " . (int)$product_id;

		foreach ($msf_variation_values as $msf_variation_id => $value) {
			$sql .= "
				AND (pvm{$msf_variation_id}.msf_variation_id = " . (int)$msf_variation_id . " AND pvm{$msf_variation_id}.msf_variation_value_id = " . (isset($value['id']) ? (int)$value['id'] : 0) . ")
			";
		}

		$result = $this->db->query($sql);

		foreach ($result->rows as &$row) {
			$res = $this->db->query("
				SELECT
					*
				FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix`
				WHERE `ms_product_msf_variation_id` = " . (int)$row['id']
			);

			foreach ($res->rows as $r) {
				$row['values'][] = $this->getValue($r['msf_variation_value_id']);
			}
		}

		return $result->rows;
	}

	public function getProductSelectedMsfVariations($product_id, $msf_variation_id)
	{
		$msf_variation_value_ids = [];

		$sql = "
			SELECT DISTINCT
				pvm.msf_variation_value_id
			FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` pvm
			LEFT JOIN (SELECT id, product_id FROM `" . DB_PREFIX . "ms_product_msf_variation`) pv
				ON (pv.id = pvm.ms_product_msf_variation_id)
			WHERE pv.product_id = " . (int)$product_id . "
				AND pvm.msf_variation_id = " . (int)$msf_variation_id
		;

		$result = $this->db->query($sql);

		foreach ($result->rows as $row) {
			$msf_variation_value_ids[] = $row['msf_variation_value_id'];
		}

		return $msf_variation_value_ids;
	}

	public function getProductMsfVariation($product_id, $msf_variation_values)
	{
		$product_msf_variation = [];

		$sql = "
			SELECT
				pv.*
			FROM `" . DB_PREFIX . "ms_product_msf_variation` pv
		";

		foreach ($msf_variation_values as $msf_variation_id => $value) {
			$sql .= "
				LEFT JOIN `" . DB_PREFIX . "ms_product_msf_variation_matrix` pvm{$msf_variation_id}
					ON (pvm{$msf_variation_id}.ms_product_msf_variation_id = pv.id)
			";
		}

		$sql .= " WHERE pv.product_id = " . (int)$product_id;

		foreach ($msf_variation_values as $msf_variation_id => $value) {
			$sql .= "
				AND (pvm{$msf_variation_id}.msf_variation_id = " . (int)$msf_variation_id . " AND pvm{$msf_variation_id}.msf_variation_value_id = " . (isset($value['id']) ? (int)$value['id'] : 0) . ")
			";
		}

		$result = $this->db->query($sql);

		if ($result->num_rows) {
			$matrix_row_key = $values_data = [];
			foreach ($msf_variation_values as $msf_variation_id => $value) {
				$matrix_row_key[] = $value['msf_variation_id'] . '-' . $value['id'];

				$values_data[] = [
					'msf_variation_id' => $msf_variation_id,
					'msf_variation_value_id' => $value['id'],
					'name' => $this->getName($msf_variation_id),
					'value' => $this->getValueName($value['id'])
				];
			}

			$product_msf_variation = [
				'id' => $result->row['id'],
				'key' => implode('_', $matrix_row_key),
				'quantity' => $result->row['quantity'],
				'price' => $result->row['price'],
				'image' => $result->row['image'],
				'status' => $result->row['status'],
				'values_data' => $values_data
			];
		}

		return $product_msf_variation;
	}

	public function generateEmptyVariationsMatrix($selected_msf_variations)
	{
		$msf_variations_matrix = [];

		// Generate variations matrix
		$combinations = $this->MsLoader->MsHelper->getPossibleCombinations($selected_msf_variations);

		foreach ($combinations as $row_id => $combination) {
			$msf_variations_matrix[$row_id] = [
				'key' => '',
				'quantity' => 0,
				'price' => '',
				'image' => '',
				'status' => MsfVariation::STATUS_ACTIVE
			];

			$matrix_row_key = [];

			foreach ($combination as $msf_variation_value_id) {
				$msf_variation_value = $this->MsLoader->MsfVariation->getValue($msf_variation_value_id);
				$msf_variation_id = $msf_variation_value['msf_variation_id'];

				$msf_variations_matrix[$row_id]['values'][$msf_variation_id] = $msf_variation_value;

				$matrix_row_key[$row_id][] = $msf_variation_id . '-' . $msf_variation_value_id;
			}

			$msf_variations_matrix[$row_id]['key'] = implode('_', $matrix_row_key[$row_id]);
		}

		return $msf_variations_matrix;
	}

	public function getProductMsfVariationByKey($product_id, $key)
	{
		$key_parts = explode('_', $key);

		$selected = [];
		foreach ($key_parts as $key_part) {
			list($msf_variation_id, $msf_variation_value_id) = explode('-', $key_part);
			$selected[$msf_variation_id] = $this->getValue($msf_variation_value_id);
		}

		$product_msf_variation = $this->getProductMsfVariation($product_id, $selected);

		return $product_msf_variation;
	}

	public function prepareProductMsfVariationForCart($product_msf_variation)
	{
		$msf_variation = [];

		if (!empty($product_msf_variation)) {
			$product_msf_variation = (array)$product_msf_variation;

			$msf_variation = [
				'id' => $product_msf_variation['id'],
				'key' => $product_msf_variation['key'],
			];
		}

		return $msf_variation;
	}



	public function getProductsByMsfVariationId($msf_variation_id)
	{
		$products_ids = [];

		$products = $this->db->query("
			SELECT
				pv.`product_id`
			FROM `" . DB_PREFIX . "ms_product_msf_variation` pv
			LEFT JOIN `" . DB_PREFIX . "ms_product_msf_variation_matrix` pvm
				ON (pvm.ms_product_msf_variation_id = pv.id)
			WHERE pvm.msf_variation_id = '" . (int)$msf_variation_id . "'
			GROUP BY pv.product_id
		");

		foreach ($products->rows as $row) {
			$products_ids[] = $row['product_id'];
		}

		return array_unique($products_ids);
	}

	public function getOcCategoryMsfVariations($oc_category_id, $retrieve_inherited_category = false)
	{
		$sql = "
			SELECT
				c.`category_id`,
				c.`parent_id`,
				GROUP_CONCAT(msca.msf_variation_id ORDER BY msca.sort_order SEPARATOR ',') as `msf_variation_ids`
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN `" . DB_PREFIX . "ms_oc_category_msf_variation` msca
				ON (msca.oc_category_id = c.category_id)
			WHERE c.`category_id` = " . (int)$oc_category_id . "
			GROUP BY msca.oc_category_id
		";

		$oc_category = $this->db->query($sql)->row;

		if (!empty($oc_category['msf_variation_ids'])) {
			$msf_variation_ids = explode(',', $oc_category['msf_variation_ids']);
			$inherited_category_id = $oc_category['category_id'];
		} elseif (!empty($oc_category['parent_id'])) {
			$msf_variation_ids = $this->getOcCategoryMsfVariations($oc_category['parent_id']);
			$inherited_category_id = $oc_category['parent_id'];
		} else {
			$msf_variation_ids = [];
			$inherited_category_id = 0;
		}

		return $retrieve_inherited_category ? [$msf_variation_ids, $inherited_category_id] : $msf_variation_ids;
	}

	public function getOcCategoriesByMsfVariationId($msf_variation_id)
	{
		$oc_category_ids = [];

		$oc_categories = $this->db->query("
			SELECT DISTINCT
				`oc_category_id`
			FROM `" . DB_PREFIX . "ms_oc_category_msf_variation`
			WHERE msf_variation_id = '" . (int)$msf_variation_id . "'
		");

		foreach ($oc_categories->rows as $row) {
			$oc_category_ids[] = $row['oc_category_id'];
		}

		return $oc_category_ids;
	}

	private function cleanupVariationValues($msf_variation_id, $msf_variation_values_ids)
	{
		foreach ($msf_variation_values_ids as $value_id) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_value` WHERE `id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "msf_variation_value_description` WHERE `msf_variation_value_id` = " . (int)$value_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` WHERE `msf_variation_id` = " . (int)$msf_variation_id . " AND `msf_variation_value_id` = " . (int)$value_id);
		}
	}
}
