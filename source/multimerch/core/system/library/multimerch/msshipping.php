<?php

class MsShipping extends Model
{
	/**
	 * Gets list of cart weight-based shipping rules for seller.
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Filter data.
	 * @return	array				List of shipping rules.
	 */
	public function getCartWeightBasedRules($seller_id, $data = [])
	{
		$sql = "
			SELECT
				sr.*,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description` WHERE `shipping_method_id` = sr.`shipping_method_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `shipping_method_name`,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_delivery_time_description` WHERE `delivery_time_id` = sr.`delivery_time_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `delivery_time_name`
			FROM `" . DB_PREFIX . "ms_shipping_rule_cart_weight` sr
			WHERE `seller_id` = " . (int)$seller_id
		;

		$rules = $this->db->query($sql);

		foreach ($rules->rows as &$rule) {
			$sql = "
				SELECT
					sd.`geo_zone_id` as `id`,
					gzd.`name`
				FROM `" . DB_PREFIX . "ms_shipping_rule_cart_weight_destination` sd
				LEFT JOIN (SELECT `geo_zone_id`, `name` FROM `" . DB_PREFIX . "geo_zone`) gzd
					ON (gzd.`geo_zone_id` = sd.geo_zone_id)
				WHERE `rule_id` = " . (int)$rule['id']
			;

			$destinations = $this->db->query($sql);

			foreach ($destinations->rows as &$destination) {
				if ((int)$this->config->get('msconf_shipping_worldwide_destination_id') === (int)$destination['id']) {
					$destination['name'] = $this->language->get('ms_account_product_shipping_elsewhere');
				}
			}

			$rule['destinations'] = $destinations->rows;

			// Format costs
			$rule['cost_formatted'] = $this->currency->format($rule['cost'], $rule['currency_code']);
			$rule['cost_per_weight_unit_formatted'] = $this->currency->format($rule['cost_per_weight_unit'], $rule['currency_code']);
		}

		return $rules->rows;
	}

	/**
	 * Creates cart weight-based shipping rule.
	 *
	 * @param	array	$data	Data for a shipping rule creation.
	 * @return	bool			Whether rule is created or not.
	 */
	public function createCartWeightBasedRule($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_cart_weight`
			SET `seller_id` = " . (int)$data['seller_id'] . ",
				`weight_class_id` = " . (isset($data['weight_class_id']) ? (int)$data['weight_class_id'] : (int)$this->config->get('config_weight_class_id')) . ",
				`currency_id` = " . (isset($data['currency_id']) ? (int)$data['currency_id'] : (int)$this->currency->getId($this->config->get('config_currency'))) . ",
				`currency_code` = '" . $this->db->escape(isset($data['currency_code']) ? $data['currency_code'] : $this->config->get('config_currency')) . "'"

				. (isset($data['shipping_method_id']) ? ", `shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
				. (isset($data['delivery_time_id']) ? ", `delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
				. (isset($data['weight_from']) ? ", `weight_from` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['weight_from']) : "")
				. (isset($data['weight_to']) ? ", `weight_to` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['weight_to']) : "")
				. (isset($data['cost']) ? ", `cost` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost']) : "")
				. (isset($data['cost_per_weight_unit']) ? ", `cost_per_weight_unit` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost_per_weight_unit']) : "")
		;

		$this->db->query($sql);

		$rule_id = $this->db->getLastId();

		if (!empty($data['destinations'])) {
			foreach ($data['destinations'] as $destination_id) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_cart_weight_destination`
					SET `rule_id` = " . (int)$rule_id . ",
						`geo_zone_id` = " . (int)$destination_id . "
				";

				$this->db->query($sql);
			}
		}

		return true;
	}

	/**
	 * Deletes cart weight-based shipping rule(s).
	 *
	 * @todo 9.0: confirm join works correctly with multiple destinations
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Conditions for rule deleting.
	 * @return	bool				Whether rule is deleted or not.
	 */
	public function deleteCartWeightBasedRule($seller_id, $data = [])
	{
		$sql = "
			DELETE r, rd
			FROM `" . DB_PREFIX . "ms_shipping_rule_cart_weight` r
			LEFT JOIN `" . DB_PREFIX . "ms_shipping_rule_cart_weight_destination` rd
				ON (rd.`rule_id` = r.`id`)
			WHERE r.`seller_id` = " . (int)$seller_id
			. (isset($data['shipping_method_id']) ? " AND r.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? " AND r.`delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
		;

		$this->db->query($sql);

		return true;
	}

	/**
	 * Gets list of cart total-based shipping rules for seller.
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Filter data.
	 * @return	array				List of shipping rules.
	 */
	public function getCartTotalBasedRules($seller_id, $data = [])
	{
		$sql = "
			SELECT
				sr.*,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description` WHERE `shipping_method_id` = sr.`shipping_method_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `shipping_method_name`,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_delivery_time_description` WHERE `delivery_time_id` = sr.`delivery_time_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `delivery_time_name`
			FROM `" . DB_PREFIX . "ms_shipping_rule_cart_total` sr
			WHERE `seller_id` = " . (int)$seller_id
		;

		$rules = $this->db->query($sql);

		foreach ($rules->rows as &$rule) {
			$sql = "
				SELECT
					sd.`geo_zone_id` as `id`,
					gzd.`name`
				FROM `" . DB_PREFIX . "ms_shipping_rule_cart_total_destination` sd
				LEFT JOIN (SELECT `geo_zone_id`, `name` FROM `" . DB_PREFIX . "geo_zone`) gzd
					ON (gzd.`geo_zone_id` = sd.geo_zone_id)
				WHERE `rule_id` = " . (int)$rule['id']
			;

			$destinations = $this->db->query($sql);

			foreach ($destinations->rows as &$destination) {
				if ((int)$this->config->get('msconf_shipping_worldwide_destination_id') === (int)$destination['id']) {
					$destination['name'] = $this->language->get('ms_account_product_shipping_elsewhere');
				}
			}

			$rule['destinations'] = $destinations->rows;

			// Format totals and costs
			$rule['total_from_formatted'] = $this->currency->format($rule['total_from'], $rule['currency_code']);
			$rule['total_to_formatted'] = $this->currency->format($rule['total_to'], $rule['currency_code']);
			$rule['cost_formatted'] = $this->currency->format($rule['cost'], $rule['currency_code']);
		}

		return $rules->rows;
	}

	/**
	 * Creates cart total-based shipping rule.
	 *
	 * @param	array	$data	Data for a shipping rule creation.
	 * @return	bool			Whether rule is created or not.
	 */
	public function createCartTotalBasedRule($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_cart_total`
			SET `seller_id` = " . (int)$data['seller_id'] . ",
				`currency_id` = " . (isset($data['currency_id']) ? (int)$data['currency_id'] : (int)$this->currency->getId($this->config->get('config_currency'))) . ",
				`currency_code` = '" . $this->db->escape(isset($data['currency_code']) ? $data['currency_code'] : $this->config->get('config_currency')) . "'"

			. (isset($data['shipping_method_id']) ? ", `shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? ", `delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
			. (isset($data['total_from']) ? ", `total_from` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['total_from']) : "")
			. (isset($data['total_to']) ? ", `total_to` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['total_to']) : "")
			. (isset($data['cost']) ? ", `cost` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost']) : "")
		;

		$this->db->query($sql);

		$rule_id = $this->db->getLastId();

		if (!empty($data['destinations'])) {
			foreach ($data['destinations'] as $destination_id) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_cart_total_destination`
					SET `rule_id` = " . (int)$rule_id . ",
						`geo_zone_id` = " . (int)$destination_id . "
				";

				$this->db->query($sql);
			}
		}

		return true;
	}

	/**
	 * Deletes cart total-based shipping rule(s).
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Conditions for rule deleting.
	 * @return	bool				Whether rule is deleted or not.
	 */
	public function deleteCartTotalBasedRule($seller_id, $data = [])
	{
		$sql = "
			DELETE r, rd
			FROM `" . DB_PREFIX . "ms_shipping_rule_cart_total` r
			LEFT JOIN `" . DB_PREFIX . "ms_shipping_rule_cart_weight_destination` rd
				ON (rd.`rule_id` = r.`id`)
			WHERE r.`seller_id` = " . (int)$seller_id
			. (isset($data['shipping_method_id']) ? " AND r.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? " AND r.`delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
		;

		$this->db->query($sql);

		return true;
	}

	/**
	 * Gets list of flat shipping rules for seller.
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Filter data.
	 * @return	array				List of shipping rules.
	 */
	public function getFlatRules($seller_id, $data = [])
	{
		$sql = "
			SELECT
				sr.*,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description` WHERE `shipping_method_id` = sr.`shipping_method_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `shipping_method_name`,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_delivery_time_description` WHERE `delivery_time_id` = sr.`delivery_time_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `delivery_time_name`
			FROM `" . DB_PREFIX . "ms_shipping_rule_flat` sr
			WHERE `seller_id` = " . (int)$seller_id
		;

		$rules = $this->db->query($sql);

		foreach ($rules->rows as &$rule) {
			$sql = "
				SELECT
					sd.`geo_zone_id` as `id`,
					gzd.`name`
				FROM `" . DB_PREFIX . "ms_shipping_rule_flat_destination` sd
				LEFT JOIN (SELECT `geo_zone_id`, `name` FROM `" . DB_PREFIX . "geo_zone`) gzd
					ON (gzd.`geo_zone_id` = sd.geo_zone_id)
				WHERE `rule_id` = " . (int)$rule['id']
			;

			$destinations = $this->db->query($sql);

			foreach ($destinations->rows as &$destination) {
				if ((int)$this->config->get('msconf_shipping_worldwide_destination_id') === (int)$destination['id']) {
					$destination['name'] = $this->language->get('ms_account_product_shipping_elsewhere');
				}
			}

			$rule['destinations'] = $destinations->rows;

			// Format cost
			$rule['cost_formatted'] = $this->currency->format($rule['cost'], $rule['currency_code']);
		}

		return $rules->rows;
	}

	/**
	 * Creates flat shipping rule.
	 *
	 * @param	array	$data	Data for a shipping rule creation.
	 * @return	bool			Whether rule is created or not.
	 */
	public function createFlatRule($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_flat`
			SET `seller_id` = " . (int)$data['seller_id'] . ",
				`currency_id` = " . (isset($data['currency_id']) ? (int)$data['currency_id'] : (int)$this->currency->getId($this->config->get('config_currency'))) . ",
				`currency_code` = '" . $this->db->escape(isset($data['currency_code']) ? $data['currency_code'] : $this->config->get('config_currency')) . "'"

			. (isset($data['shipping_method_id']) ? ", `shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? ", `delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
			. (isset($data['cost']) ? ", `cost` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost']) : "")
		;

		$this->db->query($sql);

		$rule_id = $this->db->getLastId();

		if (!empty($data['destinations'])) {
			foreach ($data['destinations'] as $destination_id) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_flat_destination`
					SET `rule_id` = " . (int)$rule_id . ",
						`geo_zone_id` = " . (int)$destination_id . "
				";

				$this->db->query($sql);
			}
		}

		return true;
	}

	/**
	 * Deletes flat shipping rule(s).
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$data		Conditions for rule deleting.
	 * @return	bool				Whether rule is deleted or not.
	 */
	public function deleteFlatRule($seller_id, $data = [])
	{
		$sql = "
			DELETE r, rd
			FROM `" . DB_PREFIX . "ms_shipping_rule_flat` r
			LEFT JOIN `" . DB_PREFIX . "ms_shipping_rule_flat_destination` rd
				ON (rd.`rule_id` = r.`id`)
			WHERE r.`seller_id` = " . (int)$seller_id
			. (isset($data['shipping_method_id']) ? " AND r.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? " AND r.`delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
		;

		$this->db->query($sql);

		return true;
	}

	/**
	 * Gets list of per-product shipping rules for seller.
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	int		$product_id	Product id.
	 * @param	array	$data		Filter data.
	 * @return	array				List of shipping rules.
	 */
	public function getPerProductRules($seller_id, $product_id, $data = [])
	{
		$sql = "
			SELECT
				sr.*,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_method_description` WHERE `shipping_method_id` = sr.`shipping_method_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `shipping_method_name`,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_shipping_delivery_time_description` WHERE `delivery_time_id` = sr.`delivery_time_id` AND `language_id` = " . (int)$this->config->get('config_language_id') . ") as `delivery_time_name`
			FROM `" . DB_PREFIX . "ms_shipping_rule_per_product` sr
			WHERE `seller_id` = " . (int)$seller_id . "
				AND `product_id` = " . (int)$product_id
		;

		$rules = $this->db->query($sql);

		foreach ($rules->rows as &$rule) {
			$sql = "
				SELECT
					sd.`geo_zone_id` as `id`,
					gzd.`name`
				FROM `" . DB_PREFIX . "ms_shipping_rule_per_product_destination` sd
				LEFT JOIN (SELECT `geo_zone_id`, `name` FROM `" . DB_PREFIX . "geo_zone`) gzd
					ON (gzd.`geo_zone_id` = sd.geo_zone_id)
				WHERE `rule_id` = " . (int)$rule['id']
			;

			$destinations = $this->db->query($sql);

			foreach ($destinations->rows as &$destination) {
				if ((int)$this->config->get('msconf_shipping_worldwide_destination_id') === (int)$destination['id']) {
					$destination['name'] = $this->language->get('ms_account_product_shipping_elsewhere');
				}
			}

			$rule['destinations'] = $destinations->rows;

			// Format costs
			$rule['cost_formatted'] = $this->currency->format($rule['cost'], $rule['currency_code']);
			$rule['cost_per_additional_item_formatted'] = $this->currency->format($rule['cost_per_additional_item'], $rule['currency_code']);
		}

		return $rules->rows;
	}

	/**
	 * Creates per-product shipping rule.
	 *
	 * @param	array	$data	Data for a shipping rule creation.
	 * @return	bool			Whether rule is created or not.
	 */
	public function createPerProductRule($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_per_product`
			SET `seller_id` = " . (int)$data['seller_id'] . ",
				`product_id` = " . (int)$data['product_id'] . ",
				`currency_id` = " . (isset($data['currency_id']) ? (int)$data['currency_id'] : (int)$this->currency->getId($this->config->get('config_currency'))) . ",
				`currency_code` = '" . $this->db->escape(isset($data['currency_code']) ? $data['currency_code'] : $this->config->get('config_currency')) . "'"

			. (isset($data['shipping_method_id']) ? ", `shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? ", `delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
			. (isset($data['cost']) ? ", `cost` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost']) : "")
			. (isset($data['cost_per_additional_item']) ? ", `cost_per_additional_item` = " . $this->MsLoader->MsHelper->uniformDecimalPoint($data['cost_per_additional_item']) : "")
		;

		$this->db->query($sql);

		$rule_id = $this->db->getLastId();

		if (!empty($data['destinations'])) {
			foreach ($data['destinations'] as $destination_id) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_shipping_rule_per_product_destination`
					SET `rule_id` = " . (int)$rule_id . ",
						`geo_zone_id` = " . (int)$destination_id . "
				";

				$this->db->query($sql);
			}
		}

		return true;
	}

	/**
	 * Deletes per-product shipping rule(s).
	 *
	 * @param	int		$seller_id	Seller id.
	 * @param	int		$product_id	Product id.
	 * @param	array	$data		Conditions for rule deleting.
	 * @return	bool				Whether rule is deleted or not.
	 */
	public function deletePerProductRule($seller_id, $product_id, $data = [])
	{
		$sql = "
			DELETE r, rd
			FROM `" . DB_PREFIX . "ms_shipping_rule_per_product` r
			LEFT JOIN `" . DB_PREFIX . "ms_shipping_rule_per_product_destination` rd
				ON (rd.`rule_id` = r.`id`)
			WHERE r.`seller_id` = " . (int)$seller_id . "
				AND r.`product_id` = " . (int)$product_id
			. (isset($data['shipping_method_id']) ? " AND r.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "")
			. (isset($data['delivery_time_id']) ? " AND r.`delivery_time_id` = " . (int)$data['delivery_time_id'] : "")
		;

		$this->db->query($sql);

		return true;
	}

	/**
	 * Gets suborder combined shipping data.
	 *
	 * @param	int		$suborder_id	Suborder id.
	 * @param	array	$data			Conditions.
	 * @return	array					Shipping data.
	 */
	public function getSuborderShippingData($suborder_id, $data = [])
	{
		$sql = "
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_suborder_shipping_data`
			WHERE `suborder_id` = " . (int)$suborder_id
		;

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row : [];
	}

	/**
	 * Adds shipping data for the suborder.
	 *
	 * @param	array	$data	Suborder shipping data.
	 * @return	bool			Whether data is stored or not.
	 */
	public function addSuborderShippingData($data = [])
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_suborder_shipping_data`
			SET `suborder_id` = " . (int)$data['suborder_id'] . ",
				`shipping_method_name` = '" . $this->db->escape($data['shipping_method_name']) . "',
				`cost` = " . (float)$data['cost']
		;

		$this->db->query($sql);

		return true;
	}

	/**
	 * Gets countries list from OpenCart `oc_country` table.
	 *
	 * @param	array	$data	Filter data.
	 * @return	array			List of countries.
	 */
	public function getCountries($data = []) {
		$sql = "
			SELECT
				`country_id`,
				`name`
			FROM `" . DB_PREFIX . "country`
			WHERE 1 = 1"
			. (isset($data['name']) ? " AND `name` LIKE '" . $this->db->escape($data['name']) . "%'" : "")
			. (isset($data['country_id']) ? " AND `country_id` = " . (int)$data['country_id'] : "")
		;

		$countries = $this->db->query($sql);

		return (isset($data['country_id']) && $countries->num_rows) ? $countries->row : $countries->rows;
	}

	/**
	 * Gets geo zones list from OpenCart `oc_geo_zone` table.
	 * Adds fallback `Worldwide` option to the result array.
	 *
	 * @param	array	$data	Filter data.
	 * @return	array			List of geo zones.
	 */
	public function getGeoZones($data = []) {
		$sql = "
			SELECT
				`geo_zone_id`,
				`name`
			FROM `" . DB_PREFIX . "geo_zone`
			WHERE 1 = 1"
			. (isset($data['name']) ? " AND `name` LIKE '" . $this->db->escape($data['name']) . "%'" : "")
			. (isset($data['geo_zone_id']) ? " AND `geo_zone_id` = " . (int)$data['geo_zone_id'] : "")
		;

		$geo_zones = $this->db->query($sql);

		array_unshift($geo_zones->rows, [
			'geo_zone_id' => $this->config->get('msconf_shipping_worldwide_destination_id'),
			'name' => strip_tags(html_entity_decode($this->language->get('ms_account_product_shipping_elsewhere'), ENT_QUOTES, 'UTF-8')),
		]);

		return (isset($data['geo_zone_id']) && $geo_zones->num_rows) ? $geo_zones->row : $geo_zones->rows;
	}

	/**
	 * Gets shipping methods list from MultiMerch `ms_shipping_method` table.
	 *
	 * @param	array	$data	Filter data.
	 * @return	array			List of shipping methods.
	 */
	public function getShippingMethods($data = []) {
		$sql = "
			SELECT
				sm.`shipping_method_id`,
				smd.`name`
			FROM `" . DB_PREFIX . "ms_shipping_method` sm
			LEFT JOIN (SELECT `shipping_method_id`, `name` FROM `" . DB_PREFIX . "ms_shipping_method_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") smd
				ON (smd.shipping_method_id = sm.shipping_method_id)
			WHERE sm.`status` = 1"

			. (isset($data['name']) ? " AND smd.`name` LIKE '" . $this->db->escape($data['name']) . "%'" : "")
			. (isset($data['shipping_method_id']) ? " AND sm.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "");

		$shipping_methods = $this->db->query($sql);

		return (isset($data['shipping_method_id']) && $shipping_methods->num_rows) ? $shipping_methods->row : $shipping_methods->rows;
	}

	/**
	 * Gets delivery times list from MultiMerch `ms_shipping_delivery_time` table.
	 *
	 * @param	array	$data	Filter data.
	 * @return	array			List of delivery times.
	 */
	public function getDeliveryTimes($data = [])
	{
		$sql = "
			SELECT
				dt.`delivery_time_id`,
				dtd.`name`
			FROM `" . DB_PREFIX . "ms_shipping_delivery_time` dt
			LEFT JOIN (SELECT `delivery_time_id`, `name` FROM `" . DB_PREFIX . "ms_shipping_delivery_time_description` WHERE `language_id` = " . (int)$this->config->get('config_language_id') . ") dtd
				ON (dtd.delivery_time_id = dt.delivery_time_id)
			WHERE 1 = 1"

			. (isset($data['name']) ? " AND dtd.`name` LIKE '" . $this->db->escape($data['name']) . "%'" : "")
			. (isset($data['delivery_time_id']) ? " AND dt.`shipping_method_id` = " . (int)$data['shipping_method_id'] : "");

		$delivery_times = $this->db->query($sql);

		return (isset($data['delivery_time_id']) && $delivery_times->num_rows) ? $delivery_times->row : $delivery_times->rows;
	}

	/**
	 * Checks whether a shipping rule is available for passed address.
	 *
	 * @param	array	$rule		Shipping rule.
	 * @param	array	$address	Customer address.
	 * @return	bool				Whether the rule is available.
	 */
	public function isRuleAvailableForAddress($rule, $address)
	{
		foreach ($rule['destinations'] as $destination) {
			if ((int)$this->config->get('msconf_shipping_worldwide_destination_id') === (int)$destination['id']) {
				return true;
			}

			$sql = "
				SELECT DISTINCT
					`country_id`,
					`zone_id`
				FROM `" . DB_PREFIX . "zone_to_geo_zone`
				WHERE `geo_zone_id` = " . (int)$destination['id']
			;

			$geo_zone_data = $this->db->query($sql);

			foreach ($geo_zone_data->rows as $row) {
				if (
					(isset($address['country_id']) && (int)$row['country_id'] === (int)$address['country_id']) &&
					((isset($address['zone_id']) && (int)$row['zone_id'] === (int)$address['zone_id']) || (int)$row['zone_id'] === 0)
				) {
					return true;
				}
			}
		}

		return false;
	}
}
