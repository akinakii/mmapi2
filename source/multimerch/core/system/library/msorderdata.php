<?php
class MsOrderData extends Model {
	/**
	 * Opencart order states
	 */
	const STATE_PENDING = 1;
	const STATE_PROCESSING = 2;
	const STATE_COMPLETED = 3;
	const STATE_FAILED = 4;
	const STATE_CANCELLED = 5;

	/** orders **/
	public function getOrders($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					*,"
					// additional columns
					. (isset($cols['total_amount']) ? "
						(SELECT IFNULL(
							(SELECT SUM(opd2.seller_net_amt) as 'total' FROM `" . DB_PREFIX . "order_product` op JOIN `" . DB_PREFIX . "ms_order_product_data` opd2 ON (op.order_id = opd2.order_id AND op.product_id = opd2.product_id AND opd2.order_product_id IS NULL) WHERE op.order_id=o.order_id" . (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '') . "),
							(SELECT SUM(opd.seller_net_amt) as 'total' FROM `" . DB_PREFIX . "order_product` op JOIN `" . DB_PREFIX . "ms_order_product_data` opd ON (op.order_product_id = opd.order_product_id AND opd.order_product_id IS NOT NULL) WHERE op.order_id=o.order_id" . (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '') . ")
						)) as total_amount,
						" : "")

					// product names for filtering
					. (isset($cols['products']) ? "
						(SELECT GROUP_CONCAT(name)
						FROM " . DB_PREFIX . "order_product
						LEFT JOIN " . DB_PREFIX . "ms_order_product_data
						USING(order_id, product_id)
						WHERE order_id = o.order_id
						AND seller_id = mopd.seller_id) as products,
					" : "")
				."1
		FROM `" . DB_PREFIX . "order` o
		INNER JOIN `" . DB_PREFIX . "ms_order_product_data` mopd
		USING (order_id)
		WHERE seller_id = " . (int)$data['seller_id']
		. (isset($data['order_status']) && $data['order_status'] ? " AND o.order_status_id IN  (" .  $this->db->escape(implode(',', $data['order_status'])) . ")" : " AND o.order_status_id > '0' ")
		
		. $wFilters
		
		. " GROUP BY order_id HAVING 1 = 1 "
		
		. $hFilters
		
		. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
		. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");

		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];
		return $res->rows;
	}

	public function getOrderTotal($order_id, $data) {
		$sql = "SELECT IFNULL(
					(SELECT SUM(opd.seller_net_amt + opd.store_commission_flat + opd.store_commission_pct) as 'total' FROM `" . DB_PREFIX . "order_product` op JOIN `" . DB_PREFIX . "ms_order_product_data` opd ON (op.order_product_id = opd.order_product_id AND opd.order_product_id IS NOT NULL) WHERE op.order_id=" . (int)$order_id . (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '') . "),
					(SELECT SUM(opd2.seller_net_amt + opd2.store_commission_flat + opd2.store_commission_pct) as 'total' FROM `" . DB_PREFIX . "order_product` op JOIN `" . DB_PREFIX . "ms_order_product_data` opd2 ON (op.order_id = opd2.order_id AND op.product_id = opd2.product_id AND opd2.order_product_id IS NULL) WHERE op.order_id=" . (int)$order_id . (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '') . ")
				) as total";

		$res = $this->db->query($sql);

		return $res->row['total'];		
	}

	public function getOrderMsCouponTotal($order_id, $data = array()) {
		$result = $this->db->query("
			SELECT
				`amount` as `total`
			FROM `" . DB_PREFIX . "ms_coupon_history`
			WHERE `order_id` = " . (int)$order_id . "
				AND `suborder_id` = " . (int)$data['suborder_id']
		);

		return isset($result->row['total']) ? $result->row['total'] : false;
	}

	public function getOrderData($data = array()) {
		$sql = "SELECT
					opd.*,
					opsd.shipping_cost,
					COALESCE(opsd.shipping_method_name, '') as `shipping_method`
				FROM " . DB_PREFIX . "ms_order_product_data opd
				LEFT JOIN " . DB_PREFIX . "ms_order_product_shipping_data opsd
					ON (opd.order_id = opsd.order_id AND opd.product_id = opsd.product_id AND opd.order_product_id = opsd.order_product_id)
				WHERE 1 = 1"
				. (isset($data['product_id']) ? " AND opd.product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['order_product_id']) ? " AND opd.order_product_id =  " .  (int)$data['order_product_id'] : '')
				. (isset($data['order_id']) ? " AND opd.order_id =  " .  (int)$data['order_id'] : '');
		
		$res = $this->db->query($sql);

		return ($res->num_rows == 1 && isset($data['single']) ? $res->row : $res->rows);
	}

	public function getOrderComment($data = array()) {
		$sql = "SELECT *
				FROM " . DB_PREFIX . "ms_order_comment
				WHERE 1 = 1 "
			. (isset($data['product_id']) ? " AND product_id =  " .  (int)$data['product_id'] : '')
			. (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '')
			. (isset($data['order_id']) ? " AND order_id =  " .  (int)$data['order_id'] : '');

		$res = $this->db->query($sql);

		return isset($res->row['comment']) ? $res->row['comment'] : '';
	}

	// @todo 9.0: get rid of union. Must pass order_id and seller_id to make it working correctly, otherwise - duplicates error.
	public function getOrderProducts($data) {
		$sql = "SELECT
					*
				FROM (
					SELECT
						op.*,
						opd1.seller_id,
						opd1.seller_net_amt,
						opsd1.shipping_cost,
						COALESCE(opsd1.shipping_method_name, '') as `shipping_method`
					FROM " . DB_PREFIX . "order_product op
					LEFT JOIN (SELECT order_product_id, seller_id, seller_net_amt FROM `" . DB_PREFIX . "ms_order_product_data`) opd1
						ON (op.order_product_id = opd1.order_product_id AND opd1.order_product_id IS NOT NULL)
					LEFT JOIN (SELECT order_product_id, shipping_cost, shipping_method_name FROM `" . DB_PREFIX . "ms_order_product_shipping_data`) opsd1
						ON (op.order_product_id = opsd1.order_product_id AND opsd1.order_product_id IS NOT NULL)
				UNION
					SELECT
						op.*,
						opd2.seller_id,
						opd2.seller_net_amt,
						opsd2.shipping_cost,
						COALESCE(opsd2.shipping_method_name, '') as `shipping_method`
					FROM " . DB_PREFIX . "order_product op
					LEFT JOIN (SELECT order_id, product_id, order_product_id, seller_id, seller_net_amt FROM `" . DB_PREFIX . "ms_order_product_data`) opd2
						ON (op.order_id = opd2.order_id AND op.product_id = opd2.product_id AND opd2.order_product_id IS NULL)
					LEFT JOIN (SELECT order_id, product_id, order_product_id, shipping_cost, shipping_method_name FROM `" . DB_PREFIX . "ms_order_product_shipping_data`) opsd2
						ON (op.order_id = opsd2.order_id AND op.product_id = opsd2.product_id AND opsd2.order_product_id IS NULL)
				) AS u
				WHERE 1 = 1"

				. (isset($data['order_id']) ? " AND u.order_id =  " .  (int)$data['order_id'] : '')
				. (isset($data['seller_id']) ? " AND u.seller_id =  " .  (int)$data['seller_id'] : '')
				. (isset($data['product_id']) ? " AND u.product_id =  " .  (int)$data['product_id'] : '')

				. " ORDER BY u.order_product_id";

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function getOrderProductsNew($data) {
		$sql = "SELECT
					op.*,
					msopd.seller_id,
					msopd.store_commission_flat,
					msopd.store_commission_pct,
					msopd.seller_net_amt,
					msopsd.shipping_cost,
					COALESCE(msopsd.shipping_method_name, '') as `shipping_method`
				FROM " . DB_PREFIX . "order_product op
				LEFT JOIN `" . DB_PREFIX . "ms_order_product_data` msopd
					ON (op.order_id = msopd.order_id AND op.product_id = msopd.product_id AND op.order_product_id = msopd.order_product_id)
				LEFT JOIN `" . DB_PREFIX . "ms_order_product_shipping_data` msopsd
					ON (op.order_id = msopsd.order_id AND op.product_id = msopsd.product_id AND op.order_product_id = msopsd.order_product_id)
				WHERE 1 = 1"

			. (isset($data['order_id']) ? " AND op.order_id =  " .  (int)$data['order_id'] : '')
			. (isset($data['seller_id']) ? " AND msopd.seller_id =  " .  (int)$data['seller_id'] : '')
			. (isset($data['product_id']) ? " AND op.product_id =  " .  (int)$data['product_id'] : '');

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function addOrderProductData($order_id, $product_id, $data) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_order_product_data
				SET order_id = " . (int)$order_id . ",
					product_id = " . (int)$product_id . ",
					order_product_id = " . (int)$data['order_product_id'] . ",
					suborder_id = " . (int)$data['suborder_id'] . ",
					seller_id = " . (int)$data['seller_id'] . ",
					store_commission_flat = " . (float)$data['store_commission_flat'] . ",
					store_commission_pct = " . (float)$data['store_commission_pct'] . ",
					seller_net_amt = " . (float)$data['seller_net_amt'];

		$this->db->query($sql);
		return $this->db->getLastId();
	}

	/**
	 * Add shipping information for each order product
	 *
	 * @param $order_id
	 * @param $product_id
	 * @param $data
	 */
	public function addOrderProductShippingData($order_id, $product_id, $data) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_order_product_shipping_data
				SET order_id = " . (int)$order_id . ",
					product_id = " . (int)$product_id . ",
					order_product_id = " . (int)$data['order_product_id']

			. (isset($data['shipping_method_name']) ? ", shipping_method_name = '" . $this->db->escape($data['shipping_method_name']) . "'" : "")
			. (isset($data['shipping_cost']) ? ", shipping_cost = " . (float)$data['shipping_cost'] : "");

		$this->db->query($sql);
	}

	public function addOrderComment($order_id, $product_id, $data) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_order_comment
				SET order_id = " . (int)$order_id . ",
					product_id = " . (int)$product_id . ",
					seller_id = " . (int)$data['seller_id'] . ",
					comment = '" . $data['comment']. "'";

		$this->db->query($sql);

		$order_comment_id = $this->db->getLastId();
		return $order_comment_id;
	}

	/**
	 * Check if order was created by the customer. Customers can only see their orders.
	 *
	 * @param int $order_id
	 * @param int $customer_id
	 * @return mixed
	 */
	public function isOrderCreatedByCustomer($order_id = 0, $customer_id = 0) {
		$sql = "SELECT 1
				FROM `" . DB_PREFIX . "order`
				WHERE 
					order_id = " . (int)$order_id . "
					AND customer_id = " . (int)$customer_id;

		$res = $this->db->query($sql);

		return $res->num_rows;
	}

	/**
	 * Gets order product shipping cost.
	 *
	 * @param int	$product_id
	 * @param int	$order_id
	 * @param int 	$order_product_id
	 * @return float
	 */
	public function getOrderProductShippingCost($product_id, $order_id, $order_product_id) {
		$result = $this->db->query("
			SELECT
				`shipping_cost`
			FROM `" . DB_PREFIX . "ms_order_product_shipping_data`
			WHERE `shipping_cost` IS NOT NULL
				AND `product_id` = " . (int)$product_id . "
				AND `order_id` = " . (int)$order_id . "
				AND `order_product_id` = " . (int)$order_product_id
		);

		return $result->num_rows ? (float)$result->row['shipping_cost'] : 0;
	}

	/**
	 * Gets Opencart order state information by passed order_state_id.
	 *
	 * If $order_state_id is not set or set to 0, forces cache creation and returns array with all states-statuses linkings.
	 *
	 * @param	int		$order_state_id		Opencart order state id.
	 * @return	array						Order state info, containing linked oc statuses ids.
	 */
	public function getOrderStateData($order_state_id = 0) {
		$order_state_info = $this->cache->get('ms_order_state_' . $order_state_id);

		if (!$order_state_info) {
			foreach ($this->config->get('msconf_order_state') as $state_id => $statuses) {
				if($order_state_id && (int)$order_state_id === (int)$state_id) {
					$order_state_info = $statuses;
				} elseif (!$order_state_id) {
					$order_state_info[$state_id] = $statuses;
				}

				$this->cache->set('ms_order_state_' . $state_id, $statuses);
			}
		}

		return $order_state_info;
	}

	/**
	 * Gets Opencart order state id by passed order_status_id.
	 *
	 * @param	int		$order_status_id	Order status id.
	 * @return	int							Order state id.
	 */
	public function getOrderStateByStatusId($order_status_id) {
		$order_state_id = 0;

		$order_states_data = $this->getOrderStateData();

		foreach ($order_states_data as $state_id => $statuses) {
			if(in_array($order_status_id, $statuses))
				$order_state_id = $state_id;
		}

		return $order_state_id;
	}

	/**
	 * Gets MultiMerch product variation for order product.
	 *
	 * @param	int		$order_id			Order id.
	 * @param	int		$order_product_id	Order product id.
	 * @return	array						MSF variation data.
	 */
	public function getOrderProductMsfVariations($order_id, $order_product_id)
	{
		$result = $this->db->query("
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_order_product_msf_variation`
			WHERE `order_id` = " . (int)$order_id . "
				AND `order_product_id` = " . (int)$order_product_id
		);

		return $result->rows;
	}

	// FROM OPENCART

	public function ocGetOrder($order_id)
	{
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
			} else {
				$language_code = $this->config->get('config_language');
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => json_decode($order_query->row['custom_field'], true),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => json_decode($order_query->row['payment_custom_field'], true),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => json_decode($order_query->row['shipping_custom_field'], true),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified']
			);
		} else {
			return false;
		}
	}
}