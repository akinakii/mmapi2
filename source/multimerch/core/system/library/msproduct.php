<?php
class MsProduct extends Model {
	use \MultiMerch\Module\Traits\Stripe;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_DISABLED = 3;
	const STATUS_DELETED = 4;
	const STATUS_UNPAID = 5;
	const STATUS_IMPORTED = 6;
	
	const MS_PRODUCT_VALIDATION_NONE = 1;
	const MS_PRODUCT_VALIDATION_APPROVAL = 2;
	
	private $errors;
	

	public function getDefaultStockStatus() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX ."stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY stock_status_id ASC");
		return $query->row['stock_status_id'];

	}
	
	public function getSellerId($product_id) {
		$sql = "SELECT seller_id FROM " . DB_PREFIX . "ms_product
				WHERE product_id = " . (int)$product_id;
				
		$res = $this->db->query($sql);
		
		if (isset($res->row['seller_id']))
			return $res->row['seller_id'];
		else
			return 0;
	}
	
	public function isEnabled($product_id) {
		$sql = "SELECT	p.status as enabled,
				FROM `" . DB_PREFIX . "product` p
				WHERE p.product_id = " . (int)$product_id;

		$res = $this->db->query($sql);
		
		if (!$res->row['enabled'])
			return false;
		else
			return true;
	}

	public function isShippableByOC($product_id) {
		$sql = "SELECT	p.shipping as shippable
				FROM `" . DB_PREFIX . "product` p
				WHERE p.product_id = " . (int)$product_id;

		$res = $this->db->query($sql);

		if (!$res->row['shippable'])
			return false;
		else
			return true;
	}
	
	public function getProductImages($product_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC";
		$res = $this->db->query($sql);
		
		$images = array();
		foreach ($res->rows as $row) {
			$images[$row['product_image_id']] = $row;
		}
		
		return $images;
	}

	public function getProductOcCategories($product_id) {
		$sql = "SELECT group_concat(ptc.category_id separator ',') as category_id FROM `" . DB_PREFIX . "product_to_category` ptc WHERE product_id = " . (int)$product_id;
		$res = $this->db->query($sql);
		return $res->row['category_id'];
	}

	public function getProductMsCategories($product_id) {
		$sql = "SELECT group_concat(msp2c.ms_category_id separator ',') as ms_category_id FROM `" . DB_PREFIX . "ms_product_to_category` msp2c WHERE product_id = " . (int)$product_id;
		$res = $this->db->query($sql);
		return $res->row['ms_category_id'];
	}

	public function getProductDownloads($product_id) {
		$sql = "SELECT 	*
				FROM `" . DB_PREFIX . "download` d
				LEFT JOIN `" . DB_PREFIX . "product_to_download` pd
					USING(download_id)
				WHERE pd.product_id = " . (int)$product_id;
		$res = $this->db->query($sql);
		
		$downloads = array();
		foreach ($res->rows as $row) {
			$downloads[$row['download_id']] = $row;
		}
		
		return $downloads;
	}

	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
		
		return $query->rows;
	}
	
	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");
		
		return $query->rows;
	}

	public function getProductThumbnail($product_id) {
		$query = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		
		return $query->row;
	}

	public function getProductCommissionId($product_id) {
		$sql = "SELECT commission_id
				FROM `" . DB_PREFIX . "ms_product`
				WHERE product_id = " . (int)$product_id;
		$res = $this->db->query($sql);

		return $res->num_rows && isset($res->row['commission_id']) ? $res->row['commission_id'] : FALSE;
	}


	private function _saveImages($product_id, $images = array(), $data = array()) {
		foreach ($images as $key => $img) {
			$newImagePath = $this->MsLoader->MsFile->moveImage($img);
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($newImagePath, ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$key . "'");
		}
	}

	private function _saveDownloads($product_id, $downloads = array(), $data = array()) {
		foreach ($downloads as $key => $dl) {
			$newFile = $this->MsLoader->MsFile->moveDownload($dl['filename']);
			$fileMask = substr($newFile,0,strrpos($newFile,'.'));

			$this->db->query("INSERT INTO " . DB_PREFIX . "download SET filename = '" . $this->db->escape($newFile) . "', mask = '" . $this->db->escape($fileMask) . "'");
			$download_id = $this->db->getLastId();
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			$this->load->model('localisation/language');
			$languages = $this->model_localisation_language->getLanguages();
			foreach ($languages as  $language) {
				$language_id = $language['language_id'];
				$this->db->query("INSERT INTO " . DB_PREFIX . "download_description SET download_id = '" . (int)$download_id . "', name = '" . $this->db->escape($fileMask) . "', language_id = '" . (int)$language_id . "'");
			}
		}
	}

	private function _saveKeyword($product_id, $keyword) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($this->MsLoader->MsHelper->handleSlugDuplicate($keyword)) . "'");

		$this->cache->delete('multimerch_seo_url');
	}

	private function _saveDescriptions($product_id, $data = array()) {
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $l) {
			$language_id = $l['language_id'];
			$description = array(
				'product_name' => $data['product_name'][$language_id],
				'product_description' => $data['product_description'][$language_id],
				'product_meta_description' => (isset($data['product_meta_description'][$language_id]) AND $data['product_meta_description'][$language_id]) ? $data['product_meta_description'][$language_id] : $data['product_description'][$language_id],
				'product_meta_keyword' => isset($data['product_meta_keywords'][$language_id]) ? $data['product_meta_keywords'][$language_id] : '',
				'product_meta_title' => (isset($data['product_meta_title'][$language_id]) AND $data['product_meta_title'][$language_id]) ? $data['product_meta_title'][$language_id] : $data['product_name'][$language_id],
				'product_tags' => isset($data['product_tags'][$language_id]) ? $data['product_tags'][$language_id] : ''
			);

			$description['product_meta_description'] = $this->MsLoader->MsHelper->generateMetaDescription($description['product_meta_description']);

			$sql = "INSERT INTO " . DB_PREFIX . "product_description
					SET product_id = " . (int)$product_id . ",
						name = '" . $this->db->escape($description['product_name']) . "',
						description = '" . $this->db->escape($description['product_description']) . "',
						meta_description = '" . $this->db->escape($description['product_meta_description']) . "',
						meta_keyword = '" . $this->db->escape($description['product_meta_keyword']) . "',
						meta_title = '" . $this->db->escape($description['product_meta_title']) . "',
						tag = '" . $this->db->escape($description['product_tags']) . "',
						language_id = " . (int)$language_id;
			$this->db->query($sql);
		}
	}

	private function _updateDescriptions($product_id, $data = array()) {
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $l) {
			$language_id = $l['language_id'];
			$description = array(
				'product_name' => $data['product_name'][$language_id],
				'product_description' => $data['product_description'][$language_id],
				'product_meta_description' => isset($data['product_meta_description'][$language_id]) && $data['product_meta_description'][$language_id] ? $data['product_meta_description'][$language_id] : $data['product_description'][$language_id],
				'product_meta_keyword' => isset($data['product_meta_keywords'][$language_id]) ? $data['product_meta_keywords'][$language_id] : '',
				'product_meta_title' => isset($data['product_meta_title'][$language_id]) && $data['product_meta_title'][$language_id] ? $data['product_meta_title'][$language_id] : $data['product_name'][$language_id],
				'product_tags' => isset($data['product_tags'][$language_id]) ? $data['product_tags'][$language_id] : ''
			);

			$description['product_meta_description'] = $this->MsLoader->MsHelper->generateMetaDescription($description['product_meta_description']);

			$sql = "UPDATE " . DB_PREFIX . "product_description
					SET	name = '" . $this->db->escape($description['product_name']) . "',
						description = '" . $this->db->escape($description['product_description']) . "',
						meta_description = '" . $this->db->escape($description['product_meta_description']) . "',
						meta_keyword = '" . $this->db->escape($description['product_meta_keyword']) . "',
						meta_title = '" . $this->db->escape($description['product_meta_title']) . "',
						tag = '" . $this->db->escape($description['product_tags']) . "'
					WHERE product_id = " . (int)$product_id . "
					AND language_id = " . (int)$language_id;

			$this->db->query($sql);
		}


	}

	private function _saveOcCategories($product_id, $categories) {
		foreach ($categories as $id => $category_id) {
			$sql = "INSERT IGNORE INTO " . DB_PREFIX . "product_to_category
				SET product_id = " . (int)$product_id . ",
					category_id = " . (int)$category_id;
			$this->db->query($sql);
		}
	}

	private function _saveMsCategories($product_id, $ms_categories) {
		foreach ($ms_categories as $id => $category_id) {
			$sql = "INSERT IGNORE INTO " . DB_PREFIX . "ms_product_to_category
				SET product_id = " . (int)$product_id . ",
					ms_category_id = " . (int)$category_id;
			$this->db->query($sql);
		}
	}

	public function getProductStores($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store
					WHERE product_id = " . (int)$product_id);
		return $query ->rows;
	}

	private function _saveStores($product_id, $stores) {
		foreach ($stores as $store_id) {
			$sql = "INSERT INTO " . DB_PREFIX . "product_to_store
					SET product_id = " . (int)$product_id . ",
						store_id = " . (int)$store_id;
			$this->db->query($sql);
		}
	}

	private function _saveOptions($product_id, $options) {
		foreach ($options as $product_option) {
			// unset sample
			if (isset($product_option['product_option_value'][0])) unset($product_option['product_option_value'][0]);

			// get type
			$o = $this->MsLoader->MsOption->getOptions(array('option_id' => $product_option['option_id'], 'single' => 1));
			if (!$o) continue; else { $product_option['type'] = $o['type']; }

			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (isset($product_option['required']) ? (int)$product_option['required'] : 0) . "'");

				$product_option_id = $this->db->getLastId();

				if (isset($product_option['product_option_value']) && count($product_option['product_option_value']) > 0 ) {
					foreach ($product_option['product_option_value'] as $product_option_value) {
						$product_option_value['price_prefix'] = ($product_option_value['price_prefix'] == '-' ? '-' : '+');
						//$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (isset($product_option_value['subtract']) ? (int)$product_option_value['subtract'] : 0) . "', price = '" . (float)$this->MsLoader->MsHelper->uniformDecimalPoint($product_option_value['price']) . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "'");
					}
				}else{
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_option_id = '".$product_option_id."'");
				}
			} else {
				//$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['option_value']) . "', required = '" . (int)$product_option['required'] . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '', required = '" . (isset($product_option['required']) ? (int)$product_option['required'] : 0) . "'");
			}
		}
	}

	private function _saveSpecials($product_id, $specials = array()) {
		$customer_group_id = (int)$this->config->get('config_customer_group_id');

		foreach ($specials as $product_special) {
			if (isset($product_special['customer_group_id'])) $customer_group_id = (int)$product_special['customer_group_id'];
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . $customer_group_id . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$this->MsLoader->MsHelper->uniformDecimalPoint($product_special['price']) . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
		}
	}

	private function _saveRelated($product_id, $related = array()) {
		foreach ($related as $related_id) {
			$this->db->query(
				"INSERT INTO " . DB_PREFIX . "product_related"
				. " SET product_id = '" . (int)$product_id . "', "
				. " related_id = '" . (int)$related_id . "'"
			);
		}
	}

	private function _saveDiscounts($product_id, $discounts = array()) {
		$customer_group_id = (int)$this->config->get('config_customer_group_id');
		foreach ($discounts as $product_discount) {
			if (isset($product_discount['customer_group_id'])) $customer_group_id = (int)$product_discount['customer_group_id'];
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . $customer_group_id . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$this->MsLoader->MsHelper->uniformDecimalPoint($product_discount['price']) . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
		}
	}

	private function _saveFilters($product_id, $filters = array()) {
		foreach ($filters as $filter_id) {
			$this->db->query(
				"INSERT INTO " . DB_PREFIX . "product_filter"
				. " SET product_id = '" . (int) $product_id . "',"
				. " filter_id = '" . (int) $filter_id . "'"
			);
		}
	}

	private function _saveAttributes($product_id, $attributes = array()) {
		// unset sample
		if (isset($attributes[0])) unset($attributes[0]);

		foreach ($attributes as $product_attribute) {
			if ($product_attribute['attribute_id']) {
				foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($product_attribute_description['text']) . "'");
				}
			}
		}
	}

	private function _saveShipping($product_id, $shipping = array()) {
		$this->load->controller('multimerch/shipping/save', array_merge($shipping, ['product_id' => $product_id]));
	}

	private function _saveCustomFields($product_id, $custom_fields = []) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_custom_field` WHERE `product_id` = '" . (int)$product_id . "'");

		foreach ($custom_fields as $custom_field_id => $ms_cf) {
			$this->MsLoader->MsCustomField->createOrUpdateProductCustomField(
				array_merge($ms_cf, [
					'product_id' => $product_id,
					'custom_field_id' => $custom_field_id
				])
			);
		}
	}

	public function saveProductMsfAttributes($product_id, $product_msf_attributes)
	{
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_id` WHERE `product_id` = " . (int)$product_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_attribute_value_text` WHERE `product_id` = " . (int)$product_id);

		foreach ($product_msf_attributes as $msf_attribute_id => $values) {
			if (!empty($values['id_values'])) {
				foreach ($values['id_values'] as $value_id) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_product_msf_attribute_value_id`
						SET `product_id` = " . (int)$product_id . ",
							`msf_attribute_id` = " . (int)$msf_attribute_id . ",
							`msf_attribute_value_id` = " . (int)$value_id
					);
				}
			} elseif (!empty($values['text_values'])) {
				foreach ($values['text_values'] as $language_id => $value_text) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_product_msf_attribute_value_text`
						SET `product_id` = " . (int)$product_id . ",
							`msf_attribute_id` = " . (int)$msf_attribute_id . ",
							`language_id` = " . (int)$language_id . ",
							`value` = '" . $this->db->escape($value_text) . "'
					");
				}
			}
		}
	}

	public function saveProductMsfVariations($product_id, $product_msf_variations)
	{
		$existing_variations = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_product_msf_variation` WHERE `product_id` = " . (int)$product_id)->rows;

		// Cleanup previous variations
		foreach ($existing_variations as $variation) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_variation` WHERE `id` = " . (int)$variation['id']);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_product_msf_variation_matrix` WHERE `ms_product_msf_variation_id` = " . (int)$variation['id']);
		}

		foreach ($product_msf_variations as $key => $row) {
			$sql = "
				INSERT INTO `" . DB_PREFIX . "ms_product_msf_variation`
				SET `seller_id` = " . (int)$this->getSellerId($product_id) . ",
					`product_id` = " . (int)$product_id . ",
					`status` = " . (int)$row['status']

				. ((isset($row['quantity']) && $row['quantity'] !== '') ? ", `quantity` = " . (int)$row['quantity'] : "")
				. ((isset($row['price']) && $row['price'] !== '') ? ", `price` = " . (float)$row['price'] : "")
				. (isset($row['sku']) ? ", `sku` = '" . $this->db->escape($row['sku']) . "'" : "")
				. (isset($row['image']) ? ", `image` = '" . $this->db->escape($row['image']) . "'" : "")
			;

			$this->db->query($sql);

			$ms_product_msf_variation_id = $this->db->getLastId();

			foreach (explode('_', $key) as $row_key) {
				list($msf_variation_id, $msf_variation_value_id) = explode('-', $row_key);

				$sql_2 = "
					INSERT INTO `" . DB_PREFIX . "ms_product_msf_variation_matrix`
					SET `ms_product_msf_variation_id` = " . (int)$ms_product_msf_variation_id . ",
						`msf_variation_id` = " . (int)$msf_variation_id . ",
						`msf_variation_value_id` = " . (int)$msf_variation_value_id
				;

				$this->db->query($sql_2);
			}
		}
	}

	private function _createProduct($data = array()) {
		$sql = "INSERT INTO " . DB_PREFIX . "product
				SET model = '" . $this->db->escape(isset($data['product_model']) ? $data['product_model'] : reset($data['product_name'])) . "',
				    minimum = '" . (isset($data['minimum']) ? (int)$data['minimum'] : 1) . "',
				    sku = '" . $this->db->escape(isset($data['product_sku']) ? $data['product_sku'] : '') . "',
				    upc = '" . $this->db->escape(isset($data['product_upc']) ? $data['product_upc'] : '') . "',
				    ean = '" . $this->db->escape(isset($data['product_ean']) ? $data['product_ean'] : '') . "',
				    jan = '" . $this->db->escape(isset($data['product_jan']) ? $data['product_jan'] : '') . "',
				    isbn = '" . $this->db->escape(isset($data['product_isbn']) ? $data['product_isbn'] : '') . "',
				    mpn = '" . $this->db->escape(isset($data['product_mpn']) ? $data['product_mpn'] : '') . "',
				    manufacturer_id = '" . (isset($data['product_manufacturer_id']) ? (int)$data['product_manufacturer_id'] : 0 ) . "',
				    price = " . (float)$this->MsLoader->MsHelper->uniformDecimalPoint($data['product_price']) . ",
				    weight = '" . (isset($data['weight']) ? (float)$data['weight'] : 0) . "',
				    weight_class_id = '" . (isset($data['weight_class_id']) ? (int)$data['weight_class_id'] : 0) . "',
				    length = '" . (isset($data['length']) ? (float)$data['length'] : 0) . "',
				    width = '" . (isset($data['width']) ? (float)$data['width'] : 0) . "',
				    height = '" . (isset($data['height']) ? (float)$data['height'] : 0) . "',
				    length_class_id = '" . (isset($data['length_class_id']) ? (int)$data['length_class_id'] : 0) . "',
					image = '" .  $this->db->escape(isset($data['product_thumbnail'])? $this->MsLoader->MsFile->moveImage($data['product_thumbnail']) : '')  . "',
					subtract = " . (isset($data['product_subtract']) ? (int)$data['product_subtract'] : 1) . ",
					tax_class_id = '" . (isset($data['product_tax_class_id']) ? (int)$data['product_tax_class_id'] : 0) . "',
					stock_status_id = '" . (isset($data['product_stock_status_id']) ? (int)$data['product_stock_status_id'] : (int)$this->getDefaultStockStatus()) . "',
					date_available = '" . $this->db->escape(isset($data['product_date_available']) ? $data['product_date_available'] : date('Y-m-d', time() - 86400)) . "',
					quantity = " . (int)$data['product_quantity'] . ",
					shipping = " . (int)$data['product_enable_shipping'] . ",
					status = " . (int)$data['enabled'] . ",
					date_added = NOW(),
					date_modified = NOW()";

		$this->db->query($sql);
		return $this->db->getLastId();
	}

	private function _updateProduct($product_id, $data = array()) {
        $included_field_sql = '';
        isset($data['product_model']) ? $included_field_sql .= " model = '" . $this->db->escape($data['product_model']) . "',"  : '';
        isset($data['minimum']) ? $included_field_sql .= " minimum = '" . (int)$data['minimum'] . "'," : '';
        isset($data['product_sku']) ? $included_field_sql .= " sku = '" . $this->db->escape($data['product_sku']) . "',"  : '';
        isset($data['product_upc']) ? $included_field_sql .= " upc = '" . $this->db->escape($data['product_upc']) . "',"  : '';
        isset($data['product_ean']) ? $included_field_sql .= " ean = '" . $this->db->escape($data['product_ean']) . "',"  : '';
        isset($data['product_jan']) ? $included_field_sql .= " jan = '" . $this->db->escape($data['product_jan']) . "',"  : '';
        isset($data['product_isbn']) ? $included_field_sql .= " isbn = '" . $this->db->escape($data['product_isbn']) . "',"  : '';
        isset($data['product_mpn']) ? $included_field_sql .= " mpn = '" . $this->db->escape($data['product_mpn']) . "',"  : '';
        isset($data['product_manufacturer_id']) ? $included_field_sql .= " manufacturer_id = '" . (int)$data['product_manufacturer_id'] . "',"  : '';
        isset($data['product_tax_class_id']) ? $included_field_sql .= " tax_class_id = '" . $this->db->escape($data['product_tax_class_id']) . "',"  : '';
        isset($data['product_stock_status_id']) ? $included_field_sql .= " stock_status_id = '" . (int)$data['product_stock_status_id'] . "',"  : '';
        isset($data['product_date_available']) ? $included_field_sql .= " date_available = '" . $this->db->escape($data['product_date_available']) . "',"  : '';

		isset($data['weight']) ? $included_field_sql .= " weight = '" . $this->db->escape($data['weight']) . "',"  : '';
		isset($data['weight_class_id']) ? $included_field_sql .= " weight_class_id = '" . $this->db->escape($data['weight_class_id']) . "',"  : '';
		isset($data['length']) ? $included_field_sql .= " length = '" . $this->db->escape($data['length']) . "',"  : '';
		isset($data['width']) ? $included_field_sql .= " width = '" . $this->db->escape($data['width']) . "',"  : '';
		isset($data['height']) ? $included_field_sql .= " height = '" . $this->db->escape($data['height']) . "',"  : '';
		isset($data['length_class_id']) ? $included_field_sql .= " length_class_id = '" . $this->db->escape($data['length_class_id']) . "',"  : '';

		$sql = "UPDATE " . DB_PREFIX . "product
				SET" . $included_field_sql . " price = " . (float)$this->MsLoader->MsHelper->uniformDecimalPoint($data['product_price']) . ",
					status = " . (int)$data['enabled'] . ",
					subtract = " . (isset($data['product_subtract']) ? (int)$data['product_subtract'] : 1) . ",
					quantity = " . (int)$data['product_quantity'] . ",
					shipping = " . (int)$data['product_enable_shipping'] . ",
					date_modified = NOW()
				WHERE product_id = " . (int)$product_id;

		$this->db->query($sql);
	}


	private function _updateImages($product_id, $data) {
		$old_thumbnail = $this->getProductThumbnail($product_id);
		$old_images = $this->getProductImages($product_id);

		// thumbnail
		if (isset($data['product_thumbnail'])) {
			$keep_thumbnail = false;
			foreach ($old_images as $old_image) {
				if ($old_image['image'] == $data['product_thumbnail']) {
					$keep_thumbnail = true;
					$thumbnail = $old_image['image'];
					break;
				}
			}

			if (!$keep_thumbnail) {
				if ($old_thumbnail['image'] == $data['product_thumbnail']) {
					$thumbnail = $old_thumbnail['image'];
				} else {
					$this->MsLoader->MsFile->deleteImage($old_thumbnail['image']);
					$thumbnail = $this->MsLoader->MsFile->moveImage($data['product_thumbnail']);
				}
			}
		} else {
			$this->MsLoader->MsFile->deleteImage($old_thumbnail['image']);
			$thumbnail = '';
		}

		$sql = "UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($thumbnail) . "' WHERE product_id = " . (int)$product_id;
		$this->db->query($sql);

		// images
		if (isset($data['product_images'])) {

			$new_images = $data['product_images'];

			foreach($old_images as $k => $old_image) {
				$key = array_search($old_image['image'], $data['product_images']);
				if ($key !== FALSE) {
					unset($old_images[$k]);
					unset($data['product_images'][$key]);
				}
			}

			foreach ($data['product_images'] as $key => $product_image) {
				$newImagePath = $this->MsLoader->MsFile->moveImage($product_image);
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($newImagePath, ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)array_search($product_image, $new_images) . "'");
			}

			$i = 0;
			foreach ($new_images as $key => $image) {
				$this->db->query("UPDATE " . DB_PREFIX . "product_image SET sort_order = " . $i++ . " WHERE product_id = '" . (int)$product_id . "' AND image = '" . $this->db->escape(html_entity_decode($image, ENT_QUOTES, 'UTF-8')) . "'");
			}
		}

		// delete old images
		foreach($old_images as $old_image) {
			if ($old_image['image'] != $thumbnail) {
				$this->MsLoader->MsFile->deleteImage($old_image['image']);
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' AND product_image_id = '" . (int)$old_image['product_image_id'] . "'");
		}
	}

	private function _updateDownloads($product_id, $data){
		$old_downloads = $this->getProductDownloads($product_id);
		$languages = $this->model_localisation_language->getLanguages();

		if (isset($data['product_downloads'])) {
			foreach ($data['product_downloads'] as $key => $dl) {
				if (!empty($dl['download_id'])) {
					if (!empty($dl['filename'])) {
						// update download #download_id:
						$newFile = $this->MsLoader->MsFile->moveDownload($dl['filename']);
						$fileMask = substr($newFile,0,strrpos($newFile,'.'));

						$this->db->query("UPDATE " . DB_PREFIX . "download SET filename = '" . $this->db->escape($newFile) . "', mask = '" . $this->db->escape($fileMask) . "' WHERE download_id = '" . (int)$dl['download_id'] . "'");


						foreach ($languages as $l) {
							$language_id = $l['language_id'];
							$this->db->query("UPDATE " . DB_PREFIX . "download_description SET name = '" . $this->db->escape($fileMask) . "' WHERE download_id = '" . (int)$dl['download_id'] . "' AND language_id = '" . (int)$language_id . "'");
						}

						// $this->MsLoader->MsFile->deleteDownload($old_downloads[$dl['download_id']]['filename']);
					} else {
						// do nothing
					}

					// don't remove the download
					unset($old_downloads[$dl['download_id']]);
				} else if (!empty($dl['filename'])) {
					// add new download
					$newFile = $this->MsLoader->MsFile->moveDownload($dl['filename']);
					$fileMask = substr($newFile,0,strrpos($newFile,'.'));

					$this->db->query("INSERT INTO " . DB_PREFIX . "download SET filename = '" . $this->db->escape($newFile) . "', mask = '" . $this->db->escape($fileMask) . "'");
					$download_id = $this->db->getLastId();
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");

					foreach ($languages as $l) {
							$language_id = $l['language_id'];
						$this->db->query("INSERT INTO " . DB_PREFIX . "download_description SET download_id = '" . (int)$download_id . "', name = '" . $this->db->escape($fileMask) . "', language_id = '" . (int)$language_id . "'");
					}
				}
			}
		}

		if (!empty($old_downloads)) {
			foreach($old_downloads as $old_download) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "download WHERE download_id ='" . (int)$old_download['download_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "download_description WHERE download_id ='" . (int)$old_download['download_id'] . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE download_id ='" . (int)$old_download['download_id'] . "'");
				$this->MsLoader->MsFile->deleteDownload($old_download['filename']);
			}
		}
	}

	/*
	 *
	 *
	 *
	 */

	public function saveProduct($data) {
		// create main record
		$product_id = $this->_createProduct($data);

		// keyword
		if(isset($data['keyword']) && !empty($data['keyword'])) $this->_saveKeyword($product_id, $data['keyword']);

		// categories
		if (isset($data['product_category'])) $this->_saveOcCategories($product_id, $data['product_category']);

		// seller categories
		if (isset($data['product_ms_category'])) $this->_saveMsCategories($product_id, $data['product_ms_category']);

		// stores (default by default)
		if (!isset($data['stores'])){
			$data['stores'] = array($this->config->get('config_store_id'));
		}
		$this->_saveStores($product_id, $data['stores']);

		// images
		if (isset($data['product_images'])) $this->_saveImages($product_id, $data['product_images'], $data);

		// downloads
		if (isset($data['product_downloads'])) $this->_saveDownloads($product_id, $data['product_downloads'], $data);

		// options
		if (isset($data['product_option'])) $this->_saveOptions($product_id, $data['product_option']);

		// specials
		if (isset($data['product_specials'])) $this->_saveSpecials($product_id, $data['product_specials']);

		// discounts
		if (isset($data['product_discounts'])) $this->_saveDiscounts($product_id, $data['product_discounts']);

		// filters
		if (isset($data['product_filter'])) $this->_saveFilters($product_id, $data['product_filter']);

		// related products
		if (isset($data['product_related'])) $this->_saveRelated($product_id, $data['product_related']);

		// attributes
		if (isset($data['product_attribute'])) $this->_saveAttributes($product_id, $data['product_attribute']);

		// shipping
		$this->_saveShipping($product_id, isset($data['shipping']) ? $data['shipping'] : ['rules' => []]);

		// custom fields
		if(isset($data['product_cf'])) $this->_saveCustomFields($product_id, $data['product_cf']);

		// digital product
		if(isset($data['product_is_digital'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET shipping = '" . (($data['product_is_digital'] && $this->config->get('msconf_allow_digital_products')) ? 0 : 1) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		// MSF attributes
		if ($this->config->get('msconf_msf_attribute_enabled')) {
			$this->saveProductMsfAttributes($product_id, isset($data['product_msf_attribute']) ? $data['product_msf_attribute'] : []);
		}

		// MSF variations
		if ($this->config->get('msconf_msf_variation_enabled')) {
			$this->saveProductMsfVariations($product_id, isset($data['product_msf_variation']) ? $data['product_msf_variation'] : []);
		}

		// descriptions, metas
		$this->_saveDescriptions($product_id, $data);

		$sql = "INSERT INTO " . DB_PREFIX . "ms_product
				SET product_id = " . (int)$product_id . ",
					seller_id = " . (int)$this->registry->get('customer')->getId() . ",
					product_status = " . (int)$data['product_status'] . ",
					product_approved = " . (int)$data['product_approved']. ",
					primary_oc_category = ".(int)$data['primary_oc_category'] . ",
					primary_ms_category = ".(int)$data['primary_ms_category']
					. ( (isset($data['list_until']) && $data['list_until'] != NULL ) ? ", list_until = '" . $this->db->escape($data['list_until']) . "'" : "");

		$this->db->query($sql);

		// Report usage (only if status is `Active`) if seller is subscribed to per-product Stripe plan
		if ($this->config->get('ms_stripe_connect_status') && (int)MsProduct::STATUS_ACTIVE === (int)$data['product_status'])
			$this->reportStripePerProductPlanUsage($product_id);

		// delete OC product cache
		$this->registry->get('cache')->delete('product');
		
		return $product_id;
	}	

	public function editProductDescription($product_id, $data = array()){
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $l) {
			$language_id = $l['language_id'];

			$sql = "UPDATE " . DB_PREFIX . "product_description
					SET	description = '" . $this->db->escape($data[$language_id]) . "'
					WHERE product_id = " . (int)$product_id . "
					AND language_id = " . (int)$language_id;

			$this->db->query($sql);
		}
	}
	
	public function editProduct($data) {
		$product_id = $data['product_id'];

		// delete existing records
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE 'product_id=" . (int)$product_id . "'");

		if (isset($data['product_ms_category'])) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product_to_category WHERE product_id = '" . (int)$product_id . "'");
		}

		// update main record
		$this->_updateProduct($product_id, $data);

		$sql = "UPDATE " . DB_PREFIX . "ms_product
				SET product_status = " . (int)$data['product_status'] . ",
					product_approved = " . (int)$data['product_approved']
					. (isset($data['primary_oc_category']) ? ", primary_oc_category = ".(int)$data['primary_oc_category'] : "")
					. (isset($data['primary_ms_category']) ? ", primary_ms_category = ".(int)$data['primary_ms_category'] : "")
					. ", primary_ms_category = ".(int)$data['primary_ms_category'] . "
				WHERE product_id = " . (int)$product_id;
		$this->db->query($sql);

		// keyword
		if(isset($data['keyword']) && !empty($data['keyword'])) $this->_saveKeyword($product_id, $data['keyword']);

		// categories
		if (isset($data['product_category'])) $this->_saveOcCategories($product_id, $data['product_category']);

		// seller categories
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product_to_category WHERE product_id = '" . (int)$product_id . "'");
		if (isset($data['product_ms_category'])) $this->_saveMsCategories($product_id, $data['product_ms_category']);

		// options
		if (isset($data['product_attribute'])) $this->_saveAttributes($product_id, $data['product_attribute']);

		// options
		if (isset($data['product_option'])) $this->_saveOptions($product_id, $data['product_option']);

		// specials
		if (isset($data['product_specials'])) $this->_saveSpecials($product_id, $data['product_specials']);

		// discounts
		if (isset($data['product_discounts'])) $this->_saveDiscounts($product_id, $data['product_discounts']);

		// filters
		if (isset($data['product_filter'])) $this->_saveFilters($product_id, $data['product_filter']);

		// related products
		if (isset($data['product_related'])) $this->_saveRelated($product_id, $data['product_related']);

		// shipping
		$this->_saveShipping($product_id, isset($data['shipping']) ? $data['shipping'] : ['rules' => []]);

		// custom fields
		if(isset($data['product_cf'])) $this->_saveCustomFields($product_id, $data['product_cf']);

		// digital product
		if(isset($data['product_is_digital'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET shipping = '" . (($data['product_is_digital'] && $this->config->get('msconf_allow_digital_products')) ? 0 : 1) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		// MSF attributes
		if ($this->config->get('msconf_msf_attribute_enabled')) {
			$this->saveProductMsfAttributes($product_id, isset($data['product_msf_attribute']) ? $data['product_msf_attribute'] : []);
		}

		// MSF variations
		if ($this->config->get('msconf_msf_variation_enabled')) {
			$this->saveProductMsfVariations($product_id, isset($data['product_msf_variation']) ? $data['product_msf_variation'] : []);
		}

		// descriptions, metas
		$this->_updateDescriptions($product_id, $data);

		// images & thumbnail
		$this->_updateImages($product_id, $data);

		// downloads
		$this->_updateDownloads($product_id, $data);

		// delete OC product cache
		$this->registry->get('cache')->delete('product');
		
		return $product_id;
	}
	
	public function hasDownload($product_id, $download_id) {
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "product_to_download`
				WHERE product_id = " . (int)$product_id . " 
				AND download_id = " . (int)$download_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['total'];
	}
	
	public function getDownload($download_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row;
	}
	
	public function productOwnedBySeller($product_id, $seller_id) {
		$sql = "SELECT COUNT(*) as 'total'
				FROM `" . DB_PREFIX . "ms_product`
				WHERE seller_id = " . (int)$seller_id . " 
				AND product_id = " . (int)$product_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['total'];			
	}
	
	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product WHERE product_id = '" . (int)$product_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id. "'");

		// delete unpaid listing fee requests for this product
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_invoice` WHERE `type` = 'listing' AND `object_type` = 'product' AND `object_id` = " . (int)$product_id . " AND `status` = " . (int)\MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID);

		// delete related questions
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_question WHERE product_id = '" . (int)$product_id . "'");

		// delete related conversations
		$conversations = $this->db->query("SELECT conversation_id FROM `" . DB_PREFIX . "ms_conversation_to_product` WHERE product_id = '" . (int)$product_id . "'");
		foreach ($conversations->rows as $conversation) {
			$this->MsLoader->MsConversation->deleteConversation($conversation['conversation_id']);
		}

		// delete related MsCategories
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product_to_category WHERE product_id = '" . (int)$product_id . "'");

		// delete MsShipping
		$this->MsLoader->MsShipping->deletePerProductRule($this->MsLoader->MsProduct->getSellerId($product_id), $product_id);

		// delete MsCustomFields
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product_custom_field WHERE product_id = '" . (int)$product_id . "'");

		// delete product from MsCoupons
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_coupon_product` WHERE `product_id` = '" . (int)$product_id . "'");

		// delete product_id from review table
		$this->db->query("UPDATE " . DB_PREFIX . "ms_review SET product_id = NULL WHERE product_id = '" . (int)$product_id . "'");

		$this->registry->get('cache')->delete('product');

		$this->cache->delete('multimerch_seo_url');
	}
	
	/*****************************************/

	public function getTotalProducts($data) {

		// fix for counting different language translations
		// as separate products
		$sql = "
			SELECT COUNT(DISTINCT product_id) as total
			FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "ms_product mp
				USING (product_id)
			LEFT JOIN " . DB_PREFIX . "ms_seller ms
				USING (seller_id)
			LEFT JOIN " . DB_PREFIX . "product_description pd
				USING(product_id)
			WHERE 1 = 1 "
			. (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '')
			. (isset($data['search']) ? " AND pd.name LIKE '%" .  $this->db->escape($data['search']) . "%'" : '')
			. (isset($data['product_status']) ? " AND product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")" : '')
			. (isset($data['oc_status']) ? " AND p.status =  " .  (int)$data['oc_status'] : '')
			. (isset($data['available']) ? " AND p.date_available <= NOW()" : '')
			. (isset($data['enabled']) ? " AND status =  " .  (int)$data['enabled'] : '');

		$res = $this->db->query($sql);

		return $res->row['total'];
	}

	public function getTotalProductViews($data) {
		$sql = "
			SELECT SUM(p.viewed) as total
			FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "ms_product mp
				USING (product_id)
			LEFT JOIN " . DB_PREFIX . "ms_seller ms
				USING (seller_id)
			WHERE 1 = 1 "
			. (isset($data['seller_id']) ? " AND seller_id =  " .  (int)$data['seller_id'] : '');

		$res = $this->db->query($sql);

		return $res->row['total'];
	}
	
	//todo
	public function getProduct($product_id) {
		$sql = "SELECT 	p.price,
		                p.model, p.sku, p.upc, p.ean, p.jan, p.isbn, p.mpn, p.minimum,
		                p.manufacturer_id, p.tax_class_id, p.subtract, p.stock_status_id, p.date_available,
		                p.weight, p.weight_class_id, p.length, p.width, p.height, p.length_class_id,
						p.product_id as 'product_id',
						mp.product_status as 'mp.product_status',
						p.status as enabled,
						p.image as thumbnail,
						p.shipping as shipping,
						p.quantity as quantity,
						(SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "' LIMIT 1) AS keyword,
						mp.product_status,
						mp.product_approved,
						mp.commission_id,
						mp.primary_oc_category,
						mp.primary_ms_category,
						mp.seller_id
				FROM `" . DB_PREFIX . "product` p
				LEFT JOIN `" . DB_PREFIX . "ms_product` mp
					ON p.product_id = mp.product_id
				WHERE p.product_id = " . (int)$product_id;
		$res = $this->db->query($sql);

		if (!$res->num_rows) return FALSE;

		$sql = "SELECT pd.*,
					   pd.description as 'pd.description'
				FROM " . DB_PREFIX . "product_description pd
				WHERE pd.product_id = " . (int)$product_id . "
				GROUP BY language_id";

		$descriptions = $this->db->query($sql);
		$product_description_data = array();
		foreach ($descriptions->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'tags'      => $result['tag'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_title'     => $result['meta_title'],
				'meta_description' => $result['meta_description']
			);
		}

		$res->row['languages'] = $product_description_data;
		return $res->row;
	}

	// Compact product information
	public function getProductCompact($product_id, $data = [])
	{
		$sql = "
			SELECT
				p.product_id,
				p.image,
				pd.name,
				pd.description,
				p.price,
				p.minimum,
				(SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
				p.tax_class_id,
				p.quantity,
				msp.seller_id,
				mss.nickname as `seller_nickname`,
				mss.avatar as `seller_avatar`,
				msr.rating,
				msr.total_reviews
			FROM `" . DB_PREFIX . "product` p
			LEFT JOIN (SELECT product_id, `name`, description FROM `" . DB_PREFIX . "product_description` WHERE language_id = " . (int)$this->config->get('config_language_id') . ") pd
				ON (pd.product_id = p.product_id)
			LEFT JOIN (SELECT product_id, seller_id FROM `" . DB_PREFIX . "ms_product`) msp
				ON (msp.product_id = p.product_id)
			LEFT JOIN (SELECT seller_id, nickname, avatar FROM `" . DB_PREFIX . "ms_seller`) mss
				ON (mss.seller_id = msp.seller_id)
			LEFT JOIN (SELECT product_id, COUNT(1) as `total_reviews`, AVG(rating) as `rating` FROM `" . DB_PREFIX . "ms_review` WHERE status = '1' GROUP BY product_id) msr
				ON (msr.product_id = p.product_id)
			WHERE p.product_id = " . (int)$product_id

			. (isset($data['oc_status']) ? " AND p.status = " . (int)$data['oc_status'] : "")
		;

		$result = $this->db->query($sql);

		return $result->row;
	}

	public function getProducts($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`p.date_added`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		// Debit order statuses
		$msconf_debit_order_statuses = $this->config->get('msconf_debit_order_statuses');

		// todo validate order parameters
		$sql = "SELECT
					SQL_CALC_FOUND_ROWS "
					// additional columns
					. (isset($cols['product_earnings']) ? "
						IFNULL((SELECT
							SUM(seller_net_amt) AS seller_total
						FROM " . DB_PREFIX . "order_product op
						LEFT JOIN (SELECT order_product_id, seller_net_amt FROM `" . DB_PREFIX . "ms_order_product_data`) mopd
							ON (op.order_product_id = mopd.order_product_id)
						LEFT JOIN (SELECT order_id, order_status_id FROM `" . DB_PREFIX . "order`) o
							ON (o.order_id = op.order_id)
						WHERE op.product_id = p.product_id
							AND o.order_status_id NOT IN (0," . implode(',', !empty($msconf_debit_order_statuses['oc']) ? $msconf_debit_order_statuses['oc'] : array(-1)) . ")), 0) as product_earnings,
					" : "")

					. (isset($cols['product_sales']) ? "
						(SELECT
							SUM(op.quantity) as total
						FROM " . DB_PREFIX . "order_product op
						LEFT JOIN (SELECT product_id, seller_id FROM ". DB_PREFIX . "ms_product) msp
							ON (msp.product_id = op.product_id)
						LEFT JOIN (SELECT order_id, order_status_id FROM `" . DB_PREFIX . "order`) o
							ON (o.order_id = op.order_id)
						WHERE op.product_id = p.product_id
							AND msp.seller_id = ms.seller_id
							AND o.order_status_id NOT IN (0," . implode(',', !empty($msconf_debit_order_statuses['oc']) ? $msconf_debit_order_statuses['oc'] : array(-1)) . ")) as number_sold,
					" : "")
					
					."p.product_id as 'product_id',
					p.image as 'p.image',
					p.price as 'p.price',
					p.quantity as 'p.quantity',
					p.model as 'p.model',
					p.status as 'p.status',
					p.tax_class_id as 'p.tax_class_id',
					pd.name as 'pd.name',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					ms.avatar as 'ms.avatar',
					mp.product_status as 'mp.product_status',
					mp.product_approved as 'mp.product_approved',
					mp.list_until as 'mp.list_until',
					p.date_added as 'p.date_added',
					CASE WHEN p.date_modified THEN p.date_modified ELSE '' END as 'p.date_modified',
					(SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
					pd.description as 'pd.description'"

					. (isset($cols['product_ms_rating']) ? "
						, msr.total_reviews as 'ms.total_reviews',
						msr.rating as 'ms.rating'
					" : "")

				. " FROM " . DB_PREFIX . "product p
				INNER JOIN " . DB_PREFIX . "product_description pd
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product mp
					USING(product_id)
				LEFT JOIN " . DB_PREFIX . "ms_seller ms
					USING (seller_id)
				LEFT JOIN " . DB_PREFIX . "product_to_category p2c
					ON(p2c.product_id = p.product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product_to_category msp2c
					ON(msp2c.product_id = p.product_id)"

				. (isset($cols['product_ms_rating']) ? "
					LEFT JOIN (
						SELECT
							product_id,
							COUNT(1) as `total_reviews`,
							AVG(rating) as `rating`
						FROM `" . DB_PREFIX . "ms_review`
						WHERE status = '1'
						GROUP BY product_id
					) msr
						ON(msr.product_id = p.product_id)
				" : "")

				. "WHERE 1 = 1"

				. (isset($data['product_id']) ? " AND p.product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['seller_id']) ? " AND ms.seller_id =  " .  (int)$data['seller_id'] : '')
				. (isset($data['category_id']) ? " AND p2c.category_id =  " .  (int)$data['category_id'] : '')
				. (isset($data['ms_category_id']) ? " AND FIND_IN_SET (msp2c.ms_category_id, (SELECT GROUP_CONCAT(category_id SEPARATOR ',') FROM `" . DB_PREFIX . "ms_category_path` WHERE path_id = " . (int)$data['ms_category_id'] . "))" : '')
				. (isset($data['language_id']) ? " AND pd.language_id =  " .  (int)$data['language_id'] : '')
				. (isset($data['product_status']) ? " AND product_status IN  (" .  $this->db->escape(implode(',', $data['product_status'])) . ")" : '')
				. (isset($data['oc_status']) ? " AND p.status =  " .  (int)$data['oc_status'] : '')
				. (isset($data['available']) ? " AND p.date_available <= NOW()" : '')
				. (!empty($data['name']) ? " AND pd.name LIKE '" . $this->db->escape($data['name']) . "'" : '')
				. (isset($data['model']) ? " AND p.model LIKE '" . $this->db->escape($data['model']) . "'" : '')

				. $wFilters

				. " GROUP BY p.product_id HAVING 1 = 1 "
				
				. $hFilters
				
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];

		return isset($data['single']) ? $res->rows[0] : $res->rows;
	}

	public function getProductName($product_id, $language_id = null)
	{
		if (!$language_id)
			$language_id = $this->config->get('config_language_id');

		$sql = "
			SELECT
				`name`
			FROM " . DB_PREFIX . "product_description
			WHERE product_id = " . (int)$product_id . "
				AND language_id = " . (int)$language_id . "
		";

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['name'] : '';
	}

	public function getStatus($product_id) {
		$sql = "SELECT mp.product_status AS status
				FROM `" . DB_PREFIX . "ms_product` mp
				WHERE product_id = " . (int)$product_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['status'];
	}

	public function changeStatus($product_id, $product_status)
	{
		$this->db->query("
			UPDATE `" . DB_PREFIX . "ms_product`
			SET	`product_status` = " .  (int)$product_status . "
			WHERE `product_id` = " . (int)$product_id
		);

		$enabled = ($product_status == MsProduct::STATUS_ACTIVE) ? 1 : 0;

		$this->db->query("
			UPDATE `" . DB_PREFIX . "product`
			SET `status` = " . (int)$enabled . "
			WHERE `product_id` = " . (int)$product_id
		);

		// Delete product from coupons
		if (!$enabled) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_coupon_product` WHERE product_id = " . (int)$product_id);
		}

		// Create Stripe Usage record
		if ($this->config->get('ms_stripe_connect_status') && (int)MsProduct::STATUS_ACTIVE === (int)$product_status)
			$this->reportStripePerProductPlanUsage($product_id);

		$this->cache->delete('product');
	}
	
	public function approve($product_id) {
		$sql = "UPDATE " . DB_PREFIX . "ms_product
				SET	product_approved =  1
				WHERE product_id = " . (int)$product_id;
		
		$res = $this->db->query($sql);
		$this->registry->get('cache')->delete('product');
	}
	
	public function disapprove($product_id) {
		$sql = "UPDATE " . DB_PREFIX . "ms_product
				SET	product_approved =  0
				WHERE product_id = " . (int)$product_id;
		
		$res = $this->db->query($sql);
		
		$this->registry->get('cache')->delete('product');
	}

	public function createRecord($product_id, $data = array()) {
		$sql = "INSERT IGNORE INTO " . DB_PREFIX . "ms_product
				SET	product_id =  " . (int)$product_id . ",
					product_status = " . (int)MsProduct::STATUS_INACTIVE
				. (isset($data['seller_id']) ? ", seller_id =  " .  (int)$data['seller_id'] : '');
		
		$res = $this->db->query($sql);
	}
	
	public function changeSeller($product_id, $seller_id) {
		$sql = "UPDATE " . DB_PREFIX . "ms_product
				SET	seller_id =  " . (int)$seller_id . "
				WHERE product_id = " . (int)$product_id;
		$res = $this->db->query($sql);
		$this->registry->get('cache')->delete('product');
	}

    public function getManufacturers($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "manufacturer";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    protected function reportStripePerProductPlanUsage($product_id)
	{
		try {
			list($stripe_connect_settings, $env) = $this->initStripe();

			if (!$stripe_connect_settings['enable_subscription'])
				return;

			// @todo 9.0: problem with amazon-style listing
			$seller_id = $this->getSellerId($product_id);

			if (!$seller_id)
				throw new \MultiMerch\Module\Errors\Generic("Could not retrieve seller id for product #$product_id!");

			$stripe_subscription_id = $this->MsLoader->MsSetting->getSellerSettings([
				'seller_id' => $seller_id,
				'name' => 'slr_stripe_subscription_id',
				'single' => true
			]);

			if (empty($stripe_subscription_id))
				return;

			// Retrieve Stripe subscription
			$subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

			if (empty($subscription))
				throw new \MultiMerch\Module\Errors\Generic("Stripe Subscription retrieved but is empty!");

			$billed_product_ids = $this->config->get('ms_stripe_connect_current_subscription_period_billed_product_ids');

			if (empty($billed_product_ids[$seller_id]))
				$billed_product_ids[$seller_id] = [];

			foreach ($subscription->items->data as $item) {
				if ('metered' === (string)$item->plan->usage_type && !in_array($product_id, $billed_product_ids[$seller_id])) {
					\Stripe\UsageRecord::create([
						'quantity' => 1,
						'timestamp' => time(),
						'subscription_item' => $item->id,
						'action' => 'increment'
					]);

					$billed_product_ids[$seller_id][] = $product_id;
				}
			}

			// Update `billed_product_ids` setting for current period
			$this->MsLoader->MsHelper->createOCSetting([
				'code' => 'ms_stripe_connect',
				'key' => 'ms_stripe_connect_current_subscription_period_billed_product_ids',
				'value' => $billed_product_ids
			]);
		} catch (\Stripe\Error\InvalidRequest $e) {
			$this->ms_logger->error($e->getMessage());
		} catch (\MultiMerch\Module\Errors\Settings $e) {
			$this->ms_logger->error($e->getMessage());
		} catch (\MultiMerch\Module\Errors\Generic $e) {
			$this->ms_logger->error($e->getMessage());
		}
	}

	public function getFeeRates($product_id, $fee_type = null, $product_total = 0)
	{
		$product = $this->getProduct($product_id);

		if (!empty($product['commission_id'])) {
			$rates = $this->MsLoader->MsCommission->getCommissionRates($product['commission_id']);
		} else {
			$category_ids = (array)explode(',', $this->getProductOcCategories($product_id));
			$rates = $this->MsLoader->MsCategory->getOcCategoryBiggestFeeRates($category_ids, $fee_type, $product_total);
		}

		return $rates;
	}

	public function getProductPrimaryOcCategory($product_id)
	{
		$result = $this->db->query("SELECT primary_oc_category FROM `" . DB_PREFIX . "ms_product` WHERE `product_id` = " . (int)$product_id);

		return $result->row['primary_oc_category'] ?? 0;
	}

	public function getProductPrimaryMsCategory($product_id)
	{
		$result = $this->db->query("SELECT primary_ms_category FROM `" . DB_PREFIX . "ms_product` WHERE `product_id` = " . (int)$product_id);

		return $result->row['primary_ms_category'] ?? 0;
	}
}
