<?php

class MsTransaction extends Model
{
	/**
	 * @var \MultiMerch\Core\Invoice\Manager Invoice manager.
	 */
	protected $invoice_manager;

	/**
	 * MsTransaction constructor.
	 *
	 * @param	mixed	$registry	OpenCart registry.
	 */
	public function __construct($registry) {
		parent::__construct($registry);

		$this->load->language('multiseller/multiseller');

		$this->invoice_manager = new \MultiMerch\Core\Invoice\Manager($registry);
	}

	/**
	 * Represents the main flow of how MultiMerch processing customer order.
	 *
	 * Creates suborders, calculates totals for each suborder (shipping, coupons etc.), processes selling fees.
	 *
	 * @param	int		$order_id	Order id.
	 * @return	bool				Whether order is processed.
	 */
	public function process($order_id)
	{
		$this->ms_logger->debug("Begin MultiMerch transactions process for order #{$order_id}");

		// Order products
		$order_products = $this->getOrderProducts($order_id);

		// Group products by seller id for suborders
		$products_by_seller_id = $this->groupProductsBySellerId($order_products);

		// Process suborders
		foreach ($products_by_seller_id as $seller_id => $products) {
			$this->ms_logger->debug("Products by seller #{$seller_id}: " . print_r($products, true));

			// Create or retrieve existing suborder
			$suborder = $this->processSuborder($order_id, $seller_id, $products);

			$this->ms_logger->debug("Suborder for seller #{$seller_id} in order #{$order_id}: " . print_r($suborder, true));

			// Process sale fees
			$this->processSaleFees($order_id, $seller_id, $suborder, $products);
		}

		// Create paid invoice (from platform to customer)
		$this->createCustomerInvoice($order_id);

		// If the customer is also a seller, we flag this using `slr_is_customer` setting
		if ($this->customer->isLogged() && $this->MsLoader->MsSeller->isCustomerSeller($this->customer->getId())) {
			$this->MsLoader->MsSetting->createSellerSetting([
				'seller_id' => $this->customer->getId(),
				'settings' => ['slr_is_customer' => 1]
			]);

			$this->ms_logger->info("Seller is also marked as customer");
		}

		$this->ms_logger->info("Finished processing customer order #{$order_id}");

		return true;
	}

	/**
	 * Processes MutliMerch related records when admin edits order.
	 *
	 * DEPRECATED.
	 *
	 * @param	int		$order_id	Order id.
	 * @return	bool				Whether order is processed.
	 */
	public function processInitiatedByAdmin($order_id)
	{
		// Order products
		$order_products = $this->getOrderProducts($order_id);

		// Group products by seller id for suborders
		$products_by_seller_id = $this->groupProductsBySellerId($order_products);

		foreach ($products_by_seller_id as $seller_id => $products) {
			// Retrieve suborder
			$suborder = $this->MsLoader->MsSuborder->getSuborders([
				'order_id' => $order_id,
				'seller_id' => $seller_id,
				'include_abandoned' => true,
				'single' => true
			]);

			if (!empty($suborder['suborder_id'])) {
				foreach ($products as $product) {
					$this->db->query("
						UPDATE " . DB_PREFIX . "ms_order_product_data
						SET order_product_id = " . (int)$product['order_product_id'] . "
						WHERE order_id = " . (int)$product['order_id'] . "
							AND product_id = " . (int)$product['product_id']
					);

					$this->db->query("
						UPDATE " . DB_PREFIX . "ms_order_product_shipping_data
						SET order_product_id = " . (int)$product['order_product_id'] . "
						WHERE order_id = " . (int)$product['order_id'] . "
							AND product_id = " . (int)$product['product_id']
					);
				}
			}
		}

		return true;
	}

	/**
	 * Creates and retrieves suborder data (including totals) for specific seller in the order.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$products	Products by seller in the order.
	 * @return	array				Suborder data.
	 */
	protected function processSuborder($order_id, $seller_id, $products)
	{
		// Create suborder
		$suborder = $this->MsLoader->MsSuborder->getSuborders([
			'order_id' => $order_id,
			'seller_id' => $seller_id,
			'include_abandoned' => 1,
			'single' => 1
		]);

		if (empty($suborder)) {
			$suborder = $this->MsLoader->MsSuborder->createSuborder([
				'order_id' => $order_id,
				'seller_id' => $seller_id,
				'order_status_id' => $this->config->get('msconf_suborder_default_status'),
				'customer_comment' => $this->session->data['suborder_comments'][$seller_id] ?? ''
			], true);

			$this->MsLoader->MsSuborder->addSuborderHistory([
				'suborder_id' => $suborder['suborder_id'],
				'order_status_id' => $this->config->get('msconf_suborder_default_status'),
				'comment' => $this->session->data['suborder_comments'][$seller_id] ?? $this->language->get('ms_transaction_order_created')
			]);

			// Totals
			$suborder['totals'] = $this->getSuborderTotals($order_id, $seller_id, $suborder, $products);
			foreach ($suborder['totals'] as $code => $total) {
				$this->MsLoader->MsSuborder->createSuborderTotal($suborder['suborder_id'], $code, $total['title'], $total['value'], $total['sort_order']);
			}

			$this->ms_logger->info("Created suborder #{$suborder['suborder_id']} with status {$this->config->get('msconf_suborder_default_status')}");
		} else {
			$this->ms_logger->info("Retrieved suborder #{$suborder['suborder_id']}");
		}

		return $suborder;
	}

	/**
	 * Calculates suborder totals.
	 *
	 * Totals are: Subtotal, MM shipping, OC taxes, OC coupon, MM coupon, Grand suborder total.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$suborder	Suborder data.
	 * @param	array	$products	Products by seller in the order.
	 * @return	array				An array of suborder totals.
	 */
	protected function getSuborderTotals($order_id, $seller_id, $suborder, $products)
	{
		$totals = [];

		// Subtotal
		$this->processSubtotal($products, $totals);

		$this->ms_logger->debug("Suborder totals after processing subtotal: " . print_r($totals, true));

		// MultiMerch shipping
		$this->processMMShipping($order_id, $seller_id, $suborder, $products, $totals);

		$this->ms_logger->debug("Suborder totals after processing MM shipping: " . print_r($totals, true));

		// OpenCart taxes
		$this->processTax($products, $totals);

		$this->ms_logger->debug("Suborder totals after processing OC taxes: " . print_r($totals, true));

		// OpenCart coupon
		$this->processOcCoupon($totals);

		$this->ms_logger->debug("Suborder totals after processing OC coupon: " . print_r($totals, true));

		// MultiMerch coupon
		$this->processMsCoupon($seller_id, $totals);

		$this->ms_logger->debug("Suborder totals after processing MM coupon: " . print_r($totals, true));

		// Total
		$grand_total = 0;
		foreach ($totals as $total) {
			// @todo 9.0: For multiple MultiMerch coupons per user, ms_coupon total will be array
			$grand_total += $total['value'];
		}

		$totals['total'] = [
			'title' => $this->language->get('ms_report_column_total'),
			'value' => $grand_total,
			'sort_order' => 6
		];

		$this->ms_logger->debug("Suborder totals with grand total: " . print_r($totals, true));

		uasort($totals, function ($total_1, $total_2) {
			return $total_1['sort_order'] <=> $total_2['sort_order'];
		});

		return $totals;
	}

	/**
	 * Calculates suborder subtotal of suborder.
	 *
	 * @param	array	$products	Products by seller in the order.
	 * @param	array	$totals		Global totals array for suborder. Passed by reference.
	 * @return	array				Subtotal data.
	 */
	protected function processSubtotal($products, &$totals)
	{
		$sub_total = 0;

		foreach ($products as $product) {
			$sub_total += $product['total'];
		}

		$totals['sub_total'] = [
			'title' => $this->language->get('ms_invoice_total_title_subtotal'),
			'value' => $sub_total,
			'sort_order' => 1
		];

		return $totals['sub_total'];
	}

	/**
	 * Calculates MultiMerch shipping total of suborder.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$suborder	Suborder data.
	 * @param	array	$products	Products by seller in the order.
	 * @param	array	$totals		Global totals array for suborder. Passed by reference.
	 * @return	array				MultiMerch shipping data.
	 */
	protected function processMMShipping($order_id, $seller_id, $suborder, $products, &$totals)
	{
		$shipping = 0;

		$shipping_data = isset($this->session->data['ms_checkout_shipping'][$seller_id]) ? $this->session->data['ms_checkout_shipping'][$seller_id] : [];

		// Per-product shipping
		foreach ($products as $product) {
			if (isset($shipping_data['individual'][$product['cart_id']]) && $shipping_data['individual'][$product['cart_id']]['cost'] > 0) {
				$this->ms_logger->debug("Order product #{$product['order_product_id']} has individual shipping applied (cost: {$shipping_data['individual'][$product['cart_id']]['cost']})");

				$this->MsLoader->MsOrderData->addOrderProductShippingData(
					$order_id,
					$product['product_id'],
					[
						'order_product_id' => $product['order_product_id'],
						'shipping_method_name' => $shipping_data['individual'][$product['cart_id']]['name'],
						'shipping_cost' => $shipping_data['individual'][$product['cart_id']]['cost']
					]
				);

				$shipping += $shipping_data['individual'][$product['cart_id']]['cost'];
			}
		}

		// Suborder shipping
		if (isset($shipping_data['combined']) && $shipping_data['combined']['cost']) {
			$this->ms_logger->debug("Suborder #{$suborder['suborder_id']} has combined shipping applied (cost: {$shipping_data['combined']['cost']})");

			$this->MsLoader->MsShipping->addSuborderShippingData([
				'suborder_id' => $suborder['suborder_id'],
				'shipping_method_name' => $shipping_data['combined']['name'],
				'cost' => $shipping_data['combined']['cost'],
			]);

			$shipping += $shipping_data['combined']['cost'];
		}

		$totals['mm_shipping_total'] = [
			'title' => $this->language->get('ms_report_column_shipping'),
			'value' => $shipping,
			'sort_order' => 2
		];

		return $totals['mm_shipping_total'];
	}

	/**
	 * Calculates OpenCart taxes total of suborder.
	 *
	 * @param	array	$products	Products by seller in the order.
	 * @param	array	$totals		Global totals array for suborder. Passed by reference.
	 * @return	array				OpenCart taxes data.
	 */
	protected function processTax($products, &$totals)
	{
		$tax = 0;

		foreach ($products as $product) {
			$tax += ($product['tax'] * $product['quantity']);
		}

		$totals['tax'] = [
			'title' => $this->language->get('ms_report_column_tax'),
			'value' => $tax,
			'sort_order' => 3
		];

		return $totals['tax'];
	}

	/**
	 * Calculates OpenCart coupon total of suborder.
	 *
	 * @param	array		$totals		Global totals array for suborder. Passed by reference.
	 * @return	array|null				OpenCart coupon data.
	 */
	protected function processOcCoupon(&$totals)
	{
		if (!isset($this->session->data['coupon'])) {
			return null;
		}

		$this->load->language('extension/total/coupon');

		$discount = 0;

		$coupon = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "coupon` WHERE `code` = '" . $this->db->escape($this->session->data['coupon']) . "'")->row;

		if (!empty($coupon['type']) && $coupon['type'] == 'P') {
			$discount = $totals['sub_total']['value'] / 100 * $coupon['discount'];
		}

		$totals['coupon'] = [
			'title' => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
			'value' => -$discount,
			'sort_order' => 4
		];

		$this->ms_logger->debug("OpenCart coupon total: " . print_r($totals['coupon'], true));

		return $totals['coupon'];
	}

	/**
	 * Calculates MultiMerch coupon total of suborder.
	 *
	 * @param	int			$seller_id	Seller id.
	 * @param	array		$totals		Global totals array for suborder. Passed by reference.
	 * @return	array|null				MultiMerch coupon data.
	 */
	protected function processMsCoupon($seller_id, &$totals)
	{
		if (empty($this->session->data['ms_coupons'][$seller_id])) {
			return null;
		}

		$discount = 0;

		$ms_coupon = $this->MsLoader->MsCoupon->getCoupons([
			'seller_id' => $seller_id,
			'code' => $this->session->data['ms_coupons'][$seller_id]
		]);

		if (!empty($ms_coupon)) {
			if (MsCoupon::TYPE_DISCOUNT_PERCENT === (int)$ms_coupon['type']) {
				$discount = (float)$totals['sub_total']['value'] * (float)$ms_coupon['value'] / 100;
			} elseif (MsCoupon::TYPE_DISCOUNT_FIXED === (int)$ms_coupon['type']) {
				$discount = (float)$ms_coupon['value'];
			}
		}

		// Fallback if discount is bigger than subtotal
		if ($discount > $totals['sub_total']['value']) {
			$discount = $totals['sub_total']['value'];
		}

		$totals['ms_coupon'] = [
			'title' => sprintf($this->language->get('ms_total_coupon_title'), $this->MsLoader->MsSeller->getSellerNickname($seller_id), $this->session->data['ms_coupons'][$seller_id]),
			'value' => -$discount,
			'sort_order' => 5
		];

		$this->ms_logger->debug("MultiMerch coupon total: " . print_r($totals['ms_coupon'], true));

		return $totals['ms_coupon'];
	}

	/**
	 * Processes selling fees for suborder and products in it.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$suborder	Suborder data.
	 * @param	array	$products	Products by seller in the order.
	 */
	protected function processSaleFees($order_id, $seller_id, $suborder, $products)
	{
		switch ($this->config->get('msconf_sale_fee_calculation_mode')) {
			case 2:
				$this->ms_logger->info("Fee calculation mode: Legacy");
				$this->processSaleFeesForOrderProducts($order_id, $seller_id, $suborder, $products);
				break;

			case 1:
			default:
				$this->ms_logger->info("Fee calculation mode: Standard");
				$this->processSaleFeesForSuborders($order_id, $seller_id, $suborder, $products);
				break;
		}
	}

	/**
	 * Processes selling fees for each product in suborder.
	 *
	 * DEPRECATED.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$suborder	Suborder data.
	 * @param	array	$products	Products by seller in the order.
	 */
	protected function processSaleFeesForOrderProducts($order_id, $seller_id, $suborder, $products)
	{
		$this->ms_logger->info("DEPRECATED FLOW: Processing selling fees for each order product by seller #{$seller_id} in order #{$order_id}");

		foreach ($products as $product) {
			$product_total = $product['total'];

			if ($product_total <= 0) {
				$this->ms_logger->error('Order product total is less or equals zero.');
				continue;
			}

			$flat = 0;
			$percent = 0;
			$seller_net = $product_total + $product['tax'];

			$rates = [];

			if ($this->config->get('msconf_fee_priority') == 1) {
				$this->ms_logger->debug("Selling fee priority: Catalog");
				$rates = $this->MsLoader->MsProduct->getFeeRates($product['product_id'], MsCommission::RATE_SALE, $product_total);
			}

			if ($this->config->get('msconf_fee_priority') == 2) {
				$this->ms_logger->debug("Selling fee priority: Vendor");
				$rates = $this->MsLoader->MsSeller->getFeeRates($seller_id);
			}

			$this->ms_logger->debug('Fee rates:' . print_r($rates, true));

			if (!empty($rates[MsCommission::RATE_SALE])) {
				$flat = !empty($rates[MsCommission::RATE_SALE]['flat'])
					? (float)$rates[MsCommission::RATE_SALE]['flat']
					: 0;

				$percent = !empty($rates[MsCommission::RATE_SALE]['percent'])
					? ($product_total * (float)$rates[MsCommission::RATE_SALE]['percent'] / 100)
					: 0;

				$seller_net -= ($flat + $percent);

				// Fallback if commissions are bigger than product total
				if ($flat > $product_total || $percent > $product_total || ($flat + $percent) > $product_total) {
					$flat = $product_total;
					$percent = 0;
					$seller_net = 0;
				}
			}

			$this->ms_logger->debug("Calculated selling fees: flat {$flat}; percent {$percent}; seller net {$seller_net}");

			$this->MsLoader->MsOrderData->addOrderProductData(
				$order_id,
				$product['product_id'],
				[
					'order_product_id' => $product['order_product_id'],
					'seller_id' => $seller_id,
					'suborder_id' => $suborder['suborder_id'],
					'store_commission_flat' => $flat,
					'store_commission_pct' => $percent,
					'seller_net_amt' => $seller_net
				]
			);

			$this->ms_logger->debug("Selling fees for order product #{$product['order_product_id']} are stored.");
		}

		$this->ms_logger->info("DEPRECATED FLOW: Finished processing selling fees for each order product by seller #{$seller_id} in order #{$order_id}");
	}

	/**
	 * Processes selling fees for suborder and for products in it.
	 *
	 * In the future, this will be the main flow. It calculates suborder selling fees based on seller/seller's group
	 * settings + calculates selling fees for each product unit in the order based on product/product's category
	 * settings.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$suborder	Suborder data.
	 * @param	array	$products	Products by seller in the order.
	 */
	protected function processSaleFeesForSuborders($order_id, $seller_id, $suborder, $products)
	{
		$this->ms_logger->info("Processing selling fees for suborder #{$suborder['suborder_id']} by seller #{$seller_id} in order #{$order_id}");

		// Seller sale fees
		if ($this->config->get('msconf_sale_fee_seller_enabled')) {
			$this->ms_logger->debug('Processing seller selling fees');

			// Suborder total excluding taxes and shipping cost
			$suborder_total = (float)$suborder['totals']['sub_total']['value'];

			// Include OpenCart coupon discount if exists
			if (!empty($suborder['totals']['coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['coupon']['value'];
			}

			// Include MultiMerch coupon discount if exists
			if (!empty($suborder['totals']['ms_coupon']['value'])) {
				$suborder_total += (float)$suborder['totals']['ms_coupon']['value'];
			}

			// @todo 9.0: whether to include shipping cost in commission calculations

			$rates = $this->MsLoader->MsSeller->getFeeRates($seller_id);

			$this->ms_logger->debug('Fee rates:' . print_r($rates, true));

			$flat = 0;
			$percent = 0;

			if (!empty($rates[MsCommission::RATE_SALE])) {
				// Flat store commission
				$flat = !empty($rates[MsCommission::RATE_SALE]['flat'])
					? (float)$rates[MsCommission::RATE_SALE]['flat']
					: 0;

				$percent = !empty($rates[MsCommission::RATE_SALE]['percent'])
					? $suborder_total * (float)$rates[MsCommission::RATE_SALE]['percent'] / 100
					: 0;

				// Fallback if commissions are bigger than suborder total
				if ($flat > $suborder_total || $percent > $suborder_total || ($flat + $percent) > $suborder_total) {
					$flat = $suborder_total;
					$percent = 0;
				}
			}

			$this->ms_logger->debug("Calculated selling fees: flat {$flat}; percent {$percent}");

			$this->MsLoader->MsSuborder->saveSuborderCommissions($suborder['suborder_id'], [
				'flat' => $flat,
				'percent' => $percent,
			]);

			$this->ms_logger->debug("Selling fees for suborder #{$suborder['suborder_id']} are stored.");
		}

		// Catalog sale fees

		foreach ($products as $product) {
			$product_total = $product['total'];

			$flat = 0;
			$percent = 0;
			$seller_net = $product_total;

			if ($this->config->get('msconf_sale_fee_catalog_enabled')) {
				$this->ms_logger->debug('Processing order products selling fees');

				$rates = $this->MsLoader->MsProduct->getFeeRates($product['product_id'], MsCommission::RATE_SALE, $product_total);

				$this->ms_logger->debug("Fee rates for product {$product['product_id']}: " . print_r($rates, true));

				if (!empty($rates[MsCommission::RATE_SALE])) {
					$flat = !empty($rates[MsCommission::RATE_SALE]['flat'])
						? (float)$rates[MsCommission::RATE_SALE]['flat'] * $product['quantity']
						: 0;

					$percent = !empty($rates[MsCommission::RATE_SALE]['percent'])
						? ($product['price'] * (float)$rates[MsCommission::RATE_SALE]['percent'] / 100) * $product['quantity']
						: 0;

					$seller_net -= ($flat + $percent);

					// Fallback if commissions are bigger than product total
					if ($flat > $product_total || $percent > $product_total || ($flat + $percent) > $product_total) {
						$flat = $product_total;
						$percent = 0;
						$seller_net = 0;
					}
				}

				$this->ms_logger->debug("Calculated selling fees: flat {$flat}; percent {$percent}; seller net {$seller_net}");
			}

			$this->MsLoader->MsOrderData->addOrderProductData(
				$order_id,
				$product['product_id'],
				[
					'order_product_id' => $product['order_product_id'],
					'seller_id' => $seller_id,
					'suborder_id' => $suborder['suborder_id'],
					'store_commission_flat' => $flat,
					'store_commission_pct' => $percent,
					'seller_net_amt' => $seller_net
				]
			);

			$this->ms_logger->debug("Selling fees for order product #{$product['order_product_id']} are stored.");
		}

		$this->ms_logger->info("Finished processing selling fees for suborder #{$suborder['suborder_id']} by seller #{$seller_id} in order #{$order_id}");
	}

	/**
	 * Creates sale invoice from platform to customer.
	 *
	 * @param	int		$order_id	Order id.
	 */
	protected function createCustomerInvoice($order_id)
	{
		$order_info = $this->MsLoader->MsOrderData->ocGetOrder($order_id);

		// NOTE: If advanced payments flow is used (Stripe / PayPal Adaptive), invoices are created in those payment gateways controllers

		// If order is being paid using default OC payment gateway
		if (!empty($order_info['payment_code']) && !in_array($order_info['payment_code'], ['ms_pp_adaptive', 'ms_stripe_connect'])) {
			$this->ms_logger->info('Creating invoice from the marketplace to the customer for all the products in order');
			$this->invoice_manager->createSaleInvoiceFromPlatformToCustomer($order_id, $this->customer->getId(), false);
		}
	}

	/**
	 * Processes balance changes for seller in the order.
	 *
	 * @param	int		$order_id				Order id.
	 * @param	int		$seller_id				Seller id.
	 * @param	int		$suborder_status_id		New suborder status id.
	 * @return	bool							Whether balance changes are processed.
	 */
	public function processSuborderTransactions($order_id, $seller_id, $suborder_status_id)
	{
		$this->ms_logger->info("Processing balance changes for seller #{$seller_id} in the order #{$order_id}");

		$order_info = $this->MsLoader->MsOrderData->ocGetOrder($order_id);

		$is_direct_payment = !empty($order_info['payment_code']) && in_array($order_info['payment_code'], ['ms_pp_adaptive', 'ms_stripe_connect']);

		// If it is direct payment, no transactions must be created
		if ($is_direct_payment) {
			$this->ms_logger->debug("Direct payment ({$order_info['payment_code']}). No balance transactions must be created.");
			return false;
		}

		// Credit and Debit suborder statuses
		$ms_credit_statuses = !empty($this->config->get('msconf_credit_order_statuses')['ms']) ? $this->config->get('msconf_credit_order_statuses')['ms'] : [];
		$ms_debit_statuses = !empty($this->config->get('msconf_debit_order_statuses')['ms']) ? $this->config->get('msconf_debit_order_statuses')['ms'] : [];

		$is_credit_transaction = in_array($suborder_status_id, $ms_credit_statuses);
		$is_debit_transaction = in_array($suborder_status_id, $ms_debit_statuses);

		// Suborder transactions
		if ($is_credit_transaction) {
			$this->createBalanceEntriesForSuborder($order_id, $seller_id);
		} elseif ($is_debit_transaction) {
			$this->createBalanceEntriesForSuborder($order_id, $seller_id, true);
		}

		$order_products = $this->getOrderProducts($order_id, $seller_id);

		// Per-order-product transactions
		foreach ($order_products as $product) {
			if ($is_credit_transaction) {
				$this->createBalanceEntriesForOrderProduct($order_id, $seller_id, $product);
			} elseif ($is_debit_transaction) {
				$this->createBalanceEntriesForOrderProduct($order_id, $seller_id, $product, true);
			}
		}

		$this->ms_logger->info("Balance changes processed");

		return true;
	}

	/**
	 * Processes balance changes for all the sellers in the order.
	 *
	 * @param	int		$order_id			Order id.
	 * @param	int		$order_status_id	New order status id.
	 * @return	bool						Whether balance changes are processed.
	 */
	public function processOrderTransactions($order_id, $order_status_id)
	{
		$this->ms_logger->info("Processing balance changes for all the sellers in the order #{$order_id}");

		$order_info = $this->MsLoader->MsOrderData->ocGetOrder($order_id);

		$is_direct_payment = !empty($order_info['payment_code']) && in_array($order_info['payment_code'], ['ms_pp_adaptive', 'ms_stripe_connect']);

		// If it is direct payment, no transactions must be created
		if ($is_direct_payment) {
			$this->ms_logger->debug("Direct payment ({$order_info['payment_code']}). No balance transactions must be created.");
			return false;
		}

		// Credit and Debit suborder statuses
		$oc_credit_statuses = !empty($this->config->get('msconf_credit_order_statuses')['oc']) ? $this->config->get('msconf_credit_order_statuses')['oc'] : [];
		$oc_debit_statuses = !empty($this->config->get('msconf_debit_order_statuses')['oc']) ? $this->config->get('msconf_debit_order_statuses')['oc'] : [];

		$is_credit_transaction = in_array($order_status_id, $oc_credit_statuses);
		$is_debit_transaction = in_array($order_status_id, $oc_debit_statuses);

		// Order products
		$order_products = $this->getOrderProducts($order_id);
		$products_by_seller_id = $this->groupProductsBySellerId($order_products);

		foreach ($products_by_seller_id as $seller_id => $products) {
			$this->ms_logger->info("Processing balance changes for seller #{$seller_id} in the order #{$order_id}");

			// Suborder transactions
			if ($is_credit_transaction) {
				$this->createBalanceEntriesForSuborder($order_id, $seller_id);
			} elseif ($is_debit_transaction) {
				$this->createBalanceEntriesForSuborder($order_id, $seller_id, true);
			}

			// Per-order-product transactions
			foreach ($products as $product) {
				if ($is_credit_transaction) {
					$this->createBalanceEntriesForOrderProduct($order_id, $seller_id, $product);
				} elseif ($is_debit_transaction) {
					$this->createBalanceEntriesForOrderProduct($order_id, $seller_id, $product, true);
				}
			}
		}

		$this->ms_logger->info("Balance changes processed");

		return true;
	}

	/**
	 * Processes all possible transactions for the whole suborder.
	 *
	 * All possible transactions are: suborder (per-seller) selling fees, suborder (combined) shipping, OC coupons, MM coupons.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	bool	$is_refund	Indicates whether the transaction must be refunded.
	 */
	protected function createBalanceEntriesForSuborder($order_id, $seller_id, $is_refund = false)
	{
		$this->ms_logger->info(($is_refund ? "Refunding" : "Creating") . " transactions for seller #{$seller_id} in order #{$order_id}");

		$suborder = $this->MsLoader->MsSuborder->getSuborders([
			'order_id' => $order_id,
			'seller_id' => $seller_id,
			'single' => true
		]);

		// 1. Suborder (per-seller) sale fee
		$suborder_sale_fee = (float)$suborder['store_commission_flat'] + (float)$suborder['store_commission_pct'];
		$description = sprintf($this->language->get('ms_transaction_sale_fee_order'), $order_id);
		$amount = -(float)$suborder_sale_fee;

		if ($is_refund) {
			$description = sprintf($this->language->get('ms_transaction_sale_fee_order_refund'), $order_id);
			$amount = -$amount;
		}

		if ($this->createBalanceEntry($order_id, $seller_id, null, null, MsBalance::MS_BALANCE_TYPE_SALE_FEE, $amount, $description)) {
			$this->ms_logger->info("Sale fee transaction for suborder #{$suborder['suborder_id']} (order #{$order_id}, seller #{$seller_id}) created");
		}

		// 2. Suborder (combined) shipping
		$suborder_combined_shipping = $this->MsLoader->MsShipping->getSuborderShippingData($suborder['suborder_id']);
		if (!empty($suborder_combined_shipping['cost'])) {
			$description = sprintf($this->language->get('ms_transaction_shipping_order'), $order_id);
			$amount = (float)$suborder_combined_shipping['cost'];

			if ($is_refund) {
				$description = sprintf($this->language->get('ms_transaction_shipping_order_refund'), $order_id);
				$amount = -$amount;
			}

			if ($this->createBalanceEntry($order_id, $seller_id, null, null, MsBalance::MS_BALANCE_TYPE_SHIPPING, $amount, $description)) {
				$this->ms_logger->info("Combined shipping transaction for suborder #{$suborder['suborder_id']} (order #{$order_id}, seller #{$seller_id}) created");
			}
		}

		// 3. OpenCart coupons
		if (!empty($suborder['totals']['coupon']['value'])) {
			$description = $this->language->get('ms_transaction_coupon');
			$amount = (float)$suborder['totals']['coupon']['value'];

			if ($is_refund) {
				$description = $this->language->get('ms_transaction_coupon_refund');
				$amount = -$amount;
			}

			if ($this->createBalanceEntry($order_id, $seller_id, null, null, MsBalance::MS_BALANCE_TYPE_OCCOUPON, $amount, $description)) {
				$this->ms_logger->info("OC coupon transaction for suborder #{$suborder['suborder_id']} (order #{$order_id}, seller #{$seller_id}) created");
			}
		}

		// 4. MultiMerch coupons
		if (!empty($suborder['totals']['ms_coupon']['value'])) {
			$description = $this->language->get('ms_transaction_ms_coupon');
			$amount = (float)$suborder['totals']['ms_coupon']['value'];

			if ($is_refund) {
				$description = $this->language->get('ms_transaction_ms_coupon_refund');
				$amount = -$amount;
			}

			if ($this->createBalanceEntry($order_id, $seller_id, null, null, MsBalance::MS_BALANCE_TYPE_MSCOUPON, $amount, $description)) {
				$this->ms_logger->info("MM coupon transaction for suborder #{$suborder['suborder_id']} (order #{$order_id}, seller #{$seller_id}) created");
			}
		}
	}

	/**
	 * Processes all possible transactions for each suborder product.
	 *
	 * All possible transactions are: per-order-product selling fees, per-product shipping.
	 *
	 * @param	int		$order_id	Order id.
	 * @param	int		$seller_id	Seller id.
	 * @param	array	$product	Suborder product data.
	 * @param	bool	$is_refund	Indicates whether the transaction must be refunded.
	 */
	protected function createBalanceEntriesForOrderProduct($order_id, $seller_id, $product, $is_refund = false)
	{
		$order_product_data = $this->MsLoader->MsOrderData->getOrderData([
			'product_id' => $product['product_id'],
			'order_product_id' => $product['order_product_id'],
			'order_id' => $order_id,
			'single' => true
		]);

		// 1. Sale fee transaction
		$product_sale_fee = (float)$order_product_data['store_commission_flat'] + (float)$order_product_data['store_commission_pct'];
		$description = $product_sale_fee
			? sprintf($this->language->get('ms_transaction_sale'), ($product['quantity'] > 1 ? $product['quantity'] . ' x ' : '') . $product['name'], $this->currency->format($product_sale_fee, $this->config->get('config_currency')))
			: sprintf($this->language->get('ms_transaction_sale_no_commission'), ($product['quantity'] > 1 ? $product['quantity'] . ' x ' : '') . $product['name']);
		$amount = (float)$product['total'] - (float)$product_sale_fee;

		if ($is_refund) {
			$description = sprintf($this->language->get('ms_transaction_refund'), ($product['quantity'] > 1 ? $product['quantity'] . ' x ' : '') . $product['name']);
			$amount = -$amount;
		}

		if ($this->createBalanceEntry($order_id, $seller_id, $product['product_id'], $product['order_product_id'], MsBalance::MS_BALANCE_TYPE_SALE, $amount, $description)) {
			$this->ms_logger->info("Sale transaction for product #{$product['product_id']} by seller #{$seller_id} created");
		}

		// 2. Per-product shipping transaction
		if ((float)$order_product_data['shipping_cost'] > 0) {
			$description = sprintf($this->language->get('ms_transaction_shipping'), ($product['quantity'] > 1 ? $product['quantity'] . ' x ' : '') . $product['name']);
			$amount = $order_product_data['shipping_cost'];

			if ($is_refund) {
				$description = sprintf($this->language->get('ms_transaction_shipping_refund'), ($product['quantity'] > 1 ? $product['quantity'] . ' x ' : '') . $product['name']);
				$amount = -$amount;
			}

			if ($this->createBalanceEntry($order_id, $seller_id, $product['product_id'], $product['order_product_id'], MsBalance::MS_BALANCE_TYPE_SHIPPING, $amount, $description)) {
				$this->ms_logger->info("Per-product shipping transaction for product #{$product['product_id']} by seller #{$seller_id} created");
			}
		}
	}

	// Helpers

	/**
	 * Gets order products and gets their options.
	 *
	 * Assigns item id from the customer's cart for the order product.
	 *
	 * @param	int			$order_id		Order id.
	 * @param	bool|int	$seller_id		Seller id.
	 * @return	array						Order products.
	 */
	protected function getOrderProducts($order_id, $seller_id = false)
	{
		$order_products = [];
		$cart_products = $this->cart->getProducts();

		$order_products_data = $this->db->query("
			SELECT
				op.*,
				COALESCE(mss.seller_id, 0) as `seller_id`
			FROM `" . DB_PREFIX . "order_product` op
			LEFT JOIN (SELECT product_id, seller_id FROM `" . DB_PREFIX . "ms_product`) mss
				ON (mss.product_id = op.product_id)
			WHERE op.`order_id` = " . (int)$order_id
			. ($seller_id ? " AND mss.`seller_id` = " . (int)$seller_id : "")
		);

		if ($order_products_data->num_rows) {
			foreach ($order_products_data->rows as &$row) {
				if ($this->config->get('msconf_msf_variation_enabled')) {
					$order_product_msf_variation_data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_order_product_msf_variation` WHERE `order_id` = " . (int)$order_id . " AND `order_product_id` = " . (int)$row['order_product_id']);
					$row['msf_variation'] = $order_product_msf_variation_data->rows;
				} else {
					$order_product_option_data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_option` WHERE `order_id` = " . (int)$order_id . " AND `order_product_id` = " . (int)$row['order_product_id']);
					$row['option'] = $order_product_option_data->rows;
				}

				$row['cart_id'] = $this->getCartIdForOrderProduct($row, $cart_products);

				$order_products[] = $row;
			}
		}

		return $order_products;
	}

	/**
	 * Gets item id from the customer's cart for the order product.
	 *
	 * @param	array	$order_product	Order product data.
	 * @param	array	$cart_products	All the products in customer's cart.
	 * @return	int						Cart id.
	 */
	protected function getCartIdForOrderProduct($order_product, $cart_products)
	{
		$cart_id = 0;

		foreach ($cart_products as $cart_product) {
			// If product ids not same, go to the next cart product
			if ((int)$cart_product['product_id'] !== (int)$order_product['product_id']) {
				continue;
			}

			if (isset($order_product['msf_variation'])) {
				$equal_msf_variations = [];

				// Go through all the product MSF variations, and get product form the cart with the same variation
				foreach ($order_product['msf_variation'] as $op_msf_variation) {
					foreach ($cart_product['msf_variation'] as $cp_msf_variation) {
						if (
							(int)$op_msf_variation['msf_variation_id'] === (int)$cp_msf_variation['msf_variation_id']
							&& (int)$op_msf_variation['msf_variation_value_id'] === (int)$cp_msf_variation['msf_variation_value_id']
							&& $op_msf_variation['value'] === $cp_msf_variation['value']
						) {
							$equal_msf_variations[] = $op_msf_variation;
						}
					}
				}

				if ($order_product['msf_variation'] == $equal_msf_variations) {
					$cart_id = $cart_product['cart_id'];
				}
			} elseif (isset($order_product['option'])) {
				$equal_options = [];

				// Go through all the product options, and get product form the cart with the same options
				foreach ($order_product['option'] as $op_option) {
					foreach ($cart_product['option'] as $cp_option) {
						if (
							(int)$op_option['product_option_id'] === (int)$cp_option['product_option_id']
							&& (int)$op_option['product_option_value_id'] === (int)$cp_option['product_option_value_id']
							&& $op_option['value'] === $cp_option['value']
						) {
							$equal_options[] = $op_option;
						}
					}
				}

				if ($order_product['option'] == $equal_options) {
					$cart_id = $cart_product['cart_id'];
				}
			}
		}

		return $cart_id;
	}

	/**
	 * Groups order products by seller id.
	 *
	 * @param	array	$products	Order products.
	 * @return	array				Grouped order products.
	 */
	protected function groupProductsBySellerId($products)
	{
		$result = [];

		foreach ($products as $product) {
			if (!empty($product['seller_id'])) {
				$result[$product['seller_id']][] = $product;
			}
		}

		return $result;
	}

	/**
	 * Stores transaction data in the database.
	 *
	 * @param	int		$order_id			Order id.
	 * @param	int		$seller_id			Seller id.
	 * @param	int		$product_id			Product id.
	 * @param	int		$order_product_id	Order product id.
	 * @param	int		$balance_type		Balance transaction type.
	 * @param	float	$amount				Transaction amount.
	 * @param	string	$description		Transaction description.
	 * @return	bool						Whether the transaction data is stored.
	 */
	protected function createBalanceEntry($order_id, $seller_id, $product_id, $order_product_id, $balance_type, $amount, $description)
	{
		$this->ms_logger->debug("Processing transaction with parameters - order id: {$order_id}, seller_id: {$seller_id}, product id: {$product_id}, order product id: {$order_product_id}, balance type: {$balance_type}, amount: {$amount}, description: {$description}");

		// If amount is zero, it means no changes should be made
		if ((float)$amount === 0.0) {
			$this->ms_logger->debug("Transaction must not be created - amount is 0.");
			return false;
		}

		// Check if transaction has been already created
		$transaction_exists = $this->MsLoader->MsBalance->getBalanceEntry([
			'seller_id' => $seller_id,
			'order_id' => (int)$order_id,
			'product_id' => (int)$product_id,
			'order_product_id' => (int)$order_product_id,
			'balance_type' => (int)$balance_type,
		]);

		if ($transaction_exists) {
			$this->ms_logger->debug("Transaction already exists.");
			return false;
		}

		$this->MsLoader->MsBalance->addBalanceEntry($seller_id, [
			'order_id' => (int)$order_id,
			'product_id' => (int)$product_id,
			'order_product_id' => (int)$order_product_id,
			'balance_type' => (int)$balance_type,
			'amount' => (float)$amount,
			'description' => $description
		]);

		$this->ms_logger->debug("Transaction created");

		return true;
	}
}
