<?php
class MsShippingMethod extends Model {
	// Shipping methods status
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 2;


	/**
	 *  Admin-side related methods
	 */
	public function getShippingMethods($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					*
					FROM `" . DB_PREFIX . "ms_shipping_method` sm
					JOIN `" . DB_PREFIX . "ms_shipping_method_description` smd
						USING(shipping_method_id)

					WHERE 1 = 1"

			. (isset($data['shipping_method_id']) ? " AND sm.shipping_method_id =  " .  (int)$data['shipping_method_id'] : '')
			. (isset($data['language_id']) ? " AND smd.language_id =  " .  (int)$data['language_id'] : '')

			. $wFilters

			. " GROUP BY sm.shipping_method_id HAVING 1 = 1 "

			. $hFilters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];

		if(isset($data['shipping_method_id'])) {
			$sql = "SELECT
					*
					FROM " . DB_PREFIX . "ms_shipping_method_description
					WHERE shipping_method_id = " . (int)$data['shipping_method_id'];

			$descriptions = $this->db->query($sql);
			$shipping_method_description_data = array();
			foreach ($descriptions->rows as $result) {
				$shipping_method_description_data[$result['language_id']] = array(
					'name'             => $result['name'],
					'description'      => $result['description'],
				);
			}
			$res->row['languages'] = $shipping_method_description_data;
		}

		return (isset($data['shipping_method_id'])) ? $res->row : $res->rows;
	}

	public function createShippingMethod($data = array()) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_shipping_method
				SET logo = '" . $this->db->escape($data['logo']) . "',
					status = " . (isset($data['status']) ? (int)$data['status'] : 2);

		$this->db->query($sql);
		$shipping_method_id = $this->db->getLastId();

		if(isset($data['description'])) {
			foreach ($data['description'] as $language_id => $row) {
				$sql = "INSERT INTO " . DB_PREFIX . "ms_shipping_method_description
						SET shipping_method_id = " . (int)$shipping_method_id . ",
							name = '" . $this->db->escape($row['name']) . "',
							description = '" . $this->db->escape($row['description']) . "',
							language_id = " . (int)$language_id;

				$this->db->query($sql);
			}
		}
	}

	public function editShippingMethod($shipping_method_id, $data = array()) {
		$sql = "UPDATE " . DB_PREFIX . "ms_shipping_method
				SET logo = '" . $this->db->escape($data['logo']) . "',
					status = " . (isset($data['status']) ? (int)$data['status'] : 2) . "
				WHERE
					shipping_method_id = " . (int)$shipping_method_id;

		$this->db->query($sql);

		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_shipping_method_description WHERE shipping_method_id = " . (int)$shipping_method_id);
		if(isset($data['description'])) {
			foreach ($data['description'] as $language_id => $row) {
				$sql = "INSERT INTO " . DB_PREFIX . "ms_shipping_method_description
						SET shipping_method_id = " . (int)$shipping_method_id . ",
							name = '" . $this->db->escape($row['name']) . "',
							description = '" . $this->db->escape($row['description']) . "',
							language_id = " . (int)$language_id;

				$this->db->query($sql);
			}
		}

	}

	public function deleteShippingMethod($shipping_method_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_shipping_method WHERE shipping_method_id = " . (int)$shipping_method_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_shipping_method_description WHERE shipping_method_id = " . (int)$shipping_method_id);

		// Unset shipping method from all shipping rules
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_shipping_rule_cart_total` SET `shipping_method_id` = NULL WHERE `shipping_method_id` = " . (int)$shipping_method_id);
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_shipping_rule_cart_weight` SET `shipping_method_id` = NULL WHERE `shipping_method_id` = " . (int)$shipping_method_id);
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_shipping_rule_flat` SET `shipping_method_id` = NULL WHERE `shipping_method_id` = " . (int)$shipping_method_id);
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_shipping_rule_per_product` SET `shipping_method_id` = NULL WHERE `shipping_method_id` = " . (int)$shipping_method_id);
	}

	public function shippingMethodExists($shipping_method_id) {
		$exists = $this->db->query("SELECT 1 FROM " . DB_PREFIX . "ms_shipping_method WHERE shipping_method_id = " . (int)$shipping_method_id);
		return $exists->num_rows;
	}

	public function getShippingDeliveryTimes($data = array()) {
		$delivery_times_data = array();
		$sql = "SELECT 
					*
				FROM " . DB_PREFIX . "ms_shipping_delivery_time
				WHERE 1 = 1"

			. (isset($data['delivery_time_id']) ? " AND delivery_time_id = " . (int)$data['delivery_time_id'] : "");

		$delivery_time_ids = $this->db->query($sql);

		foreach ($delivery_time_ids->rows as $row) {
			$sql = "SELECT
						*
					FROM " . DB_PREFIX . "ms_shipping_delivery_time_description
					WHERE delivery_time_id = " . (int)$row['delivery_time_id']

				. (isset($data['language_id']) ? " AND language_id = " . (int)$data['language_id'] : "");

			$res = $this->db->query($sql);

			foreach ($res->rows as $time) {
				$delivery_times_data[$row['delivery_time_id']][$time['language_id']] = $time['name'];
			}
		}

		return $delivery_times_data;
	}

	public function createShippingDeliveryTime($data = array()) {
		if(!isset($data['delivery_time_id'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "ms_shipping_delivery_time () VALUES ()");
			$delivery_time_id = $this->db->getLastId();
		} else {
			$delivery_time_id = $data['delivery_time_id'];
		}

		foreach ($data['names'] as $item) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "ms_shipping_delivery_time_description
				SET delivery_time_id = " . (int)$delivery_time_id . ",
					name = '" . $this->db->escape($item['name']) . "',
					language_id = " . (int)$item['language_id'] . "
				ON DUPLICATE KEY UPDATE
					name = '" . $this->db->escape($item['name']) . "'");
		}

		return $delivery_time_id;
	}

	public function editShippingDeliveryTime($data = array()) {
		$this->db->query("UPDATE " . DB_PREFIX . "ms_shipping_delivery_time_description
			SET name = '" . $this->db->escape($data['name']) . "'
			WHERE delivery_time_id = " . (int)$data['delivery_time_id'] . "
				AND language_id = " . (int)$data['language_id']
		);
	}

	public function deleteShippingDeliveryTime($delivery_time_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_shipping_delivery_time WHERE delivery_time_id = " . (int)$delivery_time_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_shipping_delivery_time_description WHERE delivery_time_id = " . (int)$delivery_time_id);
	}
}
