<?php
class MsSuborder extends Model {
	/**
	 * Default order states.
	 *
	 * You can add more manually here.
	 */
	const STATE_PENDING = 1;
	const STATE_PROCESSING = 2;
	const STATE_COMPLETED = 3;
	const STATE_FAILED = 4;
	const STATE_CANCELLED = 5;

	/** suborder histories **/
	public function addSuborderHistory($data = array()) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_suborder_history
				SET suborder_id = " . (int)$data['suborder_id'] . ",
					order_status_id = " . (int)$data['order_status_id'] . ",
					comment = '" . $this->db->escape(isset($data['comment']) ? $data['comment'] : '') . "',
					date_added = NOW()";

		return $this->db->query($sql);
	}

	public function getSuborderHistory($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "ms_suborder_history
				WHERE 1 = 1"
			. (isset($data['suborder_id']) ? " AND suborder_id =  " .  (int)$data['suborder_id'] : '');

		$res = $this->db->query($sql);
		return ($res->num_rows == 1 && isset($data['single']) ? $res->row : $res->rows);
	}

	/** suborders **/
	public function createSuborder($data = [], $retrieve = false)
	{
		$sql = "
			INSERT INTO `" . DB_PREFIX . "ms_suborder`
			SET order_id = " . (int)$data['order_id'] . ",
				seller_id = " . (int)$data['seller_id'] . ",
				order_status_id = " . (int)$data['order_status_id'] . ",
				date_added = NOW()"

			. (isset($data['date_modified']) ? ", date_modified = '" . $this->db->escape($data['date_modified']) . "'" : "")
			. (!empty($data['customer_comment']) ? ", `customer_comment` = '" . $this->db->escape($data['customer_comment']) . "'" : "")
		;

		$this->db->query($sql);

		$suborder_id = $this->db->getLastId();

		if ($retrieve) {
			$suborder = $this->getSuborders([
				'suborder_id' => $suborder_id,
				'include_abandoned' => 1,
				'single' => 1
			]);

			return $suborder;
		}

		return $suborder_id;
	}

	public function getOrders($data = array(), $sort = array()) {
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					o.order_id,
					o.customer_id,
					o.firstname, o.lastname,
					(SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status,
					o.total,
					o.date_added, 
					o.date_modified
					FROM `" . DB_PREFIX . "order` o
					WHERE o.order_status_id > '0' "

			. (isset($data['order_id']) ? " AND o.order_id =  " .  (int)$data['order_id'] : '')

			. $filters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');
		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) {
			$res->rows[0]['total_rows'] = $total->row['total'];
			foreach ($res->rows as &$row){
				$query = $this->db->query("
					SELECT
						so.order_status_id,
						so.seller_id,
						sod.name as status_name,
						seller.nickname
					FROM " . DB_PREFIX . "ms_suborder so
					LEFT JOIN " . DB_PREFIX . "ms_suborder_status_description sod
						ON (so.order_status_id = sod.ms_suborder_status_id AND sod.language_id = '" . (int)$this->config->get('config_language_id') . "')
					LEFT JOIN " . DB_PREFIX . "ms_seller seller
						USING (seller_id)
					WHERE order_id = '" .  (int)$row['order_id'] . "'
				");

				$row['suborders'] = $query->rows;
			}
		}

		return $res->rows;
	}

	public function getTotalSuborders($seller_id)
	{
		$sql = "
			SELECT
				COUNT(1) as `total`
			FROM `" . DB_PREFIX . "ms_suborder`
			WHERE seller_id = " . (int)$seller_id . "
		";

		$result = $this->db->query($sql);

		return $result->num_rows ? $result->row['total'] : 0;
	}

	public function getSuborders($data = array(), $sort = array()) {
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS *,
				(SELECT nickname FROM " . DB_PREFIX . "ms_seller s WHERE s.seller_id = mso.seller_id) AS seller,
				mso.order_status_id as `suborder_status_id`,
				(SELECT name FROM " . DB_PREFIX . "ms_suborder_status_description msosd WHERE msosd.ms_suborder_status_id = mso.order_status_id AND msosd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status
				FROM " . DB_PREFIX . "ms_suborder mso
				LEFT JOIN `" . DB_PREFIX . "order` o
					ON (o.order_id = mso.order_id)
				WHERE 1 = 1 "
					. (isset($data['include_abandoned']) ? "" : " AND o.order_status_id > '0'")
					. (isset($data['seller_id']) ? " AND mso.seller_id =  " .  (int)$data['seller_id'] : "")
					. (isset($data['order_id']) ? " AND mso.order_id =  " .  (int)$data['order_id'] : "")
					. (isset($data['suborder_id']) ? " AND mso.suborder_id =  " .  (int)$data['suborder_id'] : "")
					. (isset($data['order_status_id']) ? " AND mso.order_status_id =  " .  (int)$data['order_status_id'] : "")
					. (isset($data['period_start']) ? " AND DATEDIFF(mso.date_added, '{$data['period_start']}') >= 0" : "")
					. $filters
					. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
					. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) {
			$res->rows[0]['total_rows'] = $total->row['total'];

			foreach ($res->rows as &$row) {
				$row['totals'] = $this->getSuborderTotals($row['order_id'], $row['seller_id']);
			}
		}

		return isset($data['single']) && !empty($res->rows[0]) ? $res->rows[0] : $res->rows;
	}

	public function getSellerSubordersList($seller_id, $data = [], $sort = [])
	{
		$filters = '';
		if(isset($sort['filters'])) {
			foreach($sort['filters'] as $k => $v) {
				$filters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				mso.order_id,
				mso.date_added,
				mso.order_status_id as `suborder_status_id`,
				(SELECT `name` FROM `" . DB_PREFIX . "ms_suborder_status_description` WHERE ms_suborder_status_id = mso.order_status_id AND language_id = '" . (int)$this->config->get('config_language_id') . "') AS status,
				CONCAT(o.firstname, ' ', o.lastname) as `customer_name`,
				(SELECT `value` FROM `" . DB_PREFIX . "ms_suborder_total` WHERE suborder_id = mso.suborder_id AND `code` LIKE 'total' LIMIT 1) as `suborder_total`
			FROM `" . DB_PREFIX . "ms_suborder` mso
			LEFT JOIN (SELECT order_id, firstname, lastname, order_status_id FROM `" . DB_PREFIX . "order`) o
				ON (o.order_id = mso.order_id)
			WHERE mso.seller_id = " . (int)$seller_id
				. (isset($data['include_abandoned']) ? "" : " AND o.order_status_id > '0'")
				. $filters
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '')
		;

		$res = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) {
			$res->rows[0]['total_rows'] = $total->row['total'];
		}

		return $res->rows;
	}

	public function getSuborderProductsCompact($order_id, $seller_id, $data = [])
	{
		$sql = "
			SELECT
				op.`product_id`,
				op.`quantity`,
				op.`name`,
				op.`total`"

			. (isset($data['full'])
				? ",
					op.`order_product_id`,
					op.`model`,
					op.`price`,
					op.`tax`,
					msopsd.shipping_cost as `individual_shipping_cost`,
					msopsd.shipping_method_name as `individual_shipping_method_name`,
					IF (msopsd.shipping_cost IS NULL, msssd.shipping_cost, NULL) as `combined_shipping_cost`,
					IF (msopsd.shipping_method_name IS NULL, msssd.shipping_method_name, NULL) as `combined_shipping_method_name`
				"
				: ""
			)

			. " FROM " . DB_PREFIX . "order_product op
			LEFT JOIN (SELECT order_product_id, seller_id FROM `" . DB_PREFIX . "ms_order_product_data`) msopd
				ON (msopd.order_product_id = op.order_product_id)"

			. (isset($data['full'])
				? " LEFT JOIN (SELECT order_product_id, shipping_cost, shipping_method_name FROM `" . DB_PREFIX . "ms_order_product_shipping_data`) msopsd
						ON (msopsd.order_product_id = op.order_product_id)
					LEFT JOIN (
						SELECT
							suborder_id,
							cost / (
								SELECT
									COUNT(1)
								FROM `" . DB_PREFIX . "ms_order_product_data` msopd2
								WHERE msopd2.`order_id` = " . (int)$order_id . "
									AND msopd2.`seller_id` = " . (int)$seller_id . "
									AND (SELECT shipping_cost FROM `" . DB_PREFIX . "ms_order_product_shipping_data` WHERE order_product_id = msopd2.order_product_id) IS NULL
							) as `shipping_cost`,
							shipping_method_name
						FROM `" . DB_PREFIX . "ms_suborder_shipping_data`
					) msssd
						ON (msssd.suborder_id = (SELECT suborder_id FROM `" . DB_PREFIX . "ms_suborder` WHERE `order_id` = " . (int)$order_id . " AND `seller_id` = " . (int)$seller_id . "))
				"
				: ""
			)

			. " WHERE op.order_id = " . (int)$order_id
				. " AND msopd.seller_id = " . (int)$seller_id
		;

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function getSuborderStatus($data = array()) {
		$res = $this->db->query("SELECT order_status_id
			FROM " . DB_PREFIX . "ms_suborder
			WHERE order_id = " . (int)$data['order_id'] . "
				AND seller_id = " . (int)$data['seller_id']
		);

		return isset($res->row['order_status_id']) ? $res->row['order_status_id'] : false;
	}

	public function updateSuborderStatus($data = array()) {
		$sql = "UPDATE " . DB_PREFIX . "ms_suborder
				SET order_status_id = " . (int)$data['order_status_id'] . "
				WHERE suborder_id = " . (int)$data['suborder_id'];

		$this->db->query($sql);
	}

	public function isValidSeller($suborder_id, $seller_id) {
		$sql = "SELECT
				seller_id
				FROM " . DB_PREFIX . "ms_suborder
				WHERE suborder_id = " . (int)$suborder_id;

		$res = $this->db->query($sql);

		return $seller_id == $res->row['seller_id'] ? true : false;
	}

	public function getSuborderTotals($order_id, $seller_id, $data = [])
	{
		$totals = [];

		$suborder = $this->db->query("
			SELECT
				suborder_id
			FROM `" . DB_PREFIX . "ms_suborder`
			WHERE `order_id` = " . (int)$order_id . "
				AND `seller_id` = " . (int)$seller_id . "
			ORDER BY `date_added` DESC LIMIT 1
		");

		if (!empty($suborder->row['suborder_id'])) {
			if (!empty($data['codes'])) {
				foreach ($data['codes'] as &$code) {
					$code = "'{$this->db->escape($code)}'";
				}
			}

			$sql = "
				SELECT
					*
				FROM `" . DB_PREFIX . "ms_suborder_total`
				WHERE `suborder_id` = " . (int)$suborder->row['suborder_id']

				. (isset($data['code']) ? " AND `code` = '" . $this->db->escape($data['code']) . "'" : "")
				. (isset($data['codes']) ? " AND `code` IN (" . implode(',', $data['codes']) . ")" : "")
			;

			$result = $this->db->query($sql);

			if ($result->num_rows) {
				$order_info = $this->db->query("SELECT currency_code FROM `" . DB_PREFIX . "order` WHERE `order_id` = " . (int)$order_id);
				$currency_code = !empty($order_info->row['currency_code']) ? $order_info->row['currency_code'] : $this->config->get('config_currency');

				foreach ($result->rows as $row) {
					$totals[$row['code']] = [
						'title' => $row['title'],
						'value' => $row['value'],
						'text' => $this->currency->format($row['value'], $currency_code),
						'sort_order' => $row['sort_order']
					];
				}
			}
		}

		return $totals;
	}

	public function createSuborderTotal($suborder_id, $code, $title, $value, $sort_order)
	{
		if (empty($code) || !isset($title) || !isset($value) || !isset($sort_order)) {
			return false;
		}

		// Skip if total value is 0 and it is not grand total
		if (0.0 === (float)$value && 'total' !== (string)$code) {
			return false;
		}

		$this->db->query("
			INSERT INTO `" . DB_PREFIX . "ms_suborder_total`
			SET `suborder_id` = {$suborder_id},
				`code` = '{$this->db->escape($code)}',
				`title` = '{$this->db->escape($title)}',
				`value` = {$value},
				`sort_order` = {$sort_order}
		");
	}

	public function saveSuborderCommissions($suborder_id, $data)
	{
		$sql = "
			UPDATE `" . DB_PREFIX . "ms_suborder`
			SET `suborder_id` = `suborder_id`"
			. (isset($data['flat']) ? ", `store_commission_flat` = " . (float)$data['flat'] : "")
			. (isset($data['percent']) ? ", `store_commission_pct` = " . (float)$data['percent'] : "")
			. " WHERE `suborder_id` = " . (int)$suborder_id
		;

		$this->db->query($sql);
	}
}