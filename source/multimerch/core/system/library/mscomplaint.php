<?php
class MsComplaint extends Model {
	public function getComplaints($sort = array()) {
		$sql = "SELECT SQL_CALC_FOUND_ROWS msc.*, CONCAT(cs.firstname, ' ', cs.lastname) as user_name, pd.name as product_name, mss.nickname as seller_name FROM `" . DB_PREFIX . "ms_complaint` msc";

		$sql .= " LEFT JOIN " . DB_PREFIX . "ms_seller mss USING(seller_id)";
		$sql .= " LEFT JOIN (SELECT product_id, name FROM " . DB_PREFIX . "product_description WHERE language_id = " . $this->config->get('config_language_id') . ") pd
				ON (pd.product_id = msc.product_id)";
		$sql .= " LEFT JOIN (SELECT customer_id, firstname, lastname FROM " . DB_PREFIX . "customer) cs USING(customer_id) "
			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
			. (isset($sort['limit']) ? " LIMIT " . (int) $sort['offset'] . ', ' . (int) ($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) {
			$res->rows[0]['total_rows'] = $total->row['total'];
		}

		return $res->rows;
	}

	public function createComplaint($data = array()) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "ms_complaint 
			SET customer_id = " . (int) $data['customer_id'] . ",
			    seller_id = " . (int) $data['seller_id'] . ",
			    product_id = " . (int) $data['product_id'] . ",
			    comment = '" . $this->db->escape($data['comment']) . "',
			    date_added = NOW()");

		return $this->db->getLastId();
	}

	public function deleteComplaint($complaint_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_complaint WHERE complaint_id = " . (int) $complaint_id);
	}

}
?>