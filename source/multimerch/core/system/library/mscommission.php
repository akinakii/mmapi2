<?php
class MsCommission extends Model {
	const RATE_SALE = 1;
	const RATE_LISTING = 2;
	const RATE_SIGNUP = 3;
	
	/*
	const PAYMENT_TYPE_BALANCE = 1;
	const PAYMENT_TYPE_GATEWAY = 2;
	const PAYMENT_TYPE_COMBINED = 3;
	*/
	
	const TYPE_SALES_QUANTITY = 1;
	const TYPE_SALES_AMOUNT = 2;
	const TYPE_PERIODIC = 3;
	const TYPE_DATE_UNTIL = 4;
	
	public function createCommission($rates) {
		if (!empty($rates)) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "ms_commission () VALUES ()");
			$commission_id = $this->db->getLastId();
			
			foreach ($rates as $type => $rate) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_commission_rate`
					SET `commission_id` = " . (int)$commission_id . ",
						`rate_type` = " . (int)$type . ",
						`flat` = " . (isset($rate['flat']) ? (float)$rate['flat'] : 0) . ",
						`percent` = " . (isset($rate['percent']) ? (float)$rate['percent'] : 0) . ",
						`payment_method` = " . (isset($rate['payment_method']) ? (float)$rate['payment_method'] : 'NULL')
				;

				$this->db->query($sql);
			}
		} else {
			$commission_id = null;
		}
		
		return $commission_id;
	}
	
	public function editCommission($commission_id, $rates) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_commission` WHERE `commission_id` = " . (int)$commission_id);
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_commission_rate` WHERE `commission_id` = " . (int)$commission_id);

		if (!empty($rates)) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_commission` (`commission_id`) VALUES (" . (int)$commission_id . ")");

			foreach ($rates as $type => $rate) {
				$sql = "
					INSERT INTO `" . DB_PREFIX . "ms_commission_rate`
					SET `commission_id` = " . (int)$commission_id . ",
						`rate_type` = " . (int)$type . ",
						`flat` = " . (isset($rate['flat']) ? (float)$rate['flat'] : 0) . ",
						`percent` = " . (isset($rate['percent']) ? (float)$rate['percent'] : 0) . ",
						`payment_method` = " . (isset($rate['payment_method']) ? (float)$rate['payment_method'] : 'NULL')
				;

				$this->db->query($sql);
			}
		} else {
			$commission_id = null;
		}
		
		return $commission_id;
	}
		
	// Get commissions
	public function getCommissionRates($commission_id) {
		$sql = "SELECT 	mcr.rate_id as 'mcr.rate_id',
						mcr.rate_type as 'mcr.rate_type',
						mcr.flat as 'mcr.flat',
						mcr.percent as 'mcr.percent',
						mcr.payment_method as 'mcr.payment_method'
				FROM `" . DB_PREFIX . "ms_commission_rate` mcr
				WHERE mcr.commission_id = " . (int)$commission_id . "
				ORDER BY mcr.rate_type";
				
		$res = $this->db->query($sql);
		$rates = array();

		if (empty($res->rows)) {
			foreach ([self::RATE_SALE, self::RATE_LISTING, self::RATE_SIGNUP] as $type) {
				$rates[$type] = array(
					'rate_id' => null,
					'rate_type' => $type,
					'flat' => 0,
					'percent' => 0,
					'payment_method' => null
				);
			}
		} else {
			foreach ($res->rows as $row) {
				$rates[$row['mcr.rate_type']] = array(
					'rate_id' => $row['mcr.rate_id'],
					'rate_type' => $row['mcr.rate_type'],
					'flat' => $row['mcr.flat'],
					'percent' => $row['mcr.percent'],
					'payment_method' => $row['mcr.payment_method'],
				);
			}
		}

		return $rates;
	}
}
