<?php
final class MsSeller extends Model {
	const STATUS_ACTIVE = 1; // fully functional
	const STATUS_INACTIVE = 2; // awaiting approvement
	const STATUS_DISABLED = 3; // disabled by admin
	const STATUS_DELETED = 4; // deleted by admin
	const STATUS_UNPAID = 5; // @todo 9.0: remove
	const STATUS_INCOMPLETE = 6; // @todo 9.0: remove
		
	const MS_SELLER_VALIDATION_NONE = 1;
	const MS_SELLER_VALIDATION_ACTIVATION = 2;
	const MS_SELLER_VALIDATION_APPROVAL = 3;

	private $isSeller = FALSE; 
	private $nickname;
	private $description;
	private $company;
	private $country_id;
	private $avatar;
	private $seller_status;

	public function __construct($registry) {
		parent::__construct($registry);

		//$this->log->write('creating seller object: ' . $this->session->data['customer_id']);
		if (isset($this->session->data['customer_id'])) {
			//TODO 
			//$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$this->session->data['customer_id'] . "' AND seller_status = '1'");
			$seller_query = $this->db->query("
				SELECT s.*, md.description as md_description FROM " . DB_PREFIX . "ms_seller s
				LEFT JOIN `" . DB_PREFIX . "ms_seller_description` md
					ON (s.seller_id = md.seller_id AND md.language_id = '" . (int)$this->config->get('config_language_id') . "')
				WHERE s.seller_id = '" . (int)$this->session->data['customer_id'] . "'
				");
			
			if ($seller_query->num_rows) {
				$this->isSeller = TRUE;
				$this->nickname = $seller_query->row['nickname'];
				$this->description = $seller_query->row['md_description'];
				//$this->company = $seller_query->row['company'];
				//$this->country_id = $seller_query->row['country_id'];
				$this->avatar = $seller_query->row['avatar'];
				$this->seller_status = $seller_query->row['seller_status'];
			}
		}
	}

  	public function isCustomerSeller($customer_id) {
		$res = $this->db->query("
			SELECT COUNT(*) as `total`
			FROM `" . DB_PREFIX . "ms_seller`
			WHERE `seller_id` = " . (int)$customer_id
		);

		return 0 === (int)$res->row['total'] ? false : true;
	}

	// @todo: think of removing this method and use getSellerFullName instead
	public function getSellerName($seller_id) {
		$sql = "SELECT firstname as 'firstname'
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['firstname'];
	}

	public function getSellerFullName($seller_id) {
		$sql = "SELECT CONCAT(firstname, ' ', lastname) as `name`,
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;

		$res = $this->db->query($sql);

		// @todo: unify language var for deleted customer for admin and front.
		return $res->num_rows && isset($res->row['name']) ? $res->row['name'] : $this->language->get('ms_questions_customer_deleted');
	}

	public function getSellerDescriptions($seller_id) {
		$seller_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_seller_description WHERE seller_id = '" . (int)$seller_id . "'");

		foreach ($query->rows as $result) {
			$seller_description_data[$result['language_id']] = array(
				'description' => $result['description'],
				'slogan' => $result['slogan']
			);
		}

		return $seller_description_data;
	}

	public function getSellerNickname($seller_id) {
		$sql = "SELECT nickname
				FROM `" . DB_PREFIX . "ms_seller`
				WHERE seller_id = " . (int)$seller_id;

		$res = $this->db->query($sql);

		return ($res->rows && isset($res->row['nickname'])) ? $res->row['nickname'] : '';
	}
	
	public function getSellerEmail($seller_id) {
		$sql = "SELECT email as 'email' 
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['email'];
	}

	public function getSellerStatus($seller_id) {
		$res = $this->db->query("
			SELECT `seller_status`
			FROM `" . DB_PREFIX . "ms_seller`
			WHERE seller_id = " . (int)$seller_id
		);

		return ($res->num_rows && isset($res->row['seller_status'])) ? $res->row['seller_status'] : 0;
	}

	public function updateSellerPositions() {
		// Set empty value google geolocation for all sellers
		$this->db->query("
			INSERT IGNORE INTO `" . DB_PREFIX . "ms_seller_setting` (seller_id, `name`, `value`)
			SELECT DISTINCT seller_id, 'slr_google_geolocation', '' FROM `" . DB_PREFIX . "ms_seller`
		");

		// Get all sellers without google geolocation
		$sellers = $this->db->query("SELECT DISTINCT seller_id FROM `" . DB_PREFIX . "ms_seller`");

		// Set google geolocation for all sellers with country
		$this->load->model('localisation/country');
		foreach ($sellers->rows as $seller) {
			$seller_ms_address = $this->getSellerMsAddress(['seller_id' => $seller['seller_id'], 'single' => true]);

			$country_name = '';
			if (!empty($seller_ms_address['country_id'])) {
				$country = $this->model_localisation_country->getCountry($seller_ms_address['country_id']);
				$country_name = (isset($country['name']) ? $country['name'] : '');
			}

			if ($country_name) {
				$geo_address = trim((!empty($seller_ms_address['city']) ? $seller_ms_address['city'] . ', ' : '') . $country_name);

				$position = $this->getSellerGoogleGeoLocation($geo_address);
				if ($position) {
					$this->db->query("
						UPDATE `" . DB_PREFIX . "ms_seller_setting`
						SET	`value` =  '" .  $this->db->escape($position) . "'
						WHERE `seller_id` = '" . (int)$seller['seller_id']. "'
							AND `name` = 'slr_google_geolocation'
					");
				}
			}
		}

		return true;
	}

	public function getSellerGoogleGeoLocation($address) {
		$result = false;

		if ($this->config->get('msconf_google_api_key')) {
			$user_api_key = trim($this->config->get('msconf_google_api_key'));
			$url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode(trim($address)) . '&key=' . $user_api_key;
			if( $curl = curl_init() ) {
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				$json_response = json_decode( curl_exec($curl) );
				curl_close($curl);

				if ( isset($json_response->status) AND $json_response->status == 'OK' ) {
					$lat_coord = $json_response->results[0]->geometry->location->lat;
					$lng_coord = $json_response->results[0]->geometry->location->lng;
					$result = '{"lat": '. $lat_coord . ', "lng": ' . $lng_coord . '}';
				}
			}
		}

		return $result;
	}

	public function processGoogleGeoLocationChange($seller_id, $data = array(), $create_setting = true) {
		$position = false;

		$this->load->model('localisation/country');
		$country = $this->model_localisation_country->getCountry(isset($data['country_id']) ? (int)$data['country_id'] : 0);

		if (!empty($country['name'])) {
			$geo_address = trim((!empty($data['city']) ? $data['city'] . ', ' : '') . $country['name']);
			$position = $this->getSellerGoogleGeoLocation($geo_address);

			if ($position && $create_setting) {
				$this->MsLoader->MsSetting->createSellerSetting(array(
					'seller_id' => $seller_id,
					'settings' => array(
						'slr_google_geolocation' => $position
					)
				));
			}
		}

		return $position;
	}

	public function nicknameTaken($nickname) {
		$sql = "SELECT nickname
				FROM `" . DB_PREFIX . "ms_seller` p
				WHERE p.nickname = '" . $this->db->escape($nickname) . "'";

		$res = $this->db->query($sql);

		return $res->num_rows;
	}

	public function createSeller($data) {
		$avatar = !empty($data['avatar_name']) ? $this->MsLoader->MsFile->moveImage($data['avatar_name']) : '';
		$banner = !empty($data['banner_name']) ? $this->MsLoader->MsFile->moveImage($data['banner_name']) : '';

		if (isset($data['commission'])) {
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		}
		
		$sql = "INSERT INTO " . DB_PREFIX . "ms_seller
				SET seller_id = " . (int)$data['seller_id'] . ",
					seller_status = " . (isset($data['status']) ? (int)$data['status'] : self::STATUS_INACTIVE) . ",
					seller_approved = " . (isset($data['approved']) ? (int)$data['approved'] : 0) . ",
					seller_group = " .  (isset($data['seller_group']) ? (int)$data['seller_group'] : $this->config->get('msconf_default_seller_group_id'))  .  ",
					nickname = '" . $this->db->escape($data['nickname']) . "',
					commission_id = " . (isset($commission_id) ? (int)$commission_id : 'NULL') . ",
					avatar = '" . $this->db->escape($avatar) . "',
					banner = '" . $this->db->escape($banner) . "',
					date_created = NOW()";

		$this->db->query($sql);
		$seller_id = $this->db->getLastId();

		// Description and slogan
		if (!empty($data['languages'])) {
			foreach ($data['languages'] as $language_id => $value) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_seller_description`
					SET `seller_id` = " . (int)$seller_id . ",
						`slogan` = '" . (isset($value['slogan']) ? $this->db->escape($value['slogan']) : '') . "',
						`description` = '" . (isset($value['description']) ? $this->db->escape($value['description']) : '') . "',
						`language_id` = '" . (int)$language_id . "'
				");
			}
		}

		// Settings
		if(!empty($data['settings'])) {
			if (!empty($data['settings']['slr_website']))
				$data['settings']['slr_website'] = $this->MsLoader->MsHelper->addHttp($data['settings']['slr_website']);

			$this->MsLoader->MsSetting->createSellerSetting($data);
		}

		// MultiMerch address
		if (!empty($data['address'])) {
			// Create ms_address
			$ms_address_id = $this->createSellerMsAddress($seller_id, $data['address']);

			$this->MsLoader->MsSetting->createSellerSetting(array(
				'seller_id' => $seller_id,
				'settings' => array(
					'slr_ms_address' => $ms_address_id
				)
			));

			if (isset($data['address']['country_id'])) {
				$this->processGoogleGeoLocationChange($seller_id, $data['address']);
			}
		}

		// Badges
		if (isset($data['badges'])) {
			foreach ($data['badges'] as $k => $badge_id) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_badge_seller_group` (`badge_id`, `seller_id`) VALUES (" . (int)$badge_id.",".(int)$seller_id . ")");
			}
		}

		// If seo keyword is not passed, generate it from seller's store name
		if (empty($data['keyword']))
			$data['keyword'] = $this->MsLoader->MsHelper->slugify($data['nickname']);

		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'seller_id=" . (int)$seller_id . "', `keyword` = '" . $this->db->escape($this->MsLoader->MsHelper->handleSlugDuplicate($data['keyword'])) . "'");

		$this->cache->delete('multimerch_seo_url');

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->editMsfSellerProperties($seller_id, $data);
		}
	}

	public function editSeller($data) {
		$seller_id = (int)$data['seller_id'];

		$this->editSellerProfile($data);

		// Badges
		if (!empty($data['badges'])) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_badge_seller_group` WHERE `seller_id` = " . (int)$seller_id);
			foreach ($data['badges'] as $k => $badge_id) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_badge_seller_group` (`badge_id`, `seller_id`) VALUES (" . (int)$badge_id.", " . (int)$seller_id . ")");
			}
		}

		// Commissions
		if (!$data['commission_id']) {
			$commission_id = $this->MsLoader->MsCommission->createCommission($data['commission']);
		} else {
			$commission_id = $this->MsLoader->MsCommission->editCommission($data['commission_id'], $data['commission']);
		}

		$this->db->query("
			UPDATE `" . DB_PREFIX . "ms_seller`
			SET `commission_id` = " . (isset($commission_id) ? (int)$commission_id : 'NULL') . "
			WHERE `seller_id` = " . (int)$seller_id
		);

		// SEO keyword
		// @todo 9.0: remove duplicates
		if (empty($data['keyword'])) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'seller_id=" . (int)$seller_id . "'");
		} else {
			$existing_keyword = $this->db->query("SELECT `keyword` FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'seller_id=" . (int)$seller_id . "' ORDER BY url_alias_id ASC LIMIT 1")->row;

			if (empty($existing_keyword['keyword'])) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'seller_id=" . (int)$seller_id . "', `keyword` = '" . $this->db->escape($this->MsLoader->MsHelper->handleSlugDuplicate($data['keyword'])) . "'");
			} elseif ((string)$data['keyword'] !== (string)$existing_keyword['keyword']) {
				$this->db->query("UPDATE `" . DB_PREFIX . "url_alias` SET `keyword` = '" . $this->db->escape($this->MsLoader->MsHelper->handleSlugDuplicate($data['keyword'])) . "' WHERE `query` = 'seller_id=" . (int)$seller_id . "'");
			}
		}

		$this->cache->delete('multimerch_seo_url');

		// MSF seller properties
		if ($this->config->get('msconf_msf_seller_property_enabled')) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id` WHERE `seller_id` = " . (int)$seller_id);
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_seller_msf_seller_property_value_text` WHERE `seller_id` = " . (int)$seller_id);

			// MSF seller properties
			$this->editMsfSellerProperties($seller_id, $data);
		}
	}

	public function editSellerProfile($data) {
		$seller_id = (int)$data['seller_id'];

		// Avatar
		$avatar = '';
		$old_avatar = $this->getSellerAvatar($seller_id);

		if (!isset($data['avatar_name']) || ($old_avatar['avatar'] != $data['avatar_name']))
			$this->MsLoader->MsFile->deleteImage($old_avatar['avatar']);

		if (isset($data['avatar_name']))
			$avatar = ($old_avatar['avatar'] != $data['avatar_name']) ? $this->MsLoader->MsFile->moveImage($data['avatar_name']) : $old_avatar['avatar'];

		// Banner
		$banner = '';
		$old_banner = $this->getSellerBanner($seller_id);

		if (!isset($data['banner_name']) || ($old_banner['banner'] != $data['banner_name']))
			$this->MsLoader->MsFile->deleteImage($old_banner['banner']);

		if (isset($data['banner_name']))
			$banner = ($old_banner['banner'] != $data['banner_name']) ? $this->MsLoader->MsFile->moveImage($data['banner_name']) : $old_banner['banner'];

		$this->db->query("
			UPDATE `" . DB_PREFIX . "ms_seller`
			SET `nickname` = '" . $this->db->escape($data['nickname'])  . "',
				`avatar` = '" . $this->db->escape($avatar) . "',
				`banner` = '" . $this->db->escape($banner) . "'"
				. (isset($data['status']) ? ", `seller_status` = " .  (int)$data['status'] : "")
				. (isset($data['seller_group']) ? ", `seller_group` = " .  (int)$data['seller_group'] : "")
				. (isset($data['approved']) ? ", `seller_approved` = " .  (int)$data['approved'] : "")

			. " WHERE seller_id = " . (int)$seller_id
		);

		// Description and slogan
		if (!empty($data['languages'])) {
			foreach ($data['languages'] as $language_id => $value) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_seller_description`
					SET `seller_id` = " . (int)$seller_id . ",
						`slogan` = '" . (isset($value['slogan']) ? $this->db->escape($value['slogan']) : '') . "',
						`description` = '" . (isset($value['description']) ? $this->db->escape($value['description']) : '') . "',
						`language_id` = '" . (int)$language_id . "'
					ON DUPLICATE KEY UPDATE
						`slogan` = '" . (isset($value['slogan']) ? $this->db->escape($value['slogan']) : '') . "',
						`description` = '" . (isset($value['description']) ? $this->db->escape($value['description']) : '') . "'
				");
			}
		}

		// Settings
		if (!empty($data['settings'])) {
			if (!empty($data['settings']['slr_website']))
				$data['settings']['slr_website'] = $this->MsLoader->MsHelper->addHttp($data['settings']['slr_website']);

			$this->MsLoader->MsSetting->createSellerSetting($data);
		}
	}

	public function editSellerAccountInfo($data = array()) {
		$seller_id = isset($data['seller_id']) ? (int)$data['seller_id'] : 0;

		$this->db->query("
			UPDATE `" . DB_PREFIX . "customer`
			SET customer_id = customer_id"
			. (isset($data['account']['firstname']) ? ", `firstname` = '" . $this->db->escape($data['account']['firstname']) . "'" : "")
			. (isset($data['account']['lastname']) ? ", `lastname` = '" . $this->db->escape($data['account']['lastname']) . "'" : "")
			. (isset($data['account']['email']) ? ", `email` = '" . $this->db->escape($data['account']['email']) . "'" : "")

			. " WHERE customer_id = '" . (int)$seller_id . "'
		");
	}

	public function getSellerAvatar($seller_id) {
		$query = $this->db->query("SELECT avatar as avatar FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");
		
		return $query->row;
	}

	public function getSellerBanner($seller_id) {
		$query = $this->db->query("SELECT banner as banner FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");

		return $query->row;
	}
		
	public function getNickname() {
		return $this->nickname;
	}

	public function getCompany() {
		return $this->company;
	}

	public function getCountryId() {
		return $this->country_id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getStatus() {
		return $this->seller_status;
	}

	public function isSeller() {
		return $this->isSeller;
	}
	
	public function getSalt($seller_id) {
		$sql = "SELECT salt
				FROM `" . DB_PREFIX . "customer`
				WHERE customer_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
		
		return $res->row['salt'];		
	}
	

	/********************************************************/
	
	
	public function getTotalSellers($data = array()) {
		$sql = "
			SELECT COUNT(*) as total
			FROM " . DB_PREFIX . "ms_seller ms
			WHERE 1 = 1 "
			. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '');

		$res = $this->db->query($sql);

		return $res->row['total'];
	}
	
	public function getSeller($seller_id, $data = array()) {
		$sql = "SELECT	CONCAT(c.firstname, ' ', c.lastname) as name,
						c.email as 'c.email',
						ms.seller_id as 'seller_id',
						ms.nickname as 'ms.nickname',
						ms.seller_status as 'ms.seller_status',
						ms.seller_approved as 'ms.seller_approved',
						ms.date_created as 'ms.date_created',
						ms.avatar as 'ms.avatar',
						ms.banner as 'banner',
						md.slogan as 'ms.slogan',
						md.description as 'ms.description',
						ms.commission_id as 'ms.commission_id',
						ms.seller_group as 'ms.seller_group',
						(SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE `query` = 'seller_id=" . (int)$seller_id . "' LIMIT 1) AS keyword
				FROM `" . DB_PREFIX . "customer` c
				INNER JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_product` mp
					ON (c.customer_id = mp.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller_description` md
					ON (c.customer_id = md.seller_id AND md.language_id = '" . (int)$this->config->get('config_language_id') . "')
				WHERE ms.seller_id = " .  (int)$seller_id
				. (isset($data['product_id']) ? " AND mp.product_id =  " .  (int)$data['product_id'] : '')
				. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '')
				. " GROUP BY ms.seller_id
				LIMIT 1";
				
		$res = $this->db->query($sql);
		if (!isset($res->row['seller_id']) || !$res->row['seller_id']) {
			return FALSE;
		} else {
			$res->row['descriptions'] = $this->getSellerDescriptions($res->row['seller_id']);
			$res->row['product_validation'] = $this->MsLoader->MsSetting->calculateSellerSettingValue($res->row['seller_id'], 'slr_product_validation', 'slr_gr_product_validation');

			return $res->row;
		}
	}
	
	public function getSellers($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`c.name`" => 1, "`ms.date_created`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}
		
		$sql = "SELECT
					SQL_CALC_FOUND_ROWS"
					// additional columns
					. (isset($cols['total_products']) ? "
						(SELECT COUNT(1) FROM " . DB_PREFIX . "ms_product mp WHERE mp.seller_id = ms.seller_id
						) as total_products,
					" : "")
					
					. (isset($cols['current_balance']) ? "
						(SELECT COALESCE(
							(SELECT balance FROM " . DB_PREFIX . "ms_balance
								WHERE seller_id = ms.seller_id  
								ORDER BY balance_id DESC
								LIMIT 1
							),
							0
						)) as current_balance,
					" : "")

					. (isset($cols['total_sales']) ? "
						(SELECT count(1) as total FROM `" . DB_PREFIX . "ms_suborder` mss
						LEFT JOIN (SELECT order_id, order_status_id FROM `" . DB_PREFIX . "order`) o
							ON (mss.order_id = o.order_id)
						WHERE mss.seller_id = ms.seller_id AND o.order_status_id <> 0) as total_sales,
					" : "")

					// default columns
					." CONCAT(c.firstname, ' ', c.lastname) as 'c.name',
					c.email as 'c.email',
					ms.seller_id as 'seller_id',
					ms.nickname as 'ms.nickname',
					ms.seller_status as 'ms.seller_status',
					ms.seller_approved as 'ms.seller_approved',
					ms.date_created as 'ms.date_created',
					ms.avatar as 'ms.avatar',
					ms.banner as 'banner',
					md.description as 'ms.description'
				FROM `" . DB_PREFIX . "customer` c
				INNER JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller_description` md
					ON (c.customer_id = md.seller_id AND md.language_id = '" . (int)$this->config->get('config_language_id') . "')
				WHERE 1 = 1 "
				. (isset($data['seller_id']) ? " AND ms.seller_id =  " .  (int)$data['seller_id'] : '')
				. (isset($data['seller_group_id']) ? " AND ms.seller_group =  " .  (int)$data['seller_group_id'] : '')
				. (isset($data['seller_status']) ? " AND seller_status IN  (" .  $this->db->escape(implode(',', $data['seller_status'])) . ")" : '')
				. (!empty($data['nickname']) ? " AND ms.nickname LIKE '" .  $this->db->escape($data['nickname']) . "'" : '')

				. $wFilters
				
				. " GROUP BY ms.seller_id HAVING 1 = 1 "
				
				. $hFilters
				
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->rows) $res->rows[0]['total_rows'] = $total->row['total'];

		return isset($data['single']) ? $res->row : $res->rows;
	}
	
	public function getCustomers($sort = array()) {
		$sql = "SELECT  CONCAT(c.firstname, ' ', c.lastname) as 'c.name',
						c.email as 'c.email',
						c.customer_id as 'c.customer_id',
						ms.seller_id as 'seller_id'
				FROM `" . DB_PREFIX . "customer` c
				LEFT JOIN `" . DB_PREFIX . "ms_seller` ms
					ON (c.customer_id = ms.seller_id)
				WHERE ms.seller_id IS NULL"
				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		
		return $res->rows;
	}
	
	public function getTotalEarnings($seller_id, $data = array()) {
		// note: update getSellers() if updating this
		$sql = "SELECT COALESCE(SUM(amount),0)
					   - (SELECT COALESCE(ABS(SUM(amount)),0)
						  FROM `" . DB_PREFIX . "ms_balance`
						  WHERE seller_id = " . (int)$seller_id . "
						  AND balance_type = ". MsBalance::MS_BALANCE_TYPE_REFUND
						  . (isset($data['period_start']) ? " AND DATEDIFF(date_created, '{$data['period_start']}') >= 0" : "")
				. ") as total
				FROM `" . DB_PREFIX . "ms_balance`
				WHERE seller_id = " . (int)$seller_id . "
				AND balance_type IN (". implode(',', array(MsBalance::MS_BALANCE_TYPE_SALE, MsBalance::MS_BALANCE_TYPE_SHIPPING)) . ")"
				. (isset($data['period_start']) ? " AND DATEDIFF(date_created, '{$data['period_start']}') >= 0" : "");

		$res = $this->db->query($sql);
		return $res->row['total'];
	}
	
	public function changeStatus($seller_id, $seller_status) {
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET	seller_status =  " .  (int)$seller_status . "
				WHERE seller_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
	}
	
	public function changeApproval($seller_id, $approved) {
		$sql = "UPDATE " . DB_PREFIX . "ms_seller
				SET	approved =  " .  (int)$approved . "
				WHERE seller_id = " . (int)$seller_id;
		
		$res = $this->db->query($sql);
	}
	
	public function deleteSeller($seller_id) {
		// Change status of all related products to DELETED
		$products = $this->MsLoader->MsProduct->getProducts(array('seller_id' => $seller_id));
		foreach ($products as $product) {
			$this->MsLoader->MsProduct->changeStatus($product['product_id'], MsProduct::STATUS_DELETED);

			// Unbind deleted seller from his products
			$this->db->query("UPDATE `" . DB_PREFIX . "ms_product` SET `seller_id` = NULL WHERE `product_id` = " . (int)$product['product_id']);

			// Delete all fixed-shipping settings for these products
			$this->MsLoader->MsShipping->deletePerProductRule($seller_id, $product['product_id']);
		}

		// Delete all seller's settings
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_setting WHERE seller_id = '" . (int)$seller_id . "'");

		// Delete seller's social channels information
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_channel WHERE seller_id = '" . (int)$seller_id . "'");

		// Delete seller's description
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller_description WHERE seller_id = '" . (int)$seller_id . "'");

		// Delete seller's global shipping settings
		$this->MsLoader->MsShipping->deleteCartWeightBasedRule($seller_id);
		$this->MsLoader->MsShipping->deleteCartTotalBasedRule($seller_id);
		$this->MsLoader->MsShipping->deleteFlatRule($seller_id);

		// Delete seller's badges
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_badge_seller_group WHERE seller_id = " . (int)$seller_id);

		// Delete seller's balance records
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_balance WHERE seller_id = '" . (int)$seller_id . "'");

		// Delete seller's payment requests and payments
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_pg_payment WHERE seller_id = '" . (int)$seller_id . "'");
		// @todo 9.0 Invoicing: decide whether to delete or not all unpaid invoices on seller deletion
		/*$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_invoice` WHERE
			(`sender_type` = 'seller' AND `sender_id` = " . (int)$seller_id . ")
			OR
			(`recipient_type` = 'seller' AND `recipient_id` = " . (int)$seller_id . ")
			AND `status` = " . (int)\MultiMerch\Core\Invoice\Invoice::STATUS_UNPAID
		);*/

		// Delete seller's MsAttribute groups
		$seller_attribute_groups = $this->MsLoader->MsAttribute->getAttributeGroups(array('seller_ids' => $seller_id));
		foreach ($seller_attribute_groups as $seller_attribute_group) {
			$this->MsLoader->MsAttribute->deleteAttributeGroup($seller_attribute_group['attribute_group_id']);
		}

		// Delete seller's MsAttributes (just in case)
		$seller_attributes = $this->MsLoader->MsAttribute->getAttributes(array('seller_ids' => $seller_id));
		foreach ($seller_attributes as $seller_attribute) {
			$this->MsLoader->MsAttribute->deleteAttribute($seller_attribute['attribute_id']);
		}

		// Delete seller's MsCategories
		$seller_categories = $this->MsLoader->MsCategory->getCategories(array('seller_ids' => $seller_id));
		foreach ($seller_categories as $seller_category) {
			$this->MsLoader->MsCategory->deleteCategory($seller_category['category_id']);
		}

		// Delete seller's MsOptions
		$seller_options = $this->MsLoader->MsOption->getOptions(array('seller_ids' => $seller_id));
		foreach ($seller_options as $seller_option) {
			$this->MsLoader->MsOption->deleteOption($seller_option['option_id']);
		}

		// Delete seller's coupons
		$seller_coupons = $this->MsLoader->MsCoupon->getCoupons(array('seller_id' => $seller_id));
		foreach ($seller_coupons as $seller_coupon) {
			$this->MsLoader->MsCoupon->deleteCoupon($seller_coupon['coupon_id']);
		}

		// Delete seller's SEO url
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` = 'seller_id=".(int)$seller_id."'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_seller WHERE seller_id = '" . (int)$seller_id . "'");

		$this->cache->delete('multimerch_seo_url');
	}

	public function getSellerMsCategories($seller_id) {
		$sql = "SELECT
					msc.category_id,
					msc.parent_id,
					mscd.name,
					COUNT(DISTINCT msp2c.product_id) AS total
				FROM " . DB_PREFIX . "ms_category msc
				LEFT JOIN " . DB_PREFIX . "ms_category_description mscd
					USING (category_id)
				LEFT JOIN " . DB_PREFIX . "ms_product_to_category msp2c
					ON (msc.category_id = msp2c.ms_category_id)
				LEFT JOIN " . DB_PREFIX . "product p
					USING (product_id)
				LEFT JOIN " . DB_PREFIX . "ms_product msp
					ON (msp2c.product_id = msp.product_id AND p.status = 1 AND p.date_available <= NOW())
				WHERE msp.seller_id = '" . (int)$seller_id . "'
					AND mscd.language_id = '" . (int)$this->config->get('config_language_id') . "'
					AND msp.product_status = '" . MsProduct::STATUS_ACTIVE . "'
				GROUP BY msp2c.ms_category_id";

		$res = $this->db->query($sql);

		foreach ($res->rows as &$row) {
			$row['path'] = $this->MsLoader->MsCategory->getMsCategoryPath($row['category_id']);
		}

		return $res->rows;
	}

	public function getSellerCustomers($seller_id, $data = array()) {
		$result = $this->db->query("
			SELECT DISTINCT
				o.customer_id,
				CONCAT_WS(' ', c.firstname, c.lastname) as `customer_name`
			FROM `" . DB_PREFIX . "ms_suborder` mss
			LEFT JOIN (SELECT order_id, customer_id FROM `" . DB_PREFIX . "order`) o
				ON (o.order_id = mss.order_id)
			LEFT JOIN (SELECT customer_id, firstname, lastname FROM `" . DB_PREFIX . "customer`) c
				ON (c.customer_id = o.customer_id)
			WHERE mss.seller_id = " . (int)$seller_id

			. (isset($data['name']) ? " AND `customer_name` = '" . $this->db->escape($data['name']) . "'" : "")
		);

		return $result->rows;
	}

	public function getSellerMsAddress($data = array()) {
		$result = $this->db->query("
			SELECT
				*
			FROM `" . DB_PREFIX . "ms_seller_address`
			WHERE 1 = 1"
			. (isset($data['seller_id']) ? " AND `seller_id` = '" . (int)$data['seller_id'] . "'" : "")
			. (isset($data['address_id']) ? " AND `address_id` = '" . (int)$data['address_id'] . "'" : "")
		);

		return isset($data['single']) ? $result->row : $result->rows;
	}

	public function createSellerMsAddress($seller_id, $data = array()) {
		$this->db->query("
			INSERT IGNORE INTO `" . DB_PREFIX . "ms_seller_address`
			SET `seller_id` = '" . (int)$seller_id . "',
				`fullname` = '" . (isset($data['fullname']) ? $this->db->escape($data['fullname']) : '') . "',
				`address_1` = '" . (isset($data['address_1']) ? $this->db->escape($data['address_1']) : '') . "',
				`address_2` = '" . (isset($data['address_2']) ? $this->db->escape($data['address_2']) : '') . "',
				`city` = '" . (isset($data['city']) ? $this->db->escape($data['city']) : '') . "',
				`state` = '" . (isset($data['state']) ? $this->db->escape($data['state']) : '') . "',
				`zip` = '" . (isset($data['zip']) ? $this->db->escape($data['zip']) : '') . "',
				`country_id` = '" . (isset($data['country_id']) ? (int)$data['country_id'] : $this->config->get('config_country_id')) . "'
		");

		$ms_address_id = $this->db->getLastId();

		return $ms_address_id;
	}

	public function editSellerMsAddress($data = array()) {
		$seller_id = isset($data['seller_id']) ? (int)$data['seller_id'] : 0;

		$ms_address = isset($data['address']) ? $data['address'] : FALSE;
		$ms_address_id = isset($ms_address['address_id']) ? (int)$ms_address['address_id'] : 0;

		// Get seller's ms_address and update it only if it exists
		$old_ms_address = $this->getSellerMsAddress(array('address_id' => $ms_address_id, 'single' => 1));

		if (!empty($old_ms_address)) {
			// Update existing ms_address
			$this->db->query("
				UPDATE `" . DB_PREFIX . "ms_seller_address`
				SET address_id = address_id"
					. (isset($ms_address['fullname']) ? ", `fullname` = '" . $this->db->escape($ms_address['fullname']) . "'" : "")
					. (isset($ms_address['address_1']) ? ", `address_1` = '" . $this->db->escape($ms_address['address_1']) . "'" : "")
					. (isset($ms_address['address_2']) ? ", `address_2` = '" . $this->db->escape($ms_address['address_2']) . "'" : "")
					. (isset($ms_address['city']) ? ", `city` = '" . $this->db->escape($ms_address['city']) . "'" : "")
					. (isset($ms_address['state']) ? ", `state` = '" . $this->db->escape($ms_address['state']) . "'" : "")
					. (isset($ms_address['zip']) ? ", `zip` = '" . $this->db->escape($ms_address['zip']) . "'" : "")
					. (isset($ms_address['country_id']) ? ", `country_id` = '" . (int)$ms_address['country_id'] . "'" : "")

					. " WHERE address_id = '" . (int)$ms_address_id . "'
						AND seller_id = '" . (int)$seller_id . "'
			");

			// If country and city are not set, remove seller's Google geolocation coordinates
			if (!isset($ms_address['country_id']) && !isset($ms_address['city'])) {
				$this->MsLoader->MsSetting->createSellerSetting(array(
					'seller_id' => $seller_id,
					'settings' => array(
						'slr_google_geolocation' => ''
					)
				));
			}

			// Process seller's Google geolocation change when country or city (or both) are changed
			if (
				(isset($old_ms_address['country_id']) && isset($ms_address['country_id']) && (int)$old_ms_address['country_id'] !== (int)$ms_address['country_id'])
				||
				(isset($old_ms_address['city']) && isset($ms_address['city']) && (string)$old_ms_address['city'] !== (string)$ms_address['city'])
			) {
				$this->processGoogleGeoLocationChange($seller_id, $ms_address);
			}
		}
	}

	public function deleteSellerMsAddress($seller_id, $data = array()) {
		$this->db->query("
			DELETE FROM `" . DB_PREFIX . "ms_seller_address`
			WHERE seller_id = '" . (int)$seller_id . "'"
			. (isset($data['address_id']) ? " AND `address_id` = '" . (int)$data['address_id'] . "'" : "")
		);
	}
	
	public function editSellerHolidayMode($data = array()) {
		$seller_id = (int) $data['seller_id'];
		$holiday_mode_note = FALSE;
		if (!empty($data['languages'])) {
			foreach ($data['languages'] as $language_id => $value) {
				$holiday_mode_note[$language_id] = trim($value['slr_holiday_mode_note']);
			}
		}

		$this->MsLoader->MsSetting->createSellerSetting(array(
			'seller_id' => $seller_id,
			'settings' => array(
				'slr_holiday_mode_enabled' => (int) $data['settings']['slr_holiday_mode_enabled']
			)
		));
		if ($holiday_mode_note) {
			$this->MsLoader->MsSetting->createSellerSetting(array(
				'seller_id' => $seller_id,
				'settings' => array(
					'slr_holiday_mode_note' => json_encode($holiday_mode_note)
				)
			));
		}
	}
	
	public function isSellerOnHolidayMode($seller_id) {
		$settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
		return !empty($settings['slr_holiday_mode_enabled']);
	}

	public function getSellerHolidayModeNote($seller_id) {
		$settings = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $seller_id]);
		$language_id = (int) $this->config->get('config_language_id');
		if (!empty($settings['slr_holiday_mode_note'])) {
			$settings['slr_holiday_mode_note'] = json_decode($settings['slr_holiday_mode_note'], true);
			if ($settings['slr_holiday_mode_note'][$language_id]) {
				$str = sprintf($this->language->get('ms_front_holiday_mode_warning'), $settings['slr_holiday_mode_note'][$language_id]);
			} else {
				$str = $this->language->get('ms_front_holiday_mode_warning_empty');
			}
			return $str;
		} else return '';
	}

	public function getFeeRates($seller_id)
	{
		$seller = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_seller` WHERE `seller_id` = " . (int)$seller_id)->row;

		$rates = [];

		if (!empty($seller['commission_id'])) {
			$rates = $this->MsLoader->MsCommission->getCommissionRates($seller['commission_id']);
		} elseif (!empty($seller['seller_group'])) {
			$rates = $this->MsLoader->MsSellerGroup->getFeeRates($seller['seller_group']);
		}

		return $rates;
	}

	// MSF seller properties
	public function editMsfSellerProperties($seller_id, $data = [])
	{
		if (!$this->config->get('msconf_msf_seller_property_enabled') || empty($data['msf_seller_properties'])) {
			return false;
		}

		foreach ($data['msf_seller_properties'] as $msf_seller_property_id => $values) {
			if (!empty($values['id_values'])) {
				foreach ($values['id_values'] as $value_id) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_seller_msf_seller_property_value_id`
						SET `seller_id` = " . (int)$seller_id . ",
							`msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
							`msf_seller_property_value_id` = " . (int)$value_id
					);
				}
			} elseif (!empty($values['text_values'])) {
				foreach ($values['text_values'] as $language_id => $value_text) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_seller_msf_seller_property_value_text`
						SET `seller_id` = " . (int)$seller_id . ",
							`msf_seller_property_id` = " . (int)$msf_seller_property_id . ",
							`language_id` = " . (int)$language_id . ",
							`value` = '" . $this->db->escape($value_text) . "'
					");
				}
			}
		}
	}
}
