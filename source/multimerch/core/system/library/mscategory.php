<?php
class MsCategory extends Model {
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;
	const STATUS_DISABLED = 4;


	/* ============================================   MS CATEGORIES   =============================================== */


	/**
	 * Gets MsCategory(ies) created by seller(s).
	 *
	 * @param	array	$data	Conditions.
	 * @param	array	$sort	Data for sorting or filtering results.
	 * @param	array	$cols	Data for sorting or filtering results.
	 * @return	array			MsCategory(ies) created by seller(s).
	 */
	public function getCategories($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`c.name`" => 1, "`ms.date_created`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		$sql = "SELECT
					SQL_CALC_FOUND_ROWS
					msc.*,
					mscd.*"

				. (isset($data['category_id']) ?
					", (SELECT DISTINCT keyword
						FROM " . DB_PREFIX . "url_alias
						WHERE `query` = 'ms_category_id=" . (int)$data['category_id'] . "'
					) AS keyword"
				: "")

				. ", @path := (SELECT GROUP_CONCAT(mscd1.name ORDER BY `level` SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;')
					FROM `" . DB_PREFIX . "ms_category_path` mscp
					LEFT JOIN `" . DB_PREFIX . "ms_category_description` mscd1
						ON (mscp.path_id = mscd1.category_id AND mscp.category_id != mscp.path_id)
					WHERE
						mscp.category_id = msc.category_id
						AND mscd1.language_id = '" . (int)$this->config->get('config_language_id') . "'
					GROUP BY mscp.category_id) AS path,
					IF(@path IS NULL, mscd.name, CONCAT(@path, '&nbsp;&nbsp;&gt;&nbsp;&nbsp;', mscd.name)) as `full_path`,
					mss.nickname
				FROM `" . DB_PREFIX . "ms_category` msc
				LEFT JOIN `" . DB_PREFIX . "ms_category_description` mscd
					USING (category_id)
				LEFT JOIN `" . DB_PREFIX . "ms_seller` mss
					ON (mss.seller_id = msc.seller_id)
				WHERE mscd.language_id = '" . (int)$this->config->get('config_language_id') . "'"

				. (isset($data['category_id']) ? " AND msc.category_id = '" . (int)$data['category_id'] . "'" : "")
				. (isset($data['parent_id']) ? " AND msc.parent_id = '" . (int)$data['parent_id'] . "'" : "")
				. (isset($data['category_status']) ? " AND msc.category_status = '" . (int)$data['category_status'] . "'" : "")
				. (isset($data['seller_ids']) ? " AND msc.seller_id IN (" . $data['seller_ids'] . ")" : "")
				. (isset($data['exclude_category_ids']) ? " AND msc.category_id NOT IN (" . $data['exclude_category_ids'] . ")" : "")

				. $wFilters

				. " GROUP BY msc.category_id HAVING 1 = 1 "

				. $hFilters

				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($res->num_rows) {
			if(isset($data['single'])) {
				$res->row['total_rows'] = $total->row['total'];
			} else {
				$res->rows[0]['total_rows'] = $total->row['total'];
			}
		}

		if(isset($data['category_id'])) {
			$res->row['languages'] = $this->_getDescriptions($data['category_id']);
			$res->row['filters'] = $this->_getFilters($data['category_id']);
			$res->row['stores'] = $this->_getStores($data['category_id']);
			if (isset($data['ms_path'])){
				$res->row['ms_path'] =  $this->getMsCategoryPath($data['category_id']);
			}
		}

		return ($res->num_rows && isset($data['single'])) ? $res->row : $res->rows;
	}

	/**
	 * Creates seller's category.
	 *
	 * @param	array	$data			Conditions.
	 * @return	int		$category_id	Category id.
	 */
	public function createCategory($data = array()) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category`
				SET parent_id = '" . (int)$data['parent_id'] . "',
					seller_id = '" . (int)$data['seller_id'] . "',
					sort_order = '" . (int)$data['sort_order'] . "',
					category_status = '" . (int)$data['status'] . "'"
				. (isset($data['image']) ? ", `image` = '" . $this->db->escape($data['image']) . "'" : ""));

		$category_id = $this->db->getLastId();

		// descriptions
		if (isset($data['category_description'])) $this->_saveDescriptions($category_id, $data['category_description']);

		// filters
		if (isset($data['category_filter'])) $this->_saveFilters($category_id, $data['category_filter']);

		// category to store
		if (isset($data['category_store'])) $this->_saveStores($category_id, $data['category_store']);

		// seo keyword
		if (!empty($data['keyword'])) $this->_saveKeyword($category_id, $data['keyword']);

		// category path
		if(isset($data['parent_id'])) $this->_savePath($category_id, $data);

		return $category_id;
	}
	
	/**
	 * Creates marketplace's (opencart) category - based on opencart admin category model
	 *
	 * @param	array	$data			Conditions.
	 * @return	int		$category_id	Category id.
	 */
	public function createOcCategory($data = array()){
		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = 0, `column` = 1, sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$category_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		// Set which layout to use with this category
		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		if (empty($data['category_ms_fee']['commission_id'])) {
			$commission_id = $this->MsLoader->MsCommission->createCommission(!empty($data['category_ms_fee']['commission_rates']) ? $data['category_ms_fee']['commission_rates'] : []);
		} else {
			$commission_id = $this->MsLoader->MsCommission->editCommission($data['category_ms_fee']['commission_id'], !empty($data['category_ms_fee']['commission_rates']) ? $data['category_ms_fee']['commission_rates'] : []);
		}

		$this->saveCategoryCommission($category_id, $commission_id);

		// MM product attributes
		if ($this->config->get('msconf_msf_attribute_enabled') && !empty($data['msf_attributes'])) {
			foreach ($data['msf_attributes'] as $sort_order => $msf_attribute_id) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_oc_category_msf_attribute`
					SET `oc_category_id` = " . (int)$category_id . ",
						`msf_attribute_id` = " . (int)$msf_attribute_id . ",
						`sort_order` = " . (int)$sort_order
				);
			}
		}

		// MM product variations
		if ($this->config->get('msconf_msf_variation_enabled') && !empty($data['msf_variations'])) {
			foreach ($data['msf_variations'] as $sort_order => $msf_variation_id) {
				$this->db->query("
					INSERT INTO `" . DB_PREFIX . "ms_oc_category_msf_variation`
					SET `oc_category_id` = " . (int)$category_id . ",
						`msf_variation_id` = " . (int)$msf_variation_id . ",
						`sort_order` = " . (int)$sort_order
				);
			}
		}

		// MM product filter blocks
		// Product filter blocks status
		$this->load->model('setting/setting');
		$mspf_settings = $this->model_setting_setting->getSetting('multimerch_productfilter');
		if (!empty($mspf_settings['multimerch_productfilter_status']) && !empty($data['mspf_blocks'])) {
			$this->MsLoader->MsProductFilter->saveOcCategoryMspfBlocks($category_id, $data['mspf_blocks']);
		}

		$this->cache->delete('category');

		return $category_id;
	}
	
	/**
	 * Updates marketplace (opencart) category - based on opencart catalog model
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$data			Conditions.
	 */
	public function updateOcCategory($category_id, $data = array()) {
		$this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = 0, `column` = '1', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

		if ($query->rows) {
			foreach ($query->rows as $category_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

				$path = array();

				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Combine the paths with a new level
				$level = 0;

				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		if (empty($data['category_ms_fee']['commission_id'])) {
			$commission_id = $this->MsLoader->MsCommission->createCommission(!empty($data['category_ms_fee']['commission_rates']) ? $data['category_ms_fee']['commission_rates'] : []);
		} else {
			$commission_id = $this->MsLoader->MsCommission->editCommission($data['category_ms_fee']['commission_id'], !empty($data['category_ms_fee']['commission_rates']) ? $data['category_ms_fee']['commission_rates'] : []);
		}

		$this->saveCategoryCommission($category_id, $commission_id);

		// MM product attributes
		if ($this->config->get('msconf_msf_attribute_enabled')) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_oc_category_msf_attribute` WHERE `oc_category_id` = " . (int)$category_id);

			if (!empty($data['msf_attributes'])) {
				foreach ($data['msf_attributes'] as $sort_order => $msf_attribute_id) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_oc_category_msf_attribute`
						SET `oc_category_id` = " . (int)$category_id . ",
							`msf_attribute_id` = " . (int)$msf_attribute_id . ",
							`sort_order` = " . (int)$sort_order
					);
				}
			}
		}

		// MM product variations
		if ($this->config->get('msconf_msf_variation_enabled')) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_oc_category_msf_variation` WHERE `oc_category_id` = " . (int)$category_id);

			if (!empty($data['msf_variations'])) {
				foreach ($data['msf_variations'] as $sort_order => $msf_variation_id) {
					$this->db->query("
						INSERT INTO `" . DB_PREFIX . "ms_oc_category_msf_variation`
						SET `oc_category_id` = " . (int)$category_id . ",
							`msf_variation_id` = " . (int)$msf_variation_id . ",
							`sort_order` = " . (int)$sort_order
					);
				}
			}
		}

		// MM product filter blocks
		// Product filter blocks status
		$this->load->model('setting/setting');
		$mspf_settings = $this->model_setting_setting->getSetting('multimerch_productfilter');
		if (!empty($mspf_settings['multimerch_productfilter_status']) && !empty($data['mspf_blocks'])) {
			$this->MsLoader->MsProductFilter->saveOcCategoryMspfBlocks($category_id, $data['mspf_blocks']);
		}

		$this->cache->delete('category');
	}
	
	/**
	 * Updates seller's category.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$data			Conditions.
	 */
	public function updateCategory($category_id, $data = array()) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_description WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_filter WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_to_store WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE 'ms_category_id=" . (int)$category_id . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "ms_category
			SET parent_id = '" . (int)$data['parent_id'] . "',
				seller_id = '" . (int)$data['seller_id'] . "',
				sort_order = '" . (int)$data['sort_order'] . "',
				category_status = '" . (int)$data['status'] . "'"
			. (isset($data['image']) ? ", `image` = '" . $this->db->escape($data['image']) . "'" : "")

			. " WHERE category_id = '" . (int)$category_id . "'");

		// descriptions
		if (isset($data['category_description'])) $this->_saveDescriptions($category_id, $data['category_description']);

		// filters
		if (isset($data['category_filter'])) $this->_saveFilters($category_id, $data['category_filter']);

		// category to store
		if (isset($data['category_store'])) $this->_saveStores($category_id, $data['category_store']);

		// seo keyword
		if (!empty($data['keyword'])) $this->_saveKeyword($category_id, $data['keyword']);

		// category path
		if(isset($data['parent_id'])) {
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

			if ($query->rows) {
				foreach ($query->rows as $category_path) {
					// Delete the path below the current one
					$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

					$path = array();

					// Get the nodes new parents
					$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

					foreach ($query->rows as $result) {
						$path[] = $result['path_id'];
					}

					// Get whats left of the nodes current path
					$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

					foreach ($query->rows as $result) {
						$path[] = $result['path_id'];
					}

					// Combine the paths with a new level
					$level = 0;

					foreach ($path as $path_id) {
						$this->db->query("REPLACE INTO `" . DB_PREFIX . "ms_category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

						$level++;
					}
				}
			} else {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_category_path` WHERE category_id = '" . (int)$category_id . "'");

				// Fix for records with no paths
				$this->_savePath($category_id, $data);
			}
		}
	}

	/**
	 * Deletes seller's category.
	 *
	 * @param	int		$category_id	Category id.
	 */
	public function deleteCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_description WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_filter WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_to_store WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query LIKE 'ms_category_id=" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_category_path WHERE category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "ms_product_to_category WHERE ms_category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "ms_coupon_ms_category` WHERE ms_category_id = '" . (int)$category_id . "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_category_path WHERE path_id = '" . (int)$category_id . "'");
		foreach ($query->rows as $result) {
			$this->deleteCategory($result['category_id']);
		}

		$this->cache->delete('multimerch_seo_url');
	}

	/**
	 * Gets full MsCategory hierarchy.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	string					Comma separated categories ids.
	 */
	public function getMsCategoryPath($category_id) {
		$sql = "SELECT GROUP_CONCAT(mscp.path_id ORDER BY `level` SEPARATOR ',') as category_path
			FROM `" . DB_PREFIX . "ms_category_path` mscp
			WHERE mscp.category_id = '" . (int)$category_id . "'
			GROUP BY mscp.category_id";

		$res = $this->db->query($sql);

		return $res->num_rows ? $res->row['category_path'] : '';
	}

	/**
	 * Gets Multimerch category name translated in selected language.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	int		$language_id	Language id.
	 * @return	string					Category name translated in selected language.
	 */
	public function getMsCategoryName($category_id, $language_id) {
		$result = $this->db->query("
			SELECT
				`name`
			FROM `" . DB_PREFIX . "ms_category_description`
			WHERE category_id = '" . (int)$category_id . "'
				AND language_id = '" . (int)$language_id . "'
		");

		return $result->row['name'] ?: '';
	}


	/* ============================================   HELPERS   ===================================================== */


	/**
	 * Checks the legitimacy of the category.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	bool					True if category is MsCategory, false if not.
	 */
	public function isMsCategory($category_id, $data = array()) {
		$sql = "SELECT 1 FROM " . DB_PREFIX. "ms_category
				WHERE category_id = " . (int)$category_id
			. (isset($data['seller_id']) ? " AND seller_id = " . (int)$data['seller_id'] : "");

		$res = $this->db->query($sql);

		return $res->num_rows ? true : false;
	}

	/**
	 * Changes category to seller relation.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	int		$seller_id		Seller id.
	 */
	public function changeSeller($category_id, $seller_id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_category`
			SET `seller_id` = '" . (int)$seller_id . "'
			WHERE `category_id` = '" . (int)$category_id . "'");
	}

	/**
	 * Changes category status.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	int		$status_id		Category status id.
	 */
	public function changeStatus($category_id, $status_id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "ms_category`
			SET `category_status` = '" . (int)$status_id . "'
			WHERE `category_id` = '" . (int)$category_id . "'");
	}

	/**
	 * Gets products related to passed category id.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					Product ids.
	 */
	public function getProductsByCategoryId($category_id) {
		$products_ids = array();

		$products = $this->db->query("
			SELECT
				GROUP_CONCAT(product_id SEPARATOR ',') as `products_ids`
			FROM `" . DB_PREFIX . "ms_product_to_category`
			WHERE ms_category_id = '" . (int)$category_id . "'
			GROUP BY ms_category_id
		");

		if($products->num_rows && $products->row['products_ids']) {
			$products_ids = explode(',', $products->row['products_ids']);
		}

		$child_categories = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "ms_category_path WHERE path_id = '" . (int)$category_id . "'");

		foreach ($child_categories->rows as $child_category) {
			$products_2 = $this->db->query("
				SELECT
					GROUP_CONCAT(product_id SEPARATOR ',') as `products_ids`
				FROM `" . DB_PREFIX . "ms_product_to_category`
				WHERE ms_category_id = '" . (int)$child_category['category_id'] . "'
				GROUP BY ms_category_id
			");

			if($products_2->num_rows && $products_2->row['products_ids']) {
				$products_ids_2 = explode(',', $products_2->row['products_ids']);
				$products_ids = array_merge($products_ids, $products_ids_2);
			}
		}

		return $products_ids;
	}

	/**
	 * Gets child categories ids for passed category id.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					Child categories ids.
	 */
	public function getChildCategoriesByCategoryId($category_id) {
		$child_categories = $this->db->query("
			SELECT
				GROUP_CONCAT(category_id SEPARATOR ',') as `categories_ids`
			FROM `" . DB_PREFIX . "ms_category_path`
			WHERE path_id = '" . (int)$category_id . "'
				AND category_id <> '" . (int)$category_id . "'
			GROUP BY path_id
		");

		$categories_ids = array();
		if($child_categories->num_rows && $child_categories->row['categories_ids']) {
			$categories_ids = explode(',', $child_categories->row['categories_ids']);
		}

		return $categories_ids;
	}

	/**
	 * Gets name, description, meta_title, meta_description and meta_keyword of MsCategory.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					MsCategory description data.
	 */
	private function _getDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => htmlentities($result['name']),
				'meta_title'       => htmlentities($result['meta_title']),
				'meta_description' => htmlentities($result['meta_description']),
				'meta_keyword'     => htmlentities($result['meta_keyword']),
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}

	/**
	 * Gets filters of MsCategory.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					MsCategory filters data.
	 */
	private function _getFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT
				mscf.oc_filter_id,
				fd.name,
				(SELECT `name` FROM `" . DB_PREFIX . "filter_group_description` fgd WHERE f.filter_group_id = fgd.filter_group_id AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS `group`
			FROM `" . DB_PREFIX . "ms_category_filter` mscf
			LEFT JOIN `" . DB_PREFIX . "filter` f
				ON (mscf.oc_filter_id = f.filter_id)
			LEFT JOIN `" . DB_PREFIX . "filter_description` fd
				ON (mscf.oc_filter_id = fd.filter_id)
			WHERE mscf.category_id = '" . (int)$category_id . "'
				AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = array(
				'filter_id' => $result['oc_filter_id'],
				'name' => strip_tags(html_entity_decode($result['group'] . ' &gt; ' . $result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}

		return $category_filter_data;
	}

	/**
	 * Gets MsCategory to stores relation.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					MsCategory to stores data.
	 */
	private function _getStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ms_category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}

	/**
	 * Saves category descriptions.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$descriptions	Category's name, description, meta_title, meta_description, meta_keyword.
	 */
	private function _saveDescriptions($category_id, $descriptions = array()) {
		foreach ($descriptions as $language_id => $value) {
			if (isset($value['meta_title']) AND !$value['meta_title']){
				$value['meta_title'] = $value['name'];
			}
			if (isset($value['meta_description']) AND !$value['meta_description']){
				$value['meta_description'] = $this->MsLoader->MsHelper->generateMetaDescription($value['meta_description']);
			}
			$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category_description`
				SET category_id = '" . (int)$category_id . "',
					language_id = '" . (int)$language_id . "',
					`name` = '" . $this->db->escape($value['name']) . "',
					`description` = '" . $this->db->escape($value['description']) . "'"
				. (isset($value['meta_title']) ? ", meta_title = '" . $this->db->escape($value['meta_title']) . "'" : "")
				. (isset($value['meta_description']) ? ", meta_description = '" . $this->db->escape($value['meta_description']) . "'" : "")
				. (isset($value['meta_keyword']) ? ", meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'" : ""));
		}
	}

	/**
	 * Saves category filters.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$filters		Category to filters.
	 */
	private function _saveFilters($category_id, $filters = array()) {
		foreach ($filters as $filter_id) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category_filter`
				SET category_id = '" . (int)$category_id . "',
					oc_filter_id = '" . (int)$filter_id . "'");
		}
	}

	/**
	 * Saves category to store relation.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$stores			Category to stores.
	 */
	private function _saveStores($category_id, $stores = array()) {
		foreach ($stores as $store_id) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category_to_store`
				SET category_id = '" . (int)$category_id . "',
					store_id = '" . (int)$store_id . "'");
		}
	}

	/**
	 * Saves seo url keyword for category
	 *
	 * @param	int		$category_id	Category id.
	 * @param	string	$keyword		Category's keyword.
	 */
	private function _saveKeyword($category_id, $keyword) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'ms_category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($this->MsLoader->MsHelper->handleSlugDuplicate($keyword)) . "'");

		$this->cache->delete('multimerch_seo_url');
	}

	/**
	 * Saves categories hierarchy. Uses MySQL Hierarchical Data Closure Table Pattern.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	array	$data			Conditions.
	 */
	private function _savePath($category_id, $data = array()) {
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "ms_category_path` WHERE `category_id` = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "ms_category_path`
			SET `category_id` = '" . (int)$category_id . "',
				`path_id` = '" . (int)$category_id . "',
				`level` = '" . (int)$level . "'
			ON DUPLICATE KEY UPDATE
				`level` = '" . (int)$level . "'");
	}


	/* ============================================   OC CATEGORIES   =============================================== */


	/**
	 * Gets Opencart categories with passed parent category id.
	 *
	 * @param	array	$data	Conditions.
	 * @param	array 	$sort	Data for sorting or filtering results.
	 * @param	array	$cols	Data for sorting or filtering results.
	 * @return	array			Opencart categories.
	 */
	public function getOcCategories($data = array(), $sort = array(), $cols = array()) {
		$hFilters = $wFilters = '';
		if(isset($sort['filters'])) {
			$cols = array_merge($cols, array("`c.name`" => 1, "`ms.date_created`" => 1));
			foreach($sort['filters'] as $k => $v) {
				if (!isset($cols[$k])) {
					$wFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				} else {
					$hFilters .= " AND {$k} LIKE '%" . $this->db->escape($v) . "%'";
				}
			}
		}

		$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				c.category_id,
				c.status,
				c.image,
				c.sort_order,
				c.parent_id,
				cd.name,"
				. (isset($data['category_id']) ?
					"(SELECT DISTINCT keyword
						FROM " . DB_PREFIX . "url_alias
						WHERE `query` = 'category_id=" . (int)$data['category_id'] . "'
					) AS keyword, "
				: "")

				. (isset($data['category_id']) ?
					"(SELECT commission_id
						FROM `" . DB_PREFIX . "ms_category_commission`
						WHERE `category_id` = " . (int)$data['category_id'] . "
					) AS commission_id, "
				: "")

				."
				@path := (SELECT GROUP_CONCAT(cd1.name ORDER BY `level` SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;')
					FROM `" . DB_PREFIX . "category_path` cp
					LEFT JOIN `" . DB_PREFIX . "category_description` cd1
						ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id)
					WHERE
						cp.category_id = c.category_id
						AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "'
					GROUP BY cp.category_id) AS path,
				IF(@path IS NULL, cd.name, CONCAT(@path, '&nbsp;&nbsp;&gt;&nbsp;&nbsp;', cd.name)) as `full_path`,
				(SELECT GROUP_CONCAT(cp.path_id ORDER BY `level` SEPARATOR '_')
					FROM `" . DB_PREFIX . "category_path` cp
					WHERE cp.category_id = c.category_id
					GROUP BY cp.category_id) AS `full_path_ids`
			FROM " . DB_PREFIX . "category c
			LEFT JOIN (SELECT category_id, name FROM " . DB_PREFIX . "category_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "') cd
				ON (c.category_id = cd.category_id)
			LEFT JOIN " . DB_PREFIX . "category_to_store c2s
				ON (c.category_id = c2s.category_id)
			WHERE c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'"
			. (isset($data['category_id']) ? " AND c.category_id = '" . (int)$data['category_id'] . "'" : "")
			. (isset($data['parent_id']) ? " AND c.parent_id = '" . (int)$data['parent_id'] . "'" : "")
			. (isset($data['category_status']) ? " AND c.status = '" . (int)$data['category_status'] . "'" : "")
			. (!empty($data['name']) ? " AND cd.`name` LIKE '" . $this->db->escape($data['name']) . "'" : '')

			. $wFilters

			. " GROUP BY c.category_id HAVING 1 = 1 "

			. $hFilters

			. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : "")
			. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : "")

		;
		
		$query = $this->db->query($sql);

		$total = $this->db->query("SELECT FOUND_ROWS() as total");
		if ($query->num_rows) {
			$query->rows[0]['total_rows'] = $total->row['total'];
		}

		foreach ($query->rows as &$row) {
			$row['name'] = str_replace('&amp;', '&', $row['name']);

			if ($this->config->get('msconf_msf_attribute_enabled')) {
				$row['msf_attributes'] = $this->MsLoader->MsfAttribute->getOcCategoryMsfAttributes($row['category_id']);
			}

			if ($this->config->get('msconf_msf_variation_enabled')) {
				$row['msf_variations'] = $this->MsLoader->MsfVariation->getOcCategoryMsfVariations($row['category_id']);
			}
		}

		return isset($data['single']) ? $query->rows[0] : $query->rows;
	}

	/**
	 * Gets full OcCategory hierarchy.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	string					Comma separated categories ids.
	 */
	public function getOcCategoryPath($category_id) {
		$sql = "SELECT GROUP_CONCAT(cp.path_id ORDER BY `level` SEPARATOR ',') as category_path
			FROM `" . DB_PREFIX . "category_path` cp
			WHERE cp.category_id = '" . (int)$category_id . "'
			GROUP BY cp.category_id";

		$res = $this->db->query($sql);

		return $res->num_rows ? $res->row['category_path'] : '';
	}

	/**
	 * Gets Opencart category name translated in selected language.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	int		$language_id	Language id.
	 * @return	string					Category name translated in selected language.
	 */
	public function getOcCategoryName($category_id, $language_id) {
		$result = $this->db->query("
			SELECT
				`name`
			FROM `" . DB_PREFIX . "category_description`
			WHERE category_id = '" . (int)$category_id . "'
				AND language_id = '" . (int)$language_id . "'
		");

		return !empty($result->row['name']) ? $result->row['name'] : '';
	}

	/**
	 * Saves commission settings for Opencart's category.
	 *
	 * @param	int		$category_id	Category id.
	 * @param	int		$commission_id	Commission id.
	 */
	public function saveCategoryCommission($category_id, $commission_id) {
		$sql = "INSERT INTO " . DB_PREFIX . "ms_category_commission
				SET category_id = " . (int)$category_id . ",
					commission_id = " . (is_null($commission_id) ? 'NULL' : (int)$commission_id) . "
				ON DUPLICATE KEY UPDATE
					commission_id = " . (is_null($commission_id) ? 'NULL' : (int)$commission_id);

		$this->db->query($sql);
	}

	/**
	 * Gets child categories ids for passed OpenCart category id.
	 *
	 * @param	int		$category_id	Category id.
	 * @return	array					Child categories ids.
	 */
	public function getChildCategoriesByOcCategoryId($category_id) {
		$child_categories = $this->db->query("
			SELECT
				GROUP_CONCAT(cp.category_id SEPARATOR ',') as `categories_ids`
			FROM `" . DB_PREFIX . "category_path` cp
			LEFT JOIN (SELECT category_id, status FROM `" . DB_PREFIX . "category`) c
				ON (c.category_id = cp.category_id)
			WHERE cp.path_id = '" . (int)$category_id . "'
				AND cp.category_id <> '" . (int)$category_id . "'
				AND c.status <> 0
			GROUP BY cp.path_id
		");

		$categories_ids = array();
		if($child_categories->num_rows && $child_categories->row['categories_ids']) {
			$categories_ids = explode(',', $child_categories->row['categories_ids']);
		}

		return $categories_ids;
	}

	public function getOcCategoryFeeRates($category_id, $retrieve_commissioned_category = false)
	{
		$sql = "
			SELECT
				c.`category_id`,
				c.`parent_id`,
				mcc.`commission_id`
			FROM `" . DB_PREFIX . "category` c
			LEFT JOIN `" . DB_PREFIX . "ms_category_commission` mcc
				ON (mcc.`category_id` = c.`category_id`)
			WHERE c.`category_id` = " . (int)$category_id
		;

		$oc_category = $this->db->query($sql)->row;

		if (!empty($oc_category['commission_id'])) {
			$rates = $this->MsLoader->MsCommission->getCommissionRates($oc_category['commission_id']);
			$commissioned_category_id = $category_id;
		} elseif (!empty($oc_category)) {
			$rates = $this->getOcCategoryFeeRates($oc_category['parent_id']);
			$commissioned_category_id = $oc_category['parent_id'];
		} else {
			foreach ([MsCommission::RATE_SALE, MsCommission::RATE_LISTING, MsCommission::RATE_SIGNUP] as $type) {
				$rates[$type] = [
					'rate_id' => null,
					'rate_type' => $type,
					'flat' => 0,
					'percent' => 0,
					'payment_method' => null
				];
			}

			$commissioned_category_id = 0;
		}

		return !$retrieve_commissioned_category ? $rates : [$rates, $commissioned_category_id];
	}

	public function getOcCategoryBiggestFeeRates($category_ids, $fee_type, $total, $retrieve_commissioned_category = false)
	{
		if (!in_array($fee_type, [MsCommission::RATE_LISTING, MsCommission::RATE_SALE])) {
			return [];
		}

		$rates = [];
		$rates_for_category = [];
		$commissioned_category_id = 0;

		foreach ((array)$category_ids as $category_id) {
			$r = $this->getOcCategoryFeeRates($category_id);

			$flat = !empty($r[$fee_type]['flat'])
				? (float)$r[$fee_type]['flat']
				: 0;

			$percent = !empty($r[$fee_type]['percent'])
				? ((float)$total * (float)$r[$fee_type]['percent'] / 100)
				: 0;

			// Get rates for each category
			$rates_for_category[$category_id] = [
				'category_id' => $category_id,
				'rates' => $r,
				'fee' => $flat + $percent
			];
		}

		if (!empty($rates_for_category)) {
			// Get the biggest fee
			uasort($rates_for_category, function ($rate_1, $rate_2) {
				return $rate_2['fee'] <=> $rate_1['fee'];
			});

			$biggest_rates = array_shift($rates_for_category);

			$rates = $biggest_rates['rates'];
			$commissioned_category_id = $biggest_rates['category_id'];
		}

		return !$retrieve_commissioned_category ? $rates : [$rates, $commissioned_category_id];
	}
}
