<?php
class MsPrinter extends Model {
	private $mpdf = false;

	public function __construct($registry) {
		parent::__construct($registry);
		require_once DIR_SYSTEM . 'vendor/mpdf/autoload.php';

		$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
		$fontData = $defaultFontConfig['fontdata'];

		$this->mpdf = new \Mpdf\Mpdf([
			'fontdata' => $fontData + [
			'opensans' => [
				'R' => 'OpenSans-Regular.ttf',
				'I' => 'OpenSans-Italic.ttf',
				'B' => 'OpenSans-Bold.ttf',
				'BI' => 'OpenSans-BoldItalic.ttf'
			]
			],
			'default_font' => 'opensans'
			//fonts: open sans, XB Riyaz (arabic)
		]);
		$this->mpdf->autoScriptToLang = true;
		$this->mpdf->autoLangToFont = true;
		//$this->mpdf->SetDirectionality('rtl');
		$this->mpdf->AddFont('opensans');
	}

	private function _modelExists($model) {
		$file = DIR_APPLICATION . 'model/' . $model . '.php';
		return file_exists($file);
	}

	public function printOrderPackingSlip($order_id, $suborder_id) {

		$order_info = $this->MsLoader->MsOrderData->ocGetOrder($order_id);

		$suborder = $this->MsLoader->MsSuborder->getSuborders(array(
			'suborder_id' => $suborder_id,
			'single' => true)
		);

		$seller_setting = $this->MsLoader->MsSetting->getSellerSettings(['seller_id' => $suborder['seller_id']]);

		if (!empty($seller_setting['slr_ms_address'])) {

			$seller_address = $this->MsLoader->MsSeller->getSellerMsAddress(array(
				'seller_id' => $suborder['seller_id'],
				'address_id' => $seller_setting['slr_ms_address'],
				'single' => true
			));

			$this->load->model('localisation/country');
			$seller_address['country_name'] = $this->model_localisation_country->getCountry($seller_address['country_id'])['name'];
		} else {
			$seller_address = false;
		}

		$products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);

		foreach ($products as $pid => &$product) {
			if ($product['seller_id'] != $suborder['seller_id']) {
				unset($products[$pid]);
				continue;
			}
			// Product MSF variation
			$msf_variation_data = [];

			$msf_variations = $this->MsLoader->MsOrderData->getOrderProductMsfVariations($order_id, $product['order_product_id']);
			foreach ($msf_variations as $msf_variation) {
				$msf_variation_data[] = [
					'name' => $msf_variation['name'],
					'value' => $msf_variation['value']
				];
			}

			// Product option
			$option_data = [];

			if ($this->_modelExists('account/order')) {
				$this->load->model('account/order');
				$options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);
			} else {
				// admin
				$this->load->model('sale/order');
				$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
			}

			foreach ($options as $option) {
				$o = [
					'name' => $option['name'],
					'value' => $option['value'],
					'type' => $option['type']
				];

				if ('file' === (string) $option['type'] && $upload_info = $this->model_tool_upload->getUploadByCode($option['value'])) {
					$o['value'] = $upload_info['name'];
					$o['href'] = $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true);
				}

				$option_data[] = $o;
			}

			$product['msf_variation'] = $msf_variation_data;
			$product['option'] = $option_data;
		}

		$order_data = array(
			'order_id' => $order_id,
			'order_info' => $order_info,
			'products' => $products,
			'seller_name' => $suborder['seller'],
			'seller_company' => !empty($seller_setting['slr_company']) ? $seller_setting['slr_company'] : '',
			'seller_phone' => !empty($seller_setting['slr_phone']) ? $seller_setting['slr_phone'] : '',
			'seller_address' => $seller_address
		);

		$html = $this->MsLoader->MsHelper->loadCommonView('print/seller-order-packing-slip', $order_data);

		$this->mpdf->SetTitle($order_data['seller_name'] . " - Order " . $order_data['order_id'] . ' - Packing slip');
		//$this->mpdf->SetAuthor("MultiMerch");
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output($order_data['seller_name'] . "_order-" . $order_data['order_id'] . '_packing-slip.pdf', 'I');
	}

	public function printOrderInvoice($order_id) {

		$order_info = $this->MsLoader->MsOrderData->ocGetOrder($order_id);

		$products = $this->MsLoader->MsOrderData->getOrderProductsNew(['order_id' => $order_id]);

		foreach ($products as $pid => &$product) {
			// Product MSF variation
			$msf_variation_data = [];

			$msf_variations = $this->MsLoader->MsOrderData->getOrderProductMsfVariations($order_id, $product['order_product_id']);
			foreach ($msf_variations as $msf_variation) {
				$msf_variation_data[] = [
					'name' => $msf_variation['name'],
					'value' => $msf_variation['value']
				];
			}

			// Product option
			$option_data = [];

			if ($this->_modelExists('account/order')) {
				$this->load->model('account/order');
				$options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);
				$totals = $this->model_account_order->getOrderTotals($order_id);
			} else {
				// admin
				$this->load->model('sale/order');
				$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
				$totals = $this->model_sale_order->getOrderTotals($order_id);
			}

			foreach ($options as $option) {
				$o = [
					'name' => $option['name'],
					'value' => $option['value'],
					'type' => $option['type']
				];

				if ('file' === (string) $option['type'] && $upload_info = $this->model_tool_upload->getUploadByCode($option['value'])) {
					$o['value'] = $upload_info['name'];
					$o['href'] = $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true);
				}

				$option_data[] = $o;
			}

			$product['price'] = $this->currency->format($product['price']);
			$product['total'] = $this->currency->format($product['total']);

			$product['msf_variation'] = $msf_variation_data;
			$product['option'] = $option_data;
		}

		foreach ($totals as $one) {
			$one['value'] = $this->currency->format($one['value']);
			$order_totals[$one['code']] = $one;
		}

		$order_data = array(
			'order_id' => $order_id,
			'order_info' => $order_info,
			'totals' => $order_totals,
			'products' => $products,
			'marketplace_name' => $this->config->get('config_name'),
			'marketplace_address' => $this->config->get('config_address'),
			'marketplace_phone' => $this->config->get('config_telephone'),
			'marketplace_email' => $this->config->get('config_email')
		);

		$html = $this->MsLoader->MsHelper->loadCommonView('print/seller-order-invoice', $order_data);

		$this->mpdf->SetTitle("Order " . $order_id . ' - Invoice');
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output("Order-" . $order_id . '_invoice.pdf', 'I');
	}

}