<?php namespace Tests\Repositories;

use App\Models\MsProduct;
use App\Repositories\MsProductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MsProductRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MsProductRepository
     */
    protected $msProductRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->msProductRepo = \App::make(MsProductRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ms_product()
    {
        $msProduct = MsProduct::factory()->make()->toArray();

        $createdMsProduct = $this->msProductRepo->create($msProduct);

        $createdMsProduct = $createdMsProduct->toArray();
        $this->assertArrayHasKey('id', $createdMsProduct);
        $this->assertNotNull($createdMsProduct['id'], 'Created MsProduct must have id specified');
        $this->assertNotNull(MsProduct::find($createdMsProduct['id']), 'MsProduct with given id must be in DB');
        $this->assertModelData($msProduct, $createdMsProduct);
    }

    /**
     * @test read
     */
    public function test_read_ms_product()
    {
        $msProduct = MsProduct::factory()->create();

        $dbMsProduct = $this->msProductRepo->find($msProduct->id);

        $dbMsProduct = $dbMsProduct->toArray();
        $this->assertModelData($msProduct->toArray(), $dbMsProduct);
    }

    /**
     * @test update
     */
    public function test_update_ms_product()
    {
        $msProduct = MsProduct::factory()->create();
        $fakeMsProduct = MsProduct::factory()->make()->toArray();

        $updatedMsProduct = $this->msProductRepo->update($fakeMsProduct, $msProduct->id);

        $this->assertModelData($fakeMsProduct, $updatedMsProduct->toArray());
        $dbMsProduct = $this->msProductRepo->find($msProduct->id);
        $this->assertModelData($fakeMsProduct, $dbMsProduct->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ms_product()
    {
        $msProduct = MsProduct::factory()->create();

        $resp = $this->msProductRepo->delete($msProduct->id);

        $this->assertTrue($resp);
        $this->assertNull(MsProduct::find($msProduct->id), 'MsProduct should not exist in DB');
    }
}
