<?php namespace Tests\Repositories;

use App\Models\SellerAddress;
use App\Repositories\SellerAddressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SellerAddressRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SellerAddressRepository
     */
    protected $sellerAddressRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sellerAddressRepo = \App::make(SellerAddressRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->make()->toArray();

        $createdSellerAddress = $this->sellerAddressRepo->create($sellerAddress);

        $createdSellerAddress = $createdSellerAddress->toArray();
        $this->assertArrayHasKey('id', $createdSellerAddress);
        $this->assertNotNull($createdSellerAddress['id'], 'Created SellerAddress must have id specified');
        $this->assertNotNull(SellerAddress::find($createdSellerAddress['id']), 'SellerAddress with given id must be in DB');
        $this->assertModelData($sellerAddress, $createdSellerAddress);
    }

    /**
     * @test read
     */
    public function test_read_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();

        $dbSellerAddress = $this->sellerAddressRepo->find($sellerAddress->id);

        $dbSellerAddress = $dbSellerAddress->toArray();
        $this->assertModelData($sellerAddress->toArray(), $dbSellerAddress);
    }

    /**
     * @test update
     */
    public function test_update_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();
        $fakeSellerAddress = SellerAddress::factory()->make()->toArray();

        $updatedSellerAddress = $this->sellerAddressRepo->update($fakeSellerAddress, $sellerAddress->id);

        $this->assertModelData($fakeSellerAddress, $updatedSellerAddress->toArray());
        $dbSellerAddress = $this->sellerAddressRepo->find($sellerAddress->id);
        $this->assertModelData($fakeSellerAddress, $dbSellerAddress->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();

        $resp = $this->sellerAddressRepo->delete($sellerAddress->id);

        $this->assertTrue($resp);
        $this->assertNull(SellerAddress::find($sellerAddress->id), 'SellerAddress should not exist in DB');
    }
}
