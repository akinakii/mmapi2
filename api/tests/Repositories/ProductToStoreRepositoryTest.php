<?php namespace Tests\Repositories;

use App\Models\ProductToStore;
use App\Repositories\ProductToStoreRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductToStoreRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductToStoreRepository
     */
    protected $productToStoreRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productToStoreRepo = \App::make(ProductToStoreRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_to_store()
    {
        $productToStore = ProductToStore::factory()->make()->toArray();

        $createdProductToStore = $this->productToStoreRepo->create($productToStore);

        $createdProductToStore = $createdProductToStore->toArray();
        $this->assertArrayHasKey('id', $createdProductToStore);
        $this->assertNotNull($createdProductToStore['id'], 'Created ProductToStore must have id specified');
        $this->assertNotNull(ProductToStore::find($createdProductToStore['id']), 'ProductToStore with given id must be in DB');
        $this->assertModelData($productToStore, $createdProductToStore);
    }

    /**
     * @test read
     */
    public function test_read_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();

        $dbProductToStore = $this->productToStoreRepo->find($productToStore->id);

        $dbProductToStore = $dbProductToStore->toArray();
        $this->assertModelData($productToStore->toArray(), $dbProductToStore);
    }

    /**
     * @test update
     */
    public function test_update_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();
        $fakeProductToStore = ProductToStore::factory()->make()->toArray();

        $updatedProductToStore = $this->productToStoreRepo->update($fakeProductToStore, $productToStore->id);

        $this->assertModelData($fakeProductToStore, $updatedProductToStore->toArray());
        $dbProductToStore = $this->productToStoreRepo->find($productToStore->id);
        $this->assertModelData($fakeProductToStore, $dbProductToStore->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();

        $resp = $this->productToStoreRepo->delete($productToStore->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductToStore::find($productToStore->id), 'ProductToStore should not exist in DB');
    }
}
