<?php namespace Tests\Repositories;

use App\Models\SellerDescription;
use App\Repositories\SellerDescriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SellerDescriptionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SellerDescriptionRepository
     */
    protected $sellerDescriptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sellerDescriptionRepo = \App::make(SellerDescriptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->make()->toArray();

        $createdSellerDescription = $this->sellerDescriptionRepo->create($sellerDescription);

        $createdSellerDescription = $createdSellerDescription->toArray();
        $this->assertArrayHasKey('id', $createdSellerDescription);
        $this->assertNotNull($createdSellerDescription['id'], 'Created SellerDescription must have id specified');
        $this->assertNotNull(SellerDescription::find($createdSellerDescription['id']), 'SellerDescription with given id must be in DB');
        $this->assertModelData($sellerDescription, $createdSellerDescription);
    }

    /**
     * @test read
     */
    public function test_read_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();

        $dbSellerDescription = $this->sellerDescriptionRepo->find($sellerDescription->id);

        $dbSellerDescription = $dbSellerDescription->toArray();
        $this->assertModelData($sellerDescription->toArray(), $dbSellerDescription);
    }

    /**
     * @test update
     */
    public function test_update_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();
        $fakeSellerDescription = SellerDescription::factory()->make()->toArray();

        $updatedSellerDescription = $this->sellerDescriptionRepo->update($fakeSellerDescription, $sellerDescription->id);

        $this->assertModelData($fakeSellerDescription, $updatedSellerDescription->toArray());
        $dbSellerDescription = $this->sellerDescriptionRepo->find($sellerDescription->id);
        $this->assertModelData($fakeSellerDescription, $dbSellerDescription->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();

        $resp = $this->sellerDescriptionRepo->delete($sellerDescription->id);

        $this->assertTrue($resp);
        $this->assertNull(SellerDescription::find($sellerDescription->id), 'SellerDescription should not exist in DB');
    }
}
