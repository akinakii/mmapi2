<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MsProduct;

class MsProductApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ms_product()
    {
        $msProduct = MsProduct::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ms_products', $msProduct
        );

        $this->assertApiResponse($msProduct);
    }

    /**
     * @test
     */
    public function test_read_ms_product()
    {
        $msProduct = MsProduct::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ms_products/'.$msProduct->id
        );

        $this->assertApiResponse($msProduct->toArray());
    }

    /**
     * @test
     */
    public function test_update_ms_product()
    {
        $msProduct = MsProduct::factory()->create();
        $editedMsProduct = MsProduct::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ms_products/'.$msProduct->id,
            $editedMsProduct
        );

        $this->assertApiResponse($editedMsProduct);
    }

    /**
     * @test
     */
    public function test_delete_ms_product()
    {
        $msProduct = MsProduct::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ms_products/'.$msProduct->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ms_products/'.$msProduct->id
        );

        $this->response->assertStatus(404);
    }
}
