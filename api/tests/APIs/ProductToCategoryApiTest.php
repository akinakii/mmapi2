<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductToCategory;

class ProductToCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_to_category()
    {
        $productToCategory = ProductToCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_to_categories', $productToCategory
        );

        $this->assertApiResponse($productToCategory);
    }

    /**
     * @test
     */
    public function test_read_product_to_category()
    {
        $productToCategory = ProductToCategory::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/product_to_categories/'.$productToCategory->id
        );

        $this->assertApiResponse($productToCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_to_category()
    {
        $productToCategory = ProductToCategory::factory()->create();
        $editedProductToCategory = ProductToCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_to_categories/'.$productToCategory->id,
            $editedProductToCategory
        );

        $this->assertApiResponse($editedProductToCategory);
    }

    /**
     * @test
     */
    public function test_delete_product_to_category()
    {
        $productToCategory = ProductToCategory::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_to_categories/'.$productToCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_to_categories/'.$productToCategory->id
        );

        $this->response->assertStatus(404);
    }
}
