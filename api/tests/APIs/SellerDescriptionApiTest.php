<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SellerDescription;

class SellerDescriptionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seller_descriptions', $sellerDescription
        );

        $this->assertApiResponse($sellerDescription);
    }

    /**
     * @test
     */
    public function test_read_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/seller_descriptions/'.$sellerDescription->id
        );

        $this->assertApiResponse($sellerDescription->toArray());
    }

    /**
     * @test
     */
    public function test_update_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();
        $editedSellerDescription = SellerDescription::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seller_descriptions/'.$sellerDescription->id,
            $editedSellerDescription
        );

        $this->assertApiResponse($editedSellerDescription);
    }

    /**
     * @test
     */
    public function test_delete_seller_description()
    {
        $sellerDescription = SellerDescription::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seller_descriptions/'.$sellerDescription->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seller_descriptions/'.$sellerDescription->id
        );

        $this->response->assertStatus(404);
    }
}
