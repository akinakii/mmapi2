<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SellerAddress;

class SellerAddressApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seller_addresses', $sellerAddress
        );

        $this->assertApiResponse($sellerAddress);
    }

    /**
     * @test
     */
    public function test_read_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/seller_addresses/'.$sellerAddress->id
        );

        $this->assertApiResponse($sellerAddress->toArray());
    }

    /**
     * @test
     */
    public function test_update_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();
        $editedSellerAddress = SellerAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seller_addresses/'.$sellerAddress->id,
            $editedSellerAddress
        );

        $this->assertApiResponse($editedSellerAddress);
    }

    /**
     * @test
     */
    public function test_delete_seller_address()
    {
        $sellerAddress = SellerAddress::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seller_addresses/'.$sellerAddress->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seller_addresses/'.$sellerAddress->id
        );

        $this->response->assertStatus(404);
    }
}
