<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductToStore;

class ProductToStoreApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_to_store()
    {
        $productToStore = ProductToStore::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_to_stores', $productToStore
        );

        $this->assertApiResponse($productToStore);
    }

    /**
     * @test
     */
    public function test_read_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/product_to_stores/'.$productToStore->id
        );

        $this->assertApiResponse($productToStore->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();
        $editedProductToStore = ProductToStore::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_to_stores/'.$productToStore->id,
            $editedProductToStore
        );

        $this->assertApiResponse($editedProductToStore);
    }

    /**
     * @test
     */
    public function test_delete_product_to_store()
    {
        $productToStore = ProductToStore::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_to_stores/'.$productToStore->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_to_stores/'.$productToStore->id
        );

        $this->response->assertStatus(404);
    }
}
