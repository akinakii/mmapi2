<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$nameSpace = 'App\Http\Controllers\API\\';

Route::group(['namespace' => $nameSpace], function () {
    Route::match(['get', 'post'], 'auth/reset-code-by-email', 'Auth\AuthApiController@getCodeByEmail');
    Route::match(['get', 'post'], 'auth/login', 'Auth\AuthApiController@login');
    Route::match(['get', 'post'], 'auth/reset-password', 'Auth\AuthApiController@resetPassword');
    Route::match(['get', 'post'], 'auth/change-password', 'Auth\AuthApiController@changePassword');

    Route::match(['get', 'post'], 'seller/register', 'SellerAPIController@register');
    Route::match(['get', 'post'], 'customer/register', 'SellerAPIController@registerCustomer');

    Route::match(['get', 'post'], 'country/list', 'CountryAPIController@index');
    Route::match(['get', 'post'], 'zone/list-by-country/{countryId}', 'ZoneAPIController@index');

    Route::match(['get', 'post'], 'shipping/method/list', 'ShippingMethodDescriptionAPIController@index');

    //product
    Route::group(['middleware' => 'auth:api'], function () {
        Route::match(['get', 'post'], 'product/upload-image', 'ProductAPIController@uploadImage');
        Route::match(['get', 'post'], 'product/create', 'ProductAPIController@store');
        Route::match(['get', 'post'], 'product/update/{productId}', 'ProductAPIController@update');
        Route::match(['get', 'post'], 'product/show/{productId}', 'ProductAPIController@show');
        Route::match(['get', 'post'], 'product/list', 'ProductAPIController@index');
    });

    //seller
    Route::group(['middleware' => 'auth:api'], function () {
        Route::match(['get', 'post'], 'seller/profile', 'SellerAPIController@profile');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('auth/logout', 'Auth\AuthApiController@logout');
    });

});
