<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductToCategory
 * @package App\Models
 * @version April 1, 2021, 11:11 am UTC
 *
 * @property integer $category_id
 */
class ProductToCategory extends Model
{

    use HasFactory;

    public $table = 'mms_product_to_category';

    public $timestamps = false;

    protected $primaryKey = 'product_id';



    public $fillable = [
        'category_id',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_id' => 'required|integer'
    ];


    public function category()
    {
        return $this->hasMany(CategoryDescription::class, 'category_id', 'category_id');
    }

}
