<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SellerDescription
 * @package App\Models
 * @version March 19, 2021, 10:27 am UTC
 *
 * @property integer $seller_id
 * @property integer $language_id
 * @property string $slogan
 * @property string $description
 */
class SellerDescription extends Model
{

    use HasFactory;

    public $table = 'mms_ms_seller_description';

    public $timestamps = false;

    protected $primaryKey = 'seller_description_id';



    public $fillable = [
        'seller_id',
        'language_id',
        'slogan',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'seller_description_id' => 'integer',
        'seller_id' => 'integer',
        'language_id' => 'integer',
        'slogan' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'seller_id' => 'required|integer',
        'language_id' => 'required|integer',
        'slogan' => 'required|string',
        'description' => 'nullable|string'
    ];

    
}
