<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Address
 * @package App\Models
 * @version March 19, 2021, 10:06 am UTC
 *
 * @property integer $customer_id
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $postcode
 * @property integer $country_id
 * @property integer $zone_id
 * @property string $custom_field
 */
class Address extends Model
{

    use HasFactory;

    public $table = 'mms_address';

    public $timestamps = false;

    protected $primaryKey = 'address_id';



    public $fillable = [
        'customer_id',
        'firstname',
        'lastname',
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'custom_field'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'address_id' => 'integer',
        'customer_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'company' => 'string',
        'address_1' => 'string',
        'address_2' => 'string',
        'city' => 'string',
        'postcode' => 'string',
        'country_id' => 'integer',
        'zone_id' => 'integer',
        'custom_field' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required|integer',
        'firstname' => 'required|string|max:32',
        'lastname' => 'required|string|max:32',
        'company' => 'required|string|max:40',
        'address_1' => 'required|string|max:128',
        'address_2' => 'required|string|max:128',
        'city' => 'required|string|max:128',
        'postcode' => 'required|string|max:10',
        'country_id' => 'required|integer',
        'zone_id' => 'required|integer',
        'custom_field' => 'required|string'
    ];

    
}
