<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Setting
 * @package App\Models
 * @version March 26, 2021, 11:59 am UTC
 *
 * @property integer $store_id
 * @property string $code
 * @property string $key
 * @property string $value
 * @property integer $serialized
 */
class Setting extends Model
{

    use HasFactory;

    public $table = 'mms_setting';

    public $timestamps = false;

    protected $primaryKey = 'setting_id';



    public $fillable = [
        'store_id',
        'code',
        'key',
        'value',
        'serialized'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'setting_id' => 'integer',
        'store_id' => 'integer',
        'code' => 'string',
        'key' => 'string',
        'value' => 'string',
        'serialized' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required|integer',
        'code' => 'required|string|max:32',
        'key' => 'required|string|max:64',
        'value' => 'required|string',
        'serialized' => 'required|boolean'
    ];


}
