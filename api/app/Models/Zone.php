<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Zone
 * @package App\Models
 * @version April 8, 2021, 7:20 am UTC
 *
 * @property integer $country_id
 * @property string $name
 * @property string $code
 * @property boolean $status
 */
class Zone extends Model
{

    use HasFactory;

    public $table = 'mms_zone';

    public $timestamps = false;

    protected $primaryKey = 'zone_id';


    public $fillable = [
        'country_id',
        'name',
        'code',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'zone_id' => 'integer',
        'country_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'country_id' => 'required|integer',
        'name' => 'required|string|max:128',
        'code' => 'required|string|max:32',
        'status' => 'required|boolean'
    ];


}
