<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Seller
 * @package App\Models
 * @version March 19, 2021, 10:23 am UTC
 *
 * @property string $nickname
 * @property string $company
 * @property string $website
 * @property string $description
 * @property integer $country_id
 * @property integer $zone_id
 * @property string $avatar
 * @property string $banner
 * @property string|\Carbon\Carbon $date_created
 * @property integer $seller_status
 * @property integer $seller_approved
 * @property integer $seller_group
 * @property integer $commission_id
 */
class Seller extends Model
{

    use HasFactory;

    public $table = 'mms_ms_seller';

    public $timestamps = false;

    protected $primaryKey = 'seller_id';



    public $fillable = [
        'nickname',
        'seller_id',
        'company',
        'website',
        'description',
        'country_id',
        'zone_id',
        'avatar',
        'banner',
        'date_created',
        'seller_status',
        'seller_approved',
        'seller_group',
        'commission_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'seller_id' => 'integer',
        'nickname' => 'string',
        'company' => 'string',
        'website' => 'string',
        'description' => 'string',
        'country_id' => 'integer',
        'zone_id' => 'integer',
        'avatar' => 'string',
        'banner' => 'string',
        'date_created' => 'datetime',
        'seller_status' => 'integer',
        'seller_approved' => 'integer',
        'seller_group' => 'integer',
        'commission_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nickname' => 'required|string|max:32',
        'company' => 'required|string|max:32',
        'website' => 'required|string|max:2083',
        'description' => 'required|string',
        'country_id' => 'required|integer',
        'zone_id' => 'required|integer',
        'avatar' => 'nullable|string|max:255',
        'banner' => 'nullable|string|max:255',
    ];

    const STATUS_ACTIVE = 1; // fully functional
    const STATUS_INACTIVE = 2; // awaiting approvement
    const STATUS_DISABLED = 3; // disabled by admin
    const STATUS_DELETED = 4; // deleted by admin
    const STATUS_UNPAID = 5; // @todo 9.0: remove
    const STATUS_INCOMPLETE = 6; // @todo 9.0: remove
}
