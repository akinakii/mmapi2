<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Country
 * @package App\Models
 * @version April 8, 2021, 7:12 am UTC
 *
 * @property string $name
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property string $address_format
 * @property boolean $postcode_required
 * @property boolean $status
 */
class Country extends Model
{

    use HasFactory;

    public $table = 'mms_country';

    public $timestamps = false;

    protected $primaryKey = 'country_id';


    public $fillable = [
        'name',
        'iso_code_2',
        'iso_code_3',
        'address_format',
        'postcode_required',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'country_id' => 'integer',
        'name' => 'string',
        'iso_code_2' => 'string',
        'iso_code_3' => 'string',
        'address_format' => 'string',
        'postcode_required' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:128',
        'iso_code_2' => 'required|string|max:2',
        'iso_code_3' => 'required|string|max:3',
        'address_format' => 'required|string',
        'postcode_required' => 'required|boolean',
        'status' => 'required|boolean'
    ];


}
