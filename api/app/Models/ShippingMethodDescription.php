<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ShippingMethodDescription
 * @package App\Models
 * @version April 8, 2021, 9:46 am UTC
 *
 * @property integer $shipping_method_id
 * @property string $name
 * @property string $description
 * @property integer $language_id
 */
class ShippingMethodDescription extends Model
{

    use HasFactory;

    public $table = 'mms_ms_shipping_method_description';

    public $timestamps = false;

    protected $primaryKey = 'shipping_method_description_id';


    public $fillable = [
        'shipping_method_id',
        'name',
        'description',
        'language_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'shipping_method_description_id' => 'integer',
        'shipping_method_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'language_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shipping_method_id' => 'required|integer',
        'name' => 'required|string|max:32',
        'description' => 'nullable|string',
        'language_id' => 'nullable|integer'
    ];


}
