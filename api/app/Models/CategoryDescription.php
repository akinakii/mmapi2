<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CategoryDescription
 * @package App\Models
 * @version April 7, 2021, 1:28 pm UTC
 *
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class CategoryDescription extends Model
{

    use HasFactory;

    public $table = 'mms_category_description';

    public $timestamps = false;

    protected $primaryKey = 'category_id';


    public $fillable = [
        'language_id',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'meta_keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'language_id' => 'required|integer',
        'name' => 'required|string|max:255',
        'description' => 'required|string',
        'meta_title' => 'required|string|max:255',
        'meta_description' => 'required|string|max:255',
        'meta_keyword' => 'required|string|max:255'
    ];


}
