<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SellerSetting
 * @package App\Models
 * @version March 24, 2021, 8:49 am UTC
 *
 * @property integer $seller_id
 * @property string $name
 * @property string $value
 * @property integer $is_encoded
 */
class SellerSetting extends Model
{

    use HasFactory;

    public $table = 'mms_ms_seller_setting';

    public $timestamps = false;

    protected $primaryKey = 'id';


    public $fillable = [
        'seller_id',
        'name',
        'value',
        'is_encoded'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'seller_id' => 'integer',
        'name' => 'string',
        'value' => 'string',
        'is_encoded' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'seller_id' => 'nullable|integer',
        'name' => 'nullable|string|max:50',
        'value' => 'nullable|string|max:250',
        'is_encoded' => 'nullable'
    ];

    
}
