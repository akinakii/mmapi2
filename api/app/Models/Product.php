<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Product
 * @package App\Models
 * @version March 30, 2021, 7:30 am UTC
 *
 * @property string $model
 * @property string $sku
 * @property string $upc
 * @property string $ean
 * @property string $jan
 * @property string $isbn
 * @property string $mpn
 * @property string $location
 * @property integer $quantity
 * @property integer $stock_status_id
 * @property string $image
 * @property integer $manufacturer_id
 * @property integer $shipping
 * @property number $price
 * @property integer $points
 * @property integer $tax_class_id
 * @property string $date_available
 * @property number $weight
 * @property integer $weight_class_id
 * @property number $length
 * @property number $width
 * @property number $height
 * @property integer $length_class_id
 * @property integer $subtract
 * @property integer $minimum
 * @property integer $sort_order
 * @property integer $status
 * @property integer $viewed
 * @property string|\Carbon\Carbon $date_added
 * @property string|\Carbon\Carbon $date_modified
 */
class Product extends Model
{

    use HasFactory;

    public $table = 'mms_product';

    public $timestamps = false;

    protected $primaryKey = 'product_id';



    public $fillable = [
        'model',
        'sku',
        'upc',
        'ean',
        'jan',
        'isbn',
        'mpn',
        'location',
        'quantity',
        'stock_status_id',
        'image',
        'manufacturer_id',
        'shipping',
        'price',
        'points',
        'tax_class_id',
        'date_available',
        'weight',
        'weight_class_id',
        'length',
        'width',
        'height',
        'length_class_id',
        'subtract',
        'minimum',
        'sort_order',
        'status',
        'viewed',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'model' => 'string',
        'sku' => 'string',
        'upc' => 'string',
        'ean' => 'string',
        'jan' => 'string',
        'isbn' => 'string',
        'mpn' => 'string',
        'location' => 'string',
        'quantity' => 'integer',
        'stock_status_id' => 'integer',
        'image' => 'string',
        'manufacturer_id' => 'integer',
        'shipping' => 'integer',
        'price' => 'decimal:4',
        'points' => 'integer',
        'tax_class_id' => 'integer',
        'date_available' => 'date',
        'weight' => 'decimal:8',
        'weight_class_id' => 'integer',
        'length' => 'decimal:8',
        'width' => 'decimal:8',
        'height' => 'decimal:8',
        'length_class_id' => 'integer',
        'subtract' => 'integer',
        'minimum' => 'integer',
        'sort_order' => 'integer',
        'status' => 'integer',
        'viewed' => 'integer',
        'date_added' => 'datetime',
        'date_modified' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'model' => 'required|string|max:64',
        'sku' => 'required|string|max:64',
        'upc' => 'required|string|max:12',
        'ean' => 'required|string|max:14',
        'jan' => 'required|string|max:13',
        'isbn' => 'required|string|max:17',
        'mpn' => 'required|string|max:64',
        'location' => 'required|string|max:128',
        'quantity' => 'required|integer',
        'stock_status_id' => 'required|integer',
        'image' => 'nullable|string|max:255',
        'manufacturer_id' => 'required|integer',
        'shipping' => 'required|boolean',
        'price' => 'required|numeric',
        'points' => 'required|integer',
        'tax_class_id' => 'required|integer',
        'date_available' => 'required',
        'weight' => 'required|numeric',
        'weight_class_id' => 'required|integer',
        'length' => 'required|numeric',
        'width' => 'required|numeric',
        'height' => 'required|numeric',
        'length_class_id' => 'required|integer',
        'subtract' => 'required|boolean',
        'minimum' => 'required|integer',
        'sort_order' => 'required|integer',
        'status' => 'required|boolean',
        'viewed' => 'required|integer',
        'date_added' => 'required',
        'date_modified' => 'required'
    ];

    const PRODUCT_QUANTITY = 999;
    const STOCK_STATUS_ID = 5;
    const DEFAULT_LANGUAGE = 1;

    public function descriptions()
    {
        return $this->hasMany(ProductDescription::class, 'product_id', 'product_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'product_id');
    }

    public function productToCategory()
    {
        return $this->hasMany(ProductToCategory::class, 'product_id', 'product_id');
    }
}
