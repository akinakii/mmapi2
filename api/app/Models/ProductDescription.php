<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductDescription
 * @package App\Models
 * @version March 31, 2021, 9:10 am UTC
 *
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $tag
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class ProductDescription extends Model
{

    use HasFactory;

    public $table = 'mms_product_description';

    public $timestamps = false;

    protected $primaryKey = 'product_id';


    public $fillable = [
        'language_id',
        'name',
        'description',
        'tag',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'tag' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'meta_keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'language_id' => 'required|integer',
        'name' => 'required|string|max:255',
        'description' => 'required|string',
        'tag' => 'required|string',
        'meta_title' => 'required|string|max:255',
        'meta_description' => 'required|string|max:255',
        'meta_keyword' => 'required|string|max:255'
    ];


}
