<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $table = 'mms_customer';

    protected $primaryKey = 'customer_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'customer_group_id',
        'store_id',
        'language_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'password',
        'salt',
        'cart',
        'wishlist',
        'newsletter',
        'address_id',
        'custom_field',
        'ip',
        'status',
        'approved',
        'safe',
        'token',
        'code',
        'date_added'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'salt',
        'code',
        'ip',
        'token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_group_id' => 'required|integer',
        'store_id' => 'required|integer',
        'language_id' => 'required|integer',
        'firstname' => 'required|string|max:32',
        'lastname' => 'required|string|max:32',
        'email' => 'required|string|max:96',
        'telephone' => 'required|string|max:32',
        'fax' => 'required|string|max:32',
        'password' => 'required|string|max:40',
        'salt' => 'required|string|max:9',
        'cart' => 'nullable|string',
        'wishlist' => 'nullable|string',
        'newsletter' => 'required|boolean',
        'address_id' => 'required|integer',
        'custom_field' => 'required|string',
        'ip' => 'required|string|max:40',
        'status' => 'required|boolean',
        'approved' => 'required|boolean',
        'safe' => 'required|boolean',
        'token' => 'required|string',
        'code' => 'required|string|max:40',
        'date_added' => 'required'
    ];

    /**
     * @return mixed
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

    public function seller()
    {
        return $this->hasOne(Seller::class, 'seller_id', 'customer_id');
    }

    public function isSeller()
    {
        $return = false;
        if (isset($this->seller->seller_id)) {
            $return = true;
        }
        return $return;
    }
}
