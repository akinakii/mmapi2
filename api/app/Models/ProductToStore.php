<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductToStore
 * @package App\Models
 * @version March 31, 2021, 9:07 am UTC
 *
 * @property integer $store_id
 */
class ProductToStore extends Model
{

    use HasFactory;

    public $table = 'mms_product_to_store';

    public $timestamps = false;

    protected $primaryKey = 'product_id';


    public $fillable = [
        'store_id',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'store_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'store_id' => 'required|integer'
    ];


}
