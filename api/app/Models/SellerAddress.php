<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SellerAddress
 * @package App\Models
 * @version March 19, 2021, 10:26 am UTC
 *
 * @property integer $seller_id
 * @property string $fullname
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property integer $country_id
 */
class SellerAddress extends Model
{

    use HasFactory;

    public $table = 'mms_ms_seller_address';

    public $timestamps = false;

    protected $primaryKey = 'address_id';



    public $fillable = [
        'seller_id',
        'fullname',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'country_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'address_id' => 'integer',
        'seller_id' => 'integer',
        'fullname' => 'string',
        'address_1' => 'string',
        'address_2' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'country_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'seller_id' => 'required|integer',
        'fullname' => 'required|string|max:255',
        'address_1' => 'required|string',
        'address_2' => 'required|string',
        'city' => 'required|string|max:255',
        'state' => 'required|string|max:255',
        'zip' => 'required|string|max:16',
        'country_id' => 'required|integer'
    ];

    
}
