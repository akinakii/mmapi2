<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Language
 * @package App\Models
 * @version March 25, 2021, 2:53 pm UTC
 *
 * @property string $name
 * @property string $code
 * @property string $locale
 * @property string $image
 * @property string $directory
 * @property integer $sort_order
 * @property integer $status
 */
class Language extends Model
{

    use HasFactory;

    public $table = 'mms_language';

    public $timestamps = false;

    protected $primaryKey = 'language_id';


    public $fillable = [
        'name',
        'code',
        'locale',
        'image',
        'directory',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'language_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'locale' => 'string',
        'image' => 'string',
        'directory' => 'string',
        'sort_order' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:32',
        'code' => 'required|string|max:5',
        'locale' => 'required|string|max:255',
        'image' => 'required|string|max:64',
        'directory' => 'required|string|max:32',
        'sort_order' => 'required|integer',
        'status' => 'required|boolean'
    ];


}
