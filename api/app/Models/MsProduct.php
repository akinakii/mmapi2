<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MsProduct
 * @package App\Models
 * @version March 31, 2021, 8:56 am UTC
 *
 * @property integer $seller_id
 * @property integer $product_status
 * @property integer $product_approved
 * @property string $list_until
 * @property integer $commission_id
 * @property integer $primary_oc_category
 * @property integer $primary_ms_category
 */
class MsProduct extends Model
{

    use HasFactory;

    public $table = 'mms_ms_product';

    public $timestamps = false;

    protected $primaryKey = 'product_id';



    public $fillable = [
        'product_id',
        'seller_id',
        'product_status',
        'product_approved',
        'list_until',
        'commission_id',
        'primary_oc_category',
        'primary_ms_category'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'seller_id' => 'integer',
        'product_status' => 'integer',
        'product_approved' => 'integer',
        'list_until' => 'date',
        'commission_id' => 'integer',
        'primary_oc_category' => 'integer',
        'primary_ms_category' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'seller_id' => 'nullable|integer',
        'product_status' => 'required|boolean',
        'product_approved' => 'required|boolean',
        'list_until' => 'nullable',
        'commission_id' => 'nullable|integer',
        'primary_oc_category' => 'nullable|integer',
        'primary_ms_category' => 'nullable|integer'
    ];


}
