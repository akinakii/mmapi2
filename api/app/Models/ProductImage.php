<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductImage
 * @package App\Models
 * @version March 31, 2021, 10:33 am UTC
 *
 * @property integer $product_id
 * @property string $image
 * @property integer $sort_order
 */
class ProductImage extends Model
{

    use HasFactory;

    public $table = 'mms_product_image';

    public $timestamps = false;

    protected $primaryKey = 'product_image_id';


    public $fillable = [
        'product_id',
        'image',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_image_id' => 'integer',
        'product_id' => 'integer',
        'image' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_id' => 'required|integer',
        'image' => 'nullable|string|max:255',
        'sort_order' => 'required|integer'
    ];


}
