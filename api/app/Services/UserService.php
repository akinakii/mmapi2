<?php

namespace App\Services;

use App\Models\Seller;
use App\Repositories\AddressRepository;
use App\Repositories\SellerAddressRepository;
use App\Repositories\SellerDescriptionRepository;
use App\Repositories\SellerRepository;
use App\Repositories\SellerSettingRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;

class UserService
{
    private $userRepository;
    private $addressRepository;
    private $sellerAddressRepository;
    private $sellerRepository;
    private $sellerSettingRepository;
    private $sellerDescriptionRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param AddressRepository $addressRepository
     * @param SellerAddressRepository $sellerAddressRepository
     * @param SellerRepository $sellerRepository
     * @param SellerSettingRepository $sellerSettingRepository
     * @param SellerDescriptionRepository $sellerDescriptionRepository
     */
    public function __construct(
        UserRepository $userRepository,
        AddressRepository $addressRepository,
        SellerAddressRepository $sellerAddressRepository,
        SellerRepository $sellerRepository,
        SellerSettingRepository $sellerSettingRepository,
        SellerDescriptionRepository $sellerDescriptionRepository
    ) {
        $this->userRepository = $userRepository;
        $this->addressRepository = $addressRepository;
        $this->sellerAddressRepository = $sellerAddressRepository;
        $this->sellerRepository = $sellerRepository;
        $this->sellerSettingRepository = $sellerSettingRepository;
        $this->sellerDescriptionRepository = $sellerDescriptionRepository;
    }

    public function registerCustomer($request)
    {
        $customerData = $this->prepareCustomerData($request);
        $customer = $this->userRepository->create($customerData);

        $customerId = $customer->customer_id;
        $customerData['customer_id'] = $customerId;

        $this->addressRepository->create($customerData);

        return $customer;
    }

    public function registerSeller($request)
    {
        $data = $request->all();
        $customer = $this->registerCustomer($request);
        $customerId = $customer->customer_id;

        $customerData = $this->prepareCustomerData($request);

        $sellerData = [
            'avatar_name' => isset($data['avatar']) ? $data['avatar'] : '',
            'banner_name' => isset($data['banner']) ? $data['banner'] : '',
            'avatar' => isset($data['avatar']) && $data['avatar'] ? $data['avatar'] : '',
            'banner' => isset($data['banner']) && $data['banner'] ? $data['banner'] : '',
            'fullname' => isset($data['fullname']) ? $data['fullname'] : '',
            'seller_id' => $customerId,
            'seller_status' => Seller::STATUS_ACTIVE,
            'seller_approved' => 1,
            'seller_group' => $data['seller_group_id'],
            'nickname' => $data['nickname'],
            'date_created' => Carbon::now(),
            'zip' => isset($data['zip']) ? $data['zip'] : '',
        ];

        $seller = $this->sellerRepository->create($sellerData);
        $sellerAddress = $this->sellerAddressRepository->create(array_merge($sellerData, $customerData));

        $settingsData = [
            'slr_website' => isset($data['website']) ? $data['website'] : '',
            'slr_company' => isset($data['company']) ? $data['company'] : '',
            'slr_phone' => isset($data['phone']) ? $data['phone'] : '',
            'slr_ms_address' => $sellerAddress->address_id
        ];

        foreach ($settingsData as $key => $value) {
            if (!empty($value)) {
                $this->sellerSettingRepository->create([
                    'seller_id' => $customerId,
                    'name' => $key,
                    'value' => $value,
                ]);
            }
        }


        $descriptiondata = [
            'slogan' => isset($data['slogan']) ? $data['slogan'] : '',
            'description' => isset($data['description']) ? $data['description'] : '',
            'language_id' => 1,
            'seller_id' => $customerId,
        ];

        $this->sellerDescriptionRepository->create($descriptiondata);

        return $seller;
    }

    private function prepareCustomerData($request)
    {
        $ip = $request->ip();
        $data = $request->all();

        return $customerData = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => isset($data['telephone']) ? $data['telephone'] : '',
            'phone' => isset($data['telephone']) ? $data['telephone'] : '',
            'customer_group_id' => isset($data['customer_group_id']) && !empty($data['customer_group_id']) ? $data['customer_group_id'] : 1,
            'password' => $data['password'],
            'fax' => '',
            'newsletter' => 0,
            'language_id' => 1,
            'custom_field' => isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '',
            'ip' => $ip,
            'status' => 1,
            'approved' => 1,
            'safe' => 0,
            'token' => '',
            'code' => '',

            'company' => isset($data['company']) ? $data['company'] : '',
            'address_1' => isset($data['address_1']) ? $data['address_1'] : '',
            'address_2' => isset($data['address_2']) ? $data['address_2'] : '',
            'city' => isset($data['city']) ? $data['city'] : '',
            'postcode' => isset($data['zip']) ? $data['zip'] : '',
            'country_id' => isset($data['country_id']) ? $data['country_id'] : '',
            'zone_id' => isset($data['zone_id']) ? $data['zone_id'] : '',
        ];
    }
}
