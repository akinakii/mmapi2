<?php


namespace App\Services;


use App\Exceptions\ApiException;
use App\Helpers\UploadHelper;
use App\Models\Product;
use App\Repositories\MsProductRepository;
use App\Repositories\ProductDescriptionRepository;
use App\Repositories\ProductImageRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductToCategoryRepository;
use App\Repositories\ProductToStoreRepository;
use Carbon\Carbon;

class ProductService
{
    /** @var MsProductRepository */
    private $msProductRepository;
    /** @var ProductDescriptionRepository*/
    private $productDescriptionRepository;
    /** @var ProductImageRepository */
    private $productImageRepository;
    /** @var ProductRepository */
    private $productRepository;
    /** @var ProductToCategoryRepository */
    private $productToCategoryRepository;
    /** @var ProductToStoreRepository */
    private $productToStoreRepository;

    /**
     * ProductService constructor.
     * @param MsProductRepository $msProductRepository
     * @param ProductDescriptionRepository $productDescriptionRepository
     * @param ProductImageRepository $productImageRepository
     * @param ProductRepository $productRepository
     * @param ProductToCategoryRepository $productToCategoryRepository
     * @param ProductToStoreRepository $productToStoreRepository
     */
    public function __construct(
        MsProductRepository $msProductRepository,
        ProductDescriptionRepository $productDescriptionRepository,
        ProductImageRepository $productImageRepository,
        ProductRepository $productRepository,
        ProductToCategoryRepository $productToCategoryRepository,
        ProductToStoreRepository $productToStoreRepository
    ) {
        $this->msProductRepository = $msProductRepository;
        $this->productDescriptionRepository = $productDescriptionRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productRepository = $productRepository;
        $this->productToCategoryRepository = $productToCategoryRepository;
        $this->productToStoreRepository = $productToStoreRepository;
    }

    /**
     * @param $request
     * @return array
     */
    public function uploadProductImage($request) {

        return Uploadhelper::uploadImage($request);

    }

    public function create($request)
    {
        $user = $request->user();

        if (!$user->isSeller()) {
            ApiException::throw(ApiException::USER_NOT_SELLER);
        }

        $data = $request->all();

        $sellerId = $user->customer_id;

        $productData = [
            'model' => $data['name'][Product::DEFAULT_LANGUAGE],
            'sku' => '',
            'upc' => '',
            'ean' => '',
            'jan' => '',
            'isbn' => '',
            'mpn' => '',
            'location' => '',
            'quantity' => Product::PRODUCT_QUANTITY,
            'stock_status_id' => Product::STOCK_STATUS_ID,
            'image' => isset($data['images'][0]) ? UploadHelper::sellersImagePath($data['images'][0], $sellerId, false) : '',
            'manufacturer_id' => 0,
            'shipping' => 1,
            'price' => $data['price'],
            'points' => 0,
            'tax_class_id' => 0,
            'date_available' => Carbon::now()->toDateString(),
            'weight' => 0.00000000,
            'weight_class_id' => 0,
            'length' => 0.00000000,
            'width' => 0.00000000,
            'height' => 0.00000000,
            'length_class_id' => 0,
            'subtract' => 0,
            'minimum' => 0,
            'sort_order' => 0,
            'status' => 1,
            'viewed' => 0,
            'date_added' => Carbon::now(),
            'date_modified' => Carbon::now(),
        ];

        $product = $this->productRepository->create($this->checkData($productData, $data));

        $msProductData = [
            'product_id' => $product->product_id,
            'seller_id' => $sellerId,
            'product_status' => 1,
            'product_approved' => 1,
            'list_until' => null,
            'commission_id' => null,
            'primary_oc_category' => 0,
            'primary_ms_category' => 0,
        ];
        $this->msProductRepository->create($this->checkData($msProductData, $data));

        $productToStoreData = [
            'product_id' => $product->product_id,
            'store_id' => $user->store_id
        ];

        $this->productToStoreRepository->create($productToStoreData);

        if (isset($data['product_oc_category'])) {
            foreach ($data['product_oc_category'] as $category) {
                $this->productToCategoryRepository->create([
                    'product_id' => $product->product_id,
                    'category_id' => $category
                ]);
            }
        }

        $this->productDescriptionRepository->bulkInsert($this->prepareDescriptionData($data, $product));

        $additionalImages = $this->prepareImages($data, $product, $user);
        $this->productImageRepository->bulkInsert($additionalImages);

        return $product;
    }

    public function update($request, $product)
    {
        $oldImages[] = $product->image;

        $productId = $product->product_id;
        $user = $request->user();

        if (!$user->isSeller()) {
            ApiException::throw(ApiException::USER_NOT_SELLER);
        }

        $data = $request->all();
        $sellerId = $user->customer_id;

        $productData = [
            'model' => isset($data['name'][Product::DEFAULT_LANGUAGE]) ? $data['name'][Product::DEFAULT_LANGUAGE] : '',
            'image' => isset($data['images'][0]) ? UploadHelper::sellersImagePath($data['images'][0], $sellerId, false) : '',
            'date_modified' => Carbon::now(),
        ];

        $productData = $this->unsetEmptyValues(array_merge($productData, $data));
        $this->productRepository->update($productData, $product->product_id);

        if (isset($data['product_oc_category'])) {
            $this->productToCategoryRepository->delete($productId);
            foreach ($data['product_oc_category'] as $category) {
                $this->productToCategoryRepository->create([
                    'product_id' => $product->product_id,
                    'category_id' => $category
                ]);
            }
        }

        $this->productDescriptionRepository->delete($productId);
        $this->productDescriptionRepository->bulkInsert($this->prepareDescriptionData($data, $product));

        $productImages = $this->productImageRepository->getProductImagesByProductId($productId);

        foreach ($productImages as $image) {
            $oldImages[] = $image->image;
        }

        if (isset($data['images'])) {
            $deleteImages = array_diff($oldImages, $data['images']);
            foreach ($deleteImages as $i) {
                UploadHelper::deleteImage($i);
            }
            $this->productImageRepository->deleteProductImagesByProductId($productId);
        }


        $this->productImageRepository->bulkInsert($this->prepareImages($data, $product, $user));

        return $this->productRepository->find($productId);
    }

    private function unsetEmptyValues($productData)
    {
        foreach ($productData as $key => $data) {
            if (empty($data)) {
                unset($productData[$key]);
            }
        }

        return $productData;
    }

    private function prepareImages($data, $product, $user)
    {
        $images = [];

        if (isset($data['images'])) {
            foreach ($data['images'] as $key => $value) {
                $newImagePath = UploadHelper::sellersImagePath($value, $user->customer_id, false);
                $newFullImagePath = UploadHelper::sellersImagePath($value, $user->customer_id);
                if ($key > 0) {
                    $images[] = [
                        'product_id' => $product->product_id,
                        'image' => $newImagePath,
                        'sort_order' => $key
                    ];
                }
                $path = pathinfo($value);
                if (strpos($path['dirname'], UploadHelper::SELLERS_PRODUCT_IMAGE_PATH) === false) {
                    UploadHelper::moveProductImageFromTemporaryFolder($value, $newFullImagePath);
                }
            }
        }

        return $images;
    }

    private function prepareDescriptionData($data, $product)
    {
        $productDescriptionData = [];

        foreach ($data['name'] as $key => $value) {

            $productDescriptionData[] = [
                'product_id' => $product->product_id,
                'language_id' => $key,
                'name' => $value,
                'description' => isset($data['description'][$key]) ? $data['description'][$key] : '',
                'tag' => isset($data['tag'][$key]) ? $data['tag'][$key] : '',
                'meta_title' => isset($data['meta_title'][$key]) ? $data['meta_title'][$key] : '',
                'meta_description' => isset($data['meta_description'][$key]) ? $data['meta_description'][$key] : '',
                'meta_keyword' => isset($data['meta_keyword'][$key]) ? $data['meta_keyword'][$key] : '',
            ];
        }

        return $productDescriptionData;
    }

    private function checkData($productData, $data)
    {
        foreach ($productData as $key => $d) {
            if (isset($data[$key]) && !empty($data[$key])) {
                $productData[$key] = $data[$key];
            }
        }

        return $productData;
    }
}
