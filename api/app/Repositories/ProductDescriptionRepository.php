<?php

namespace App\Repositories;

use App\Models\ProductDescription;
use App\Repositories\BaseRepository;

/**
 * Class ProductDescriptionRepository
 * @package App\Repositories
 * @version March 31, 2021, 9:10 am UTC
*/

class ProductDescriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name',
        'description',
        'tag',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductDescription::class;
    }


    /**
     * @param $input
     * @return mixed
     */
    public function bulkInsert($input)
    {
        return $this->model()::insert($input);
    }
}
