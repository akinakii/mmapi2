<?php

namespace App\Repositories;

use App\Models\CategoryDescription;
use App\Repositories\BaseRepository;

/**
 * Class CategoryDescriptionRepository
 * @package App\Repositories
 * @version April 7, 2021, 1:28 pm UTC
*/

class CategoryDescriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryDescription::class;
    }
}
