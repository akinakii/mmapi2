<?php

namespace App\Repositories;

use App\Models\SellerAddress;
use App\Repositories\BaseRepository;

/**
 * Class SellerAddressRepository
 * @package App\Repositories
 * @version March 19, 2021, 10:26 am UTC
*/

class SellerAddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seller_id',
        'fullname',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'country_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellerAddress::class;
    }
}
