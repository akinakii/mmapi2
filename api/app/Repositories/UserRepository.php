<?php

namespace App\Repositories;

use App\Helpers\HashGeneratorHelper;
use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version April 19, 2019, 9:13 am UTC
 */
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * @param array $input
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($input)
    {
        if (isset($input['password'])) {
            $salt = HashGeneratorHelper::generateSalt();
            $input['salt'] = $salt;
            $input['password'] = HashGeneratorHelper::generateOpenCartHash($salt, $input['password']);
        }

        return parent::create($input);
    }

    /**
     * @param string $email
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserByEmail(string $email)
    {
        return User::query()
            ->where('email', $email)
            ->with(['language'])
            ->first();
    }

    /**
     * @param array $input
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function update($input, $id)
    {
        if (isset($input['password'])) {
            $salt = HashGeneratorHelper::generateSalt();
            $input['salt'] = $salt;
            $input['password'] = HashGeneratorHelper::generateOpenCartHash($salt, $input['password']);
        }

        return parent::update($input, $id);
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public function getUserByCustomerId($customerId)
    {
        return $this->model()::query()
            ->where('customer_id', $customerId)
            ->first();
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getUserByCode($code)
    {
        return $this->model()::query()
            ->where('code', $code)
            ->first();
    }
}
