<?php

namespace App\Repositories;

use App\Models\ProductToStore;
use App\Repositories\BaseRepository;

/**
 * Class ProductToStoreRepository
 * @package App\Repositories
 * @version March 31, 2021, 9:07 am UTC
*/

class ProductToStoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductToStore::class;
    }
}
