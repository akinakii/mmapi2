<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Repositories\BaseRepository;

/**
 * Class SettingRepository
 * @package App\Repositories
 * @version March 26, 2021, 11:59 am UTC
*/

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'code',
        'key',
        'value',
        'serialized'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }

    /**
     * @param $key
     * @param int $storeId
     * @return string
     */
    public static function getConfigValueByStoreIdAndKey($key, $storeId = 0)
    {
        $return = Setting::query()
        ->where('store_id', $storeId)
        ->where('key', $key)
        ->first();

        return isset($return->value) ? $return->value : '';
    }
}
