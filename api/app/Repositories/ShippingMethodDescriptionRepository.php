<?php

namespace App\Repositories;

use App\Models\ShippingMethodDescription;
use App\Repositories\BaseRepository;

/**
 * Class ShippingMethodDescriptionRepository
 * @package App\Repositories
 * @version April 8, 2021, 9:46 am UTC
*/

class ShippingMethodDescriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shipping_method_id',
        'name',
        'description',
        'language_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShippingMethodDescription::class;
    }
}
