<?php

namespace App\Repositories;

use App\Models\SellerDescription;
use App\Repositories\BaseRepository;

/**
 * Class SellerDescriptionRepository
 * @package App\Repositories
 * @version March 19, 2021, 10:27 am UTC
*/

class SellerDescriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seller_id',
        'language_id',
        'slogan',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellerDescription::class;
    }
}
