<?php

namespace App\Repositories;

use App\Models\ProductImage;
use App\Repositories\BaseRepository;

/**
 * Class ProductImageRepository
 * @package App\Repositories
 * @version March 31, 2021, 10:33 am UTC
*/

class ProductImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'image',
        'sort_order'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductImage::class;
    }

    public function bulkInsert($input)
    {
        if (!empty($input)) {
            $this->model()::insert($input);
        }
    }

    public function getProductImagesByProductId($productId)
    {
        return $this->model()::query()
            ->where('product_id', $productId)
            ->get();
    }

    public function deleteProductImagesByProductId($productId)
    {
        return $this->model()::query()
            ->where('product_id', $productId)
            ->delete();
    }
}
