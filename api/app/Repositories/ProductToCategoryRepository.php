<?php

namespace App\Repositories;

use App\Models\ProductToCategory;
use App\Repositories\BaseRepository;

/**
 * Class ProductToCategoryRepository
 * @package App\Repositories
 * @version April 1, 2021, 11:11 am UTC
*/

class ProductToCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductToCategory::class;
    }
}
