<?php

namespace App\Repositories;

use App\Models\Seller;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

/**
 * Class SellerRepository
 * @package App\Repositories
 * @version March 19, 2021, 10:23 am UTC
*/

class SellerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nickname',
        'company',
        'website',
        'description',
        'country_id',
        'zone_id',
        'avatar',
        'banner',
        'date_created',
        'seller_status',
        'seller_approved',
        'seller_group',
        'commission_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Seller::class;
    }
}
