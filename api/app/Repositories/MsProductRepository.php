<?php

namespace App\Repositories;

use App\Models\MsProduct;
use App\Repositories\BaseRepository;

/**
 * Class MsProductRepository
 * @package App\Repositories
 * @version March 31, 2021, 8:56 am UTC
*/

class MsProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seller_id',
        'product_status',
        'product_approved',
        'list_until',
        'commission_id',
        'primary_oc_category',
        'primary_ms_category'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MsProduct::class;
    }
}
