<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version March 30, 2021, 7:30 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'model',
        'sku',
        'upc',
        'ean',
        'jan',
        'isbn',
        'mpn',
        'location',
        'quantity',
        'stock_status_id',
        'image',
        'manufacturer_id',
        'shipping',
        'price',
        'points',
        'tax_class_id',
        'date_available',
        'weight',
        'weight_class_id',
        'length',
        'width',
        'height',
        'length_class_id',
        'subtract',
        'minimum',
        'sort_order',
        'status',
        'viewed',
        'date_added',
        'date_modified'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }

    /**
     * @param $productId
     * @param $sellerId
     * @return mixed
     */
    public function getProductByProductIdAndSellerId($productId, $sellerId)
    {
        return $this->model()::query()
            ->leftjoin('mms_ms_product','mms_ms_product.product_id','=','mms_product.product_id')
            ->where('mms_product.product_id', $productId)
            ->where('mms_ms_product.seller_id', $sellerId)
            ->first();
    }

    /**
     * @param $sellerId
     * @param int $perPage
     * @param string[] $columns
     * @return mixed
     */
    public function getProductsBySellerId($sellerId, $perPage = 0, $columns = ['*'])
    {
        $query = $this->model()::query()
            ->with(['descriptions'])
            ->leftjoin('mms_ms_product','mms_ms_product.product_id','=','mms_product.product_id')
            ->where('mms_ms_product.seller_id', $sellerId);

        if ($perPage > 0) {
            return $query->paginate($perPage, $columns);
        } else {
            return $query->get();
        }
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getFullProductByProductId($productId)
    {
        return $this->model()::query()
            ->with(['descriptions', 'images', 'productToCategory.category'])
            ->where('mms_product.product_id', $productId)
            ->first();
    }
}
