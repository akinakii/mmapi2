<?php

namespace App\Repositories;

use App\Models\SellerSetting;
use App\Repositories\BaseRepository;

/**
 * Class SellerSettingRepository
 * @package App\Repositories
 * @version March 24, 2021, 8:49 am UTC
*/

class SellerSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seller_id',
        'name',
        'value',
        'is_encoded'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellerSetting::class;
    }
}
