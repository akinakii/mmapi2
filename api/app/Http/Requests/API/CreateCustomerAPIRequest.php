<?php

namespace App\Http\Requests\API;

use App\Helpers\ValidationHelper;
use InfyOm\Generator\Request\APIRequest;

class CreateCustomerAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_group_id' => 'required|integer|exists:mms_customer_group,customer_group_id',
            'firstname' => 'required|string|max:32',
            'lastname' => 'required|string|max:32',
            'email' => 'required|email|max:96|unique:mms_customer',
            'telephone' => 'nullable|string|max:32',
            'fax' => 'nullable|string|max:32',
            'password' => 'required|confirmed|min:6|max:40',
            'newsletter' => 'required|boolean',
            'agree' => 'required|boolean',

            'company' => 'nullable|string|max:40',
            'address_1' => 'nullable|string|max:128',
            'address_2' => 'nullable|string|max:128',
            'city' => 'nullable|string|max:128',
            'postcode' => 'nullable|string|max:10',
            'country_id' => 'required|integer|exists:mms_country,country_id',
            'zone_id' => 'required|integer|exists:mms_zone,zone_id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return ValidationHelper::renderValidationMessages($this->rules());
    }
}
