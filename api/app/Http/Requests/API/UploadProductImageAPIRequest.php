<?php


namespace App\Http\Requests\API;


use App\Helpers\UploadHelper;
use App\Helpers\ValidationHelper;
use InfyOm\Generator\Request\APIRequest;

class UploadProductImageAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return UploadHelper::imageFileValidationRules();
    }

    /**
     * @return array
     */
    public function messages()
    {
        return ValidationHelper::renderValidationMessages($this->rules());
    }
}
