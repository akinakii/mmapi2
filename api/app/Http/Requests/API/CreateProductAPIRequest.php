<?php

namespace App\Http\Requests\API;

use App\Helpers\ValidationHelper;
use App\Models\Product;
use InfyOm\Generator\Request\APIRequest;

class CreateProductAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|array|min:1',
            'name.*' => 'required|string|max:64',
            'description' => 'required|array|min:1',
            'description.*' => 'required|string',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'minimum' => 'required|numeric',
            'product_oc_category' => 'sometimes|required|array|min:1',
            'tag' => 'sometimes|required|array|min:1',
            'meta_title' => 'sometimes|required|array|min:1',
            'meta_title.*' => 'sometimes|required|string|min:1|max:255',
            'meta_description' => 'sometimes|required|array|min:1',
            'meta_description.*' => 'sometimes|required|string|min:1|max:255',
            'meta_keyword' => 'sometimes|required|array|min:1',
            'meta_keyword.*' => 'sometimes|required|string|min:1|max:255',
            'images' => 'sometimes|required|array|min:1',
            'images.*' => 'sometimes|required|string|min:1|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return ValidationHelper::renderValidationMessages($this->rules());
    }
}
