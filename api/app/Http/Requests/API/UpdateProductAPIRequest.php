<?php

namespace App\Http\Requests\API;

use App\Helpers\ValidationHelper;
use App\Models\Product;
use InfyOm\Generator\Request\APIRequest;

class UpdateProductAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|array|min:1',
            'name.*' => 'sometimes|required|string|max:64',
            'description' => 'sometimes|required|array|min:1',
            'description.*' => 'sometimes|required|string',
            'price' => 'sometimes|required|numeric',
            'quantity' => 'sometimes|required|numeric',
            'minimum' => 'sometimes|required|numeric',
            'product_oc_category' => 'sometimes|required|array|min:1',
            'tag' => 'sometimes|required|array|min:1',
            'meta_title' => 'sometimes|required|array|min:1',
            'meta_title.*' => 'sometimes|required|string|min:1|max:255',
            'meta_description' => 'sometimes|required|array|min:1',
            'meta_description.*' => 'sometimes|required|string|min:1|max:255',
            'meta_keyword' => 'sometimes|required|array|min:1',
            'meta_keyword.*' => 'sometimes|required|string|min:1|max:255',
            'images' => 'sometimes|required|array|min:1',
            'images.*' => 'sometimes|required|string|min:1|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return ValidationHelper::renderValidationMessages($this->rules());
    }
}
