<?php

namespace App\Http\Controllers\API\Auth;

use App\Exceptions\ApiException;
use App\Helpers\DateHelper;
use App\Helpers\HashGeneratorHelper;
use App\Helpers\MailHelper;
use App\Helpers\PassportTokenHelper;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Auth\CheckCredentialsAPIRequest;
use App\Http\Requests\API\Auth\PasswordResetAPIRequest;
use App\Http\Requests\API\Auth\SignInUserAPIRequest;
use App\Http\Requests\API\Auth\SignUpAPIRequest;
use App\Http\Requests\API\Auth\UpdatePasswordAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthApiController extends AppBaseController
{

    /** @var  UserRepository */
    private $userRepository;
    /** @var DateHelper */
    private $dateHelper;
    /** @var UserService */
    private $userService;
    /** @var PassportTokenHelper */
    private $passportTokenHelper;
    /** @var MailHelper */
    private $mailHelper;

    /**
     * AuthApiController constructor.
     * @param UserRepository $userRepository
     * @param DateHelper $dateHelper
     * @param UserService $userService
     * @param PassportTokenHelper $passportTokenHelper,
     * @param MailHelper $mailHelper
     */
    public function __construct(
        UserRepository $userRepository,
        DateHelper $dateHelper,
        UserService $userService,
        PassportTokenHelper $passportTokenHelper,
        MailHelper $mailHelper
    ) {
        $this->userRepository = $userRepository;
        $this->dateHelper = $dateHelper;
        $this->userService = $userService;
        $this->passportTokenHelper = $passportTokenHelper;
        $this->mailHelper = $mailHelper;
    }


    /**
     * @param SignUpAPIRequest $request
     * @return mixed
     */
    public function signUp(SignUpAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->userService->signUp($input, User::CUSTOMER);

        return $this->sendResponse($user);
    }

    /**
     * @param CheckCredentialsAPIRequest $request
     * @return mixed
     */
    public function checkCredentials(CheckCredentialsAPIRequest $request)
    {
        return $this->sendResponse();
    }

    /**
     * @param SignInUserAPIRequest $request
     * @return mixed
     */
    public function login(SignInUserAPIRequest $request)
    {

        $data = $request->all();

        $email =  $data['login'];
        $password = $data['password'];

        $user = $this->passportTokenHelper->login($email, $password);

        if (is_null($user)) {
            ApiException::throw(ApiException::NOT_CORRECT_CREDENTIALS);
        }

        $result = $this->passportTokenHelper->createBearerTokenForUser($user, $request);

        return $this->sendResponse($result);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function accessToken(Request $request)
    {
        $user = $request->user();

        $accessToken = PassportTokenHelper::parseToken();
        PassportTokenHelper::refreshToken($user->token()->id);

        $data = [
            'accessToken' => $accessToken,
            'tokenType' => 'Bearer',
            'expiresAt' => Carbon::parse($user->token()->expires_at)->toDateTimeString(),
            'user' => $this->userRepository->getUserByCustomerId($user->customer_id)

        ];

        return $this->sendResponse($data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return $this->sendResponse();
    }

    /**
     * @param UpdatePasswordAPIRequest $request
     * @return mixed
     */
    public function changePassword(UpdatePasswordAPIRequest $request)
    {
        $data = $request->all();

        $code = $data['code'];

        $user = $this->userRepository->getUserByCode($code);

        $this->userRepository->update(['password' => $data['password'], 'code' => ''], $user->customer_id);

        return $this->sendResponse();
    }

    /**
     * @param PasswordResetAPIRequest $request
     * @return mixed
     */
    public function resetPassword(PasswordResetAPIRequest $request)
    {
        $data = $request->all();

        $user = $this->userRepository->getUserByEmail($data['email']);

        if (is_null($user)) {
            ApiException::throw(ApiException::USER_NOT_FOUND);
        }

        $code = HashGeneratorHelper::generatePasswordResetCode();
        $this->userRepository->update(['code' => $code], $user->customer_id);

        $this->mailHelper->resetPasswordEmail($code, $request, $user);

        return $this->sendResponse();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getUser(Request $request)
    {
        $user = $request->user();

        return $this->sendResponse($user);
    }

    public function getCodeByEmail(Request $request)
    {
        $input = $request->all();

        $user = User::query()
            ->where('email', $input['email'])
            ->first();

        if (is_null($user)) {
            return [];
        }

        return response()->json($user->code);
    }

}
