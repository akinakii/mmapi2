<?php

namespace App\Http\Controllers\API;

use App\Repositories\ShippingMethodDescriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ShippingMethodDescriptionController
 * @package App\Http\Controllers\API
 */

class ShippingMethodDescriptionAPIController extends AppBaseController
{
    /** @var  ShippingMethodDescriptionRepository */
    private $shippingMethodDescriptionRepository;

    public function __construct(ShippingMethodDescriptionRepository $shippingMethodDescriptionRepo)
    {
        $this->shippingMethodDescriptionRepository = $shippingMethodDescriptionRepo;
    }

    /**
     * Display a listing of the ShippingMethodDescription.
     * GET|HEAD /shippingMethodDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $shippingMethodDescriptions = $this->shippingMethodDescriptionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($shippingMethodDescriptions->toArray());
    }

}
