<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSellerAddressAPIRequest;
use App\Http\Requests\API\UpdateSellerAddressAPIRequest;
use App\Models\SellerAddress;
use App\Repositories\SellerAddressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SellerAddressController
 * @package App\Http\Controllers\API
 */

class SellerAddressAPIController extends AppBaseController
{
    /** @var  SellerAddressRepository */
    private $sellerAddressRepository;

    public function __construct(SellerAddressRepository $sellerAddressRepo)
    {
        $this->sellerAddressRepository = $sellerAddressRepo;
    }

    /**
     * Display a listing of the SellerAddress.
     * GET|HEAD /sellerAddresses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sellerAddresses = $this->sellerAddressRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sellerAddresses->toArray(), 'Seller Addresses retrieved successfully');
    }

    /**
     * Store a newly created SellerAddress in storage.
     * POST /sellerAddresses
     *
     * @param CreateSellerAddressAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSellerAddressAPIRequest $request)
    {
        $input = $request->all();

        $sellerAddress = $this->sellerAddressRepository->create($input);

        return $this->sendResponse($sellerAddress->toArray(), 'Seller Address saved successfully');
    }

    /**
     * Display the specified SellerAddress.
     * GET|HEAD /sellerAddresses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SellerAddress $sellerAddress */
        $sellerAddress = $this->sellerAddressRepository->find($id);

        if (empty($sellerAddress)) {
            return $this->sendError('Seller Address not found');
        }

        return $this->sendResponse($sellerAddress->toArray(), 'Seller Address retrieved successfully');
    }

    /**
     * Update the specified SellerAddress in storage.
     * PUT/PATCH /sellerAddresses/{id}
     *
     * @param int $id
     * @param UpdateSellerAddressAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellerAddressAPIRequest $request)
    {
        $input = $request->all();

        /** @var SellerAddress $sellerAddress */
        $sellerAddress = $this->sellerAddressRepository->find($id);

        if (empty($sellerAddress)) {
            return $this->sendError('Seller Address not found');
        }

        $sellerAddress = $this->sellerAddressRepository->update($input, $id);

        return $this->sendResponse($sellerAddress->toArray(), 'SellerAddress updated successfully');
    }

    /**
     * Remove the specified SellerAddress from storage.
     * DELETE /sellerAddresses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SellerAddress $sellerAddress */
        $sellerAddress = $this->sellerAddressRepository->find($id);

        if (empty($sellerAddress)) {
            return $this->sendError('Seller Address not found');
        }

        $sellerAddress->delete();

        return $this->sendSuccess('Seller Address deleted successfully');
    }
}
