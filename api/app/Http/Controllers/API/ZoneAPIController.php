<?php

namespace App\Http\Controllers\API;

use App\Repositories\ZoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ZoneController
 * @package App\Http\Controllers\API
 */

class ZoneAPIController extends AppBaseController
{
    /** @var  ZoneRepository */
    private $zoneRepository;

    public function __construct(ZoneRepository $zoneRepo)
    {
        $this->zoneRepository = $zoneRepo;
    }

    /**
     * Display a listing of the Zone.
     * GET|HEAD /zones
     *
     * @param Request $request
     * @param $countryId
     * @return Response
     */
    public function index($countryId, Request $request)
    {
        $countryId = (int)$countryId;
        $zones = $this->zoneRepository->getZoneListByCountryId($countryId);

        return $this->sendResponse($zones->toArray());
    }

}
