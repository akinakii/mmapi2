<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ApiException;
use App\Http\Requests\API\CreateProductAPIRequest;
use App\Http\Requests\API\UpdateProductAPIRequest;
use App\Http\Requests\API\UploadProductImageAPIRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductController
 * @package App\Http\Controllers\API
 */

class ProductAPIController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;
    private $productService;

    public function __construct(
        ProductRepository $productRepo,
        ProductService $productService
    ) {
        $this->productRepository = $productRepo;
        $this->productService = $productService;
    }

    /**
     * Display a listing of the Product.
     * GET|HEAD /products
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        if (!$user->isSeller()) {
            ApiException::throw(ApiException::USER_NOT_SELLER);
        }

        $perPage = !empty($request->get('per_page')) ? $request->get('per_page') : 0;

        $sellerId = $user->customer_id;
        $products = $this->productRepository->getProductsBySellerId($sellerId, $perPage);

        return $this->sendResponse($products->toArray());
    }

    /**
     * Store a newly created Product in storage.
     * POST /products
     *
     * @param CreateProductAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAPIRequest $request)
    {
        $product = $this->productService->create($request);

        return $this->sendResponse($product->toArray());
    }

    /**
     * Display the specified Product.
     * GET|HEAD /products/{id}
     *
     * @param int $productId
     *
     * @return Response
     */
    public function show($productId)
    {
        /** @var Product $product */
        $product = $this->productRepository->getFullProductByProductId($productId);

        if (is_null($product)) {
            ApiException::throw(ApiException::PRODUCT_NOT_FOUND);
        }

        return $this->sendResponse($product->toArray());
    }

    /**
     * Update the specified Product in storage.
     * PUT/PATCH /products/{productId}
     *
     * @param int $productId
     * @param UpdateProductAPIRequest $request
     *
     * @return Response
     */
    public function update($productId, UpdateProductAPIRequest $request)
    {
        $user = $request->user();
        $productId = (int) $productId;
        /** @var Product $product */
        $product = $this->productRepository->getProductByProductIdAndSellerId($productId, $user->customer_id);

        if (is_null($product)) {
           ApiException::throw(ApiException::PRODUCT_NOT_FOUND);
        }

        $product = $this->productService->update($request, $product);

        return $this->sendResponse($product->toArray());
    }

    /**
     * Remove the specified Product from storage.
     * DELETE /products/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            return $this->sendError('Product not found');
        }

        $product->delete();

        return $this->sendResponse([]);
    }

    /**
     * @param UploadProductImageAPIRequest $request
     */
    public function uploadImage(UploadProductImageAPIRequest $request)
    {
        return $this->sendResponse($this->productService->uploadProductImage($request));
    }
}
