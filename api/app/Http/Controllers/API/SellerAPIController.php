<?php

namespace App\Http\Controllers\API;

use App\Exceptions\ApiException;
use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\CreateSellerAPIRequest;
use App\Http\Requests\API\UpdateSellerAPIRequest;
use App\Models\Seller;
use App\Repositories\SellerRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SellerController
 * @package App\Http\Controllers\API
 */

class SellerAPIController extends AppBaseController
{
    /** @var  SellerRepository */
    private $sellerRepository;
    private $userService;

    public function __construct(
        SellerRepository $sellerRepo,
        UserService $userService
    ) {
        $this->sellerRepository = $sellerRepo;
        $this->userService = $userService;
    }


    /**
     * Store a newly created Seller in storage.
     * POST /sellers
     *
     * @param CreateSellerAPIRequest $request
     *
     * @return Response
     */
    public function register(CreateSellerAPIRequest $request)
    {
        $seller = $this->userService->registerSeller($request);

        return $this->sendResponse($seller->toArray());
    }

    /**
     * @param CreateCustomerAPIRequest $request
     * @return mixed
     */
    public function registerCustomer(CreateCustomerAPIRequest $request)
    {
       $response = $this->userService->registerCustomer($request);

       return $this->sendResponse($response->toArray());
    }

    /**
     * Display the specified Seller.
     * GET|HEAD /seller/profile
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function profile(Request $request)
    {
        $user = $request->user();

        if (!$user->isSeller()) {
            ApiException::throw(ApiException::USER_NOT_SELLER);
        }
dd(1);
        return $this->sendResponse($seller->toArray());
    }

    /**
     * Update the specified Seller in storage.
     * PUT/PATCH /sellers/{id}
     *
     * @param int $id
     * @param UpdateSellerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        $seller = $this->sellerRepository->update($input, $id);

        return $this->sendResponse($seller->toArray());
    }

}
