<?php

namespace App\Http\Controllers;

use Flugg\Responder\Facades\Responder;
use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result = [])
    {
        return Responder::success($result);
    }

    public function sendError($error, $code = 404, $data = [])
    {
        return Responder::error($code, $error)->data($data);
    }
}
