<?php


namespace App\Helpers;

use App\Helpers\OpenCartCore\LanguageHelper;
use App\Repositories\SettingRepository;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailHelper
{

    private $languageHelper;

    /**
     * MailHelper constructor.
     * @param LanguageHelper $languageHelper
     */
    public function __construct(
        LanguageHelper $languageHelper
    ) {
        $this->languageHelper = $languageHelper;
    }

    public static function sendMail($to, $subject = '', $params = [], $attachData = [])
    {
        $fromAddress = config('mail.from.address');
        $fromName = config('mail.from.name');

        try {
            Mail::send(
                'mails.index',
                [
                    'body' => $params['message']
                ],
                function ($mail) use ($fromAddress, $fromName, $to, $params, $subject, $attachData) {
                    /** @var $mail Message */
                    $mail->subject($subject);

                    foreach ($attachData as $file){
                        if($file['data']){
                            $mail->attachData($file['data'], $file['name'], ['mime' => $file['mime']]);
                        }
                    }

                    $mail->from($fromAddress, $fromName);
                    $mail->to($to);
                }
            );
        } catch (\Exception $exception) {
            Log::critical(__('messages.mail-send-fail').' '.$exception->getMessage());
        }
    }

    /**
     * @param $code
     * @param $request
     * @param $user
     */
    public function resetPasswordEmail($code, $request, $user)
    {
        $language = ['default' => $user->language->code];
        $this->languageHelper->load('mail/forgotten', $language);

        $storeName = html_entity_decode(SettingRepository::getConfigValueByStoreIdAndKey('config_name', $user->store_id), ENT_QUOTES, 'UTF-8');
        $subject = sprintf($this->languageHelper->get('text_subject'), $storeName);

        $message  = sprintf($this->languageHelper->get('text_greeting'), $storeName) . "\n\n";
        $message .= $this->languageHelper->get('text_change') . "\n\n";
        $message .= env('FRONT_URL') . 'account/reset?code=' . $code . "\n\n";
        $message .= sprintf($this->languageHelper->get('text_ip'), $request->ip()) . "\n\n";

        self::sendMail($user->email, $subject, ['message' => $message]);
    }
}
