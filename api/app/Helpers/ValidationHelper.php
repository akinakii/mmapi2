<?php

namespace App\Helpers;

class ValidationHelper
{
    /**
     * @param $rules
     * @return mixed
     */
    public static function renderValidationMessages($rules)
    {
        $return = [];

        foreach ($rules as $field => $rule) {

            if (is_string($rule)) {
                $rule = explode("|", $rule);
            }

            foreach ($rule as $r) {
                if (is_object($r)) {
                    continue;
                }
                if (strpos($r, ":")) {
                    $r = substr($r, 0, strpos($r, ":"));
                }
                $key = $field . '.' . $r;
                $fieldWithDashes = str_replace('_', '-', $field);
                $fieldWithDashes = str_replace('.*.', '-', $fieldWithDashes);

                $rulesWithDashes = str_replace('_', '-', $r);

                $message = 'validation-' . $fieldWithDashes . '-' . $rulesWithDashes;
                $return[$key] = $message;
            }

        }

        return $return;
    }

    /**
     * @param $string
     * @param $start
     * @param $end
     * @return bool|string
     */
    public static function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);

        if ($ini == 0) return '';

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

}
