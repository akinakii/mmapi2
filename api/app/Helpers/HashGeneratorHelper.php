<?php


namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Str;

class HashGeneratorHelper
{
    public static function generateHash()
    {
        return md5(Carbon::now()) . md5(mt_rand(0, 9)) . md5(Str::random(1));
    }

    public static function generateStrRandom($len)
    {
        return Str::random($len);
    }

    public static function generateOpenCartHash($salt, $password)
    {
        return sha1($salt . sha1($salt . sha1($password)));
    }

    public static function generateSalt()
    {
        return Str::random(env('SALT_LENGTH', 9));
    }

    public static function generatePasswordResetCode()
    {
        return Str::random(env('PASSWORD_RESET_CODE_LENGTH', 40));
    }
}
