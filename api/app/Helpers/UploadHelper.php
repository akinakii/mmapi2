<?php

namespace App\Helpers;

use App\Exceptions\ApiException;
use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class UploadHelper
{

    const SELLERS_PRODUCT_IMAGE_PATH = 'sellers/';

    /**
     * @return array
     */
    public static function imageFileValidationRules()
    {
        return [
            'images' => 'required|array|min:1',
            'images.*' => 'required|image|mimes:jpeg,png,jpg|max:4000',
        ];
    }

    public static function sellersImagePath($imageName, $sellerId, $full = true)
    {
        if ($full) {
            $path = DIR_IMAGE . self::SELLERS_PRODUCT_IMAGE_PATH . $sellerId . '/' . $imageName;
        } else {
            $path = self::SELLERS_PRODUCT_IMAGE_PATH . $sellerId . '/' . $imageName;
        }

        return $path;
    }

    /**
     * @param $request
     * @param bool $resize
     * @param string $height
     * @param string $width
     * @return array
     */
    public static function uploadImage($request, $resize = true, $height = '', $width = '')
    {
        $files = $request->file('images');
        $tempImagePath = SettingRepository::getConfigValueByStoreIdAndKey( 'msconf_temp_image_path');

        $fileNames = [];

        foreach ($files as $key => $file) {
            $originalName = $file->getClientOriginalName();
            $filename =   time() . '_' . md5(rand()) . '.' . $originalName;
            $destinationPath = DIR_IMAGE . $tempImagePath;
            $file->move($destinationPath, $filename);

            $fileNames['files'][$key]['name'] = $filename;

            if ($resize) {
                $thumb = self::fit($filename, $height, $width, $destinationPath);
                $fileNames['files'][$key]['thumb'] = $thumb;
            }

        }

        return $fileNames;
    }

    /**
     * @param string $path
     */
    public static function deleteImage($path)
    {
        if (file_exists($path)) {
            unlink(DIR_IMAGE . $path);
        }
    }

    public static function moveProductImageFromTemporaryFolder($imageName, $newPath)
    {
        $tempImagePath = SettingRepository::getConfigValueByStoreIdAndKey( 'msconf_temp_image_path');
        $temp = DIR_IMAGE . $tempImagePath;
        $oldPath = $temp . $imageName;

        self::moveFile($oldPath, $newPath);
    }

    /**
     * @param $oldPath
     * @param $newPath
     */
    public static function moveFile($oldPath, $newPath)
    {
        $path = pathinfo($newPath);
        if (!file_exists($path['dirname'])) {
            mkdir($path['dirname'], 0777, true);
        }

        if (file_exists($oldPath)) {
            copy($oldPath, $newPath);
        }
    }

    /**
     * @param $filename
     * @param $height
     * @param $width
     * @param $path
     */
    private static function fit($filename, $height, $width, $path)
    {
        try {
            $image = ImageManagerStatic::make($path . $filename);

        } catch (\Exception $exception) {
            ApiException::throw(ApiException::CORRUPTED_IMAGE_FILE);
        }

        if (empty($height)) {
            $height = SettingRepository::getConfigValueByStoreIdAndKey('msconf_preview_product_image_height');
        }

        if (empty($width)) {
            $width = SettingRepository::getConfigValueByStoreIdAndKey('msconf_preview_product_image_width');
        }

        $pathInfo = pathinfo($filename);

        $newFileName = $pathInfo['filename'] . '-' . $width . 'x' . $height . '.' . $pathInfo['extension'];

        $image->fit($width, $height)->encode();
        $image->save($path . $newFileName);

        return $newFileName;
    }
}
