<?php

namespace Database\Factories;

use App\Models\SellerAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class SellerAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SellerAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seller_id' => $this->faker->randomDigitNotNull,
        'fullname' => $this->faker->word,
        'address_1' => $this->faker->text,
        'address_2' => $this->faker->text,
        'city' => $this->faker->word,
        'state' => $this->faker->word,
        'zip' => $this->faker->word,
        'country_id' => $this->faker->randomDigitNotNull
        ];
    }
}
