<?php

namespace Database\Factories;

use App\Models\ProductDescription;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductDescriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductDescription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'language_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'description' => $this->faker->text,
        'tag' => $this->faker->text,
        'meta_title' => $this->faker->word,
        'meta_description' => $this->faker->word,
        'meta_keyword' => $this->faker->word
        ];
    }
}
