<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->randomDigitNotNull,
        'firstname' => $this->faker->word,
        'lastname' => $this->faker->word,
        'company' => $this->faker->word,
        'address_1' => $this->faker->word,
        'address_2' => $this->faker->word,
        'city' => $this->faker->word,
        'postcode' => $this->faker->word,
        'country_id' => $this->faker->randomDigitNotNull,
        'zone_id' => $this->faker->randomDigitNotNull,
        'custom_field' => $this->faker->text
        ];
    }
}
