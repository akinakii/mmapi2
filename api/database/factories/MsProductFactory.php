<?php

namespace Database\Factories;

use App\Models\MsProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class MsProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MsProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seller_id' => $this->faker->randomDigitNotNull,
        'product_status' => $this->faker->word,
        'product_approved' => $this->faker->word,
        'list_until' => $this->faker->word,
        'commission_id' => $this->faker->randomDigitNotNull,
        'primary_oc_category' => $this->faker->randomDigitNotNull,
        'primary_ms_category' => $this->faker->randomDigitNotNull
        ];
    }
}
