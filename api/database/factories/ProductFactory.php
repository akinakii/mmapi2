<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'model' => $this->faker->word,
        'sku' => $this->faker->word,
        'upc' => $this->faker->word,
        'ean' => $this->faker->word,
        'jan' => $this->faker->word,
        'isbn' => $this->faker->word,
        'mpn' => $this->faker->word,
        'location' => $this->faker->word,
        'quantity' => $this->faker->randomDigitNotNull,
        'stock_status_id' => $this->faker->randomDigitNotNull,
        'image' => $this->faker->word,
        'manufacturer_id' => $this->faker->randomDigitNotNull,
        'shipping' => $this->faker->word,
        'price' => $this->faker->word,
        'points' => $this->faker->randomDigitNotNull,
        'tax_class_id' => $this->faker->randomDigitNotNull,
        'date_available' => $this->faker->word,
        'weight' => $this->faker->word,
        'weight_class_id' => $this->faker->randomDigitNotNull,
        'length' => $this->faker->word,
        'width' => $this->faker->word,
        'height' => $this->faker->word,
        'length_class_id' => $this->faker->randomDigitNotNull,
        'subtract' => $this->faker->word,
        'minimum' => $this->faker->randomDigitNotNull,
        'sort_order' => $this->faker->randomDigitNotNull,
        'status' => $this->faker->word,
        'viewed' => $this->faker->randomDigitNotNull,
        'date_added' => $this->faker->date('Y-m-d H:i:s'),
        'date_modified' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
