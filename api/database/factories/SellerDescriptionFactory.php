<?php

namespace Database\Factories;

use App\Models\SellerDescription;
use Illuminate\Database\Eloquent\Factories\Factory;

class SellerDescriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SellerDescription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seller_id' => $this->faker->randomDigitNotNull,
        'language_id' => $this->faker->randomDigitNotNull,
        'slogan' => $this->faker->text,
        'description' => $this->faker->text
        ];
    }
}
