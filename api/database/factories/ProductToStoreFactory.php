<?php

namespace Database\Factories;

use App\Models\ProductToStore;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductToStoreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductToStore::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'store_id' => $this->faker->randomDigitNotNull
        ];
    }
}
