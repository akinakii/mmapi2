<?php

namespace Database\Factories;

use App\Models\Seller;
use Illuminate\Database\Eloquent\Factories\Factory;

class SellerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Seller::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nickname' => $this->faker->word,
        'company' => $this->faker->word,
        'website' => $this->faker->word,
        'description' => $this->faker->text,
        'country_id' => $this->faker->randomDigitNotNull,
        'zone_id' => $this->faker->randomDigitNotNull,
        'avatar' => $this->faker->word,
        'banner' => $this->faker->word,
        'date_created' => $this->faker->date('Y-m-d H:i:s'),
        'seller_status' => $this->faker->word,
        'seller_approved' => $this->faker->word,
        'seller_group' => $this->faker->randomDigitNotNull,
        'commission_id' => $this->faker->randomDigitNotNull
        ];
    }
}
